<?php

/* TwigBundle:Exception:error404.html.twig */
class __TwigTemplate_df5e0a53a11b137310db72f5547f2e51 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::layout.html.twig");

        $this->blocks = array(
            'PageTitle' => array($this, 'block_PageTitle'),
            'StyleSheets' => array($this, 'block_StyleSheets'),
            'NavbarOuter' => array($this, 'block_NavbarOuter'),
            'MainContainerOuter' => array($this, 'block_MainContainerOuter'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_PageTitle($context, array $blocks = array())
    {
        $this->displayParentBlock("PageTitle", $context, $blocks);
        echo " - Page non trouvée";
    }

    // line 6
    public function block_StyleSheets($context, array $blocks = array())
    {
        // line 7
        echo "<style type=\"text/css\">
    body {
        background-color: #f5f5f5;
    }

    .frame {
        position: absolute;
        top: 20%;
        bottom: 20%;
        left: 20%;
        right: 20%;
    }

    .title {
        font-size: 72px;
        line-height: 1;
    }
</style>
";
    }

    // line 27
    public function block_NavbarOuter($context, array $blocks = array())
    {
    }

    // line 29
    public function block_MainContainerOuter($context, array $blocks = array())
    {
        // line 30
        echo "<!-- MAIN CONTAINER -->
<div class=\"well frame\">
    <p class=\"title\">Erreur 404</p>

    <h3>Page non trouvée</h3>

    <p class=\"lead text-center\">
        ";
        // line 37
        echo $this->env->getExtension('code')->formatFileFromText(nl2br(twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : null), "message"), "html", null, true)));
        echo "
    </p>

    <p>
        La page que demandez n'existe pas, ou plus. Vous pouvez
        essayer de revenir à la page précédente ou alors, pour retourner
        à l'accueil, <a href=\"";
        // line 43
        echo $this->env->getExtension('routing')->getPath("laplace_common_homepage");
        echo "\">cliquez ici</a>.
    </p>
</div>

";
    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error404.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  89 => 43,  80 => 37,  71 => 30,  68 => 29,  63 => 27,  41 => 7,  38 => 6,  31 => 3,);
    }
}
