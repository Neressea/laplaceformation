<?php

/* LaplaceTrainingBundle:Thread:view.html.twig */
class __TwigTemplate_9de81bd6cb4f446c73c9485f01bac879 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        $context["container"] = (((isset($context["admin"]) ? $context["admin"] : null)) ? ("LaplaceCommonBundle::admin-page.html.twig") : ("LaplaceCommonBundle::user-page.html.twig"));
        // line 6
        echo "
";
        // line 7
        $this->env->loadTemplate("LaplaceTrainingBundle:Thread:view.html.twig", "522353728")->display(array_merge($context, array("page" => array(0 => "thread", 1 => "view"))));
    }

    public function getTemplateName()
    {
        return "LaplaceTrainingBundle:Thread:view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  186 => 64,  145 => 39,  280 => 103,  255 => 90,  245 => 88,  157 => 50,  150 => 48,  487 => 243,  482 => 240,  475 => 236,  470 => 234,  465 => 232,  454 => 226,  452 => 225,  449 => 224,  438 => 215,  436 => 214,  426 => 208,  421 => 204,  418 => 202,  412 => 199,  406 => 196,  399 => 192,  396 => 191,  394 => 190,  391 => 189,  386 => 185,  380 => 182,  373 => 178,  363 => 174,  349 => 163,  346 => 162,  344 => 161,  336 => 156,  323 => 149,  307 => 136,  304 => 135,  302 => 134,  288 => 126,  281 => 122,  265 => 111,  263 => 110,  260 => 109,  251 => 89,  233 => 94,  223 => 89,  185 => 72,  170 => 53,  180 => 58,  174 => 57,  65 => 28,  234 => 74,  225 => 76,  237 => 85,  230 => 70,  221 => 64,  213 => 58,  207 => 54,  200 => 52,  110 => 25,  34 => 20,  335 => 118,  330 => 153,  325 => 97,  320 => 102,  318 => 101,  311 => 97,  308 => 96,  299 => 133,  297 => 95,  292 => 92,  289 => 91,  282 => 84,  275 => 100,  273 => 84,  259 => 107,  257 => 107,  249 => 102,  240 => 98,  238 => 77,  231 => 72,  222 => 75,  218 => 85,  211 => 65,  198 => 77,  194 => 75,  184 => 59,  179 => 71,  152 => 46,  96 => 16,  77 => 7,  63 => 17,  58 => 24,  53 => 21,  83 => 37,  227 => 77,  215 => 79,  210 => 77,  205 => 68,  226 => 85,  219 => 81,  195 => 69,  172 => 58,  202 => 62,  190 => 95,  146 => 69,  113 => 25,  108 => 23,  59 => 23,  52 => 19,  37 => 11,  267 => 94,  261 => 125,  254 => 90,  246 => 101,  244 => 118,  228 => 91,  212 => 70,  204 => 75,  197 => 88,  191 => 48,  175 => 69,  167 => 42,  159 => 35,  127 => 34,  118 => 27,  134 => 47,  129 => 41,  100 => 45,  178 => 65,  161 => 48,  131 => 36,  120 => 29,  104 => 20,  102 => 26,  23 => 3,  181 => 63,  153 => 51,  148 => 40,  124 => 36,  97 => 16,  90 => 14,  76 => 7,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 227,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 210,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 176,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 160,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 129,  285 => 85,  283 => 88,  278 => 86,  268 => 112,  264 => 79,  258 => 81,  252 => 79,  247 => 78,  241 => 86,  235 => 95,  229 => 73,  224 => 71,  220 => 81,  214 => 79,  208 => 68,  169 => 60,  143 => 35,  140 => 44,  132 => 51,  128 => 29,  119 => 39,  111 => 25,  107 => 28,  71 => 10,  177 => 60,  165 => 54,  160 => 40,  139 => 65,  135 => 36,  126 => 34,  114 => 36,  84 => 18,  70 => 10,  67 => 15,  61 => 13,  47 => 17,  28 => 3,  87 => 19,  55 => 17,  201 => 78,  196 => 63,  183 => 91,  171 => 43,  166 => 65,  163 => 52,  158 => 67,  156 => 48,  151 => 57,  142 => 59,  138 => 34,  136 => 37,  123 => 37,  121 => 35,  117 => 38,  115 => 26,  105 => 27,  91 => 48,  69 => 5,  66 => 27,  62 => 27,  49 => 19,  31 => 8,  29 => 6,  21 => 6,  25 => 5,  98 => 25,  93 => 42,  88 => 11,  78 => 14,  44 => 10,  32 => 4,  43 => 24,  40 => 11,  101 => 18,  94 => 15,  89 => 12,  85 => 10,  79 => 8,  75 => 53,  72 => 52,  68 => 29,  56 => 9,  50 => 14,  41 => 7,  27 => 18,  38 => 7,  24 => 7,  46 => 18,  35 => 9,  26 => 6,  22 => 2,  19 => 2,  209 => 77,  203 => 78,  199 => 71,  193 => 68,  189 => 66,  187 => 54,  182 => 62,  176 => 87,  173 => 54,  168 => 52,  164 => 41,  162 => 49,  154 => 44,  149 => 44,  147 => 44,  144 => 42,  141 => 43,  133 => 31,  130 => 33,  125 => 31,  122 => 25,  116 => 26,  112 => 30,  109 => 22,  106 => 48,  103 => 54,  99 => 17,  95 => 17,  92 => 26,  86 => 12,  82 => 9,  80 => 8,  73 => 19,  64 => 14,  60 => 20,  57 => 11,  54 => 27,  51 => 20,  48 => 11,  45 => 8,  42 => 13,  39 => 16,  36 => 5,  33 => 12,  30 => 7,);
    }
}


/* LaplaceTrainingBundle:Thread:view.html.twig */
class __TwigTemplate_9de81bd6cb4f446c73c9485f01bac879_522353728 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'PageTitle' => array($this, 'block_PageTitle'),
            'ContentTitle' => array($this, 'block_ContentTitle'),
            'ContentBody' => array($this, 'block_ContentBody'),
        );
    }

    protected function doGetParent(array $context)
    {
        return $this->env->resolveTemplate((isset($context["container"]) ? $context["container"] : null));
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 9
    public function block_PageTitle($context, array $blocks = array())
    {
        $this->displayParentBlock("PageTitle", $context, $blocks);
        echo " - ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["thread"]) ? $context["thread"] : null), "title"), "html", null, true);
    }

    // line 11
    public function block_ContentTitle($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["thread"]) ? $context["thread"] : null), "title"), "html", null, true);
    }

    // line 15
    public function block_ContentBody($context, array $blocks = array())
    {
        // line 16
        echo "
";
        // line 17
        if ($this->getAttribute((isset($context["thread"]) ? $context["thread"] : null), "need")) {
            // line 18
            echo "<h5>Besoin associé à la discussion</h5>
";
            // line 20
            $this->env->loadTemplate("LaplaceTrainingBundle::category-breadcrumb.html.twig")->display(array_merge($context, array("reference" => $this->getAttribute((isset($context["thread"]) ? $context["thread"] : null), "need"), "link" => $this->env->getExtension('routing')->getPath(((isset($context["laplace_training"]) ? $context["laplace_training"] : null) . "view_need"), array("id" => $this->getAttribute($this->getAttribute((isset($context["thread"]) ? $context["thread"] : null), "need"), "id"))))));
        }
        // line 28
        echo "
";
        // line 29
        if ($this->getAttribute((isset($context["thread"]) ? $context["thread"] : null), "request")) {
            // line 30
            echo "<h5>Demande associée à la discussion</h5>
";
            // line 32
            $this->env->loadTemplate("LaplaceTrainingBundle::category-breadcrumb.html.twig")->display(array_merge($context, array("reference" => $this->getAttribute((isset($context["thread"]) ? $context["thread"] : null), "request"), "link" => $this->env->getExtension('routing')->getPath(((isset($context["laplace_training"]) ? $context["laplace_training"] : null) . "view_request"), array("id" => $this->getAttribute($this->getAttribute((isset($context["thread"]) ? $context["thread"] : null), "request"), "id"))))));
        }
        // line 40
        echo "
<h5>Informations sur la discussion</h5>

";
        // line 43
        if ((!(isset($context["admin"]) ? $context["admin"] : null))) {
            // line 44
            echo "
<p>
    Discussion ouverte par ";
            // line 46
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["thread"]) ? $context["thread"] : null), "author"), "fullName"), "html", null, true);
            echo ",
    <strong>le ";
            // line 47
            echo twig_escape_filter($this->env, twig_localized_date_filter($this->env, $this->getAttribute((isset($context["thread"]) ? $context["thread"] : null), "date"), "full", "short"), "html", null, true);
            echo "</strong>
    entre les personnes suivantes :
    <ul>
    ";
            // line 50
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["participants"]) ? $context["participants"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["participant"]) {
                // line 51
                echo "        <li>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["participant"]) ? $context["participant"] : null), "fullName"), "html", null, true);
                echo "</li>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['participant'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 53
            echo "        <li><em>Correspondant formation</em></li>
    </ul>
</p>

";
        } else {
            // line 58
            echo "
<p>
    Discussion ouverte par
    <a href=\"";
            // line 61
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath(((isset($context["laplace_user"]) ? $context["laplace_user"] : null) . "view_profile"), array("username" => $this->getAttribute($this->getAttribute((isset($context["thread"]) ? $context["thread"] : null), "author"), "username"))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["thread"]) ? $context["thread"] : null), "author"), "fullName"), "html", null, true);
            echo "</a>,
    <strong>le ";
            // line 62
            echo twig_escape_filter($this->env, twig_localized_date_filter($this->env, $this->getAttribute((isset($context["thread"]) ? $context["thread"] : null), "date"), "full", "short"), "html", null, true);
            echo "</strong>
    entre les personnes suivantes :
    <ul>
    ";
            // line 65
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["participants"]) ? $context["participants"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["participant"]) {
                // line 66
                echo "        <li><a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath(((isset($context["laplace_user"]) ? $context["laplace_user"] : null) . "view_profile"), array("username" => $this->getAttribute((isset($context["participant"]) ? $context["participant"] : null), "username"))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["participant"]) ? $context["participant"] : null), "fullName"), "html", null, true);
                echo "</a></li>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['participant'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 68
            echo "        <li><em>Correspondant formation</em></li>
    </ul>
</p>

<p class=\"text-right\">

    <i class=\"icon-edit\"></i>
    <a href=\"";
            // line 75
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath(((isset($context["laplace_training"]) ? $context["laplace_training"] : null) . "edit_thread"), array("id" => $this->getAttribute((isset($context["thread"]) ? $context["thread"] : null), "id"))), "html", null, true);
            echo "\">Modifier</a>

    <br />

    <i class=\"icon-trash\"></i>
    <a href=\"";
            // line 80
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath(((isset($context["laplace_training"]) ? $context["laplace_training"] : null) . "delete_thread"), array("id" => $this->getAttribute((isset($context["thread"]) ? $context["thread"] : null), "id"))), "html", null, true);
            echo "\">Supprimer</a>

</p>

";
        }
        // line 85
        echo "
";
        // line 87
        echo "
<hr />

";
        // line 91
        echo "
<h3>Messages</h3>

";
        // line 94
        if (twig_test_empty($this->getAttribute((isset($context["thread"]) ? $context["thread"] : null), "messages"))) {
            // line 95
            echo "<p>
    <em>Aucun message</em>.
</p>
";
        }
        // line 99
        echo "
";
        // line 100
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["messages"]) ? $context["messages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["msginfo"]) {
            // line 101
            echo "
";
            // line 102
            $context["delete_form"] = $this->getAttribute((isset($context["delete_msg_forms"]) ? $context["delete_msg_forms"] : null), $this->getAttribute($this->getAttribute((isset($context["msginfo"]) ? $context["msginfo"] : null), "message"), "id"), array(), "array");
            // line 103
            echo "
<p>
    <div class=\"pull-right text-right\">
        ";
            // line 106
            if ((!(null === (isset($context["delete_form"]) ? $context["delete_form"] : null)))) {
                // line 107
                echo "        <button class=\"close\" type=\"button\" data-toggle=\"collapse\" data-target=\"#msg_";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["msginfo"]) ? $context["msginfo"] : null), "message"), "id"), "html", null, true);
                echo "\">
            <i class=\"icon-trash\"></i>
        </button>
        ";
            }
            // line 111
            echo "    </div>
    <span>
        Le ";
            // line 113
            echo twig_escape_filter($this->env, twig_localized_date_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["msginfo"]) ? $context["msginfo"] : null), "message"), "date"), "full", "short"), "html", null, true);
            echo ",
        ";
            // line 114
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["msginfo"]) ? $context["msginfo"] : null), "author"), "fullName"), "html", null, true);
            echo " a dit :
    </span>
    <br class=\"clearfix\" />
    <pre>";
            // line 117
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["msginfo"]) ? $context["msginfo"] : null), "message"), "text"), "html", null, true);
            echo "</pre>
    ";
            // line 118
            if ((!(null === (isset($context["delete_form"]) ? $context["delete_form"] : null)))) {
                // line 119
                echo "    <div id=\"msg_";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["msginfo"]) ? $context["msginfo"] : null), "message"), "id"), "html", null, true);
                echo "\" class=\"text-right collapse\">
        ";
                // line 120
                echo                 $this->env->getExtension('form')->renderer->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : null), 'form_start', array("attr" => array("class" => "form-inline")));
                echo "
        Supprimer ce message ?
        ";
                // line 122
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["delete_form"]) ? $context["delete_form"] : null), "do"), 'widget', array("attr" => array("class" => "btn-danger")));
                echo "
        ";
                // line 123
                echo                 $this->env->getExtension('form')->renderer->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : null), 'form_end');
                echo "
    </div>
    ";
            }
            // line 126
            echo "</p>

";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['msginfo'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 129
        echo "

";
        // line 131
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["message_form"]) ? $context["message_form"] : null), 'form_start');
        echo "

<fieldset>

    <legend>Envoyer un message</legend>

    ";
        // line 137
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["message_form"]) ? $context["message_form"] : null), "message"), 'rest');
        echo "

    ";
        // line 139
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["message_form"]) ? $context["message_form"] : null), 'rest');
        echo "

</fieldset>

";
        // line 143
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["message_form"]) ? $context["message_form"] : null), 'form_end');
        echo "



";
        // line 148
        echo "
";
    }

    public function getTemplateName()
    {
        return "LaplaceTrainingBundle:Thread:view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  334 => 148,  327 => 143,  315 => 137,  306 => 131,  284 => 122,  279 => 120,  274 => 119,  272 => 118,  262 => 114,  239 => 103,  206 => 85,  186 => 64,  145 => 39,  280 => 103,  255 => 90,  245 => 88,  157 => 50,  150 => 48,  487 => 243,  482 => 240,  475 => 236,  470 => 234,  465 => 232,  454 => 226,  452 => 225,  449 => 224,  438 => 215,  436 => 214,  426 => 208,  421 => 204,  418 => 202,  412 => 199,  406 => 196,  399 => 192,  396 => 191,  394 => 190,  391 => 189,  386 => 185,  380 => 182,  373 => 178,  363 => 174,  349 => 163,  346 => 162,  344 => 161,  336 => 156,  323 => 149,  307 => 136,  304 => 135,  302 => 129,  288 => 123,  281 => 122,  265 => 111,  263 => 110,  260 => 109,  251 => 89,  233 => 94,  223 => 89,  185 => 72,  170 => 66,  180 => 58,  174 => 57,  65 => 28,  234 => 101,  225 => 76,  237 => 102,  230 => 100,  221 => 95,  213 => 58,  207 => 54,  200 => 52,  110 => 25,  34 => 20,  335 => 118,  330 => 153,  325 => 97,  320 => 139,  318 => 101,  311 => 97,  308 => 96,  299 => 133,  297 => 95,  292 => 92,  289 => 91,  282 => 84,  275 => 100,  273 => 84,  259 => 107,  257 => 107,  249 => 102,  240 => 98,  238 => 77,  231 => 72,  222 => 75,  218 => 85,  211 => 65,  198 => 80,  194 => 75,  184 => 59,  179 => 71,  152 => 46,  96 => 16,  77 => 11,  63 => 17,  58 => 24,  53 => 21,  83 => 15,  227 => 99,  215 => 79,  210 => 77,  205 => 68,  226 => 85,  219 => 94,  195 => 69,  172 => 58,  202 => 62,  190 => 75,  146 => 69,  113 => 43,  108 => 40,  59 => 23,  52 => 19,  37 => 11,  267 => 94,  261 => 125,  254 => 111,  246 => 107,  244 => 106,  228 => 91,  212 => 70,  204 => 75,  197 => 88,  191 => 48,  175 => 69,  167 => 42,  159 => 35,  127 => 34,  118 => 27,  134 => 47,  129 => 50,  100 => 29,  178 => 65,  161 => 48,  131 => 36,  120 => 29,  104 => 20,  102 => 30,  23 => 3,  181 => 68,  153 => 51,  148 => 40,  124 => 36,  97 => 28,  90 => 14,  76 => 7,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 227,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 210,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 176,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 160,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 126,  285 => 85,  283 => 88,  278 => 86,  268 => 117,  264 => 79,  258 => 113,  252 => 79,  247 => 78,  241 => 86,  235 => 95,  229 => 73,  224 => 71,  220 => 81,  214 => 91,  208 => 68,  169 => 60,  143 => 35,  140 => 44,  132 => 51,  128 => 29,  119 => 46,  111 => 25,  107 => 28,  71 => 10,  177 => 60,  165 => 54,  160 => 62,  139 => 65,  135 => 36,  126 => 34,  114 => 36,  84 => 18,  70 => 10,  67 => 15,  61 => 13,  47 => 17,  28 => 3,  87 => 19,  55 => 17,  201 => 78,  196 => 63,  183 => 91,  171 => 43,  166 => 65,  163 => 52,  158 => 67,  156 => 48,  151 => 57,  142 => 53,  138 => 34,  136 => 37,  123 => 47,  121 => 35,  117 => 38,  115 => 44,  105 => 32,  91 => 18,  69 => 9,  66 => 27,  62 => 27,  49 => 19,  31 => 8,  29 => 6,  21 => 6,  25 => 5,  98 => 25,  93 => 42,  88 => 11,  78 => 14,  44 => 10,  32 => 4,  43 => 24,  40 => 11,  101 => 18,  94 => 20,  89 => 17,  85 => 10,  79 => 8,  75 => 53,  72 => 52,  68 => 29,  56 => 9,  50 => 14,  41 => 7,  27 => 18,  38 => 7,  24 => 7,  46 => 18,  35 => 9,  26 => 6,  22 => 2,  19 => 2,  209 => 87,  203 => 78,  199 => 71,  193 => 68,  189 => 66,  187 => 54,  182 => 62,  176 => 87,  173 => 54,  168 => 52,  164 => 41,  162 => 49,  154 => 61,  149 => 58,  147 => 44,  144 => 42,  141 => 43,  133 => 51,  130 => 33,  125 => 31,  122 => 25,  116 => 26,  112 => 30,  109 => 22,  106 => 48,  103 => 54,  99 => 17,  95 => 17,  92 => 26,  86 => 16,  82 => 9,  80 => 8,  73 => 19,  64 => 14,  60 => 20,  57 => 11,  54 => 27,  51 => 20,  48 => 11,  45 => 8,  42 => 13,  39 => 16,  36 => 5,  33 => 12,  30 => 7,);
    }
}
