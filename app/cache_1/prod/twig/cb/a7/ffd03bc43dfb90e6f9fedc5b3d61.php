<?php

/* LaplaceTrainingBundle:Thread:table.html.twig */
class __TwigTemplate_cba7ffd03bc43dfb90e6f9fedc5b3d61 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
<table class=\"table table-striped table-bordered thread-table\">

    <tr>
        <th class=\"thread-author\">Auteur</th>
        <th class=\"thread-title\">Titre</th>
        <th class=\"thread-date\">Date</th>
    </tr>

";
        // line 10
        if (twig_test_empty((isset($context["threads"]) ? $context["threads"] : null))) {
            // line 11
            echo "    <tr>
        <td colspan=\"3\"><em>Aucune discussion</em></td>
    </tr>
";
        }
        // line 15
        echo "
";
        // line 16
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["threads"]) ? $context["threads"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["threadinfo"]) {
            // line 17
            echo "
    <tr>
        <td class=\"thread-author\">
            ";
            // line 20
            if ((isset($context["admin"]) ? $context["admin"] : null)) {
                // line 21
                echo "            <a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath(((isset($context["laplace_user"]) ? $context["laplace_user"] : null) . "view_profile"), array("username" => $this->getAttribute($this->getAttribute((isset($context["threadinfo"]) ? $context["threadinfo"] : null), "author"), "username"))), "html", null, true);
                echo "\">";
                // line 22
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["threadinfo"]) ? $context["threadinfo"] : null), "author"), "fullName"), "html", null, true);
                // line 23
                echo "</a>
            ";
            } else {
                // line 25
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["threadinfo"]) ? $context["threadinfo"] : null), "author"), "fullName"), "html", null, true);
            }
            // line 27
            echo "        </td>
        <td class=\"thread-title\">
            <a href=\"";
            // line 29
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath(((isset($context["laplace_training"]) ? $context["laplace_training"] : null) . "view_thread"), array("id" => $this->getAttribute($this->getAttribute((isset($context["threadinfo"]) ? $context["threadinfo"] : null), "thread"), "id"))), "html", null, true);
            echo "\">
                ";
            // line 30
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["threadinfo"]) ? $context["threadinfo"] : null), "thread"), "title"), "html", null, true);
            echo "
            </a>
        </td>
        <td class=\"thread-date\">
            ";
            // line 34
            echo twig_escape_filter($this->env, twig_localized_date_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["threadinfo"]) ? $context["threadinfo"] : null), "thread"), "date"), "long", "none"), "html", null, true);
            echo "
        </td>
    </tr>

";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['threadinfo'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 39
        echo "
</table>
";
    }

    public function getTemplateName()
    {
        return "LaplaceTrainingBundle:Thread:table.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  334 => 148,  327 => 143,  315 => 137,  306 => 131,  284 => 122,  279 => 120,  274 => 119,  272 => 118,  262 => 114,  239 => 103,  206 => 85,  186 => 64,  145 => 39,  280 => 103,  255 => 90,  245 => 88,  157 => 50,  150 => 48,  487 => 243,  482 => 240,  475 => 236,  470 => 234,  465 => 232,  454 => 226,  452 => 225,  449 => 224,  438 => 215,  436 => 214,  426 => 208,  421 => 204,  418 => 202,  412 => 199,  406 => 196,  399 => 192,  396 => 191,  394 => 190,  391 => 189,  386 => 185,  380 => 182,  373 => 178,  363 => 174,  349 => 163,  346 => 162,  344 => 161,  336 => 156,  323 => 149,  307 => 136,  304 => 135,  302 => 129,  288 => 123,  281 => 122,  265 => 111,  263 => 110,  260 => 109,  251 => 89,  233 => 94,  223 => 89,  185 => 72,  170 => 66,  180 => 58,  174 => 57,  65 => 27,  234 => 101,  225 => 76,  237 => 102,  230 => 100,  221 => 95,  213 => 58,  207 => 54,  200 => 52,  110 => 25,  34 => 20,  335 => 118,  330 => 153,  325 => 97,  320 => 139,  318 => 101,  311 => 97,  308 => 96,  299 => 133,  297 => 95,  292 => 92,  289 => 91,  282 => 84,  275 => 100,  273 => 84,  259 => 107,  257 => 107,  249 => 102,  240 => 98,  238 => 77,  231 => 72,  222 => 75,  218 => 85,  211 => 65,  198 => 80,  194 => 75,  184 => 59,  179 => 71,  152 => 46,  96 => 16,  77 => 11,  63 => 17,  58 => 23,  53 => 21,  83 => 15,  227 => 99,  215 => 79,  210 => 77,  205 => 68,  226 => 85,  219 => 94,  195 => 69,  172 => 58,  202 => 62,  190 => 75,  146 => 69,  113 => 43,  108 => 40,  59 => 23,  52 => 21,  37 => 11,  267 => 94,  261 => 125,  254 => 111,  246 => 107,  244 => 106,  228 => 91,  212 => 70,  204 => 75,  197 => 88,  191 => 48,  175 => 69,  167 => 42,  159 => 35,  127 => 34,  118 => 27,  134 => 47,  129 => 50,  100 => 29,  178 => 65,  161 => 48,  131 => 36,  120 => 29,  104 => 20,  102 => 30,  23 => 3,  181 => 68,  153 => 51,  148 => 40,  124 => 36,  97 => 28,  90 => 14,  76 => 7,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 227,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 210,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 176,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 160,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 126,  285 => 85,  283 => 88,  278 => 86,  268 => 117,  264 => 79,  258 => 113,  252 => 79,  247 => 78,  241 => 86,  235 => 95,  229 => 73,  224 => 71,  220 => 81,  214 => 91,  208 => 68,  169 => 60,  143 => 35,  140 => 44,  132 => 51,  128 => 29,  119 => 46,  111 => 25,  107 => 28,  71 => 10,  177 => 60,  165 => 54,  160 => 62,  139 => 65,  135 => 36,  126 => 34,  114 => 36,  84 => 18,  70 => 10,  67 => 15,  61 => 13,  47 => 17,  28 => 3,  87 => 19,  55 => 17,  201 => 78,  196 => 63,  183 => 91,  171 => 43,  166 => 65,  163 => 52,  158 => 67,  156 => 48,  151 => 57,  142 => 53,  138 => 34,  136 => 37,  123 => 47,  121 => 35,  117 => 38,  115 => 44,  105 => 32,  91 => 39,  69 => 29,  66 => 27,  62 => 25,  49 => 19,  31 => 8,  29 => 6,  21 => 6,  25 => 5,  98 => 25,  93 => 42,  88 => 11,  78 => 14,  44 => 10,  32 => 11,  43 => 24,  40 => 11,  101 => 18,  94 => 20,  89 => 17,  85 => 10,  79 => 8,  75 => 53,  72 => 52,  68 => 29,  56 => 22,  50 => 20,  41 => 16,  27 => 18,  38 => 15,  24 => 7,  46 => 18,  35 => 9,  26 => 6,  22 => 2,  19 => 1,  209 => 87,  203 => 78,  199 => 71,  193 => 68,  189 => 66,  187 => 54,  182 => 62,  176 => 87,  173 => 54,  168 => 52,  164 => 41,  162 => 49,  154 => 61,  149 => 58,  147 => 44,  144 => 42,  141 => 43,  133 => 51,  130 => 33,  125 => 31,  122 => 25,  116 => 26,  112 => 30,  109 => 22,  106 => 48,  103 => 54,  99 => 17,  95 => 17,  92 => 26,  86 => 16,  82 => 9,  80 => 34,  73 => 30,  64 => 14,  60 => 20,  57 => 11,  54 => 27,  51 => 20,  48 => 11,  45 => 17,  42 => 13,  39 => 16,  36 => 5,  33 => 12,  30 => 10,);
    }
}
