<?php

/* LaplaceUserBundle:UserProfile:view-limited.html.twig */
class __TwigTemplate_eea55a65ea975a3f460aaa847357f545 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "

";
        // line 3
        $this->env->loadTemplate("LaplaceUserBundle:UserProfile:view-limited.html.twig", "363311300")->display(array_merge($context, array("page" => array(0 => "profile", 1 => "limited"))));
    }

    public function getTemplateName()
    {
        return "LaplaceUserBundle:UserProfile:view-limited.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  23 => 3,  181 => 63,  153 => 44,  148 => 42,  124 => 31,  97 => 16,  90 => 11,  76 => 6,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 144,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 112,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 105,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 90,  285 => 89,  283 => 88,  278 => 86,  268 => 85,  264 => 84,  258 => 81,  252 => 80,  247 => 78,  241 => 77,  235 => 74,  229 => 73,  224 => 71,  220 => 70,  214 => 69,  208 => 68,  169 => 60,  143 => 40,  140 => 55,  132 => 51,  128 => 49,  119 => 42,  111 => 24,  107 => 36,  71 => 17,  177 => 65,  165 => 64,  160 => 61,  139 => 50,  135 => 47,  126 => 45,  114 => 25,  84 => 28,  70 => 20,  67 => 15,  61 => 13,  47 => 9,  28 => 3,  87 => 20,  55 => 15,  201 => 92,  196 => 90,  183 => 70,  171 => 56,  166 => 71,  163 => 70,  158 => 67,  156 => 58,  151 => 57,  142 => 59,  138 => 38,  136 => 56,  123 => 47,  121 => 46,  117 => 44,  115 => 43,  105 => 40,  91 => 31,  69 => 4,  66 => 15,  62 => 23,  49 => 19,  31 => 5,  29 => 3,  21 => 2,  25 => 4,  98 => 31,  93 => 28,  88 => 10,  78 => 21,  44 => 12,  32 => 4,  43 => 6,  40 => 8,  101 => 32,  94 => 22,  89 => 20,  85 => 9,  79 => 18,  75 => 23,  72 => 16,  68 => 14,  56 => 9,  50 => 10,  41 => 5,  27 => 4,  38 => 6,  24 => 4,  46 => 7,  35 => 5,  26 => 6,  22 => 2,  19 => 1,  209 => 82,  203 => 78,  199 => 67,  193 => 73,  189 => 71,  187 => 84,  182 => 66,  176 => 64,  173 => 74,  168 => 66,  164 => 52,  162 => 62,  154 => 54,  149 => 51,  147 => 58,  144 => 53,  141 => 51,  133 => 36,  130 => 35,  125 => 44,  122 => 30,  116 => 36,  112 => 42,  109 => 41,  106 => 33,  103 => 19,  99 => 30,  95 => 34,  92 => 33,  86 => 28,  82 => 8,  80 => 19,  73 => 19,  64 => 14,  60 => 13,  57 => 11,  54 => 10,  51 => 14,  48 => 8,  45 => 8,  42 => 7,  39 => 9,  36 => 5,  33 => 4,  30 => 7,);
    }
}


/* LaplaceUserBundle:UserProfile:view-limited.html.twig */
class __TwigTemplate_eea55a65ea975a3f460aaa847357f545_363311300 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("LaplaceCommonBundle::admin-page.html.twig");

        $this->blocks = array(
            'PageTitle' => array($this, 'block_PageTitle'),
            'ContentTitle' => array($this, 'block_ContentTitle'),
            'ContentBody' => array($this, 'block_ContentBody'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "LaplaceCommonBundle::admin-page.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 6
    public function block_PageTitle($context, array $blocks = array())
    {
        $this->displayParentBlock("PageTitle", $context, $blocks);
        echo " - Tous les profils limités (";
        echo twig_escape_filter($this->env, (isset($context["requested"]) ? $context["requested"] : null), "html", null, true);
        echo ")";
    }

    // line 8
    public function block_ContentTitle($context, array $blocks = array())
    {
        echo "Tous les profils limités de l'année ";
        echo twig_escape_filter($this->env, (isset($context["requested"]) ? $context["requested"] : null), "html", null, true);
    }

    // line 12
    public function block_ContentBody($context, array $blocks = array())
    {
        // line 13
        echo "
";
        // line 14
        $this->env->loadTemplate("::pagination.html.twig")->display(array_merge($context, array("route_name" => ((isset($context["laplace_user"]) ? $context["laplace_user"] : null) . "limited_profiles"), "route_args" => array())));
        // line 16
        echo "
";
        // line 17
        $this->env->loadTemplate("LaplaceUserBundle:UserProfile:table.html.twig")->display(array_merge($context, array("show_role_limited" => true)));
        // line 19
        echo "
";
        // line 20
        $this->env->loadTemplate("::pagination.html.twig")->display(array_merge($context, array("route_name" => ((isset($context["laplace_user"]) ? $context["laplace_user"] : null) . "limited_profiles"), "route_args" => array())));
        // line 22
        echo "
";
    }

    public function getTemplateName()
    {
        return "LaplaceUserBundle:UserProfile:view-limited.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  104 => 22,  102 => 20,  23 => 3,  181 => 63,  153 => 44,  148 => 42,  124 => 31,  97 => 17,  90 => 11,  76 => 6,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 144,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 112,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 105,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 90,  285 => 89,  283 => 88,  278 => 86,  268 => 85,  264 => 84,  258 => 81,  252 => 80,  247 => 78,  241 => 77,  235 => 74,  229 => 73,  224 => 71,  220 => 70,  214 => 69,  208 => 68,  169 => 60,  143 => 40,  140 => 55,  132 => 51,  128 => 49,  119 => 42,  111 => 24,  107 => 36,  71 => 17,  177 => 65,  165 => 64,  160 => 61,  139 => 50,  135 => 47,  126 => 45,  114 => 25,  84 => 28,  70 => 6,  67 => 15,  61 => 13,  47 => 9,  28 => 3,  87 => 20,  55 => 15,  201 => 92,  196 => 90,  183 => 70,  171 => 56,  166 => 71,  163 => 70,  158 => 67,  156 => 58,  151 => 57,  142 => 59,  138 => 38,  136 => 56,  123 => 47,  121 => 46,  117 => 44,  115 => 43,  105 => 40,  91 => 31,  69 => 4,  66 => 15,  62 => 23,  49 => 19,  31 => 5,  29 => 3,  21 => 2,  25 => 4,  98 => 31,  93 => 28,  88 => 10,  78 => 21,  44 => 12,  32 => 4,  43 => 6,  40 => 8,  101 => 32,  94 => 16,  89 => 13,  85 => 9,  79 => 8,  75 => 23,  72 => 16,  68 => 14,  56 => 9,  50 => 10,  41 => 5,  27 => 4,  38 => 6,  24 => 4,  46 => 7,  35 => 5,  26 => 6,  22 => 2,  19 => 1,  209 => 82,  203 => 78,  199 => 67,  193 => 73,  189 => 71,  187 => 84,  182 => 66,  176 => 64,  173 => 74,  168 => 66,  164 => 52,  162 => 62,  154 => 54,  149 => 51,  147 => 58,  144 => 53,  141 => 51,  133 => 36,  130 => 35,  125 => 44,  122 => 30,  116 => 36,  112 => 42,  109 => 41,  106 => 33,  103 => 19,  99 => 19,  95 => 34,  92 => 14,  86 => 12,  82 => 8,  80 => 19,  73 => 19,  64 => 14,  60 => 13,  57 => 11,  54 => 10,  51 => 14,  48 => 8,  45 => 8,  42 => 7,  39 => 9,  36 => 5,  33 => 4,  30 => 7,);
    }
}
