<?php

/* LaplaceTrainingBundle:Thread:edit.html.twig */
class __TwigTemplate_415b5c9740688462b43a2a1b3586e2b0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
";
        // line 2
        $this->env->loadTemplate("LaplaceTrainingBundle:Thread:edit.html.twig", "1648764374")->display(array_merge($context, array("page" => array(0 => "thread", 1 => "edit"))));
    }

    public function getTemplateName()
    {
        return "LaplaceTrainingBundle:Thread:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  280 => 103,  255 => 90,  245 => 88,  157 => 50,  150 => 48,  487 => 243,  482 => 240,  475 => 236,  470 => 234,  465 => 232,  454 => 226,  452 => 225,  449 => 224,  438 => 215,  436 => 214,  426 => 208,  421 => 204,  418 => 202,  412 => 199,  406 => 196,  399 => 192,  396 => 191,  394 => 190,  391 => 189,  386 => 185,  380 => 182,  373 => 178,  363 => 174,  349 => 163,  346 => 162,  344 => 161,  336 => 156,  323 => 149,  307 => 136,  304 => 135,  302 => 134,  288 => 126,  281 => 122,  265 => 111,  263 => 110,  260 => 109,  251 => 89,  233 => 94,  223 => 89,  185 => 72,  170 => 55,  180 => 58,  174 => 57,  65 => 28,  234 => 74,  225 => 76,  237 => 85,  230 => 70,  221 => 64,  213 => 58,  207 => 54,  200 => 52,  110 => 25,  34 => 20,  335 => 118,  330 => 153,  325 => 97,  320 => 102,  318 => 101,  311 => 97,  308 => 96,  299 => 133,  297 => 95,  292 => 92,  289 => 91,  282 => 84,  275 => 100,  273 => 84,  259 => 107,  257 => 107,  249 => 102,  240 => 98,  238 => 77,  231 => 72,  222 => 75,  218 => 85,  211 => 65,  198 => 77,  194 => 75,  184 => 59,  179 => 71,  152 => 46,  96 => 16,  77 => 7,  63 => 17,  58 => 24,  53 => 21,  83 => 37,  227 => 77,  215 => 79,  210 => 77,  205 => 68,  226 => 85,  219 => 81,  195 => 69,  172 => 58,  202 => 62,  190 => 95,  146 => 69,  113 => 22,  108 => 33,  59 => 23,  52 => 19,  37 => 11,  267 => 94,  261 => 125,  254 => 90,  246 => 101,  244 => 118,  228 => 91,  212 => 70,  204 => 75,  197 => 88,  191 => 48,  175 => 69,  167 => 42,  159 => 35,  127 => 39,  118 => 24,  134 => 47,  129 => 41,  100 => 45,  178 => 65,  161 => 63,  131 => 45,  120 => 29,  104 => 20,  102 => 26,  23 => 3,  181 => 63,  153 => 51,  148 => 55,  124 => 36,  97 => 16,  90 => 14,  76 => 8,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 227,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 210,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 176,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 160,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 129,  285 => 85,  283 => 88,  278 => 86,  268 => 112,  264 => 79,  258 => 81,  252 => 79,  247 => 78,  241 => 86,  235 => 95,  229 => 73,  224 => 71,  220 => 81,  214 => 79,  208 => 68,  169 => 60,  143 => 35,  140 => 44,  132 => 51,  128 => 29,  119 => 39,  111 => 25,  107 => 28,  71 => 10,  177 => 60,  165 => 54,  160 => 40,  139 => 65,  135 => 36,  126 => 34,  114 => 36,  84 => 18,  70 => 10,  67 => 15,  61 => 13,  47 => 17,  28 => 3,  87 => 19,  55 => 17,  201 => 78,  196 => 63,  183 => 91,  171 => 43,  166 => 65,  163 => 52,  158 => 67,  156 => 48,  151 => 57,  142 => 59,  138 => 34,  136 => 43,  123 => 37,  121 => 35,  117 => 38,  115 => 32,  105 => 27,  91 => 48,  69 => 6,  66 => 27,  62 => 27,  49 => 19,  31 => 8,  29 => 6,  21 => 6,  25 => 5,  98 => 25,  93 => 42,  88 => 16,  78 => 14,  44 => 10,  32 => 4,  43 => 24,  40 => 11,  101 => 20,  94 => 15,  89 => 12,  85 => 11,  79 => 8,  75 => 53,  72 => 52,  68 => 29,  56 => 9,  50 => 14,  41 => 7,  27 => 18,  38 => 7,  24 => 7,  46 => 18,  35 => 9,  26 => 6,  22 => 2,  19 => 1,  209 => 77,  203 => 78,  199 => 64,  193 => 73,  189 => 66,  187 => 54,  182 => 62,  176 => 87,  173 => 68,  168 => 84,  164 => 41,  162 => 49,  154 => 49,  149 => 44,  147 => 44,  144 => 42,  141 => 43,  133 => 31,  130 => 33,  125 => 29,  122 => 25,  116 => 26,  112 => 30,  109 => 22,  106 => 48,  103 => 54,  99 => 31,  95 => 17,  92 => 26,  86 => 12,  82 => 10,  80 => 8,  73 => 19,  64 => 14,  60 => 20,  57 => 11,  54 => 27,  51 => 20,  48 => 11,  45 => 8,  42 => 13,  39 => 16,  36 => 5,  33 => 12,  30 => 7,);
    }
}


/* LaplaceTrainingBundle:Thread:edit.html.twig */
class __TwigTemplate_415b5c9740688462b43a2a1b3586e2b0_1648764374 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("LaplaceCommonBundle::admin-page.html.twig");

        $this->blocks = array(
            'PageTitle' => array($this, 'block_PageTitle'),
            'ContentTitle' => array($this, 'block_ContentTitle'),
            'ContentBody' => array($this, 'block_ContentBody'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "LaplaceCommonBundle::admin-page.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_PageTitle($context, array $blocks = array())
    {
        $this->displayParentBlock("PageTitle", $context, $blocks);
        echo " - Modifier une discussion";
    }

    // line 7
    public function block_ContentTitle($context, array $blocks = array())
    {
        echo "Modifier une discussion";
    }

    // line 9
    public function block_ContentBody($context, array $blocks = array())
    {
        // line 10
        echo "
";
        // line 11
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["edit_form"]) ? $context["edit_form"] : null), 'form_start');
        echo "

<fieldset>
    <legend>Modifier une discussion</legend>

    ";
        // line 16
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "thread"), 'rest');
        echo "

    ";
        // line 18
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["edit_form"]) ? $context["edit_form"] : null), 'rest');
        echo "

</fieldset>

";
        // line 22
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["edit_form"]) ? $context["edit_form"] : null), 'form_end');
        echo "



";
        // line 26
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["add_participant_form"]) ? $context["add_participant_form"] : null), 'form_start');
        echo "

<fieldset>
    <legend>Ajouter un participant</legend>

    Les personnes suivantes participent déjà à la discussion :
    <ul>
    ";
        // line 33
        if (twig_test_empty((isset($context["participants"]) ? $context["participants"] : null))) {
            // line 34
            echo "        <li><em>Aucun participant !</em></li>
    ";
        } else {
            // line 36
            echo "        ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["participants"]) ? $context["participants"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["participant"]) {
                // line 37
                echo "        <li>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["participant"]) ? $context["participant"] : null), "fullName"), "html", null, true);
                echo "</li>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['participant'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 39
            echo "    ";
        }
        // line 40
        echo "    </ul>

    <br />

    ";
        // line 44
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["add_participant_form"]) ? $context["add_participant_form"] : null), 'rest');
        echo "

</fieldset>

";
        // line 48
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["add_participant_form"]) ? $context["add_participant_form"] : null), 'form_end');
        echo "



";
        // line 52
        if ((!(null === (isset($context["rem_participant_form"]) ? $context["rem_participant_form"] : null)))) {
            // line 53
            echo "
";
            // line 54
            echo             $this->env->getExtension('form')->renderer->renderBlock((isset($context["rem_participant_form"]) ? $context["rem_participant_form"] : null), 'form_start');
            echo "

<fieldset>
    <legend>Retirer un participant</legend>

    <p>
        Les messages de la personne seront conservés mais celle-ci ne pourra
        plus accéder à la discussion.
    </p>

    ";
            // line 64
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["rem_participant_form"]) ? $context["rem_participant_form"] : null), 'rest');
            echo "

</fieldset>

";
            // line 68
            echo             $this->env->getExtension('form')->renderer->renderBlock((isset($context["rem_participant_form"]) ? $context["rem_participant_form"] : null), 'form_end');
            echo "

";
        }
        // line 71
        echo "
";
    }

    public function getTemplateName()
    {
        return "LaplaceTrainingBundle:Thread:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  186 => 64,  145 => 39,  280 => 103,  255 => 90,  245 => 88,  157 => 50,  150 => 48,  487 => 243,  482 => 240,  475 => 236,  470 => 234,  465 => 232,  454 => 226,  452 => 225,  449 => 224,  438 => 215,  436 => 214,  426 => 208,  421 => 204,  418 => 202,  412 => 199,  406 => 196,  399 => 192,  396 => 191,  394 => 190,  391 => 189,  386 => 185,  380 => 182,  373 => 178,  363 => 174,  349 => 163,  346 => 162,  344 => 161,  336 => 156,  323 => 149,  307 => 136,  304 => 135,  302 => 134,  288 => 126,  281 => 122,  265 => 111,  263 => 110,  260 => 109,  251 => 89,  233 => 94,  223 => 89,  185 => 72,  170 => 53,  180 => 58,  174 => 57,  65 => 28,  234 => 74,  225 => 76,  237 => 85,  230 => 70,  221 => 64,  213 => 58,  207 => 54,  200 => 52,  110 => 25,  34 => 20,  335 => 118,  330 => 153,  325 => 97,  320 => 102,  318 => 101,  311 => 97,  308 => 96,  299 => 133,  297 => 95,  292 => 92,  289 => 91,  282 => 84,  275 => 100,  273 => 84,  259 => 107,  257 => 107,  249 => 102,  240 => 98,  238 => 77,  231 => 72,  222 => 75,  218 => 85,  211 => 65,  198 => 77,  194 => 75,  184 => 59,  179 => 71,  152 => 46,  96 => 16,  77 => 7,  63 => 17,  58 => 24,  53 => 21,  83 => 37,  227 => 77,  215 => 79,  210 => 77,  205 => 68,  226 => 85,  219 => 81,  195 => 69,  172 => 58,  202 => 62,  190 => 95,  146 => 69,  113 => 22,  108 => 22,  59 => 23,  52 => 19,  37 => 11,  267 => 94,  261 => 125,  254 => 90,  246 => 101,  244 => 118,  228 => 91,  212 => 70,  204 => 75,  197 => 88,  191 => 48,  175 => 69,  167 => 42,  159 => 35,  127 => 34,  118 => 24,  134 => 47,  129 => 41,  100 => 45,  178 => 65,  161 => 48,  131 => 36,  120 => 29,  104 => 20,  102 => 26,  23 => 3,  181 => 63,  153 => 51,  148 => 40,  124 => 36,  97 => 16,  90 => 14,  76 => 7,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 227,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 210,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 176,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 160,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 129,  285 => 85,  283 => 88,  278 => 86,  268 => 112,  264 => 79,  258 => 81,  252 => 79,  247 => 78,  241 => 86,  235 => 95,  229 => 73,  224 => 71,  220 => 81,  214 => 79,  208 => 68,  169 => 60,  143 => 35,  140 => 44,  132 => 51,  128 => 29,  119 => 39,  111 => 25,  107 => 28,  71 => 10,  177 => 60,  165 => 54,  160 => 40,  139 => 65,  135 => 36,  126 => 34,  114 => 36,  84 => 18,  70 => 10,  67 => 15,  61 => 13,  47 => 17,  28 => 3,  87 => 19,  55 => 17,  201 => 78,  196 => 63,  183 => 91,  171 => 43,  166 => 65,  163 => 52,  158 => 67,  156 => 48,  151 => 57,  142 => 59,  138 => 34,  136 => 37,  123 => 37,  121 => 35,  117 => 38,  115 => 26,  105 => 27,  91 => 48,  69 => 5,  66 => 27,  62 => 27,  49 => 19,  31 => 8,  29 => 6,  21 => 6,  25 => 5,  98 => 25,  93 => 42,  88 => 11,  78 => 14,  44 => 10,  32 => 4,  43 => 24,  40 => 11,  101 => 18,  94 => 15,  89 => 12,  85 => 10,  79 => 8,  75 => 53,  72 => 52,  68 => 29,  56 => 9,  50 => 14,  41 => 7,  27 => 18,  38 => 7,  24 => 7,  46 => 18,  35 => 9,  26 => 6,  22 => 2,  19 => 1,  209 => 77,  203 => 78,  199 => 71,  193 => 68,  189 => 66,  187 => 54,  182 => 62,  176 => 87,  173 => 54,  168 => 52,  164 => 41,  162 => 49,  154 => 44,  149 => 44,  147 => 44,  144 => 42,  141 => 43,  133 => 31,  130 => 33,  125 => 33,  122 => 25,  116 => 26,  112 => 30,  109 => 22,  106 => 48,  103 => 54,  99 => 31,  95 => 17,  92 => 26,  86 => 12,  82 => 9,  80 => 8,  73 => 19,  64 => 14,  60 => 20,  57 => 11,  54 => 27,  51 => 20,  48 => 11,  45 => 8,  42 => 13,  39 => 16,  36 => 5,  33 => 12,  30 => 7,);
    }
}
