<?php

/* LaplaceTrainingBundle:Mail:new-message.html.twig */
class __TwigTemplate_76ee512d788af66ad80517eadd11865e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
<h3>Nouvelle réponse</h3>

<p>
<b>";
        // line 5
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["thread"]) ? $context["thread"] : null), "author"), "fullName"), "html", null, true);
        echo "</b> a répondu à la discussion intitulée
<b>";
        // line 6
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["thread"]) ? $context["thread"] : null), "title"), "html", null, true);
        echo "</b>.
</p>

<blockquote>";
        // line 9
        echo nl2br(twig_escape_filter($this->env, $this->getAttribute((isset($context["message"]) ? $context["message"] : null), "text"), "html", null, true));
        echo "</blockquote>

<p>
La discussion est accessible à l'adresse suivante :<br />
<a href=\"";
        // line 13
        echo twig_escape_filter($this->env, (isset($context["url"]) ? $context["url"] : null), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, (isset($context["url"]) ? $context["url"] : null), "html", null, true);
        echo "</a>
</p>
";
    }

    public function getTemplateName()
    {
        return "LaplaceTrainingBundle:Mail:new-message.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  280 => 103,  255 => 90,  245 => 88,  157 => 50,  150 => 48,  487 => 243,  482 => 240,  475 => 236,  470 => 234,  465 => 232,  454 => 226,  452 => 225,  449 => 224,  438 => 215,  436 => 214,  426 => 208,  421 => 204,  418 => 202,  412 => 199,  406 => 196,  399 => 192,  396 => 191,  394 => 190,  391 => 189,  386 => 185,  380 => 182,  373 => 178,  363 => 174,  349 => 163,  346 => 162,  344 => 161,  336 => 156,  323 => 149,  307 => 136,  304 => 135,  302 => 134,  288 => 126,  281 => 122,  265 => 111,  263 => 110,  260 => 109,  251 => 89,  233 => 94,  223 => 89,  185 => 72,  170 => 55,  180 => 58,  174 => 57,  65 => 28,  234 => 74,  225 => 76,  237 => 85,  230 => 70,  221 => 64,  213 => 58,  207 => 54,  200 => 52,  110 => 25,  34 => 20,  335 => 118,  330 => 153,  325 => 97,  320 => 102,  318 => 101,  311 => 97,  308 => 96,  299 => 133,  297 => 95,  292 => 92,  289 => 91,  282 => 84,  275 => 100,  273 => 84,  259 => 107,  257 => 107,  249 => 102,  240 => 98,  238 => 77,  231 => 72,  222 => 75,  218 => 85,  211 => 65,  198 => 77,  194 => 75,  184 => 59,  179 => 71,  152 => 46,  96 => 16,  77 => 7,  63 => 17,  58 => 24,  53 => 21,  83 => 37,  227 => 77,  215 => 79,  210 => 77,  205 => 68,  226 => 90,  219 => 80,  195 => 68,  172 => 40,  202 => 62,  190 => 95,  146 => 69,  113 => 22,  108 => 33,  59 => 23,  52 => 19,  37 => 11,  267 => 94,  261 => 125,  254 => 90,  246 => 101,  244 => 118,  228 => 91,  212 => 70,  204 => 79,  197 => 88,  191 => 48,  175 => 69,  167 => 42,  159 => 35,  127 => 39,  118 => 24,  134 => 47,  129 => 41,  100 => 45,  178 => 65,  161 => 63,  131 => 45,  120 => 29,  104 => 24,  102 => 26,  23 => 3,  181 => 63,  153 => 51,  148 => 55,  124 => 36,  97 => 30,  90 => 20,  76 => 7,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 227,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 210,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 176,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 160,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 129,  285 => 85,  283 => 88,  278 => 86,  268 => 112,  264 => 79,  258 => 81,  252 => 79,  247 => 78,  241 => 86,  235 => 95,  229 => 73,  224 => 71,  220 => 81,  214 => 82,  208 => 68,  169 => 60,  143 => 35,  140 => 44,  132 => 51,  128 => 29,  119 => 39,  111 => 25,  107 => 28,  71 => 10,  177 => 59,  165 => 54,  160 => 40,  139 => 65,  135 => 42,  126 => 34,  114 => 36,  84 => 18,  70 => 10,  67 => 15,  61 => 13,  47 => 17,  28 => 3,  87 => 19,  55 => 17,  201 => 78,  196 => 63,  183 => 91,  171 => 43,  166 => 65,  163 => 53,  158 => 67,  156 => 38,  151 => 57,  142 => 59,  138 => 34,  136 => 43,  123 => 37,  121 => 35,  117 => 38,  115 => 32,  105 => 27,  91 => 48,  69 => 5,  66 => 27,  62 => 27,  49 => 19,  31 => 8,  29 => 6,  21 => 6,  25 => 5,  98 => 25,  93 => 42,  88 => 16,  78 => 14,  44 => 10,  32 => 4,  43 => 24,  40 => 11,  101 => 20,  94 => 118,  89 => 12,  85 => 10,  79 => 8,  75 => 53,  72 => 52,  68 => 29,  56 => 9,  50 => 14,  41 => 7,  27 => 18,  38 => 7,  24 => 7,  46 => 18,  35 => 9,  26 => 6,  22 => 2,  19 => 1,  209 => 76,  203 => 78,  199 => 64,  193 => 73,  189 => 65,  187 => 54,  182 => 61,  176 => 87,  173 => 68,  168 => 84,  164 => 41,  162 => 49,  154 => 49,  149 => 29,  147 => 44,  144 => 41,  141 => 43,  133 => 31,  130 => 40,  125 => 43,  122 => 25,  116 => 30,  112 => 30,  109 => 28,  106 => 48,  103 => 54,  99 => 31,  95 => 17,  92 => 26,  86 => 12,  82 => 9,  80 => 8,  73 => 19,  64 => 14,  60 => 20,  57 => 11,  54 => 27,  51 => 20,  48 => 11,  45 => 8,  42 => 13,  39 => 16,  36 => 5,  33 => 12,  30 => 7,);
    }
}
