<?php

/* LaplaceUserBundle:UserProfile:create-local.html.twig */
class __TwigTemplate_ebc3d1e71564f3679f3ae01b35bb644b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
";
        // line 2
        $this->env->loadTemplate("LaplaceUserBundle:UserProfile:create-local.html.twig", "1041949701")->display(array_merge($context, array("page" => array(0 => "profile", 1 => "create"))));
    }

    public function getTemplateName()
    {
        return "LaplaceUserBundle:UserProfile:create-local.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  100 => 20,  178 => 65,  161 => 56,  131 => 39,  120 => 33,  104 => 22,  102 => 23,  23 => 3,  181 => 63,  153 => 51,  148 => 42,  124 => 31,  97 => 17,  90 => 16,  76 => 7,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 144,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 112,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 105,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 90,  285 => 89,  283 => 88,  278 => 86,  268 => 85,  264 => 84,  258 => 81,  252 => 80,  247 => 78,  241 => 77,  235 => 74,  229 => 73,  224 => 71,  220 => 70,  214 => 69,  208 => 68,  169 => 60,  143 => 45,  140 => 55,  132 => 51,  128 => 49,  119 => 30,  111 => 24,  107 => 25,  71 => 17,  177 => 65,  165 => 64,  160 => 61,  139 => 50,  135 => 47,  126 => 34,  114 => 28,  84 => 13,  70 => 6,  67 => 15,  61 => 13,  47 => 9,  28 => 3,  87 => 14,  55 => 15,  201 => 92,  196 => 90,  183 => 70,  171 => 56,  166 => 58,  163 => 70,  158 => 67,  156 => 58,  151 => 57,  142 => 59,  138 => 38,  136 => 41,  123 => 34,  121 => 46,  117 => 44,  115 => 43,  105 => 40,  91 => 31,  69 => 5,  66 => 15,  62 => 23,  49 => 19,  31 => 5,  29 => 3,  21 => 6,  25 => 4,  98 => 31,  93 => 28,  88 => 10,  78 => 11,  44 => 12,  32 => 4,  43 => 6,  40 => 8,  101 => 32,  94 => 18,  89 => 12,  85 => 10,  79 => 8,  75 => 23,  72 => 16,  68 => 14,  56 => 9,  50 => 10,  41 => 5,  27 => 4,  38 => 6,  24 => 7,  46 => 7,  35 => 5,  26 => 6,  22 => 2,  19 => 1,  209 => 82,  203 => 78,  199 => 67,  193 => 73,  189 => 71,  187 => 84,  182 => 66,  176 => 64,  173 => 62,  168 => 66,  164 => 52,  162 => 62,  154 => 54,  149 => 49,  147 => 58,  144 => 53,  141 => 51,  133 => 36,  130 => 35,  125 => 44,  122 => 30,  116 => 36,  112 => 42,  109 => 26,  106 => 33,  103 => 19,  99 => 19,  95 => 34,  92 => 14,  86 => 12,  82 => 9,  80 => 19,  73 => 19,  64 => 14,  60 => 13,  57 => 11,  54 => 10,  51 => 14,  48 => 8,  45 => 8,  42 => 7,  39 => 9,  36 => 5,  33 => 4,  30 => 7,);
    }
}


/* LaplaceUserBundle:UserProfile:create-local.html.twig */
class __TwigTemplate_ebc3d1e71564f3679f3ae01b35bb644b_1041949701 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("LaplaceCommonBundle::admin-page.html.twig");

        $this->blocks = array(
            'PageTitle' => array($this, 'block_PageTitle'),
            'ContentTitle' => array($this, 'block_ContentTitle'),
            'ContentBody' => array($this, 'block_ContentBody'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "LaplaceCommonBundle::admin-page.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_PageTitle($context, array $blocks = array())
    {
        $this->displayParentBlock("PageTitle", $context, $blocks);
        echo " - Créer un profil local";
    }

    // line 7
    public function block_ContentTitle($context, array $blocks = array())
    {
        echo "Créer un profil local";
    }

    // line 9
    public function block_ContentBody($context, array $blocks = array())
    {
        // line 10
        echo "
";
        // line 11
        if ((!array_key_exists("new_profile_form", $context))) {
            // line 12
            echo "
<p class=\"lead\">
    Cliquez sur le lien correspondant au type de profil local à créer :
    <ul>
        <li>
            <a href=\"";
            // line 17
            echo $this->env->getExtension('routing')->getPath(((isset($context["laplace_user"]) ? $context["laplace_user"] : null) . "create_local_profile_docto"));
            echo "\">Doctorant</a>
        </li>
        <li>
            <a href=\"";
            // line 20
            echo $this->env->getExtension('routing')->getPath(((isset($context["laplace_user"]) ? $context["laplace_user"] : null) . "create_local_profile_agent"));
            echo "\">Agent</a>
        </li>
    </ul>
</p>
";
        } else {
            // line 25
            echo "
";
            // line 26
            echo             $this->env->getExtension('form')->renderer->renderBlock((isset($context["new_profile_form"]) ? $context["new_profile_form"] : null), 'form_start');
            echo "

<fieldset>
    <legend>Nouveau profil</legend>

    ";
            // line 31
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["new_profile_form"]) ? $context["new_profile_form"] : null), "identity"), "username"), 'row');
            echo "
    
    <p>La mention <strong>(local)</strong> sera automatiquement ajoutée au nom.</p>
    
    ";
            // line 35
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["new_profile_form"]) ? $context["new_profile_form"] : null), "name"), 'row');
            echo "
    
    ";
            // line 37
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["new_profile_form"]) ? $context["new_profile_form"] : null), "identity"), 'rest');
            echo "

    ";
            // line 39
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["new_profile_form"]) ? $context["new_profile_form"] : null), "user"), 'rest');
            echo "

    ";
            // line 41
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["new_profile_form"]) ? $context["new_profile_form"] : null), "userinfo"), 'rest');
            echo "

    ";
            // line 43
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["new_profile_form"]) ? $context["new_profile_form"] : null), 'rest');
            echo "

</fieldset>

";
            // line 47
            echo             $this->env->getExtension('form')->renderer->renderBlock((isset($context["new_profile_form"]) ? $context["new_profile_form"] : null), 'form_end');
            echo "

";
        }
        // line 50
        echo "
";
    }

    public function getTemplateName()
    {
        return "LaplaceUserBundle:UserProfile:create-local.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  134 => 37,  129 => 35,  100 => 20,  178 => 65,  161 => 56,  131 => 39,  120 => 33,  104 => 22,  102 => 23,  23 => 3,  181 => 63,  153 => 51,  148 => 42,  124 => 31,  97 => 17,  90 => 12,  76 => 7,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 144,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 112,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 105,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 90,  285 => 89,  283 => 88,  278 => 86,  268 => 85,  264 => 84,  258 => 81,  252 => 80,  247 => 78,  241 => 77,  235 => 74,  229 => 73,  224 => 71,  220 => 70,  214 => 69,  208 => 68,  169 => 60,  143 => 45,  140 => 55,  132 => 51,  128 => 49,  119 => 30,  111 => 25,  107 => 25,  71 => 17,  177 => 65,  165 => 64,  160 => 61,  139 => 39,  135 => 47,  126 => 34,  114 => 26,  84 => 13,  70 => 6,  67 => 15,  61 => 13,  47 => 9,  28 => 3,  87 => 14,  55 => 15,  201 => 92,  196 => 90,  183 => 70,  171 => 56,  166 => 58,  163 => 70,  158 => 67,  156 => 47,  151 => 57,  142 => 59,  138 => 38,  136 => 41,  123 => 34,  121 => 46,  117 => 44,  115 => 43,  105 => 40,  91 => 31,  69 => 5,  66 => 15,  62 => 23,  49 => 19,  31 => 5,  29 => 3,  21 => 6,  25 => 4,  98 => 31,  93 => 28,  88 => 11,  78 => 11,  44 => 12,  32 => 4,  43 => 6,  40 => 8,  101 => 32,  94 => 18,  89 => 12,  85 => 10,  79 => 8,  75 => 23,  72 => 16,  68 => 14,  56 => 9,  50 => 10,  41 => 5,  27 => 4,  38 => 6,  24 => 7,  46 => 7,  35 => 5,  26 => 6,  22 => 2,  19 => 1,  209 => 82,  203 => 78,  199 => 67,  193 => 73,  189 => 71,  187 => 84,  182 => 66,  176 => 64,  173 => 62,  168 => 66,  164 => 52,  162 => 50,  154 => 54,  149 => 43,  147 => 58,  144 => 41,  141 => 51,  133 => 36,  130 => 35,  125 => 44,  122 => 31,  116 => 36,  112 => 42,  109 => 26,  106 => 33,  103 => 20,  99 => 19,  95 => 34,  92 => 14,  86 => 12,  82 => 9,  80 => 19,  73 => 19,  64 => 14,  60 => 13,  57 => 11,  54 => 10,  51 => 14,  48 => 8,  45 => 8,  42 => 7,  39 => 9,  36 => 5,  33 => 4,  30 => 7,);
    }
}
