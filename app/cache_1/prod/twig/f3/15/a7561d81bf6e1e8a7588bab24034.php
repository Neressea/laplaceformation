<?php

/* LaplaceTrainingBundle:Event:need-history.html.twig */
class __TwigTemplate_f315a7561d81bf6e1e8a7588bab24034 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "

";
        // line 3
        $this->env->loadTemplate("LaplaceTrainingBundle:Event:need-history.html.twig", "1533846542")->display(array_merge($context, array("page" => array(0 => "history", 1 => "needs"))));
    }

    public function getTemplateName()
    {
        return "LaplaceTrainingBundle:Event:need-history.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  234 => 74,  225 => 68,  237 => 74,  230 => 70,  221 => 64,  213 => 58,  207 => 54,  200 => 52,  110 => 21,  34 => 20,  335 => 118,  330 => 101,  325 => 97,  320 => 102,  318 => 101,  311 => 97,  308 => 96,  299 => 104,  297 => 95,  292 => 92,  289 => 91,  282 => 84,  275 => 86,  273 => 84,  259 => 107,  257 => 91,  249 => 78,  240 => 109,  238 => 77,  231 => 72,  222 => 69,  218 => 67,  211 => 65,  198 => 60,  194 => 59,  184 => 46,  179 => 44,  152 => 30,  96 => 16,  77 => 112,  63 => 17,  58 => 15,  53 => 13,  83 => 44,  227 => 85,  215 => 79,  210 => 77,  205 => 75,  226 => 84,  219 => 80,  195 => 68,  172 => 40,  202 => 62,  190 => 95,  146 => 69,  113 => 22,  108 => 13,  59 => 23,  52 => 19,  37 => 14,  267 => 80,  261 => 125,  254 => 90,  246 => 77,  244 => 118,  228 => 107,  212 => 97,  204 => 74,  197 => 88,  191 => 48,  175 => 44,  167 => 42,  159 => 35,  127 => 60,  118 => 24,  134 => 32,  129 => 41,  100 => 20,  178 => 65,  161 => 82,  131 => 29,  120 => 24,  104 => 24,  102 => 22,  23 => 3,  181 => 63,  153 => 51,  148 => 42,  124 => 31,  97 => 19,  90 => 13,  76 => 7,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 144,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 112,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 105,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 90,  285 => 85,  283 => 88,  278 => 86,  268 => 85,  264 => 79,  258 => 81,  252 => 79,  247 => 78,  241 => 78,  235 => 111,  229 => 73,  224 => 71,  220 => 81,  214 => 66,  208 => 68,  169 => 60,  143 => 35,  140 => 34,  132 => 51,  128 => 29,  119 => 30,  111 => 25,  107 => 25,  71 => 17,  177 => 59,  165 => 83,  160 => 40,  139 => 65,  135 => 35,  126 => 34,  114 => 26,  84 => 114,  70 => 6,  67 => 15,  61 => 13,  47 => 17,  28 => 3,  87 => 14,  55 => 14,  201 => 92,  196 => 69,  183 => 91,  171 => 43,  166 => 58,  163 => 51,  158 => 67,  156 => 38,  151 => 57,  142 => 59,  138 => 34,  136 => 41,  123 => 34,  121 => 46,  117 => 35,  115 => 43,  105 => 23,  91 => 48,  69 => 5,  66 => 27,  62 => 23,  49 => 19,  31 => 3,  29 => 19,  21 => 6,  25 => 5,  98 => 52,  93 => 17,  88 => 12,  78 => 7,  44 => 10,  32 => 4,  43 => 24,  40 => 8,  101 => 23,  94 => 118,  89 => 13,  85 => 11,  79 => 8,  75 => 53,  72 => 52,  68 => 14,  56 => 9,  50 => 10,  41 => 7,  27 => 18,  38 => 7,  24 => 17,  46 => 25,  35 => 5,  26 => 6,  22 => 2,  19 => 1,  209 => 76,  203 => 78,  199 => 89,  193 => 73,  189 => 65,  187 => 54,  182 => 61,  176 => 87,  173 => 62,  168 => 84,  164 => 41,  162 => 36,  154 => 54,  149 => 29,  147 => 36,  144 => 41,  141 => 51,  133 => 31,  130 => 30,  125 => 26,  122 => 25,  116 => 22,  112 => 29,  109 => 27,  106 => 20,  103 => 54,  99 => 21,  95 => 17,  92 => 14,  86 => 12,  82 => 9,  80 => 113,  73 => 19,  64 => 14,  60 => 29,  57 => 11,  54 => 27,  51 => 14,  48 => 11,  45 => 8,  42 => 7,  39 => 22,  36 => 5,  33 => 4,  30 => 7,);
    }
}


/* LaplaceTrainingBundle:Event:need-history.html.twig */
class __TwigTemplate_f315a7561d81bf6e1e8a7588bab24034_1533846542 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("LaplaceCommonBundle::user-page.html.twig");

        $this->blocks = array(
            'PageTitle' => array($this, 'block_PageTitle'),
            'ContentTitle' => array($this, 'block_ContentTitle'),
            'ContentBody' => array($this, 'block_ContentBody'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "LaplaceCommonBundle::user-page.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 6
    public function block_PageTitle($context, array $blocks = array())
    {
        $this->displayParentBlock("PageTitle", $context, $blocks);
        echo " - Historique des besoins (";
        echo twig_escape_filter($this->env, (isset($context["requested"]) ? $context["requested"] : null), "html", null, true);
        echo ")";
    }

    // line 8
    public function block_ContentTitle($context, array $blocks = array())
    {
        echo "Historique des besoins de l'année ";
        echo twig_escape_filter($this->env, (isset($context["requested"]) ? $context["requested"] : null), "html", null, true);
    }

    // line 12
    public function block_ContentBody($context, array $blocks = array())
    {
        // line 13
        echo "
<p>
    Cette page regroupe l'ensemble de tous les évènements liés à vos besoins
    de formation.
</p>

";
        // line 19
        $this->env->loadTemplate("::pagination.html.twig")->display(array_merge($context, array("route_name" => "laplace_training_need_history", "route_args" => array())));
        // line 21
        echo "
";
        // line 22
        $this->env->loadTemplate("LaplaceTrainingBundle:Event:table.html.twig")->display(array_merge($context, array("events" => (isset($context["events"]) ? $context["events"] : null))));
        // line 24
        echo "
";
        // line 25
        $this->env->loadTemplate("::pagination.html.twig")->display(array_merge($context, array("route_name" => "laplace_training_need_history", "route_args" => array())));
        // line 27
        echo "
";
    }

    public function getTemplateName()
    {
        return "LaplaceTrainingBundle:Event:need-history.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  234 => 74,  225 => 68,  237 => 74,  230 => 70,  221 => 64,  213 => 58,  207 => 54,  200 => 52,  110 => 21,  34 => 20,  335 => 118,  330 => 101,  325 => 97,  320 => 102,  318 => 101,  311 => 97,  308 => 96,  299 => 104,  297 => 95,  292 => 92,  289 => 91,  282 => 84,  275 => 86,  273 => 84,  259 => 107,  257 => 91,  249 => 78,  240 => 109,  238 => 77,  231 => 72,  222 => 69,  218 => 67,  211 => 65,  198 => 60,  194 => 59,  184 => 46,  179 => 44,  152 => 30,  96 => 16,  77 => 112,  63 => 17,  58 => 15,  53 => 13,  83 => 44,  227 => 85,  215 => 79,  210 => 77,  205 => 75,  226 => 84,  219 => 80,  195 => 68,  172 => 40,  202 => 62,  190 => 95,  146 => 69,  113 => 22,  108 => 13,  59 => 23,  52 => 19,  37 => 14,  267 => 80,  261 => 125,  254 => 90,  246 => 77,  244 => 118,  228 => 107,  212 => 97,  204 => 74,  197 => 88,  191 => 48,  175 => 44,  167 => 42,  159 => 35,  127 => 60,  118 => 24,  134 => 32,  129 => 41,  100 => 20,  178 => 65,  161 => 82,  131 => 29,  120 => 24,  104 => 24,  102 => 22,  23 => 3,  181 => 63,  153 => 51,  148 => 42,  124 => 31,  97 => 19,  90 => 13,  76 => 7,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 144,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 112,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 105,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 90,  285 => 85,  283 => 88,  278 => 86,  268 => 85,  264 => 79,  258 => 81,  252 => 79,  247 => 78,  241 => 78,  235 => 111,  229 => 73,  224 => 71,  220 => 81,  214 => 66,  208 => 68,  169 => 60,  143 => 35,  140 => 34,  132 => 51,  128 => 29,  119 => 30,  111 => 25,  107 => 25,  71 => 17,  177 => 59,  165 => 83,  160 => 40,  139 => 65,  135 => 35,  126 => 34,  114 => 26,  84 => 114,  70 => 6,  67 => 15,  61 => 13,  47 => 17,  28 => 3,  87 => 14,  55 => 14,  201 => 92,  196 => 69,  183 => 91,  171 => 43,  166 => 58,  163 => 51,  158 => 67,  156 => 38,  151 => 57,  142 => 59,  138 => 34,  136 => 41,  123 => 34,  121 => 46,  117 => 35,  115 => 43,  105 => 23,  91 => 48,  69 => 5,  66 => 27,  62 => 23,  49 => 19,  31 => 3,  29 => 19,  21 => 6,  25 => 5,  98 => 52,  93 => 17,  88 => 12,  78 => 7,  44 => 10,  32 => 4,  43 => 24,  40 => 8,  101 => 23,  94 => 118,  89 => 13,  85 => 11,  79 => 8,  75 => 53,  72 => 52,  68 => 14,  56 => 9,  50 => 10,  41 => 7,  27 => 18,  38 => 7,  24 => 17,  46 => 25,  35 => 5,  26 => 6,  22 => 2,  19 => 1,  209 => 76,  203 => 78,  199 => 89,  193 => 73,  189 => 65,  187 => 54,  182 => 61,  176 => 87,  173 => 62,  168 => 84,  164 => 41,  162 => 36,  154 => 54,  149 => 29,  147 => 36,  144 => 41,  141 => 51,  133 => 31,  130 => 30,  125 => 26,  122 => 25,  116 => 22,  112 => 29,  109 => 27,  106 => 20,  103 => 54,  99 => 21,  95 => 17,  92 => 14,  86 => 12,  82 => 9,  80 => 113,  73 => 19,  64 => 14,  60 => 29,  57 => 11,  54 => 27,  51 => 14,  48 => 11,  45 => 8,  42 => 7,  39 => 22,  36 => 5,  33 => 4,  30 => 7,);
    }
}
