<?php

/* LaplaceTrainingBundle:Need:view.html.twig */
class __TwigTemplate_ad75e41e0767e6d589044c6fec593211 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        $context["container"] = (((isset($context["admin"]) ? $context["admin"] : null)) ? ("LaplaceCommonBundle::admin-page.html.twig") : ("LaplaceCommonBundle::user-page.html.twig"));
        // line 6
        echo "

";
        // line 8
        $this->env->loadTemplate("LaplaceTrainingBundle:Need:view.html.twig", "1652906069")->display(array_merge($context, array("page" => array(0 => "need", 1 => "view"))));
    }

    public function getTemplateName()
    {
        return "LaplaceTrainingBundle:Need:view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  180 => 58,  174 => 54,  65 => 28,  234 => 74,  225 => 68,  237 => 74,  230 => 70,  221 => 64,  213 => 58,  207 => 54,  200 => 52,  110 => 25,  34 => 20,  335 => 118,  330 => 101,  325 => 97,  320 => 102,  318 => 101,  311 => 97,  308 => 96,  299 => 104,  297 => 95,  292 => 92,  289 => 91,  282 => 84,  275 => 86,  273 => 84,  259 => 107,  257 => 91,  249 => 78,  240 => 109,  238 => 77,  231 => 72,  222 => 69,  218 => 67,  211 => 65,  198 => 60,  194 => 59,  184 => 46,  179 => 44,  152 => 46,  96 => 16,  77 => 7,  63 => 17,  58 => 24,  53 => 21,  83 => 37,  227 => 85,  215 => 79,  210 => 77,  205 => 75,  226 => 84,  219 => 80,  195 => 68,  172 => 40,  202 => 62,  190 => 95,  146 => 69,  113 => 22,  108 => 49,  59 => 23,  52 => 19,  37 => 14,  267 => 80,  261 => 125,  254 => 90,  246 => 77,  244 => 118,  228 => 107,  212 => 97,  204 => 74,  197 => 88,  191 => 48,  175 => 44,  167 => 42,  159 => 35,  127 => 39,  118 => 24,  134 => 32,  129 => 41,  100 => 45,  178 => 65,  161 => 82,  131 => 40,  120 => 29,  104 => 24,  102 => 24,  23 => 3,  181 => 63,  153 => 51,  148 => 42,  124 => 56,  97 => 22,  90 => 17,  76 => 7,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 144,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 112,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 105,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 90,  285 => 85,  283 => 88,  278 => 86,  268 => 85,  264 => 79,  258 => 81,  252 => 79,  247 => 78,  241 => 78,  235 => 111,  229 => 73,  224 => 71,  220 => 81,  214 => 66,  208 => 68,  169 => 60,  143 => 35,  140 => 34,  132 => 51,  128 => 29,  119 => 34,  111 => 25,  107 => 28,  71 => 30,  177 => 59,  165 => 83,  160 => 40,  139 => 65,  135 => 42,  126 => 34,  114 => 51,  84 => 114,  70 => 5,  67 => 15,  61 => 13,  47 => 17,  28 => 3,  87 => 14,  55 => 23,  201 => 92,  196 => 69,  183 => 91,  171 => 43,  166 => 51,  163 => 51,  158 => 67,  156 => 38,  151 => 57,  142 => 59,  138 => 34,  136 => 41,  123 => 37,  121 => 46,  117 => 33,  115 => 32,  105 => 23,  91 => 48,  69 => 5,  66 => 27,  62 => 27,  49 => 19,  31 => 11,  29 => 19,  21 => 6,  25 => 8,  98 => 19,  93 => 42,  88 => 16,  78 => 7,  44 => 10,  32 => 4,  43 => 24,  40 => 8,  101 => 20,  94 => 118,  89 => 12,  85 => 10,  79 => 8,  75 => 53,  72 => 52,  68 => 29,  56 => 9,  50 => 10,  41 => 7,  27 => 18,  38 => 7,  24 => 7,  46 => 18,  35 => 5,  26 => 6,  22 => 2,  19 => 2,  209 => 76,  203 => 78,  199 => 89,  193 => 73,  189 => 65,  187 => 54,  182 => 61,  176 => 87,  173 => 62,  168 => 84,  164 => 41,  162 => 49,  154 => 47,  149 => 29,  147 => 44,  144 => 41,  141 => 43,  133 => 31,  130 => 30,  125 => 26,  122 => 25,  116 => 22,  112 => 30,  109 => 28,  106 => 48,  103 => 54,  99 => 21,  95 => 17,  92 => 17,  86 => 12,  82 => 9,  80 => 8,  73 => 19,  64 => 14,  60 => 25,  57 => 11,  54 => 27,  51 => 20,  48 => 11,  45 => 8,  42 => 17,  39 => 16,  36 => 5,  33 => 12,  30 => 7,);
    }
}


/* LaplaceTrainingBundle:Need:view.html.twig */
class __TwigTemplate_ad75e41e0767e6d589044c6fec593211_1652906069 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'PageTitle' => array($this, 'block_PageTitle'),
            'ContentTitle' => array($this, 'block_ContentTitle'),
            'ContentBody' => array($this, 'block_ContentBody'),
        );
    }

    protected function doGetParent(array $context)
    {
        return $this->env->resolveTemplate((isset($context["container"]) ? $context["container"] : null));
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 10
    public function block_PageTitle($context, array $blocks = array())
    {
        $this->displayParentBlock("PageTitle", $context, $blocks);
        echo " - ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["need"]) ? $context["need"] : null), "title"), "html", null, true);
    }

    // line 12
    public function block_ContentTitle($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["need"]) ? $context["need"] : null), "title"), "html", null, true);
    }

    // line 16
    public function block_ContentBody($context, array $blocks = array())
    {
        // line 17
        echo "
";
        // line 19
        $this->env->loadTemplate("LaplaceTrainingBundle::category-breadcrumb.html.twig")->display(array_merge($context, array("reference" => (isset($context["need"]) ? $context["need"] : null), "link" => $this->env->getExtension('routing')->getPath(((isset($context["laplace_training"]) ? $context["laplace_training"] : null) . "view_need"), array("id" => $this->getAttribute((isset($context["need"]) ? $context["need"] : null), "id"))))));
        // line 26
        echo "
<h5>Description du besoin</h5>

";
        // line 30
        if ((isset($context["admin"]) ? $context["admin"] : null)) {
            // line 31
            echo "    <p>
        Par <a href=\"";
            // line 32
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath(((isset($context["laplace_user"]) ? $context["laplace_user"] : null) . "view_profile"), array("username" => $this->getAttribute($this->getAttribute((isset($context["need"]) ? $context["need"] : null), "author"), "username"))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["need"]) ? $context["need"] : null), "author"), "fullName"), "html", null, true);
            echo "</a>,
        le ";
            // line 33
            echo twig_escape_filter($this->env, twig_localized_date_filter($this->env, $this->getAttribute((isset($context["need"]) ? $context["need"] : null), "issueDate"), "long", "short"), "html", null, true);
            echo ".
    </p>
";
        }
        // line 36
        echo "
";
        // line 38
        if (twig_test_empty($this->getAttribute((isset($context["need"]) ? $context["need"] : null), "description"))) {
            // line 39
            echo "    <p>
        <em>Aucune description</em>.
    </p>
";
        } else {
            // line 43
            echo "    <pre>";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["need"]) ? $context["need"] : null), "description"), "html", null, true);
            echo "</pre>
";
        }
        // line 45
        echo "
";
        // line 47
        if ((isset($context["admin"]) ? $context["admin"] : null)) {
            // line 48
            echo "    <p class=\"text-right\">
        <i class=\"icon-edit\"></i>
        <a href=\"";
            // line 50
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath(((isset($context["laplace_training"]) ? $context["laplace_training"] : null) . "edit_need"), array("id" => $this->getAttribute((isset($context["need"]) ? $context["need"] : null), "id"))), "html", null, true);
            echo "\">Modifier</a>

        <br />

        <i class=\"icon-trash\"></i>
        <a href=\"";
            // line 55
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath(((isset($context["laplace_training"]) ? $context["laplace_training"] : null) . "delete_need"), array("id" => $this->getAttribute((isset($context["need"]) ? $context["need"] : null), "id"))), "html", null, true);
            echo "\">Supprimer</a>
    </p>
";
        }
        // line 58
        echo "


<h5>Inscriptions</h5>
<p class=\"clearfix\">
";
        // line 63
        if ((array_key_exists("subscriptions", $context) && (!twig_test_empty((isset($context["subscriptions"]) ? $context["subscriptions"] : null))))) {
            // line 64
            echo "    <ul>
    ";
            // line 65
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["subscriptions"]) ? $context["subscriptions"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["subinfo"]) {
                // line 66
                echo "        <li>
            <span class=\"label label-info\">";
                // line 68
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["subinfo"]) ? $context["subinfo"] : null), "subscription"), "type"), "name"), "html", null, true);
                // line 69
                echo "</span>

            <a href=\"";
                // line 71
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath(((isset($context["laplace_user"]) ? $context["laplace_user"] : null) . "view_profile"), array("username" => $this->getAttribute($this->getAttribute((isset($context["subinfo"]) ? $context["subinfo"] : null), "user"), "username"))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["subinfo"]) ? $context["subinfo"] : null), "user"), "fullName"), "html", null, true);
                echo "</a>
            (";
                // line 72
                echo twig_escape_filter($this->env, twig_localized_date_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["subinfo"]) ? $context["subinfo"] : null), "subscription"), "subscriptionDate"), "long", "none"), "html", null, true);
                echo ")
        </li>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subinfo'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 75
            echo "    </ul>
";
        } else {
            // line 77
            echo "    ";
            $context["count"] = $this->getAttribute($this->getAttribute((isset($context["need"]) ? $context["need"] : null), "subscriptions"), "count", array(), "method");
            // line 78
            echo "    Ce besoin a été ajouté par
    <span class=\"badge";
            // line 79
            if (((isset($context["count"]) ? $context["count"] : null) > 0)) {
                echo " badge-info";
            }
            echo "\">";
            echo twig_escape_filter($this->env, (isset($context["count"]) ? $context["count"] : null), "html", null, true);
            echo "</span>
    personne(s).
";
        }
        // line 82
        echo "</p>

";
        // line 85
        echo "
<hr />

";
        // line 89
        echo "
";
        // line 90
        if ((!(isset($context["admin"]) ? $context["admin"] : null))) {
            // line 91
            echo "
<h3>Gestion</h3>

    ";
            // line 94
            if ((array_key_exists("subscription", $context) && (!(null === (isset($context["subscription"]) ? $context["subscription"] : null))))) {
                // line 95
                echo "
<p>
    Vous avez ajouté ce besoin à votre liste personnelle
    le <strong>";
                // line 98
                echo twig_escape_filter($this->env, twig_localized_date_filter($this->env, $this->getAttribute((isset($context["subscription"]) ? $context["subscription"] : null), "subscriptionDate"), "long", "short"), "html", null, true);
                echo "</strong>
    avec le type
    <span class=\"label label-info\"
          title=\"";
                // line 101
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["subscription"]) ? $context["subscription"] : null), "type"), "nameWithDescription"), "html", null, true);
                echo "\">";
                // line 102
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["subscription"]) ? $context["subscription"] : null), "type"), "name"), "html", null, true);
                // line 103
                echo "</span>.
</p>

    ";
            }
            // line 107
            echo "
";
            // line 109
            echo "
    ";
            // line 110
            if (array_key_exists("edit_form", $context)) {
                // line 111
                echo "
";
                // line 112
                echo                 $this->env->getExtension('form')->renderer->renderBlock((isset($context["edit_form"]) ? $context["edit_form"] : null), 'form_start');
                echo "

<fieldset>
    <legend>Modifier</legend>

    <p>
        Ce formulaire vous permet de changer le type de besoin.
    </p>


    ";
                // line 122
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["edit_form"]) ? $context["edit_form"] : null), 'rest');
                echo "

</fieldset>

";
                // line 126
                echo                 $this->env->getExtension('form')->renderer->renderBlock((isset($context["edit_form"]) ? $context["edit_form"] : null), 'form_end');
                echo "

    ";
            }
            // line 129
            echo "


";
            // line 133
            echo "
    ";
            // line 134
            if (array_key_exists("satisfied_form", $context)) {
                // line 135
                echo "
";
                // line 136
                echo                 $this->env->getExtension('form')->renderer->renderBlock((isset($context["satisfied_form"]) ? $context["satisfied_form"] : null), 'form_start');
                echo "

<fieldset>
    <legend>Indiquer que le besoin est satisfait</legend>

    <p>
        Ce formulaire vous permet d'indiquer que ce besoin est pourvu.
        <strong>Attention !</strong> Il ne sera alors plus possible de
        modifier cette inscription.
        Les besoins pourvus restent inscrits dans votre historique.
    </p>


    ";
                // line 149
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["satisfied_form"]) ? $context["satisfied_form"] : null), 'rest');
                echo "

</fieldset>

";
                // line 153
                echo                 $this->env->getExtension('form')->renderer->renderBlock((isset($context["satisfied_form"]) ? $context["satisfied_form"] : null), 'form_end');
                echo "

    ";
            }
            // line 156
            echo "


";
            // line 160
            echo "
    ";
            // line 161
            if (array_key_exists("unsubscribe_form", $context)) {
                // line 162
                echo "
";
                // line 163
                echo                 $this->env->getExtension('form')->renderer->renderBlock((isset($context["unsubscribe_form"]) ? $context["unsubscribe_form"] : null), 'form_start');
                echo "

<fieldset>
    <legend>Retirer ce besoin</legend>

    <p>
        Si vous avez par erreur ajouté ce besoin à votre liste, vous pouvez
        le retirer en utilisant ce formulaire.
        Le besoin disparaîtra également de votre historique.
    </p>

    ";
                // line 174
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["unsubscribe_form"]) ? $context["unsubscribe_form"] : null), "unsubscribe"), 'row');
                echo "

    ";
                // line 176
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["unsubscribe_form"]) ? $context["unsubscribe_form"] : null), "do"), 'row', array("attr" => array("class" => "btn-danger")));
                echo "

    ";
                // line 178
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["unsubscribe_form"]) ? $context["unsubscribe_form"] : null), 'rest');
                echo "

</fieldset>

";
                // line 182
                echo                 $this->env->getExtension('form')->renderer->renderBlock((isset($context["unsubscribe_form"]) ? $context["unsubscribe_form"] : null), 'form_end');
                echo "

    ";
            }
            // line 185
            echo "


";
            // line 189
            echo "
    ";
            // line 190
            if (array_key_exists("subscribe_form", $context)) {
                // line 191
                echo "
";
                // line 192
                echo                 $this->env->getExtension('form')->renderer->renderBlock((isset($context["subscribe_form"]) ? $context["subscribe_form"] : null), 'form_start');
                echo "
<fieldset>
    <legend>Ajouter ce besoin</legend>

";
                // line 196
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["subscribe_form"]) ? $context["subscribe_form"] : null), 'rest');
                echo "

</fieldset>
";
                // line 199
                echo                 $this->env->getExtension('form')->renderer->renderBlock((isset($context["subscribe_form"]) ? $context["subscribe_form"] : null), 'form_end');
                echo "

    ";
            }
            // line 202
            echo "
";
            // line 204
            echo "
<hr />

";
            // line 208
            echo "
";
        }
        // line 210
        echo "

<h3>Discussions</h3>

";
        // line 214
        if ((!(isset($context["admin"]) ? $context["admin"] : null))) {
            // line 215
            echo "<p>
    Cet espace vous permet d'échanger de manière privée avec le correspondant
    formation. Pour poser une question ou ajouter une remarque concernant ce
    besoin, ouvrez une nouvelle discussion à l'aide du formulaire ci-dessous.
    Les messages publiés ne seront visibles que par vous,
    le correspondant formation, et les autres personnes autorisées
    individuellement à participer à la discussion.
</p>
";
        }
        // line 224
        echo "
";
        // line 225
        $this->env->loadTemplate("LaplaceTrainingBundle:Thread:table.html.twig")->display($context);
        // line 226
        echo "
";
        // line 227
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["thread_form"]) ? $context["thread_form"] : null), 'form_start');
        echo "

<fieldset>
    <legend>Ouvrir une nouvelle discussion</legend>

    ";
        // line 232
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["thread_form"]) ? $context["thread_form"] : null), "thread"), 'rest');
        echo "

    ";
        // line 234
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["thread_form"]) ? $context["thread_form"] : null), "message"), 'rest');
        echo "

    ";
        // line 236
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["thread_form"]) ? $context["thread_form"] : null), 'rest');
        echo "

</fieldset>

";
        // line 240
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["thread_form"]) ? $context["thread_form"] : null), 'form_end');
        echo "

";
        // line 243
        echo "
";
    }

    public function getTemplateName()
    {
        return "LaplaceTrainingBundle:Need:view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  487 => 243,  482 => 240,  475 => 236,  470 => 234,  465 => 232,  454 => 226,  452 => 225,  449 => 224,  438 => 215,  436 => 214,  426 => 208,  421 => 204,  418 => 202,  412 => 199,  406 => 196,  399 => 192,  396 => 191,  394 => 190,  391 => 189,  386 => 185,  380 => 182,  373 => 178,  363 => 174,  349 => 163,  346 => 162,  344 => 161,  336 => 156,  323 => 149,  307 => 136,  304 => 135,  302 => 134,  288 => 126,  281 => 122,  265 => 111,  263 => 110,  260 => 109,  251 => 103,  233 => 94,  223 => 89,  185 => 72,  170 => 66,  180 => 58,  174 => 54,  65 => 28,  234 => 74,  225 => 68,  237 => 74,  230 => 70,  221 => 64,  213 => 58,  207 => 54,  200 => 52,  110 => 25,  34 => 20,  335 => 118,  330 => 153,  325 => 97,  320 => 102,  318 => 101,  311 => 97,  308 => 96,  299 => 133,  297 => 95,  292 => 92,  289 => 91,  282 => 84,  275 => 86,  273 => 84,  259 => 107,  257 => 107,  249 => 102,  240 => 98,  238 => 77,  231 => 72,  222 => 69,  218 => 85,  211 => 65,  198 => 77,  194 => 75,  184 => 46,  179 => 71,  152 => 46,  96 => 16,  77 => 7,  63 => 17,  58 => 24,  53 => 21,  83 => 37,  227 => 85,  215 => 79,  210 => 77,  205 => 75,  226 => 90,  219 => 80,  195 => 68,  172 => 40,  202 => 62,  190 => 95,  146 => 69,  113 => 22,  108 => 33,  59 => 23,  52 => 19,  37 => 14,  267 => 80,  261 => 125,  254 => 90,  246 => 101,  244 => 118,  228 => 91,  212 => 97,  204 => 79,  197 => 88,  191 => 48,  175 => 69,  167 => 42,  159 => 35,  127 => 39,  118 => 24,  134 => 47,  129 => 41,  100 => 45,  178 => 65,  161 => 63,  131 => 45,  120 => 29,  104 => 24,  102 => 32,  23 => 3,  181 => 63,  153 => 51,  148 => 55,  124 => 56,  97 => 30,  90 => 19,  76 => 7,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 227,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 210,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 176,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 160,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 129,  285 => 85,  283 => 88,  278 => 86,  268 => 112,  264 => 79,  258 => 81,  252 => 79,  247 => 78,  241 => 78,  235 => 95,  229 => 73,  224 => 71,  220 => 81,  214 => 82,  208 => 68,  169 => 60,  143 => 35,  140 => 50,  132 => 51,  128 => 29,  119 => 39,  111 => 25,  107 => 28,  71 => 30,  177 => 59,  165 => 83,  160 => 40,  139 => 65,  135 => 42,  126 => 34,  114 => 36,  84 => 16,  70 => 10,  67 => 15,  61 => 13,  47 => 17,  28 => 3,  87 => 17,  55 => 23,  201 => 78,  196 => 69,  183 => 91,  171 => 43,  166 => 65,  163 => 64,  158 => 67,  156 => 38,  151 => 57,  142 => 59,  138 => 34,  136 => 48,  123 => 37,  121 => 46,  117 => 38,  115 => 32,  105 => 23,  91 => 48,  69 => 5,  66 => 27,  62 => 27,  49 => 19,  31 => 11,  29 => 19,  21 => 6,  25 => 8,  98 => 19,  93 => 42,  88 => 16,  78 => 12,  44 => 10,  32 => 4,  43 => 24,  40 => 8,  101 => 20,  94 => 118,  89 => 12,  85 => 10,  79 => 8,  75 => 53,  72 => 52,  68 => 29,  56 => 9,  50 => 10,  41 => 7,  27 => 18,  38 => 7,  24 => 7,  46 => 18,  35 => 5,  26 => 6,  22 => 2,  19 => 2,  209 => 76,  203 => 78,  199 => 89,  193 => 73,  189 => 65,  187 => 54,  182 => 61,  176 => 87,  173 => 68,  168 => 84,  164 => 41,  162 => 49,  154 => 58,  149 => 29,  147 => 44,  144 => 41,  141 => 43,  133 => 31,  130 => 30,  125 => 43,  122 => 25,  116 => 22,  112 => 30,  109 => 28,  106 => 48,  103 => 54,  99 => 31,  95 => 17,  92 => 26,  86 => 12,  82 => 9,  80 => 8,  73 => 19,  64 => 14,  60 => 25,  57 => 11,  54 => 27,  51 => 20,  48 => 11,  45 => 8,  42 => 17,  39 => 16,  36 => 5,  33 => 12,  30 => 7,);
    }
}
