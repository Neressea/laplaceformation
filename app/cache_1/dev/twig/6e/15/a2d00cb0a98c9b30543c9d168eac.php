<?php

/* LaplaceTrainingBundle:Request:view-all.html.twig */
class __TwigTemplate_6e15a2d00cb0a98c9b30543c9d168eac extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        $context["container"] = (((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin"))) ? ("LaplaceCommonBundle::admin-page.html.twig") : ("LaplaceCommonBundle::user-page.html.twig"));
        // line 6
        echo "

";
        // line 8
        $this->env->loadTemplate("LaplaceTrainingBundle:Request:view-all.html.twig", "1453322698")->display(array_merge($context, array("page" => array(0 => "request", 1 => "all"))));
    }

    public function getTemplateName()
    {
        return "LaplaceTrainingBundle:Request:view-all.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  828 => 400,  823 => 397,  816 => 393,  811 => 391,  806 => 389,  798 => 384,  795 => 383,  793 => 382,  790 => 381,  779 => 372,  777 => 371,  772 => 368,  767 => 364,  764 => 362,  760 => 360,  752 => 354,  746 => 350,  729 => 347,  722 => 346,  705 => 345,  699 => 341,  683 => 338,  681 => 337,  678 => 336,  673 => 335,  656 => 334,  642 => 322,  640 => 321,  637 => 320,  634 => 318,  630 => 316,  624 => 313,  618 => 310,  589 => 284,  586 => 283,  584 => 282,  581 => 281,  576 => 277,  570 => 274,  563 => 270,  550 => 260,  547 => 259,  545 => 258,  542 => 257,  537 => 253,  531 => 250,  524 => 246,  519 => 244,  514 => 242,  500 => 231,  497 => 230,  495 => 229,  492 => 228,  481 => 221,  458 => 206,  456 => 205,  450 => 202,  446 => 200,  439 => 195,  434 => 192,  428 => 188,  422 => 184,  416 => 180,  414 => 179,  400 => 170,  395 => 167,  390 => 165,  378 => 159,  375 => 158,  358 => 147,  353 => 144,  348 => 140,  332 => 134,  329 => 133,  316 => 128,  310 => 127,  301 => 122,  271 => 106,  250 => 95,  242 => 92,  216 => 82,  137 => 48,  81 => 13,  188 => 63,  334 => 148,  327 => 143,  315 => 137,  306 => 125,  284 => 122,  279 => 111,  274 => 119,  272 => 118,  262 => 114,  239 => 91,  206 => 85,  186 => 62,  280 => 103,  255 => 97,  245 => 88,  150 => 48,  487 => 224,  482 => 240,  475 => 236,  470 => 234,  465 => 232,  454 => 226,  452 => 225,  449 => 224,  438 => 215,  436 => 214,  426 => 208,  421 => 204,  418 => 202,  412 => 199,  406 => 196,  399 => 192,  396 => 191,  394 => 190,  391 => 189,  386 => 185,  380 => 160,  373 => 178,  363 => 149,  349 => 163,  346 => 162,  344 => 137,  336 => 156,  323 => 149,  307 => 136,  304 => 124,  302 => 129,  288 => 123,  281 => 122,  265 => 103,  263 => 110,  260 => 99,  251 => 89,  233 => 94,  223 => 89,  185 => 70,  170 => 66,  180 => 67,  174 => 65,  65 => 27,  234 => 89,  225 => 76,  237 => 90,  230 => 100,  221 => 84,  213 => 81,  207 => 54,  200 => 76,  110 => 32,  34 => 20,  335 => 135,  330 => 153,  325 => 131,  320 => 139,  318 => 101,  311 => 97,  308 => 96,  299 => 133,  297 => 121,  292 => 119,  289 => 91,  282 => 84,  275 => 100,  273 => 84,  259 => 107,  257 => 107,  249 => 102,  240 => 98,  238 => 77,  231 => 72,  222 => 75,  218 => 85,  211 => 80,  198 => 75,  194 => 75,  184 => 59,  179 => 71,  152 => 46,  77 => 7,  63 => 17,  58 => 23,  53 => 21,  227 => 99,  215 => 79,  210 => 77,  205 => 68,  226 => 86,  219 => 94,  195 => 74,  172 => 58,  202 => 62,  190 => 72,  146 => 50,  113 => 33,  59 => 23,  267 => 104,  261 => 125,  254 => 111,  244 => 106,  228 => 91,  212 => 70,  204 => 75,  197 => 88,  191 => 48,  175 => 69,  167 => 53,  159 => 49,  127 => 39,  118 => 27,  134 => 41,  129 => 50,  100 => 21,  178 => 58,  161 => 48,  104 => 20,  102 => 30,  23 => 3,  181 => 68,  153 => 53,  148 => 45,  124 => 36,  97 => 20,  90 => 15,  76 => 7,  480 => 162,  474 => 217,  469 => 158,  461 => 207,  457 => 227,  453 => 204,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 210,  427 => 143,  423 => 142,  413 => 134,  409 => 176,  407 => 131,  402 => 171,  398 => 129,  393 => 166,  387 => 122,  384 => 162,  381 => 120,  379 => 119,  374 => 116,  368 => 176,  365 => 150,  362 => 110,  360 => 148,  355 => 106,  341 => 160,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 120,  285 => 114,  283 => 88,  278 => 86,  268 => 117,  264 => 79,  258 => 113,  252 => 96,  247 => 94,  241 => 86,  235 => 95,  229 => 87,  224 => 85,  220 => 81,  214 => 74,  208 => 79,  169 => 60,  143 => 35,  140 => 49,  132 => 51,  128 => 29,  119 => 46,  107 => 28,  71 => 10,  177 => 60,  165 => 54,  160 => 62,  135 => 47,  126 => 41,  114 => 36,  84 => 18,  70 => 10,  67 => 15,  61 => 13,  28 => 5,  87 => 19,  201 => 78,  196 => 65,  183 => 91,  171 => 55,  166 => 60,  163 => 58,  158 => 67,  156 => 54,  151 => 52,  142 => 53,  138 => 34,  136 => 37,  121 => 37,  117 => 33,  105 => 30,  91 => 39,  62 => 25,  49 => 19,  31 => 8,  21 => 6,  25 => 8,  93 => 18,  88 => 16,  78 => 12,  44 => 10,  94 => 20,  89 => 12,  85 => 10,  75 => 53,  68 => 29,  56 => 22,  27 => 18,  38 => 15,  24 => 7,  46 => 18,  26 => 6,  19 => 2,  79 => 8,  72 => 52,  69 => 5,  47 => 17,  40 => 11,  37 => 11,  22 => 2,  246 => 107,  157 => 48,  145 => 39,  139 => 65,  131 => 40,  123 => 37,  120 => 37,  115 => 34,  111 => 25,  108 => 31,  101 => 20,  98 => 19,  96 => 16,  83 => 15,  74 => 14,  66 => 27,  55 => 17,  52 => 21,  50 => 20,  43 => 24,  41 => 16,  35 => 9,  32 => 11,  29 => 6,  209 => 87,  203 => 77,  199 => 71,  193 => 68,  189 => 66,  187 => 71,  182 => 60,  176 => 87,  173 => 54,  168 => 61,  164 => 41,  162 => 49,  154 => 47,  149 => 58,  147 => 44,  144 => 42,  141 => 43,  133 => 43,  130 => 43,  125 => 31,  122 => 35,  116 => 26,  112 => 31,  109 => 30,  106 => 26,  103 => 23,  99 => 17,  95 => 19,  92 => 17,  86 => 14,  82 => 9,  80 => 8,  73 => 30,  64 => 14,  60 => 20,  57 => 11,  54 => 27,  51 => 20,  48 => 11,  45 => 17,  42 => 13,  39 => 10,  36 => 5,  33 => 7,  30 => 10,);
    }
}


/* LaplaceTrainingBundle:Request:view-all.html.twig */
class __TwigTemplate_6e15a2d00cb0a98c9b30543c9d168eac_1453322698 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'PageTitle' => array($this, 'block_PageTitle'),
            'ContentTitle' => array($this, 'block_ContentTitle'),
            'Sidebar' => array($this, 'block_Sidebar'),
            'ContentBody' => array($this, 'block_ContentBody'),
        );
    }

    protected function doGetParent(array $context)
    {
        return $this->env->resolveTemplate((isset($context["container"]) ? $context["container"] : $this->getContext($context, "container")));
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 10
    public function block_PageTitle($context, array $blocks = array())
    {
        $this->displayParentBlock("PageTitle", $context, $blocks);
        echo " - Toutes les demandes";
    }

    // line 14
    public function block_ContentTitle($context, array $blocks = array())
    {
        echo "Toutes les demandes";
    }

    // line 18
    public function block_Sidebar($context, array $blocks = array())
    {
        // line 19
        echo "
";
        // line 20
        $this->displayParentBlock("Sidebar", $context, $blocks);
        echo "

<li class=\"divider\"></li>

<li class=\"nav-header\">Domaines de connaissances</li>
";
        // line 25
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["domains"]) ? $context["domains"] : $this->getContext($context, "domains")));
        foreach ($context['_seq'] as $context["_key"] => $context["domain"]) {
            // line 26
            echo "<li>
    <a href=\"#dom_";
            // line 27
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["domain"]) ? $context["domain"] : $this->getContext($context, "domain")), "id"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["domain"]) ? $context["domain"] : $this->getContext($context, "domain")), "name"), "html", null, true);
            echo "</a>
</li>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['domain'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 30
        echo "
";
    }

    // line 35
    public function block_ContentBody($context, array $blocks = array())
    {
        // line 36
        echo "
<p>
    Cliquez sur la formation de votre choix pour <strong>voir sa description
    complète</strong> ou pour <strong>vous y inscrire</strong>.
    Si vous souhaitez ajouter une formation qui ne figure pas dans cette liste,
    vous pouvez en <a href=\"";
        // line 41
        echo $this->env->getExtension('routing')->getPath(((isset($context["laplace_training"]) ? $context["laplace_training"] : $this->getContext($context, "laplace_training")) . "create_request"));
        echo "\">créer une nouvelle</a>.
</p>

";
        // line 44
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["domains"]) ? $context["domains"] : $this->getContext($context, "domains")));
        foreach ($context['_seq'] as $context["_key"] => $context["domain"]) {
            // line 45
            echo "<h3 id=\"dom_";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["domain"]) ? $context["domain"] : $this->getContext($context, "domain")), "id"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["domain"]) ? $context["domain"] : $this->getContext($context, "domain")), "name"), "html", null, true);
            echo "</h3>

<table class=\"table table-striped table-bordered table-condensed cat-table\">

    ";
            // line 49
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["domain"]) ? $context["domain"] : $this->getContext($context, "domain")), "categories"));
            foreach ($context['_seq'] as $context["_key"] => $context["cat"]) {
                // line 50
                echo "    <tr>
        <th>";
                // line 51
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["cat"]) ? $context["cat"] : $this->getContext($context, "cat")), "name"), "html", null, true);
                echo "</th>
    </tr>

    ";
                // line 54
                if (($this->getAttribute((isset($context["groups"]) ? $context["groups"] : null), $this->getAttribute((isset($context["cat"]) ? $context["cat"] : $this->getContext($context, "cat")), "id"), array(), "array", true, true) && (!twig_test_empty($this->getAttribute((isset($context["groups"]) ? $context["groups"] : $this->getContext($context, "groups")), $this->getAttribute((isset($context["cat"]) ? $context["cat"] : $this->getContext($context, "cat")), "id"), array(), "array"))))) {
                    // line 55
                    echo "    ";
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["groups"]) ? $context["groups"] : $this->getContext($context, "groups")), $this->getAttribute((isset($context["cat"]) ? $context["cat"] : $this->getContext($context, "cat")), "id"), array(), "array"));
                    foreach ($context['_seq'] as $context["_key"] => $context["requestinfo"]) {
                        // line 56
                        echo "    <tr>
        <td>
        ";
                        // line 58
                        if ((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin"))) {
                            // line 59
                            echo "            <i class=\"icon-eye-close";
                            echo (((!$this->getAttribute($this->getAttribute((isset($context["requestinfo"]) ? $context["requestinfo"] : $this->getContext($context, "requestinfo")), "request"), "validated"))) ? ("") : (" icon-white"));
                            echo "\"></i>
        ";
                        }
                        // line 61
                        echo "            <i class=\"icon-lock";
                        echo (($this->getAttribute($this->getAttribute((isset($context["requestinfo"]) ? $context["requestinfo"] : $this->getContext($context, "requestinfo")), "request"), "close")) ? ("") : (" icon-white"));
                        echo "\"></i>
            <span class=\"badge";
                        // line 62
                        if (($this->getAttribute((isset($context["requestinfo"]) ? $context["requestinfo"] : $this->getContext($context, "requestinfo")), "subscriptionCount") > 0)) {
                            echo " badge-info";
                        }
                        echo "\"
                  title=\"Nombre de personnes intéressées\">";
                        // line 63
                        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["requestinfo"]) ? $context["requestinfo"] : $this->getContext($context, "requestinfo")), "subscriptionCount"), "html", null, true);
                        echo "</span>
            <a href=\"";
                        // line 64
                        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath(((isset($context["laplace_training"]) ? $context["laplace_training"] : $this->getContext($context, "laplace_training")) . "view_request"), array("id" => $this->getAttribute($this->getAttribute((isset($context["requestinfo"]) ? $context["requestinfo"] : $this->getContext($context, "requestinfo")), "request"), "id"))), "html", null, true);
                        echo "\">";
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["requestinfo"]) ? $context["requestinfo"] : $this->getContext($context, "requestinfo")), "request"), "title"), "html", null, true);
                        echo "</a>
        </td>
    </tr>
    ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['requestinfo'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 68
                    echo "    ";
                } else {
                    // line 69
                    echo "    <tr>
        <td><em>Vide</em></td>
    </tr>
    ";
                }
                // line 73
                echo "
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['cat'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 75
            echo "
</table>

<hr />
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['domain'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 80
        echo "
";
        // line 81
        if ((!twig_test_empty($this->getAttribute((isset($context["groups"]) ? $context["groups"] : $this->getContext($context, "groups")), null, array(), "array")))) {
            // line 82
            echo "<h3 id=\"dom_other\">Autre</h3>

<table class=\"table table-striped table-bordered table-condensed cat-table\">

    <tr>
        <th>Demandes non classées</th>
    </tr>

    ";
            // line 90
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["groups"]) ? $context["groups"] : $this->getContext($context, "groups")), null, array(), "array"));
            foreach ($context['_seq'] as $context["_key"] => $context["requestinfo"]) {
                // line 91
                echo "    <tr>
        <td>
        ";
                // line 93
                if ((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin"))) {
                    // line 94
                    echo "            <i class=\"icon-eye-close";
                    echo (((!$this->getAttribute($this->getAttribute((isset($context["requestinfo"]) ? $context["requestinfo"] : $this->getContext($context, "requestinfo")), "request"), "validated"))) ? ("") : (" icon-white"));
                    echo "\"></i>
        ";
                }
                // line 96
                echo "            <i class=\"icon-lock";
                echo (($this->getAttribute($this->getAttribute((isset($context["requestinfo"]) ? $context["requestinfo"] : $this->getContext($context, "requestinfo")), "request"), "close")) ? ("") : (" icon-white"));
                echo "\"></i>
            <span class=\"badge";
                // line 97
                if (($this->getAttribute((isset($context["requestinfo"]) ? $context["requestinfo"] : $this->getContext($context, "requestinfo")), "subscriptionCount") > 0)) {
                    echo " badge-info";
                }
                echo "\"
                  title=\"Nombre de personnes intéressées\">";
                // line 98
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["requestinfo"]) ? $context["requestinfo"] : $this->getContext($context, "requestinfo")), "subscriptionCount"), "html", null, true);
                echo "</span>
            <a href=\"";
                // line 99
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath(((isset($context["laplace_training"]) ? $context["laplace_training"] : $this->getContext($context, "laplace_training")) . "view_request"), array("id" => $this->getAttribute($this->getAttribute((isset($context["requestinfo"]) ? $context["requestinfo"] : $this->getContext($context, "requestinfo")), "request"), "id"))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["requestinfo"]) ? $context["requestinfo"] : $this->getContext($context, "requestinfo")), "request"), "title"), "html", null, true);
                echo "</a>
        </td>
    </tr>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['requestinfo'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 103
            echo "
</table>

<hr />

";
        }
        // line 109
        echo "
<p class=\"text-right\">
    <i class=\"icon-plus\"></i>
    <a href=\"";
        // line 112
        echo $this->env->getExtension('routing')->getPath(((isset($context["laplace_training"]) ? $context["laplace_training"] : $this->getContext($context, "laplace_training")) . "create_request"));
        echo "\">Ajouter une demande qui ne figure pas dans cette liste</a>
</p>

";
    }

    public function getTemplateName()
    {
        return "LaplaceTrainingBundle:Request:view-all.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  236 => 80,  155 => 50,  828 => 400,  823 => 397,  816 => 393,  811 => 391,  806 => 389,  798 => 384,  795 => 383,  793 => 382,  790 => 381,  779 => 372,  777 => 371,  772 => 368,  767 => 364,  764 => 362,  760 => 360,  752 => 354,  746 => 350,  729 => 347,  722 => 346,  705 => 345,  699 => 341,  683 => 338,  681 => 337,  678 => 336,  673 => 335,  656 => 334,  642 => 322,  640 => 321,  637 => 320,  634 => 318,  630 => 316,  624 => 313,  618 => 310,  589 => 284,  586 => 283,  584 => 282,  581 => 281,  576 => 277,  570 => 274,  563 => 270,  550 => 260,  547 => 259,  545 => 258,  542 => 257,  537 => 253,  531 => 250,  524 => 246,  519 => 244,  514 => 242,  500 => 231,  497 => 230,  495 => 229,  492 => 228,  481 => 221,  458 => 206,  456 => 205,  450 => 202,  446 => 200,  439 => 195,  434 => 192,  428 => 188,  422 => 184,  416 => 180,  414 => 179,  400 => 170,  395 => 167,  390 => 165,  378 => 159,  375 => 158,  358 => 147,  353 => 144,  348 => 140,  332 => 134,  329 => 133,  316 => 128,  310 => 127,  301 => 122,  271 => 106,  250 => 95,  242 => 92,  216 => 82,  137 => 44,  81 => 13,  188 => 62,  334 => 148,  327 => 143,  315 => 137,  306 => 125,  284 => 122,  279 => 111,  274 => 119,  272 => 97,  262 => 114,  239 => 81,  206 => 85,  186 => 62,  280 => 103,  255 => 91,  245 => 88,  150 => 48,  487 => 224,  482 => 240,  475 => 236,  470 => 234,  465 => 232,  454 => 226,  452 => 225,  449 => 224,  438 => 215,  436 => 214,  426 => 208,  421 => 204,  418 => 202,  412 => 199,  406 => 196,  399 => 192,  396 => 191,  394 => 190,  391 => 189,  386 => 185,  380 => 160,  373 => 178,  363 => 149,  349 => 163,  346 => 162,  344 => 137,  336 => 156,  323 => 149,  307 => 112,  304 => 124,  302 => 109,  288 => 123,  281 => 122,  265 => 103,  263 => 110,  260 => 99,  251 => 90,  233 => 94,  223 => 89,  185 => 70,  170 => 66,  180 => 67,  174 => 65,  65 => 27,  234 => 89,  225 => 76,  237 => 90,  230 => 100,  221 => 84,  213 => 69,  207 => 54,  200 => 76,  110 => 32,  34 => 20,  335 => 135,  330 => 153,  325 => 131,  320 => 139,  318 => 101,  311 => 97,  308 => 96,  299 => 133,  297 => 121,  292 => 119,  289 => 91,  282 => 99,  275 => 100,  273 => 84,  259 => 93,  257 => 107,  249 => 102,  240 => 98,  238 => 77,  231 => 72,  222 => 75,  218 => 85,  211 => 80,  198 => 64,  194 => 63,  184 => 59,  179 => 71,  152 => 46,  77 => 7,  63 => 17,  58 => 23,  53 => 21,  227 => 99,  215 => 79,  210 => 68,  205 => 68,  226 => 75,  219 => 73,  195 => 74,  172 => 58,  202 => 62,  190 => 72,  146 => 50,  113 => 33,  59 => 23,  267 => 96,  261 => 94,  254 => 111,  244 => 106,  228 => 91,  212 => 70,  204 => 75,  197 => 88,  191 => 48,  175 => 58,  167 => 53,  159 => 49,  127 => 39,  118 => 27,  134 => 41,  129 => 50,  100 => 21,  178 => 58,  161 => 48,  104 => 20,  102 => 26,  23 => 3,  181 => 68,  153 => 53,  148 => 45,  124 => 36,  97 => 20,  90 => 20,  76 => 7,  480 => 162,  474 => 217,  469 => 158,  461 => 207,  457 => 227,  453 => 204,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 210,  427 => 143,  423 => 142,  413 => 134,  409 => 176,  407 => 131,  402 => 171,  398 => 129,  393 => 166,  387 => 122,  384 => 162,  381 => 120,  379 => 119,  374 => 116,  368 => 176,  365 => 150,  362 => 110,  360 => 148,  355 => 106,  341 => 160,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 103,  285 => 114,  283 => 88,  278 => 98,  268 => 117,  264 => 79,  258 => 113,  252 => 96,  247 => 94,  241 => 82,  235 => 95,  229 => 87,  224 => 85,  220 => 81,  214 => 74,  208 => 79,  169 => 60,  143 => 35,  140 => 49,  132 => 51,  128 => 29,  119 => 46,  107 => 28,  71 => 10,  177 => 59,  165 => 54,  160 => 62,  135 => 47,  126 => 41,  114 => 36,  84 => 18,  70 => 10,  67 => 15,  61 => 13,  28 => 5,  87 => 19,  201 => 78,  196 => 65,  183 => 61,  171 => 56,  166 => 55,  163 => 58,  158 => 51,  156 => 54,  151 => 49,  142 => 53,  138 => 34,  136 => 37,  121 => 35,  117 => 33,  105 => 27,  91 => 39,  62 => 25,  49 => 19,  31 => 8,  21 => 6,  25 => 8,  93 => 18,  88 => 16,  78 => 14,  44 => 10,  94 => 20,  89 => 12,  85 => 10,  75 => 53,  68 => 29,  56 => 22,  27 => 18,  38 => 15,  24 => 7,  46 => 18,  26 => 6,  19 => 2,  79 => 8,  72 => 52,  69 => 5,  47 => 17,  40 => 11,  37 => 11,  22 => 2,  246 => 107,  157 => 48,  145 => 39,  139 => 65,  131 => 41,  123 => 37,  120 => 37,  115 => 34,  111 => 25,  108 => 31,  101 => 20,  98 => 25,  96 => 16,  83 => 15,  74 => 14,  66 => 27,  55 => 17,  52 => 21,  50 => 20,  43 => 24,  41 => 16,  35 => 9,  32 => 11,  29 => 6,  209 => 87,  203 => 77,  199 => 71,  193 => 68,  189 => 66,  187 => 71,  182 => 60,  176 => 87,  173 => 54,  168 => 61,  164 => 54,  162 => 49,  154 => 47,  149 => 58,  147 => 44,  144 => 42,  141 => 45,  133 => 43,  130 => 43,  125 => 31,  122 => 35,  116 => 30,  112 => 31,  109 => 30,  106 => 26,  103 => 23,  99 => 17,  95 => 19,  92 => 17,  86 => 14,  82 => 9,  80 => 8,  73 => 30,  64 => 14,  60 => 20,  57 => 11,  54 => 27,  51 => 20,  48 => 11,  45 => 17,  42 => 13,  39 => 10,  36 => 5,  33 => 7,  30 => 10,);
    }
}
