<?php

/* LaplaceTrainingBundle:Request:view-active-subscriptions.html.twig */
class __TwigTemplate_a66ab1d31fbfeb99c6b676e3a8ff4599 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
";
        // line 2
        $this->env->loadTemplate("LaplaceTrainingBundle:Request:view-active-subscriptions.html.twig", "1092938268")->display(array_merge($context, array("page" => array(0 => "request", 1 => "personal"))));
    }

    public function getTemplateName()
    {
        return "LaplaceTrainingBundle:Request:view-active-subscriptions.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  334 => 148,  327 => 143,  315 => 137,  306 => 131,  284 => 122,  279 => 120,  274 => 119,  272 => 118,  262 => 114,  239 => 103,  206 => 85,  186 => 64,  280 => 103,  255 => 90,  245 => 88,  150 => 48,  487 => 243,  482 => 240,  475 => 236,  470 => 234,  465 => 232,  454 => 226,  452 => 225,  449 => 224,  438 => 215,  436 => 214,  426 => 208,  421 => 204,  418 => 202,  412 => 199,  406 => 196,  399 => 192,  396 => 191,  394 => 190,  391 => 189,  386 => 185,  380 => 182,  373 => 178,  363 => 174,  349 => 163,  346 => 162,  344 => 161,  336 => 156,  323 => 149,  307 => 136,  304 => 135,  302 => 129,  288 => 123,  281 => 122,  265 => 111,  263 => 110,  260 => 109,  251 => 89,  233 => 94,  223 => 89,  185 => 72,  170 => 66,  180 => 58,  174 => 57,  65 => 27,  234 => 101,  225 => 76,  237 => 102,  230 => 100,  221 => 95,  213 => 58,  207 => 54,  200 => 52,  110 => 25,  34 => 20,  335 => 118,  330 => 153,  325 => 97,  320 => 139,  318 => 101,  311 => 97,  308 => 96,  299 => 133,  297 => 95,  292 => 92,  289 => 91,  282 => 84,  275 => 100,  273 => 84,  259 => 107,  257 => 107,  249 => 102,  240 => 98,  238 => 77,  231 => 72,  222 => 75,  218 => 85,  211 => 65,  198 => 80,  194 => 75,  184 => 59,  179 => 71,  152 => 46,  77 => 11,  63 => 17,  58 => 23,  53 => 21,  227 => 99,  215 => 79,  210 => 77,  205 => 68,  226 => 85,  219 => 94,  195 => 69,  172 => 58,  202 => 62,  190 => 75,  146 => 69,  113 => 29,  59 => 23,  267 => 94,  261 => 125,  254 => 111,  244 => 106,  228 => 91,  212 => 70,  204 => 75,  197 => 88,  191 => 48,  175 => 69,  167 => 42,  159 => 35,  127 => 33,  118 => 27,  134 => 41,  129 => 50,  100 => 22,  178 => 65,  161 => 48,  104 => 20,  102 => 30,  23 => 3,  181 => 68,  153 => 51,  148 => 40,  124 => 36,  97 => 28,  90 => 17,  76 => 7,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 227,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 210,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 176,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 160,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 126,  285 => 85,  283 => 88,  278 => 86,  268 => 117,  264 => 79,  258 => 113,  252 => 79,  247 => 78,  241 => 86,  235 => 95,  229 => 73,  224 => 71,  220 => 81,  214 => 91,  208 => 68,  169 => 60,  143 => 35,  140 => 44,  132 => 51,  128 => 29,  119 => 46,  107 => 28,  71 => 10,  177 => 60,  165 => 54,  160 => 62,  135 => 36,  126 => 39,  114 => 36,  84 => 18,  70 => 10,  67 => 15,  61 => 13,  28 => 5,  87 => 19,  201 => 78,  196 => 63,  183 => 91,  171 => 43,  166 => 65,  163 => 52,  158 => 67,  156 => 48,  151 => 57,  142 => 53,  138 => 34,  136 => 37,  121 => 37,  117 => 38,  105 => 32,  91 => 39,  62 => 25,  49 => 19,  31 => 8,  21 => 6,  25 => 5,  93 => 18,  88 => 16,  78 => 14,  44 => 10,  94 => 20,  89 => 12,  85 => 10,  75 => 53,  68 => 29,  56 => 22,  27 => 18,  38 => 15,  24 => 7,  46 => 18,  26 => 6,  19 => 1,  79 => 8,  72 => 52,  69 => 5,  47 => 17,  40 => 11,  37 => 11,  22 => 2,  246 => 107,  157 => 50,  145 => 39,  139 => 65,  131 => 36,  123 => 47,  120 => 29,  115 => 27,  111 => 25,  108 => 40,  101 => 19,  98 => 25,  96 => 16,  83 => 15,  74 => 14,  66 => 27,  55 => 17,  52 => 21,  50 => 20,  43 => 24,  41 => 16,  35 => 9,  32 => 11,  29 => 6,  209 => 87,  203 => 78,  199 => 71,  193 => 68,  189 => 66,  187 => 54,  182 => 62,  176 => 87,  173 => 54,  168 => 52,  164 => 41,  162 => 49,  154 => 61,  149 => 58,  147 => 44,  144 => 42,  141 => 43,  133 => 43,  130 => 33,  125 => 31,  122 => 35,  116 => 26,  112 => 31,  109 => 30,  106 => 26,  103 => 54,  99 => 17,  95 => 19,  92 => 26,  86 => 16,  82 => 9,  80 => 34,  73 => 30,  64 => 14,  60 => 20,  57 => 11,  54 => 27,  51 => 20,  48 => 11,  45 => 17,  42 => 13,  39 => 10,  36 => 5,  33 => 7,  30 => 10,);
    }
}


/* LaplaceTrainingBundle:Request:view-active-subscriptions.html.twig */
class __TwigTemplate_a66ab1d31fbfeb99c6b676e3a8ff4599_1092938268 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("LaplaceCommonBundle::user-page.html.twig");

        $this->blocks = array(
            'PageTitle' => array($this, 'block_PageTitle'),
            'StyleSheets' => array($this, 'block_StyleSheets'),
            'ContentTitle' => array($this, 'block_ContentTitle'),
            'ContentBody' => array($this, 'block_ContentBody'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "LaplaceCommonBundle::user-page.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_PageTitle($context, array $blocks = array())
    {
        $this->displayParentBlock("PageTitle", $context, $blocks);
        echo " - Demandes personnelles";
    }

    // line 7
    public function block_StyleSheets($context, array $blocks = array())
    {
        // line 8
        $this->displayParentBlock("StyleSheets", $context, $blocks);
        echo "
<style>
    .active-sub-table td,
    .active-sub-table th {
        width: 25%;
    }
</style>
";
    }

    // line 17
    public function block_ContentTitle($context, array $blocks = array())
    {
        echo "Demandes personnelles";
    }

    // line 19
    public function block_ContentBody($context, array $blocks = array())
    {
        // line 20
        echo "
<p>
    Cette page regroupe l'ensemble de vos <strong>inscriptions à des demandes de formation</strong>,
    classées par date d'ajout (de la plus récente à la moins récente).

    Cliquez sur le titre d'une demande pour voir sa description complète ou pour
    <strong>modifier votre inscription</strong>.
</p>

<br />

<table class=\"table table-striped table-bordered active-sub-table\">
    ";
        // line 32
        if (twig_test_empty((isset($context["subscriptions"]) ? $context["subscriptions"] : $this->getContext($context, "subscriptions")))) {
            // line 33
            echo "    <tr>
        <td>Aucune demande.</td>
    <tr>
    ";
        }
        // line 37
        echo "

    ";
        // line 39
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["subscriptions"]) ? $context["subscriptions"] : $this->getContext($context, "subscriptions")));
        foreach ($context['_seq'] as $context["_key"] => $context["sub"]) {
            // line 40
            echo "    <tr>
        <td>
            <span class=\"badge badge-info\" title=\"";
            // line 42
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["sub"]) ? $context["sub"] : $this->getContext($context, "sub")), "type"), "description"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["sub"]) ? $context["sub"] : $this->getContext($context, "sub")), "type"), "name"), "html", null, true);
            echo "</span>
            <a href=\"";
            // line 43
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("laplace_training_view_request", array("id" => $this->getAttribute($this->getAttribute((isset($context["sub"]) ? $context["sub"] : $this->getContext($context, "sub")), "request"), "id"))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["sub"]) ? $context["sub"] : $this->getContext($context, "sub")), "request"), "title"), "html", null, true);
            echo "</a>
            <span class=\"pull-right text-right\">
                Ajoutée le ";
            // line 45
            echo twig_escape_filter($this->env, twig_localized_date_filter($this->env, $this->getAttribute((isset($context["sub"]) ? $context["sub"] : $this->getContext($context, "sub")), "subscriptionDate"), "long", "short"), "html", null, true);
            echo "<br />
            ";
            // line 46
            if (($this->getAttribute((isset($context["sub"]) ? $context["sub"] : $this->getContext($context, "sub")), "accepted") === true)) {
                // line 47
                echo "                <i class=\"icon-ok-sign\"></i> Acceptée /
                ";
                // line 48
                if (($this->getAttribute((isset($context["sub"]) ? $context["sub"] : $this->getContext($context, "sub")), "attended") === true)) {
                    // line 49
                    echo "                Effectuée
                ";
                } elseif (($this->getAttribute((isset($context["sub"]) ? $context["sub"] : $this->getContext($context, "sub")), "attended") === false)) {
                    // line 51
                    echo "                Non effectuée
                ";
                } else {
                    // line 53
                    echo "                <span class=\"text-error\">En attente</span>
                ";
                }
                // line 55
                echo "            ";
            } elseif (($this->getAttribute((isset($context["sub"]) ? $context["sub"] : $this->getContext($context, "sub")), "accepted") === false)) {
                // line 56
                echo "                <i class=\"icon-minus-sign\"></i> Refusée
            ";
            } else {
                // line 58
                echo "                <i class=\"icon-question-sign\"></i> En attente de validation
            ";
            }
            // line 60
            echo "            </span>
            <br />
            ";
            // line 62
            if ($this->getAttribute($this->getAttribute((isset($context["sub"]) ? $context["sub"] : $this->getContext($context, "sub")), "request"), "category")) {
                // line 63
                echo "            <small>";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["sub"]) ? $context["sub"] : $this->getContext($context, "sub")), "request"), "category"), "domain"), "name"), "html", null, true);
                echo " / <small>";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["sub"]) ? $context["sub"] : $this->getContext($context, "sub")), "request"), "category"), "name"), "html", null, true);
                echo "</small></small>
            ";
            } else {
                // line 65
                echo "            <small><em>Aucune catégorie</em></small>
            ";
            }
            // line 67
            echo "        </td>
    </tr>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sub'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 70
        echo "</table>

<p class=\"text-right\">
    <i class=\"icon-list\"></i>
    <a href=\"";
        // line 74
        echo $this->env->getExtension('routing')->getPath("laplace_training_all_requests");
        echo "\">Voir toutes les demandes</a>
</p>

";
    }

    public function getTemplateName()
    {
        return "LaplaceTrainingBundle:Request:view-active-subscriptions.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  188 => 63,  334 => 148,  327 => 143,  315 => 137,  306 => 131,  284 => 122,  279 => 120,  274 => 119,  272 => 118,  262 => 114,  239 => 103,  206 => 85,  186 => 62,  280 => 103,  255 => 90,  245 => 88,  150 => 48,  487 => 243,  482 => 240,  475 => 236,  470 => 234,  465 => 232,  454 => 226,  452 => 225,  449 => 224,  438 => 215,  436 => 214,  426 => 208,  421 => 204,  418 => 202,  412 => 199,  406 => 196,  399 => 192,  396 => 191,  394 => 190,  391 => 189,  386 => 185,  380 => 182,  373 => 178,  363 => 174,  349 => 163,  346 => 162,  344 => 161,  336 => 156,  323 => 149,  307 => 136,  304 => 135,  302 => 129,  288 => 123,  281 => 122,  265 => 111,  263 => 110,  260 => 109,  251 => 89,  233 => 94,  223 => 89,  185 => 72,  170 => 66,  180 => 58,  174 => 56,  65 => 27,  234 => 101,  225 => 76,  237 => 102,  230 => 100,  221 => 95,  213 => 58,  207 => 54,  200 => 67,  110 => 25,  34 => 20,  335 => 118,  330 => 153,  325 => 97,  320 => 139,  318 => 101,  311 => 97,  308 => 96,  299 => 133,  297 => 95,  292 => 92,  289 => 91,  282 => 84,  275 => 100,  273 => 84,  259 => 107,  257 => 107,  249 => 102,  240 => 98,  238 => 77,  231 => 72,  222 => 75,  218 => 85,  211 => 65,  198 => 80,  194 => 75,  184 => 59,  179 => 71,  152 => 46,  77 => 7,  63 => 17,  58 => 23,  53 => 21,  227 => 99,  215 => 79,  210 => 77,  205 => 68,  226 => 85,  219 => 94,  195 => 69,  172 => 58,  202 => 62,  190 => 75,  146 => 69,  113 => 29,  59 => 23,  267 => 94,  261 => 125,  254 => 111,  244 => 106,  228 => 91,  212 => 70,  204 => 75,  197 => 88,  191 => 48,  175 => 69,  167 => 53,  159 => 49,  127 => 39,  118 => 27,  134 => 41,  129 => 50,  100 => 22,  178 => 58,  161 => 48,  104 => 20,  102 => 30,  23 => 3,  181 => 68,  153 => 51,  148 => 45,  124 => 36,  97 => 28,  90 => 17,  76 => 7,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 227,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 210,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 176,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 160,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 126,  285 => 85,  283 => 88,  278 => 86,  268 => 117,  264 => 79,  258 => 113,  252 => 79,  247 => 78,  241 => 86,  235 => 95,  229 => 73,  224 => 71,  220 => 81,  214 => 74,  208 => 70,  169 => 60,  143 => 35,  140 => 44,  132 => 51,  128 => 29,  119 => 46,  107 => 28,  71 => 10,  177 => 60,  165 => 54,  160 => 62,  135 => 42,  126 => 39,  114 => 36,  84 => 18,  70 => 5,  67 => 15,  61 => 13,  28 => 5,  87 => 19,  201 => 78,  196 => 65,  183 => 91,  171 => 55,  166 => 65,  163 => 51,  158 => 67,  156 => 48,  151 => 57,  142 => 53,  138 => 34,  136 => 37,  121 => 37,  117 => 33,  105 => 32,  91 => 39,  62 => 25,  49 => 19,  31 => 8,  21 => 6,  25 => 5,  93 => 18,  88 => 16,  78 => 14,  44 => 10,  94 => 20,  89 => 12,  85 => 10,  75 => 53,  68 => 29,  56 => 22,  27 => 18,  38 => 15,  24 => 7,  46 => 18,  26 => 6,  19 => 1,  79 => 8,  72 => 52,  69 => 5,  47 => 17,  40 => 11,  37 => 11,  22 => 2,  246 => 107,  157 => 48,  145 => 39,  139 => 65,  131 => 40,  123 => 37,  120 => 29,  115 => 32,  111 => 25,  108 => 40,  101 => 20,  98 => 19,  96 => 16,  83 => 15,  74 => 14,  66 => 27,  55 => 17,  52 => 21,  50 => 20,  43 => 24,  41 => 16,  35 => 9,  32 => 11,  29 => 6,  209 => 87,  203 => 78,  199 => 71,  193 => 68,  189 => 66,  187 => 54,  182 => 60,  176 => 87,  173 => 54,  168 => 52,  164 => 41,  162 => 49,  154 => 47,  149 => 58,  147 => 44,  144 => 42,  141 => 43,  133 => 43,  130 => 33,  125 => 31,  122 => 35,  116 => 26,  112 => 31,  109 => 30,  106 => 26,  103 => 54,  99 => 17,  95 => 19,  92 => 17,  86 => 16,  82 => 9,  80 => 8,  73 => 30,  64 => 14,  60 => 20,  57 => 11,  54 => 27,  51 => 20,  48 => 11,  45 => 17,  42 => 13,  39 => 10,  36 => 5,  33 => 7,  30 => 10,);
    }
}
