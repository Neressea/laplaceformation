<?php

/* LaplaceTrainingBundle:System:manage-types.html.twig */
class __TwigTemplate_136f51f8731f21ed7f663f91f6addd5f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
";
        // line 2
        $this->env->loadTemplate("LaplaceTrainingBundle:System:manage-types.html.twig", "1400452793")->display(array_merge($context, array("page" => array(0 => "system", 1 => "types"))));
    }

    public function getTemplateName()
    {
        return "LaplaceTrainingBundle:System:manage-types.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  280 => 103,  255 => 90,  245 => 88,  150 => 48,  487 => 243,  482 => 240,  475 => 236,  470 => 234,  465 => 232,  454 => 226,  452 => 225,  449 => 224,  438 => 215,  436 => 214,  426 => 208,  421 => 204,  418 => 202,  412 => 199,  406 => 196,  399 => 192,  396 => 191,  394 => 190,  391 => 189,  386 => 185,  380 => 182,  373 => 178,  363 => 174,  349 => 163,  346 => 162,  344 => 161,  336 => 156,  323 => 149,  307 => 136,  304 => 135,  302 => 134,  288 => 126,  281 => 122,  265 => 111,  263 => 110,  260 => 109,  251 => 89,  233 => 94,  223 => 89,  185 => 72,  170 => 55,  180 => 58,  174 => 57,  65 => 28,  234 => 74,  225 => 76,  237 => 85,  230 => 70,  221 => 64,  213 => 58,  207 => 54,  200 => 52,  110 => 25,  34 => 20,  335 => 118,  330 => 153,  325 => 97,  320 => 102,  318 => 101,  311 => 97,  308 => 96,  299 => 133,  297 => 95,  292 => 92,  289 => 91,  282 => 84,  275 => 100,  273 => 84,  259 => 107,  257 => 107,  249 => 102,  240 => 98,  238 => 77,  231 => 72,  222 => 75,  218 => 85,  211 => 65,  198 => 77,  194 => 75,  184 => 59,  179 => 71,  152 => 46,  77 => 7,  63 => 17,  58 => 24,  53 => 21,  227 => 77,  215 => 79,  210 => 77,  205 => 68,  226 => 90,  219 => 80,  195 => 68,  172 => 57,  202 => 62,  190 => 95,  146 => 69,  113 => 22,  59 => 23,  267 => 94,  261 => 125,  254 => 90,  244 => 118,  228 => 91,  212 => 70,  204 => 79,  197 => 88,  191 => 48,  175 => 69,  167 => 42,  159 => 35,  127 => 39,  118 => 24,  134 => 47,  129 => 41,  100 => 45,  178 => 65,  161 => 63,  104 => 19,  102 => 26,  23 => 3,  181 => 63,  153 => 51,  148 => 55,  124 => 36,  97 => 15,  90 => 13,  76 => 7,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 227,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 210,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 176,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 160,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 129,  285 => 85,  283 => 88,  278 => 86,  268 => 112,  264 => 79,  258 => 81,  252 => 79,  247 => 78,  241 => 86,  235 => 95,  229 => 73,  224 => 71,  220 => 81,  214 => 82,  208 => 68,  169 => 60,  143 => 35,  140 => 44,  132 => 51,  128 => 29,  119 => 39,  107 => 28,  71 => 10,  177 => 59,  165 => 54,  160 => 40,  135 => 35,  126 => 34,  114 => 36,  84 => 18,  70 => 10,  67 => 15,  61 => 13,  28 => 3,  87 => 19,  201 => 78,  196 => 63,  183 => 91,  171 => 43,  166 => 65,  163 => 51,  158 => 67,  156 => 47,  151 => 57,  142 => 59,  138 => 34,  136 => 43,  121 => 35,  117 => 38,  105 => 27,  91 => 48,  62 => 27,  49 => 19,  31 => 8,  21 => 6,  25 => 5,  93 => 42,  88 => 16,  78 => 14,  44 => 10,  94 => 14,  89 => 12,  85 => 10,  75 => 53,  68 => 29,  56 => 9,  27 => 18,  38 => 7,  24 => 7,  46 => 18,  26 => 6,  19 => 1,  79 => 8,  72 => 52,  69 => 5,  47 => 17,  40 => 11,  37 => 11,  22 => 2,  246 => 101,  157 => 50,  145 => 46,  139 => 65,  131 => 45,  123 => 37,  120 => 29,  115 => 32,  111 => 25,  108 => 33,  101 => 20,  98 => 25,  96 => 16,  83 => 37,  74 => 14,  66 => 27,  55 => 17,  52 => 19,  50 => 14,  43 => 24,  41 => 7,  35 => 9,  32 => 4,  29 => 6,  209 => 76,  203 => 78,  199 => 64,  193 => 73,  189 => 65,  187 => 54,  182 => 61,  176 => 87,  173 => 68,  168 => 84,  164 => 41,  162 => 49,  154 => 49,  149 => 43,  147 => 44,  144 => 41,  141 => 43,  133 => 31,  130 => 32,  125 => 28,  122 => 25,  116 => 25,  112 => 30,  109 => 21,  106 => 48,  103 => 54,  99 => 31,  95 => 17,  92 => 26,  86 => 12,  82 => 9,  80 => 8,  73 => 19,  64 => 14,  60 => 20,  57 => 11,  54 => 27,  51 => 20,  48 => 11,  45 => 17,  42 => 13,  39 => 16,  36 => 5,  33 => 12,  30 => 7,);
    }
}


/* LaplaceTrainingBundle:System:manage-types.html.twig */
class __TwigTemplate_136f51f8731f21ed7f663f91f6addd5f_1400452793 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("LaplaceCommonBundle::admin-page.html.twig");

        $this->blocks = array(
            'PageTitle' => array($this, 'block_PageTitle'),
            'ContentTitle' => array($this, 'block_ContentTitle'),
            'ContentBody' => array($this, 'block_ContentBody'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "LaplaceCommonBundle::admin-page.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 6
    public function block_PageTitle($context, array $blocks = array())
    {
        $this->displayParentBlock("PageTitle", $context, $blocks);
        echo " - Types";
    }

    // line 8
    public function block_ContentTitle($context, array $blocks = array())
    {
        echo "Types";
    }

    // line 10
    public function block_ContentBody($context, array $blocks = array())
    {
        // line 11
        echo "
<h3>Modifier</h3>

";
        // line 14
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["edit_forms"]) ? $context["edit_forms"] : $this->getContext($context, "edit_forms")));
        foreach ($context['_seq'] as $context["_key"] => $context["edit_form"]) {
            // line 15
            echo "
";
            // line 16
            echo             $this->env->getExtension('form')->renderer->renderBlock((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), 'form_start');
            echo "

<fieldset>

    ";
            // line 20
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "type"), 'rest');
            echo "

    ";
            // line 22
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), 'rest');
            echo "

</fieldset>

";
            // line 26
            echo             $this->env->getExtension('form')->renderer->renderBlock((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), 'form_end');
            echo "

";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['edit_form'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 29
        echo "
<hr />

";
        // line 33
        echo "
<h3>Créer / Supprimer / Remplacer</h3>

";
        // line 36
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["create_form"]) ? $context["create_form"] : $this->getContext($context, "create_form")), 'form_start');
        echo "

<fieldset>

    <legend>Créer un type</legend>

    ";
        // line 42
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["create_form"]) ? $context["create_form"] : $this->getContext($context, "create_form")), "type"), 'rest');
        echo "

    ";
        // line 44
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["create_form"]) ? $context["create_form"] : $this->getContext($context, "create_form")), 'rest');
        echo "

</fieldset>

";
        // line 48
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["create_form"]) ? $context["create_form"] : $this->getContext($context, "create_form")), 'form_end');
        echo "



";
        // line 52
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'form_start');
        echo "

<fieldset>

    <legend>Supprimer un type</legend>

    ";
        // line 58
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), "type"), 'row');
        echo "

    ";
        // line 60
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), "do"), 'row', array("attr" => array("class" => "btn-danger")));
        echo "

    ";
        // line 62
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'rest');
        echo "

</fieldset>

";
        // line 66
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'form_end');
        echo "


";
        // line 69
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["replace_form"]) ? $context["replace_form"] : $this->getContext($context, "replace_form")), 'form_start');
        echo "

<fieldset>

    <legend>Remplacer un type par un autre</legend>

    ";
        // line 75
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["replace_form"]) ? $context["replace_form"] : $this->getContext($context, "replace_form")), "from"), 'row');
        echo "

    ";
        // line 77
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["replace_form"]) ? $context["replace_form"] : $this->getContext($context, "replace_form")), "to"), 'row');
        echo "

    ";
        // line 79
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["replace_form"]) ? $context["replace_form"] : $this->getContext($context, "replace_form")), "do"), 'row', array("attr" => array("class" => "btn-danger")));
        echo "

    ";
        // line 81
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["replace_form"]) ? $context["replace_form"] : $this->getContext($context, "replace_form")), 'rest');
        echo "

</fieldset>

";
        // line 85
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["replace_form"]) ? $context["replace_form"] : $this->getContext($context, "replace_form")), 'form_end');
        echo "

";
    }

    public function getTemplateName()
    {
        return "LaplaceTrainingBundle:System:manage-types.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  280 => 103,  255 => 90,  245 => 88,  150 => 48,  487 => 243,  482 => 240,  475 => 236,  470 => 234,  465 => 232,  454 => 226,  452 => 225,  449 => 224,  438 => 215,  436 => 214,  426 => 208,  421 => 204,  418 => 202,  412 => 199,  406 => 196,  399 => 192,  396 => 191,  394 => 190,  391 => 189,  386 => 185,  380 => 182,  373 => 178,  363 => 174,  349 => 163,  346 => 162,  344 => 161,  336 => 156,  323 => 149,  307 => 136,  304 => 135,  302 => 134,  288 => 126,  281 => 122,  265 => 111,  263 => 110,  260 => 109,  251 => 89,  233 => 94,  223 => 89,  185 => 72,  170 => 55,  180 => 58,  174 => 57,  65 => 28,  234 => 74,  225 => 76,  237 => 85,  230 => 70,  221 => 64,  213 => 58,  207 => 54,  200 => 52,  110 => 25,  34 => 20,  335 => 118,  330 => 153,  325 => 97,  320 => 102,  318 => 101,  311 => 97,  308 => 96,  299 => 133,  297 => 95,  292 => 92,  289 => 91,  282 => 84,  275 => 100,  273 => 84,  259 => 107,  257 => 107,  249 => 102,  240 => 98,  238 => 77,  231 => 72,  222 => 75,  218 => 85,  211 => 65,  198 => 77,  194 => 75,  184 => 59,  179 => 71,  152 => 46,  77 => 7,  63 => 17,  58 => 24,  53 => 21,  227 => 77,  215 => 79,  210 => 77,  205 => 68,  226 => 85,  219 => 81,  195 => 69,  172 => 58,  202 => 62,  190 => 95,  146 => 69,  113 => 22,  59 => 23,  267 => 94,  261 => 125,  254 => 90,  244 => 118,  228 => 91,  212 => 70,  204 => 75,  197 => 88,  191 => 48,  175 => 69,  167 => 42,  159 => 35,  127 => 39,  118 => 24,  134 => 47,  129 => 41,  100 => 45,  178 => 65,  161 => 63,  104 => 20,  102 => 26,  23 => 3,  181 => 63,  153 => 51,  148 => 55,  124 => 36,  97 => 16,  90 => 14,  76 => 8,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 227,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 210,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 176,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 160,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 129,  285 => 85,  283 => 88,  278 => 86,  268 => 112,  264 => 79,  258 => 81,  252 => 79,  247 => 78,  241 => 86,  235 => 95,  229 => 73,  224 => 71,  220 => 81,  214 => 79,  208 => 68,  169 => 60,  143 => 35,  140 => 44,  132 => 51,  128 => 29,  119 => 39,  107 => 28,  71 => 10,  177 => 60,  165 => 54,  160 => 40,  135 => 36,  126 => 34,  114 => 36,  84 => 18,  70 => 10,  67 => 15,  61 => 13,  28 => 3,  87 => 19,  201 => 78,  196 => 63,  183 => 91,  171 => 43,  166 => 65,  163 => 52,  158 => 67,  156 => 48,  151 => 57,  142 => 59,  138 => 34,  136 => 43,  121 => 35,  117 => 38,  105 => 27,  91 => 48,  62 => 27,  49 => 19,  31 => 8,  21 => 6,  25 => 5,  93 => 42,  88 => 16,  78 => 14,  44 => 10,  94 => 15,  89 => 12,  85 => 11,  75 => 53,  68 => 29,  56 => 9,  27 => 18,  38 => 7,  24 => 7,  46 => 18,  26 => 6,  19 => 1,  79 => 8,  72 => 52,  69 => 6,  47 => 17,  40 => 11,  37 => 11,  22 => 2,  246 => 101,  157 => 50,  145 => 46,  139 => 65,  131 => 45,  123 => 37,  120 => 29,  115 => 32,  111 => 25,  108 => 33,  101 => 20,  98 => 25,  96 => 16,  83 => 37,  74 => 14,  66 => 27,  55 => 17,  52 => 19,  50 => 14,  43 => 24,  41 => 7,  35 => 9,  32 => 4,  29 => 6,  209 => 77,  203 => 78,  199 => 64,  193 => 73,  189 => 66,  187 => 54,  182 => 62,  176 => 87,  173 => 68,  168 => 84,  164 => 41,  162 => 49,  154 => 49,  149 => 44,  147 => 44,  144 => 42,  141 => 43,  133 => 31,  130 => 33,  125 => 29,  122 => 25,  116 => 26,  112 => 30,  109 => 22,  106 => 48,  103 => 54,  99 => 31,  95 => 17,  92 => 26,  86 => 12,  82 => 10,  80 => 8,  73 => 19,  64 => 14,  60 => 20,  57 => 11,  54 => 27,  51 => 20,  48 => 11,  45 => 17,  42 => 13,  39 => 16,  36 => 5,  33 => 12,  30 => 7,);
    }
}
