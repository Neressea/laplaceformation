<?php

/* LaplaceUserBundle:Security:login.html.twig */
class __TwigTemplate_332a540d81e7497c460b7f8b3ea2f0b9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::layout.html.twig");

        $this->blocks = array(
            'PageTitle' => array($this, 'block_PageTitle'),
            'StyleSheets' => array($this, 'block_StyleSheets'),
            'NavbarOuter' => array($this, 'block_NavbarOuter'),
            'MainContainerOuter' => array($this, 'block_MainContainerOuter'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_PageTitle($context, array $blocks = array())
    {
        $this->displayParentBlock("PageTitle", $context, $blocks);
        echo " - Connexion";
    }

    // line 6
    public function block_StyleSheets($context, array $blocks = array())
    {
        // line 7
        echo "<style type=\"text/css\">
        body {
            padding-top: 40px;
            padding-bottom: 40px;
            background-color: #f5f5f5;
        }

        .form-signin {
            max-width: 300px;
            padding: 19px 29px 29px;
            margin: 0 auto 20px;
            background-color: #fff;
            border: 1px solid #e5e5e5;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
            -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
            -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
            box-shadow: 0 1px 2px rgba(0,0,0,.05);
        }
        .form-signin .form-signin-heading,
        .form-signin .checkbox {
            margin-bottom: 10px;
        }
        .form-signin input[type=\"text\"],
        .form-signin input[type=\"password\"] {
            font-size: 16px;
            height: auto;
            margin-bottom: 15px;
            padding: 7px 9px;
        }

    </style>
";
    }

    // line 42
    public function block_NavbarOuter($context, array $blocks = array())
    {
    }

    // line 44
    public function block_MainContainerOuter($context, array $blocks = array())
    {
        // line 45
        echo "    <!-- MAIN CONTAINER -->
    <div class=\"container\">

        <form method=\"POST\" action=\"";
        // line 48
        echo $this->env->getExtension('routing')->getPath("login_check");
        echo "\" class=\"form-signin\">

            <h2 class=\"form-signin-heading\">Connexion</h2>

            ";
        // line 52
        if ((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error"))) {
            // line 53
            echo "            <div class=\"alert alert-error\">
                ";
            // line 54
            echo twig_escape_filter($this->env, (isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), "html", null, true);
            echo "
            </div>
            ";
        }
        // line 57
        echo "
            <label for=\"username\">Login :</label>
            <input type=\"text\" id=\"username\" name=\"_username\" value=\"";
        // line 59
        echo twig_escape_filter($this->env, (isset($context["last_username"]) ? $context["last_username"] : $this->getContext($context, "last_username")), "html", null, true);
        echo "\" class=\"input-block-level\" />

            <label for=\"password\">Mot de passe :</label>
            <input type=\"password\" id=\"password\" name=\"_password\" class=\"input-block-level\" />

            <button type=\"submit\" class=\"btn btn-large btn-primary input-block-level\">Connexion</button>
        </form>

    </div>

";
    }

    public function getTemplateName()
    {
        return "LaplaceUserBundle:Security:login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  227 => 85,  215 => 79,  210 => 77,  205 => 75,  226 => 84,  219 => 80,  195 => 68,  172 => 57,  202 => 100,  190 => 95,  146 => 69,  113 => 59,  59 => 23,  267 => 127,  261 => 125,  254 => 123,  244 => 118,  228 => 107,  212 => 97,  204 => 74,  197 => 88,  191 => 85,  175 => 72,  167 => 67,  159 => 62,  127 => 60,  118 => 32,  134 => 37,  129 => 41,  100 => 53,  178 => 65,  161 => 82,  104 => 19,  102 => 25,  23 => 3,  181 => 63,  153 => 51,  148 => 42,  124 => 31,  97 => 15,  90 => 13,  76 => 7,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 144,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 112,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 105,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 90,  285 => 89,  283 => 88,  278 => 86,  268 => 85,  264 => 84,  258 => 81,  252 => 80,  247 => 78,  241 => 77,  235 => 111,  229 => 73,  224 => 71,  220 => 81,  214 => 78,  208 => 68,  169 => 60,  143 => 45,  140 => 55,  132 => 51,  128 => 49,  119 => 30,  107 => 25,  71 => 17,  177 => 59,  165 => 83,  160 => 61,  135 => 35,  126 => 34,  114 => 26,  84 => 35,  70 => 6,  67 => 15,  61 => 13,  28 => 3,  87 => 14,  201 => 92,  196 => 69,  183 => 91,  171 => 85,  166 => 58,  163 => 51,  158 => 67,  156 => 47,  151 => 57,  142 => 59,  138 => 38,  136 => 41,  121 => 46,  117 => 35,  105 => 49,  91 => 48,  62 => 23,  49 => 19,  31 => 3,  21 => 6,  25 => 5,  93 => 17,  88 => 15,  78 => 42,  44 => 16,  94 => 14,  89 => 13,  85 => 10,  75 => 23,  68 => 14,  56 => 9,  27 => 4,  38 => 6,  24 => 3,  46 => 7,  26 => 6,  19 => 1,  79 => 8,  72 => 28,  69 => 5,  47 => 17,  40 => 8,  37 => 14,  22 => 2,  246 => 119,  157 => 56,  145 => 46,  139 => 65,  131 => 39,  123 => 34,  120 => 56,  115 => 43,  111 => 25,  108 => 29,  101 => 23,  98 => 52,  96 => 31,  83 => 44,  74 => 14,  66 => 27,  55 => 15,  52 => 19,  50 => 10,  43 => 6,  41 => 7,  35 => 5,  32 => 4,  29 => 3,  209 => 76,  203 => 78,  199 => 89,  193 => 73,  189 => 65,  187 => 84,  182 => 61,  176 => 87,  173 => 62,  168 => 84,  164 => 52,  162 => 50,  154 => 54,  149 => 43,  147 => 58,  144 => 41,  141 => 51,  133 => 61,  130 => 32,  125 => 28,  122 => 37,  116 => 25,  112 => 29,  109 => 57,  106 => 36,  103 => 54,  99 => 24,  95 => 34,  92 => 14,  86 => 45,  82 => 9,  80 => 19,  73 => 19,  64 => 14,  60 => 6,  57 => 11,  54 => 10,  51 => 14,  48 => 8,  45 => 17,  42 => 7,  39 => 9,  36 => 5,  33 => 4,  30 => 7,);
    }
}
