<?php

/* LaplaceUserBundle:UserProfile:edit.html.twig */
class __TwigTemplate_6963b5e165ec1a47149dcfc3766a64c1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        $context["container"] = (((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin"))) ? ("LaplaceCommonBundle::admin-page.html.twig") : ("LaplaceCommonBundle::user-page.html.twig"));
        // line 6
        echo "
";
        // line 7
        $this->env->loadTemplate("LaplaceUserBundle:UserProfile:edit.html.twig", "580517751")->display(array_merge($context, array("page" => array(0 => "profile", 1 => "edit"))));
    }

    public function getTemplateName()
    {
        return "LaplaceUserBundle:UserProfile:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  104 => 22,  102 => 20,  23 => 3,  181 => 63,  153 => 44,  148 => 42,  124 => 31,  97 => 17,  90 => 11,  76 => 6,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 144,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 112,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 105,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 90,  285 => 89,  283 => 88,  278 => 86,  268 => 85,  264 => 84,  258 => 81,  252 => 80,  247 => 78,  241 => 77,  235 => 74,  229 => 73,  224 => 71,  220 => 70,  214 => 69,  208 => 68,  169 => 60,  143 => 40,  140 => 55,  132 => 51,  128 => 49,  119 => 42,  107 => 36,  71 => 17,  177 => 65,  165 => 64,  160 => 61,  135 => 47,  126 => 45,  114 => 25,  84 => 28,  70 => 6,  67 => 15,  61 => 13,  28 => 3,  87 => 20,  201 => 92,  196 => 90,  183 => 70,  171 => 56,  166 => 71,  163 => 70,  158 => 67,  156 => 58,  151 => 57,  142 => 59,  138 => 38,  136 => 56,  121 => 46,  117 => 44,  105 => 40,  91 => 31,  62 => 23,  49 => 19,  31 => 5,  21 => 6,  25 => 4,  93 => 28,  88 => 10,  78 => 21,  44 => 12,  94 => 16,  89 => 13,  85 => 9,  75 => 23,  68 => 14,  56 => 9,  27 => 4,  38 => 6,  24 => 7,  46 => 7,  26 => 6,  19 => 2,  79 => 8,  72 => 16,  69 => 4,  47 => 9,  40 => 8,  37 => 10,  22 => 2,  246 => 32,  157 => 56,  145 => 46,  139 => 50,  131 => 42,  123 => 47,  120 => 40,  115 => 43,  111 => 24,  108 => 37,  101 => 32,  98 => 31,  96 => 31,  83 => 25,  74 => 14,  66 => 15,  55 => 15,  52 => 21,  50 => 10,  43 => 6,  41 => 5,  35 => 5,  32 => 4,  29 => 3,  209 => 82,  203 => 78,  199 => 67,  193 => 73,  189 => 71,  187 => 84,  182 => 66,  176 => 64,  173 => 74,  168 => 66,  164 => 52,  162 => 62,  154 => 54,  149 => 51,  147 => 58,  144 => 53,  141 => 51,  133 => 36,  130 => 35,  125 => 44,  122 => 30,  116 => 36,  112 => 42,  109 => 41,  106 => 36,  103 => 19,  99 => 19,  95 => 34,  92 => 14,  86 => 12,  82 => 8,  80 => 19,  73 => 19,  64 => 14,  60 => 6,  57 => 11,  54 => 10,  51 => 14,  48 => 8,  45 => 17,  42 => 7,  39 => 9,  36 => 5,  33 => 4,  30 => 7,);
    }
}


/* LaplaceUserBundle:UserProfile:edit.html.twig */
class __TwigTemplate_6963b5e165ec1a47149dcfc3766a64c1_580517751 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'PageTitle' => array($this, 'block_PageTitle'),
            'ContentTitle' => array($this, 'block_ContentTitle'),
            'ContentBody' => array($this, 'block_ContentBody'),
        );
    }

    protected function doGetParent(array $context)
    {
        return $this->env->resolveTemplate((isset($context["container"]) ? $context["container"] : $this->getContext($context, "container")));
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 9
    public function block_PageTitle($context, array $blocks = array())
    {
        $this->displayParentBlock("PageTitle", $context, $blocks);
        echo " - Modifier le profil (";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["profile"]) ? $context["profile"] : $this->getContext($context, "profile")), "fullName"), "html", null, true);
        echo ")";
    }

    // line 11
    public function block_ContentTitle($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["profile"]) ? $context["profile"] : $this->getContext($context, "profile")), "fullName"), "html", null, true);
    }

    // line 13
    public function block_ContentBody($context, array $blocks = array())
    {
        // line 14
        echo "
";
        // line 16
        echo "

";
        // line 18
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["identity_form"]) ? $context["identity_form"] : $this->getContext($context, "identity_form")), 'form_start');
        echo "

<fieldset>
    <legend>Identité</legend>

    ";
        // line 23
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["identity_form"]) ? $context["identity_form"] : $this->getContext($context, "identity_form")), "identity"), 'rest');
        echo "

    ";
        // line 25
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["identity_form"]) ? $context["identity_form"] : $this->getContext($context, "identity_form")), 'rest');
        echo "

</fieldset>

";
        // line 29
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["identity_form"]) ? $context["identity_form"] : $this->getContext($context, "identity_form")), 'form_end');
        echo "


";
        // line 33
        echo "
";
        // line 34
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["user_form"]) ? $context["user_form"] : $this->getContext($context, "user_form")), 'form_start');
        echo "

<fieldset>
    <legend>Rattachement</legend>

    ";
        // line 39
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["user_form"]) ? $context["user_form"] : $this->getContext($context, "user_form")), "user"), 'rest');
        echo "

    ";
        // line 41
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["user_form"]) ? $context["user_form"] : $this->getContext($context, "user_form")), 'rest');
        echo "

</fieldset>

";
        // line 45
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["user_form"]) ? $context["user_form"] : $this->getContext($context, "user_form")), 'form_end');
        echo "


";
        // line 49
        echo "

";
        // line 51
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["userinfo_form"]) ? $context["userinfo_form"] : $this->getContext($context, "userinfo_form")), 'form_start');
        echo "

<fieldset>
    <legend>Situation professionnelle</legend>

    ";
        // line 56
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["userinfo_form"]) ? $context["userinfo_form"] : $this->getContext($context, "userinfo_form")), "userinfo"), 'rest');
        echo "

    ";
        // line 58
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["userinfo_form"]) ? $context["userinfo_form"] : $this->getContext($context, "userinfo_form")), 'rest');
        echo "

</fieldset>

";
        // line 62
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["user_form"]) ? $context["user_form"] : $this->getContext($context, "user_form")), 'form_end');
        echo "

";
        // line 65
        echo "
";
    }

    public function getTemplateName()
    {
        return "LaplaceUserBundle:UserProfile:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  178 => 65,  161 => 56,  104 => 22,  102 => 23,  23 => 3,  181 => 63,  153 => 51,  148 => 42,  124 => 31,  97 => 17,  90 => 16,  76 => 6,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 144,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 112,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 105,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 90,  285 => 89,  283 => 88,  278 => 86,  268 => 85,  264 => 84,  258 => 81,  252 => 80,  247 => 78,  241 => 77,  235 => 74,  229 => 73,  224 => 71,  220 => 70,  214 => 69,  208 => 68,  169 => 60,  143 => 45,  140 => 55,  132 => 51,  128 => 49,  119 => 42,  107 => 25,  71 => 17,  177 => 65,  165 => 64,  160 => 61,  135 => 47,  126 => 45,  114 => 29,  84 => 13,  70 => 6,  67 => 15,  61 => 13,  28 => 3,  87 => 14,  201 => 92,  196 => 90,  183 => 70,  171 => 56,  166 => 58,  163 => 70,  158 => 67,  156 => 58,  151 => 57,  142 => 59,  138 => 38,  136 => 41,  121 => 46,  117 => 44,  105 => 40,  91 => 31,  62 => 23,  49 => 19,  31 => 5,  21 => 6,  25 => 4,  93 => 28,  88 => 10,  78 => 11,  44 => 12,  94 => 18,  89 => 13,  85 => 9,  75 => 23,  68 => 14,  56 => 9,  27 => 4,  38 => 6,  24 => 7,  46 => 7,  26 => 6,  19 => 2,  79 => 8,  72 => 16,  69 => 9,  47 => 9,  40 => 8,  37 => 10,  22 => 2,  246 => 32,  157 => 56,  145 => 46,  139 => 50,  131 => 39,  123 => 34,  120 => 33,  115 => 43,  111 => 24,  108 => 37,  101 => 32,  98 => 31,  96 => 31,  83 => 25,  74 => 14,  66 => 15,  55 => 15,  52 => 21,  50 => 10,  43 => 6,  41 => 5,  35 => 5,  32 => 4,  29 => 3,  209 => 82,  203 => 78,  199 => 67,  193 => 73,  189 => 71,  187 => 84,  182 => 66,  176 => 64,  173 => 62,  168 => 66,  164 => 52,  162 => 62,  154 => 54,  149 => 49,  147 => 58,  144 => 53,  141 => 51,  133 => 36,  130 => 35,  125 => 44,  122 => 30,  116 => 36,  112 => 42,  109 => 41,  106 => 36,  103 => 19,  99 => 19,  95 => 34,  92 => 14,  86 => 12,  82 => 8,  80 => 19,  73 => 19,  64 => 14,  60 => 6,  57 => 11,  54 => 10,  51 => 14,  48 => 8,  45 => 17,  42 => 7,  39 => 9,  36 => 5,  33 => 4,  30 => 7,);
    }
}
