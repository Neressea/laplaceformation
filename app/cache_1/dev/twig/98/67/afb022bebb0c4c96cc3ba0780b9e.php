<?php

/* ::layout.html.twig */
class __TwigTemplate_9867afb022bebb0c4c96cc3ba0780b9e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'PageTitle' => array($this, 'block_PageTitle'),
            'StyleSheets' => array($this, 'block_StyleSheets'),
            'NavbarOuter' => array($this, 'block_NavbarOuter'),
            'NavbarLeft' => array($this, 'block_NavbarLeft'),
            'NavbarRight' => array($this, 'block_NavbarRight'),
            'MainContainerOuter' => array($this, 'block_MainContainerOuter'),
            'MainContainer' => array($this, 'block_MainContainer'),
            'SidebarOuter' => array($this, 'block_SidebarOuter'),
            'Sidebar' => array($this, 'block_Sidebar'),
            'ContentOuter' => array($this, 'block_ContentOuter'),
            'Content' => array($this, 'block_Content'),
            'ContentTitle' => array($this, 'block_ContentTitle'),
            'ContentBody' => array($this, 'block_ContentBody'),
            'JavaScripts' => array($this, 'block_JavaScripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "<!DOCTYPE html>
<html>
    <head>
        <title>";
        // line 7
        $this->displayBlock('PageTitle', $context, $blocks);
        echo "</title>
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />

        <link href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bootstrap/css/cerulean.bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" media=\"screen\" />
        <link href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" media=\"screen\" />

        ";
        // line 13
        $this->displayBlock('StyleSheets', $context, $blocks);
        // line 14
        echo "
        <link href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bootstrap/css/bootstrap-responsive.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" media=\"screen\" />

        <script src=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery-1.10.1.min.js"), "html", null, true);
        echo "\"></script>
    </head>
    <body>
        <!-- NAVBAR -->
        ";
        // line 21
        $this->displayBlock('NavbarOuter', $context, $blocks);
        // line 52
        echo "
        ";
        // line 53
        $this->displayBlock('MainContainerOuter', $context, $blocks);
        // line 112
        echo "
        <script src=\"";
        // line 113
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bootstrap/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 114
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/user-selector-typeahead.js"), "html", null, true);
        echo "\"></script>

        <input type=\"hidden\" id=\"api_find_users\" data-url=\"";
        // line 116
        echo $this->env->getExtension('routing')->getPath("laplace_user_api_find_users");
        echo "\" />

        ";
        // line 118
        $this->displayBlock('JavaScripts', $context, $blocks);
        // line 119
        echo "    </body>
</html>
";
    }

    // line 7
    public function block_PageTitle($context, array $blocks = array())
    {
        echo "Laplace";
    }

    // line 13
    public function block_StyleSheets($context, array $blocks = array())
    {
    }

    // line 21
    public function block_NavbarOuter($context, array $blocks = array())
    {
        // line 22
        echo "        <div class=\"navbar-wrapper\">
            <div class=\"container\">
                <div class=\"navbar";
        // line 24
        echo ((array_key_exists("inverse_navbar", $context)) ? (" navbar-inverse") : (""));
        echo "\">
                    <div class=\"navbar-inner\">
                        <a class=\"brand\" href=\"";
        // line 26
        echo $this->env->getExtension('routing')->getPath("laplace_common_homepage");
        echo "\">Laplace</a>

                        <ul class=\"nav\">
                        ";
        // line 29
        $this->displayBlock('NavbarLeft', $context, $blocks);
        // line 31
        echo "                        </ul>

                        <ul class=\"nav pull-right\">
                        ";
        // line 34
        $this->displayBlock('NavbarRight', $context, $blocks);
        // line 46
        echo "                        </ul>
                    </div>
                </div>
            </div>
        </div>
        ";
    }

    // line 29
    public function block_NavbarLeft($context, array $blocks = array())
    {
        // line 30
        echo "                        ";
    }

    // line 34
    public function block_NavbarRight($context, array $blocks = array())
    {
        // line 35
        echo "
                            ";
        // line 36
        if ($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user")) {
            // line 37
            echo "                            <li>
                                <a href=\"";
            // line 38
            echo $this->env->getExtension('routing')->getPath("logout");
            echo "\">
                                    <i class=\"icon-off icon-white\"></i>
                                    Déconnexion (";
            // line 40
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user"), "username"), "html", null, true);
            echo ")
                                </a>
                            </li>
                            ";
        }
        // line 44
        echo "
                        ";
    }

    // line 53
    public function block_MainContainerOuter($context, array $blocks = array())
    {
        // line 54
        echo "        <!-- Main Container -->
        <div class=\"container\">

            <div class=\"row\">
                <div class=\"span12\">
                    ";
        // line 59
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session"), "flashbag"), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 60
            echo "                    <div class=\"alert\">
                        <strong>Attention</strong><br />
                        <i class=\"icon-warning-sign\"></i> ";
            // line 62
            echo twig_escape_filter($this->env, (isset($context["message"]) ? $context["message"] : $this->getContext($context, "message")), "html", null, true);
            echo "
                    </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 65
        echo "
                    ";
        // line 66
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session"), "flashbag"), "get", array(0 => "info"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 67
            echo "                    <div class=\"alert alert-success\">
                        <strong>Information</strong><br />
                        <i class=\"icon-ok\"></i> ";
            // line 69
            echo twig_escape_filter($this->env, (isset($context["message"]) ? $context["message"] : $this->getContext($context, "message")), "html", null, true);
            echo "
                    </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 72
        echo "                </div>
            </div>


            <div class=\"row\">
                ";
        // line 77
        $this->displayBlock('MainContainer', $context, $blocks);
        // line 109
        echo "            </div>
        </div>
        ";
    }

    // line 77
    public function block_MainContainer($context, array $blocks = array())
    {
        // line 78
        echo "
                    ";
        // line 79
        $this->displayBlock('SidebarOuter', $context, $blocks);
        // line 90
        echo "
                    ";
        // line 91
        $this->displayBlock('ContentOuter', $context, $blocks);
        // line 107
        echo "
                ";
    }

    // line 79
    public function block_SidebarOuter($context, array $blocks = array())
    {
        // line 80
        echo "                    <!-- SIDEBAR -->
                    <div class=\"span3\">
                        <div class=\"well sidebar-nav\">
                            <ul class=\"nav nav-list\">
                                ";
        // line 84
        $this->displayBlock('Sidebar', $context, $blocks);
        // line 86
        echo "                            </ul>
                        </div>
                    </div>
                    ";
    }

    // line 84
    public function block_Sidebar($context, array $blocks = array())
    {
        // line 85
        echo "                                ";
    }

    // line 91
    public function block_ContentOuter($context, array $blocks = array())
    {
        // line 92
        echo "                    <!-- BODY -->
                    <div class=\"span9\">
                        <div class=\"well\">
                            ";
        // line 95
        $this->displayBlock('Content', $context, $blocks);
        // line 104
        echo "                        </div>
                    </div>
                    ";
    }

    // line 95
    public function block_Content($context, array $blocks = array())
    {
        // line 96
        echo "
                            <h1>";
        // line 97
        $this->displayBlock('ContentTitle', $context, $blocks);
        echo "</h1>

                            <hr />

                            ";
        // line 101
        $this->displayBlock('ContentBody', $context, $blocks);
        // line 102
        echo "
                            ";
    }

    // line 97
    public function block_ContentTitle($context, array $blocks = array())
    {
    }

    // line 101
    public function block_ContentBody($context, array $blocks = array())
    {
    }

    // line 118
    public function block_JavaScripts($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  335 => 118,  330 => 101,  325 => 97,  320 => 102,  318 => 101,  311 => 97,  308 => 96,  299 => 104,  297 => 95,  292 => 92,  289 => 91,  282 => 84,  275 => 86,  273 => 84,  259 => 107,  257 => 91,  249 => 78,  240 => 109,  238 => 77,  231 => 72,  222 => 69,  218 => 67,  211 => 65,  198 => 60,  194 => 59,  184 => 53,  179 => 44,  152 => 30,  77 => 112,  63 => 17,  58 => 15,  53 => 13,  227 => 85,  215 => 79,  210 => 77,  205 => 75,  226 => 84,  219 => 80,  195 => 68,  172 => 40,  202 => 62,  190 => 95,  146 => 69,  113 => 21,  59 => 23,  267 => 80,  261 => 125,  254 => 90,  244 => 118,  228 => 107,  212 => 97,  204 => 74,  197 => 88,  191 => 85,  175 => 72,  167 => 38,  159 => 35,  127 => 60,  118 => 32,  134 => 37,  129 => 41,  100 => 53,  178 => 65,  161 => 82,  104 => 19,  102 => 7,  23 => 3,  181 => 63,  153 => 51,  148 => 42,  124 => 31,  97 => 15,  90 => 13,  76 => 7,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 144,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 112,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 105,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 90,  285 => 85,  283 => 88,  278 => 86,  268 => 85,  264 => 79,  258 => 81,  252 => 79,  247 => 78,  241 => 77,  235 => 111,  229 => 73,  224 => 71,  220 => 81,  214 => 66,  208 => 68,  169 => 60,  143 => 45,  140 => 46,  132 => 51,  128 => 49,  119 => 30,  107 => 25,  71 => 17,  177 => 59,  165 => 83,  160 => 61,  135 => 35,  126 => 34,  114 => 26,  84 => 114,  70 => 21,  67 => 15,  61 => 13,  28 => 3,  87 => 14,  201 => 92,  196 => 69,  183 => 91,  171 => 85,  166 => 58,  163 => 51,  158 => 67,  156 => 34,  151 => 57,  142 => 59,  138 => 34,  136 => 41,  121 => 46,  117 => 35,  105 => 49,  91 => 48,  62 => 23,  49 => 19,  31 => 3,  21 => 6,  25 => 5,  93 => 17,  88 => 15,  78 => 42,  44 => 10,  94 => 118,  89 => 116,  85 => 10,  75 => 53,  68 => 14,  56 => 9,  27 => 4,  38 => 7,  24 => 3,  46 => 7,  26 => 6,  19 => 1,  79 => 8,  72 => 52,  69 => 5,  47 => 17,  40 => 8,  37 => 14,  22 => 2,  246 => 77,  157 => 56,  145 => 46,  139 => 65,  131 => 29,  123 => 34,  120 => 24,  115 => 43,  111 => 25,  108 => 13,  101 => 23,  98 => 52,  96 => 119,  83 => 44,  74 => 14,  66 => 27,  55 => 14,  52 => 19,  50 => 10,  43 => 6,  41 => 7,  35 => 5,  32 => 4,  29 => 3,  209 => 76,  203 => 78,  199 => 89,  193 => 73,  189 => 65,  187 => 54,  182 => 61,  176 => 87,  173 => 62,  168 => 84,  164 => 37,  162 => 36,  154 => 54,  149 => 29,  147 => 58,  144 => 41,  141 => 51,  133 => 31,  130 => 32,  125 => 26,  122 => 37,  116 => 22,  112 => 29,  109 => 57,  106 => 36,  103 => 54,  99 => 24,  95 => 34,  92 => 14,  86 => 45,  82 => 9,  80 => 113,  73 => 19,  64 => 14,  60 => 6,  57 => 11,  54 => 10,  51 => 14,  48 => 11,  45 => 17,  42 => 7,  39 => 9,  36 => 5,  33 => 4,  30 => 7,);
    }
}
