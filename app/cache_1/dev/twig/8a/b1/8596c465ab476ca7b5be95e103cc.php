<?php

/* LaplaceTrainingBundle:Need:view-active-subscriptions.html.twig */
class __TwigTemplate_8ab18596c465ab476ca7b5be95e103cc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
";
        // line 2
        $this->env->loadTemplate("LaplaceTrainingBundle:Need:view-active-subscriptions.html.twig", "659559648")->display(array_merge($context, array("page" => array(0 => "need", 1 => "personal"))));
    }

    public function getTemplateName()
    {
        return "LaplaceTrainingBundle:Need:view-active-subscriptions.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  65 => 28,  234 => 74,  225 => 68,  237 => 74,  230 => 70,  221 => 64,  213 => 58,  207 => 54,  200 => 52,  110 => 25,  34 => 20,  335 => 118,  330 => 101,  325 => 97,  320 => 102,  318 => 101,  311 => 97,  308 => 96,  299 => 104,  297 => 95,  292 => 92,  289 => 91,  282 => 84,  275 => 86,  273 => 84,  259 => 107,  257 => 91,  249 => 78,  240 => 109,  238 => 77,  231 => 72,  222 => 69,  218 => 67,  211 => 65,  198 => 60,  194 => 59,  184 => 46,  179 => 44,  152 => 30,  77 => 34,  63 => 17,  58 => 24,  53 => 21,  227 => 85,  215 => 79,  210 => 77,  205 => 75,  226 => 84,  219 => 80,  195 => 68,  172 => 40,  202 => 62,  190 => 95,  146 => 69,  113 => 22,  59 => 23,  267 => 80,  261 => 125,  254 => 90,  244 => 118,  228 => 107,  212 => 97,  204 => 74,  197 => 88,  191 => 48,  175 => 44,  167 => 42,  159 => 35,  127 => 33,  118 => 24,  134 => 32,  129 => 41,  100 => 45,  178 => 65,  161 => 82,  104 => 24,  102 => 24,  23 => 3,  181 => 63,  153 => 51,  148 => 42,  124 => 56,  97 => 22,  90 => 17,  76 => 7,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 144,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 112,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 105,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 90,  285 => 85,  283 => 88,  278 => 86,  268 => 85,  264 => 79,  258 => 81,  252 => 79,  247 => 78,  241 => 78,  235 => 111,  229 => 73,  224 => 71,  220 => 81,  214 => 66,  208 => 68,  169 => 60,  143 => 35,  140 => 34,  132 => 51,  128 => 29,  119 => 34,  107 => 28,  71 => 30,  177 => 59,  165 => 83,  160 => 40,  135 => 35,  126 => 34,  114 => 51,  84 => 114,  70 => 6,  67 => 15,  61 => 13,  28 => 3,  87 => 14,  201 => 92,  196 => 69,  183 => 91,  171 => 43,  166 => 58,  163 => 51,  158 => 67,  156 => 38,  151 => 57,  142 => 59,  138 => 34,  136 => 41,  121 => 46,  117 => 35,  105 => 23,  91 => 48,  62 => 27,  49 => 19,  31 => 11,  21 => 6,  25 => 5,  93 => 42,  88 => 16,  78 => 7,  44 => 10,  94 => 118,  89 => 12,  85 => 10,  75 => 53,  68 => 29,  56 => 9,  27 => 18,  38 => 7,  24 => 7,  46 => 18,  26 => 6,  19 => 1,  79 => 8,  72 => 52,  69 => 5,  47 => 17,  40 => 8,  37 => 14,  22 => 2,  246 => 77,  157 => 56,  145 => 46,  139 => 65,  131 => 29,  123 => 34,  120 => 29,  115 => 27,  111 => 25,  108 => 49,  101 => 19,  98 => 22,  96 => 16,  83 => 37,  74 => 14,  66 => 27,  55 => 23,  52 => 19,  50 => 10,  43 => 24,  41 => 7,  35 => 5,  32 => 4,  29 => 19,  209 => 76,  203 => 78,  199 => 89,  193 => 73,  189 => 65,  187 => 54,  182 => 61,  176 => 87,  173 => 62,  168 => 84,  164 => 41,  162 => 36,  154 => 54,  149 => 29,  147 => 36,  144 => 41,  141 => 51,  133 => 31,  130 => 30,  125 => 26,  122 => 25,  116 => 22,  112 => 30,  109 => 28,  106 => 48,  103 => 54,  99 => 21,  95 => 17,  92 => 14,  86 => 12,  82 => 9,  80 => 113,  73 => 19,  64 => 14,  60 => 25,  57 => 11,  54 => 27,  51 => 20,  48 => 11,  45 => 17,  42 => 17,  39 => 16,  36 => 5,  33 => 12,  30 => 7,);
    }
}


/* LaplaceTrainingBundle:Need:view-active-subscriptions.html.twig */
class __TwigTemplate_8ab18596c465ab476ca7b5be95e103cc_659559648 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("LaplaceCommonBundle::user-page.html.twig");

        $this->blocks = array(
            'PageTitle' => array($this, 'block_PageTitle'),
            'StyleSheets' => array($this, 'block_StyleSheets'),
            'ContentTitle' => array($this, 'block_ContentTitle'),
            'ContentBody' => array($this, 'block_ContentBody'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "LaplaceCommonBundle::user-page.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_PageTitle($context, array $blocks = array())
    {
        $this->displayParentBlock("PageTitle", $context, $blocks);
        echo " - Besoins personnels";
    }

    // line 7
    public function block_StyleSheets($context, array $blocks = array())
    {
        // line 8
        $this->displayParentBlock("StyleSheets", $context, $blocks);
        echo "
<style>
    .active-sub-table td,
    .active-sub-table th {
        width: 25%;
    }
</style>
";
    }

    // line 17
    public function block_ContentTitle($context, array $blocks = array())
    {
        echo "Besoins personnels";
    }

    // line 19
    public function block_ContentBody($context, array $blocks = array())
    {
        // line 20
        echo "
<p>
    Cette page regroupe l'ensemble de vos besoins de formation <strong>non pourvus</strong>,
    classés par date d'ajout (du plus récent au moins récent).

    Cliquez sur le titre d'un besoin pour voir sa description complète ou pour
    <strong>indiquer que ce besoin a été pourvu</strong>.
</p>

<br />

<table class=\"table table-striped table-bordered active-sub-table\">
    ";
        // line 32
        if (twig_test_empty((isset($context["subscriptions"]) ? $context["subscriptions"] : $this->getContext($context, "subscriptions")))) {
            // line 33
            echo "    <tr>
        <td>Aucun besoin.</td>
    <tr>
    ";
        }
        // line 37
        echo "

    ";
        // line 39
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["subscriptions"]) ? $context["subscriptions"] : $this->getContext($context, "subscriptions")));
        foreach ($context['_seq'] as $context["_key"] => $context["sub"]) {
            // line 40
            echo "    <tr>
        <td>
            <span class=\"badge badge-info\" title=\"";
            // line 42
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["sub"]) ? $context["sub"] : $this->getContext($context, "sub")), "type"), "description"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["sub"]) ? $context["sub"] : $this->getContext($context, "sub")), "type"), "name"), "html", null, true);
            echo "</span>
            <a href=\"";
            // line 43
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("laplace_training_view_need", array("id" => $this->getAttribute($this->getAttribute((isset($context["sub"]) ? $context["sub"] : $this->getContext($context, "sub")), "need"), "id"))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["sub"]) ? $context["sub"] : $this->getContext($context, "sub")), "need"), "title"), "html", null, true);
            echo "</a>
            <span class=\"pull-right text-right\">Ajouté le <strong>";
            // line 44
            echo twig_escape_filter($this->env, twig_localized_date_filter($this->env, $this->getAttribute((isset($context["sub"]) ? $context["sub"] : $this->getContext($context, "sub")), "subscriptionDate"), "long", "short"), "html", null, true);
            echo "</strong></span>
            <br />
            ";
            // line 46
            if ($this->getAttribute($this->getAttribute((isset($context["sub"]) ? $context["sub"] : $this->getContext($context, "sub")), "need"), "category")) {
                // line 47
                echo "            <small>";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["sub"]) ? $context["sub"] : $this->getContext($context, "sub")), "need"), "category"), "domain"), "name"), "html", null, true);
                echo " / <small>";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["sub"]) ? $context["sub"] : $this->getContext($context, "sub")), "need"), "category"), "name"), "html", null, true);
                echo "</small></small>
            ";
            } else {
                // line 49
                echo "            <small><em>Aucune catégorie</em></small>
            ";
            }
            // line 51
            echo "        </td>
    </tr>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sub'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 54
        echo "</table>

<p class=\"pull-right text-right\">
    <i class=\"icon-list\"></i>
    <a href=\"";
        // line 58
        echo $this->env->getExtension('routing')->getPath("laplace_training_all_needs");
        echo "\">Voir tous les besoins</a>
</p>

<div class=\"clearfix\"></div>


";
    }

    public function getTemplateName()
    {
        return "LaplaceTrainingBundle:Need:view-active-subscriptions.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  180 => 58,  174 => 54,  65 => 28,  234 => 74,  225 => 68,  237 => 74,  230 => 70,  221 => 64,  213 => 58,  207 => 54,  200 => 52,  110 => 25,  34 => 20,  335 => 118,  330 => 101,  325 => 97,  320 => 102,  318 => 101,  311 => 97,  308 => 96,  299 => 104,  297 => 95,  292 => 92,  289 => 91,  282 => 84,  275 => 86,  273 => 84,  259 => 107,  257 => 91,  249 => 78,  240 => 109,  238 => 77,  231 => 72,  222 => 69,  218 => 67,  211 => 65,  198 => 60,  194 => 59,  184 => 46,  179 => 44,  152 => 46,  77 => 7,  63 => 17,  58 => 24,  53 => 21,  227 => 85,  215 => 79,  210 => 77,  205 => 75,  226 => 84,  219 => 80,  195 => 68,  172 => 40,  202 => 62,  190 => 95,  146 => 69,  113 => 22,  59 => 23,  267 => 80,  261 => 125,  254 => 90,  244 => 118,  228 => 107,  212 => 97,  204 => 74,  197 => 88,  191 => 48,  175 => 44,  167 => 42,  159 => 35,  127 => 39,  118 => 24,  134 => 32,  129 => 41,  100 => 45,  178 => 65,  161 => 82,  104 => 24,  102 => 24,  23 => 3,  181 => 63,  153 => 51,  148 => 42,  124 => 56,  97 => 22,  90 => 17,  76 => 7,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 144,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 112,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 105,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 90,  285 => 85,  283 => 88,  278 => 86,  268 => 85,  264 => 79,  258 => 81,  252 => 79,  247 => 78,  241 => 78,  235 => 111,  229 => 73,  224 => 71,  220 => 81,  214 => 66,  208 => 68,  169 => 60,  143 => 35,  140 => 34,  132 => 51,  128 => 29,  119 => 34,  107 => 28,  71 => 30,  177 => 59,  165 => 83,  160 => 40,  135 => 42,  126 => 34,  114 => 51,  84 => 114,  70 => 5,  67 => 15,  61 => 13,  28 => 3,  87 => 14,  201 => 92,  196 => 69,  183 => 91,  171 => 43,  166 => 51,  163 => 51,  158 => 67,  156 => 38,  151 => 57,  142 => 59,  138 => 34,  136 => 41,  121 => 46,  117 => 33,  105 => 23,  91 => 48,  62 => 27,  49 => 19,  31 => 11,  21 => 6,  25 => 5,  93 => 42,  88 => 16,  78 => 7,  44 => 10,  94 => 118,  89 => 12,  85 => 10,  75 => 53,  68 => 29,  56 => 9,  27 => 18,  38 => 7,  24 => 7,  46 => 18,  26 => 6,  19 => 1,  79 => 8,  72 => 52,  69 => 5,  47 => 17,  40 => 8,  37 => 14,  22 => 2,  246 => 77,  157 => 56,  145 => 46,  139 => 65,  131 => 40,  123 => 37,  120 => 29,  115 => 32,  111 => 25,  108 => 49,  101 => 20,  98 => 19,  96 => 16,  83 => 37,  74 => 14,  66 => 27,  55 => 23,  52 => 19,  50 => 10,  43 => 24,  41 => 7,  35 => 5,  32 => 4,  29 => 19,  209 => 76,  203 => 78,  199 => 89,  193 => 73,  189 => 65,  187 => 54,  182 => 61,  176 => 87,  173 => 62,  168 => 84,  164 => 41,  162 => 49,  154 => 47,  149 => 29,  147 => 44,  144 => 41,  141 => 43,  133 => 31,  130 => 30,  125 => 26,  122 => 25,  116 => 22,  112 => 30,  109 => 28,  106 => 48,  103 => 54,  99 => 21,  95 => 17,  92 => 17,  86 => 12,  82 => 9,  80 => 8,  73 => 19,  64 => 14,  60 => 25,  57 => 11,  54 => 27,  51 => 20,  48 => 11,  45 => 17,  42 => 17,  39 => 16,  36 => 5,  33 => 12,  30 => 7,);
    }
}
