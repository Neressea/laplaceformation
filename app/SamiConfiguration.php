<?php

use Sami\Sami;
use Sami\Parser\Filter\TrueFilter;
use Symfony\Component\Finder\Finder;

$iterator = Finder::create()
    ->files()
    ->name('*.php')
    ->exclude('Resources')
    ->exclude('Tests')
    ->exclude('DependencyInjection')
    ->in(__DIR__.'/../src')
;

$sami = new Sami($iterator, array(
    'theme'                 => 'enhanced',
    'title'                 => 'Formation Laplace',
    'build_dir'             => __DIR__.'/../web/doc/',
    'cache_dir'             => __DIR__.'/cache/laplace-formation/doc/',
    'default_opened_level'  => 2,
));

$sami['filter'] = $sami->share(function () {
    return new TrueFilter();
});

return $sami;
