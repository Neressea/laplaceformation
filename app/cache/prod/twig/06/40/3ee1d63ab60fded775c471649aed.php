<?php

/* LaplaceUserBundle:UserProfile:view-all.html.twig */
class __TwigTemplate_06403ee1d63ab60fded775c471649aed extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "

";
        // line 3
        $this->env->loadTemplate("LaplaceUserBundle:UserProfile:view-all.html.twig", "90623275")->display(array_merge($context, array("page" => array(0 => "profile", 1 => "all"))));
    }

    public function getTemplateName()
    {
        return "LaplaceUserBundle:UserProfile:view-all.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  214 => 96,  191 => 87,  185 => 85,  42 => 15,  293 => 127,  286 => 125,  277 => 123,  267 => 119,  255 => 111,  238 => 102,  229 => 97,  220 => 100,  212 => 88,  188 => 72,  161 => 57,  144 => 44,  135 => 39,  126 => 32,  172 => 50,  165 => 47,  151 => 65,  116 => 50,  113 => 49,  153 => 45,  145 => 61,  139 => 60,  127 => 33,  112 => 25,  87 => 13,  94 => 16,  91 => 15,  23 => 3,  170 => 62,  125 => 31,  103 => 19,  82 => 9,  549 => 162,  543 => 161,  538 => 158,  530 => 155,  526 => 153,  522 => 151,  512 => 149,  505 => 148,  502 => 147,  497 => 146,  491 => 144,  488 => 143,  483 => 142,  473 => 134,  469 => 132,  466 => 131,  460 => 130,  455 => 129,  450 => 126,  444 => 122,  441 => 121,  437 => 120,  434 => 119,  429 => 116,  423 => 112,  420 => 111,  416 => 110,  413 => 109,  408 => 106,  394 => 105,  390 => 103,  375 => 101,  365 => 99,  362 => 98,  359 => 97,  355 => 95,  348 => 91,  344 => 90,  330 => 89,  327 => 88,  321 => 86,  307 => 85,  302 => 84,  295 => 81,  287 => 80,  279 => 78,  271 => 77,  264 => 118,  256 => 73,  251 => 71,  247 => 107,  239 => 69,  231 => 68,  219 => 67,  201 => 66,  147 => 51,  143 => 49,  134 => 36,  131 => 34,  122 => 52,  102 => 19,  92 => 25,  84 => 32,  72 => 27,  48 => 7,  35 => 5,  29 => 3,  76 => 7,  69 => 5,  54 => 14,  51 => 8,  31 => 7,  205 => 85,  199 => 91,  190 => 66,  182 => 84,  179 => 67,  175 => 58,  168 => 57,  164 => 51,  156 => 68,  148 => 47,  138 => 44,  123 => 30,  117 => 28,  108 => 22,  83 => 24,  71 => 10,  64 => 16,  110 => 20,  89 => 12,  65 => 14,  63 => 23,  58 => 10,  34 => 5,  227 => 92,  224 => 91,  221 => 90,  207 => 82,  197 => 74,  195 => 65,  192 => 72,  189 => 63,  186 => 60,  181 => 67,  178 => 83,  173 => 82,  162 => 58,  158 => 69,  155 => 55,  152 => 42,  142 => 47,  136 => 44,  133 => 35,  130 => 56,  120 => 29,  105 => 23,  100 => 17,  75 => 20,  53 => 19,  39 => 7,  98 => 33,  80 => 11,  78 => 28,  46 => 16,  44 => 9,  36 => 6,  32 => 4,  60 => 21,  57 => 9,  40 => 6,  114 => 25,  109 => 48,  106 => 23,  101 => 20,  88 => 34,  85 => 10,  77 => 12,  67 => 9,  47 => 6,  28 => 4,  25 => 7,  55 => 19,  43 => 7,  38 => 6,  26 => 8,  24 => 3,  50 => 7,  27 => 2,  22 => 2,  19 => 1,  232 => 82,  226 => 78,  222 => 76,  215 => 89,  211 => 84,  208 => 95,  202 => 68,  196 => 64,  193 => 65,  187 => 62,  183 => 60,  180 => 59,  171 => 54,  166 => 51,  163 => 50,  160 => 49,  157 => 43,  149 => 42,  146 => 40,  140 => 38,  137 => 37,  129 => 36,  124 => 35,  121 => 24,  118 => 29,  115 => 39,  111 => 26,  107 => 28,  104 => 47,  97 => 17,  93 => 16,  90 => 35,  81 => 12,  70 => 9,  66 => 13,  62 => 11,  59 => 15,  56 => 12,  52 => 10,  49 => 17,  45 => 6,  41 => 9,  37 => 14,  33 => 4,  30 => 3,);
    }
}


/* LaplaceUserBundle:UserProfile:view-all.html.twig */
class __TwigTemplate_06403ee1d63ab60fded775c471649aed_90623275 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("LaplaceCommonBundle::admin-page.html.twig");

        $this->blocks = array(
            'PageTitle' => array($this, 'block_PageTitle'),
            'ContentTitle' => array($this, 'block_ContentTitle'),
            'ContentBody' => array($this, 'block_ContentBody'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "LaplaceCommonBundle::admin-page.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 6
    public function block_PageTitle($context, array $blocks = array())
    {
        $this->displayParentBlock("PageTitle", $context, $blocks);
        echo " - Tous les profils (";
        if (isset($context["requested"])) { $_requested_ = $context["requested"]; } else { $_requested_ = null; }
        echo twig_escape_filter($this->env, $_requested_, "html", null, true);
        echo ")";
    }

    // line 8
    public function block_ContentTitle($context, array $blocks = array())
    {
        echo "Tous les profils de l'année ";
        if (isset($context["requested"])) { $_requested_ = $context["requested"]; } else { $_requested_ = null; }
        echo twig_escape_filter($this->env, $_requested_, "html", null, true);
    }

    // line 12
    public function block_ContentBody($context, array $blocks = array())
    {
        // line 13
        echo "
";
        // line 14
        if (isset($context["laplace_user"])) { $_laplace_user_ = $context["laplace_user"]; } else { $_laplace_user_ = null; }
        $this->env->loadTemplate("::pagination.html.twig")->display(array_merge($context, array("route_name" => ($_laplace_user_ . "all_profiles"), "route_args" => array())));
        // line 16
        echo "
";
        // line 18
        $this->env->loadTemplate("LaplaceUserBundle:UserProfile:table.html.twig")->display(array_merge($context, array("show_role_admin" => true, "show_role_user" => true, "show_role_limited" => true)));
        // line 24
        echo "
";
        // line 25
        if (isset($context["laplace_user"])) { $_laplace_user_ = $context["laplace_user"]; } else { $_laplace_user_ = null; }
        $this->env->loadTemplate("::pagination.html.twig")->display(array_merge($context, array("route_name" => ($_laplace_user_ . "all_profiles"), "route_args" => array())));
        // line 27
        echo "

";
        // line 29
        if (isset($context["search_form"])) { $_search_form_ = $context["search_form"]; } else { $_search_form_ = null; }
        echo         $this->env->getExtension('form')->renderer->renderBlock($_search_form_, 'form_start');
        echo "

<fieldset>

    <legend>Rechercher un utilisateur</legend>

    ";
        // line 35
        if (isset($context["search_form"])) { $_search_form_ = $context["search_form"]; } else { $_search_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_search_form_, "user"), 'rest');
        echo "

    ";
        // line 37
        if (isset($context["search_form"])) { $_search_form_ = $context["search_form"]; } else { $_search_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($_search_form_, 'rest');
        echo "

</fieldset>

";
        // line 41
        if (isset($context["search_form"])) { $_search_form_ = $context["search_form"]; } else { $_search_form_ = null; }
        echo         $this->env->getExtension('form')->renderer->renderBlock($_search_form_, 'form_end');
        echo "

";
    }

    public function getTemplateName()
    {
        return "LaplaceUserBundle:UserProfile:view-all.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  128 => 37,  214 => 96,  191 => 87,  185 => 85,  42 => 15,  293 => 127,  286 => 125,  277 => 123,  267 => 119,  255 => 111,  238 => 102,  229 => 97,  220 => 100,  212 => 88,  188 => 72,  161 => 57,  144 => 44,  135 => 39,  126 => 32,  172 => 50,  165 => 47,  151 => 65,  116 => 50,  113 => 49,  153 => 45,  145 => 61,  139 => 60,  127 => 33,  112 => 29,  87 => 13,  94 => 14,  91 => 13,  23 => 3,  170 => 62,  125 => 31,  103 => 19,  82 => 9,  549 => 162,  543 => 161,  538 => 158,  530 => 155,  526 => 153,  522 => 151,  512 => 149,  505 => 148,  502 => 147,  497 => 146,  491 => 144,  488 => 143,  483 => 142,  473 => 134,  469 => 132,  466 => 131,  460 => 130,  455 => 129,  450 => 126,  444 => 122,  441 => 121,  437 => 120,  434 => 119,  429 => 116,  423 => 112,  420 => 111,  416 => 110,  413 => 109,  408 => 106,  394 => 105,  390 => 103,  375 => 101,  365 => 99,  362 => 98,  359 => 97,  355 => 95,  348 => 91,  344 => 90,  330 => 89,  327 => 88,  321 => 86,  307 => 85,  302 => 84,  295 => 81,  287 => 80,  279 => 78,  271 => 77,  264 => 118,  256 => 73,  251 => 71,  247 => 107,  239 => 69,  231 => 68,  219 => 67,  201 => 66,  147 => 51,  143 => 49,  134 => 36,  131 => 34,  122 => 35,  102 => 24,  92 => 25,  84 => 32,  72 => 27,  48 => 7,  35 => 5,  29 => 3,  76 => 7,  69 => 5,  54 => 14,  51 => 8,  31 => 7,  205 => 85,  199 => 91,  190 => 66,  182 => 84,  179 => 67,  175 => 58,  168 => 57,  164 => 51,  156 => 68,  148 => 47,  138 => 44,  123 => 30,  117 => 28,  108 => 27,  83 => 24,  71 => 10,  64 => 16,  110 => 20,  89 => 12,  65 => 14,  63 => 23,  58 => 10,  34 => 5,  227 => 92,  224 => 91,  221 => 90,  207 => 82,  197 => 74,  195 => 65,  192 => 72,  189 => 63,  186 => 60,  181 => 67,  178 => 83,  173 => 82,  162 => 58,  158 => 69,  155 => 55,  152 => 42,  142 => 47,  136 => 41,  133 => 35,  130 => 56,  120 => 29,  105 => 25,  100 => 18,  75 => 20,  53 => 19,  39 => 7,  98 => 33,  80 => 8,  78 => 28,  46 => 16,  44 => 9,  36 => 6,  32 => 4,  60 => 21,  57 => 9,  40 => 6,  114 => 25,  109 => 48,  106 => 23,  101 => 20,  88 => 12,  85 => 10,  77 => 12,  67 => 9,  47 => 6,  28 => 4,  25 => 7,  55 => 19,  43 => 7,  38 => 6,  26 => 8,  24 => 3,  50 => 7,  27 => 2,  22 => 2,  19 => 1,  232 => 82,  226 => 78,  222 => 76,  215 => 89,  211 => 84,  208 => 95,  202 => 68,  196 => 64,  193 => 65,  187 => 62,  183 => 60,  180 => 59,  171 => 54,  166 => 51,  163 => 50,  160 => 49,  157 => 43,  149 => 42,  146 => 40,  140 => 38,  137 => 37,  129 => 36,  124 => 35,  121 => 24,  118 => 29,  115 => 39,  111 => 26,  107 => 28,  104 => 47,  97 => 16,  93 => 16,  90 => 35,  81 => 12,  70 => 6,  66 => 13,  62 => 11,  59 => 15,  56 => 12,  52 => 10,  49 => 17,  45 => 6,  41 => 9,  37 => 14,  33 => 4,  30 => 3,);
    }
}
