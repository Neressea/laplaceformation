<?php

/* LaplaceUserBundle:UserProfile:view.html.twig */
class __TwigTemplate_7bbe8b682aa927dd61148ec8bae86f01 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        if (isset($context["admin"])) { $_admin_ = $context["admin"]; } else { $_admin_ = null; }
        $context["container"] = (($_admin_) ? ("LaplaceCommonBundle::admin-page.html.twig") : ("LaplaceCommonBundle::user-page.html.twig"));
        // line 6
        echo "

";
        // line 8
        $this->env->loadTemplate("LaplaceUserBundle:UserProfile:view.html.twig", "400409669")->display(array_merge($context, array("page" => array(0 => "profile", 1 => "view"))));
    }

    public function getTemplateName()
    {
        return "LaplaceUserBundle:UserProfile:view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  172 => 50,  165 => 47,  151 => 41,  116 => 26,  113 => 25,  153 => 45,  145 => 39,  139 => 37,  127 => 33,  112 => 25,  87 => 13,  94 => 14,  91 => 13,  23 => 3,  170 => 52,  125 => 31,  103 => 19,  82 => 9,  549 => 162,  543 => 161,  538 => 158,  530 => 155,  526 => 153,  522 => 151,  512 => 149,  505 => 148,  502 => 147,  497 => 146,  491 => 144,  488 => 143,  483 => 142,  473 => 134,  469 => 132,  466 => 131,  460 => 130,  455 => 129,  450 => 126,  444 => 122,  441 => 121,  437 => 120,  434 => 119,  429 => 116,  423 => 112,  420 => 111,  416 => 110,  413 => 109,  408 => 106,  394 => 105,  390 => 103,  375 => 101,  365 => 99,  362 => 98,  359 => 97,  355 => 95,  348 => 91,  344 => 90,  330 => 89,  327 => 88,  321 => 86,  307 => 85,  302 => 84,  295 => 81,  287 => 80,  279 => 78,  271 => 77,  264 => 74,  256 => 73,  251 => 71,  247 => 70,  239 => 69,  231 => 68,  219 => 67,  201 => 66,  147 => 51,  143 => 49,  134 => 36,  131 => 34,  122 => 37,  102 => 19,  92 => 25,  84 => 21,  72 => 15,  48 => 7,  35 => 5,  29 => 3,  76 => 7,  69 => 5,  54 => 14,  51 => 8,  31 => 7,  205 => 70,  199 => 69,  190 => 66,  182 => 62,  179 => 58,  175 => 58,  168 => 57,  164 => 51,  156 => 51,  148 => 47,  138 => 44,  123 => 30,  117 => 28,  108 => 22,  83 => 24,  71 => 19,  64 => 16,  110 => 20,  89 => 12,  65 => 14,  63 => 13,  58 => 10,  34 => 5,  227 => 92,  224 => 91,  221 => 90,  207 => 82,  197 => 74,  195 => 65,  192 => 72,  189 => 63,  186 => 60,  181 => 67,  178 => 56,  173 => 56,  162 => 58,  158 => 44,  155 => 55,  152 => 42,  142 => 47,  136 => 44,  133 => 35,  130 => 34,  120 => 29,  105 => 20,  100 => 17,  75 => 20,  53 => 19,  39 => 7,  98 => 33,  80 => 11,  78 => 25,  46 => 12,  44 => 9,  36 => 6,  32 => 4,  60 => 21,  57 => 9,  40 => 6,  114 => 25,  109 => 21,  106 => 23,  101 => 20,  88 => 11,  85 => 10,  77 => 12,  67 => 9,  47 => 6,  28 => 4,  25 => 7,  55 => 15,  43 => 7,  38 => 6,  26 => 8,  24 => 4,  50 => 7,  27 => 2,  22 => 6,  19 => 2,  232 => 82,  226 => 78,  222 => 76,  215 => 73,  211 => 84,  208 => 70,  202 => 68,  196 => 64,  193 => 65,  187 => 62,  183 => 60,  180 => 59,  171 => 54,  166 => 51,  163 => 50,  160 => 49,  157 => 43,  149 => 42,  146 => 40,  140 => 38,  137 => 37,  129 => 36,  124 => 35,  121 => 24,  118 => 33,  115 => 39,  111 => 26,  107 => 28,  104 => 20,  97 => 17,  93 => 16,  90 => 12,  81 => 14,  70 => 9,  66 => 13,  62 => 11,  59 => 15,  56 => 12,  52 => 10,  49 => 9,  45 => 6,  41 => 9,  37 => 5,  33 => 4,  30 => 3,);
    }
}


/* LaplaceUserBundle:UserProfile:view.html.twig */
class __TwigTemplate_7bbe8b682aa927dd61148ec8bae86f01_400409669 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'PageTitle' => array($this, 'block_PageTitle'),
            'ContentTitle' => array($this, 'block_ContentTitle'),
            'ContentBody' => array($this, 'block_ContentBody'),
        );
    }

    protected function doGetParent(array $context)
    {
        return $this->env->resolveTemplate($this->getContext($context, "container"));
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 10
    public function block_PageTitle($context, array $blocks = array())
    {
        $this->displayParentBlock("PageTitle", $context, $blocks);
        echo " - Voir le profil (";
        if (isset($context["profile"])) { $_profile_ = $context["profile"]; } else { $_profile_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_profile_, "fullName"), "html", null, true);
        echo ")";
    }

    // line 12
    public function block_ContentTitle($context, array $blocks = array())
    {
        if (isset($context["profile"])) { $_profile_ = $context["profile"]; } else { $_profile_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_profile_, "fullName"), "html", null, true);
    }

    // line 14
    public function block_ContentBody($context, array $blocks = array())
    {
        // line 15
        echo "
";
        // line 16
        if (isset($context["admin"])) { $_admin_ = $context["admin"]; } else { $_admin_ = null; }
        if ((!$_admin_)) {
            // line 17
            echo "<p>
    Les informations ci-dessous sont strictement réservées à l'utilisateur et
    au correspondant formation, qui peuvent les modifier. Elles ne sont pas
    accessibles aux autres utilisateurs.
</p>
";
        } else {
            // line 23
            echo "<p class=\"text-right\">
    <i class=\"icon-edit\"></i>
    <a href=\"";
            // line 25
            if (isset($context["laplace_user"])) { $_laplace_user_ = $context["laplace_user"]; } else { $_laplace_user_ = null; }
            if (isset($context["profile"])) { $_profile_ = $context["profile"]; } else { $_profile_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath(($_laplace_user_ . "edit_profile"), array("username" => $this->getAttribute($_profile_, "username"))), "html", null, true);
            echo "\">Modifier</a>
    <br />

    <i class=\"icon-trash\"></i>
    <a href=\"";
            // line 29
            if (isset($context["laplace_user"])) { $_laplace_user_ = $context["laplace_user"]; } else { $_laplace_user_ = null; }
            if (isset($context["profile"])) { $_profile_ = $context["profile"]; } else { $_profile_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath(($_laplace_user_ . "delete_profile"), array("username" => $this->getAttribute($_profile_, "username"))), "html", null, true);
            echo "\">Supprimer</a>
</p>
";
        }
        // line 32
        echo "
<h3>Identité</h3>

<table class=\"table table-striped table-bordered user-profile-table\">

    <tr>
        <th>Login</th>
        <td>";
        // line 39
        if (isset($context["profile"])) { $_profile_ = $context["profile"]; } else { $_profile_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_profile_, "username"), "html", null, true);
        echo "</td>
    </tr>

    <tr>
        <th>Adresse email</th>
        <td>";
        // line 44
        if (isset($context["profile"])) { $_profile_ = $context["profile"]; } else { $_profile_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_profile_, "emailAddress"), "html", null, true);
        echo "</td>
    </tr>

</table>

<hr />

<h3>Rattachement</h3>

<table class=\"table table-striped table-bordered user-profile-table\">

    <tr>
        <th>Employeur</th>
        <td>";
        // line 57
        if (isset($context["profile"])) { $_profile_ = $context["profile"]; } else { $_profile_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_profile_, "tutelle"), "nom"), "html", null, true);
        echo "</td>
    </tr>

    <tr>
        <th>Site</th>
        <td>";
        // line 62
        if (isset($context["profile"])) { $_profile_ = $context["profile"]; } else { $_profile_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_profile_, "site"), "name"), "html", null, true);
        echo "</td>
    </tr>

    <tr>
        <th>Téléphone professionnel 1</th>
        <td>";
        // line 67
        if (isset($context["profile"])) { $_profile_ = $context["profile"]; } else { $_profile_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_profile_, "telephoneNumber1"), "html", null, true);
        echo "</td>
    </tr>

    <tr>
        <th>Téléphone professionnel 2</th>
        <td>";
        // line 72
        if (isset($context["profile"])) { $_profile_ = $context["profile"]; } else { $_profile_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_profile_, "telephoneNumber2"), "html", null, true);
        echo "</td>
    </tr>

</table>

<hr />

<h3>Situation professionnelle</h3>

<table class=\"table table-striped table-bordered user-profile-table\">

    <tr>
        <th>Statut</th>
        <td>";
        // line 85
        if (isset($context["profile"])) { $_profile_ = $context["profile"]; } else { $_profile_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($_profile_, "userInfo"), "statut"), "intitule"), "html", null, true);
        echo "</td>
    </tr>

";
        // line 88
        if (isset($context["profile"])) { $_profile_ = $context["profile"]; } else { $_profile_ = null; }
        if ($this->getAttribute($_profile_, "estDoctorant", array(), "method")) {
            // line 89
            echo "
    <tr>
        <th>Début de thèse</th>
        <td>";
            // line 92
            if (isset($context["profile"])) { $_profile_ = $context["profile"]; } else { $_profile_ = null; }
            echo twig_escape_filter($this->env, twig_localized_date_filter($this->env, $this->getAttribute($this->getAttribute($_profile_, "userInfo"), "debutThese"), "short", "none"), "html", null, true);
            echo "</td>
    </tr>

    <tr>
        <th>Responsable</th>
        <td>";
            // line 97
            if (isset($context["profile"])) { $_profile_ = $context["profile"]; } else { $_profile_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_profile_, "userInfo"), "responsable"), "html", null, true);
            echo "</td>
    </tr>

    <tr>
        <th>Ecole doctorale</th>
        <td>";
            // line 102
            if (isset($context["profile"])) { $_profile_ = $context["profile"]; } else { $_profile_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_profile_, "userInfo"), "ecoleDoctorale"), "html", null, true);
            echo "</td>
    </tr>

    <tr>
        <th>Bourse</th>
        <td>";
            // line 107
            if (isset($context["profile"])) { $_profile_ = $context["profile"]; } else { $_profile_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_profile_, "userInfo"), "bourse"), "html", null, true);
            echo "</td>
    </tr>

";
        }
        // line 111
        echo "
</table>

<hr />

<p class=\"text-right\">
    <i class=\"icon-edit\"></i>
    ";
        // line 118
        if (isset($context["admin"])) { $_admin_ = $context["admin"]; } else { $_admin_ = null; }
        if ($_admin_) {
            // line 119
            echo "    <a href=\"";
            if (isset($context["laplace_user"])) { $_laplace_user_ = $context["laplace_user"]; } else { $_laplace_user_ = null; }
            if (isset($context["profile"])) { $_profile_ = $context["profile"]; } else { $_profile_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath(($_laplace_user_ . "edit_profile"), array("username" => $this->getAttribute($_profile_, "username"))), "html", null, true);
            echo "\">Modifier ces informations</a>
    <br />

    <i class=\"icon-user\"></i>
    <a href=\"";
            // line 123
            if (isset($context["profile"])) { $_profile_ = $context["profile"]; } else { $_profile_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("laplace_common_user_homepage", array("_switch_user" => $this->getAttribute($_profile_, "username"))), "html", null, true);
            echo "\">Se connecter en tant que ";
            if (isset($context["profile"])) { $_profile_ = $context["profile"]; } else { $_profile_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_profile_, "fullName"), "html", null, true);
            echo "</a>
    ";
        } else {
            // line 125
            echo "    <a href=\"";
            if (isset($context["laplace_user"])) { $_laplace_user_ = $context["laplace_user"]; } else { $_laplace_user_ = null; }
            echo $this->env->getExtension('routing')->getPath(($_laplace_user_ . "edit_my_profile"));
            echo "\">Modifier ces informations</a>
    ";
        }
        // line 127
        echo "</p>

";
    }

    public function getTemplateName()
    {
        return "LaplaceUserBundle:UserProfile:view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  293 => 127,  286 => 125,  277 => 123,  267 => 119,  255 => 111,  238 => 102,  229 => 97,  220 => 92,  212 => 88,  188 => 72,  161 => 57,  144 => 44,  135 => 39,  126 => 32,  172 => 50,  165 => 47,  151 => 41,  116 => 26,  113 => 25,  153 => 45,  145 => 39,  139 => 37,  127 => 33,  112 => 25,  87 => 13,  94 => 16,  91 => 15,  23 => 3,  170 => 62,  125 => 31,  103 => 19,  82 => 9,  549 => 162,  543 => 161,  538 => 158,  530 => 155,  526 => 153,  522 => 151,  512 => 149,  505 => 148,  502 => 147,  497 => 146,  491 => 144,  488 => 143,  483 => 142,  473 => 134,  469 => 132,  466 => 131,  460 => 130,  455 => 129,  450 => 126,  444 => 122,  441 => 121,  437 => 120,  434 => 119,  429 => 116,  423 => 112,  420 => 111,  416 => 110,  413 => 109,  408 => 106,  394 => 105,  390 => 103,  375 => 101,  365 => 99,  362 => 98,  359 => 97,  355 => 95,  348 => 91,  344 => 90,  330 => 89,  327 => 88,  321 => 86,  307 => 85,  302 => 84,  295 => 81,  287 => 80,  279 => 78,  271 => 77,  264 => 118,  256 => 73,  251 => 71,  247 => 107,  239 => 69,  231 => 68,  219 => 67,  201 => 66,  147 => 51,  143 => 49,  134 => 36,  131 => 34,  122 => 37,  102 => 19,  92 => 25,  84 => 21,  72 => 15,  48 => 7,  35 => 5,  29 => 3,  76 => 7,  69 => 5,  54 => 14,  51 => 8,  31 => 7,  205 => 85,  199 => 69,  190 => 66,  182 => 62,  179 => 67,  175 => 58,  168 => 57,  164 => 51,  156 => 51,  148 => 47,  138 => 44,  123 => 30,  117 => 28,  108 => 22,  83 => 24,  71 => 10,  64 => 16,  110 => 20,  89 => 12,  65 => 14,  63 => 13,  58 => 10,  34 => 5,  227 => 92,  224 => 91,  221 => 90,  207 => 82,  197 => 74,  195 => 65,  192 => 72,  189 => 63,  186 => 60,  181 => 67,  178 => 56,  173 => 56,  162 => 58,  158 => 44,  155 => 55,  152 => 42,  142 => 47,  136 => 44,  133 => 35,  130 => 34,  120 => 29,  105 => 23,  100 => 17,  75 => 20,  53 => 19,  39 => 7,  98 => 33,  80 => 11,  78 => 25,  46 => 12,  44 => 9,  36 => 6,  32 => 4,  60 => 21,  57 => 9,  40 => 6,  114 => 25,  109 => 25,  106 => 23,  101 => 20,  88 => 14,  85 => 10,  77 => 12,  67 => 9,  47 => 6,  28 => 4,  25 => 7,  55 => 15,  43 => 7,  38 => 6,  26 => 8,  24 => 4,  50 => 7,  27 => 2,  22 => 6,  19 => 2,  232 => 82,  226 => 78,  222 => 76,  215 => 89,  211 => 84,  208 => 70,  202 => 68,  196 => 64,  193 => 65,  187 => 62,  183 => 60,  180 => 59,  171 => 54,  166 => 51,  163 => 50,  160 => 49,  157 => 43,  149 => 42,  146 => 40,  140 => 38,  137 => 37,  129 => 36,  124 => 35,  121 => 24,  118 => 29,  115 => 39,  111 => 26,  107 => 28,  104 => 20,  97 => 17,  93 => 16,  90 => 12,  81 => 12,  70 => 9,  66 => 13,  62 => 11,  59 => 15,  56 => 12,  52 => 10,  49 => 9,  45 => 6,  41 => 9,  37 => 5,  33 => 4,  30 => 3,);
    }
}
