<?php

/* LaplaceTrainingBundle:Thread:edit.html.twig */
class __TwigTemplate_415b5c9740688462b43a2a1b3586e2b0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
";
        // line 2
        $this->env->loadTemplate("LaplaceTrainingBundle:Thread:edit.html.twig", "444400898")->display(array_merge($context, array("page" => array(0 => "thread", 1 => "edit"))));
    }

    public function getTemplateName()
    {
        return "LaplaceTrainingBundle:Thread:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  306 => 103,  301 => 100,  278 => 90,  266 => 88,  262 => 86,  200 => 59,  141 => 43,  539 => 243,  533 => 240,  525 => 236,  519 => 234,  513 => 232,  504 => 227,  501 => 226,  499 => 225,  496 => 224,  485 => 215,  482 => 214,  476 => 210,  472 => 208,  467 => 204,  464 => 202,  457 => 199,  442 => 192,  439 => 191,  422 => 182,  414 => 178,  402 => 174,  387 => 163,  384 => 162,  382 => 161,  379 => 160,  374 => 156,  367 => 153,  342 => 136,  339 => 135,  337 => 134,  334 => 133,  329 => 129,  322 => 126,  300 => 112,  297 => 111,  292 => 109,  289 => 107,  283 => 103,  280 => 102,  276 => 101,  269 => 98,  261 => 94,  250 => 89,  241 => 75,  184 => 55,  176 => 51,  73 => 29,  99 => 25,  79 => 14,  257 => 85,  249 => 74,  253 => 90,  235 => 64,  198 => 46,  177 => 54,  174 => 41,  169 => 40,  68 => 29,  61 => 27,  341 => 118,  336 => 101,  331 => 97,  326 => 102,  324 => 101,  317 => 97,  314 => 122,  311 => 95,  305 => 104,  303 => 95,  298 => 92,  291 => 85,  288 => 84,  281 => 86,  273 => 89,  270 => 79,  265 => 107,  263 => 91,  260 => 90,  258 => 79,  252 => 77,  244 => 76,  223 => 67,  218 => 64,  159 => 46,  96 => 40,  86 => 15,  246 => 109,  210 => 69,  245 => 85,  237 => 81,  225 => 77,  209 => 69,  194 => 62,  150 => 42,  95 => 15,  128 => 36,  214 => 52,  191 => 87,  185 => 85,  42 => 15,  293 => 94,  286 => 125,  277 => 123,  267 => 119,  255 => 78,  238 => 81,  229 => 79,  220 => 75,  212 => 88,  188 => 60,  161 => 57,  144 => 44,  135 => 33,  126 => 29,  172 => 52,  165 => 38,  151 => 65,  116 => 30,  113 => 28,  153 => 50,  145 => 43,  139 => 60,  127 => 38,  112 => 22,  87 => 16,  94 => 118,  91 => 20,  23 => 3,  170 => 62,  125 => 35,  103 => 19,  82 => 10,  549 => 162,  543 => 161,  538 => 158,  530 => 155,  526 => 153,  522 => 151,  512 => 149,  505 => 148,  502 => 147,  497 => 146,  491 => 144,  488 => 143,  483 => 142,  473 => 134,  469 => 132,  466 => 131,  460 => 130,  455 => 129,  450 => 196,  444 => 122,  441 => 121,  437 => 190,  434 => 189,  429 => 185,  423 => 112,  420 => 111,  416 => 110,  413 => 109,  408 => 176,  394 => 105,  390 => 103,  375 => 101,  365 => 99,  362 => 98,  359 => 149,  355 => 95,  348 => 91,  344 => 90,  330 => 89,  327 => 88,  321 => 86,  307 => 85,  302 => 84,  295 => 110,  287 => 80,  279 => 84,  271 => 77,  264 => 95,  256 => 91,  251 => 71,  247 => 77,  239 => 68,  231 => 79,  219 => 75,  201 => 60,  147 => 34,  143 => 45,  134 => 40,  131 => 29,  122 => 24,  102 => 30,  92 => 17,  84 => 114,  72 => 10,  48 => 18,  35 => 5,  29 => 3,  76 => 8,  69 => 6,  54 => 14,  51 => 19,  31 => 11,  205 => 48,  199 => 71,  190 => 58,  182 => 58,  179 => 67,  175 => 58,  168 => 38,  164 => 48,  156 => 44,  148 => 47,  138 => 34,  123 => 30,  117 => 33,  108 => 32,  83 => 14,  71 => 10,  64 => 20,  110 => 28,  89 => 12,  65 => 14,  63 => 17,  58 => 15,  34 => 12,  227 => 58,  224 => 68,  221 => 54,  207 => 82,  197 => 74,  195 => 58,  192 => 68,  189 => 66,  186 => 53,  181 => 64,  178 => 63,  173 => 53,  162 => 47,  158 => 48,  155 => 36,  152 => 30,  142 => 47,  136 => 43,  133 => 40,  130 => 29,  120 => 26,  105 => 31,  100 => 22,  75 => 53,  53 => 13,  39 => 11,  98 => 16,  80 => 12,  78 => 42,  46 => 16,  44 => 10,  36 => 6,  32 => 8,  60 => 21,  57 => 9,  40 => 10,  114 => 25,  109 => 48,  106 => 20,  101 => 20,  88 => 19,  85 => 11,  77 => 7,  67 => 27,  47 => 24,  28 => 18,  25 => 5,  55 => 14,  43 => 11,  38 => 7,  26 => 8,  24 => 3,  50 => 7,  27 => 2,  22 => 2,  19 => 1,  232 => 79,  226 => 78,  222 => 77,  215 => 63,  211 => 84,  208 => 72,  202 => 66,  196 => 59,  193 => 65,  187 => 44,  183 => 60,  180 => 59,  171 => 58,  166 => 50,  163 => 49,  160 => 49,  157 => 43,  149 => 48,  146 => 44,  140 => 36,  137 => 42,  129 => 36,  124 => 36,  121 => 24,  118 => 33,  115 => 32,  111 => 57,  107 => 27,  104 => 26,  97 => 26,  93 => 19,  90 => 14,  81 => 12,  70 => 5,  66 => 13,  62 => 24,  59 => 17,  56 => 21,  52 => 10,  49 => 17,  45 => 13,  41 => 7,  37 => 9,  33 => 4,  30 => 6,);
    }
}


/* LaplaceTrainingBundle:Thread:edit.html.twig */
class __TwigTemplate_415b5c9740688462b43a2a1b3586e2b0_444400898 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("LaplaceCommonBundle::admin-page.html.twig");

        $this->blocks = array(
            'PageTitle' => array($this, 'block_PageTitle'),
            'ContentTitle' => array($this, 'block_ContentTitle'),
            'ContentBody' => array($this, 'block_ContentBody'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "LaplaceCommonBundle::admin-page.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_PageTitle($context, array $blocks = array())
    {
        $this->displayParentBlock("PageTitle", $context, $blocks);
        echo " - Modifier une discussion";
    }

    // line 7
    public function block_ContentTitle($context, array $blocks = array())
    {
        echo "Modifier une discussion";
    }

    // line 9
    public function block_ContentBody($context, array $blocks = array())
    {
        // line 10
        echo "
";
        // line 11
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo         $this->env->getExtension('form')->renderer->renderBlock($_edit_form_, 'form_start');
        echo "

<fieldset>
    <legend>Modifier une discussion</legend>

    ";
        // line 16
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_edit_form_, "thread"), 'rest');
        echo "

    ";
        // line 18
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($_edit_form_, 'rest');
        echo "

</fieldset>

";
        // line 22
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo         $this->env->getExtension('form')->renderer->renderBlock($_edit_form_, 'form_end');
        echo "



";
        // line 26
        if (isset($context["add_participant_form"])) { $_add_participant_form_ = $context["add_participant_form"]; } else { $_add_participant_form_ = null; }
        echo         $this->env->getExtension('form')->renderer->renderBlock($_add_participant_form_, 'form_start');
        echo "

<fieldset>
    <legend>Ajouter un participant</legend>

    Les personnes suivantes participent déjà à la discussion :
    <ul>
    ";
        // line 33
        if (isset($context["participants"])) { $_participants_ = $context["participants"]; } else { $_participants_ = null; }
        if (twig_test_empty($_participants_)) {
            // line 34
            echo "        <li><em>Aucun participant !</em></li>
    ";
        } else {
            // line 36
            echo "        ";
            if (isset($context["participants"])) { $_participants_ = $context["participants"]; } else { $_participants_ = null; }
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($_participants_);
            foreach ($context['_seq'] as $context["_key"] => $context["participant"]) {
                // line 37
                echo "        <li>";
                if (isset($context["participant"])) { $_participant_ = $context["participant"]; } else { $_participant_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($_participant_, "fullName"), "html", null, true);
                echo "</li>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['participant'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 39
            echo "    ";
        }
        // line 40
        echo "    </ul>

    <br />

    ";
        // line 44
        if (isset($context["add_participant_form"])) { $_add_participant_form_ = $context["add_participant_form"]; } else { $_add_participant_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($_add_participant_form_, 'rest');
        echo "

</fieldset>

";
        // line 48
        if (isset($context["add_participant_form"])) { $_add_participant_form_ = $context["add_participant_form"]; } else { $_add_participant_form_ = null; }
        echo         $this->env->getExtension('form')->renderer->renderBlock($_add_participant_form_, 'form_end');
        echo "



";
        // line 52
        if (isset($context["rem_participant_form"])) { $_rem_participant_form_ = $context["rem_participant_form"]; } else { $_rem_participant_form_ = null; }
        if ((!(null === $_rem_participant_form_))) {
            // line 53
            echo "
";
            // line 54
            if (isset($context["rem_participant_form"])) { $_rem_participant_form_ = $context["rem_participant_form"]; } else { $_rem_participant_form_ = null; }
            echo             $this->env->getExtension('form')->renderer->renderBlock($_rem_participant_form_, 'form_start');
            echo "

<fieldset>
    <legend>Retirer un participant</legend>

    <p>
        Les messages de la personne seront conservés mais celle-ci ne pourra
        plus accéder à la discussion.
    </p>

    ";
            // line 64
            if (isset($context["rem_participant_form"])) { $_rem_participant_form_ = $context["rem_participant_form"]; } else { $_rem_participant_form_ = null; }
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($_rem_participant_form_, 'rest');
            echo "

</fieldset>

";
            // line 68
            if (isset($context["rem_participant_form"])) { $_rem_participant_form_ = $context["rem_participant_form"]; } else { $_rem_participant_form_ = null; }
            echo             $this->env->getExtension('form')->renderer->renderBlock($_rem_participant_form_, 'form_end');
            echo "

";
        }
        // line 71
        echo "
";
    }

    public function getTemplateName()
    {
        return "LaplaceTrainingBundle:Thread:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  213 => 71,  206 => 68,  119 => 26,  306 => 103,  301 => 100,  278 => 90,  266 => 88,  262 => 86,  200 => 59,  141 => 43,  539 => 243,  533 => 240,  525 => 236,  519 => 234,  513 => 232,  504 => 227,  501 => 226,  499 => 225,  496 => 224,  485 => 215,  482 => 214,  476 => 210,  472 => 208,  467 => 204,  464 => 202,  457 => 199,  442 => 192,  439 => 191,  422 => 182,  414 => 178,  402 => 174,  387 => 163,  384 => 162,  382 => 161,  379 => 160,  374 => 156,  367 => 153,  342 => 136,  339 => 135,  337 => 134,  334 => 133,  329 => 129,  322 => 126,  300 => 112,  297 => 111,  292 => 109,  289 => 107,  283 => 103,  280 => 102,  276 => 101,  269 => 98,  261 => 94,  250 => 89,  241 => 75,  184 => 54,  176 => 51,  73 => 29,  99 => 25,  79 => 14,  257 => 85,  249 => 74,  253 => 90,  235 => 64,  198 => 64,  177 => 54,  174 => 41,  169 => 40,  68 => 29,  61 => 27,  341 => 118,  336 => 101,  331 => 97,  326 => 102,  324 => 101,  317 => 97,  314 => 122,  311 => 95,  305 => 104,  303 => 95,  298 => 92,  291 => 85,  288 => 84,  281 => 86,  273 => 89,  270 => 79,  265 => 107,  263 => 91,  260 => 90,  258 => 79,  252 => 77,  244 => 76,  223 => 67,  218 => 64,  159 => 46,  96 => 40,  86 => 15,  246 => 109,  210 => 69,  245 => 85,  237 => 81,  225 => 77,  209 => 69,  194 => 62,  150 => 42,  95 => 15,  128 => 36,  214 => 52,  191 => 87,  185 => 85,  42 => 15,  293 => 94,  286 => 125,  277 => 123,  267 => 119,  255 => 78,  238 => 81,  229 => 79,  220 => 75,  212 => 88,  188 => 60,  161 => 57,  144 => 44,  135 => 33,  126 => 29,  172 => 52,  165 => 38,  151 => 65,  116 => 30,  113 => 28,  153 => 39,  145 => 43,  139 => 60,  127 => 38,  112 => 22,  87 => 16,  94 => 118,  91 => 20,  23 => 3,  170 => 48,  125 => 35,  103 => 18,  82 => 9,  549 => 162,  543 => 161,  538 => 158,  530 => 155,  526 => 153,  522 => 151,  512 => 149,  505 => 148,  502 => 147,  497 => 146,  491 => 144,  488 => 143,  483 => 142,  473 => 134,  469 => 132,  466 => 131,  460 => 130,  455 => 129,  450 => 196,  444 => 122,  441 => 121,  437 => 190,  434 => 189,  429 => 185,  423 => 112,  420 => 111,  416 => 110,  413 => 109,  408 => 176,  394 => 105,  390 => 103,  375 => 101,  365 => 99,  362 => 98,  359 => 149,  355 => 95,  348 => 91,  344 => 90,  330 => 89,  327 => 88,  321 => 86,  307 => 85,  302 => 84,  295 => 110,  287 => 80,  279 => 84,  271 => 77,  264 => 95,  256 => 91,  251 => 71,  247 => 77,  239 => 68,  231 => 79,  219 => 75,  201 => 60,  147 => 34,  143 => 37,  134 => 40,  131 => 29,  122 => 24,  102 => 30,  92 => 17,  84 => 114,  72 => 10,  48 => 18,  35 => 5,  29 => 3,  76 => 7,  69 => 5,  54 => 14,  51 => 19,  31 => 11,  205 => 48,  199 => 71,  190 => 58,  182 => 58,  179 => 67,  175 => 58,  168 => 38,  164 => 48,  156 => 40,  148 => 47,  138 => 34,  123 => 30,  117 => 33,  108 => 32,  83 => 14,  71 => 10,  64 => 20,  110 => 28,  89 => 12,  65 => 14,  63 => 17,  58 => 15,  34 => 12,  227 => 58,  224 => 68,  221 => 54,  207 => 82,  197 => 74,  195 => 58,  192 => 68,  189 => 66,  186 => 53,  181 => 53,  178 => 52,  173 => 53,  162 => 44,  158 => 48,  155 => 36,  152 => 30,  142 => 47,  136 => 43,  133 => 34,  130 => 33,  120 => 26,  105 => 31,  100 => 22,  75 => 53,  53 => 13,  39 => 11,  98 => 16,  80 => 12,  78 => 42,  46 => 16,  44 => 10,  36 => 6,  32 => 8,  60 => 21,  57 => 9,  40 => 10,  114 => 25,  109 => 48,  106 => 20,  101 => 20,  88 => 11,  85 => 10,  77 => 7,  67 => 27,  47 => 24,  28 => 18,  25 => 5,  55 => 14,  43 => 11,  38 => 7,  26 => 8,  24 => 3,  50 => 7,  27 => 2,  22 => 2,  19 => 1,  232 => 79,  226 => 78,  222 => 77,  215 => 63,  211 => 84,  208 => 72,  202 => 66,  196 => 59,  193 => 65,  187 => 44,  183 => 60,  180 => 59,  171 => 58,  166 => 50,  163 => 49,  160 => 49,  157 => 43,  149 => 48,  146 => 44,  140 => 36,  137 => 36,  129 => 36,  124 => 36,  121 => 24,  118 => 33,  115 => 32,  111 => 22,  107 => 27,  104 => 26,  97 => 16,  93 => 19,  90 => 14,  81 => 12,  70 => 5,  66 => 13,  62 => 24,  59 => 17,  56 => 21,  52 => 10,  49 => 17,  45 => 13,  41 => 7,  37 => 9,  33 => 4,  30 => 6,);
    }
}
