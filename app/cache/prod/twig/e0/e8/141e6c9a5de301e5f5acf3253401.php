<?php

/* LaplaceTrainingBundle:Request:view.html.twig */
class __TwigTemplate_e0e8141e6c9a5de301e5f5acf3253401 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        if (isset($context["admin"])) { $_admin_ = $context["admin"]; } else { $_admin_ = null; }
        $context["container"] = (($_admin_) ? ("LaplaceCommonBundle::admin-page.html.twig") : ("LaplaceCommonBundle::user-page.html.twig"));
        // line 6
        echo "

";
        // line 8
        $this->env->loadTemplate("LaplaceTrainingBundle:Request:view.html.twig", "1740147252")->display(array_merge($context, array("page" => array(0 => "request", 1 => "view"))));
    }

    public function getTemplateName()
    {
        return "LaplaceTrainingBundle:Request:view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  154 => 45,  378 => 148,  370 => 143,  356 => 137,  346 => 131,  316 => 120,  310 => 119,  290 => 113,  274 => 106,  254 => 99,  248 => 95,  240 => 91,  203 => 68,  132 => 37,  213 => 71,  206 => 68,  119 => 26,  306 => 103,  301 => 100,  278 => 90,  266 => 88,  262 => 101,  200 => 59,  141 => 50,  539 => 243,  533 => 240,  525 => 236,  519 => 234,  513 => 232,  504 => 227,  501 => 226,  499 => 225,  496 => 224,  485 => 215,  482 => 214,  476 => 210,  472 => 208,  467 => 204,  464 => 202,  457 => 199,  442 => 192,  439 => 191,  422 => 182,  414 => 178,  402 => 174,  387 => 163,  384 => 162,  382 => 161,  379 => 160,  374 => 156,  367 => 153,  342 => 129,  339 => 135,  337 => 134,  334 => 126,  329 => 129,  322 => 122,  300 => 112,  297 => 111,  292 => 109,  289 => 107,  283 => 103,  280 => 102,  276 => 101,  269 => 103,  261 => 94,  250 => 89,  241 => 75,  184 => 65,  176 => 53,  73 => 29,  99 => 25,  79 => 11,  257 => 100,  249 => 74,  253 => 90,  235 => 87,  198 => 63,  177 => 62,  174 => 41,  169 => 40,  68 => 25,  61 => 22,  341 => 118,  336 => 101,  331 => 97,  326 => 102,  324 => 101,  317 => 97,  314 => 122,  311 => 95,  305 => 104,  303 => 95,  298 => 92,  291 => 85,  288 => 84,  281 => 86,  273 => 89,  270 => 79,  265 => 102,  263 => 91,  260 => 90,  258 => 79,  252 => 77,  244 => 76,  223 => 67,  218 => 64,  159 => 46,  96 => 23,  86 => 15,  246 => 109,  210 => 69,  245 => 94,  237 => 81,  225 => 77,  209 => 69,  194 => 62,  150 => 42,  95 => 18,  128 => 39,  214 => 52,  191 => 60,  185 => 85,  42 => 10,  293 => 94,  286 => 111,  277 => 107,  267 => 119,  255 => 78,  238 => 81,  229 => 79,  220 => 70,  212 => 67,  188 => 60,  161 => 57,  144 => 44,  135 => 33,  126 => 29,  172 => 51,  165 => 48,  151 => 65,  116 => 29,  113 => 28,  153 => 39,  145 => 43,  139 => 43,  127 => 38,  112 => 30,  87 => 16,  94 => 22,  91 => 20,  23 => 3,  170 => 48,  125 => 37,  103 => 22,  82 => 9,  549 => 162,  543 => 161,  538 => 158,  530 => 155,  526 => 153,  522 => 151,  512 => 149,  505 => 148,  502 => 147,  497 => 146,  491 => 144,  488 => 143,  483 => 142,  473 => 134,  469 => 132,  466 => 131,  460 => 130,  455 => 129,  450 => 196,  444 => 122,  441 => 121,  437 => 190,  434 => 189,  429 => 185,  423 => 112,  420 => 111,  416 => 110,  413 => 109,  408 => 176,  394 => 105,  390 => 103,  375 => 101,  365 => 99,  362 => 139,  359 => 149,  355 => 95,  348 => 91,  344 => 90,  330 => 89,  327 => 123,  321 => 86,  307 => 118,  302 => 117,  295 => 114,  287 => 80,  279 => 84,  271 => 77,  264 => 95,  256 => 91,  251 => 71,  247 => 77,  239 => 68,  231 => 79,  219 => 75,  201 => 60,  147 => 34,  143 => 37,  134 => 33,  131 => 39,  122 => 43,  102 => 39,  92 => 17,  84 => 114,  72 => 27,  48 => 18,  35 => 7,  29 => 5,  76 => 7,  69 => 5,  54 => 14,  51 => 19,  31 => 11,  205 => 48,  199 => 71,  190 => 58,  182 => 58,  179 => 67,  175 => 58,  168 => 49,  164 => 48,  156 => 53,  148 => 47,  138 => 34,  123 => 30,  117 => 40,  108 => 32,  83 => 14,  71 => 10,  64 => 23,  110 => 28,  89 => 12,  65 => 14,  63 => 17,  58 => 15,  34 => 12,  227 => 58,  224 => 68,  221 => 54,  207 => 82,  197 => 74,  195 => 62,  192 => 68,  189 => 66,  186 => 53,  181 => 53,  178 => 52,  173 => 53,  162 => 47,  158 => 48,  155 => 36,  152 => 30,  142 => 47,  136 => 43,  133 => 40,  130 => 33,  120 => 27,  105 => 31,  100 => 22,  75 => 53,  53 => 13,  39 => 15,  98 => 19,  80 => 8,  78 => 42,  46 => 16,  44 => 10,  36 => 6,  32 => 8,  60 => 21,  57 => 9,  40 => 10,  114 => 25,  109 => 26,  106 => 29,  101 => 20,  88 => 11,  85 => 10,  77 => 7,  67 => 27,  47 => 17,  28 => 18,  25 => 7,  55 => 21,  43 => 11,  38 => 7,  26 => 8,  24 => 3,  50 => 7,  27 => 2,  22 => 6,  19 => 2,  232 => 85,  226 => 74,  222 => 80,  215 => 63,  211 => 84,  208 => 65,  202 => 66,  196 => 59,  193 => 65,  187 => 58,  183 => 56,  180 => 55,  171 => 58,  166 => 50,  163 => 58,  160 => 49,  157 => 43,  149 => 48,  146 => 51,  140 => 41,  137 => 42,  129 => 46,  124 => 37,  121 => 24,  118 => 33,  115 => 32,  111 => 22,  107 => 27,  104 => 19,  97 => 19,  93 => 19,  90 => 34,  81 => 12,  70 => 5,  66 => 13,  62 => 24,  59 => 17,  56 => 21,  52 => 20,  49 => 17,  45 => 13,  41 => 7,  37 => 9,  33 => 11,  30 => 10,);
    }
}


/* LaplaceTrainingBundle:Request:view.html.twig */
class __TwigTemplate_e0e8141e6c9a5de301e5f5acf3253401_1740147252 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'PageTitle' => array($this, 'block_PageTitle'),
            'ContentTitle' => array($this, 'block_ContentTitle'),
            'ContentBody' => array($this, 'block_ContentBody'),
        );
    }

    protected function doGetParent(array $context)
    {
        return $this->env->resolveTemplate($this->getContext($context, "container"));
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 10
    public function block_PageTitle($context, array $blocks = array())
    {
        $this->displayParentBlock("PageTitle", $context, $blocks);
        echo " - ";
        if (isset($context["request"])) { $_request_ = $context["request"]; } else { $_request_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_request_, "title"), "html", null, true);
    }

    // line 12
    public function block_ContentTitle($context, array $blocks = array())
    {
        // line 13
        echo "    ";
        if (isset($context["request"])) { $_request_ = $context["request"]; } else { $_request_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_request_, "title"), "html", null, true);
        echo "
    ";
        // line 14
        if (isset($context["request"])) { $_request_ = $context["request"]; } else { $_request_ = null; }
        if ((!$this->getAttribute($_request_, "validated"))) {
            echo "<i class=\"icon-eye-close\"></i>";
        }
        // line 15
        echo "    ";
        if (isset($context["request"])) { $_request_ = $context["request"]; } else { $_request_ = null; }
        if ($this->getAttribute($_request_, "close")) {
            echo "<i class=\"icon-lock\"></i>";
        }
    }

    // line 20
    public function block_ContentBody($context, array $blocks = array())
    {
        // line 21
        echo "
";
        // line 23
        if (isset($context["request"])) { $_request_ = $context["request"]; } else { $_request_ = null; }
        if (isset($context["laplace_training"])) { $_laplace_training_ = $context["laplace_training"]; } else { $_laplace_training_ = null; }
        $this->env->loadTemplate("LaplaceTrainingBundle::category-breadcrumb.html.twig")->display(array_merge($context, array("reference" => $_request_, "link" => $this->env->getExtension('routing')->getPath(($_laplace_training_ . "view_request"), array("id" => $this->getAttribute($_request_, "id"))))));
        // line 30
        echo "
";
        // line 31
        if (isset($context["request"])) { $_request_ = $context["request"]; } else { $_request_ = null; }
        if ((!$this->getAttribute($_request_, "validated"))) {
            // line 32
            echo "<p class=\"text-warning\">
    ";
            // line 33
            if (isset($context["admin"])) { $_admin_ = $context["admin"]; } else { $_admin_ = null; }
            if ($_admin_) {
                // line 34
                echo "    Cette demande n'est pas encore validée et n'est visible que par son auteur.
    Une fois modifiée, elle deviendra accessible à tous les utilisateurs.
    ";
            } else {
                // line 37
                echo "    Cette demande de formation n'a pas encore été validée par le correspondant
    formation. Pour le moment, elle n'apparait donc pas dans la liste de toutes
    les demandes.
    ";
            }
            // line 41
            echo "</p>
";
        }
        // line 43
        echo "
<h5>Description de la demande</h5>

";
        // line 47
        if (isset($context["admin"])) { $_admin_ = $context["admin"]; } else { $_admin_ = null; }
        if ($_admin_) {
            // line 48
            echo "    <p>
        Par <a href=\"";
            // line 49
            if (isset($context["laplace_user"])) { $_laplace_user_ = $context["laplace_user"]; } else { $_laplace_user_ = null; }
            if (isset($context["request"])) { $_request_ = $context["request"]; } else { $_request_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath(($_laplace_user_ . "view_profile"), array("username" => $this->getAttribute($this->getAttribute($_request_, "author"), "username"))), "html", null, true);
            echo "\">";
            if (isset($context["request"])) { $_request_ = $context["request"]; } else { $_request_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_request_, "author"), "fullName"), "html", null, true);
            echo "</a>,
        le ";
            // line 50
            if (isset($context["request"])) { $_request_ = $context["request"]; } else { $_request_ = null; }
            echo twig_escape_filter($this->env, twig_localized_date_filter($this->env, $this->getAttribute($_request_, "issueDate"), "long", "short"), "html", null, true);
            echo ".
    </p>
    ";
            // line 52
            if (isset($context["request"])) { $_request_ = $context["request"]; } else { $_request_ = null; }
            if ((!(null === $this->getAttribute($_request_, "processingDate")))) {
                // line 53
                echo "    <p>
        <strong>Demande traitée le ";
                // line 54
                if (isset($context["request"])) { $_request_ = $context["request"]; } else { $_request_ = null; }
                echo twig_escape_filter($this->env, twig_localized_date_filter($this->env, $this->getAttribute($_request_, "processingDate"), "long", "none"), "html", null, true);
                echo ".</strong>
    </p>
    ";
            }
        }
        // line 58
        echo "
";
        // line 60
        if (isset($context["request"])) { $_request_ = $context["request"]; } else { $_request_ = null; }
        if (twig_test_empty($this->getAttribute($_request_, "description"))) {
            // line 61
            echo "    <p>
        <em>Aucune description</em>.
    </p>
";
        } else {
            // line 65
            echo "    <pre>";
            if (isset($context["request"])) { $_request_ = $context["request"]; } else { $_request_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_request_, "description"), "html", null, true);
            echo "</pre>
";
        }
        // line 67
        echo "
<dl>

    ";
        // line 70
        if (isset($context["request"])) { $_request_ = $context["request"]; } else { $_request_ = null; }
        if ($this->getAttribute($_request_, "trainingDate")) {
            // line 71
            echo "        <dt>Date de la formation :</dt>
        <dd>";
            // line 72
            if (isset($context["request"])) { $_request_ = $context["request"]; } else { $_request_ = null; }
            echo twig_escape_filter($this->env, twig_localized_date_filter($this->env, $this->getAttribute($_request_, "trainingDate"), "full", "short"), "html", null, true);
            echo "</dd>
    ";
        }
        // line 74
        echo "
    ";
        // line 75
        if (isset($context["request"])) { $_request_ = $context["request"]; } else { $_request_ = null; }
        if ($this->getAttribute($_request_, "trainingDuration")) {
            // line 76
            echo "        <dt>Durée de la formation (en nombre d'heures) :</dt>
        <dd>";
            // line 77
            if (isset($context["request"])) { $_request_ = $context["request"]; } else { $_request_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_request_, "trainingDuration"), "html", null, true);
            echo "
    ";
        }
        // line 79
        echo "
    ";
        // line 80
        if (isset($context["request"])) { $_request_ = $context["request"]; } else { $_request_ = null; }
        if ($this->getAttribute($_request_, "subscriptionDeadline")) {
            // line 81
            echo "        <dt>Date d'inscription limite :</dt>
        <dd>";
            // line 82
            if (isset($context["request"])) { $_request_ = $context["request"]; } else { $_request_ = null; }
            echo twig_escape_filter($this->env, twig_localized_date_filter($this->env, $this->getAttribute($_request_, "subscriptionDeadline"), "full", "none"), "html", null, true);
            echo "</dd>
    ";
        }
        // line 84
        echo "
    ";
        // line 85
        if (isset($context["request"])) { $_request_ = $context["request"]; } else { $_request_ = null; }
        if ($this->getAttribute($_request_, "availablePlacesCount")) {
            // line 86
            echo "        <dt>Nombre de places disponibles :</dt>
        <dd>";
            // line 87
            if (isset($context["request"])) { $_request_ = $context["request"]; } else { $_request_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_request_, "availablePlacesCount"), "html", null, true);
            echo "
    ";
        }
        // line 89
        echo "
    ";
        // line 90
        if (isset($context["request"])) { $_request_ = $context["request"]; } else { $_request_ = null; }
        if ($this->getAttribute($_request_, "agentsPrioritaires")) {
            // line 91
            echo "        <dt>Agents prioritaires :</dt>
        <dd>";
            // line 92
            if (isset($context["request"])) { $_request_ = $context["request"]; } else { $_request_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_request_, "agentsPrioritaires"), "html", null, true);
            echo "
    ";
        }
        // line 94
        echo "
    ";
        // line 95
        if (isset($context["request"])) { $_request_ = $context["request"]; } else { $_request_ = null; }
        if ($this->getAttribute($_request_, "funding")) {
            // line 96
            echo "        <dt>Financement :</dt>
        <dd>";
            // line 97
            if (isset($context["request"])) { $_request_ = $context["request"]; } else { $_request_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_request_, "funding"), "html", null, true);
            echo "
    ";
        }
        // line 99
        echo "
</dl>

";
        // line 103
        if (isset($context["admin"])) { $_admin_ = $context["admin"]; } else { $_admin_ = null; }
        if ($_admin_) {
            // line 104
            echo "    <p class=\"text-right\">
        <i class=\"icon-edit\"></i>
        <a href=\"";
            // line 106
            if (isset($context["laplace_training"])) { $_laplace_training_ = $context["laplace_training"]; } else { $_laplace_training_ = null; }
            if (isset($context["request"])) { $_request_ = $context["request"]; } else { $_request_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath(($_laplace_training_ . "edit_request"), array("id" => $this->getAttribute($_request_, "id"))), "html", null, true);
            echo "\">Modifier</a>

        <br />

        <i class=\"icon-trash\"></i>
        <a href=\"";
            // line 111
            if (isset($context["laplace_training"])) { $_laplace_training_ = $context["laplace_training"]; } else { $_laplace_training_ = null; }
            if (isset($context["request"])) { $_request_ = $context["request"]; } else { $_request_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath(($_laplace_training_ . "delete_request"), array("id" => $this->getAttribute($_request_, "id"))), "html", null, true);
            echo "\">Supprimer</a>
    </p>
";
        }
        // line 114
        echo "


<h5>Inscriptions</h5>
<p class=\"clearfix\">
";
        // line 119
        if (isset($context["subscriptions"])) { $_subscriptions_ = $context["subscriptions"]; } else { $_subscriptions_ = null; }
        if ((array_key_exists("subscriptions", $context) && (!twig_test_empty($_subscriptions_)))) {
            // line 120
            echo "    <ul>
    ";
            // line 121
            if (isset($context["subscriptions"])) { $_subscriptions_ = $context["subscriptions"]; } else { $_subscriptions_ = null; }
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($_subscriptions_);
            foreach ($context['_seq'] as $context["_key"] => $context["subinfo"]) {
                // line 122
                echo "        <li>
            <span class=\"label label-info\">";
                // line 124
                if (isset($context["subinfo"])) { $_subinfo_ = $context["subinfo"]; } else { $_subinfo_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($_subinfo_, "subscription"), "type"), "name"), "html", null, true);
                // line 125
                echo "</span>

            <a href=\"";
                // line 127
                if (isset($context["laplace_user"])) { $_laplace_user_ = $context["laplace_user"]; } else { $_laplace_user_ = null; }
                if (isset($context["subinfo"])) { $_subinfo_ = $context["subinfo"]; } else { $_subinfo_ = null; }
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath(($_laplace_user_ . "view_profile"), array("username" => $this->getAttribute($this->getAttribute($_subinfo_, "user"), "username"))), "html", null, true);
                echo "\">";
                if (isset($context["subinfo"])) { $_subinfo_ = $context["subinfo"]; } else { $_subinfo_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_subinfo_, "user"), "fullName"), "html", null, true);
                echo "</a>
            (";
                // line 128
                if (isset($context["subinfo"])) { $_subinfo_ = $context["subinfo"]; } else { $_subinfo_ = null; }
                echo twig_escape_filter($this->env, twig_localized_date_filter($this->env, $this->getAttribute($this->getAttribute($_subinfo_, "subscription"), "subscriptionDate"), "long", "none"), "html", null, true);
                echo ")
        </li>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subinfo'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 131
            echo "    </ul>
";
        } else {
            // line 133
            echo "    ";
            if (isset($context["request"])) { $_request_ = $context["request"]; } else { $_request_ = null; }
            $context["count"] = $this->getAttribute($this->getAttribute($_request_, "subscriptions"), "count", array(), "method");
            // line 134
            echo "    Le nombre de personnes inscrites à cette demande est
    <span class=\"badge";
            // line 135
            if (isset($context["count"])) { $_count_ = $context["count"]; } else { $_count_ = null; }
            if (($_count_ > 0)) {
                echo " badge-info";
            }
            echo "\">";
            if (isset($context["count"])) { $_count_ = $context["count"]; } else { $_count_ = null; }
            echo twig_escape_filter($this->env, $_count_, "html", null, true);
            echo "</span>.
";
        }
        // line 137
        echo "</p>

";
        // line 140
        echo "
<hr />

";
        // line 144
        echo "
<h3>Gestion</h3>

";
        // line 147
        if (isset($context["admin"])) { $_admin_ = $context["admin"]; } else { $_admin_ = null; }
        if ((!$_admin_)) {
            // line 148
            echo "
    ";
            // line 149
            if (isset($context["request"])) { $_request_ = $context["request"]; } else { $_request_ = null; }
            if ($this->getAttribute($_request_, "close")) {
                // line 150
                echo "
<p class=\"text-warning\">
    Cette demande de formation est close. Il n'est désormais plus possible
    de s'y inscrire, mais vous pouvez encore modifier et gérer une
    inscription en cours.
</p>

    ";
            }
            // line 158
            echo "
    ";
            // line 159
            if (isset($context["subscription"])) { $_subscription_ = $context["subscription"]; } else { $_subscription_ = null; }
            if ((array_key_exists("subscription", $context) && (!(null === $_subscription_)))) {
                // line 160
                echo "
<p>
    Vous vous êtes inscrit(e) à cette formation le <strong>";
                // line 162
                if (isset($context["subscription"])) { $_subscription_ = $context["subscription"]; } else { $_subscription_ = null; }
                echo twig_escape_filter($this->env, twig_localized_date_filter($this->env, $this->getAttribute($_subscription_, "subscriptionDate"), "long", "short"), "html", null, true);
                echo "</strong>
    avec le type
    <span class=\"label label-info\"
          title=\"";
                // line 165
                if (isset($context["subscription"])) { $_subscription_ = $context["subscription"]; } else { $_subscription_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_subscription_, "type"), "nameWithDescription"), "html", null, true);
                echo "\">";
                // line 166
                if (isset($context["subscription"])) { $_subscription_ = $context["subscription"]; } else { $_subscription_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_subscription_, "type"), "name"), "html", null, true);
                // line 167
                echo "</span>.
</p>

        ";
                // line 170
                if (isset($context["subscription"])) { $_subscription_ = $context["subscription"]; } else { $_subscription_ = null; }
                if (($this->getAttribute($_subscription_, "accepted") === null)) {
                    // line 171
                    echo "<p>
    Cette demande est <strong>en attente d'acceptation</strong> par le
    correspondant formation.
</p>
        ";
                } elseif (($this->getAttribute($_subscription_, "accepted") === true)) {
                    // line 176
                    echo "<p>
    Cette demande a été <strong>acceptée</strong> par le correspondant formation.
</p>
            ";
                    // line 179
                    if (isset($context["subscription"])) { $_subscription_ = $context["subscription"]; } else { $_subscription_ = null; }
                    if (($this->getAttribute($_subscription_, "attended") === null)) {
                        // line 180
                        echo "<p>
    Vous n'avez <strong>pas encore indiqué</strong> avoir assisté à la formation.
</p>
            ";
                    } elseif (($this->getAttribute($_subscription_, "attended") === true)) {
                        // line 184
                        echo "<p>
    Vous avez indiqué <strong>avoir assisté</strong> à la formation.
</p>
            ";
                    } else {
                        // line 188
                        echo "<p>
    Vous avez indiqué <strong>n'avoir pas assisté</strong> à la formation.
</p>
            ";
                    }
                    // line 192
                    echo "

        ";
                } else {
                    // line 195
                    echo "<p>
    Votre inscription à cette demande a été <strong>refusée</strong> par
    le correspondant formation.
</p>
        ";
                }
                // line 200
                echo "
    ";
            }
            // line 202
            echo "
";
            // line 204
            echo "
    ";
            // line 205
            if (array_key_exists("edit_form", $context)) {
                // line 206
                echo "
";
                // line 207
                if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
                echo                 $this->env->getExtension('form')->renderer->renderBlock($_edit_form_, 'form_start');
                echo "

<fieldset>
    <legend>Modifier</legend>

    <p>
        Ce formulaire vous permet de changer le type de demande.
    </p>


    ";
                // line 217
                if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($_edit_form_, 'rest');
                echo "

</fieldset>

";
                // line 221
                if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
                echo                 $this->env->getExtension('form')->renderer->renderBlock($_edit_form_, 'form_end');
                echo "

    ";
            }
            // line 224
            echo "


";
            // line 228
            echo "
    ";
            // line 229
            if (array_key_exists("unsubscribe_form", $context)) {
                // line 230
                echo "
";
                // line 231
                if (isset($context["unsubscribe_form"])) { $_unsubscribe_form_ = $context["unsubscribe_form"]; } else { $_unsubscribe_form_ = null; }
                echo                 $this->env->getExtension('form')->renderer->renderBlock($_unsubscribe_form_, 'form_start');
                echo "

<fieldset>
    <legend>Retirer cette demande</legend>

    <p>
        Si vous vous êtes inscrit(e) par erreur à cette demande, vous pouvez
        la retirer en utilisant ce formulaire.
        L'inscription disparaîtra également de votre historique.
    </p>

    ";
                // line 242
                if (isset($context["unsubscribe_form"])) { $_unsubscribe_form_ = $context["unsubscribe_form"]; } else { $_unsubscribe_form_ = null; }
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_unsubscribe_form_, "unsubscribe"), 'row');
                echo "

    ";
                // line 244
                if (isset($context["unsubscribe_form"])) { $_unsubscribe_form_ = $context["unsubscribe_form"]; } else { $_unsubscribe_form_ = null; }
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_unsubscribe_form_, "do"), 'row', array("attr" => array("class" => "btn-danger")));
                echo "

    ";
                // line 246
                if (isset($context["unsubscribe_form"])) { $_unsubscribe_form_ = $context["unsubscribe_form"]; } else { $_unsubscribe_form_ = null; }
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($_unsubscribe_form_, 'rest');
                echo "

</fieldset>

";
                // line 250
                if (isset($context["unsubscribe_form"])) { $_unsubscribe_form_ = $context["unsubscribe_form"]; } else { $_unsubscribe_form_ = null; }
                echo                 $this->env->getExtension('form')->renderer->renderBlock($_unsubscribe_form_, 'form_end');
                echo "

    ";
            }
            // line 253
            echo "


";
            // line 257
            echo "
    ";
            // line 258
            if (array_key_exists("attended_form", $context)) {
                // line 259
                echo "
";
                // line 260
                if (isset($context["attended_form"])) { $_attended_form_ = $context["attended_form"]; } else { $_attended_form_ = null; }
                echo                 $this->env->getExtension('form')->renderer->renderBlock($_attended_form_, 'form_start');
                echo "

<fieldset>
    <legend>Indiquer sa présence</legend>

    <p>
        Ce formulaire vous permet d'indiquer que vous avez suivi ou non la
        formation.
    </p>

    ";
                // line 270
                if (isset($context["attended_form"])) { $_attended_form_ = $context["attended_form"]; } else { $_attended_form_ = null; }
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($_attended_form_, 'rest');
                echo "

</fieldset>

";
                // line 274
                if (isset($context["attended_form"])) { $_attended_form_ = $context["attended_form"]; } else { $_attended_form_ = null; }
                echo                 $this->env->getExtension('form')->renderer->renderBlock($_attended_form_, 'form_end');
                echo "

    ";
            }
            // line 277
            echo "


";
            // line 281
            echo "
    ";
            // line 282
            if (array_key_exists("subscribe_form", $context)) {
                // line 283
                echo "
";
                // line 284
                if (isset($context["subscribe_form"])) { $_subscribe_form_ = $context["subscribe_form"]; } else { $_subscribe_form_ = null; }
                echo                 $this->env->getExtension('form')->renderer->renderBlock($_subscribe_form_, 'form_start');
                echo "
<fieldset>
    <legend>S'inscrire à cette demande</legend>

<p>
    Si vous souhaitez <strong>participer à cette formation</strong>,
    inscrivez-vous ci-dessous. Par la suite, le correspondant formation
    <strong>validera ou refusera</strong> votre inscription à cette
    demande de formation.
</p>

<p>
    <strong>Rappel des formalités administratives</strong>. Une fois votre
    demande validée, n'oubliez pas de :
    <ul>
        <li>remplir le formulaire de <em>Demande d'inscription à une formation</em> ;</li>
        <li>remplir le formulaire de <em>Demande d'ordre de mission</em> s'il y a déplacement ;</li>
        <li>faire signer votre demande par le correspondant formation si la formation est dispensée par le CNRS ;</li>
        <li>prévenir le secrétariat.</li>
    </ul>
</p>

<p class=\"text-right\">
    <a href=\"http://intranet.laplace.univ-tlse.fr/spip.php?rubrique137\">Voir l'ensemble des démarches à effectuer</a>
</p>

";
                // line 310
                if (isset($context["subscribe_form"])) { $_subscribe_form_ = $context["subscribe_form"]; } else { $_subscribe_form_ = null; }
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($_subscribe_form_, 'rest');
                echo "

</fieldset>
";
                // line 313
                if (isset($context["subscribe_form"])) { $_subscribe_form_ = $context["subscribe_form"]; } else { $_subscribe_form_ = null; }
                echo                 $this->env->getExtension('form')->renderer->renderBlock($_subscribe_form_, 'form_end');
                echo "

    ";
            }
            // line 316
            echo "
";
        } else {
            // line 318
            echo "
";
            // line 320
            echo "
";
            // line 321
            if (isset($context["subscriptions"])) { $_subscriptions_ = $context["subscriptions"]; } else { $_subscriptions_ = null; }
            if ((!twig_test_empty($_subscriptions_))) {
                // line 322
                echo "
<fieldset>
    <legend>Gérer les inscriptions</legend>

<p>
    Pour chacune des demandes ci-dessous, vous pouvez <strong>accepter,
    refuser ou mettre en attente</strong> l'inscription.
    La participation est ré-initialisée à \"Aucune indication\" si l'état
    de la demande n'est pas \"Demande acceptée\".
</p>

<ul class=\"nav nav-pills\">
    ";
                // line 334
                if (isset($context["subscriptions"])) { $_subscriptions_ = $context["subscriptions"]; } else { $_subscriptions_ = null; }
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($_subscriptions_);
                $context['loop'] = array(
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                );
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["_key"] => $context["subinfo"]) {
                    // line 335
                    echo "    <li class=\"";
                    if (isset($context["loop"])) { $_loop_ = $context["loop"]; } else { $_loop_ = null; }
                    echo (($this->getAttribute($_loop_, "first")) ? ("active") : (""));
                    echo "\">
        <a href=\"#manage_";
                    // line 336
                    if (isset($context["loop"])) { $_loop_ = $context["loop"]; } else { $_loop_ = null; }
                    echo twig_escape_filter($this->env, $this->getAttribute($_loop_, "index"), "html", null, true);
                    echo "\" data-toggle=\"tab\">";
                    // line 337
                    if (isset($context["subinfo"])) { $_subinfo_ = $context["subinfo"]; } else { $_subinfo_ = null; }
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_subinfo_, "user"), "fullName"), "html", null, true);
                    // line 338
                    echo "</a>
    </li>
    ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subinfo'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 341
                echo "</ul>

<div class=\"tab-content\">

    ";
                // line 345
                if (isset($context["subscriptions"])) { $_subscriptions_ = $context["subscriptions"]; } else { $_subscriptions_ = null; }
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($_subscriptions_);
                $context['loop'] = array(
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                );
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["_key"] => $context["subinfo"]) {
                    // line 346
                    echo "    <div id=\"manage_";
                    if (isset($context["loop"])) { $_loop_ = $context["loop"]; } else { $_loop_ = null; }
                    echo twig_escape_filter($this->env, $this->getAttribute($_loop_, "index"), "html", null, true);
                    echo "\" class=\"tab-pane fade ";
                    if (isset($context["loop"])) { $_loop_ = $context["loop"]; } else { $_loop_ = null; }
                    echo (($this->getAttribute($_loop_, "first")) ? ("active in") : (""));
                    echo "\">
    ";
                    // line 347
                    if (isset($context["subinfo"])) { $_subinfo_ = $context["subinfo"]; } else { $_subinfo_ = null; }
                    echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("LaplaceTrainingBundle:Request:manageSubscriptionForm", array("subinfo" => $_subinfo_)));
                    echo "
    </div>
    ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subinfo'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 350
                echo "
</div>

";
            } else {
                // line 354
                echo "
<p>
    <em>Aucune inscription à gérer.</em>
</p>

";
            }
            // line 360
            echo "
";
        }
        // line 362
        echo "
";
        // line 364
        echo "
<hr />

";
        // line 368
        echo "
<h3>Discussions</h3>

";
        // line 371
        if (isset($context["admin"])) { $_admin_ = $context["admin"]; } else { $_admin_ = null; }
        if ((!$_admin_)) {
            // line 372
            echo "<p>
    Cet espace vous permet d'échanger de manière privée avec le correspondant
    formation. Pour poser une question ou ajouter une remarque concernant cette
    demande, ouvrez une nouvelle discussion à l'aide du formulaire ci-dessous.
    Les messages publiés ne seront visibles que par vous,
    le correspondant formation, et les autres personnes autorisées
    individuellement à participer à la discussion.
</p>
";
        }
        // line 381
        echo "
";
        // line 382
        $this->env->loadTemplate("LaplaceTrainingBundle:Thread:table.html.twig")->display($context);
        // line 383
        echo "
";
        // line 384
        if (isset($context["thread_form"])) { $_thread_form_ = $context["thread_form"]; } else { $_thread_form_ = null; }
        echo         $this->env->getExtension('form')->renderer->renderBlock($_thread_form_, 'form_start');
        echo "

<fieldset>
    <legend>Ouvrir une nouvelle discussion</legend>

    ";
        // line 389
        if (isset($context["thread_form"])) { $_thread_form_ = $context["thread_form"]; } else { $_thread_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_thread_form_, "thread"), 'rest');
        echo "

    ";
        // line 391
        if (isset($context["thread_form"])) { $_thread_form_ = $context["thread_form"]; } else { $_thread_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_thread_form_, "message"), 'rest');
        echo "

    ";
        // line 393
        if (isset($context["thread_form"])) { $_thread_form_ = $context["thread_form"]; } else { $_thread_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($_thread_form_, 'rest');
        echo "

</fieldset>

";
        // line 397
        if (isset($context["thread_form"])) { $_thread_form_ = $context["thread_form"]; } else { $_thread_form_ = null; }
        echo         $this->env->getExtension('form')->renderer->renderBlock($_thread_form_, 'form_end');
        echo "

";
        // line 400
        echo "
";
    }

    public function getTemplateName()
    {
        return "LaplaceTrainingBundle:Request:view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  910 => 400,  904 => 397,  896 => 393,  890 => 391,  884 => 389,  875 => 384,  872 => 383,  870 => 382,  867 => 381,  856 => 372,  853 => 371,  848 => 368,  843 => 364,  840 => 362,  836 => 360,  828 => 354,  822 => 350,  804 => 347,  795 => 346,  777 => 345,  771 => 341,  755 => 338,  752 => 337,  748 => 336,  742 => 335,  724 => 334,  710 => 322,  707 => 321,  704 => 320,  701 => 318,  697 => 316,  690 => 313,  683 => 310,  653 => 284,  650 => 283,  648 => 282,  645 => 281,  640 => 277,  633 => 274,  625 => 270,  611 => 260,  608 => 259,  606 => 258,  603 => 257,  598 => 253,  591 => 250,  583 => 246,  577 => 244,  571 => 242,  556 => 231,  553 => 230,  551 => 229,  548 => 228,  536 => 221,  528 => 217,  514 => 207,  511 => 206,  509 => 205,  506 => 204,  503 => 202,  492 => 195,  487 => 192,  481 => 188,  475 => 184,  461 => 176,  454 => 171,  451 => 170,  446 => 167,  443 => 166,  432 => 162,  428 => 160,  425 => 159,  412 => 150,  409 => 149,  406 => 148,  403 => 147,  398 => 144,  393 => 140,  389 => 137,  371 => 133,  357 => 128,  338 => 122,  333 => 121,  320 => 114,  312 => 111,  284 => 97,  275 => 94,  236 => 81,  233 => 80,  230 => 79,  154 => 45,  378 => 135,  370 => 143,  356 => 137,  346 => 131,  316 => 120,  310 => 119,  290 => 99,  274 => 106,  254 => 87,  248 => 85,  240 => 91,  203 => 70,  132 => 37,  213 => 71,  206 => 71,  119 => 26,  306 => 103,  301 => 100,  278 => 95,  266 => 91,  262 => 101,  200 => 59,  141 => 50,  539 => 243,  533 => 240,  525 => 236,  519 => 234,  513 => 232,  504 => 227,  501 => 226,  499 => 200,  496 => 224,  485 => 215,  482 => 214,  476 => 210,  472 => 208,  467 => 204,  464 => 202,  457 => 199,  442 => 192,  439 => 165,  422 => 158,  414 => 178,  402 => 174,  387 => 163,  384 => 162,  382 => 161,  379 => 160,  374 => 156,  367 => 131,  342 => 129,  339 => 135,  337 => 134,  334 => 126,  329 => 129,  322 => 122,  300 => 112,  297 => 111,  292 => 109,  289 => 107,  283 => 103,  280 => 102,  276 => 101,  269 => 92,  261 => 94,  250 => 89,  241 => 75,  184 => 65,  176 => 53,  73 => 29,  99 => 25,  79 => 11,  257 => 100,  249 => 74,  253 => 90,  235 => 87,  198 => 67,  177 => 62,  174 => 41,  169 => 40,  68 => 25,  61 => 22,  341 => 124,  336 => 101,  331 => 97,  326 => 102,  324 => 101,  317 => 97,  314 => 122,  311 => 95,  305 => 104,  303 => 95,  298 => 104,  291 => 85,  288 => 84,  281 => 96,  273 => 89,  270 => 79,  265 => 102,  263 => 90,  260 => 89,  258 => 79,  252 => 77,  244 => 76,  223 => 67,  218 => 75,  159 => 50,  96 => 23,  86 => 15,  246 => 109,  210 => 69,  245 => 84,  237 => 81,  225 => 77,  209 => 72,  194 => 62,  150 => 49,  95 => 18,  128 => 39,  214 => 52,  191 => 65,  185 => 61,  42 => 10,  293 => 94,  286 => 111,  277 => 107,  267 => 119,  255 => 78,  238 => 81,  229 => 79,  220 => 70,  212 => 67,  188 => 60,  161 => 57,  144 => 47,  135 => 41,  126 => 29,  172 => 51,  165 => 52,  151 => 65,  116 => 29,  113 => 28,  153 => 39,  145 => 43,  139 => 43,  127 => 38,  112 => 30,  87 => 16,  94 => 15,  91 => 20,  23 => 3,  170 => 48,  125 => 37,  103 => 22,  82 => 9,  549 => 162,  543 => 224,  538 => 158,  530 => 155,  526 => 153,  522 => 151,  512 => 149,  505 => 148,  502 => 147,  497 => 146,  491 => 144,  488 => 143,  483 => 142,  473 => 134,  469 => 180,  466 => 179,  460 => 130,  455 => 129,  450 => 196,  444 => 122,  441 => 121,  437 => 190,  434 => 189,  429 => 185,  423 => 112,  420 => 111,  416 => 110,  413 => 109,  408 => 176,  394 => 105,  390 => 103,  375 => 134,  365 => 99,  362 => 139,  359 => 149,  355 => 95,  348 => 127,  344 => 125,  330 => 120,  327 => 119,  321 => 86,  307 => 118,  302 => 106,  295 => 103,  287 => 80,  279 => 84,  271 => 77,  264 => 95,  256 => 91,  251 => 86,  247 => 77,  239 => 82,  231 => 79,  219 => 75,  201 => 60,  147 => 48,  143 => 37,  134 => 33,  131 => 39,  122 => 43,  102 => 20,  92 => 17,  84 => 114,  72 => 27,  48 => 18,  35 => 7,  29 => 5,  76 => 7,  69 => 5,  54 => 14,  51 => 19,  31 => 11,  205 => 48,  199 => 71,  190 => 58,  182 => 60,  179 => 58,  175 => 58,  168 => 53,  164 => 48,  156 => 53,  148 => 47,  138 => 34,  123 => 30,  117 => 40,  108 => 23,  83 => 13,  71 => 10,  64 => 23,  110 => 28,  89 => 14,  65 => 14,  63 => 17,  58 => 15,  34 => 12,  227 => 58,  224 => 77,  221 => 76,  207 => 82,  197 => 74,  195 => 62,  192 => 68,  189 => 66,  186 => 53,  181 => 53,  178 => 52,  173 => 53,  162 => 47,  158 => 48,  155 => 36,  152 => 30,  142 => 47,  136 => 43,  133 => 40,  130 => 33,  120 => 27,  105 => 21,  100 => 22,  75 => 53,  53 => 13,  39 => 15,  98 => 19,  80 => 12,  78 => 42,  46 => 16,  44 => 10,  36 => 6,  32 => 8,  60 => 21,  57 => 9,  40 => 10,  114 => 25,  109 => 26,  106 => 29,  101 => 20,  88 => 11,  85 => 10,  77 => 7,  67 => 27,  47 => 17,  28 => 18,  25 => 7,  55 => 21,  43 => 11,  38 => 7,  26 => 8,  24 => 3,  50 => 7,  27 => 2,  22 => 6,  19 => 2,  232 => 85,  226 => 74,  222 => 80,  215 => 74,  211 => 84,  208 => 65,  202 => 66,  196 => 59,  193 => 65,  187 => 58,  183 => 56,  180 => 55,  171 => 54,  166 => 50,  163 => 58,  160 => 49,  157 => 43,  149 => 48,  146 => 51,  140 => 41,  137 => 42,  129 => 37,  124 => 34,  121 => 33,  118 => 32,  115 => 31,  111 => 22,  107 => 27,  104 => 19,  97 => 19,  93 => 19,  90 => 34,  81 => 12,  70 => 5,  66 => 13,  62 => 24,  59 => 17,  56 => 21,  52 => 20,  49 => 17,  45 => 13,  41 => 7,  37 => 9,  33 => 11,  30 => 10,);
    }
}
