<?php

/* LaplaceUserBundle:UserProfile:create.html.twig */
class __TwigTemplate_e6f6d7d7b297cf61208df77b948faf18 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
";
        // line 2
        $this->env->loadTemplate("LaplaceUserBundle:UserProfile:create.html.twig", "1905136813")->display(array_merge($context, array("tab" => "new-profile")));
    }

    public function getTemplateName()
    {
        return "LaplaceUserBundle:UserProfile:create.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  549 => 162,  543 => 161,  538 => 158,  530 => 155,  526 => 153,  522 => 151,  512 => 149,  505 => 148,  502 => 147,  497 => 146,  491 => 144,  488 => 143,  483 => 142,  473 => 134,  469 => 132,  466 => 131,  460 => 130,  455 => 129,  450 => 126,  444 => 122,  441 => 121,  437 => 120,  434 => 119,  429 => 116,  423 => 112,  420 => 111,  416 => 110,  413 => 109,  408 => 106,  394 => 105,  390 => 103,  375 => 101,  365 => 99,  362 => 98,  359 => 97,  355 => 95,  348 => 91,  344 => 90,  330 => 89,  327 => 88,  321 => 86,  307 => 85,  302 => 84,  295 => 81,  287 => 80,  279 => 78,  271 => 77,  264 => 74,  256 => 73,  251 => 71,  247 => 70,  239 => 69,  231 => 68,  219 => 67,  201 => 66,  147 => 51,  143 => 49,  134 => 43,  131 => 42,  122 => 37,  102 => 28,  92 => 25,  84 => 21,  72 => 15,  48 => 7,  35 => 5,  29 => 3,  76 => 17,  69 => 14,  54 => 14,  51 => 8,  31 => 7,  205 => 70,  199 => 69,  190 => 66,  182 => 62,  179 => 61,  175 => 58,  168 => 57,  164 => 55,  156 => 51,  148 => 47,  138 => 44,  123 => 42,  117 => 36,  108 => 31,  83 => 24,  71 => 19,  64 => 16,  110 => 20,  89 => 28,  65 => 14,  63 => 13,  58 => 10,  34 => 5,  227 => 92,  224 => 91,  221 => 90,  207 => 82,  197 => 74,  195 => 65,  192 => 72,  189 => 61,  186 => 60,  181 => 67,  178 => 66,  173 => 63,  162 => 58,  158 => 56,  155 => 55,  152 => 50,  142 => 47,  136 => 44,  133 => 44,  130 => 42,  120 => 40,  105 => 31,  100 => 19,  75 => 20,  53 => 19,  39 => 7,  98 => 33,  80 => 23,  78 => 25,  46 => 12,  44 => 9,  36 => 6,  32 => 4,  60 => 21,  57 => 9,  40 => 6,  114 => 22,  109 => 21,  106 => 20,  101 => 34,  88 => 6,  85 => 16,  77 => 12,  67 => 9,  47 => 6,  28 => 4,  25 => 3,  55 => 15,  43 => 7,  38 => 6,  26 => 6,  24 => 4,  50 => 7,  27 => 2,  22 => 2,  19 => 1,  232 => 82,  226 => 78,  222 => 76,  215 => 73,  211 => 84,  208 => 70,  202 => 68,  196 => 64,  193 => 63,  187 => 62,  183 => 60,  180 => 59,  171 => 54,  166 => 51,  163 => 50,  160 => 53,  157 => 48,  149 => 42,  146 => 41,  140 => 46,  137 => 37,  129 => 36,  124 => 35,  121 => 24,  118 => 33,  115 => 39,  111 => 32,  107 => 28,  104 => 27,  97 => 24,  93 => 9,  90 => 21,  81 => 14,  70 => 23,  66 => 13,  62 => 11,  59 => 15,  56 => 12,  52 => 10,  49 => 9,  45 => 6,  41 => 9,  37 => 5,  33 => 4,  30 => 3,);
    }
}


/* LaplaceUserBundle:UserProfile:create.html.twig */
class __TwigTemplate_e6f6d7d7b297cf61208df77b948faf18_1905136813 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("LaplaceCommonBundle::common-page.html.twig");

        $this->blocks = array(
            'PageTitle' => array($this, 'block_PageTitle'),
            'ContentTitle' => array($this, 'block_ContentTitle'),
            'ContentBody' => array($this, 'block_ContentBody'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "LaplaceCommonBundle::common-page.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_PageTitle($context, array $blocks = array())
    {
        $this->displayParentBlock("PageTitle", $context, $blocks);
        echo " - Inscription";
    }

    // line 6
    public function block_ContentTitle($context, array $blocks = array())
    {
        echo "Inscription";
    }

    // line 8
    public function block_ContentBody($context, array $blocks = array())
    {
        // line 9
        echo "
";
        // line 10
        if ((!array_key_exists("new_profile_form", $context))) {
            // line 11
            echo "
<p class=\"lead\">
    Cliquez sur le lien correspondant à votre situation :
    <ul>
        <li>
            <a href=\"";
            // line 16
            echo $this->env->getExtension('routing')->getPath("laplace_user_new_profile_docto");
            echo "\">Vous êtes un doctorant</a>
        </li>
        <li>
            <a href=\"";
            // line 19
            echo $this->env->getExtension('routing')->getPath("laplace_user_new_profile_agent");
            echo "\">Vous êtes un agent</a>
        </li>
    </ul>
</p>
";
        } else {
            // line 24
            echo "
";
            // line 25
            if (isset($context["new_profile_form"])) { $_new_profile_form_ = $context["new_profile_form"]; } else { $_new_profile_form_ = null; }
            echo             $this->env->getExtension('form')->renderer->renderBlock($_new_profile_form_, 'form_start');
            echo "

<fieldset>
    <legend>Nouveau profil</legend>

    ";
            // line 30
            if (array_key_exists("bad_credentials", $context)) {
                // line 31
                echo "    <p class=\"text-warning\">
        Vos identifiants ne sont pas valides. Veuillez essayer à nouveau.
    </p>
    ";
            }
            // line 35
            echo "
    ";
            // line 36
            if (isset($context["new_profile_form"])) { $_new_profile_form_ = $context["new_profile_form"]; } else { $_new_profile_form_ = null; }
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($_new_profile_form_, "identity"), "username"), 'row');
            echo "

    ";
            // line 38
            if (isset($context["new_profile_form"])) { $_new_profile_form_ = $context["new_profile_form"]; } else { $_new_profile_form_ = null; }
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_new_profile_form_, "password"), 'row');
            echo "

    ";
            // line 40
            if (isset($context["new_profile_form"])) { $_new_profile_form_ = $context["new_profile_form"]; } else { $_new_profile_form_ = null; }
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_new_profile_form_, "identity"), 'rest');
            echo "

    ";
            // line 42
            if (isset($context["new_profile_form"])) { $_new_profile_form_ = $context["new_profile_form"]; } else { $_new_profile_form_ = null; }
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_new_profile_form_, "user"), 'rest');
            echo "

    ";
            // line 44
            if (isset($context["new_profile_form"])) { $_new_profile_form_ = $context["new_profile_form"]; } else { $_new_profile_form_ = null; }
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_new_profile_form_, "userinfo"), 'rest');
            echo "

    <p>
        Vous pouvez laisser un message d'informations complémentaires pour
        le correspondant formation.
        Utilisez ce champ s'il manque un choix dans les champs ci-dessus.
    </p>

    ";
            // line 52
            if (isset($context["new_profile_form"])) { $_new_profile_form_ = $context["new_profile_form"]; } else { $_new_profile_form_ = null; }
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($_new_profile_form_, 'rest');
            echo "

</fieldset>

";
            // line 56
            if (isset($context["new_profile_form"])) { $_new_profile_form_ = $context["new_profile_form"]; } else { $_new_profile_form_ = null; }
            echo             $this->env->getExtension('form')->renderer->renderBlock($_new_profile_form_, 'form_end');
            echo "

<p>
    <i class=\"icon-asterisk\"></i> : champ obligatoire
</p>

";
        }
        // line 63
        echo "
";
    }

    public function getTemplateName()
    {
        return "LaplaceUserBundle:UserProfile:create.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  170 => 52,  125 => 31,  103 => 19,  82 => 8,  549 => 162,  543 => 161,  538 => 158,  530 => 155,  526 => 153,  522 => 151,  512 => 149,  505 => 148,  502 => 147,  497 => 146,  491 => 144,  488 => 143,  483 => 142,  473 => 134,  469 => 132,  466 => 131,  460 => 130,  455 => 129,  450 => 126,  444 => 122,  441 => 121,  437 => 120,  434 => 119,  429 => 116,  423 => 112,  420 => 111,  416 => 110,  413 => 109,  408 => 106,  394 => 105,  390 => 103,  375 => 101,  365 => 99,  362 => 98,  359 => 97,  355 => 95,  348 => 91,  344 => 90,  330 => 89,  327 => 88,  321 => 86,  307 => 85,  302 => 84,  295 => 81,  287 => 80,  279 => 78,  271 => 77,  264 => 74,  256 => 73,  251 => 71,  247 => 70,  239 => 69,  231 => 68,  219 => 67,  201 => 66,  147 => 51,  143 => 49,  134 => 36,  131 => 35,  122 => 37,  102 => 28,  92 => 25,  84 => 21,  72 => 15,  48 => 7,  35 => 5,  29 => 3,  76 => 6,  69 => 4,  54 => 14,  51 => 8,  31 => 7,  205 => 70,  199 => 69,  190 => 66,  182 => 62,  179 => 61,  175 => 58,  168 => 57,  164 => 55,  156 => 51,  148 => 47,  138 => 44,  123 => 30,  117 => 36,  108 => 31,  83 => 24,  71 => 19,  64 => 16,  110 => 20,  89 => 28,  65 => 14,  63 => 13,  58 => 10,  34 => 5,  227 => 92,  224 => 91,  221 => 90,  207 => 82,  197 => 74,  195 => 65,  192 => 72,  189 => 63,  186 => 60,  181 => 67,  178 => 56,  173 => 63,  162 => 58,  158 => 44,  155 => 55,  152 => 42,  142 => 47,  136 => 44,  133 => 44,  130 => 42,  120 => 40,  105 => 31,  100 => 19,  75 => 20,  53 => 19,  39 => 7,  98 => 33,  80 => 23,  78 => 25,  46 => 12,  44 => 9,  36 => 6,  32 => 4,  60 => 21,  57 => 9,  40 => 6,  114 => 25,  109 => 21,  106 => 20,  101 => 34,  88 => 10,  85 => 9,  77 => 12,  67 => 9,  47 => 6,  28 => 4,  25 => 3,  55 => 15,  43 => 7,  38 => 6,  26 => 6,  24 => 4,  50 => 7,  27 => 2,  22 => 2,  19 => 1,  232 => 82,  226 => 78,  222 => 76,  215 => 73,  211 => 84,  208 => 70,  202 => 68,  196 => 64,  193 => 63,  187 => 62,  183 => 60,  180 => 59,  171 => 54,  166 => 51,  163 => 50,  160 => 53,  157 => 48,  149 => 42,  146 => 40,  140 => 38,  137 => 37,  129 => 36,  124 => 35,  121 => 24,  118 => 33,  115 => 39,  111 => 24,  107 => 28,  104 => 27,  97 => 16,  93 => 9,  90 => 11,  81 => 14,  70 => 23,  66 => 13,  62 => 11,  59 => 15,  56 => 12,  52 => 10,  49 => 9,  45 => 6,  41 => 9,  37 => 5,  33 => 4,  30 => 3,);
    }
}
