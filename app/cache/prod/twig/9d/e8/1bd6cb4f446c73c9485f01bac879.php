<?php

/* LaplaceTrainingBundle:Thread:view.html.twig */
class __TwigTemplate_9de81bd6cb4f446c73c9485f01bac879 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        if (isset($context["admin"])) { $_admin_ = $context["admin"]; } else { $_admin_ = null; }
        $context["container"] = (($_admin_) ? ("LaplaceCommonBundle::admin-page.html.twig") : ("LaplaceCommonBundle::user-page.html.twig"));
        // line 6
        echo "
";
        // line 7
        $this->env->loadTemplate("LaplaceTrainingBundle:Thread:view.html.twig", "587447763")->display(array_merge($context, array("page" => array(0 => "thread", 1 => "view"))));
    }

    public function getTemplateName()
    {
        return "LaplaceTrainingBundle:Thread:view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  132 => 31,  213 => 71,  206 => 68,  119 => 26,  306 => 103,  301 => 100,  278 => 90,  266 => 88,  262 => 86,  200 => 59,  141 => 43,  539 => 243,  533 => 240,  525 => 236,  519 => 234,  513 => 232,  504 => 227,  501 => 226,  499 => 225,  496 => 224,  485 => 215,  482 => 214,  476 => 210,  472 => 208,  467 => 204,  464 => 202,  457 => 199,  442 => 192,  439 => 191,  422 => 182,  414 => 178,  402 => 174,  387 => 163,  384 => 162,  382 => 161,  379 => 160,  374 => 156,  367 => 153,  342 => 136,  339 => 135,  337 => 134,  334 => 133,  329 => 129,  322 => 126,  300 => 112,  297 => 111,  292 => 109,  289 => 107,  283 => 103,  280 => 102,  276 => 101,  269 => 98,  261 => 94,  250 => 89,  241 => 75,  184 => 54,  176 => 51,  73 => 29,  99 => 25,  79 => 14,  257 => 85,  249 => 74,  253 => 90,  235 => 64,  198 => 64,  177 => 54,  174 => 41,  169 => 40,  68 => 29,  61 => 27,  341 => 118,  336 => 101,  331 => 97,  326 => 102,  324 => 101,  317 => 97,  314 => 122,  311 => 95,  305 => 104,  303 => 95,  298 => 92,  291 => 85,  288 => 84,  281 => 86,  273 => 89,  270 => 79,  265 => 107,  263 => 91,  260 => 90,  258 => 79,  252 => 77,  244 => 76,  223 => 67,  218 => 64,  159 => 46,  96 => 40,  86 => 15,  246 => 109,  210 => 69,  245 => 85,  237 => 81,  225 => 77,  209 => 69,  194 => 62,  150 => 42,  95 => 15,  128 => 36,  214 => 52,  191 => 87,  185 => 85,  42 => 15,  293 => 94,  286 => 125,  277 => 123,  267 => 119,  255 => 78,  238 => 81,  229 => 79,  220 => 75,  212 => 88,  188 => 60,  161 => 57,  144 => 44,  135 => 33,  126 => 29,  172 => 52,  165 => 38,  151 => 65,  116 => 30,  113 => 28,  153 => 39,  145 => 43,  139 => 60,  127 => 38,  112 => 23,  87 => 16,  94 => 118,  91 => 20,  23 => 3,  170 => 48,  125 => 35,  103 => 18,  82 => 9,  549 => 162,  543 => 161,  538 => 158,  530 => 155,  526 => 153,  522 => 151,  512 => 149,  505 => 148,  502 => 147,  497 => 146,  491 => 144,  488 => 143,  483 => 142,  473 => 134,  469 => 132,  466 => 131,  460 => 130,  455 => 129,  450 => 196,  444 => 122,  441 => 121,  437 => 190,  434 => 189,  429 => 185,  423 => 112,  420 => 111,  416 => 110,  413 => 109,  408 => 176,  394 => 105,  390 => 103,  375 => 101,  365 => 99,  362 => 98,  359 => 149,  355 => 95,  348 => 91,  344 => 90,  330 => 89,  327 => 88,  321 => 86,  307 => 85,  302 => 84,  295 => 110,  287 => 80,  279 => 84,  271 => 77,  264 => 95,  256 => 91,  251 => 71,  247 => 77,  239 => 68,  231 => 79,  219 => 75,  201 => 60,  147 => 34,  143 => 37,  134 => 40,  131 => 29,  122 => 24,  102 => 17,  92 => 17,  84 => 114,  72 => 10,  48 => 18,  35 => 5,  29 => 3,  76 => 7,  69 => 5,  54 => 14,  51 => 19,  31 => 11,  205 => 48,  199 => 71,  190 => 58,  182 => 58,  179 => 67,  175 => 58,  168 => 38,  164 => 48,  156 => 40,  148 => 47,  138 => 34,  123 => 30,  117 => 33,  108 => 32,  83 => 14,  71 => 10,  64 => 20,  110 => 28,  89 => 12,  65 => 14,  63 => 17,  58 => 15,  34 => 12,  227 => 58,  224 => 68,  221 => 54,  207 => 82,  197 => 74,  195 => 58,  192 => 68,  189 => 66,  186 => 53,  181 => 53,  178 => 52,  173 => 53,  162 => 44,  158 => 48,  155 => 36,  152 => 30,  142 => 47,  136 => 43,  133 => 34,  130 => 33,  120 => 26,  105 => 31,  100 => 22,  75 => 53,  53 => 13,  39 => 11,  98 => 16,  80 => 12,  78 => 42,  46 => 16,  44 => 10,  36 => 6,  32 => 8,  60 => 21,  57 => 9,  40 => 10,  114 => 25,  109 => 48,  106 => 20,  101 => 20,  88 => 11,  85 => 10,  77 => 7,  67 => 27,  47 => 24,  28 => 18,  25 => 7,  55 => 14,  43 => 11,  38 => 7,  26 => 8,  24 => 3,  50 => 7,  27 => 2,  22 => 6,  19 => 2,  232 => 79,  226 => 78,  222 => 77,  215 => 63,  211 => 84,  208 => 72,  202 => 66,  196 => 59,  193 => 65,  187 => 44,  183 => 60,  180 => 59,  171 => 58,  166 => 50,  163 => 49,  160 => 49,  157 => 43,  149 => 48,  146 => 44,  140 => 36,  137 => 36,  129 => 36,  124 => 27,  121 => 24,  118 => 25,  115 => 32,  111 => 22,  107 => 27,  104 => 26,  97 => 16,  93 => 19,  90 => 14,  81 => 12,  70 => 5,  66 => 13,  62 => 24,  59 => 17,  56 => 21,  52 => 10,  49 => 17,  45 => 13,  41 => 7,  37 => 9,  33 => 4,  30 => 6,);
    }
}


/* LaplaceTrainingBundle:Thread:view.html.twig */
class __TwigTemplate_9de81bd6cb4f446c73c9485f01bac879_587447763 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'PageTitle' => array($this, 'block_PageTitle'),
            'ContentTitle' => array($this, 'block_ContentTitle'),
            'ContentBody' => array($this, 'block_ContentBody'),
        );
    }

    protected function doGetParent(array $context)
    {
        return $this->env->resolveTemplate($this->getContext($context, "container"));
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 9
    public function block_PageTitle($context, array $blocks = array())
    {
        $this->displayParentBlock("PageTitle", $context, $blocks);
        echo " - ";
        if (isset($context["thread"])) { $_thread_ = $context["thread"]; } else { $_thread_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_thread_, "title"), "html", null, true);
    }

    // line 11
    public function block_ContentTitle($context, array $blocks = array())
    {
        if (isset($context["thread"])) { $_thread_ = $context["thread"]; } else { $_thread_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_thread_, "title"), "html", null, true);
    }

    // line 15
    public function block_ContentBody($context, array $blocks = array())
    {
        // line 16
        echo "
";
        // line 17
        if (isset($context["thread"])) { $_thread_ = $context["thread"]; } else { $_thread_ = null; }
        if ($this->getAttribute($_thread_, "need")) {
            // line 18
            echo "<h5>Besoin associé à la discussion</h5>
";
            // line 20
            if (isset($context["thread"])) { $_thread_ = $context["thread"]; } else { $_thread_ = null; }
            if (isset($context["laplace_training"])) { $_laplace_training_ = $context["laplace_training"]; } else { $_laplace_training_ = null; }
            $this->env->loadTemplate("LaplaceTrainingBundle::category-breadcrumb.html.twig")->display(array_merge($context, array("reference" => $this->getAttribute($_thread_, "need"), "link" => $this->env->getExtension('routing')->getPath(($_laplace_training_ . "view_need"), array("id" => $this->getAttribute($this->getAttribute($_thread_, "need"), "id"))))));
        }
        // line 28
        echo "
";
        // line 29
        if (isset($context["thread"])) { $_thread_ = $context["thread"]; } else { $_thread_ = null; }
        if ($this->getAttribute($_thread_, "request")) {
            // line 30
            echo "<h5>Demande associée à la discussion</h5>
";
            // line 32
            if (isset($context["thread"])) { $_thread_ = $context["thread"]; } else { $_thread_ = null; }
            if (isset($context["laplace_training"])) { $_laplace_training_ = $context["laplace_training"]; } else { $_laplace_training_ = null; }
            $this->env->loadTemplate("LaplaceTrainingBundle::category-breadcrumb.html.twig")->display(array_merge($context, array("reference" => $this->getAttribute($_thread_, "request"), "link" => $this->env->getExtension('routing')->getPath(($_laplace_training_ . "view_request"), array("id" => $this->getAttribute($this->getAttribute($_thread_, "request"), "id"))))));
        }
        // line 40
        echo "
<h5>Informations sur la discussion</h5>

";
        // line 43
        if (isset($context["admin"])) { $_admin_ = $context["admin"]; } else { $_admin_ = null; }
        if ((!$_admin_)) {
            // line 44
            echo "
<p>
    Discussion ouverte par ";
            // line 46
            if (isset($context["thread"])) { $_thread_ = $context["thread"]; } else { $_thread_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_thread_, "author"), "fullName"), "html", null, true);
            echo ",
    <strong>le ";
            // line 47
            if (isset($context["thread"])) { $_thread_ = $context["thread"]; } else { $_thread_ = null; }
            echo twig_escape_filter($this->env, twig_localized_date_filter($this->env, $this->getAttribute($_thread_, "date"), "full", "short"), "html", null, true);
            echo "</strong>
    entre les personnes suivantes :
    <ul>
    ";
            // line 50
            if (isset($context["participants"])) { $_participants_ = $context["participants"]; } else { $_participants_ = null; }
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($_participants_);
            foreach ($context['_seq'] as $context["_key"] => $context["participant"]) {
                // line 51
                echo "        <li>";
                if (isset($context["participant"])) { $_participant_ = $context["participant"]; } else { $_participant_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($_participant_, "fullName"), "html", null, true);
                echo "</li>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['participant'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 53
            echo "        <li><em>Correspondant formation</em></li>
    </ul>
</p>

";
        } else {
            // line 58
            echo "
<p>
    Discussion ouverte par
    <a href=\"";
            // line 61
            if (isset($context["laplace_user"])) { $_laplace_user_ = $context["laplace_user"]; } else { $_laplace_user_ = null; }
            if (isset($context["thread"])) { $_thread_ = $context["thread"]; } else { $_thread_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath(($_laplace_user_ . "view_profile"), array("username" => $this->getAttribute($this->getAttribute($_thread_, "author"), "username"))), "html", null, true);
            echo "\">";
            if (isset($context["thread"])) { $_thread_ = $context["thread"]; } else { $_thread_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_thread_, "author"), "fullName"), "html", null, true);
            echo "</a>,
    <strong>le ";
            // line 62
            if (isset($context["thread"])) { $_thread_ = $context["thread"]; } else { $_thread_ = null; }
            echo twig_escape_filter($this->env, twig_localized_date_filter($this->env, $this->getAttribute($_thread_, "date"), "full", "short"), "html", null, true);
            echo "</strong>
    entre les personnes suivantes :
    <ul>
    ";
            // line 65
            if (isset($context["participants"])) { $_participants_ = $context["participants"]; } else { $_participants_ = null; }
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($_participants_);
            foreach ($context['_seq'] as $context["_key"] => $context["participant"]) {
                // line 66
                echo "        <li><a href=\"";
                if (isset($context["laplace_user"])) { $_laplace_user_ = $context["laplace_user"]; } else { $_laplace_user_ = null; }
                if (isset($context["participant"])) { $_participant_ = $context["participant"]; } else { $_participant_ = null; }
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath(($_laplace_user_ . "view_profile"), array("username" => $this->getAttribute($_participant_, "username"))), "html", null, true);
                echo "\">";
                if (isset($context["participant"])) { $_participant_ = $context["participant"]; } else { $_participant_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($_participant_, "fullName"), "html", null, true);
                echo "</a></li>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['participant'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 68
            echo "        <li><em>Correspondant formation</em></li>
    </ul>
</p>

<p class=\"text-right\">

    <i class=\"icon-edit\"></i>
    <a href=\"";
            // line 75
            if (isset($context["laplace_training"])) { $_laplace_training_ = $context["laplace_training"]; } else { $_laplace_training_ = null; }
            if (isset($context["thread"])) { $_thread_ = $context["thread"]; } else { $_thread_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath(($_laplace_training_ . "edit_thread"), array("id" => $this->getAttribute($_thread_, "id"))), "html", null, true);
            echo "\">Modifier</a>

    <br />

    <i class=\"icon-trash\"></i>
    <a href=\"";
            // line 80
            if (isset($context["laplace_training"])) { $_laplace_training_ = $context["laplace_training"]; } else { $_laplace_training_ = null; }
            if (isset($context["thread"])) { $_thread_ = $context["thread"]; } else { $_thread_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath(($_laplace_training_ . "delete_thread"), array("id" => $this->getAttribute($_thread_, "id"))), "html", null, true);
            echo "\">Supprimer</a>

</p>

";
        }
        // line 85
        echo "
";
        // line 87
        echo "
<hr />

";
        // line 91
        echo "
<h3>Messages</h3>

";
        // line 94
        if (isset($context["thread"])) { $_thread_ = $context["thread"]; } else { $_thread_ = null; }
        if (twig_test_empty($this->getAttribute($_thread_, "messages"))) {
            // line 95
            echo "<p>
    <em>Aucun message</em>.
</p>
";
        }
        // line 99
        echo "
";
        // line 100
        if (isset($context["messages"])) { $_messages_ = $context["messages"]; } else { $_messages_ = null; }
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($_messages_);
        foreach ($context['_seq'] as $context["_key"] => $context["msginfo"]) {
            // line 101
            echo "
";
            // line 102
            if (isset($context["delete_msg_forms"])) { $_delete_msg_forms_ = $context["delete_msg_forms"]; } else { $_delete_msg_forms_ = null; }
            if (isset($context["msginfo"])) { $_msginfo_ = $context["msginfo"]; } else { $_msginfo_ = null; }
            $context["delete_form"] = $this->getAttribute($_delete_msg_forms_, $this->getAttribute($this->getAttribute($_msginfo_, "message"), "id"), array(), "array");
            // line 103
            echo "
<p>
    <div class=\"pull-right text-right\">
        ";
            // line 106
            if (isset($context["delete_form"])) { $_delete_form_ = $context["delete_form"]; } else { $_delete_form_ = null; }
            if ((!(null === $_delete_form_))) {
                // line 107
                echo "        <button class=\"close\" type=\"button\" data-toggle=\"collapse\" data-target=\"#msg_";
                if (isset($context["msginfo"])) { $_msginfo_ = $context["msginfo"]; } else { $_msginfo_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_msginfo_, "message"), "id"), "html", null, true);
                echo "\">
            <i class=\"icon-trash\"></i>
        </button>
        ";
            }
            // line 111
            echo "    </div>
    <span>
        Le ";
            // line 113
            if (isset($context["msginfo"])) { $_msginfo_ = $context["msginfo"]; } else { $_msginfo_ = null; }
            echo twig_escape_filter($this->env, twig_localized_date_filter($this->env, $this->getAttribute($this->getAttribute($_msginfo_, "message"), "date"), "full", "short"), "html", null, true);
            echo ",
        ";
            // line 114
            if (isset($context["msginfo"])) { $_msginfo_ = $context["msginfo"]; } else { $_msginfo_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_msginfo_, "author"), "fullName"), "html", null, true);
            echo " a dit :
    </span>
    <br class=\"clearfix\" />
    <pre>";
            // line 117
            if (isset($context["msginfo"])) { $_msginfo_ = $context["msginfo"]; } else { $_msginfo_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_msginfo_, "message"), "text"), "html", null, true);
            echo "</pre>
    ";
            // line 118
            if (isset($context["delete_form"])) { $_delete_form_ = $context["delete_form"]; } else { $_delete_form_ = null; }
            if ((!(null === $_delete_form_))) {
                // line 119
                echo "    <div id=\"msg_";
                if (isset($context["msginfo"])) { $_msginfo_ = $context["msginfo"]; } else { $_msginfo_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_msginfo_, "message"), "id"), "html", null, true);
                echo "\" class=\"text-right collapse\">
        ";
                // line 120
                if (isset($context["delete_form"])) { $_delete_form_ = $context["delete_form"]; } else { $_delete_form_ = null; }
                echo                 $this->env->getExtension('form')->renderer->renderBlock($_delete_form_, 'form_start', array("attr" => array("class" => "form-inline")));
                echo "
        Supprimer ce message ?
        ";
                // line 122
                if (isset($context["delete_form"])) { $_delete_form_ = $context["delete_form"]; } else { $_delete_form_ = null; }
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_delete_form_, "do"), 'widget', array("attr" => array("class" => "btn-danger")));
                echo "
        ";
                // line 123
                if (isset($context["delete_form"])) { $_delete_form_ = $context["delete_form"]; } else { $_delete_form_ = null; }
                echo                 $this->env->getExtension('form')->renderer->renderBlock($_delete_form_, 'form_end');
                echo "
    </div>
    ";
            }
            // line 126
            echo "</p>

";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['msginfo'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 129
        echo "

";
        // line 131
        if (isset($context["message_form"])) { $_message_form_ = $context["message_form"]; } else { $_message_form_ = null; }
        echo         $this->env->getExtension('form')->renderer->renderBlock($_message_form_, 'form_start');
        echo "

<fieldset>

    <legend>Envoyer un message</legend>

    ";
        // line 137
        if (isset($context["message_form"])) { $_message_form_ = $context["message_form"]; } else { $_message_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_message_form_, "message"), 'rest');
        echo "

    ";
        // line 139
        if (isset($context["message_form"])) { $_message_form_ = $context["message_form"]; } else { $_message_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($_message_form_, 'rest');
        echo "

</fieldset>

";
        // line 143
        if (isset($context["message_form"])) { $_message_form_ = $context["message_form"]; } else { $_message_form_ = null; }
        echo         $this->env->getExtension('form')->renderer->renderBlock($_message_form_, 'form_end');
        echo "



";
        // line 148
        echo "
";
    }

    public function getTemplateName()
    {
        return "LaplaceTrainingBundle:Thread:view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  378 => 148,  370 => 143,  356 => 137,  346 => 131,  316 => 120,  310 => 119,  290 => 113,  274 => 106,  254 => 99,  248 => 95,  240 => 91,  203 => 68,  132 => 31,  213 => 71,  206 => 68,  119 => 26,  306 => 103,  301 => 100,  278 => 90,  266 => 88,  262 => 101,  200 => 59,  141 => 50,  539 => 243,  533 => 240,  525 => 236,  519 => 234,  513 => 232,  504 => 227,  501 => 226,  499 => 225,  496 => 224,  485 => 215,  482 => 214,  476 => 210,  472 => 208,  467 => 204,  464 => 202,  457 => 199,  442 => 192,  439 => 191,  422 => 182,  414 => 178,  402 => 174,  387 => 163,  384 => 162,  382 => 161,  379 => 160,  374 => 156,  367 => 153,  342 => 129,  339 => 135,  337 => 134,  334 => 126,  329 => 129,  322 => 122,  300 => 112,  297 => 111,  292 => 109,  289 => 107,  283 => 103,  280 => 102,  276 => 101,  269 => 103,  261 => 94,  250 => 89,  241 => 75,  184 => 65,  176 => 51,  73 => 29,  99 => 25,  79 => 11,  257 => 100,  249 => 74,  253 => 90,  235 => 87,  198 => 64,  177 => 62,  174 => 41,  169 => 40,  68 => 29,  61 => 27,  341 => 118,  336 => 101,  331 => 97,  326 => 102,  324 => 101,  317 => 97,  314 => 122,  311 => 95,  305 => 104,  303 => 95,  298 => 92,  291 => 85,  288 => 84,  281 => 86,  273 => 89,  270 => 79,  265 => 102,  263 => 91,  260 => 90,  258 => 79,  252 => 77,  244 => 76,  223 => 67,  218 => 64,  159 => 46,  96 => 40,  86 => 15,  246 => 109,  210 => 69,  245 => 94,  237 => 81,  225 => 77,  209 => 69,  194 => 62,  150 => 42,  95 => 18,  128 => 36,  214 => 52,  191 => 87,  185 => 85,  42 => 15,  293 => 94,  286 => 111,  277 => 107,  267 => 119,  255 => 78,  238 => 81,  229 => 79,  220 => 75,  212 => 75,  188 => 60,  161 => 57,  144 => 44,  135 => 33,  126 => 29,  172 => 52,  165 => 38,  151 => 65,  116 => 30,  113 => 28,  153 => 39,  145 => 43,  139 => 60,  127 => 38,  112 => 32,  87 => 16,  94 => 118,  91 => 20,  23 => 3,  170 => 48,  125 => 44,  103 => 28,  82 => 9,  549 => 162,  543 => 161,  538 => 158,  530 => 155,  526 => 153,  522 => 151,  512 => 149,  505 => 148,  502 => 147,  497 => 146,  491 => 144,  488 => 143,  483 => 142,  473 => 134,  469 => 132,  466 => 131,  460 => 130,  455 => 129,  450 => 196,  444 => 122,  441 => 121,  437 => 190,  434 => 189,  429 => 185,  423 => 112,  420 => 111,  416 => 110,  413 => 109,  408 => 176,  394 => 105,  390 => 103,  375 => 101,  365 => 99,  362 => 139,  359 => 149,  355 => 95,  348 => 91,  344 => 90,  330 => 89,  327 => 123,  321 => 86,  307 => 118,  302 => 117,  295 => 114,  287 => 80,  279 => 84,  271 => 77,  264 => 95,  256 => 91,  251 => 71,  247 => 77,  239 => 68,  231 => 79,  219 => 75,  201 => 60,  147 => 34,  143 => 37,  134 => 47,  131 => 29,  122 => 43,  102 => 17,  92 => 17,  84 => 114,  72 => 10,  48 => 18,  35 => 5,  29 => 3,  76 => 7,  69 => 5,  54 => 14,  51 => 19,  31 => 11,  205 => 48,  199 => 71,  190 => 58,  182 => 58,  179 => 67,  175 => 58,  168 => 61,  164 => 48,  156 => 53,  148 => 47,  138 => 34,  123 => 30,  117 => 40,  108 => 32,  83 => 14,  71 => 10,  64 => 20,  110 => 28,  89 => 16,  65 => 14,  63 => 17,  58 => 15,  34 => 12,  227 => 58,  224 => 68,  221 => 54,  207 => 82,  197 => 74,  195 => 58,  192 => 68,  189 => 66,  186 => 53,  181 => 53,  178 => 52,  173 => 53,  162 => 44,  158 => 48,  155 => 36,  152 => 30,  142 => 47,  136 => 43,  133 => 34,  130 => 33,  120 => 26,  105 => 31,  100 => 22,  75 => 53,  53 => 13,  39 => 11,  98 => 20,  80 => 12,  78 => 42,  46 => 16,  44 => 10,  36 => 6,  32 => 8,  60 => 21,  57 => 9,  40 => 10,  114 => 25,  109 => 30,  106 => 29,  101 => 20,  88 => 11,  85 => 10,  77 => 7,  67 => 27,  47 => 24,  28 => 18,  25 => 7,  55 => 14,  43 => 11,  38 => 7,  26 => 8,  24 => 3,  50 => 7,  27 => 2,  22 => 6,  19 => 2,  232 => 85,  226 => 78,  222 => 80,  215 => 63,  211 => 84,  208 => 72,  202 => 66,  196 => 59,  193 => 65,  187 => 44,  183 => 60,  180 => 59,  171 => 58,  166 => 50,  163 => 58,  160 => 49,  157 => 43,  149 => 48,  146 => 51,  140 => 36,  137 => 36,  129 => 46,  124 => 27,  121 => 24,  118 => 25,  115 => 32,  111 => 22,  107 => 27,  104 => 26,  97 => 16,  93 => 19,  90 => 14,  81 => 12,  70 => 9,  66 => 13,  62 => 24,  59 => 17,  56 => 21,  52 => 10,  49 => 17,  45 => 13,  41 => 7,  37 => 9,  33 => 4,  30 => 6,);
    }
}
