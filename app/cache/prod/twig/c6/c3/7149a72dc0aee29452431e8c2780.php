<?php

/* LaplaceTrainingBundle:System:manage-categories.html.twig */
class __TwigTemplate_c6c37149a72dc0aee29452431e8c2780 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
";
        // line 2
        $this->env->loadTemplate("LaplaceTrainingBundle:System:manage-categories.html.twig", "1283033984")->display(array_merge($context, array("page" => array(0 => "system", 1 => "categories"))));
    }

    public function getTemplateName()
    {
        return "LaplaceTrainingBundle:System:manage-categories.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  306 => 103,  301 => 100,  278 => 90,  266 => 88,  262 => 86,  200 => 59,  141 => 43,  539 => 243,  533 => 240,  525 => 236,  519 => 234,  513 => 232,  504 => 227,  501 => 226,  499 => 225,  496 => 224,  485 => 215,  482 => 214,  476 => 210,  472 => 208,  467 => 204,  464 => 202,  457 => 199,  442 => 192,  439 => 191,  422 => 182,  414 => 178,  402 => 174,  387 => 163,  384 => 162,  382 => 161,  379 => 160,  374 => 156,  367 => 153,  342 => 136,  339 => 135,  337 => 134,  334 => 133,  329 => 129,  322 => 126,  300 => 112,  297 => 111,  292 => 109,  289 => 107,  283 => 103,  280 => 102,  276 => 101,  269 => 98,  261 => 94,  250 => 89,  241 => 75,  184 => 55,  176 => 51,  73 => 29,  99 => 25,  79 => 14,  257 => 85,  249 => 74,  253 => 90,  235 => 64,  198 => 46,  177 => 54,  174 => 41,  169 => 40,  68 => 29,  61 => 27,  341 => 118,  336 => 101,  331 => 97,  326 => 102,  324 => 101,  317 => 97,  314 => 122,  311 => 95,  305 => 104,  303 => 95,  298 => 92,  291 => 85,  288 => 84,  281 => 86,  273 => 89,  270 => 79,  265 => 107,  263 => 91,  260 => 90,  258 => 79,  252 => 77,  244 => 76,  223 => 67,  218 => 64,  159 => 46,  96 => 40,  86 => 15,  246 => 109,  210 => 69,  245 => 85,  237 => 72,  225 => 76,  209 => 68,  194 => 62,  150 => 42,  95 => 15,  128 => 36,  214 => 52,  191 => 87,  185 => 85,  42 => 15,  293 => 94,  286 => 125,  277 => 123,  267 => 119,  255 => 78,  238 => 81,  229 => 79,  220 => 75,  212 => 88,  188 => 60,  161 => 57,  144 => 44,  135 => 33,  126 => 29,  172 => 52,  165 => 38,  151 => 65,  116 => 30,  113 => 28,  153 => 50,  145 => 43,  139 => 60,  127 => 38,  112 => 22,  87 => 16,  94 => 118,  91 => 20,  23 => 3,  170 => 62,  125 => 35,  103 => 19,  82 => 10,  549 => 162,  543 => 161,  538 => 158,  530 => 155,  526 => 153,  522 => 151,  512 => 149,  505 => 148,  502 => 147,  497 => 146,  491 => 144,  488 => 143,  483 => 142,  473 => 134,  469 => 132,  466 => 131,  460 => 130,  455 => 129,  450 => 196,  444 => 122,  441 => 121,  437 => 190,  434 => 189,  429 => 185,  423 => 112,  420 => 111,  416 => 110,  413 => 109,  408 => 176,  394 => 105,  390 => 103,  375 => 101,  365 => 99,  362 => 98,  359 => 149,  355 => 95,  348 => 91,  344 => 90,  330 => 89,  327 => 88,  321 => 86,  307 => 85,  302 => 84,  295 => 110,  287 => 80,  279 => 84,  271 => 77,  264 => 95,  256 => 91,  251 => 71,  247 => 77,  239 => 68,  231 => 70,  219 => 74,  201 => 60,  147 => 34,  143 => 45,  134 => 40,  131 => 29,  122 => 24,  102 => 30,  92 => 17,  84 => 114,  72 => 10,  48 => 18,  35 => 5,  29 => 3,  76 => 8,  69 => 6,  54 => 14,  51 => 19,  31 => 11,  205 => 48,  199 => 71,  190 => 58,  182 => 58,  179 => 67,  175 => 58,  168 => 38,  164 => 48,  156 => 44,  148 => 47,  138 => 34,  123 => 30,  117 => 33,  108 => 32,  83 => 14,  71 => 10,  64 => 20,  110 => 28,  89 => 12,  65 => 14,  63 => 17,  58 => 15,  34 => 12,  227 => 58,  224 => 68,  221 => 54,  207 => 82,  197 => 74,  195 => 58,  192 => 68,  189 => 66,  186 => 53,  181 => 64,  178 => 63,  173 => 53,  162 => 47,  158 => 48,  155 => 36,  152 => 30,  142 => 47,  136 => 43,  133 => 40,  130 => 29,  120 => 26,  105 => 31,  100 => 22,  75 => 53,  53 => 13,  39 => 11,  98 => 16,  80 => 12,  78 => 42,  46 => 16,  44 => 10,  36 => 6,  32 => 8,  60 => 21,  57 => 9,  40 => 10,  114 => 25,  109 => 48,  106 => 20,  101 => 20,  88 => 19,  85 => 11,  77 => 7,  67 => 27,  47 => 24,  28 => 18,  25 => 5,  55 => 14,  43 => 11,  38 => 7,  26 => 8,  24 => 3,  50 => 7,  27 => 2,  22 => 2,  19 => 1,  232 => 79,  226 => 78,  222 => 77,  215 => 63,  211 => 84,  208 => 72,  202 => 66,  196 => 59,  193 => 65,  187 => 44,  183 => 60,  180 => 59,  171 => 58,  166 => 50,  163 => 49,  160 => 49,  157 => 43,  149 => 48,  146 => 44,  140 => 36,  137 => 42,  129 => 36,  124 => 36,  121 => 24,  118 => 33,  115 => 32,  111 => 57,  107 => 27,  104 => 26,  97 => 26,  93 => 19,  90 => 14,  81 => 12,  70 => 5,  66 => 13,  62 => 24,  59 => 17,  56 => 21,  52 => 10,  49 => 17,  45 => 13,  41 => 7,  37 => 9,  33 => 4,  30 => 6,);
    }
}


/* LaplaceTrainingBundle:System:manage-categories.html.twig */
class __TwigTemplate_c6c37149a72dc0aee29452431e8c2780_1283033984 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("LaplaceCommonBundle::admin-page.html.twig");

        $this->blocks = array(
            'PageTitle' => array($this, 'block_PageTitle'),
            'ContentTitle' => array($this, 'block_ContentTitle'),
            'ContentBody' => array($this, 'block_ContentBody'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "LaplaceCommonBundle::admin-page.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_PageTitle($context, array $blocks = array())
    {
        $this->displayParentBlock("PageTitle", $context, $blocks);
        echo " - Catégories";
    }

    // line 7
    public function block_ContentTitle($context, array $blocks = array())
    {
        echo "Catégories";
    }

    // line 9
    public function block_ContentBody($context, array $blocks = array())
    {
        // line 10
        echo "
<h3>Modifier</h3>

";
        // line 13
        if (isset($context["edit_forms"])) { $_edit_forms_ = $context["edit_forms"]; } else { $_edit_forms_ = null; }
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($_edit_forms_);
        foreach ($context['_seq'] as $context["_key"] => $context["edit_form"]) {
            // line 14
            echo "
";
            // line 15
            if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
            echo             $this->env->getExtension('form')->renderer->renderBlock($_edit_form_, 'form_start');
            echo "

<fieldset>

    ";
            // line 19
            if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_edit_form_, "category"), 'rest');
            echo "

    ";
            // line 21
            if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($_edit_form_, 'rest');
            echo "

</fieldset>

";
            // line 25
            if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
            echo             $this->env->getExtension('form')->renderer->renderBlock($_edit_form_, 'form_end');
            echo "

";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['edit_form'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 28
        echo "
<hr />

";
        // line 32
        echo "
<h3>Créer / Supprimer</h3>

";
        // line 35
        if (isset($context["create_form"])) { $_create_form_ = $context["create_form"]; } else { $_create_form_ = null; }
        echo         $this->env->getExtension('form')->renderer->renderBlock($_create_form_, 'form_start');
        echo "

<fieldset>

    <legend>Créer une catégorie</legend>

    ";
        // line 41
        if (isset($context["create_form"])) { $_create_form_ = $context["create_form"]; } else { $_create_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_create_form_, "category"), 'rest');
        echo "

    ";
        // line 43
        if (isset($context["create_form"])) { $_create_form_ = $context["create_form"]; } else { $_create_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($_create_form_, 'rest');
        echo "

</fieldset>

";
        // line 47
        if (isset($context["create_form"])) { $_create_form_ = $context["create_form"]; } else { $_create_form_ = null; }
        echo         $this->env->getExtension('form')->renderer->renderBlock($_create_form_, 'form_end');
        echo "



";
        // line 51
        if (isset($context["delete_form"])) { $_delete_form_ = $context["delete_form"]; } else { $_delete_form_ = null; }
        echo         $this->env->getExtension('form')->renderer->renderBlock($_delete_form_, 'form_start');
        echo "

<fieldset>

    <legend>Supprimer une catégorie</legend>

    ";
        // line 57
        if (isset($context["delete_form"])) { $_delete_form_ = $context["delete_form"]; } else { $_delete_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_delete_form_, "category"), 'row');
        echo "

    ";
        // line 59
        if (isset($context["delete_form"])) { $_delete_form_ = $context["delete_form"]; } else { $_delete_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_delete_form_, "do"), 'row', array("attr" => array("class" => "btn-danger")));
        echo "

    ";
        // line 61
        if (isset($context["delete_form"])) { $_delete_form_ = $context["delete_form"]; } else { $_delete_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($_delete_form_, 'rest');
        echo "

</fieldset>

";
        // line 65
        if (isset($context["delete_form"])) { $_delete_form_ = $context["delete_form"]; } else { $_delete_form_ = null; }
        echo         $this->env->getExtension('form')->renderer->renderBlock($_delete_form_, 'form_end');
        echo "

";
    }

    public function getTemplateName()
    {
        return "LaplaceTrainingBundle:System:manage-categories.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  306 => 103,  301 => 100,  278 => 90,  266 => 88,  262 => 86,  200 => 59,  141 => 43,  539 => 243,  533 => 240,  525 => 236,  519 => 234,  513 => 232,  504 => 227,  501 => 226,  499 => 225,  496 => 224,  485 => 215,  482 => 214,  476 => 210,  472 => 208,  467 => 204,  464 => 202,  457 => 199,  442 => 192,  439 => 191,  422 => 182,  414 => 178,  402 => 174,  387 => 163,  384 => 162,  382 => 161,  379 => 160,  374 => 156,  367 => 153,  342 => 136,  339 => 135,  337 => 134,  334 => 133,  329 => 129,  322 => 126,  300 => 112,  297 => 111,  292 => 109,  289 => 107,  283 => 103,  280 => 102,  276 => 101,  269 => 98,  261 => 94,  250 => 89,  241 => 75,  184 => 55,  176 => 51,  73 => 29,  99 => 25,  79 => 14,  257 => 85,  249 => 74,  253 => 90,  235 => 64,  198 => 46,  177 => 54,  174 => 41,  169 => 40,  68 => 29,  61 => 27,  341 => 118,  336 => 101,  331 => 97,  326 => 102,  324 => 101,  317 => 97,  314 => 122,  311 => 95,  305 => 104,  303 => 95,  298 => 92,  291 => 85,  288 => 84,  281 => 86,  273 => 89,  270 => 79,  265 => 107,  263 => 91,  260 => 90,  258 => 79,  252 => 77,  244 => 76,  223 => 67,  218 => 64,  159 => 46,  96 => 40,  86 => 15,  246 => 109,  210 => 69,  245 => 85,  237 => 72,  225 => 76,  209 => 68,  194 => 61,  150 => 41,  95 => 14,  128 => 36,  214 => 52,  191 => 87,  185 => 85,  42 => 15,  293 => 94,  286 => 125,  277 => 123,  267 => 119,  255 => 78,  238 => 81,  229 => 79,  220 => 75,  212 => 88,  188 => 59,  161 => 57,  144 => 44,  135 => 32,  126 => 29,  172 => 51,  165 => 38,  151 => 65,  116 => 30,  113 => 28,  153 => 50,  145 => 43,  139 => 60,  127 => 38,  112 => 21,  87 => 16,  94 => 118,  91 => 20,  23 => 3,  170 => 62,  125 => 35,  103 => 19,  82 => 9,  549 => 162,  543 => 161,  538 => 158,  530 => 155,  526 => 153,  522 => 151,  512 => 149,  505 => 148,  502 => 147,  497 => 146,  491 => 144,  488 => 143,  483 => 142,  473 => 134,  469 => 132,  466 => 131,  460 => 130,  455 => 129,  450 => 196,  444 => 122,  441 => 121,  437 => 190,  434 => 189,  429 => 185,  423 => 112,  420 => 111,  416 => 110,  413 => 109,  408 => 176,  394 => 105,  390 => 103,  375 => 101,  365 => 99,  362 => 98,  359 => 149,  355 => 95,  348 => 91,  344 => 90,  330 => 89,  327 => 88,  321 => 86,  307 => 85,  302 => 84,  295 => 110,  287 => 80,  279 => 84,  271 => 77,  264 => 95,  256 => 91,  251 => 71,  247 => 77,  239 => 68,  231 => 70,  219 => 74,  201 => 60,  147 => 34,  143 => 45,  134 => 40,  131 => 29,  122 => 24,  102 => 30,  92 => 17,  84 => 114,  72 => 10,  48 => 18,  35 => 5,  29 => 3,  76 => 7,  69 => 5,  54 => 14,  51 => 19,  31 => 11,  205 => 48,  199 => 71,  190 => 58,  182 => 57,  179 => 67,  175 => 58,  168 => 38,  164 => 47,  156 => 43,  148 => 47,  138 => 34,  123 => 30,  117 => 33,  108 => 32,  83 => 14,  71 => 10,  64 => 20,  110 => 28,  89 => 12,  65 => 14,  63 => 17,  58 => 15,  34 => 12,  227 => 58,  224 => 68,  221 => 54,  207 => 82,  197 => 74,  195 => 58,  192 => 68,  189 => 66,  186 => 53,  181 => 64,  178 => 63,  173 => 53,  162 => 47,  158 => 48,  155 => 36,  152 => 30,  142 => 47,  136 => 43,  133 => 40,  130 => 28,  120 => 25,  105 => 31,  100 => 22,  75 => 53,  53 => 13,  39 => 11,  98 => 15,  80 => 12,  78 => 42,  46 => 16,  44 => 10,  36 => 6,  32 => 8,  60 => 21,  57 => 9,  40 => 10,  114 => 25,  109 => 48,  106 => 19,  101 => 20,  88 => 19,  85 => 10,  77 => 7,  67 => 27,  47 => 24,  28 => 18,  25 => 5,  55 => 14,  43 => 11,  38 => 7,  26 => 8,  24 => 3,  50 => 7,  27 => 2,  22 => 2,  19 => 1,  232 => 79,  226 => 78,  222 => 77,  215 => 63,  211 => 84,  208 => 72,  202 => 65,  196 => 59,  193 => 65,  187 => 44,  183 => 60,  180 => 59,  171 => 58,  166 => 50,  163 => 49,  160 => 49,  157 => 43,  149 => 48,  146 => 44,  140 => 35,  137 => 42,  129 => 36,  124 => 36,  121 => 24,  118 => 33,  115 => 32,  111 => 57,  107 => 27,  104 => 26,  97 => 26,  93 => 19,  90 => 13,  81 => 12,  70 => 5,  66 => 13,  62 => 24,  59 => 17,  56 => 21,  52 => 10,  49 => 17,  45 => 13,  41 => 7,  37 => 9,  33 => 4,  30 => 6,);
    }
}
