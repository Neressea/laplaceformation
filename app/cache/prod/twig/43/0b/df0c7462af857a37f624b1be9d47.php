<?php

/* LaplaceTrainingBundle::category-breadcrumb.html.twig */
class __TwigTemplate_430bdf0c7462af857a37f624b1be9d47 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 15
        echo "
";
        // line 16
        if (isset($context["reference"])) { $_reference_ = $context["reference"]; } else { $_reference_ = null; }
        if ((array_key_exists("reference", $context) && (!(null === $_reference_)))) {
            // line 17
            echo "<ul class=\"breadcrumb\">
";
            // line 18
            if (isset($context["reference"])) { $_reference_ = $context["reference"]; } else { $_reference_ = null; }
            if ($this->getAttribute($_reference_, "category")) {
                // line 19
                echo "    <li>";
                if (isset($context["reference"])) { $_reference_ = $context["reference"]; } else { $_reference_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($_reference_, "category"), "domain"), "name"), "html", null, true);
                echo " <span class=\"divider\"><i class=\"icon-chevron-right\"></i></span></li>
    <li>";
                // line 20
                if (isset($context["reference"])) { $_reference_ = $context["reference"]; } else { $_reference_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_reference_, "category"), "name"), "html", null, true);
                echo " <span class=\"divider\"><i class=\"icon-chevron-right\"></i></span></li>
";
            } else {
                // line 22
                echo "    <li>Catégorie inconnue <span class=\"divider\"><i class=\"icon-chevron-right\"></i></span></li>
";
            }
            // line 24
            echo "    ";
            if (isset($context["link"])) { $_link_ = $context["link"]; } else { $_link_ = null; }
            if ((array_key_exists("link", $context) && (!(null === $_link_)))) {
                // line 25
                echo "    <li><a href=\"";
                if (isset($context["link"])) { $_link_ = $context["link"]; } else { $_link_ = null; }
                echo twig_escape_filter($this->env, $_link_, "html", null, true);
                echo "\">";
                if (isset($context["reference"])) { $_reference_ = $context["reference"]; } else { $_reference_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($_reference_, "title"), "html", null, true);
                echo "</a></li>
    ";
            } else {
                // line 27
                echo "    <li>";
                if (isset($context["reference"])) { $_reference_ = $context["reference"]; } else { $_reference_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($_reference_, "title"), "html", null, true);
                echo "</li>
    ";
            }
            // line 29
            echo "</ul>
";
        }
    }

    public function getTemplateName()
    {
        return "LaplaceTrainingBundle::category-breadcrumb.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  68 => 29,  61 => 27,  341 => 118,  336 => 101,  331 => 97,  326 => 102,  324 => 101,  317 => 97,  314 => 96,  311 => 95,  305 => 104,  303 => 95,  298 => 92,  291 => 85,  288 => 84,  281 => 86,  273 => 80,  270 => 79,  265 => 107,  263 => 91,  260 => 90,  258 => 79,  252 => 77,  244 => 77,  223 => 67,  218 => 66,  159 => 35,  96 => 119,  86 => 45,  246 => 109,  210 => 69,  245 => 84,  237 => 72,  225 => 76,  209 => 68,  194 => 61,  150 => 41,  95 => 14,  128 => 37,  214 => 96,  191 => 87,  185 => 85,  42 => 15,  293 => 127,  286 => 125,  277 => 123,  267 => 119,  255 => 78,  238 => 81,  229 => 97,  220 => 75,  212 => 88,  188 => 59,  161 => 57,  144 => 44,  135 => 32,  126 => 32,  172 => 51,  165 => 37,  151 => 65,  116 => 22,  113 => 21,  153 => 45,  145 => 61,  139 => 60,  127 => 33,  112 => 21,  87 => 13,  94 => 118,  91 => 48,  23 => 3,  170 => 62,  125 => 26,  103 => 19,  82 => 9,  549 => 162,  543 => 161,  538 => 158,  530 => 155,  526 => 153,  522 => 151,  512 => 149,  505 => 148,  502 => 147,  497 => 146,  491 => 144,  488 => 143,  483 => 142,  473 => 134,  469 => 132,  466 => 131,  460 => 130,  455 => 129,  450 => 126,  444 => 122,  441 => 121,  437 => 120,  434 => 119,  429 => 116,  423 => 112,  420 => 111,  416 => 110,  413 => 109,  408 => 106,  394 => 105,  390 => 103,  375 => 101,  365 => 99,  362 => 98,  359 => 97,  355 => 95,  348 => 91,  344 => 90,  330 => 89,  327 => 88,  321 => 86,  307 => 85,  302 => 84,  295 => 91,  287 => 80,  279 => 84,  271 => 77,  264 => 118,  256 => 73,  251 => 71,  247 => 107,  239 => 69,  231 => 78,  219 => 74,  201 => 60,  147 => 51,  143 => 49,  134 => 36,  131 => 29,  122 => 35,  102 => 7,  92 => 25,  84 => 114,  72 => 52,  48 => 11,  35 => 5,  29 => 3,  76 => 7,  69 => 5,  54 => 14,  51 => 25,  31 => 19,  205 => 62,  199 => 91,  190 => 66,  182 => 57,  179 => 67,  175 => 58,  168 => 38,  164 => 47,  156 => 34,  148 => 47,  138 => 34,  123 => 30,  117 => 28,  108 => 13,  83 => 44,  71 => 10,  64 => 16,  110 => 20,  89 => 116,  65 => 14,  63 => 17,  58 => 15,  34 => 5,  227 => 69,  224 => 91,  221 => 90,  207 => 82,  197 => 74,  195 => 65,  192 => 72,  189 => 54,  186 => 53,  181 => 44,  178 => 83,  173 => 40,  162 => 36,  158 => 69,  155 => 55,  152 => 30,  142 => 47,  136 => 41,  133 => 31,  130 => 28,  120 => 24,  105 => 25,  100 => 18,  75 => 53,  53 => 13,  39 => 7,  98 => 52,  80 => 113,  78 => 42,  46 => 16,  44 => 10,  36 => 6,  32 => 8,  60 => 21,  57 => 9,  40 => 12,  114 => 25,  109 => 48,  106 => 19,  101 => 53,  88 => 12,  85 => 10,  77 => 112,  67 => 9,  47 => 24,  28 => 18,  25 => 17,  55 => 14,  43 => 22,  38 => 7,  26 => 8,  24 => 3,  50 => 7,  27 => 2,  22 => 16,  19 => 15,  232 => 79,  226 => 77,  222 => 76,  215 => 65,  211 => 84,  208 => 95,  202 => 65,  196 => 59,  193 => 65,  187 => 62,  183 => 60,  180 => 59,  171 => 54,  166 => 51,  163 => 50,  160 => 49,  157 => 43,  149 => 29,  146 => 40,  140 => 46,  137 => 37,  129 => 36,  124 => 35,  121 => 24,  118 => 29,  115 => 59,  111 => 57,  107 => 28,  104 => 54,  97 => 16,  93 => 16,  90 => 13,  81 => 12,  70 => 21,  66 => 13,  62 => 11,  59 => 15,  56 => 12,  52 => 10,  49 => 17,  45 => 6,  41 => 7,  37 => 20,  33 => 4,  30 => 3,);
    }
}
