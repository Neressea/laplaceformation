<?php

/* LaplaceUserBundle:Security:login.html.twig */
class __TwigTemplate_332a540d81e7497c460b7f8b3ea2f0b9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::layout.html.twig");

        $this->blocks = array(
            'PageTitle' => array($this, 'block_PageTitle'),
            'StyleSheets' => array($this, 'block_StyleSheets'),
            'NavbarOuter' => array($this, 'block_NavbarOuter'),
            'MainContainerOuter' => array($this, 'block_MainContainerOuter'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_PageTitle($context, array $blocks = array())
    {
        $this->displayParentBlock("PageTitle", $context, $blocks);
        echo " - Connexion";
    }

    // line 6
    public function block_StyleSheets($context, array $blocks = array())
    {
        // line 7
        echo "<style type=\"text/css\">
        body {
            padding-top: 40px;
            padding-bottom: 40px;
            background-color: #f5f5f5;
        }

        .form-signin {
            max-width: 300px;
            padding: 19px 29px 29px;
            margin: 0 auto 20px;
            background-color: #fff;
            border: 1px solid #e5e5e5;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
            -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
            -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
            box-shadow: 0 1px 2px rgba(0,0,0,.05);
        }
        .form-signin .form-signin-heading,
        .form-signin .checkbox {
            margin-bottom: 10px;
        }
        .form-signin input[type=\"text\"],
        .form-signin input[type=\"password\"] {
            font-size: 16px;
            height: auto;
            margin-bottom: 15px;
            padding: 7px 9px;
        }

    </style>
";
    }

    // line 42
    public function block_NavbarOuter($context, array $blocks = array())
    {
    }

    // line 44
    public function block_MainContainerOuter($context, array $blocks = array())
    {
        // line 45
        echo "    <!-- MAIN CONTAINER -->
    <div class=\"container\">

        <form method=\"POST\" action=\"";
        // line 48
        echo $this->env->getExtension('routing')->getPath("login_check");
        echo "\" class=\"form-signin\">

            <h2 class=\"form-signin-heading\">Connexion</h2>

            ";
        // line 52
        if (isset($context["error"])) { $_error_ = $context["error"]; } else { $_error_ = null; }
        if ($_error_) {
            // line 53
            echo "            <div class=\"alert alert-error\">
                ";
            // line 54
            if (isset($context["error"])) { $_error_ = $context["error"]; } else { $_error_ = null; }
            echo twig_escape_filter($this->env, $_error_, "html", null, true);
            echo "
            </div>
            ";
        }
        // line 57
        echo "
            <label for=\"username\">Login :</label>
            <input type=\"text\" id=\"username\" name=\"_username\" value=\"";
        // line 59
        if (isset($context["last_username"])) { $_last_username_ = $context["last_username"]; } else { $_last_username_ = null; }
        echo twig_escape_filter($this->env, $_last_username_, "html", null, true);
        echo "\" class=\"input-block-level\" />

            <label for=\"password\">Mot de passe :</label>
            <input type=\"password\" id=\"password\" name=\"_password\" class=\"input-block-level\" />

            <button type=\"submit\" class=\"btn btn-large btn-primary input-block-level\">Connexion</button>
        </form>

    </div>

";
    }

    public function getTemplateName()
    {
        return "LaplaceUserBundle:Security:login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  86 => 45,  246 => 85,  210 => 69,  245 => 84,  237 => 80,  225 => 76,  209 => 68,  194 => 61,  150 => 41,  95 => 14,  128 => 37,  214 => 96,  191 => 87,  185 => 85,  42 => 15,  293 => 127,  286 => 125,  277 => 123,  267 => 119,  255 => 111,  238 => 81,  229 => 97,  220 => 75,  212 => 88,  188 => 59,  161 => 57,  144 => 44,  135 => 32,  126 => 32,  172 => 51,  165 => 47,  151 => 65,  116 => 50,  113 => 49,  153 => 45,  145 => 61,  139 => 60,  127 => 33,  112 => 21,  87 => 13,  94 => 14,  91 => 48,  23 => 3,  170 => 62,  125 => 31,  103 => 19,  82 => 9,  549 => 162,  543 => 161,  538 => 158,  530 => 155,  526 => 153,  522 => 151,  512 => 149,  505 => 148,  502 => 147,  497 => 146,  491 => 144,  488 => 143,  483 => 142,  473 => 134,  469 => 132,  466 => 131,  460 => 130,  455 => 129,  450 => 126,  444 => 122,  441 => 121,  437 => 120,  434 => 119,  429 => 116,  423 => 112,  420 => 111,  416 => 110,  413 => 109,  408 => 106,  394 => 105,  390 => 103,  375 => 101,  365 => 99,  362 => 98,  359 => 97,  355 => 95,  348 => 91,  344 => 90,  330 => 89,  327 => 88,  321 => 86,  307 => 85,  302 => 84,  295 => 81,  287 => 80,  279 => 78,  271 => 77,  264 => 118,  256 => 73,  251 => 71,  247 => 107,  239 => 69,  231 => 78,  219 => 74,  201 => 66,  147 => 51,  143 => 49,  134 => 36,  131 => 34,  122 => 35,  102 => 24,  92 => 25,  84 => 32,  72 => 27,  48 => 7,  35 => 5,  29 => 3,  76 => 7,  69 => 5,  54 => 14,  51 => 8,  31 => 3,  205 => 85,  199 => 91,  190 => 66,  182 => 57,  179 => 67,  175 => 58,  168 => 57,  164 => 47,  156 => 43,  148 => 47,  138 => 44,  123 => 30,  117 => 28,  108 => 27,  83 => 44,  71 => 10,  64 => 16,  110 => 20,  89 => 12,  65 => 14,  63 => 23,  58 => 10,  34 => 5,  227 => 92,  224 => 91,  221 => 90,  207 => 82,  197 => 74,  195 => 65,  192 => 72,  189 => 63,  186 => 60,  181 => 67,  178 => 83,  173 => 82,  162 => 58,  158 => 69,  155 => 55,  152 => 42,  142 => 47,  136 => 41,  133 => 35,  130 => 28,  120 => 25,  105 => 25,  100 => 18,  75 => 20,  53 => 19,  39 => 7,  98 => 52,  80 => 8,  78 => 42,  46 => 16,  44 => 9,  36 => 6,  32 => 8,  60 => 21,  57 => 9,  40 => 12,  114 => 25,  109 => 48,  106 => 19,  101 => 53,  88 => 12,  85 => 10,  77 => 12,  67 => 9,  47 => 6,  28 => 4,  25 => 5,  55 => 19,  43 => 7,  38 => 6,  26 => 8,  24 => 3,  50 => 7,  27 => 2,  22 => 2,  19 => 1,  232 => 79,  226 => 77,  222 => 76,  215 => 89,  211 => 84,  208 => 95,  202 => 65,  196 => 64,  193 => 65,  187 => 62,  183 => 60,  180 => 59,  171 => 54,  166 => 51,  163 => 50,  160 => 49,  157 => 43,  149 => 42,  146 => 40,  140 => 35,  137 => 37,  129 => 36,  124 => 35,  121 => 24,  118 => 29,  115 => 59,  111 => 57,  107 => 28,  104 => 54,  97 => 16,  93 => 16,  90 => 13,  81 => 12,  70 => 6,  66 => 13,  62 => 11,  59 => 15,  56 => 12,  52 => 10,  49 => 17,  45 => 6,  41 => 7,  37 => 14,  33 => 4,  30 => 3,);
    }
}
