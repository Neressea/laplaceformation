<?php

/* LaplaceUserBundle:UserProfile:delete.html.twig */
class __TwigTemplate_e141d7ccf99008188917993db82b6120 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
";
        // line 2
        $this->env->loadTemplate("LaplaceUserBundle:UserProfile:delete.html.twig", "1246881320")->display(array_merge($context, array("page" => array(0 => "profile", 1 => "delete"))));
    }

    public function getTemplateName()
    {
        return "LaplaceUserBundle:UserProfile:delete.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  153 => 45,  145 => 41,  139 => 39,  127 => 33,  112 => 25,  87 => 13,  94 => 14,  91 => 13,  23 => 3,  170 => 52,  125 => 31,  103 => 19,  82 => 8,  549 => 162,  543 => 161,  538 => 158,  530 => 155,  526 => 153,  522 => 151,  512 => 149,  505 => 148,  502 => 147,  497 => 146,  491 => 144,  488 => 143,  483 => 142,  473 => 134,  469 => 132,  466 => 131,  460 => 130,  455 => 129,  450 => 126,  444 => 122,  441 => 121,  437 => 120,  434 => 119,  429 => 116,  423 => 112,  420 => 111,  416 => 110,  413 => 109,  408 => 106,  394 => 105,  390 => 103,  375 => 101,  365 => 99,  362 => 98,  359 => 97,  355 => 95,  348 => 91,  344 => 90,  330 => 89,  327 => 88,  321 => 86,  307 => 85,  302 => 84,  295 => 81,  287 => 80,  279 => 78,  271 => 77,  264 => 74,  256 => 73,  251 => 71,  247 => 70,  239 => 69,  231 => 68,  219 => 67,  201 => 66,  147 => 51,  143 => 49,  134 => 36,  131 => 35,  122 => 37,  102 => 19,  92 => 25,  84 => 21,  72 => 15,  48 => 7,  35 => 5,  29 => 3,  76 => 6,  69 => 4,  54 => 14,  51 => 8,  31 => 7,  205 => 70,  199 => 69,  190 => 66,  182 => 62,  179 => 58,  175 => 58,  168 => 57,  164 => 51,  156 => 51,  148 => 47,  138 => 44,  123 => 30,  117 => 36,  108 => 22,  83 => 24,  71 => 19,  64 => 16,  110 => 20,  89 => 28,  65 => 14,  63 => 13,  58 => 10,  34 => 5,  227 => 92,  224 => 91,  221 => 90,  207 => 82,  197 => 74,  195 => 65,  192 => 72,  189 => 63,  186 => 60,  181 => 67,  178 => 56,  173 => 56,  162 => 58,  158 => 44,  155 => 55,  152 => 42,  142 => 47,  136 => 44,  133 => 44,  130 => 34,  120 => 29,  105 => 20,  100 => 17,  75 => 20,  53 => 19,  39 => 7,  98 => 33,  80 => 11,  78 => 25,  46 => 12,  44 => 9,  36 => 6,  32 => 4,  60 => 21,  57 => 9,  40 => 6,  114 => 25,  109 => 21,  106 => 23,  101 => 34,  88 => 12,  85 => 9,  77 => 12,  67 => 9,  47 => 6,  28 => 4,  25 => 7,  55 => 15,  43 => 7,  38 => 6,  26 => 6,  24 => 4,  50 => 7,  27 => 2,  22 => 2,  19 => 1,  232 => 82,  226 => 78,  222 => 76,  215 => 73,  211 => 84,  208 => 70,  202 => 68,  196 => 64,  193 => 65,  187 => 62,  183 => 60,  180 => 59,  171 => 54,  166 => 51,  163 => 50,  160 => 49,  157 => 48,  149 => 42,  146 => 40,  140 => 38,  137 => 37,  129 => 36,  124 => 35,  121 => 24,  118 => 33,  115 => 39,  111 => 24,  107 => 28,  104 => 27,  97 => 18,  93 => 16,  90 => 14,  81 => 14,  70 => 9,  66 => 13,  62 => 11,  59 => 15,  56 => 12,  52 => 10,  49 => 9,  45 => 6,  41 => 9,  37 => 5,  33 => 4,  30 => 3,);
    }
}


/* LaplaceUserBundle:UserProfile:delete.html.twig */
class __TwigTemplate_e141d7ccf99008188917993db82b6120_1246881320 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("LaplaceCommonBundle::admin-page.html.twig");

        $this->blocks = array(
            'PageTitle' => array($this, 'block_PageTitle'),
            'ContentTitle' => array($this, 'block_ContentTitle'),
            'ContentBody' => array($this, 'block_ContentBody'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "LaplaceCommonBundle::admin-page.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_PageTitle($context, array $blocks = array())
    {
        $this->displayParentBlock("PageTitle", $context, $blocks);
        echo " - Supprimer un utilisateur";
    }

    // line 7
    public function block_ContentTitle($context, array $blocks = array())
    {
        echo "Supprimer un utilisateur";
    }

    // line 9
    public function block_ContentBody($context, array $blocks = array())
    {
        // line 10
        echo "
<p>
    La suppression des données de <strong>";
        // line 12
        if (isset($context["profile"])) { $_profile_ = $context["profile"]; } else { $_profile_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_profile_, "fullName"), "html", null, true);
        echo "</strong>
    entrainera également la suppression de son historique et de tous ses
    messages, dans toutes les discussions. Les besoins, demandes et fils de
    discussions créés par cet utilisateur ne seront pas supprimés, mais vous
    apparaîtrez désormais comme l'auteur de ces éléments.
    <strong>Attention ! Cette opération est irréversible.</strong>
</p>

";
        // line 20
        if (isset($context["delete_form"])) { $_delete_form_ = $context["delete_form"]; } else { $_delete_form_ = null; }
        echo         $this->env->getExtension('form')->renderer->renderBlock($_delete_form_, 'form_start');
        echo "

<fieldset>

    <legend>Supprimer un besoin</legend>

    ";
        // line 26
        if (isset($context["delete_form"])) { $_delete_form_ = $context["delete_form"]; } else { $_delete_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_delete_form_, "delete"), 'row');
        echo "

    ";
        // line 28
        if (isset($context["delete_form"])) { $_delete_form_ = $context["delete_form"]; } else { $_delete_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_delete_form_, "do"), 'row', array("attr" => array("class" => "btn-danger")));
        echo "

    ";
        // line 30
        if (isset($context["delete_form"])) { $_delete_form_ = $context["delete_form"]; } else { $_delete_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($_delete_form_, 'rest');
        echo "

</fieldset>

";
        // line 34
        if (isset($context["delete_form"])) { $_delete_form_ = $context["delete_form"]; } else { $_delete_form_ = null; }
        echo         $this->env->getExtension('form')->renderer->renderBlock($_delete_form_, 'form_end');
        echo "

";
    }

    public function getTemplateName()
    {
        return "LaplaceUserBundle:UserProfile:delete.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  153 => 45,  145 => 41,  139 => 39,  127 => 33,  112 => 25,  87 => 13,  94 => 14,  91 => 13,  23 => 3,  170 => 52,  125 => 31,  103 => 19,  82 => 9,  549 => 162,  543 => 161,  538 => 158,  530 => 155,  526 => 153,  522 => 151,  512 => 149,  505 => 148,  502 => 147,  497 => 146,  491 => 144,  488 => 143,  483 => 142,  473 => 134,  469 => 132,  466 => 131,  460 => 130,  455 => 129,  450 => 126,  444 => 122,  441 => 121,  437 => 120,  434 => 119,  429 => 116,  423 => 112,  420 => 111,  416 => 110,  413 => 109,  408 => 106,  394 => 105,  390 => 103,  375 => 101,  365 => 99,  362 => 98,  359 => 97,  355 => 95,  348 => 91,  344 => 90,  330 => 89,  327 => 88,  321 => 86,  307 => 85,  302 => 84,  295 => 81,  287 => 80,  279 => 78,  271 => 77,  264 => 74,  256 => 73,  251 => 71,  247 => 70,  239 => 69,  231 => 68,  219 => 67,  201 => 66,  147 => 51,  143 => 49,  134 => 36,  131 => 34,  122 => 37,  102 => 19,  92 => 25,  84 => 21,  72 => 15,  48 => 7,  35 => 5,  29 => 3,  76 => 7,  69 => 5,  54 => 14,  51 => 8,  31 => 7,  205 => 70,  199 => 69,  190 => 66,  182 => 62,  179 => 58,  175 => 58,  168 => 57,  164 => 51,  156 => 51,  148 => 47,  138 => 44,  123 => 30,  117 => 28,  108 => 22,  83 => 24,  71 => 19,  64 => 16,  110 => 20,  89 => 12,  65 => 14,  63 => 13,  58 => 10,  34 => 5,  227 => 92,  224 => 91,  221 => 90,  207 => 82,  197 => 74,  195 => 65,  192 => 72,  189 => 63,  186 => 60,  181 => 67,  178 => 56,  173 => 56,  162 => 58,  158 => 44,  155 => 55,  152 => 42,  142 => 47,  136 => 44,  133 => 44,  130 => 34,  120 => 29,  105 => 20,  100 => 17,  75 => 20,  53 => 19,  39 => 7,  98 => 33,  80 => 11,  78 => 25,  46 => 12,  44 => 9,  36 => 6,  32 => 4,  60 => 21,  57 => 9,  40 => 6,  114 => 25,  109 => 21,  106 => 23,  101 => 20,  88 => 12,  85 => 10,  77 => 12,  67 => 9,  47 => 6,  28 => 4,  25 => 7,  55 => 15,  43 => 7,  38 => 6,  26 => 6,  24 => 4,  50 => 7,  27 => 2,  22 => 2,  19 => 1,  232 => 82,  226 => 78,  222 => 76,  215 => 73,  211 => 84,  208 => 70,  202 => 68,  196 => 64,  193 => 65,  187 => 62,  183 => 60,  180 => 59,  171 => 54,  166 => 51,  163 => 50,  160 => 49,  157 => 48,  149 => 42,  146 => 40,  140 => 38,  137 => 37,  129 => 36,  124 => 35,  121 => 24,  118 => 33,  115 => 39,  111 => 26,  107 => 28,  104 => 27,  97 => 18,  93 => 16,  90 => 14,  81 => 14,  70 => 9,  66 => 13,  62 => 11,  59 => 15,  56 => 12,  52 => 10,  49 => 9,  45 => 6,  41 => 9,  37 => 5,  33 => 4,  30 => 3,);
    }
}
