<?php

/* LaplaceUserBundle:UserProfile:create-local.html.twig */
class __TwigTemplate_ebc3d1e71564f3679f3ae01b35bb644b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
";
        // line 2
        $this->env->loadTemplate("LaplaceUserBundle:UserProfile:create-local.html.twig", "980543753")->display(array_merge($context, array("page" => array(0 => "profile", 1 => "create"))));
    }

    public function getTemplateName()
    {
        return "LaplaceUserBundle:UserProfile:create-local.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  153 => 45,  145 => 41,  139 => 39,  127 => 33,  112 => 25,  87 => 13,  94 => 14,  91 => 13,  23 => 3,  170 => 52,  125 => 31,  103 => 19,  82 => 9,  549 => 162,  543 => 161,  538 => 158,  530 => 155,  526 => 153,  522 => 151,  512 => 149,  505 => 148,  502 => 147,  497 => 146,  491 => 144,  488 => 143,  483 => 142,  473 => 134,  469 => 132,  466 => 131,  460 => 130,  455 => 129,  450 => 126,  444 => 122,  441 => 121,  437 => 120,  434 => 119,  429 => 116,  423 => 112,  420 => 111,  416 => 110,  413 => 109,  408 => 106,  394 => 105,  390 => 103,  375 => 101,  365 => 99,  362 => 98,  359 => 97,  355 => 95,  348 => 91,  344 => 90,  330 => 89,  327 => 88,  321 => 86,  307 => 85,  302 => 84,  295 => 81,  287 => 80,  279 => 78,  271 => 77,  264 => 74,  256 => 73,  251 => 71,  247 => 70,  239 => 69,  231 => 68,  219 => 67,  201 => 66,  147 => 51,  143 => 49,  134 => 36,  131 => 34,  122 => 37,  102 => 19,  92 => 25,  84 => 21,  72 => 15,  48 => 7,  35 => 5,  29 => 3,  76 => 7,  69 => 5,  54 => 14,  51 => 8,  31 => 7,  205 => 70,  199 => 69,  190 => 66,  182 => 62,  179 => 58,  175 => 58,  168 => 57,  164 => 51,  156 => 51,  148 => 47,  138 => 44,  123 => 30,  117 => 28,  108 => 22,  83 => 24,  71 => 19,  64 => 16,  110 => 20,  89 => 12,  65 => 14,  63 => 13,  58 => 10,  34 => 5,  227 => 92,  224 => 91,  221 => 90,  207 => 82,  197 => 74,  195 => 65,  192 => 72,  189 => 63,  186 => 60,  181 => 67,  178 => 56,  173 => 56,  162 => 58,  158 => 44,  155 => 55,  152 => 42,  142 => 47,  136 => 44,  133 => 44,  130 => 34,  120 => 29,  105 => 20,  100 => 17,  75 => 20,  53 => 19,  39 => 7,  98 => 33,  80 => 11,  78 => 25,  46 => 12,  44 => 9,  36 => 6,  32 => 4,  60 => 21,  57 => 9,  40 => 6,  114 => 25,  109 => 21,  106 => 23,  101 => 20,  88 => 12,  85 => 10,  77 => 12,  67 => 9,  47 => 6,  28 => 4,  25 => 7,  55 => 15,  43 => 7,  38 => 6,  26 => 6,  24 => 4,  50 => 7,  27 => 2,  22 => 2,  19 => 1,  232 => 82,  226 => 78,  222 => 76,  215 => 73,  211 => 84,  208 => 70,  202 => 68,  196 => 64,  193 => 65,  187 => 62,  183 => 60,  180 => 59,  171 => 54,  166 => 51,  163 => 50,  160 => 49,  157 => 48,  149 => 42,  146 => 40,  140 => 38,  137 => 37,  129 => 36,  124 => 35,  121 => 24,  118 => 33,  115 => 39,  111 => 26,  107 => 28,  104 => 27,  97 => 18,  93 => 16,  90 => 14,  81 => 14,  70 => 9,  66 => 13,  62 => 11,  59 => 15,  56 => 12,  52 => 10,  49 => 9,  45 => 6,  41 => 9,  37 => 5,  33 => 4,  30 => 3,);
    }
}


/* LaplaceUserBundle:UserProfile:create-local.html.twig */
class __TwigTemplate_ebc3d1e71564f3679f3ae01b35bb644b_980543753 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("LaplaceCommonBundle::admin-page.html.twig");

        $this->blocks = array(
            'PageTitle' => array($this, 'block_PageTitle'),
            'ContentTitle' => array($this, 'block_ContentTitle'),
            'ContentBody' => array($this, 'block_ContentBody'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "LaplaceCommonBundle::admin-page.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_PageTitle($context, array $blocks = array())
    {
        $this->displayParentBlock("PageTitle", $context, $blocks);
        echo " - Créer un profil local";
    }

    // line 7
    public function block_ContentTitle($context, array $blocks = array())
    {
        echo "Créer un profil local";
    }

    // line 9
    public function block_ContentBody($context, array $blocks = array())
    {
        // line 10
        echo "
";
        // line 11
        if ((!array_key_exists("new_profile_form", $context))) {
            // line 12
            echo "
<p class=\"lead\">
    Cliquez sur le lien correspondant au type de profil local à créer :
    <ul>
        <li>
            <a href=\"";
            // line 17
            if (isset($context["laplace_user"])) { $_laplace_user_ = $context["laplace_user"]; } else { $_laplace_user_ = null; }
            echo $this->env->getExtension('routing')->getPath(($_laplace_user_ . "create_local_profile_docto"));
            echo "\">Doctorant</a>
        </li>
        <li>
            <a href=\"";
            // line 20
            if (isset($context["laplace_user"])) { $_laplace_user_ = $context["laplace_user"]; } else { $_laplace_user_ = null; }
            echo $this->env->getExtension('routing')->getPath(($_laplace_user_ . "create_local_profile_agent"));
            echo "\">Agent</a>
        </li>
    </ul>
</p>
";
        } else {
            // line 25
            echo "
";
            // line 26
            if (isset($context["new_profile_form"])) { $_new_profile_form_ = $context["new_profile_form"]; } else { $_new_profile_form_ = null; }
            echo             $this->env->getExtension('form')->renderer->renderBlock($_new_profile_form_, 'form_start');
            echo "

<fieldset>
    <legend>Nouveau profil</legend>

    ";
            // line 31
            if (isset($context["new_profile_form"])) { $_new_profile_form_ = $context["new_profile_form"]; } else { $_new_profile_form_ = null; }
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($_new_profile_form_, "identity"), "username"), 'row');
            echo "
    
    <p>La mention <strong>(local)</strong> sera automatiquement ajoutée au nom.</p>
    
    ";
            // line 35
            if (isset($context["new_profile_form"])) { $_new_profile_form_ = $context["new_profile_form"]; } else { $_new_profile_form_ = null; }
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_new_profile_form_, "name"), 'row');
            echo "
    
    ";
            // line 37
            if (isset($context["new_profile_form"])) { $_new_profile_form_ = $context["new_profile_form"]; } else { $_new_profile_form_ = null; }
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_new_profile_form_, "identity"), 'rest');
            echo "

    ";
            // line 39
            if (isset($context["new_profile_form"])) { $_new_profile_form_ = $context["new_profile_form"]; } else { $_new_profile_form_ = null; }
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_new_profile_form_, "user"), 'rest');
            echo "

    ";
            // line 41
            if (isset($context["new_profile_form"])) { $_new_profile_form_ = $context["new_profile_form"]; } else { $_new_profile_form_ = null; }
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_new_profile_form_, "userinfo"), 'rest');
            echo "

    ";
            // line 43
            if (isset($context["new_profile_form"])) { $_new_profile_form_ = $context["new_profile_form"]; } else { $_new_profile_form_ = null; }
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($_new_profile_form_, 'rest');
            echo "

</fieldset>

";
            // line 47
            if (isset($context["new_profile_form"])) { $_new_profile_form_ = $context["new_profile_form"]; } else { $_new_profile_form_ = null; }
            echo             $this->env->getExtension('form')->renderer->renderBlock($_new_profile_form_, 'form_end');
            echo "

";
        }
        // line 50
        echo "
";
    }

    public function getTemplateName()
    {
        return "LaplaceUserBundle:UserProfile:create-local.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  172 => 50,  165 => 47,  151 => 41,  116 => 26,  113 => 25,  153 => 45,  145 => 39,  139 => 37,  127 => 33,  112 => 25,  87 => 13,  94 => 14,  91 => 13,  23 => 3,  170 => 52,  125 => 31,  103 => 19,  82 => 9,  549 => 162,  543 => 161,  538 => 158,  530 => 155,  526 => 153,  522 => 151,  512 => 149,  505 => 148,  502 => 147,  497 => 146,  491 => 144,  488 => 143,  483 => 142,  473 => 134,  469 => 132,  466 => 131,  460 => 130,  455 => 129,  450 => 126,  444 => 122,  441 => 121,  437 => 120,  434 => 119,  429 => 116,  423 => 112,  420 => 111,  416 => 110,  413 => 109,  408 => 106,  394 => 105,  390 => 103,  375 => 101,  365 => 99,  362 => 98,  359 => 97,  355 => 95,  348 => 91,  344 => 90,  330 => 89,  327 => 88,  321 => 86,  307 => 85,  302 => 84,  295 => 81,  287 => 80,  279 => 78,  271 => 77,  264 => 74,  256 => 73,  251 => 71,  247 => 70,  239 => 69,  231 => 68,  219 => 67,  201 => 66,  147 => 51,  143 => 49,  134 => 36,  131 => 34,  122 => 37,  102 => 19,  92 => 25,  84 => 21,  72 => 15,  48 => 7,  35 => 5,  29 => 3,  76 => 7,  69 => 5,  54 => 14,  51 => 8,  31 => 7,  205 => 70,  199 => 69,  190 => 66,  182 => 62,  179 => 58,  175 => 58,  168 => 57,  164 => 51,  156 => 51,  148 => 47,  138 => 44,  123 => 30,  117 => 28,  108 => 22,  83 => 24,  71 => 19,  64 => 16,  110 => 20,  89 => 12,  65 => 14,  63 => 13,  58 => 10,  34 => 5,  227 => 92,  224 => 91,  221 => 90,  207 => 82,  197 => 74,  195 => 65,  192 => 72,  189 => 63,  186 => 60,  181 => 67,  178 => 56,  173 => 56,  162 => 58,  158 => 44,  155 => 55,  152 => 42,  142 => 47,  136 => 44,  133 => 35,  130 => 34,  120 => 29,  105 => 20,  100 => 17,  75 => 20,  53 => 19,  39 => 7,  98 => 33,  80 => 11,  78 => 25,  46 => 12,  44 => 9,  36 => 6,  32 => 4,  60 => 21,  57 => 9,  40 => 6,  114 => 25,  109 => 21,  106 => 23,  101 => 20,  88 => 11,  85 => 10,  77 => 12,  67 => 9,  47 => 6,  28 => 4,  25 => 7,  55 => 15,  43 => 7,  38 => 6,  26 => 6,  24 => 4,  50 => 7,  27 => 2,  22 => 2,  19 => 1,  232 => 82,  226 => 78,  222 => 76,  215 => 73,  211 => 84,  208 => 70,  202 => 68,  196 => 64,  193 => 65,  187 => 62,  183 => 60,  180 => 59,  171 => 54,  166 => 51,  163 => 50,  160 => 49,  157 => 43,  149 => 42,  146 => 40,  140 => 38,  137 => 37,  129 => 36,  124 => 35,  121 => 24,  118 => 33,  115 => 39,  111 => 26,  107 => 28,  104 => 20,  97 => 17,  93 => 16,  90 => 12,  81 => 14,  70 => 9,  66 => 13,  62 => 11,  59 => 15,  56 => 12,  52 => 10,  49 => 9,  45 => 6,  41 => 9,  37 => 5,  33 => 4,  30 => 3,);
    }
}
