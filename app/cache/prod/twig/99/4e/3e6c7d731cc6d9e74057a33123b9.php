<?php

/* LaplaceTrainingBundle:Need:view-all.html.twig */
class __TwigTemplate_994e3e6c7d731cc6d9e74057a33123b9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        if (isset($context["admin"])) { $_admin_ = $context["admin"]; } else { $_admin_ = null; }
        $context["container"] = (($_admin_) ? ("LaplaceCommonBundle::admin-page.html.twig") : ("LaplaceCommonBundle::user-page.html.twig"));
        // line 6
        echo "

";
        // line 8
        $this->env->loadTemplate("LaplaceTrainingBundle:Need:view-all.html.twig", "1951226299")->display(array_merge($context, array("page" => array(0 => "need", 1 => "all"))));
    }

    public function getTemplateName()
    {
        return "LaplaceTrainingBundle:Need:view-all.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  539 => 243,  533 => 240,  525 => 236,  519 => 234,  513 => 232,  504 => 227,  501 => 226,  499 => 225,  496 => 224,  485 => 215,  482 => 214,  476 => 210,  472 => 208,  467 => 204,  464 => 202,  457 => 199,  442 => 192,  439 => 191,  422 => 182,  414 => 178,  402 => 174,  387 => 163,  384 => 162,  382 => 161,  379 => 160,  374 => 156,  367 => 153,  342 => 136,  339 => 135,  337 => 134,  334 => 133,  329 => 129,  322 => 126,  300 => 112,  297 => 111,  292 => 109,  289 => 107,  283 => 103,  280 => 102,  276 => 101,  269 => 98,  261 => 94,  250 => 89,  241 => 82,  184 => 65,  176 => 51,  73 => 29,  99 => 22,  79 => 7,  257 => 78,  249 => 74,  253 => 90,  235 => 64,  198 => 46,  177 => 42,  174 => 41,  169 => 40,  68 => 29,  61 => 27,  341 => 118,  336 => 101,  331 => 97,  326 => 102,  324 => 101,  317 => 97,  314 => 122,  311 => 95,  305 => 104,  303 => 95,  298 => 92,  291 => 85,  288 => 84,  281 => 86,  273 => 80,  270 => 79,  265 => 107,  263 => 91,  260 => 90,  258 => 79,  252 => 77,  244 => 77,  223 => 67,  218 => 75,  159 => 46,  96 => 40,  86 => 15,  246 => 109,  210 => 69,  245 => 85,  237 => 72,  225 => 76,  209 => 68,  194 => 61,  150 => 35,  95 => 14,  128 => 39,  214 => 52,  191 => 87,  185 => 85,  42 => 15,  293 => 127,  286 => 125,  277 => 123,  267 => 119,  255 => 78,  238 => 81,  229 => 79,  220 => 75,  212 => 88,  188 => 59,  161 => 57,  144 => 44,  135 => 32,  126 => 29,  172 => 49,  165 => 38,  151 => 65,  116 => 30,  113 => 28,  153 => 50,  145 => 43,  139 => 60,  127 => 38,  112 => 27,  87 => 16,  94 => 118,  91 => 17,  23 => 3,  170 => 62,  125 => 26,  103 => 19,  82 => 9,  549 => 162,  543 => 161,  538 => 158,  530 => 155,  526 => 153,  522 => 151,  512 => 149,  505 => 148,  502 => 147,  497 => 146,  491 => 144,  488 => 143,  483 => 142,  473 => 134,  469 => 132,  466 => 131,  460 => 130,  455 => 129,  450 => 196,  444 => 122,  441 => 121,  437 => 190,  434 => 189,  429 => 185,  423 => 112,  420 => 111,  416 => 110,  413 => 109,  408 => 176,  394 => 105,  390 => 103,  375 => 101,  365 => 99,  362 => 98,  359 => 149,  355 => 95,  348 => 91,  344 => 90,  330 => 89,  327 => 88,  321 => 86,  307 => 85,  302 => 84,  295 => 110,  287 => 80,  279 => 84,  271 => 77,  264 => 95,  256 => 91,  251 => 71,  247 => 107,  239 => 68,  231 => 78,  219 => 74,  201 => 60,  147 => 34,  143 => 45,  134 => 33,  131 => 29,  122 => 24,  102 => 30,  92 => 17,  84 => 114,  72 => 52,  48 => 18,  35 => 5,  29 => 3,  76 => 7,  69 => 5,  54 => 20,  51 => 19,  31 => 11,  205 => 48,  199 => 71,  190 => 58,  182 => 43,  179 => 67,  175 => 58,  168 => 38,  164 => 47,  156 => 34,  148 => 47,  138 => 34,  123 => 30,  117 => 33,  108 => 32,  83 => 14,  71 => 10,  64 => 25,  110 => 28,  89 => 12,  65 => 14,  63 => 17,  58 => 15,  34 => 12,  227 => 58,  224 => 91,  221 => 54,  207 => 82,  197 => 74,  195 => 69,  192 => 68,  189 => 66,  186 => 53,  181 => 64,  178 => 63,  173 => 40,  162 => 47,  158 => 69,  155 => 36,  152 => 30,  142 => 47,  136 => 43,  133 => 40,  130 => 39,  120 => 27,  105 => 31,  100 => 22,  75 => 53,  53 => 13,  39 => 7,  98 => 19,  80 => 12,  78 => 42,  46 => 16,  44 => 10,  36 => 6,  32 => 8,  60 => 21,  57 => 9,  40 => 16,  114 => 25,  109 => 48,  106 => 19,  101 => 20,  88 => 12,  85 => 10,  77 => 7,  67 => 27,  47 => 24,  28 => 18,  25 => 7,  55 => 14,  43 => 17,  38 => 7,  26 => 8,  24 => 3,  50 => 7,  27 => 2,  22 => 6,  19 => 2,  232 => 79,  226 => 78,  222 => 77,  215 => 65,  211 => 84,  208 => 72,  202 => 65,  196 => 59,  193 => 65,  187 => 44,  183 => 60,  180 => 59,  171 => 58,  166 => 51,  163 => 55,  160 => 49,  157 => 43,  149 => 48,  146 => 47,  140 => 32,  137 => 42,  129 => 36,  124 => 36,  121 => 24,  118 => 33,  115 => 32,  111 => 57,  107 => 45,  104 => 19,  97 => 26,  93 => 19,  90 => 17,  81 => 12,  70 => 5,  66 => 13,  62 => 24,  59 => 23,  56 => 21,  52 => 10,  49 => 17,  45 => 6,  41 => 7,  37 => 20,  33 => 4,  30 => 3,);
    }
}


/* LaplaceTrainingBundle:Need:view-all.html.twig */
class __TwigTemplate_994e3e6c7d731cc6d9e74057a33123b9_1951226299 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'PageTitle' => array($this, 'block_PageTitle'),
            'ContentTitle' => array($this, 'block_ContentTitle'),
            'Sidebar' => array($this, 'block_Sidebar'),
            'ContentBody' => array($this, 'block_ContentBody'),
        );
    }

    protected function doGetParent(array $context)
    {
        return $this->env->resolveTemplate($this->getContext($context, "container"));
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 10
    public function block_PageTitle($context, array $blocks = array())
    {
        $this->displayParentBlock("PageTitle", $context, $blocks);
        echo " - Tous les besoins";
    }

    // line 14
    public function block_ContentTitle($context, array $blocks = array())
    {
        echo "Tous les besoins";
    }

    // line 18
    public function block_Sidebar($context, array $blocks = array())
    {
        // line 19
        echo "
";
        // line 20
        $this->displayParentBlock("Sidebar", $context, $blocks);
        echo "

<li class=\"divider\"></li>

<li class=\"nav-header\">Domaines de connaissances</li>
";
        // line 25
        if (isset($context["domains"])) { $_domains_ = $context["domains"]; } else { $_domains_ = null; }
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($_domains_);
        foreach ($context['_seq'] as $context["_key"] => $context["domain"]) {
            // line 26
            echo "<li>
    <a href=\"#dom_";
            // line 27
            if (isset($context["domain"])) { $_domain_ = $context["domain"]; } else { $_domain_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_domain_, "id"), "html", null, true);
            echo "\">";
            if (isset($context["domain"])) { $_domain_ = $context["domain"]; } else { $_domain_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_domain_, "name"), "html", null, true);
            echo "</a>
</li>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['domain'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 30
        echo "
";
    }

    // line 35
    public function block_ContentBody($context, array $blocks = array())
    {
        // line 36
        echo "
<p>
    Cliquez sur un besoin de formation pour <strong>voir sa description complète</strong> ou pour <strong>l'ajouter
    à votre liste</strong> de besoins personnels. Si un besoin ne figure pas dans cette
    liste, vous pouvez en <a href=\"";
        // line 40
        if (isset($context["laplace_training"])) { $_laplace_training_ = $context["laplace_training"]; } else { $_laplace_training_ = null; }
        echo $this->env->getExtension('routing')->getPath(($_laplace_training_ . "create_need"));
        echo "\">créer un nouveau</a>.
</p>

";
        // line 43
        if (isset($context["domains"])) { $_domains_ = $context["domains"]; } else { $_domains_ = null; }
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($_domains_);
        foreach ($context['_seq'] as $context["_key"] => $context["domain"]) {
            // line 44
            echo "<h3 id=\"dom_";
            if (isset($context["domain"])) { $_domain_ = $context["domain"]; } else { $_domain_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_domain_, "id"), "html", null, true);
            echo "\">";
            if (isset($context["domain"])) { $_domain_ = $context["domain"]; } else { $_domain_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_domain_, "name"), "html", null, true);
            echo "</h3>

<table class=\"table table-striped table-bordered table-condensed cat-table\">

    ";
            // line 48
            if (isset($context["domain"])) { $_domain_ = $context["domain"]; } else { $_domain_ = null; }
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($_domain_, "categories"));
            foreach ($context['_seq'] as $context["_key"] => $context["cat"]) {
                // line 49
                echo "    <tr>
        <th>";
                // line 50
                if (isset($context["cat"])) { $_cat_ = $context["cat"]; } else { $_cat_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($_cat_, "name"), "html", null, true);
                echo "</th>
    </tr>

    ";
                // line 53
                if (isset($context["groups"])) { $_groups_ = $context["groups"]; } else { $_groups_ = null; }
                if (isset($context["cat"])) { $_cat_ = $context["cat"]; } else { $_cat_ = null; }
                if (($this->getAttribute($_groups_, $this->getAttribute($_cat_, "id"), array(), "array", true, true) && (!twig_test_empty($this->getAttribute($_groups_, $this->getAttribute($_cat_, "id"), array(), "array"))))) {
                    // line 54
                    echo "    ";
                    if (isset($context["groups"])) { $_groups_ = $context["groups"]; } else { $_groups_ = null; }
                    if (isset($context["cat"])) { $_cat_ = $context["cat"]; } else { $_cat_ = null; }
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($_groups_, $this->getAttribute($_cat_, "id"), array(), "array"));
                    foreach ($context['_seq'] as $context["_key"] => $context["needinfo"]) {
                        // line 55
                        echo "    <tr>
        <td>
            <span class=\"badge";
                        // line 57
                        if (isset($context["needinfo"])) { $_needinfo_ = $context["needinfo"]; } else { $_needinfo_ = null; }
                        if (($this->getAttribute($_needinfo_, "subscriptionCount") > 0)) {
                            echo " badge-info";
                        }
                        echo "\"
                  title=\"Nombre de personnes intéressées\">";
                        // line 58
                        if (isset($context["needinfo"])) { $_needinfo_ = $context["needinfo"]; } else { $_needinfo_ = null; }
                        echo twig_escape_filter($this->env, $this->getAttribute($_needinfo_, "subscriptionCount"), "html", null, true);
                        echo "</span>
            <a href=\"";
                        // line 59
                        if (isset($context["laplace_training"])) { $_laplace_training_ = $context["laplace_training"]; } else { $_laplace_training_ = null; }
                        if (isset($context["needinfo"])) { $_needinfo_ = $context["needinfo"]; } else { $_needinfo_ = null; }
                        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath(($_laplace_training_ . "view_need"), array("id" => $this->getAttribute($this->getAttribute($_needinfo_, "need"), "id"))), "html", null, true);
                        echo "\">";
                        if (isset($context["needinfo"])) { $_needinfo_ = $context["needinfo"]; } else { $_needinfo_ = null; }
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_needinfo_, "need"), "title"), "html", null, true);
                        echo "</a>
        </td>
    </tr>
    ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['needinfo'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 63
                    echo "    ";
                } else {
                    // line 64
                    echo "    <tr>
        <td><em>Vide</em></td>
    </tr>
    ";
                }
                // line 68
                echo "
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['cat'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 70
            echo "
</table>

<hr />
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['domain'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 75
        echo "
";
        // line 76
        if (isset($context["groups"])) { $_groups_ = $context["groups"]; } else { $_groups_ = null; }
        if ((!twig_test_empty($this->getAttribute($_groups_, null, array(), "array")))) {
            // line 77
            echo "<h3 id=\"dom_other\">Autre</h3>

<table class=\"table table-striped table-bordered table-condensed cat-table\">

    <tr>
        <th>Besoins non classés</th>
    </tr>

    ";
            // line 85
            if (isset($context["groups"])) { $_groups_ = $context["groups"]; } else { $_groups_ = null; }
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($_groups_, null, array(), "array"));
            foreach ($context['_seq'] as $context["_key"] => $context["needinfo"]) {
                // line 86
                echo "    <tr>
        <td>
            <span class=\"badge";
                // line 88
                if (isset($context["needinfo"])) { $_needinfo_ = $context["needinfo"]; } else { $_needinfo_ = null; }
                if (($this->getAttribute($_needinfo_, "subscriptionCount") > 0)) {
                    echo " badge-info";
                }
                echo "\"
                  title=\"Nombre de personnes intéressées\">";
                // line 89
                if (isset($context["needinfo"])) { $_needinfo_ = $context["needinfo"]; } else { $_needinfo_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($_needinfo_, "subscriptionCount"), "html", null, true);
                echo "</span>
            <a href=\"";
                // line 90
                if (isset($context["laplace_training"])) { $_laplace_training_ = $context["laplace_training"]; } else { $_laplace_training_ = null; }
                if (isset($context["needinfo"])) { $_needinfo_ = $context["needinfo"]; } else { $_needinfo_ = null; }
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath(($_laplace_training_ . "view_need"), array("id" => $this->getAttribute($this->getAttribute($_needinfo_, "need"), "id"))), "html", null, true);
                echo "\">";
                if (isset($context["needinfo"])) { $_needinfo_ = $context["needinfo"]; } else { $_needinfo_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_needinfo_, "need"), "title"), "html", null, true);
                echo "</a>
        </td>
    </tr>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['needinfo'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 94
            echo "
</table>

<hr />

";
        }
        // line 100
        echo "
<p class=\"text-right\">
    <i class=\"icon-plus\"></i>
    <a href=\"";
        // line 103
        if (isset($context["laplace_training"])) { $_laplace_training_ = $context["laplace_training"]; } else { $_laplace_training_ = null; }
        echo $this->env->getExtension('routing')->getPath(($_laplace_training_ . "create_need"));
        echo "\">Ajouter un besoin qui ne figure pas dans cette liste</a>
</p>

<div class=\"clearfix\"></div>

";
    }

    public function getTemplateName()
    {
        return "LaplaceTrainingBundle:Need:view-all.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  306 => 103,  301 => 100,  278 => 90,  266 => 88,  262 => 86,  200 => 59,  141 => 43,  539 => 243,  533 => 240,  525 => 236,  519 => 234,  513 => 232,  504 => 227,  501 => 226,  499 => 225,  496 => 224,  485 => 215,  482 => 214,  476 => 210,  472 => 208,  467 => 204,  464 => 202,  457 => 199,  442 => 192,  439 => 191,  422 => 182,  414 => 178,  402 => 174,  387 => 163,  384 => 162,  382 => 161,  379 => 160,  374 => 156,  367 => 153,  342 => 136,  339 => 135,  337 => 134,  334 => 133,  329 => 129,  322 => 126,  300 => 112,  297 => 111,  292 => 109,  289 => 107,  283 => 103,  280 => 102,  276 => 101,  269 => 98,  261 => 94,  250 => 89,  241 => 75,  184 => 55,  176 => 51,  73 => 29,  99 => 25,  79 => 14,  257 => 85,  249 => 74,  253 => 90,  235 => 64,  198 => 46,  177 => 54,  174 => 41,  169 => 40,  68 => 29,  61 => 27,  341 => 118,  336 => 101,  331 => 97,  326 => 102,  324 => 101,  317 => 97,  314 => 122,  311 => 95,  305 => 104,  303 => 95,  298 => 92,  291 => 85,  288 => 84,  281 => 86,  273 => 89,  270 => 79,  265 => 107,  263 => 91,  260 => 90,  258 => 79,  252 => 77,  244 => 76,  223 => 67,  218 => 64,  159 => 46,  96 => 40,  86 => 15,  246 => 109,  210 => 69,  245 => 85,  237 => 72,  225 => 76,  209 => 68,  194 => 61,  150 => 35,  95 => 14,  128 => 36,  214 => 52,  191 => 87,  185 => 85,  42 => 15,  293 => 94,  286 => 125,  277 => 123,  267 => 119,  255 => 78,  238 => 81,  229 => 79,  220 => 75,  212 => 88,  188 => 57,  161 => 57,  144 => 44,  135 => 32,  126 => 29,  172 => 49,  165 => 38,  151 => 65,  116 => 30,  113 => 28,  153 => 50,  145 => 43,  139 => 60,  127 => 38,  112 => 27,  87 => 16,  94 => 118,  91 => 20,  23 => 3,  170 => 62,  125 => 35,  103 => 19,  82 => 9,  549 => 162,  543 => 161,  538 => 158,  530 => 155,  526 => 153,  522 => 151,  512 => 149,  505 => 148,  502 => 147,  497 => 146,  491 => 144,  488 => 143,  483 => 142,  473 => 134,  469 => 132,  466 => 131,  460 => 130,  455 => 129,  450 => 196,  444 => 122,  441 => 121,  437 => 190,  434 => 189,  429 => 185,  423 => 112,  420 => 111,  416 => 110,  413 => 109,  408 => 176,  394 => 105,  390 => 103,  375 => 101,  365 => 99,  362 => 98,  359 => 149,  355 => 95,  348 => 91,  344 => 90,  330 => 89,  327 => 88,  321 => 86,  307 => 85,  302 => 84,  295 => 110,  287 => 80,  279 => 84,  271 => 77,  264 => 95,  256 => 91,  251 => 71,  247 => 77,  239 => 68,  231 => 70,  219 => 74,  201 => 60,  147 => 34,  143 => 45,  134 => 40,  131 => 29,  122 => 24,  102 => 30,  92 => 17,  84 => 114,  72 => 10,  48 => 18,  35 => 5,  29 => 3,  76 => 7,  69 => 5,  54 => 20,  51 => 19,  31 => 11,  205 => 48,  199 => 71,  190 => 58,  182 => 43,  179 => 67,  175 => 58,  168 => 38,  164 => 47,  156 => 34,  148 => 47,  138 => 34,  123 => 30,  117 => 33,  108 => 32,  83 => 14,  71 => 10,  64 => 25,  110 => 28,  89 => 12,  65 => 14,  63 => 17,  58 => 15,  34 => 12,  227 => 58,  224 => 68,  221 => 54,  207 => 82,  197 => 74,  195 => 58,  192 => 68,  189 => 66,  186 => 53,  181 => 64,  178 => 63,  173 => 53,  162 => 47,  158 => 48,  155 => 36,  152 => 30,  142 => 47,  136 => 43,  133 => 40,  130 => 39,  120 => 30,  105 => 31,  100 => 22,  75 => 53,  53 => 13,  39 => 7,  98 => 19,  80 => 12,  78 => 42,  46 => 16,  44 => 10,  36 => 6,  32 => 8,  60 => 21,  57 => 9,  40 => 16,  114 => 25,  109 => 48,  106 => 19,  101 => 20,  88 => 19,  85 => 18,  77 => 7,  67 => 27,  47 => 24,  28 => 18,  25 => 7,  55 => 14,  43 => 17,  38 => 7,  26 => 8,  24 => 3,  50 => 7,  27 => 2,  22 => 6,  19 => 2,  232 => 79,  226 => 78,  222 => 77,  215 => 63,  211 => 84,  208 => 72,  202 => 65,  196 => 59,  193 => 65,  187 => 44,  183 => 60,  180 => 59,  171 => 58,  166 => 50,  163 => 49,  160 => 49,  157 => 43,  149 => 48,  146 => 44,  140 => 32,  137 => 42,  129 => 36,  124 => 36,  121 => 24,  118 => 33,  115 => 32,  111 => 57,  107 => 27,  104 => 26,  97 => 26,  93 => 19,  90 => 17,  81 => 12,  70 => 5,  66 => 13,  62 => 24,  59 => 23,  56 => 21,  52 => 10,  49 => 17,  45 => 6,  41 => 7,  37 => 20,  33 => 4,  30 => 3,);
    }
}
