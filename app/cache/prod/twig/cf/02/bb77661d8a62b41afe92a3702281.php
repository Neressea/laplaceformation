<?php

/* ::pagination.html.twig */
class __TwigTemplate_cf02bb77661d8a62b41afe92a3702281 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 13
        echo "
<div class=\"pagination pagination-centered\">
    <ul>
        ";
        // line 16
        if (isset($context["years"])) { $_years_ = $context["years"]; } else { $_years_ = null; }
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($_years_);
        foreach ($context['_seq'] as $context["_key"] => $context["year"]) {
            // line 17
            echo "
        <li";
            // line 18
            if (isset($context["year"])) { $_year_ = $context["year"]; } else { $_year_ = null; }
            if (isset($context["requested"])) { $_requested_ = $context["requested"]; } else { $_requested_ = null; }
            echo ((($_year_ == $_requested_)) ? (" class=\"active\"") : (""));
            echo ">
            <a href=\"";
            // line 19
            if (isset($context["route_name"])) { $_route_name_ = $context["route_name"]; } else { $_route_name_ = null; }
            if (isset($context["route_args"])) { $_route_args_ = $context["route_args"]; } else { $_route_args_ = null; }
            if (isset($context["year"])) { $_year_ = $context["year"]; } else { $_year_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath($_route_name_, twig_array_merge($_route_args_, array("year" => $_year_))), "html", null, true);
            echo "\">";
            if (isset($context["year"])) { $_year_ = $context["year"]; } else { $_year_ = null; }
            echo twig_escape_filter($this->env, $_year_, "html", null, true);
            echo "</a>
        </li>

        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['year'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 23
        echo "    </ul>
</div>
";
    }

    public function getTemplateName()
    {
        return "::pagination.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  216 => 100,  74 => 17,  1013 => 350,  1009 => 348,  999 => 346,  994 => 345,  990 => 343,  986 => 342,  983 => 341,  978 => 338,  971 => 336,  967 => 335,  938 => 334,  934 => 333,  931 => 331,  928 => 330,  925 => 329,  922 => 328,  918 => 327,  914 => 326,  911 => 325,  908 => 324,  905 => 323,  895 => 314,  885 => 312,  878 => 309,  874 => 307,  871 => 306,  864 => 301,  861 => 300,  858 => 299,  849 => 293,  845 => 291,  842 => 290,  831 => 282,  824 => 280,  819 => 279,  800 => 278,  792 => 275,  789 => 274,  785 => 273,  782 => 272,  776 => 268,  773 => 267,  768 => 264,  759 => 261,  734 => 257,  731 => 256,  727 => 255,  721 => 252,  717 => 251,  714 => 250,  711 => 249,  708 => 248,  703 => 247,  699 => 246,  696 => 245,  693 => 244,  680 => 237,  675 => 236,  668 => 233,  664 => 231,  661 => 230,  654 => 225,  647 => 223,  622 => 217,  617 => 216,  613 => 215,  607 => 213,  599 => 211,  596 => 210,  592 => 209,  590 => 208,  587 => 207,  582 => 204,  576 => 200,  574 => 199,  573 => 198,  572 => 197,  569 => 196,  565 => 195,  562 => 194,  552 => 191,  545 => 189,  542 => 188,  524 => 109,  521 => 108,  518 => 107,  515 => 106,  500 => 101,  493 => 99,  489 => 98,  486 => 97,  480 => 95,  463 => 87,  436 => 81,  404 => 77,  401 => 76,  392 => 69,  388 => 68,  385 => 67,  369 => 63,  361 => 61,  328 => 56,  325 => 55,  319 => 53,  259 => 38,  242 => 33,  234 => 28,  299 => 97,  217 => 64,  167 => 9,  910 => 400,  904 => 397,  896 => 393,  890 => 313,  884 => 389,  875 => 384,  872 => 383,  870 => 382,  867 => 381,  856 => 372,  853 => 371,  848 => 368,  843 => 364,  840 => 362,  836 => 360,  828 => 354,  822 => 350,  804 => 347,  795 => 277,  777 => 345,  771 => 341,  755 => 259,  752 => 258,  748 => 336,  742 => 335,  724 => 253,  710 => 322,  707 => 321,  704 => 320,  701 => 318,  697 => 316,  690 => 243,  683 => 310,  653 => 284,  650 => 224,  648 => 282,  645 => 281,  640 => 218,  633 => 274,  625 => 270,  611 => 260,  608 => 259,  606 => 258,  603 => 212,  598 => 253,  591 => 250,  583 => 246,  577 => 244,  571 => 242,  556 => 192,  553 => 230,  551 => 229,  548 => 190,  536 => 186,  528 => 110,  514 => 207,  511 => 206,  509 => 205,  506 => 204,  503 => 202,  492 => 195,  487 => 192,  481 => 188,  475 => 184,  461 => 176,  454 => 171,  451 => 170,  446 => 85,  443 => 166,  432 => 162,  428 => 160,  425 => 79,  412 => 150,  409 => 149,  406 => 78,  403 => 147,  398 => 144,  393 => 140,  389 => 137,  371 => 133,  357 => 128,  338 => 122,  333 => 121,  320 => 114,  312 => 111,  284 => 97,  275 => 94,  236 => 110,  233 => 80,  230 => 71,  154 => 45,  378 => 65,  370 => 143,  356 => 137,  346 => 131,  316 => 120,  310 => 119,  290 => 99,  274 => 90,  254 => 87,  248 => 35,  240 => 91,  203 => 60,  132 => 299,  213 => 71,  206 => 95,  119 => 23,  306 => 98,  301 => 100,  278 => 95,  266 => 40,  262 => 39,  200 => 59,  141 => 50,  539 => 187,  533 => 240,  525 => 236,  519 => 234,  513 => 232,  504 => 227,  501 => 226,  499 => 200,  496 => 100,  485 => 215,  482 => 214,  476 => 210,  472 => 208,  467 => 204,  464 => 202,  457 => 199,  442 => 192,  439 => 82,  422 => 158,  414 => 178,  402 => 174,  387 => 163,  384 => 162,  382 => 161,  379 => 160,  374 => 64,  367 => 131,  342 => 59,  339 => 58,  337 => 134,  334 => 109,  329 => 129,  322 => 54,  300 => 112,  297 => 111,  292 => 109,  289 => 47,  283 => 45,  280 => 102,  276 => 43,  269 => 92,  261 => 81,  250 => 89,  241 => 73,  184 => 65,  176 => 53,  73 => 32,  99 => 17,  79 => 14,  257 => 100,  249 => 74,  253 => 82,  235 => 69,  198 => 92,  177 => 82,  174 => 54,  169 => 40,  68 => 31,  61 => 13,  341 => 124,  336 => 101,  331 => 57,  326 => 103,  324 => 101,  317 => 97,  314 => 122,  311 => 99,  305 => 104,  303 => 95,  298 => 104,  291 => 85,  288 => 84,  281 => 96,  273 => 42,  270 => 41,  265 => 102,  263 => 90,  260 => 89,  258 => 80,  252 => 77,  244 => 79,  223 => 67,  218 => 67,  159 => 7,  96 => 43,  86 => 10,  246 => 109,  210 => 69,  245 => 34,  237 => 81,  225 => 77,  209 => 97,  194 => 62,  150 => 69,  95 => 27,  128 => 26,  214 => 52,  191 => 65,  185 => 86,  42 => 10,  293 => 96,  286 => 46,  277 => 107,  267 => 119,  255 => 78,  238 => 81,  229 => 107,  220 => 70,  212 => 98,  188 => 87,  161 => 57,  144 => 322,  135 => 41,  126 => 57,  172 => 79,  165 => 76,  151 => 65,  116 => 22,  113 => 21,  153 => 70,  145 => 33,  139 => 43,  127 => 290,  112 => 230,  87 => 38,  94 => 22,  91 => 41,  23 => 3,  170 => 42,  125 => 25,  103 => 26,  82 => 37,  549 => 162,  543 => 224,  538 => 158,  530 => 155,  526 => 153,  522 => 151,  512 => 149,  505 => 148,  502 => 147,  497 => 146,  491 => 144,  488 => 143,  483 => 96,  473 => 134,  469 => 180,  466 => 179,  460 => 130,  455 => 129,  450 => 196,  444 => 122,  441 => 121,  437 => 190,  434 => 189,  429 => 80,  423 => 112,  420 => 111,  416 => 110,  413 => 109,  408 => 176,  394 => 105,  390 => 103,  375 => 134,  365 => 62,  362 => 139,  359 => 149,  355 => 95,  348 => 127,  344 => 125,  330 => 120,  327 => 119,  321 => 86,  307 => 48,  302 => 106,  295 => 103,  287 => 80,  279 => 44,  271 => 77,  264 => 82,  256 => 37,  251 => 86,  247 => 80,  239 => 76,  231 => 27,  219 => 102,  201 => 60,  147 => 323,  143 => 66,  134 => 61,  131 => 60,  122 => 24,  102 => 222,  92 => 41,  84 => 104,  72 => 16,  48 => 10,  35 => 6,  29 => 17,  76 => 8,  69 => 30,  54 => 23,  51 => 11,  31 => 11,  205 => 20,  199 => 92,  190 => 58,  182 => 84,  179 => 58,  175 => 81,  168 => 78,  164 => 50,  156 => 6,  148 => 56,  138 => 306,  123 => 30,  117 => 243,  108 => 23,  83 => 9,  71 => 17,  64 => 29,  110 => 49,  89 => 19,  65 => 30,  63 => 29,  58 => 15,  34 => 18,  227 => 58,  224 => 69,  221 => 25,  207 => 82,  197 => 74,  195 => 91,  192 => 88,  189 => 87,  186 => 85,  181 => 53,  178 => 82,  173 => 53,  162 => 74,  158 => 72,  155 => 71,  152 => 69,  142 => 54,  136 => 59,  133 => 40,  130 => 33,  120 => 30,  105 => 48,  100 => 45,  75 => 19,  53 => 23,  39 => 20,  98 => 19,  80 => 35,  78 => 35,  46 => 22,  44 => 21,  36 => 8,  32 => 18,  60 => 18,  57 => 13,  40 => 7,  114 => 242,  109 => 31,  106 => 47,  101 => 44,  88 => 19,  85 => 19,  77 => 7,  67 => 19,  47 => 22,  28 => 15,  25 => 7,  55 => 24,  43 => 11,  38 => 19,  26 => 8,  24 => 16,  50 => 10,  27 => 3,  22 => 2,  19 => 13,  232 => 108,  226 => 105,  222 => 103,  215 => 24,  211 => 64,  208 => 21,  202 => 93,  196 => 90,  193 => 54,  187 => 58,  183 => 84,  180 => 83,  171 => 79,  166 => 40,  163 => 39,  160 => 49,  157 => 37,  149 => 68,  146 => 67,  140 => 63,  137 => 62,  129 => 51,  124 => 32,  121 => 62,  118 => 30,  115 => 50,  111 => 48,  107 => 19,  104 => 18,  97 => 42,  93 => 24,  90 => 12,  81 => 19,  70 => 5,  66 => 13,  62 => 14,  59 => 13,  56 => 12,  52 => 13,  49 => 23,  45 => 9,  41 => 11,  37 => 19,  33 => 5,  30 => 16,);
    }
}
