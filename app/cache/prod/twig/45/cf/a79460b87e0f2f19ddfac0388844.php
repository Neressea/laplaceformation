<?php

/* LaplaceTrainingBundle:Statistics:needs-summary.html.twig */
class __TwigTemplate_45cfa79460b87e0f2f19ddfac0388844 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
";
        // line 2
        $this->env->loadTemplate("LaplaceTrainingBundle:Statistics:needs-summary.html.twig", "1916242692")->display(array_merge($context, array("page" => array(0 => "system", 1 => "needs-summary"))));
    }

    public function getTemplateName()
    {
        return "LaplaceTrainingBundle:Statistics:needs-summary.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  68 => 29,  61 => 27,  341 => 118,  336 => 101,  331 => 97,  326 => 102,  324 => 101,  317 => 97,  314 => 96,  311 => 95,  305 => 104,  303 => 95,  298 => 92,  291 => 85,  288 => 84,  281 => 86,  273 => 80,  270 => 79,  265 => 107,  263 => 91,  260 => 90,  258 => 79,  252 => 77,  244 => 77,  223 => 67,  218 => 66,  159 => 35,  96 => 119,  86 => 45,  246 => 109,  210 => 69,  245 => 84,  237 => 72,  225 => 76,  209 => 68,  194 => 61,  150 => 41,  95 => 14,  128 => 37,  214 => 96,  191 => 87,  185 => 85,  42 => 15,  293 => 127,  286 => 125,  277 => 123,  267 => 119,  255 => 78,  238 => 81,  229 => 97,  220 => 75,  212 => 88,  188 => 59,  161 => 57,  144 => 44,  135 => 32,  126 => 32,  172 => 51,  165 => 37,  151 => 65,  116 => 22,  113 => 21,  153 => 45,  145 => 61,  139 => 60,  127 => 33,  112 => 21,  87 => 13,  94 => 118,  91 => 48,  23 => 3,  170 => 62,  125 => 26,  103 => 19,  82 => 9,  549 => 162,  543 => 161,  538 => 158,  530 => 155,  526 => 153,  522 => 151,  512 => 149,  505 => 148,  502 => 147,  497 => 146,  491 => 144,  488 => 143,  483 => 142,  473 => 134,  469 => 132,  466 => 131,  460 => 130,  455 => 129,  450 => 126,  444 => 122,  441 => 121,  437 => 120,  434 => 119,  429 => 116,  423 => 112,  420 => 111,  416 => 110,  413 => 109,  408 => 106,  394 => 105,  390 => 103,  375 => 101,  365 => 99,  362 => 98,  359 => 97,  355 => 95,  348 => 91,  344 => 90,  330 => 89,  327 => 88,  321 => 86,  307 => 85,  302 => 84,  295 => 91,  287 => 80,  279 => 84,  271 => 77,  264 => 118,  256 => 73,  251 => 71,  247 => 107,  239 => 69,  231 => 78,  219 => 74,  201 => 60,  147 => 51,  143 => 49,  134 => 36,  131 => 29,  122 => 35,  102 => 7,  92 => 25,  84 => 114,  72 => 52,  48 => 11,  35 => 5,  29 => 3,  76 => 7,  69 => 5,  54 => 14,  51 => 25,  31 => 19,  205 => 62,  199 => 91,  190 => 66,  182 => 57,  179 => 67,  175 => 58,  168 => 38,  164 => 47,  156 => 34,  148 => 47,  138 => 34,  123 => 30,  117 => 28,  108 => 13,  83 => 44,  71 => 10,  64 => 16,  110 => 20,  89 => 116,  65 => 14,  63 => 17,  58 => 15,  34 => 5,  227 => 69,  224 => 91,  221 => 90,  207 => 82,  197 => 74,  195 => 65,  192 => 72,  189 => 54,  186 => 53,  181 => 44,  178 => 83,  173 => 40,  162 => 36,  158 => 69,  155 => 55,  152 => 30,  142 => 47,  136 => 41,  133 => 31,  130 => 28,  120 => 24,  105 => 25,  100 => 18,  75 => 53,  53 => 13,  39 => 7,  98 => 52,  80 => 113,  78 => 42,  46 => 16,  44 => 10,  36 => 6,  32 => 8,  60 => 21,  57 => 9,  40 => 12,  114 => 25,  109 => 48,  106 => 19,  101 => 53,  88 => 12,  85 => 10,  77 => 112,  67 => 9,  47 => 24,  28 => 18,  25 => 17,  55 => 14,  43 => 22,  38 => 7,  26 => 8,  24 => 3,  50 => 7,  27 => 2,  22 => 2,  19 => 1,  232 => 79,  226 => 77,  222 => 76,  215 => 65,  211 => 84,  208 => 95,  202 => 65,  196 => 59,  193 => 65,  187 => 62,  183 => 60,  180 => 59,  171 => 54,  166 => 51,  163 => 50,  160 => 49,  157 => 43,  149 => 29,  146 => 40,  140 => 46,  137 => 37,  129 => 36,  124 => 35,  121 => 24,  118 => 29,  115 => 59,  111 => 57,  107 => 28,  104 => 54,  97 => 16,  93 => 16,  90 => 13,  81 => 12,  70 => 21,  66 => 13,  62 => 11,  59 => 15,  56 => 12,  52 => 10,  49 => 17,  45 => 6,  41 => 7,  37 => 20,  33 => 4,  30 => 3,);
    }
}


/* LaplaceTrainingBundle:Statistics:needs-summary.html.twig */
class __TwigTemplate_45cfa79460b87e0f2f19ddfac0388844_1916242692 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("LaplaceCommonBundle::admin-page.html.twig");

        $this->blocks = array(
            'PageTitle' => array($this, 'block_PageTitle'),
            'ContentTitle' => array($this, 'block_ContentTitle'),
            'ContentBody' => array($this, 'block_ContentBody'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "LaplaceCommonBundle::admin-page.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_PageTitle($context, array $blocks = array())
    {
        $this->displayParentBlock("PageTitle", $context, $blocks);
        echo " - Résumé des besoins non-pourvus";
    }

    // line 7
    public function block_ContentTitle($context, array $blocks = array())
    {
        echo "Résumé des besoins non pourvus";
    }

    // line 9
    public function block_ContentBody($context, array $blocks = array())
    {
        // line 10
        echo "


";
        // line 13
        if (array_key_exists("summary", $context)) {
            // line 14
            echo "
<p>
    Du <strong>";
            // line 16
            if (isset($context["summary"])) { $_summary_ = $context["summary"]; } else { $_summary_ = null; }
            echo twig_escape_filter($this->env, twig_localized_date_filter($this->env, $this->getAttribute($_summary_, "from"), "full", "short"), "html", null, true);
            echo "</strong> au
    <strong>";
            // line 17
            if (isset($context["summary"])) { $_summary_ = $context["summary"]; } else { $_summary_ = null; }
            echo twig_escape_filter($this->env, twig_localized_date_filter($this->env, $this->getAttribute($_summary_, "to"), "full", "short"), "html", null, true);
            echo "</strong>.
</p>

";
            // line 20
            if (isset($context["summary"])) { $_summary_ = $context["summary"]; } else { $_summary_ = null; }
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($_summary_, "types"));
            foreach ($context['_seq'] as $context["type"] => $context["section"]) {
                // line 21
                echo "
<h5>";
                // line 22
                if (isset($context["type"])) { $_type_ = $context["type"]; } else { $_type_ = null; }
                echo twig_escape_filter($this->env, $_type_, "html", null, true);
                echo "</h5>

    ";
                // line 24
                if (isset($context["section"])) { $_section_ = $context["section"]; } else { $_section_ = null; }
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($_section_, "categories"));
                foreach ($context['_seq'] as $context["category"] => $context["table"]) {
                    // line 25
                    echo "
    <table class=\"table table-striped table-bordered needs-summary-table\">
        <tr>
            <th class=\"needs-summary-cat\">
                ";
                    // line 29
                    if (isset($context["category"])) { $_category_ = $context["category"]; } else { $_category_ = null; }
                    if (twig_test_empty($_category_)) {
                        // line 30
                        echo "                <em>Non classés</em>
                ";
                    } else {
                        // line 32
                        echo "                ";
                        if (isset($context["category"])) { $_category_ = $context["category"]; } else { $_category_ = null; }
                        echo twig_escape_filter($this->env, $_category_, "html", null, true);
                        echo "
                ";
                    }
                    // line 34
                    echo "            </th>
            ";
                    // line 35
                    if (isset($context["summary"])) { $_summary_ = $context["summary"]; } else { $_summary_ = null; }
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($_summary_, "columns"));
                    foreach ($context['_seq'] as $context["_key"] => $context["tutelle"]) {
                        // line 36
                        echo "            <th class=\"needs-summary-tutelle\">";
                        if (isset($context["tutelle"])) { $_tutelle_ = $context["tutelle"]; } else { $_tutelle_ = null; }
                        echo twig_escape_filter($this->env, $_tutelle_, "html", null, true);
                        echo "</th>
            ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tutelle'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 38
                    echo "        </tr>

        ";
                    // line 40
                    if (isset($context["table"])) { $_table_ = $context["table"]; } else { $_table_ = null; }
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($_table_, "rows"));
                    foreach ($context['_seq'] as $context["row"] => $context["counters"]) {
                        // line 41
                        echo "        <tr>
            <td>";
                        // line 42
                        if (isset($context["row"])) { $_row_ = $context["row"]; } else { $_row_ = null; }
                        echo twig_escape_filter($this->env, $_row_, "html", null, true);
                        echo "</td>
            ";
                        // line 43
                        if (isset($context["summary"])) { $_summary_ = $context["summary"]; } else { $_summary_ = null; }
                        $context['_parent'] = (array) $context;
                        $context['_seq'] = twig_ensure_traversable($this->getAttribute($_summary_, "columns"));
                        foreach ($context['_seq'] as $context["_key"] => $context["tutelle"]) {
                            // line 44
                            echo "            <td>";
                            if (isset($context["counters"])) { $_counters_ = $context["counters"]; } else { $_counters_ = null; }
                            if (isset($context["tutelle"])) { $_tutelle_ = $context["tutelle"]; } else { $_tutelle_ = null; }
                            echo twig_escape_filter($this->env, (($this->getAttribute($_counters_, "get", array(0 => $_tutelle_), "method", true, true)) ? (_twig_default_filter($this->getAttribute($_counters_, "get", array(0 => $_tutelle_), "method"), "")) : ("")), "html", null, true);
                            echo "</td>
            ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tutelle'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 46
                        echo "        </tr>
        ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['row'], $context['counters'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 48
                    echo "
    </table>

    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['category'], $context['table'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 52
                echo "
";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['type'], $context['section'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 54
            echo "
<hr />

";
        }
        // line 58
        echo "
<h3>Générer un bilan</h3>

<p>Cet outil permet de générer un résumé des besoins non pourvus qui ont été enregistrés sur la période demandée.</p>


";
        // line 64
        if (isset($context["summary_form"])) { $_summary_form_ = $context["summary_form"]; } else { $_summary_form_ = null; }
        echo         $this->env->getExtension('form')->renderer->renderBlock($_summary_form_, 'form_start');
        echo "

<fielset>

    <legend>Choisir une période</legend>

    ";
        // line 70
        if (isset($context["summary_form"])) { $_summary_form_ = $context["summary_form"]; } else { $_summary_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($_summary_form_, 'rest');
        echo "

</fieldset>

";
        // line 74
        if (isset($context["summary_form"])) { $_summary_form_ = $context["summary_form"]; } else { $_summary_form_ = null; }
        echo         $this->env->getExtension('form')->renderer->renderBlock($_summary_form_, 'form_end');
        echo "

";
    }

    public function getTemplateName()
    {
        return "LaplaceTrainingBundle:Statistics:needs-summary.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  253 => 74,  235 => 64,  198 => 46,  177 => 42,  174 => 41,  169 => 40,  68 => 29,  61 => 27,  341 => 118,  336 => 101,  331 => 97,  326 => 102,  324 => 101,  317 => 97,  314 => 96,  311 => 95,  305 => 104,  303 => 95,  298 => 92,  291 => 85,  288 => 84,  281 => 86,  273 => 80,  270 => 79,  265 => 107,  263 => 91,  260 => 90,  258 => 79,  252 => 77,  244 => 77,  223 => 67,  218 => 66,  159 => 35,  96 => 16,  86 => 45,  246 => 109,  210 => 69,  245 => 70,  237 => 72,  225 => 76,  209 => 68,  194 => 61,  150 => 35,  95 => 14,  128 => 37,  214 => 52,  191 => 87,  185 => 85,  42 => 15,  293 => 127,  286 => 125,  277 => 123,  267 => 119,  255 => 78,  238 => 81,  229 => 97,  220 => 75,  212 => 88,  188 => 59,  161 => 57,  144 => 44,  135 => 32,  126 => 32,  172 => 51,  165 => 38,  151 => 65,  116 => 22,  113 => 21,  153 => 45,  145 => 61,  139 => 60,  127 => 25,  112 => 21,  87 => 13,  94 => 118,  91 => 48,  23 => 3,  170 => 62,  125 => 26,  103 => 19,  82 => 9,  549 => 162,  543 => 161,  538 => 158,  530 => 155,  526 => 153,  522 => 151,  512 => 149,  505 => 148,  502 => 147,  497 => 146,  491 => 144,  488 => 143,  483 => 142,  473 => 134,  469 => 132,  466 => 131,  460 => 130,  455 => 129,  450 => 126,  444 => 122,  441 => 121,  437 => 120,  434 => 119,  429 => 116,  423 => 112,  420 => 111,  416 => 110,  413 => 109,  408 => 106,  394 => 105,  390 => 103,  375 => 101,  365 => 99,  362 => 98,  359 => 97,  355 => 95,  348 => 91,  344 => 90,  330 => 89,  327 => 88,  321 => 86,  307 => 85,  302 => 84,  295 => 91,  287 => 80,  279 => 84,  271 => 77,  264 => 118,  256 => 73,  251 => 71,  247 => 107,  239 => 69,  231 => 78,  219 => 74,  201 => 60,  147 => 34,  143 => 49,  134 => 36,  131 => 29,  122 => 24,  102 => 7,  92 => 14,  84 => 114,  72 => 52,  48 => 11,  35 => 5,  29 => 3,  76 => 7,  69 => 5,  54 => 14,  51 => 25,  31 => 19,  205 => 48,  199 => 91,  190 => 66,  182 => 43,  179 => 67,  175 => 58,  168 => 38,  164 => 47,  156 => 34,  148 => 47,  138 => 34,  123 => 30,  117 => 28,  108 => 20,  83 => 44,  71 => 10,  64 => 16,  110 => 20,  89 => 116,  65 => 14,  63 => 17,  58 => 15,  34 => 5,  227 => 58,  224 => 91,  221 => 54,  207 => 82,  197 => 74,  195 => 65,  192 => 72,  189 => 54,  186 => 53,  181 => 44,  178 => 83,  173 => 40,  162 => 36,  158 => 69,  155 => 36,  152 => 30,  142 => 47,  136 => 30,  133 => 29,  130 => 28,  120 => 24,  105 => 25,  100 => 18,  75 => 53,  53 => 13,  39 => 7,  98 => 52,  80 => 113,  78 => 42,  46 => 16,  44 => 10,  36 => 6,  32 => 8,  60 => 21,  57 => 9,  40 => 12,  114 => 25,  109 => 48,  106 => 19,  101 => 17,  88 => 12,  85 => 10,  77 => 112,  67 => 9,  47 => 24,  28 => 18,  25 => 17,  55 => 14,  43 => 22,  38 => 7,  26 => 8,  24 => 3,  50 => 7,  27 => 2,  22 => 2,  19 => 1,  232 => 79,  226 => 77,  222 => 76,  215 => 65,  211 => 84,  208 => 95,  202 => 65,  196 => 59,  193 => 65,  187 => 44,  183 => 60,  180 => 59,  171 => 54,  166 => 51,  163 => 50,  160 => 49,  157 => 43,  149 => 29,  146 => 40,  140 => 32,  137 => 37,  129 => 36,  124 => 35,  121 => 24,  118 => 29,  115 => 59,  111 => 57,  107 => 28,  104 => 54,  97 => 16,  93 => 16,  90 => 13,  81 => 12,  70 => 21,  66 => 13,  62 => 11,  59 => 15,  56 => 12,  52 => 10,  49 => 17,  45 => 6,  41 => 7,  37 => 20,  33 => 4,  30 => 3,);
    }
}
