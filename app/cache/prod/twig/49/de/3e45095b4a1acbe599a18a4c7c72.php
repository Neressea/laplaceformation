<?php

/* LaplaceTrainingBundle:Event:table.html.twig */
class __TwigTemplate_49de3e45095b4a1acbe599a18a4c7c72 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "

<table class=\"table table-striped table-bordered evt-table\">

    <tr>
        <th class=\"evt-date\">Date</th>
        <th class=\"evt-text\">Evènement</th>
        <th class=\"evt-link\">Lien</th>
    </tr>

    ";
        // line 11
        if (isset($context["events"])) { $_events_ = $context["events"]; } else { $_events_ = null; }
        if (twig_test_empty($_events_)) {
            // line 12
            echo "    <tr>
        <td colspan=\"3\"><em>Aucun évènement</em></td>
    </tr>
    ";
        }
        // line 16
        echo "
    ";
        // line 17
        if (isset($context["events"])) { $_events_ = $context["events"]; } else { $_events_ = null; }
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($_events_);
        foreach ($context['_seq'] as $context["_key"] => $context["evt"]) {
            // line 18
            echo "
    ";
            // line 19
            if (isset($context["evt"])) { $_evt_ = $context["evt"]; } else { $_evt_ = null; }
            if ($this->getAttribute($_evt_, "isNeedRelated")) {
                // line 20
                echo "        ";
                // line 21
                if (isset($context["evt"])) { $_evt_ = $context["evt"]; } else { $_evt_ = null; }
                $context["link"] = $this->env->getExtension('routing')->getPath("laplace_training_view_need", array("id" => $this->getAttribute($_evt_, "referenceId")));
                // line 23
                echo "    ";
            } elseif ($this->getAttribute($_evt_, "isRequestRelated")) {
                // line 24
                echo "        ";
                // line 25
                if (isset($context["evt"])) { $_evt_ = $context["evt"]; } else { $_evt_ = null; }
                $context["link"] = $this->env->getExtension('routing')->getPath("laplace_training_view_request", array("id" => $this->getAttribute($_evt_, "referenceId")));
                // line 27
                echo "    ";
            } else {
                // line 28
                echo "        ";
                $context["link"] = null;
                // line 29
                echo "    ";
            }
            // line 30
            echo "
    <tr>

        <td class=\"evt-date\">
            ";
            // line 34
            if (isset($context["evt"])) { $_evt_ = $context["evt"]; } else { $_evt_ = null; }
            echo twig_escape_filter($this->env, twig_localized_date_filter($this->env, $this->getAttribute($_evt_, "date"), "medium", "none"), "html", null, true);
            echo "</span>
        </td>
        <td class=\"evt-text\">
            ";
            // line 37
            if (isset($context["evt"])) { $_evt_ = $context["evt"]; } else { $_evt_ = null; }
            if ($this->getAttribute($_evt_, "isUserRelated")) {
                // line 38
                echo "                <i class=\"icon-user\"></i>
            ";
            } elseif ($this->getAttribute($_evt_, "isNeedRelated")) {
                // line 40
                echo "                <i class=\"icon-star-empty\"></i>
            ";
            } elseif ($this->getAttribute($_evt_, "isRequestRelated")) {
                // line 42
                echo "                <i class=\"icon-star\"></i>
            ";
            }
            // line 44
            echo "
            ";
            // line 45
            if (isset($context["evt"])) { $_evt_ = $context["evt"]; } else { $_evt_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_evt_, "message"), "html", null, true);
            echo "
        </td>
        <td class=\"evt-link\">
            ";
            // line 48
            if (isset($context["link"])) { $_link_ = $context["link"]; } else { $_link_ = null; }
            if ($_link_) {
                // line 49
                echo "                <i class=\"icon-hand-right\"></i> <a href=\"";
                if (isset($context["link"])) { $_link_ = $context["link"]; } else { $_link_ = null; }
                echo twig_escape_filter($this->env, $_link_, "html", null, true);
                echo "\">Voir</a>
            ";
            }
            // line 51
            echo "        </td>

    </tr>

    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['evt'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 56
        echo "
</table>
";
    }

    public function getTemplateName()
    {
        return "LaplaceTrainingBundle:Event:table.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 29,  99 => 19,  79 => 7,  257 => 78,  249 => 74,  253 => 74,  235 => 64,  198 => 46,  177 => 42,  174 => 41,  169 => 40,  68 => 29,  61 => 27,  341 => 118,  336 => 101,  331 => 97,  326 => 102,  324 => 101,  317 => 97,  314 => 96,  311 => 95,  305 => 104,  303 => 95,  298 => 92,  291 => 85,  288 => 84,  281 => 86,  273 => 80,  270 => 79,  265 => 107,  263 => 91,  260 => 90,  258 => 79,  252 => 77,  244 => 77,  223 => 67,  218 => 66,  159 => 35,  96 => 40,  86 => 45,  246 => 109,  210 => 69,  245 => 70,  237 => 72,  225 => 76,  209 => 68,  194 => 61,  150 => 35,  95 => 14,  128 => 37,  214 => 52,  191 => 87,  185 => 85,  42 => 15,  293 => 127,  286 => 125,  277 => 123,  267 => 119,  255 => 78,  238 => 81,  229 => 97,  220 => 75,  212 => 88,  188 => 59,  161 => 57,  144 => 44,  135 => 32,  126 => 32,  172 => 51,  165 => 38,  151 => 65,  116 => 22,  113 => 21,  153 => 45,  145 => 61,  139 => 60,  127 => 25,  112 => 27,  87 => 11,  94 => 118,  91 => 13,  23 => 3,  170 => 62,  125 => 26,  103 => 19,  82 => 34,  549 => 162,  543 => 161,  538 => 158,  530 => 155,  526 => 153,  522 => 151,  512 => 149,  505 => 148,  502 => 147,  497 => 146,  491 => 144,  488 => 143,  483 => 142,  473 => 134,  469 => 132,  466 => 131,  460 => 130,  455 => 129,  450 => 126,  444 => 122,  441 => 121,  437 => 120,  434 => 119,  429 => 116,  423 => 112,  420 => 111,  416 => 110,  413 => 109,  408 => 106,  394 => 105,  390 => 103,  375 => 101,  365 => 99,  362 => 98,  359 => 97,  355 => 95,  348 => 91,  344 => 90,  330 => 89,  327 => 88,  321 => 86,  307 => 85,  302 => 84,  295 => 91,  287 => 80,  279 => 84,  271 => 77,  264 => 118,  256 => 73,  251 => 71,  247 => 107,  239 => 68,  231 => 78,  219 => 74,  201 => 60,  147 => 34,  143 => 49,  134 => 56,  131 => 29,  122 => 24,  102 => 20,  92 => 38,  84 => 114,  72 => 52,  48 => 18,  35 => 5,  29 => 3,  76 => 30,  69 => 5,  54 => 20,  51 => 19,  31 => 11,  205 => 48,  199 => 91,  190 => 66,  182 => 43,  179 => 67,  175 => 58,  168 => 38,  164 => 47,  156 => 34,  148 => 47,  138 => 34,  123 => 30,  117 => 49,  108 => 23,  83 => 44,  71 => 10,  64 => 25,  110 => 25,  89 => 37,  65 => 14,  63 => 17,  58 => 15,  34 => 12,  227 => 58,  224 => 91,  221 => 54,  207 => 82,  197 => 74,  195 => 65,  192 => 72,  189 => 54,  186 => 53,  181 => 44,  178 => 83,  173 => 40,  162 => 36,  158 => 69,  155 => 36,  152 => 30,  142 => 47,  136 => 30,  133 => 29,  130 => 28,  120 => 24,  105 => 22,  100 => 42,  75 => 53,  53 => 13,  39 => 7,  98 => 52,  80 => 8,  78 => 42,  46 => 16,  44 => 10,  36 => 6,  32 => 8,  60 => 21,  57 => 9,  40 => 16,  114 => 48,  109 => 48,  106 => 19,  101 => 21,  88 => 12,  85 => 10,  77 => 112,  67 => 27,  47 => 24,  28 => 18,  25 => 17,  55 => 14,  43 => 17,  38 => 7,  26 => 8,  24 => 3,  50 => 7,  27 => 2,  22 => 2,  19 => 1,  232 => 79,  226 => 77,  222 => 76,  215 => 65,  211 => 84,  208 => 95,  202 => 65,  196 => 59,  193 => 65,  187 => 44,  183 => 60,  180 => 59,  171 => 54,  166 => 51,  163 => 50,  160 => 49,  157 => 43,  149 => 29,  146 => 40,  140 => 32,  137 => 37,  129 => 36,  124 => 51,  121 => 24,  118 => 29,  115 => 59,  111 => 57,  107 => 45,  104 => 44,  97 => 17,  93 => 16,  90 => 12,  81 => 12,  70 => 28,  66 => 13,  62 => 24,  59 => 23,  56 => 21,  52 => 10,  49 => 17,  45 => 6,  41 => 7,  37 => 20,  33 => 4,  30 => 3,);
    }
}
