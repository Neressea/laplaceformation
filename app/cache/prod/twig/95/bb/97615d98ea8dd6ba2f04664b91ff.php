<?php

/* LaplaceCommonBundle:Sidebar:adm-system.html.twig */
class __TwigTemplate_95bb97615d98ea8dd6ba2f04664b91ff extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
<li class=\"nav-header\">Gestion du système</li>
<li";
        // line 3
        if (isset($context["page"])) { $_page_ = $context["page"]; } else { $_page_ = null; }
        echo ((($this->getAttribute($_page_, 1, array(), "array") == "dashboard")) ? (" class=\"active\"") : (""));
        echo ">
    <a href=\"";
        // line 4
        if (isset($context["laplace_common"])) { $_laplace_common_ = $context["laplace_common"]; } else { $_laplace_common_ = null; }
        echo $this->env->getExtension('routing')->getPath(($_laplace_common_ . "dashboard"));
        echo "\">Tableau de bord</a>
</li>
<li";
        // line 6
        if (isset($context["page"])) { $_page_ = $context["page"]; } else { $_page_ = null; }
        echo ((($this->getAttribute($_page_, 1, array(), "array") == "tutelles")) ? (" class=\"active\"") : (""));
        echo ">
    <a href=\"";
        // line 7
        if (isset($context["laplace_user"])) { $_laplace_user_ = $context["laplace_user"]; } else { $_laplace_user_ = null; }
        echo $this->env->getExtension('routing')->getPath(($_laplace_user_ . "tutelles"));
        echo "\">Tutelles</a>
</li>
<li";
        // line 9
        if (isset($context["page"])) { $_page_ = $context["page"]; } else { $_page_ = null; }
        echo ((($this->getAttribute($_page_, 1, array(), "array") == "sites")) ? (" class=\"active\"") : (""));
        echo ">
    <a href=\"";
        // line 10
        if (isset($context["laplace_user"])) { $_laplace_user_ = $context["laplace_user"]; } else { $_laplace_user_ = null; }
        echo $this->env->getExtension('routing')->getPath(($_laplace_user_ . "sites"));
        echo "\">Sites</a>
</li>
<li";
        // line 12
        if (isset($context["page"])) { $_page_ = $context["page"]; } else { $_page_ = null; }
        echo ((($this->getAttribute($_page_, 1, array(), "array") == "statuts_agents")) ? (" class=\"active\"") : (""));
        echo ">
    <a href=\"";
        // line 13
        if (isset($context["laplace_user"])) { $_laplace_user_ = $context["laplace_user"]; } else { $_laplace_user_ = null; }
        echo $this->env->getExtension('routing')->getPath(($_laplace_user_ . "statuts_agents"));
        echo "\">Statuts Agents</a>
</li>
<li";
        // line 15
        if (isset($context["page"])) { $_page_ = $context["page"]; } else { $_page_ = null; }
        echo ((($this->getAttribute($_page_, 1, array(), "array") == "statuts_doctorants")) ? (" class=\"active\"") : (""));
        echo ">
    <a href=\"";
        // line 16
        if (isset($context["laplace_user"])) { $_laplace_user_ = $context["laplace_user"]; } else { $_laplace_user_ = null; }
        echo $this->env->getExtension('routing')->getPath(($_laplace_user_ . "statuts_doctorants"));
        echo "\">Statuts Doctorants</a>
</li>
<li";
        // line 18
        if (isset($context["page"])) { $_page_ = $context["page"]; } else { $_page_ = null; }
        echo ((($this->getAttribute($_page_, 1, array(), "array") == "domains")) ? (" class=\"active\"") : (""));
        echo ">
    <a href=\"";
        // line 19
        if (isset($context["laplace_training"])) { $_laplace_training_ = $context["laplace_training"]; } else { $_laplace_training_ = null; }
        echo $this->env->getExtension('routing')->getPath(($_laplace_training_ . "domains"));
        echo "\">Domaines</a>
</li>
<li";
        // line 21
        if (isset($context["page"])) { $_page_ = $context["page"]; } else { $_page_ = null; }
        echo ((($this->getAttribute($_page_, 1, array(), "array") == "categories")) ? (" class=\"active\"") : (""));
        echo ">
    <a href=\"";
        // line 22
        if (isset($context["laplace_training"])) { $_laplace_training_ = $context["laplace_training"]; } else { $_laplace_training_ = null; }
        echo $this->env->getExtension('routing')->getPath(($_laplace_training_ . "categories"));
        echo "\">Catégories</a>
</li>
<li";
        // line 24
        if (isset($context["page"])) { $_page_ = $context["page"]; } else { $_page_ = null; }
        echo ((($this->getAttribute($_page_, 1, array(), "array") == "types")) ? (" class=\"active\"") : (""));
        echo ">
    <a href=\"";
        // line 25
        if (isset($context["laplace_training"])) { $_laplace_training_ = $context["laplace_training"]; } else { $_laplace_training_ = null; }
        echo $this->env->getExtension('routing')->getPath(($_laplace_training_ . "types"));
        echo "\">Types</a>
</li>

<li class=\"nav-header\">Résumés et Bilans</li>
<li";
        // line 29
        if (isset($context["page"])) { $_page_ = $context["page"]; } else { $_page_ = null; }
        echo ((($this->getAttribute($_page_, 1, array(), "array") == "needs-summary")) ? (" class=\"active\"") : (""));
        echo ">
    <a href=\"";
        // line 30
        if (isset($context["laplace_training"])) { $_laplace_training_ = $context["laplace_training"]; } else { $_laplace_training_ = null; }
        echo $this->env->getExtension('routing')->getPath(($_laplace_training_ . "needs_summary"));
        echo "\">Besoins non pourvus</a>
</li>
<li";
        // line 32
        if (isset($context["page"])) { $_page_ = $context["page"]; } else { $_page_ = null; }
        echo ((($this->getAttribute($_page_, 1, array(), "array") == "requests-summary")) ? (" class=\"active\"") : (""));
        echo ">
    <a href=\"";
        // line 33
        if (isset($context["laplace_training"])) { $_laplace_training_ = $context["laplace_training"]; } else { $_laplace_training_ = null; }
        echo $this->env->getExtension('routing')->getPath(($_laplace_training_ . "requests_summary"));
        echo "\">Bilan de réalisation</a>
</li>
";
    }

    public function getTemplateName()
    {
        return "LaplaceCommonBundle:Sidebar:adm-system.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  74 => 17,  1013 => 350,  1009 => 348,  999 => 346,  994 => 345,  990 => 343,  986 => 342,  983 => 341,  978 => 338,  971 => 336,  967 => 335,  938 => 334,  934 => 333,  931 => 331,  928 => 330,  925 => 329,  922 => 328,  918 => 327,  914 => 326,  911 => 325,  908 => 324,  905 => 323,  895 => 314,  885 => 312,  878 => 309,  874 => 307,  871 => 306,  864 => 301,  861 => 300,  858 => 299,  849 => 293,  845 => 291,  842 => 290,  831 => 282,  824 => 280,  819 => 279,  800 => 278,  792 => 275,  789 => 274,  785 => 273,  782 => 272,  776 => 268,  773 => 267,  768 => 264,  759 => 261,  734 => 257,  731 => 256,  727 => 255,  721 => 252,  717 => 251,  714 => 250,  711 => 249,  708 => 248,  703 => 247,  699 => 246,  696 => 245,  693 => 244,  680 => 237,  675 => 236,  668 => 233,  664 => 231,  661 => 230,  654 => 225,  647 => 223,  622 => 217,  617 => 216,  613 => 215,  607 => 213,  599 => 211,  596 => 210,  592 => 209,  590 => 208,  587 => 207,  582 => 204,  576 => 200,  574 => 199,  573 => 198,  572 => 197,  569 => 196,  565 => 195,  562 => 194,  552 => 191,  545 => 189,  542 => 188,  524 => 109,  521 => 108,  518 => 107,  515 => 106,  500 => 101,  493 => 99,  489 => 98,  486 => 97,  480 => 95,  463 => 87,  436 => 81,  404 => 77,  401 => 76,  392 => 69,  388 => 68,  385 => 67,  369 => 63,  361 => 61,  328 => 56,  325 => 55,  319 => 53,  259 => 38,  242 => 33,  234 => 28,  299 => 97,  217 => 64,  167 => 9,  910 => 400,  904 => 397,  896 => 393,  890 => 313,  884 => 389,  875 => 384,  872 => 383,  870 => 382,  867 => 381,  856 => 372,  853 => 371,  848 => 368,  843 => 364,  840 => 362,  836 => 360,  828 => 354,  822 => 350,  804 => 347,  795 => 277,  777 => 345,  771 => 341,  755 => 259,  752 => 258,  748 => 336,  742 => 335,  724 => 253,  710 => 322,  707 => 321,  704 => 320,  701 => 318,  697 => 316,  690 => 243,  683 => 310,  653 => 284,  650 => 224,  648 => 282,  645 => 281,  640 => 218,  633 => 274,  625 => 270,  611 => 260,  608 => 259,  606 => 258,  603 => 212,  598 => 253,  591 => 250,  583 => 246,  577 => 244,  571 => 242,  556 => 192,  553 => 230,  551 => 229,  548 => 190,  536 => 186,  528 => 110,  514 => 207,  511 => 206,  509 => 205,  506 => 204,  503 => 202,  492 => 195,  487 => 192,  481 => 188,  475 => 184,  461 => 176,  454 => 171,  451 => 170,  446 => 85,  443 => 166,  432 => 162,  428 => 160,  425 => 79,  412 => 150,  409 => 149,  406 => 78,  403 => 147,  398 => 144,  393 => 140,  389 => 137,  371 => 133,  357 => 128,  338 => 122,  333 => 121,  320 => 114,  312 => 111,  284 => 97,  275 => 94,  236 => 81,  233 => 80,  230 => 79,  154 => 45,  378 => 65,  370 => 143,  356 => 137,  346 => 131,  316 => 120,  310 => 119,  290 => 99,  274 => 90,  254 => 87,  248 => 35,  240 => 91,  203 => 70,  132 => 299,  213 => 71,  206 => 71,  119 => 266,  306 => 98,  301 => 100,  278 => 95,  266 => 40,  262 => 39,  200 => 59,  141 => 50,  539 => 187,  533 => 240,  525 => 236,  519 => 234,  513 => 232,  504 => 227,  501 => 226,  499 => 200,  496 => 100,  485 => 215,  482 => 214,  476 => 210,  472 => 208,  467 => 204,  464 => 202,  457 => 199,  442 => 192,  439 => 82,  422 => 158,  414 => 178,  402 => 174,  387 => 163,  384 => 162,  382 => 161,  379 => 160,  374 => 64,  367 => 131,  342 => 59,  339 => 58,  337 => 134,  334 => 109,  329 => 129,  322 => 54,  300 => 112,  297 => 111,  292 => 109,  289 => 47,  283 => 45,  280 => 102,  276 => 43,  269 => 92,  261 => 81,  250 => 89,  241 => 73,  184 => 65,  176 => 53,  73 => 16,  99 => 23,  79 => 14,  257 => 100,  249 => 74,  253 => 36,  235 => 69,  198 => 67,  177 => 62,  174 => 54,  169 => 40,  68 => 15,  61 => 13,  341 => 124,  336 => 101,  331 => 57,  326 => 103,  324 => 101,  317 => 97,  314 => 122,  311 => 99,  305 => 104,  303 => 95,  298 => 104,  291 => 85,  288 => 84,  281 => 96,  273 => 42,  270 => 41,  265 => 102,  263 => 90,  260 => 89,  258 => 80,  252 => 77,  244 => 76,  223 => 67,  218 => 75,  159 => 7,  96 => 23,  86 => 20,  246 => 109,  210 => 69,  245 => 34,  237 => 81,  225 => 77,  209 => 72,  194 => 62,  150 => 49,  95 => 27,  128 => 36,  214 => 52,  191 => 65,  185 => 13,  42 => 10,  293 => 96,  286 => 46,  277 => 107,  267 => 119,  255 => 78,  238 => 81,  229 => 79,  220 => 70,  212 => 63,  188 => 14,  161 => 57,  144 => 322,  135 => 41,  126 => 29,  172 => 51,  165 => 52,  151 => 65,  116 => 29,  113 => 29,  153 => 39,  145 => 43,  139 => 43,  127 => 290,  112 => 230,  87 => 20,  94 => 22,  91 => 26,  23 => 3,  170 => 48,  125 => 35,  103 => 26,  82 => 19,  549 => 162,  543 => 224,  538 => 158,  530 => 155,  526 => 153,  522 => 151,  512 => 149,  505 => 148,  502 => 147,  497 => 146,  491 => 144,  488 => 143,  483 => 96,  473 => 134,  469 => 180,  466 => 179,  460 => 130,  455 => 129,  450 => 196,  444 => 122,  441 => 121,  437 => 190,  434 => 189,  429 => 80,  423 => 112,  420 => 111,  416 => 110,  413 => 109,  408 => 176,  394 => 105,  390 => 103,  375 => 134,  365 => 62,  362 => 139,  359 => 149,  355 => 95,  348 => 127,  344 => 125,  330 => 120,  327 => 119,  321 => 86,  307 => 48,  302 => 106,  295 => 103,  287 => 80,  279 => 44,  271 => 77,  264 => 82,  256 => 37,  251 => 86,  247 => 77,  239 => 32,  231 => 27,  219 => 75,  201 => 60,  147 => 323,  143 => 37,  134 => 304,  131 => 39,  122 => 267,  102 => 222,  92 => 185,  84 => 104,  72 => 16,  48 => 10,  35 => 6,  29 => 5,  76 => 18,  69 => 17,  54 => 14,  51 => 11,  31 => 11,  205 => 20,  199 => 61,  190 => 58,  182 => 60,  179 => 58,  175 => 58,  168 => 53,  164 => 50,  156 => 6,  148 => 47,  138 => 306,  123 => 30,  117 => 243,  108 => 23,  83 => 19,  71 => 17,  64 => 15,  110 => 28,  89 => 21,  65 => 15,  63 => 17,  58 => 15,  34 => 6,  227 => 58,  224 => 77,  221 => 25,  207 => 82,  197 => 74,  195 => 62,  192 => 15,  189 => 58,  186 => 53,  181 => 53,  178 => 55,  173 => 53,  162 => 47,  158 => 48,  155 => 36,  152 => 341,  142 => 44,  136 => 43,  133 => 40,  130 => 33,  120 => 30,  105 => 25,  100 => 24,  75 => 19,  53 => 13,  39 => 7,  98 => 19,  80 => 18,  78 => 18,  46 => 10,  44 => 10,  36 => 8,  32 => 8,  60 => 32,  57 => 13,  40 => 7,  114 => 242,  109 => 31,  106 => 29,  101 => 28,  88 => 19,  85 => 19,  77 => 7,  67 => 15,  47 => 9,  28 => 4,  25 => 7,  55 => 19,  43 => 11,  38 => 6,  26 => 8,  24 => 4,  50 => 10,  27 => 3,  22 => 2,  19 => 1,  232 => 68,  226 => 74,  222 => 80,  215 => 24,  211 => 22,  208 => 21,  202 => 19,  196 => 59,  193 => 65,  187 => 58,  183 => 56,  180 => 55,  171 => 54,  166 => 50,  163 => 8,  160 => 49,  157 => 43,  149 => 340,  146 => 51,  140 => 319,  137 => 42,  129 => 33,  124 => 32,  121 => 33,  118 => 30,  115 => 31,  111 => 29,  107 => 227,  104 => 26,  97 => 25,  93 => 24,  90 => 34,  81 => 19,  70 => 16,  66 => 13,  62 => 14,  59 => 13,  56 => 12,  52 => 12,  49 => 11,  45 => 9,  41 => 3,  37 => 9,  33 => 5,  30 => 10,);
    }
}
