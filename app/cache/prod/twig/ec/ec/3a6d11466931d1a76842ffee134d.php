<?php

/* LaplaceCommonBundle::admin-page.html.twig */
class __TwigTemplate_ecec3a6d11466931d1a76842ffee134d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::layout.html.twig");

        $this->blocks = array(
            'PageTitle' => array($this, 'block_PageTitle'),
            'NavbarLeft' => array($this, 'block_NavbarLeft'),
            'NavbarRight' => array($this, 'block_NavbarRight'),
            'Sidebar' => array($this, 'block_Sidebar'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 12
        $context["inverse_navbar"] = true;
        // line 16
        $context["laplace_common"] = "laplace_common_adm_";
        // line 17
        $context["laplace_user"] = "laplace_user_adm_";
        // line 18
        $context["laplace_training"] = "laplace_training_adm_";
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 20
    public function block_PageTitle($context, array $blocks = array())
    {
        $this->displayParentBlock("PageTitle", $context, $blocks);
        echo " - Espace Administrateur";
    }

    // line 22
    public function block_NavbarLeft($context, array $blocks = array())
    {
        // line 23
        echo "    <li";
        if (isset($context["page"])) { $_page_ = $context["page"]; } else { $_page_ = null; }
        echo ((($this->getAttribute($_page_, 0, array(), "array") == "profile")) ? (" class=\"active\"") : (""));
        echo ">
        <a href=\"";
        // line 24
        if (isset($context["laplace_user"])) { $_laplace_user_ = $context["laplace_user"]; } else { $_laplace_user_ = null; }
        echo $this->env->getExtension('routing')->getPath(($_laplace_user_ . "all_profiles"));
        echo "\">
            <i class=\"icon-user icon-white\"></i>
            Profils
        </a>
    </li>
    <li";
        // line 29
        if (isset($context["page"])) { $_page_ = $context["page"]; } else { $_page_ = null; }
        echo ((($this->getAttribute($_page_, 0, array(), "array") == "need")) ? (" class=\"active\"") : (""));
        echo ">
        <a href=\"";
        // line 30
        if (isset($context["laplace_training"])) { $_laplace_training_ = $context["laplace_training"]; } else { $_laplace_training_ = null; }
        echo $this->env->getExtension('routing')->getPath(($_laplace_training_ . "all_needs"));
        echo "\">
            <i class=\"icon-star-empty icon-white\"></i>
            Besoins
        </a>
    </li>
    <li";
        // line 35
        if (isset($context["page"])) { $_page_ = $context["page"]; } else { $_page_ = null; }
        echo ((($this->getAttribute($_page_, 0, array(), "array") == "request")) ? (" class=\"active\"") : (""));
        echo ">
        <a href=\"";
        // line 36
        if (isset($context["laplace_training"])) { $_laplace_training_ = $context["laplace_training"]; } else { $_laplace_training_ = null; }
        echo $this->env->getExtension('routing')->getPath(($_laplace_training_ . "all_requests"));
        echo "\">
            <i class=\"icon-star icon-white\"></i>
            Demandes
        </a>
    </li>
    <li";
        // line 41
        if (isset($context["page"])) { $_page_ = $context["page"]; } else { $_page_ = null; }
        echo ((($this->getAttribute($_page_, 0, array(), "array") == "thread")) ? (" class=\"active\"") : (""));
        echo ">
        <a href=\"";
        // line 42
        if (isset($context["laplace_training"])) { $_laplace_training_ = $context["laplace_training"]; } else { $_laplace_training_ = null; }
        echo $this->env->getExtension('routing')->getPath(($_laplace_training_ . "all_threads"));
        echo "\">
            <i class=\"icon-comment icon-white\"></i>
            Discussions
        </a>
    </li>
    <li";
        // line 47
        if (isset($context["page"])) { $_page_ = $context["page"]; } else { $_page_ = null; }
        echo ((($this->getAttribute($_page_, 0, array(), "array") == "system")) ? (" class=\"active\"") : (""));
        echo ">
        <a href=\"";
        // line 48
        if (isset($context["laplace_common"])) { $_laplace_common_ = $context["laplace_common"]; } else { $_laplace_common_ = null; }
        echo $this->env->getExtension('routing')->getPath(($_laplace_common_ . "dashboard"));
        echo "\">
            <i class=\"icon-wrench icon-white\"></i>
            Système
        </a>
    </li>
";
    }

    // line 55
    public function block_NavbarRight($context, array $blocks = array())
    {
        // line 56
        echo "    <li>
        <a href=\"";
        // line 57
        echo $this->env->getExtension('routing')->getPath("laplace_common_user_homepage");
        echo "\">
            <i class=\"icon-arrow-right icon-white\"></i>
            Espace personnel
        </a>
    </li>

    ";
        // line 63
        $this->displayParentBlock("NavbarRight", $context, $blocks);
        echo "
";
    }

    // line 66
    public function block_Sidebar($context, array $blocks = array())
    {
        // line 67
        echo "
    ";
        // line 68
        if (isset($context["page"])) { $_page_ = $context["page"]; } else { $_page_ = null; }
        if (($this->getAttribute($_page_, 0, array(), "array") == "system")) {
            // line 69
            echo "
        ";
            // line 71
            echo "        ";
            $this->env->loadTemplate("LaplaceCommonBundle:Sidebar:adm-system.html.twig")->display($context);
            // line 72
            echo "
    ";
        } elseif (($this->getAttribute($_page_, 0, array(), "array") == "profile")) {
            // line 74
            echo "
        ";
            // line 76
            echo "        ";
            $this->env->loadTemplate("LaplaceCommonBundle:Sidebar:adm-profile.html.twig")->display($context);
            // line 77
            echo "
    ";
        } elseif (($this->getAttribute($_page_, 0, array(), "array") == "need")) {
            // line 79
            echo "
        ";
            // line 81
            echo "        ";
            $this->env->loadTemplate("LaplaceCommonBundle:Sidebar:adm-need.html.twig")->display($context);
            // line 82
            echo "
    ";
        } elseif (($this->getAttribute($_page_, 0, array(), "array") == "request")) {
            // line 84
            echo "
        ";
            // line 86
            echo "        ";
            $this->env->loadTemplate("LaplaceCommonBundle:Sidebar:adm-request.html.twig")->display($context);
            // line 87
            echo "
    ";
        } elseif (($this->getAttribute($_page_, 0, array(), "array") == "thread")) {
            // line 89
            echo "
        ";
            // line 91
            echo "        ";
            $this->env->loadTemplate("LaplaceCommonBundle:Sidebar:adm-thread.html.twig")->display($context);
            // line 92
            echo "
    ";
        }
        // line 94
        echo "
";
    }

    public function getTemplateName()
    {
        return "LaplaceCommonBundle::admin-page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  74 => 17,  1013 => 350,  1009 => 348,  999 => 346,  994 => 345,  990 => 343,  986 => 342,  983 => 341,  978 => 338,  971 => 336,  967 => 335,  938 => 334,  934 => 333,  931 => 331,  928 => 330,  925 => 329,  922 => 328,  918 => 327,  914 => 326,  911 => 325,  908 => 324,  905 => 323,  895 => 314,  885 => 312,  878 => 309,  874 => 307,  871 => 306,  864 => 301,  861 => 300,  858 => 299,  849 => 293,  845 => 291,  842 => 290,  831 => 282,  824 => 280,  819 => 279,  800 => 278,  792 => 275,  789 => 274,  785 => 273,  782 => 272,  776 => 268,  773 => 267,  768 => 264,  759 => 261,  734 => 257,  731 => 256,  727 => 255,  721 => 252,  717 => 251,  714 => 250,  711 => 249,  708 => 248,  703 => 247,  699 => 246,  696 => 245,  693 => 244,  680 => 237,  675 => 236,  668 => 233,  664 => 231,  661 => 230,  654 => 225,  647 => 223,  622 => 217,  617 => 216,  613 => 215,  607 => 213,  599 => 211,  596 => 210,  592 => 209,  590 => 208,  587 => 207,  582 => 204,  576 => 200,  574 => 199,  573 => 198,  572 => 197,  569 => 196,  565 => 195,  562 => 194,  552 => 191,  545 => 189,  542 => 188,  524 => 109,  521 => 108,  518 => 107,  515 => 106,  500 => 101,  493 => 99,  489 => 98,  486 => 97,  480 => 95,  463 => 87,  436 => 81,  404 => 77,  401 => 76,  392 => 69,  388 => 68,  385 => 67,  369 => 63,  361 => 61,  328 => 56,  325 => 55,  319 => 53,  259 => 38,  242 => 33,  234 => 28,  299 => 97,  217 => 64,  167 => 9,  910 => 400,  904 => 397,  896 => 393,  890 => 313,  884 => 389,  875 => 384,  872 => 383,  870 => 382,  867 => 381,  856 => 372,  853 => 371,  848 => 368,  843 => 364,  840 => 362,  836 => 360,  828 => 354,  822 => 350,  804 => 347,  795 => 277,  777 => 345,  771 => 341,  755 => 259,  752 => 258,  748 => 336,  742 => 335,  724 => 253,  710 => 322,  707 => 321,  704 => 320,  701 => 318,  697 => 316,  690 => 243,  683 => 310,  653 => 284,  650 => 224,  648 => 282,  645 => 281,  640 => 218,  633 => 274,  625 => 270,  611 => 260,  608 => 259,  606 => 258,  603 => 212,  598 => 253,  591 => 250,  583 => 246,  577 => 244,  571 => 242,  556 => 192,  553 => 230,  551 => 229,  548 => 190,  536 => 186,  528 => 110,  514 => 207,  511 => 206,  509 => 205,  506 => 204,  503 => 202,  492 => 195,  487 => 192,  481 => 188,  475 => 184,  461 => 176,  454 => 171,  451 => 170,  446 => 85,  443 => 166,  432 => 162,  428 => 160,  425 => 79,  412 => 150,  409 => 149,  406 => 78,  403 => 147,  398 => 144,  393 => 140,  389 => 137,  371 => 133,  357 => 128,  338 => 122,  333 => 121,  320 => 114,  312 => 111,  284 => 97,  275 => 94,  236 => 81,  233 => 80,  230 => 79,  154 => 45,  378 => 65,  370 => 143,  356 => 137,  346 => 131,  316 => 120,  310 => 119,  290 => 99,  274 => 90,  254 => 87,  248 => 35,  240 => 91,  203 => 70,  132 => 299,  213 => 71,  206 => 71,  119 => 50,  306 => 98,  301 => 100,  278 => 95,  266 => 40,  262 => 39,  200 => 59,  141 => 50,  539 => 187,  533 => 240,  525 => 236,  519 => 234,  513 => 232,  504 => 227,  501 => 226,  499 => 200,  496 => 100,  485 => 215,  482 => 214,  476 => 210,  472 => 208,  467 => 204,  464 => 202,  457 => 199,  442 => 192,  439 => 82,  422 => 158,  414 => 178,  402 => 174,  387 => 163,  384 => 162,  382 => 161,  379 => 160,  374 => 64,  367 => 131,  342 => 59,  339 => 58,  337 => 134,  334 => 109,  329 => 129,  322 => 54,  300 => 112,  297 => 111,  292 => 109,  289 => 47,  283 => 45,  280 => 102,  276 => 43,  269 => 92,  261 => 81,  250 => 89,  241 => 73,  184 => 65,  176 => 53,  73 => 16,  99 => 23,  79 => 14,  257 => 100,  249 => 74,  253 => 36,  235 => 69,  198 => 92,  177 => 62,  174 => 54,  169 => 40,  68 => 15,  61 => 13,  341 => 124,  336 => 101,  331 => 57,  326 => 103,  324 => 101,  317 => 97,  314 => 122,  311 => 99,  305 => 104,  303 => 95,  298 => 104,  291 => 85,  288 => 84,  281 => 96,  273 => 42,  270 => 41,  265 => 102,  263 => 90,  260 => 89,  258 => 80,  252 => 77,  244 => 76,  223 => 67,  218 => 75,  159 => 7,  96 => 23,  86 => 20,  246 => 109,  210 => 69,  245 => 34,  237 => 81,  225 => 77,  209 => 72,  194 => 62,  150 => 49,  95 => 27,  128 => 57,  214 => 52,  191 => 65,  185 => 86,  42 => 10,  293 => 96,  286 => 46,  277 => 107,  267 => 119,  255 => 78,  238 => 81,  229 => 79,  220 => 70,  212 => 63,  188 => 87,  161 => 57,  144 => 322,  135 => 41,  126 => 50,  172 => 79,  165 => 76,  151 => 65,  116 => 49,  113 => 48,  153 => 39,  145 => 55,  139 => 43,  127 => 290,  112 => 230,  87 => 20,  94 => 22,  91 => 41,  23 => 3,  170 => 48,  125 => 56,  103 => 26,  82 => 19,  549 => 162,  543 => 224,  538 => 158,  530 => 155,  526 => 153,  522 => 151,  512 => 149,  505 => 148,  502 => 147,  497 => 146,  491 => 144,  488 => 143,  483 => 96,  473 => 134,  469 => 180,  466 => 179,  460 => 130,  455 => 129,  450 => 196,  444 => 122,  441 => 121,  437 => 190,  434 => 189,  429 => 80,  423 => 112,  420 => 111,  416 => 110,  413 => 109,  408 => 176,  394 => 105,  390 => 103,  375 => 134,  365 => 62,  362 => 139,  359 => 149,  355 => 95,  348 => 127,  344 => 125,  330 => 120,  327 => 119,  321 => 86,  307 => 48,  302 => 106,  295 => 103,  287 => 80,  279 => 44,  271 => 77,  264 => 82,  256 => 37,  251 => 86,  247 => 77,  239 => 32,  231 => 27,  219 => 75,  201 => 60,  147 => 323,  143 => 66,  134 => 54,  131 => 39,  122 => 55,  102 => 222,  92 => 41,  84 => 104,  72 => 16,  48 => 10,  35 => 6,  29 => 5,  76 => 18,  69 => 30,  54 => 14,  51 => 11,  31 => 11,  205 => 20,  199 => 61,  190 => 58,  182 => 84,  179 => 58,  175 => 81,  168 => 77,  164 => 50,  156 => 6,  148 => 56,  138 => 306,  123 => 30,  117 => 243,  108 => 23,  83 => 36,  71 => 17,  64 => 29,  110 => 28,  89 => 21,  65 => 15,  63 => 17,  58 => 15,  34 => 18,  227 => 58,  224 => 77,  221 => 25,  207 => 82,  197 => 74,  195 => 91,  192 => 89,  189 => 58,  186 => 53,  181 => 53,  178 => 82,  173 => 53,  162 => 74,  158 => 72,  155 => 71,  152 => 69,  142 => 54,  136 => 59,  133 => 40,  130 => 33,  120 => 30,  105 => 48,  100 => 45,  75 => 19,  53 => 13,  39 => 20,  98 => 19,  80 => 35,  78 => 35,  46 => 22,  44 => 12,  36 => 8,  32 => 17,  60 => 18,  57 => 13,  40 => 7,  114 => 242,  109 => 31,  106 => 47,  101 => 28,  88 => 19,  85 => 19,  77 => 34,  67 => 19,  47 => 9,  28 => 12,  25 => 7,  55 => 24,  43 => 11,  38 => 6,  26 => 8,  24 => 4,  50 => 10,  27 => 3,  22 => 2,  19 => 1,  232 => 68,  226 => 74,  222 => 80,  215 => 24,  211 => 22,  208 => 21,  202 => 94,  196 => 59,  193 => 65,  187 => 58,  183 => 56,  180 => 55,  171 => 54,  166 => 50,  163 => 8,  160 => 49,  157 => 43,  149 => 68,  146 => 67,  140 => 319,  137 => 63,  129 => 51,  124 => 32,  121 => 62,  118 => 30,  115 => 31,  111 => 48,  107 => 64,  104 => 26,  97 => 42,  93 => 24,  90 => 34,  81 => 19,  70 => 16,  66 => 13,  62 => 14,  59 => 13,  56 => 12,  52 => 13,  49 => 23,  45 => 9,  41 => 11,  37 => 9,  33 => 5,  30 => 16,);
    }
}
