<?php

/* LaplaceTrainingBundle:Need:delete.html.twig */
class __TwigTemplate_5c897f445247ba2dcd0424f7c00b7d7a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
";
        // line 2
        $this->env->loadTemplate("LaplaceTrainingBundle:Need:delete.html.twig", "2021713958")->display(array_merge($context, array("page" => array(0 => "need", 1 => "delete"))));
    }

    public function getTemplateName()
    {
        return "LaplaceTrainingBundle:Need:delete.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 29,  99 => 22,  79 => 7,  257 => 78,  249 => 74,  253 => 74,  235 => 64,  198 => 46,  177 => 42,  174 => 41,  169 => 40,  68 => 29,  61 => 27,  341 => 118,  336 => 101,  331 => 97,  326 => 102,  324 => 101,  317 => 97,  314 => 96,  311 => 95,  305 => 104,  303 => 95,  298 => 92,  291 => 85,  288 => 84,  281 => 86,  273 => 80,  270 => 79,  265 => 107,  263 => 91,  260 => 90,  258 => 79,  252 => 77,  244 => 77,  223 => 67,  218 => 66,  159 => 35,  96 => 40,  86 => 15,  246 => 109,  210 => 69,  245 => 70,  237 => 72,  225 => 76,  209 => 68,  194 => 61,  150 => 35,  95 => 14,  128 => 37,  214 => 52,  191 => 87,  185 => 85,  42 => 15,  293 => 127,  286 => 125,  277 => 123,  267 => 119,  255 => 78,  238 => 81,  229 => 97,  220 => 75,  212 => 88,  188 => 59,  161 => 57,  144 => 44,  135 => 32,  126 => 32,  172 => 51,  165 => 38,  151 => 65,  116 => 30,  113 => 28,  153 => 45,  145 => 61,  139 => 60,  127 => 25,  112 => 27,  87 => 11,  94 => 118,  91 => 17,  23 => 3,  170 => 62,  125 => 26,  103 => 19,  82 => 34,  549 => 162,  543 => 161,  538 => 158,  530 => 155,  526 => 153,  522 => 151,  512 => 149,  505 => 148,  502 => 147,  497 => 146,  491 => 144,  488 => 143,  483 => 142,  473 => 134,  469 => 132,  466 => 131,  460 => 130,  455 => 129,  450 => 126,  444 => 122,  441 => 121,  437 => 120,  434 => 119,  429 => 116,  423 => 112,  420 => 111,  416 => 110,  413 => 109,  408 => 106,  394 => 105,  390 => 103,  375 => 101,  365 => 99,  362 => 98,  359 => 97,  355 => 95,  348 => 91,  344 => 90,  330 => 89,  327 => 88,  321 => 86,  307 => 85,  302 => 84,  295 => 91,  287 => 80,  279 => 84,  271 => 77,  264 => 118,  256 => 73,  251 => 71,  247 => 107,  239 => 68,  231 => 78,  219 => 74,  201 => 60,  147 => 34,  143 => 49,  134 => 56,  131 => 29,  122 => 24,  102 => 20,  92 => 38,  84 => 114,  72 => 52,  48 => 18,  35 => 5,  29 => 3,  76 => 30,  69 => 5,  54 => 20,  51 => 19,  31 => 11,  205 => 48,  199 => 91,  190 => 66,  182 => 43,  179 => 67,  175 => 58,  168 => 38,  164 => 47,  156 => 34,  148 => 47,  138 => 34,  123 => 30,  117 => 49,  108 => 23,  83 => 14,  71 => 10,  64 => 25,  110 => 28,  89 => 16,  65 => 14,  63 => 17,  58 => 15,  34 => 12,  227 => 58,  224 => 91,  221 => 54,  207 => 82,  197 => 74,  195 => 65,  192 => 72,  189 => 54,  186 => 53,  181 => 44,  178 => 83,  173 => 40,  162 => 36,  158 => 69,  155 => 36,  152 => 30,  142 => 47,  136 => 30,  133 => 29,  130 => 28,  120 => 24,  105 => 24,  100 => 22,  75 => 53,  53 => 13,  39 => 7,  98 => 52,  80 => 8,  78 => 42,  46 => 16,  44 => 10,  36 => 6,  32 => 8,  60 => 21,  57 => 9,  40 => 16,  114 => 48,  109 => 48,  106 => 19,  101 => 21,  88 => 12,  85 => 10,  77 => 11,  67 => 27,  47 => 24,  28 => 18,  25 => 7,  55 => 14,  43 => 17,  38 => 7,  26 => 8,  24 => 3,  50 => 7,  27 => 2,  22 => 2,  19 => 1,  232 => 79,  226 => 77,  222 => 76,  215 => 65,  211 => 84,  208 => 95,  202 => 65,  196 => 59,  193 => 65,  187 => 44,  183 => 60,  180 => 59,  171 => 54,  166 => 51,  163 => 50,  160 => 49,  157 => 43,  149 => 29,  146 => 40,  140 => 32,  137 => 37,  129 => 36,  124 => 34,  121 => 24,  118 => 29,  115 => 59,  111 => 57,  107 => 45,  104 => 44,  97 => 17,  93 => 16,  90 => 12,  81 => 12,  70 => 9,  66 => 13,  62 => 24,  59 => 23,  56 => 21,  52 => 10,  49 => 17,  45 => 6,  41 => 7,  37 => 20,  33 => 4,  30 => 3,);
    }
}


/* LaplaceTrainingBundle:Need:delete.html.twig */
class __TwigTemplate_5c897f445247ba2dcd0424f7c00b7d7a_2021713958 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("LaplaceCommonBundle::admin-page.html.twig");

        $this->blocks = array(
            'PageTitle' => array($this, 'block_PageTitle'),
            'ContentTitle' => array($this, 'block_ContentTitle'),
            'ContentBody' => array($this, 'block_ContentBody'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "LaplaceCommonBundle::admin-page.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_PageTitle($context, array $blocks = array())
    {
        $this->displayParentBlock("PageTitle", $context, $blocks);
        echo " - Supprimer un besoin";
    }

    // line 7
    public function block_ContentTitle($context, array $blocks = array())
    {
        echo "Supprimer un besoin";
    }

    // line 9
    public function block_ContentBody($context, array $blocks = array())
    {
        // line 10
        echo "
<p>
    La suppression du besoin <a href=\"";
        // line 12
        if (isset($context["laplace_training"])) { $_laplace_training_ = $context["laplace_training"]; } else { $_laplace_training_ = null; }
        if (isset($context["need"])) { $_need_ = $context["need"]; } else { $_need_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath(($_laplace_training_ . "view_need"), array("id" => $this->getAttribute($_need_, "id"))), "html", null, true);
        echo "\">";
        if (isset($context["need"])) { $_need_ = $context["need"]; } else { $_need_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_need_, "title"), "html", null, true);
        echo "</a>
    entrainera également la suppression, pour tous les utilisateurs, des
    informations relatives à ce besoin dont les inscriptions,
    les évènements, les discussions et les messages.
    <strong>Attention ! La suppression est irréversible.</strong>
</p>

";
        // line 19
        if (isset($context["delete_form"])) { $_delete_form_ = $context["delete_form"]; } else { $_delete_form_ = null; }
        echo         $this->env->getExtension('form')->renderer->renderBlock($_delete_form_, 'form_start');
        echo "

<fieldset>

    <legend>Supprimer un besoin</legend>

    ";
        // line 25
        if (isset($context["delete_form"])) { $_delete_form_ = $context["delete_form"]; } else { $_delete_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_delete_form_, "delete"), 'row');
        echo "

    ";
        // line 27
        if (isset($context["delete_form"])) { $_delete_form_ = $context["delete_form"]; } else { $_delete_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_delete_form_, "do"), 'row', array("attr" => array("class" => "btn-danger")));
        echo "

    ";
        // line 29
        if (isset($context["delete_form"])) { $_delete_form_ = $context["delete_form"]; } else { $_delete_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($_delete_form_, 'rest');
        echo "

</fieldset>

";
        // line 33
        if (isset($context["delete_form"])) { $_delete_form_ = $context["delete_form"]; } else { $_delete_form_ = null; }
        echo         $this->env->getExtension('form')->renderer->renderBlock($_delete_form_, 'form_end');
        echo "

";
    }

    public function getTemplateName()
    {
        return "LaplaceTrainingBundle:Need:delete.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 29,  99 => 22,  79 => 7,  257 => 78,  249 => 74,  253 => 74,  235 => 64,  198 => 46,  177 => 42,  174 => 41,  169 => 40,  68 => 29,  61 => 27,  341 => 118,  336 => 101,  331 => 97,  326 => 102,  324 => 101,  317 => 97,  314 => 96,  311 => 95,  305 => 104,  303 => 95,  298 => 92,  291 => 85,  288 => 84,  281 => 86,  273 => 80,  270 => 79,  265 => 107,  263 => 91,  260 => 90,  258 => 79,  252 => 77,  244 => 77,  223 => 67,  218 => 66,  159 => 35,  96 => 40,  86 => 15,  246 => 109,  210 => 69,  245 => 70,  237 => 72,  225 => 76,  209 => 68,  194 => 61,  150 => 35,  95 => 14,  128 => 37,  214 => 52,  191 => 87,  185 => 85,  42 => 15,  293 => 127,  286 => 125,  277 => 123,  267 => 119,  255 => 78,  238 => 81,  229 => 97,  220 => 75,  212 => 88,  188 => 59,  161 => 57,  144 => 44,  135 => 32,  126 => 29,  172 => 51,  165 => 38,  151 => 65,  116 => 30,  113 => 28,  153 => 45,  145 => 61,  139 => 60,  127 => 25,  112 => 27,  87 => 11,  94 => 118,  91 => 17,  23 => 3,  170 => 62,  125 => 26,  103 => 19,  82 => 9,  549 => 162,  543 => 161,  538 => 158,  530 => 155,  526 => 153,  522 => 151,  512 => 149,  505 => 148,  502 => 147,  497 => 146,  491 => 144,  488 => 143,  483 => 142,  473 => 134,  469 => 132,  466 => 131,  460 => 130,  455 => 129,  450 => 126,  444 => 122,  441 => 121,  437 => 120,  434 => 119,  429 => 116,  423 => 112,  420 => 111,  416 => 110,  413 => 109,  408 => 106,  394 => 105,  390 => 103,  375 => 101,  365 => 99,  362 => 98,  359 => 97,  355 => 95,  348 => 91,  344 => 90,  330 => 89,  327 => 88,  321 => 86,  307 => 85,  302 => 84,  295 => 91,  287 => 80,  279 => 84,  271 => 77,  264 => 118,  256 => 73,  251 => 71,  247 => 107,  239 => 68,  231 => 78,  219 => 74,  201 => 60,  147 => 34,  143 => 49,  134 => 33,  131 => 29,  122 => 24,  102 => 20,  92 => 38,  84 => 114,  72 => 52,  48 => 18,  35 => 5,  29 => 3,  76 => 7,  69 => 5,  54 => 20,  51 => 19,  31 => 11,  205 => 48,  199 => 91,  190 => 66,  182 => 43,  179 => 67,  175 => 58,  168 => 38,  164 => 47,  156 => 34,  148 => 47,  138 => 34,  123 => 30,  117 => 49,  108 => 23,  83 => 14,  71 => 10,  64 => 25,  110 => 28,  89 => 12,  65 => 14,  63 => 17,  58 => 15,  34 => 12,  227 => 58,  224 => 91,  221 => 54,  207 => 82,  197 => 74,  195 => 65,  192 => 72,  189 => 54,  186 => 53,  181 => 44,  178 => 83,  173 => 40,  162 => 36,  158 => 69,  155 => 36,  152 => 30,  142 => 47,  136 => 30,  133 => 29,  130 => 28,  120 => 27,  105 => 24,  100 => 22,  75 => 53,  53 => 13,  39 => 7,  98 => 52,  80 => 8,  78 => 42,  46 => 16,  44 => 10,  36 => 6,  32 => 8,  60 => 21,  57 => 9,  40 => 16,  114 => 25,  109 => 48,  106 => 19,  101 => 21,  88 => 12,  85 => 10,  77 => 11,  67 => 27,  47 => 24,  28 => 18,  25 => 7,  55 => 14,  43 => 17,  38 => 7,  26 => 8,  24 => 3,  50 => 7,  27 => 2,  22 => 2,  19 => 1,  232 => 79,  226 => 77,  222 => 76,  215 => 65,  211 => 84,  208 => 95,  202 => 65,  196 => 59,  193 => 65,  187 => 44,  183 => 60,  180 => 59,  171 => 54,  166 => 51,  163 => 50,  160 => 49,  157 => 43,  149 => 29,  146 => 40,  140 => 32,  137 => 37,  129 => 36,  124 => 34,  121 => 24,  118 => 29,  115 => 59,  111 => 57,  107 => 45,  104 => 19,  97 => 17,  93 => 16,  90 => 12,  81 => 12,  70 => 9,  66 => 13,  62 => 24,  59 => 23,  56 => 21,  52 => 10,  49 => 17,  45 => 6,  41 => 7,  37 => 20,  33 => 4,  30 => 3,);
    }
}
