<?php

/* LaplaceTrainingBundle:Request:view-all.html.twig */
class __TwigTemplate_6e15a2d00cb0a98c9b30543c9d168eac extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        if (isset($context["admin"])) { $_admin_ = $context["admin"]; } else { $_admin_ = null; }
        $context["container"] = (($_admin_) ? ("LaplaceCommonBundle::admin-page.html.twig") : ("LaplaceCommonBundle::user-page.html.twig"));
        // line 6
        echo "

";
        // line 8
        $this->env->loadTemplate("LaplaceTrainingBundle:Request:view-all.html.twig", "313675334")->display(array_merge($context, array("page" => array(0 => "request", 1 => "all"))));
    }

    public function getTemplateName()
    {
        return "LaplaceTrainingBundle:Request:view-all.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  910 => 400,  904 => 397,  896 => 393,  890 => 391,  884 => 389,  875 => 384,  872 => 383,  870 => 382,  867 => 381,  856 => 372,  853 => 371,  848 => 368,  843 => 364,  840 => 362,  836 => 360,  828 => 354,  822 => 350,  804 => 347,  795 => 346,  777 => 345,  771 => 341,  755 => 338,  752 => 337,  748 => 336,  742 => 335,  724 => 334,  710 => 322,  707 => 321,  704 => 320,  701 => 318,  697 => 316,  690 => 313,  683 => 310,  653 => 284,  650 => 283,  648 => 282,  645 => 281,  640 => 277,  633 => 274,  625 => 270,  611 => 260,  608 => 259,  606 => 258,  603 => 257,  598 => 253,  591 => 250,  583 => 246,  577 => 244,  571 => 242,  556 => 231,  553 => 230,  551 => 229,  548 => 228,  536 => 221,  528 => 217,  514 => 207,  511 => 206,  509 => 205,  506 => 204,  503 => 202,  492 => 195,  487 => 192,  481 => 188,  475 => 184,  461 => 176,  454 => 171,  451 => 170,  446 => 167,  443 => 166,  432 => 162,  428 => 160,  425 => 159,  412 => 150,  409 => 149,  406 => 148,  403 => 147,  398 => 144,  393 => 140,  389 => 137,  371 => 133,  357 => 128,  338 => 122,  333 => 121,  320 => 114,  312 => 111,  284 => 97,  275 => 94,  236 => 81,  233 => 80,  230 => 79,  154 => 45,  378 => 135,  370 => 143,  356 => 137,  346 => 131,  316 => 120,  310 => 119,  290 => 99,  274 => 106,  254 => 87,  248 => 85,  240 => 91,  203 => 70,  132 => 37,  213 => 71,  206 => 71,  119 => 26,  306 => 103,  301 => 100,  278 => 95,  266 => 91,  262 => 101,  200 => 59,  141 => 50,  539 => 243,  533 => 240,  525 => 236,  519 => 234,  513 => 232,  504 => 227,  501 => 226,  499 => 200,  496 => 224,  485 => 215,  482 => 214,  476 => 210,  472 => 208,  467 => 204,  464 => 202,  457 => 199,  442 => 192,  439 => 165,  422 => 158,  414 => 178,  402 => 174,  387 => 163,  384 => 162,  382 => 161,  379 => 160,  374 => 156,  367 => 131,  342 => 129,  339 => 135,  337 => 134,  334 => 126,  329 => 129,  322 => 122,  300 => 112,  297 => 111,  292 => 109,  289 => 107,  283 => 103,  280 => 102,  276 => 101,  269 => 92,  261 => 94,  250 => 89,  241 => 75,  184 => 65,  176 => 53,  73 => 29,  99 => 25,  79 => 11,  257 => 100,  249 => 74,  253 => 90,  235 => 87,  198 => 67,  177 => 62,  174 => 41,  169 => 40,  68 => 25,  61 => 22,  341 => 124,  336 => 101,  331 => 97,  326 => 102,  324 => 101,  317 => 97,  314 => 122,  311 => 95,  305 => 104,  303 => 95,  298 => 104,  291 => 85,  288 => 84,  281 => 96,  273 => 89,  270 => 79,  265 => 102,  263 => 90,  260 => 89,  258 => 79,  252 => 77,  244 => 76,  223 => 67,  218 => 75,  159 => 50,  96 => 23,  86 => 15,  246 => 109,  210 => 69,  245 => 84,  237 => 81,  225 => 77,  209 => 72,  194 => 62,  150 => 49,  95 => 18,  128 => 39,  214 => 52,  191 => 65,  185 => 61,  42 => 10,  293 => 94,  286 => 111,  277 => 107,  267 => 119,  255 => 78,  238 => 81,  229 => 79,  220 => 70,  212 => 67,  188 => 60,  161 => 57,  144 => 47,  135 => 41,  126 => 29,  172 => 51,  165 => 52,  151 => 65,  116 => 29,  113 => 28,  153 => 39,  145 => 43,  139 => 43,  127 => 38,  112 => 30,  87 => 16,  94 => 15,  91 => 20,  23 => 3,  170 => 48,  125 => 37,  103 => 22,  82 => 9,  549 => 162,  543 => 224,  538 => 158,  530 => 155,  526 => 153,  522 => 151,  512 => 149,  505 => 148,  502 => 147,  497 => 146,  491 => 144,  488 => 143,  483 => 142,  473 => 134,  469 => 180,  466 => 179,  460 => 130,  455 => 129,  450 => 196,  444 => 122,  441 => 121,  437 => 190,  434 => 189,  429 => 185,  423 => 112,  420 => 111,  416 => 110,  413 => 109,  408 => 176,  394 => 105,  390 => 103,  375 => 134,  365 => 99,  362 => 139,  359 => 149,  355 => 95,  348 => 127,  344 => 125,  330 => 120,  327 => 119,  321 => 86,  307 => 118,  302 => 106,  295 => 103,  287 => 80,  279 => 84,  271 => 77,  264 => 95,  256 => 91,  251 => 86,  247 => 77,  239 => 82,  231 => 79,  219 => 75,  201 => 60,  147 => 48,  143 => 37,  134 => 33,  131 => 39,  122 => 43,  102 => 20,  92 => 17,  84 => 114,  72 => 27,  48 => 18,  35 => 7,  29 => 5,  76 => 7,  69 => 5,  54 => 14,  51 => 19,  31 => 11,  205 => 48,  199 => 71,  190 => 58,  182 => 60,  179 => 58,  175 => 58,  168 => 53,  164 => 48,  156 => 53,  148 => 47,  138 => 34,  123 => 30,  117 => 40,  108 => 23,  83 => 13,  71 => 10,  64 => 23,  110 => 28,  89 => 14,  65 => 14,  63 => 17,  58 => 15,  34 => 12,  227 => 58,  224 => 77,  221 => 76,  207 => 82,  197 => 74,  195 => 62,  192 => 68,  189 => 66,  186 => 53,  181 => 53,  178 => 52,  173 => 53,  162 => 47,  158 => 48,  155 => 36,  152 => 30,  142 => 47,  136 => 43,  133 => 40,  130 => 33,  120 => 27,  105 => 21,  100 => 22,  75 => 53,  53 => 13,  39 => 15,  98 => 19,  80 => 12,  78 => 42,  46 => 16,  44 => 10,  36 => 6,  32 => 8,  60 => 21,  57 => 9,  40 => 10,  114 => 25,  109 => 26,  106 => 29,  101 => 20,  88 => 11,  85 => 10,  77 => 7,  67 => 27,  47 => 17,  28 => 18,  25 => 7,  55 => 21,  43 => 11,  38 => 7,  26 => 8,  24 => 3,  50 => 7,  27 => 2,  22 => 6,  19 => 2,  232 => 85,  226 => 74,  222 => 80,  215 => 74,  211 => 84,  208 => 65,  202 => 66,  196 => 59,  193 => 65,  187 => 58,  183 => 56,  180 => 55,  171 => 54,  166 => 50,  163 => 58,  160 => 49,  157 => 43,  149 => 48,  146 => 51,  140 => 41,  137 => 42,  129 => 37,  124 => 34,  121 => 33,  118 => 32,  115 => 31,  111 => 22,  107 => 27,  104 => 19,  97 => 19,  93 => 19,  90 => 34,  81 => 12,  70 => 5,  66 => 13,  62 => 24,  59 => 17,  56 => 21,  52 => 20,  49 => 17,  45 => 13,  41 => 7,  37 => 9,  33 => 11,  30 => 10,);
    }
}


/* LaplaceTrainingBundle:Request:view-all.html.twig */
class __TwigTemplate_6e15a2d00cb0a98c9b30543c9d168eac_313675334 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'PageTitle' => array($this, 'block_PageTitle'),
            'ContentTitle' => array($this, 'block_ContentTitle'),
            'Sidebar' => array($this, 'block_Sidebar'),
            'ContentBody' => array($this, 'block_ContentBody'),
        );
    }

    protected function doGetParent(array $context)
    {
        return $this->env->resolveTemplate($this->getContext($context, "container"));
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 10
    public function block_PageTitle($context, array $blocks = array())
    {
        $this->displayParentBlock("PageTitle", $context, $blocks);
        echo " - Toutes les demandes";
    }

    // line 14
    public function block_ContentTitle($context, array $blocks = array())
    {
        echo "Toutes les demandes";
    }

    // line 18
    public function block_Sidebar($context, array $blocks = array())
    {
        // line 19
        echo "
";
        // line 20
        $this->displayParentBlock("Sidebar", $context, $blocks);
        echo "

<li class=\"divider\"></li>

<li class=\"nav-header\">Domaines de connaissances</li>
";
        // line 25
        if (isset($context["domains"])) { $_domains_ = $context["domains"]; } else { $_domains_ = null; }
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($_domains_);
        foreach ($context['_seq'] as $context["_key"] => $context["domain"]) {
            // line 26
            echo "<li>
    <a href=\"#dom_";
            // line 27
            if (isset($context["domain"])) { $_domain_ = $context["domain"]; } else { $_domain_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_domain_, "id"), "html", null, true);
            echo "\">";
            if (isset($context["domain"])) { $_domain_ = $context["domain"]; } else { $_domain_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_domain_, "name"), "html", null, true);
            echo "</a>
</li>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['domain'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 30
        echo "
";
    }

    // line 35
    public function block_ContentBody($context, array $blocks = array())
    {
        // line 36
        echo "
<p>
    Cliquez sur la formation de votre choix pour <strong>voir sa description
    complète</strong> ou pour <strong>vous y inscrire</strong>.
    Si vous souhaitez ajouter une formation qui ne figure pas dans cette liste,
    vous pouvez en <a href=\"";
        // line 41
        if (isset($context["laplace_training"])) { $_laplace_training_ = $context["laplace_training"]; } else { $_laplace_training_ = null; }
        echo $this->env->getExtension('routing')->getPath(($_laplace_training_ . "create_request"));
        echo "\">créer une nouvelle</a>.
</p>

";
        // line 44
        if (isset($context["domains"])) { $_domains_ = $context["domains"]; } else { $_domains_ = null; }
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($_domains_);
        foreach ($context['_seq'] as $context["_key"] => $context["domain"]) {
            // line 45
            echo "<h3 id=\"dom_";
            if (isset($context["domain"])) { $_domain_ = $context["domain"]; } else { $_domain_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_domain_, "id"), "html", null, true);
            echo "\">";
            if (isset($context["domain"])) { $_domain_ = $context["domain"]; } else { $_domain_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_domain_, "name"), "html", null, true);
            echo "</h3>

<table class=\"table table-striped table-bordered table-condensed cat-table\">

    ";
            // line 49
            if (isset($context["domain"])) { $_domain_ = $context["domain"]; } else { $_domain_ = null; }
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($_domain_, "categories"));
            foreach ($context['_seq'] as $context["_key"] => $context["cat"]) {
                // line 50
                echo "    <tr>
        <th>";
                // line 51
                if (isset($context["cat"])) { $_cat_ = $context["cat"]; } else { $_cat_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($_cat_, "name"), "html", null, true);
                echo "</th>
    </tr>

    ";
                // line 54
                if (isset($context["groups"])) { $_groups_ = $context["groups"]; } else { $_groups_ = null; }
                if (isset($context["cat"])) { $_cat_ = $context["cat"]; } else { $_cat_ = null; }
                if (($this->getAttribute($_groups_, $this->getAttribute($_cat_, "id"), array(), "array", true, true) && (!twig_test_empty($this->getAttribute($_groups_, $this->getAttribute($_cat_, "id"), array(), "array"))))) {
                    // line 55
                    echo "    ";
                    if (isset($context["groups"])) { $_groups_ = $context["groups"]; } else { $_groups_ = null; }
                    if (isset($context["cat"])) { $_cat_ = $context["cat"]; } else { $_cat_ = null; }
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($_groups_, $this->getAttribute($_cat_, "id"), array(), "array"));
                    foreach ($context['_seq'] as $context["_key"] => $context["requestinfo"]) {
                        // line 56
                        echo "    <tr>
        <td>
        ";
                        // line 58
                        if (isset($context["admin"])) { $_admin_ = $context["admin"]; } else { $_admin_ = null; }
                        if ($_admin_) {
                            // line 59
                            echo "            <i class=\"icon-eye-close";
                            if (isset($context["requestinfo"])) { $_requestinfo_ = $context["requestinfo"]; } else { $_requestinfo_ = null; }
                            echo (((!$this->getAttribute($this->getAttribute($_requestinfo_, "request"), "validated"))) ? ("") : (" icon-white"));
                            echo "\"></i>
        ";
                        }
                        // line 61
                        echo "            <i class=\"icon-lock";
                        if (isset($context["requestinfo"])) { $_requestinfo_ = $context["requestinfo"]; } else { $_requestinfo_ = null; }
                        echo (($this->getAttribute($this->getAttribute($_requestinfo_, "request"), "close")) ? ("") : (" icon-white"));
                        echo "\"></i>
            <span class=\"badge";
                        // line 62
                        if (isset($context["requestinfo"])) { $_requestinfo_ = $context["requestinfo"]; } else { $_requestinfo_ = null; }
                        if (($this->getAttribute($_requestinfo_, "subscriptionCount") > 0)) {
                            echo " badge-info";
                        }
                        echo "\"
                  title=\"Nombre de personnes intéressées\">";
                        // line 63
                        if (isset($context["requestinfo"])) { $_requestinfo_ = $context["requestinfo"]; } else { $_requestinfo_ = null; }
                        echo twig_escape_filter($this->env, $this->getAttribute($_requestinfo_, "subscriptionCount"), "html", null, true);
                        echo "</span>
            <a href=\"";
                        // line 64
                        if (isset($context["laplace_training"])) { $_laplace_training_ = $context["laplace_training"]; } else { $_laplace_training_ = null; }
                        if (isset($context["requestinfo"])) { $_requestinfo_ = $context["requestinfo"]; } else { $_requestinfo_ = null; }
                        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath(($_laplace_training_ . "view_request"), array("id" => $this->getAttribute($this->getAttribute($_requestinfo_, "request"), "id"))), "html", null, true);
                        echo "\">";
                        if (isset($context["requestinfo"])) { $_requestinfo_ = $context["requestinfo"]; } else { $_requestinfo_ = null; }
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_requestinfo_, "request"), "title"), "html", null, true);
                        echo "</a>
        </td>
    </tr>
    ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['requestinfo'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 68
                    echo "    ";
                } else {
                    // line 69
                    echo "    <tr>
        <td><em>Vide</em></td>
    </tr>
    ";
                }
                // line 73
                echo "
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['cat'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 75
            echo "
</table>

<hr />
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['domain'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 80
        echo "
";
        // line 81
        if (isset($context["groups"])) { $_groups_ = $context["groups"]; } else { $_groups_ = null; }
        if ((!twig_test_empty($this->getAttribute($_groups_, null, array(), "array")))) {
            // line 82
            echo "<h3 id=\"dom_other\">Autre</h3>

<table class=\"table table-striped table-bordered table-condensed cat-table\">

    <tr>
        <th>Demandes non classées</th>
    </tr>

    ";
            // line 90
            if (isset($context["groups"])) { $_groups_ = $context["groups"]; } else { $_groups_ = null; }
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($_groups_, null, array(), "array"));
            foreach ($context['_seq'] as $context["_key"] => $context["requestinfo"]) {
                // line 91
                echo "    <tr>
        <td>
        ";
                // line 93
                if (isset($context["admin"])) { $_admin_ = $context["admin"]; } else { $_admin_ = null; }
                if ($_admin_) {
                    // line 94
                    echo "            <i class=\"icon-eye-close";
                    if (isset($context["requestinfo"])) { $_requestinfo_ = $context["requestinfo"]; } else { $_requestinfo_ = null; }
                    echo (((!$this->getAttribute($this->getAttribute($_requestinfo_, "request"), "validated"))) ? ("") : (" icon-white"));
                    echo "\"></i>
        ";
                }
                // line 96
                echo "            <i class=\"icon-lock";
                if (isset($context["requestinfo"])) { $_requestinfo_ = $context["requestinfo"]; } else { $_requestinfo_ = null; }
                echo (($this->getAttribute($this->getAttribute($_requestinfo_, "request"), "close")) ? ("") : (" icon-white"));
                echo "\"></i>
            <span class=\"badge";
                // line 97
                if (isset($context["requestinfo"])) { $_requestinfo_ = $context["requestinfo"]; } else { $_requestinfo_ = null; }
                if (($this->getAttribute($_requestinfo_, "subscriptionCount") > 0)) {
                    echo " badge-info";
                }
                echo "\"
                  title=\"Nombre de personnes intéressées\">";
                // line 98
                if (isset($context["requestinfo"])) { $_requestinfo_ = $context["requestinfo"]; } else { $_requestinfo_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($_requestinfo_, "subscriptionCount"), "html", null, true);
                echo "</span>
            <a href=\"";
                // line 99
                if (isset($context["laplace_training"])) { $_laplace_training_ = $context["laplace_training"]; } else { $_laplace_training_ = null; }
                if (isset($context["requestinfo"])) { $_requestinfo_ = $context["requestinfo"]; } else { $_requestinfo_ = null; }
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath(($_laplace_training_ . "view_request"), array("id" => $this->getAttribute($this->getAttribute($_requestinfo_, "request"), "id"))), "html", null, true);
                echo "\">";
                if (isset($context["requestinfo"])) { $_requestinfo_ = $context["requestinfo"]; } else { $_requestinfo_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_requestinfo_, "request"), "title"), "html", null, true);
                echo "</a>
        </td>
    </tr>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['requestinfo'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 103
            echo "
</table>

<hr />

";
        }
        // line 109
        echo "
<p class=\"text-right\">
    <i class=\"icon-plus\"></i>
    <a href=\"";
        // line 112
        if (isset($context["laplace_training"])) { $_laplace_training_ = $context["laplace_training"]; } else { $_laplace_training_ = null; }
        echo $this->env->getExtension('routing')->getPath(($_laplace_training_ . "create_request"));
        echo "\">Ajouter une demande qui ne figure pas dans cette liste</a>
</p>

";
    }

    public function getTemplateName()
    {
        return "LaplaceTrainingBundle:Request:view-all.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  299 => 97,  217 => 64,  167 => 51,  910 => 400,  904 => 397,  896 => 393,  890 => 391,  884 => 389,  875 => 384,  872 => 383,  870 => 382,  867 => 381,  856 => 372,  853 => 371,  848 => 368,  843 => 364,  840 => 362,  836 => 360,  828 => 354,  822 => 350,  804 => 347,  795 => 346,  777 => 345,  771 => 341,  755 => 338,  752 => 337,  748 => 336,  742 => 335,  724 => 334,  710 => 322,  707 => 321,  704 => 320,  701 => 318,  697 => 316,  690 => 313,  683 => 310,  653 => 284,  650 => 283,  648 => 282,  645 => 281,  640 => 277,  633 => 274,  625 => 270,  611 => 260,  608 => 259,  606 => 258,  603 => 257,  598 => 253,  591 => 250,  583 => 246,  577 => 244,  571 => 242,  556 => 231,  553 => 230,  551 => 229,  548 => 228,  536 => 221,  528 => 217,  514 => 207,  511 => 206,  509 => 205,  506 => 204,  503 => 202,  492 => 195,  487 => 192,  481 => 188,  475 => 184,  461 => 176,  454 => 171,  451 => 170,  446 => 167,  443 => 166,  432 => 162,  428 => 160,  425 => 159,  412 => 150,  409 => 149,  406 => 148,  403 => 147,  398 => 144,  393 => 140,  389 => 137,  371 => 133,  357 => 128,  338 => 122,  333 => 121,  320 => 114,  312 => 111,  284 => 97,  275 => 94,  236 => 81,  233 => 80,  230 => 79,  154 => 45,  378 => 135,  370 => 143,  356 => 137,  346 => 131,  316 => 120,  310 => 119,  290 => 99,  274 => 90,  254 => 87,  248 => 75,  240 => 91,  203 => 70,  132 => 37,  213 => 71,  206 => 71,  119 => 26,  306 => 98,  301 => 100,  278 => 95,  266 => 91,  262 => 101,  200 => 59,  141 => 50,  539 => 243,  533 => 240,  525 => 236,  519 => 234,  513 => 232,  504 => 227,  501 => 226,  499 => 200,  496 => 224,  485 => 215,  482 => 214,  476 => 210,  472 => 208,  467 => 204,  464 => 202,  457 => 199,  442 => 192,  439 => 165,  422 => 158,  414 => 178,  402 => 174,  387 => 163,  384 => 162,  382 => 161,  379 => 160,  374 => 156,  367 => 131,  342 => 129,  339 => 112,  337 => 134,  334 => 109,  329 => 129,  322 => 122,  300 => 112,  297 => 111,  292 => 109,  289 => 107,  283 => 93,  280 => 102,  276 => 101,  269 => 92,  261 => 81,  250 => 89,  241 => 73,  184 => 65,  176 => 53,  73 => 29,  99 => 25,  79 => 14,  257 => 100,  249 => 74,  253 => 90,  235 => 69,  198 => 67,  177 => 62,  174 => 54,  169 => 40,  68 => 25,  61 => 22,  341 => 124,  336 => 101,  331 => 97,  326 => 103,  324 => 101,  317 => 97,  314 => 122,  311 => 99,  305 => 104,  303 => 95,  298 => 104,  291 => 85,  288 => 84,  281 => 96,  273 => 89,  270 => 79,  265 => 102,  263 => 90,  260 => 89,  258 => 80,  252 => 77,  244 => 76,  223 => 67,  218 => 75,  159 => 49,  96 => 23,  86 => 15,  246 => 109,  210 => 69,  245 => 84,  237 => 81,  225 => 77,  209 => 72,  194 => 62,  150 => 49,  95 => 18,  128 => 36,  214 => 52,  191 => 65,  185 => 56,  42 => 10,  293 => 96,  286 => 94,  277 => 107,  267 => 119,  255 => 78,  238 => 81,  229 => 79,  220 => 70,  212 => 63,  188 => 60,  161 => 57,  144 => 47,  135 => 41,  126 => 29,  172 => 51,  165 => 52,  151 => 65,  116 => 29,  113 => 28,  153 => 39,  145 => 43,  139 => 43,  127 => 38,  112 => 30,  87 => 16,  94 => 15,  91 => 20,  23 => 3,  170 => 48,  125 => 35,  103 => 22,  82 => 9,  549 => 162,  543 => 224,  538 => 158,  530 => 155,  526 => 153,  522 => 151,  512 => 149,  505 => 148,  502 => 147,  497 => 146,  491 => 144,  488 => 143,  483 => 142,  473 => 134,  469 => 180,  466 => 179,  460 => 130,  455 => 129,  450 => 196,  444 => 122,  441 => 121,  437 => 190,  434 => 189,  429 => 185,  423 => 112,  420 => 111,  416 => 110,  413 => 109,  408 => 176,  394 => 105,  390 => 103,  375 => 134,  365 => 99,  362 => 139,  359 => 149,  355 => 95,  348 => 127,  344 => 125,  330 => 120,  327 => 119,  321 => 86,  307 => 118,  302 => 106,  295 => 103,  287 => 80,  279 => 91,  271 => 77,  264 => 82,  256 => 91,  251 => 86,  247 => 77,  239 => 82,  231 => 79,  219 => 75,  201 => 60,  147 => 45,  143 => 37,  134 => 33,  131 => 39,  122 => 43,  102 => 20,  92 => 17,  84 => 114,  72 => 10,  48 => 18,  35 => 7,  29 => 5,  76 => 7,  69 => 5,  54 => 14,  51 => 19,  31 => 11,  205 => 62,  199 => 61,  190 => 58,  182 => 60,  179 => 58,  175 => 58,  168 => 53,  164 => 50,  156 => 53,  148 => 47,  138 => 34,  123 => 30,  117 => 40,  108 => 23,  83 => 13,  71 => 10,  64 => 23,  110 => 28,  89 => 14,  65 => 14,  63 => 17,  58 => 15,  34 => 12,  227 => 58,  224 => 77,  221 => 76,  207 => 82,  197 => 74,  195 => 62,  192 => 59,  189 => 58,  186 => 53,  181 => 53,  178 => 55,  173 => 53,  162 => 47,  158 => 48,  155 => 36,  152 => 30,  142 => 44,  136 => 43,  133 => 40,  130 => 33,  120 => 30,  105 => 21,  100 => 22,  75 => 53,  53 => 13,  39 => 15,  98 => 19,  80 => 12,  78 => 42,  46 => 16,  44 => 10,  36 => 6,  32 => 8,  60 => 21,  57 => 9,  40 => 10,  114 => 25,  109 => 26,  106 => 29,  101 => 20,  88 => 19,  85 => 18,  77 => 7,  67 => 27,  47 => 17,  28 => 18,  25 => 7,  55 => 21,  43 => 11,  38 => 7,  26 => 8,  24 => 3,  50 => 7,  27 => 2,  22 => 6,  19 => 2,  232 => 68,  226 => 74,  222 => 80,  215 => 74,  211 => 84,  208 => 65,  202 => 66,  196 => 59,  193 => 65,  187 => 58,  183 => 56,  180 => 55,  171 => 54,  166 => 50,  163 => 58,  160 => 49,  157 => 43,  149 => 48,  146 => 51,  140 => 41,  137 => 42,  129 => 37,  124 => 34,  121 => 33,  118 => 32,  115 => 31,  111 => 22,  107 => 27,  104 => 26,  97 => 19,  93 => 19,  90 => 34,  81 => 12,  70 => 5,  66 => 13,  62 => 24,  59 => 17,  56 => 21,  52 => 20,  49 => 17,  45 => 13,  41 => 7,  37 => 9,  33 => 11,  30 => 10,);
    }
}
