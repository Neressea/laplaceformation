<?php

/* ::layout.html.twig */
class __TwigTemplate_9867afb022bebb0c4c96cc3ba0780b9e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'PageTitle' => array($this, 'block_PageTitle'),
            'StyleSheets' => array($this, 'block_StyleSheets'),
            'NavbarOuter' => array($this, 'block_NavbarOuter'),
            'NavbarLeft' => array($this, 'block_NavbarLeft'),
            'NavbarRight' => array($this, 'block_NavbarRight'),
            'MainContainerOuter' => array($this, 'block_MainContainerOuter'),
            'MainContainer' => array($this, 'block_MainContainer'),
            'SidebarOuter' => array($this, 'block_SidebarOuter'),
            'Sidebar' => array($this, 'block_Sidebar'),
            'ContentOuter' => array($this, 'block_ContentOuter'),
            'Content' => array($this, 'block_Content'),
            'ContentTitle' => array($this, 'block_ContentTitle'),
            'ContentBody' => array($this, 'block_ContentBody'),
            'JavaScripts' => array($this, 'block_JavaScripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "<!DOCTYPE html>
<html>
    <head>
        <title>";
        // line 7
        $this->displayBlock('PageTitle', $context, $blocks);
        echo "</title>
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />

        <link href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bootstrap/css/cerulean.bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" media=\"screen\" />
        <link href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" media=\"screen\" />

        ";
        // line 13
        $this->displayBlock('StyleSheets', $context, $blocks);
        // line 14
        echo "
        <link href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bootstrap/css/bootstrap-responsive.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" media=\"screen\" />

        <script src=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery-1.10.1.min.js"), "html", null, true);
        echo "\"></script>
    </head>
    <body>
        <!-- NAVBAR -->
        ";
        // line 21
        $this->displayBlock('NavbarOuter', $context, $blocks);
        // line 52
        echo "
        ";
        // line 53
        $this->displayBlock('MainContainerOuter', $context, $blocks);
        // line 112
        echo "
        <script src=\"";
        // line 113
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bootstrap/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 114
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/user-selector-typeahead.js"), "html", null, true);
        echo "\"></script>

        <input type=\"hidden\" id=\"api_find_users\" data-url=\"";
        // line 116
        echo $this->env->getExtension('routing')->getPath("laplace_user_api_find_users");
        echo "\" />

        ";
        // line 118
        $this->displayBlock('JavaScripts', $context, $blocks);
        // line 119
        echo "    </body>
</html>
";
    }

    // line 7
    public function block_PageTitle($context, array $blocks = array())
    {
        echo "Laplace";
    }

    // line 13
    public function block_StyleSheets($context, array $blocks = array())
    {
    }

    // line 21
    public function block_NavbarOuter($context, array $blocks = array())
    {
        // line 22
        echo "        <div class=\"navbar-wrapper\">
            <div class=\"container\">
                <div class=\"navbar";
        // line 24
        echo ((array_key_exists("inverse_navbar", $context)) ? (" navbar-inverse") : (""));
        echo "\">
                    <div class=\"navbar-inner\">
                        <a class=\"brand\" href=\"";
        // line 26
        echo $this->env->getExtension('routing')->getPath("laplace_common_homepage");
        echo "\">Laplace</a>

                        <ul class=\"nav\">
                        ";
        // line 29
        $this->displayBlock('NavbarLeft', $context, $blocks);
        // line 31
        echo "                        </ul>

                        <ul class=\"nav pull-right\">
                        ";
        // line 34
        $this->displayBlock('NavbarRight', $context, $blocks);
        // line 46
        echo "                        </ul>
                    </div>
                </div>
            </div>
        </div>
        ";
    }

    // line 29
    public function block_NavbarLeft($context, array $blocks = array())
    {
        // line 30
        echo "                        ";
    }

    // line 34
    public function block_NavbarRight($context, array $blocks = array())
    {
        // line 35
        echo "
                            ";
        // line 36
        if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
        if ($this->getAttribute($_app_, "user")) {
            // line 37
            echo "                            <li>
                                <a href=\"";
            // line 38
            echo $this->env->getExtension('routing')->getPath("logout");
            echo "\">
                                    <i class=\"icon-off icon-white\"></i>
                                    Déconnexion (";
            // line 40
            if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_app_, "user"), "username"), "html", null, true);
            echo ")
                                </a>
                            </li>
                            ";
        }
        // line 44
        echo "
                        ";
    }

    // line 53
    public function block_MainContainerOuter($context, array $blocks = array())
    {
        // line 54
        echo "        <!-- Main Container -->
        <div class=\"container\">

            <div class=\"row\">
                <div class=\"span12\">
                    ";
        // line 59
        if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute($_app_, "session"), "flashbag"), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 60
            echo "                    <div class=\"alert\">
                        <strong>Attention</strong><br />
                        <i class=\"icon-warning-sign\"></i> ";
            // line 62
            if (isset($context["message"])) { $_message_ = $context["message"]; } else { $_message_ = null; }
            echo twig_escape_filter($this->env, $_message_, "html", null, true);
            echo "
                    </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 65
        echo "
                    ";
        // line 66
        if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute($_app_, "session"), "flashbag"), "get", array(0 => "info"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 67
            echo "                    <div class=\"alert alert-success\">
                        <strong>Information</strong><br />
                        <i class=\"icon-ok\"></i> ";
            // line 69
            if (isset($context["message"])) { $_message_ = $context["message"]; } else { $_message_ = null; }
            echo twig_escape_filter($this->env, $_message_, "html", null, true);
            echo "
                    </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 72
        echo "                </div>
            </div>


            <div class=\"row\">
                ";
        // line 77
        $this->displayBlock('MainContainer', $context, $blocks);
        // line 109
        echo "            </div>
        </div>
        ";
    }

    // line 77
    public function block_MainContainer($context, array $blocks = array())
    {
        // line 78
        echo "
                    ";
        // line 79
        $this->displayBlock('SidebarOuter', $context, $blocks);
        // line 90
        echo "
                    ";
        // line 91
        $this->displayBlock('ContentOuter', $context, $blocks);
        // line 107
        echo "
                ";
    }

    // line 79
    public function block_SidebarOuter($context, array $blocks = array())
    {
        // line 80
        echo "                    <!-- SIDEBAR -->
                    <div class=\"span3\">
                        <div class=\"well sidebar-nav\">
                            <ul class=\"nav nav-list\">
                                ";
        // line 84
        $this->displayBlock('Sidebar', $context, $blocks);
        // line 86
        echo "                            </ul>
                        </div>
                    </div>
                    ";
    }

    // line 84
    public function block_Sidebar($context, array $blocks = array())
    {
        // line 85
        echo "                                ";
    }

    // line 91
    public function block_ContentOuter($context, array $blocks = array())
    {
        // line 92
        echo "                    <!-- BODY -->
                    <div class=\"span9\">
                        <div class=\"well\">
                            ";
        // line 95
        $this->displayBlock('Content', $context, $blocks);
        // line 104
        echo "                        </div>
                    </div>
                    ";
    }

    // line 95
    public function block_Content($context, array $blocks = array())
    {
        // line 96
        echo "
                            <h1>";
        // line 97
        $this->displayBlock('ContentTitle', $context, $blocks);
        echo "</h1>

                            <hr />

                            ";
        // line 101
        $this->displayBlock('ContentBody', $context, $blocks);
        // line 102
        echo "
                            ";
    }

    // line 97
    public function block_ContentTitle($context, array $blocks = array())
    {
    }

    // line 101
    public function block_ContentBody($context, array $blocks = array())
    {
    }

    // line 118
    public function block_JavaScripts($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  341 => 118,  336 => 101,  331 => 97,  326 => 102,  324 => 101,  317 => 97,  314 => 96,  311 => 95,  305 => 104,  303 => 95,  298 => 92,  291 => 85,  288 => 84,  281 => 86,  273 => 80,  270 => 79,  265 => 107,  263 => 91,  260 => 90,  258 => 79,  252 => 77,  244 => 77,  223 => 67,  218 => 66,  159 => 35,  96 => 119,  86 => 45,  246 => 109,  210 => 69,  245 => 84,  237 => 72,  225 => 76,  209 => 68,  194 => 61,  150 => 41,  95 => 14,  128 => 37,  214 => 96,  191 => 87,  185 => 85,  42 => 15,  293 => 127,  286 => 125,  277 => 123,  267 => 119,  255 => 78,  238 => 81,  229 => 97,  220 => 75,  212 => 88,  188 => 59,  161 => 57,  144 => 44,  135 => 32,  126 => 32,  172 => 51,  165 => 37,  151 => 65,  116 => 22,  113 => 21,  153 => 45,  145 => 61,  139 => 60,  127 => 33,  112 => 21,  87 => 13,  94 => 118,  91 => 48,  23 => 3,  170 => 62,  125 => 26,  103 => 19,  82 => 9,  549 => 162,  543 => 161,  538 => 158,  530 => 155,  526 => 153,  522 => 151,  512 => 149,  505 => 148,  502 => 147,  497 => 146,  491 => 144,  488 => 143,  483 => 142,  473 => 134,  469 => 132,  466 => 131,  460 => 130,  455 => 129,  450 => 126,  444 => 122,  441 => 121,  437 => 120,  434 => 119,  429 => 116,  423 => 112,  420 => 111,  416 => 110,  413 => 109,  408 => 106,  394 => 105,  390 => 103,  375 => 101,  365 => 99,  362 => 98,  359 => 97,  355 => 95,  348 => 91,  344 => 90,  330 => 89,  327 => 88,  321 => 86,  307 => 85,  302 => 84,  295 => 91,  287 => 80,  279 => 84,  271 => 77,  264 => 118,  256 => 73,  251 => 71,  247 => 107,  239 => 69,  231 => 78,  219 => 74,  201 => 60,  147 => 51,  143 => 49,  134 => 36,  131 => 29,  122 => 35,  102 => 7,  92 => 25,  84 => 114,  72 => 52,  48 => 11,  35 => 5,  29 => 3,  76 => 7,  69 => 5,  54 => 14,  51 => 8,  31 => 3,  205 => 62,  199 => 91,  190 => 66,  182 => 57,  179 => 67,  175 => 58,  168 => 38,  164 => 47,  156 => 34,  148 => 47,  138 => 34,  123 => 30,  117 => 28,  108 => 13,  83 => 44,  71 => 10,  64 => 16,  110 => 20,  89 => 116,  65 => 14,  63 => 17,  58 => 15,  34 => 5,  227 => 69,  224 => 91,  221 => 90,  207 => 82,  197 => 74,  195 => 65,  192 => 72,  189 => 54,  186 => 53,  181 => 44,  178 => 83,  173 => 40,  162 => 36,  158 => 69,  155 => 55,  152 => 30,  142 => 47,  136 => 41,  133 => 31,  130 => 28,  120 => 24,  105 => 25,  100 => 18,  75 => 53,  53 => 13,  39 => 7,  98 => 52,  80 => 113,  78 => 42,  46 => 16,  44 => 10,  36 => 6,  32 => 8,  60 => 21,  57 => 9,  40 => 12,  114 => 25,  109 => 48,  106 => 19,  101 => 53,  88 => 12,  85 => 10,  77 => 112,  67 => 9,  47 => 6,  28 => 4,  25 => 5,  55 => 14,  43 => 7,  38 => 7,  26 => 8,  24 => 3,  50 => 7,  27 => 2,  22 => 2,  19 => 1,  232 => 79,  226 => 77,  222 => 76,  215 => 65,  211 => 84,  208 => 95,  202 => 65,  196 => 59,  193 => 65,  187 => 62,  183 => 60,  180 => 59,  171 => 54,  166 => 51,  163 => 50,  160 => 49,  157 => 43,  149 => 29,  146 => 40,  140 => 46,  137 => 37,  129 => 36,  124 => 35,  121 => 24,  118 => 29,  115 => 59,  111 => 57,  107 => 28,  104 => 54,  97 => 16,  93 => 16,  90 => 13,  81 => 12,  70 => 21,  66 => 13,  62 => 11,  59 => 15,  56 => 12,  52 => 10,  49 => 17,  45 => 6,  41 => 7,  37 => 14,  33 => 4,  30 => 3,);
    }
}
