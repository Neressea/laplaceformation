<?php

/* LaplaceTrainingBundle:Request:view-active-subscriptions.html.twig */
class __TwigTemplate_a66ab1d31fbfeb99c6b676e3a8ff4599 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
";
        // line 2
        $this->env->loadTemplate("LaplaceTrainingBundle:Request:view-active-subscriptions.html.twig", "210076270")->display(array_merge($context, array("page" => array(0 => "request", 1 => "personal"))));
    }

    public function getTemplateName()
    {
        return "LaplaceTrainingBundle:Request:view-active-subscriptions.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  378 => 148,  370 => 143,  356 => 137,  346 => 131,  316 => 120,  310 => 119,  290 => 113,  274 => 106,  254 => 99,  248 => 95,  240 => 91,  203 => 68,  132 => 37,  213 => 71,  206 => 68,  119 => 26,  306 => 103,  301 => 100,  278 => 90,  266 => 88,  262 => 101,  200 => 59,  141 => 50,  539 => 243,  533 => 240,  525 => 236,  519 => 234,  513 => 232,  504 => 227,  501 => 226,  499 => 225,  496 => 224,  485 => 215,  482 => 214,  476 => 210,  472 => 208,  467 => 204,  464 => 202,  457 => 199,  442 => 192,  439 => 191,  422 => 182,  414 => 178,  402 => 174,  387 => 163,  384 => 162,  382 => 161,  379 => 160,  374 => 156,  367 => 153,  342 => 129,  339 => 135,  337 => 134,  334 => 126,  329 => 129,  322 => 122,  300 => 112,  297 => 111,  292 => 109,  289 => 107,  283 => 103,  280 => 102,  276 => 101,  269 => 103,  261 => 94,  250 => 89,  241 => 75,  184 => 65,  176 => 51,  73 => 29,  99 => 25,  79 => 11,  257 => 100,  249 => 74,  253 => 90,  235 => 87,  198 => 64,  177 => 62,  174 => 41,  169 => 40,  68 => 25,  61 => 22,  341 => 118,  336 => 101,  331 => 97,  326 => 102,  324 => 101,  317 => 97,  314 => 122,  311 => 95,  305 => 104,  303 => 95,  298 => 92,  291 => 85,  288 => 84,  281 => 86,  273 => 89,  270 => 79,  265 => 102,  263 => 91,  260 => 90,  258 => 79,  252 => 77,  244 => 76,  223 => 67,  218 => 64,  159 => 46,  96 => 23,  86 => 15,  246 => 109,  210 => 69,  245 => 94,  237 => 81,  225 => 77,  209 => 69,  194 => 62,  150 => 42,  95 => 18,  128 => 36,  214 => 52,  191 => 87,  185 => 85,  42 => 10,  293 => 94,  286 => 111,  277 => 107,  267 => 119,  255 => 78,  238 => 81,  229 => 79,  220 => 75,  212 => 75,  188 => 60,  161 => 57,  144 => 44,  135 => 33,  126 => 29,  172 => 52,  165 => 38,  151 => 65,  116 => 29,  113 => 28,  153 => 39,  145 => 43,  139 => 43,  127 => 38,  112 => 30,  87 => 16,  94 => 22,  91 => 20,  23 => 3,  170 => 48,  125 => 37,  103 => 22,  82 => 9,  549 => 162,  543 => 161,  538 => 158,  530 => 155,  526 => 153,  522 => 151,  512 => 149,  505 => 148,  502 => 147,  497 => 146,  491 => 144,  488 => 143,  483 => 142,  473 => 134,  469 => 132,  466 => 131,  460 => 130,  455 => 129,  450 => 196,  444 => 122,  441 => 121,  437 => 190,  434 => 189,  429 => 185,  423 => 112,  420 => 111,  416 => 110,  413 => 109,  408 => 176,  394 => 105,  390 => 103,  375 => 101,  365 => 99,  362 => 139,  359 => 149,  355 => 95,  348 => 91,  344 => 90,  330 => 89,  327 => 123,  321 => 86,  307 => 118,  302 => 117,  295 => 114,  287 => 80,  279 => 84,  271 => 77,  264 => 95,  256 => 91,  251 => 71,  247 => 77,  239 => 68,  231 => 79,  219 => 75,  201 => 60,  147 => 34,  143 => 37,  134 => 33,  131 => 39,  122 => 43,  102 => 39,  92 => 17,  84 => 114,  72 => 27,  48 => 18,  35 => 7,  29 => 5,  76 => 7,  69 => 5,  54 => 14,  51 => 19,  31 => 11,  205 => 48,  199 => 71,  190 => 58,  182 => 58,  179 => 67,  175 => 58,  168 => 61,  164 => 48,  156 => 53,  148 => 47,  138 => 34,  123 => 30,  117 => 40,  108 => 32,  83 => 14,  71 => 10,  64 => 23,  110 => 28,  89 => 12,  65 => 14,  63 => 17,  58 => 15,  34 => 12,  227 => 58,  224 => 68,  221 => 54,  207 => 82,  197 => 74,  195 => 58,  192 => 68,  189 => 66,  186 => 53,  181 => 53,  178 => 52,  173 => 53,  162 => 44,  158 => 48,  155 => 36,  152 => 30,  142 => 47,  136 => 43,  133 => 34,  130 => 33,  120 => 27,  105 => 31,  100 => 22,  75 => 53,  53 => 13,  39 => 15,  98 => 19,  80 => 12,  78 => 42,  46 => 16,  44 => 10,  36 => 6,  32 => 8,  60 => 21,  57 => 9,  40 => 10,  114 => 25,  109 => 26,  106 => 29,  101 => 20,  88 => 11,  85 => 10,  77 => 11,  67 => 27,  47 => 17,  28 => 18,  25 => 7,  55 => 21,  43 => 11,  38 => 7,  26 => 8,  24 => 3,  50 => 7,  27 => 2,  22 => 2,  19 => 1,  232 => 85,  226 => 78,  222 => 80,  215 => 63,  211 => 84,  208 => 72,  202 => 66,  196 => 59,  193 => 65,  187 => 44,  183 => 60,  180 => 59,  171 => 58,  166 => 50,  163 => 58,  160 => 49,  157 => 43,  149 => 48,  146 => 51,  140 => 41,  137 => 36,  129 => 46,  124 => 27,  121 => 24,  118 => 25,  115 => 31,  111 => 22,  107 => 27,  104 => 19,  97 => 19,  93 => 19,  90 => 34,  81 => 12,  70 => 9,  66 => 13,  62 => 24,  59 => 17,  56 => 21,  52 => 20,  49 => 17,  45 => 13,  41 => 7,  37 => 9,  33 => 11,  30 => 10,);
    }
}


/* LaplaceTrainingBundle:Request:view-active-subscriptions.html.twig */
class __TwigTemplate_a66ab1d31fbfeb99c6b676e3a8ff4599_210076270 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("LaplaceCommonBundle::user-page.html.twig");

        $this->blocks = array(
            'PageTitle' => array($this, 'block_PageTitle'),
            'StyleSheets' => array($this, 'block_StyleSheets'),
            'ContentTitle' => array($this, 'block_ContentTitle'),
            'ContentBody' => array($this, 'block_ContentBody'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "LaplaceCommonBundle::user-page.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_PageTitle($context, array $blocks = array())
    {
        $this->displayParentBlock("PageTitle", $context, $blocks);
        echo " - Demandes personnelles";
    }

    // line 7
    public function block_StyleSheets($context, array $blocks = array())
    {
        // line 8
        $this->displayParentBlock("StyleSheets", $context, $blocks);
        echo "
<style>
    .active-sub-table td,
    .active-sub-table th {
        width: 25%;
    }
</style>
";
    }

    // line 17
    public function block_ContentTitle($context, array $blocks = array())
    {
        echo "Demandes personnelles";
    }

    // line 19
    public function block_ContentBody($context, array $blocks = array())
    {
        // line 20
        echo "
<p>
    Cette page regroupe l'ensemble de vos <strong>inscriptions à des demandes de formation</strong>,
    classées par date d'ajout (de la plus récente à la moins récente).

    Cliquez sur le titre d'une demande pour voir sa description complète ou pour
    <strong>modifier votre inscription</strong>.
</p>

<br />

<table class=\"table table-striped table-bordered active-sub-table\">
    ";
        // line 32
        if (isset($context["subscriptions"])) { $_subscriptions_ = $context["subscriptions"]; } else { $_subscriptions_ = null; }
        if (twig_test_empty($_subscriptions_)) {
            // line 33
            echo "    <tr>
        <td>Aucune demande.</td>
    <tr>
    ";
        }
        // line 37
        echo "

    ";
        // line 39
        if (isset($context["subscriptions"])) { $_subscriptions_ = $context["subscriptions"]; } else { $_subscriptions_ = null; }
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($_subscriptions_);
        foreach ($context['_seq'] as $context["_key"] => $context["sub"]) {
            // line 40
            echo "    <tr>
        <td>
            <span class=\"badge badge-info\" title=\"";
            // line 42
            if (isset($context["sub"])) { $_sub_ = $context["sub"]; } else { $_sub_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_sub_, "type"), "description"), "html", null, true);
            echo "\">";
            if (isset($context["sub"])) { $_sub_ = $context["sub"]; } else { $_sub_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_sub_, "type"), "name"), "html", null, true);
            echo "</span>
            <a href=\"";
            // line 43
            if (isset($context["sub"])) { $_sub_ = $context["sub"]; } else { $_sub_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("laplace_training_view_request", array("id" => $this->getAttribute($this->getAttribute($_sub_, "request"), "id"))), "html", null, true);
            echo "\">";
            if (isset($context["sub"])) { $_sub_ = $context["sub"]; } else { $_sub_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_sub_, "request"), "title"), "html", null, true);
            echo "</a>
            <span class=\"pull-right text-right\">
                Ajoutée le ";
            // line 45
            if (isset($context["sub"])) { $_sub_ = $context["sub"]; } else { $_sub_ = null; }
            echo twig_escape_filter($this->env, twig_localized_date_filter($this->env, $this->getAttribute($_sub_, "subscriptionDate"), "long", "short"), "html", null, true);
            echo "<br />
            ";
            // line 46
            if (isset($context["sub"])) { $_sub_ = $context["sub"]; } else { $_sub_ = null; }
            if (($this->getAttribute($_sub_, "accepted") === true)) {
                // line 47
                echo "                <i class=\"icon-ok-sign\"></i> Acceptée /
                ";
                // line 48
                if (isset($context["sub"])) { $_sub_ = $context["sub"]; } else { $_sub_ = null; }
                if (($this->getAttribute($_sub_, "attended") === true)) {
                    // line 49
                    echo "                Effectuée
                ";
                } elseif (($this->getAttribute($_sub_, "attended") === false)) {
                    // line 51
                    echo "                Non effectuée
                ";
                } else {
                    // line 53
                    echo "                <span class=\"text-error\">En attente</span>
                ";
                }
                // line 55
                echo "            ";
            } elseif (($this->getAttribute($_sub_, "accepted") === false)) {
                // line 56
                echo "                <i class=\"icon-minus-sign\"></i> Refusée
            ";
            } else {
                // line 58
                echo "                <i class=\"icon-question-sign\"></i> En attente de validation
            ";
            }
            // line 60
            echo "            </span>
            <br />
            ";
            // line 62
            if (isset($context["sub"])) { $_sub_ = $context["sub"]; } else { $_sub_ = null; }
            if ($this->getAttribute($this->getAttribute($_sub_, "request"), "category")) {
                // line 63
                echo "            <small>";
                if (isset($context["sub"])) { $_sub_ = $context["sub"]; } else { $_sub_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($_sub_, "request"), "category"), "domain"), "name"), "html", null, true);
                echo " / <small>";
                if (isset($context["sub"])) { $_sub_ = $context["sub"]; } else { $_sub_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($_sub_, "request"), "category"), "name"), "html", null, true);
                echo "</small></small>
            ";
            } else {
                // line 65
                echo "            <small><em>Aucune catégorie</em></small>
            ";
            }
            // line 67
            echo "        </td>
    </tr>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sub'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 70
        echo "</table>

<p class=\"text-right\">
    <i class=\"icon-list\"></i>
    <a href=\"";
        // line 74
        echo $this->env->getExtension('routing')->getPath("laplace_training_all_requests");
        echo "\">Voir toutes les demandes</a>
</p>

";
    }

    public function getTemplateName()
    {
        return "LaplaceTrainingBundle:Request:view-active-subscriptions.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  154 => 45,  378 => 148,  370 => 143,  356 => 137,  346 => 131,  316 => 120,  310 => 119,  290 => 113,  274 => 106,  254 => 99,  248 => 95,  240 => 91,  203 => 68,  132 => 37,  213 => 71,  206 => 68,  119 => 26,  306 => 103,  301 => 100,  278 => 90,  266 => 88,  262 => 101,  200 => 59,  141 => 50,  539 => 243,  533 => 240,  525 => 236,  519 => 234,  513 => 232,  504 => 227,  501 => 226,  499 => 225,  496 => 224,  485 => 215,  482 => 214,  476 => 210,  472 => 208,  467 => 204,  464 => 202,  457 => 199,  442 => 192,  439 => 191,  422 => 182,  414 => 178,  402 => 174,  387 => 163,  384 => 162,  382 => 161,  379 => 160,  374 => 156,  367 => 153,  342 => 129,  339 => 135,  337 => 134,  334 => 126,  329 => 129,  322 => 122,  300 => 112,  297 => 111,  292 => 109,  289 => 107,  283 => 103,  280 => 102,  276 => 101,  269 => 103,  261 => 94,  250 => 89,  241 => 75,  184 => 65,  176 => 53,  73 => 29,  99 => 25,  79 => 11,  257 => 100,  249 => 74,  253 => 90,  235 => 87,  198 => 63,  177 => 62,  174 => 41,  169 => 40,  68 => 25,  61 => 22,  341 => 118,  336 => 101,  331 => 97,  326 => 102,  324 => 101,  317 => 97,  314 => 122,  311 => 95,  305 => 104,  303 => 95,  298 => 92,  291 => 85,  288 => 84,  281 => 86,  273 => 89,  270 => 79,  265 => 102,  263 => 91,  260 => 90,  258 => 79,  252 => 77,  244 => 76,  223 => 67,  218 => 64,  159 => 46,  96 => 23,  86 => 15,  246 => 109,  210 => 69,  245 => 94,  237 => 81,  225 => 77,  209 => 69,  194 => 62,  150 => 42,  95 => 18,  128 => 39,  214 => 52,  191 => 60,  185 => 85,  42 => 10,  293 => 94,  286 => 111,  277 => 107,  267 => 119,  255 => 78,  238 => 81,  229 => 79,  220 => 70,  212 => 67,  188 => 60,  161 => 57,  144 => 44,  135 => 33,  126 => 29,  172 => 51,  165 => 48,  151 => 65,  116 => 29,  113 => 28,  153 => 39,  145 => 43,  139 => 43,  127 => 38,  112 => 30,  87 => 16,  94 => 22,  91 => 20,  23 => 3,  170 => 48,  125 => 37,  103 => 22,  82 => 9,  549 => 162,  543 => 161,  538 => 158,  530 => 155,  526 => 153,  522 => 151,  512 => 149,  505 => 148,  502 => 147,  497 => 146,  491 => 144,  488 => 143,  483 => 142,  473 => 134,  469 => 132,  466 => 131,  460 => 130,  455 => 129,  450 => 196,  444 => 122,  441 => 121,  437 => 190,  434 => 189,  429 => 185,  423 => 112,  420 => 111,  416 => 110,  413 => 109,  408 => 176,  394 => 105,  390 => 103,  375 => 101,  365 => 99,  362 => 139,  359 => 149,  355 => 95,  348 => 91,  344 => 90,  330 => 89,  327 => 123,  321 => 86,  307 => 118,  302 => 117,  295 => 114,  287 => 80,  279 => 84,  271 => 77,  264 => 95,  256 => 91,  251 => 71,  247 => 77,  239 => 68,  231 => 79,  219 => 75,  201 => 60,  147 => 34,  143 => 37,  134 => 33,  131 => 39,  122 => 43,  102 => 39,  92 => 17,  84 => 114,  72 => 27,  48 => 18,  35 => 7,  29 => 5,  76 => 7,  69 => 5,  54 => 14,  51 => 19,  31 => 11,  205 => 48,  199 => 71,  190 => 58,  182 => 58,  179 => 67,  175 => 58,  168 => 49,  164 => 48,  156 => 53,  148 => 47,  138 => 34,  123 => 30,  117 => 40,  108 => 32,  83 => 14,  71 => 10,  64 => 23,  110 => 28,  89 => 12,  65 => 14,  63 => 17,  58 => 15,  34 => 12,  227 => 58,  224 => 68,  221 => 54,  207 => 82,  197 => 74,  195 => 62,  192 => 68,  189 => 66,  186 => 53,  181 => 53,  178 => 52,  173 => 53,  162 => 47,  158 => 48,  155 => 36,  152 => 30,  142 => 47,  136 => 43,  133 => 40,  130 => 33,  120 => 27,  105 => 31,  100 => 22,  75 => 53,  53 => 13,  39 => 15,  98 => 19,  80 => 8,  78 => 42,  46 => 16,  44 => 10,  36 => 6,  32 => 8,  60 => 21,  57 => 9,  40 => 10,  114 => 25,  109 => 26,  106 => 29,  101 => 20,  88 => 11,  85 => 10,  77 => 7,  67 => 27,  47 => 17,  28 => 18,  25 => 7,  55 => 21,  43 => 11,  38 => 7,  26 => 8,  24 => 3,  50 => 7,  27 => 2,  22 => 2,  19 => 1,  232 => 85,  226 => 74,  222 => 80,  215 => 63,  211 => 84,  208 => 65,  202 => 66,  196 => 59,  193 => 65,  187 => 58,  183 => 56,  180 => 55,  171 => 58,  166 => 50,  163 => 58,  160 => 49,  157 => 43,  149 => 48,  146 => 51,  140 => 41,  137 => 42,  129 => 46,  124 => 37,  121 => 24,  118 => 33,  115 => 32,  111 => 22,  107 => 27,  104 => 19,  97 => 19,  93 => 19,  90 => 34,  81 => 12,  70 => 5,  66 => 13,  62 => 24,  59 => 17,  56 => 21,  52 => 20,  49 => 17,  45 => 13,  41 => 7,  37 => 9,  33 => 11,  30 => 10,);
    }
}
