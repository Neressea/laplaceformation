<?php

/* LaplaceUserBundle:System:manage-sites.html.twig */
class __TwigTemplate_903ade8664053f4063b9c6b5e8564e92 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
";
        // line 2
        $this->env->loadTemplate("LaplaceUserBundle:System:manage-sites.html.twig", "266321725")->display(array_merge($context, array("page" => array(0 => "system", 1 => "sites"))));
    }

    public function getTemplateName()
    {
        return "LaplaceUserBundle:System:manage-sites.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  245 => 84,  237 => 80,  225 => 76,  209 => 68,  194 => 61,  150 => 41,  95 => 14,  128 => 37,  214 => 96,  191 => 87,  185 => 85,  42 => 15,  293 => 127,  286 => 125,  277 => 123,  267 => 119,  255 => 111,  238 => 102,  229 => 97,  220 => 100,  212 => 88,  188 => 59,  161 => 57,  144 => 44,  135 => 32,  126 => 32,  172 => 51,  165 => 47,  151 => 65,  116 => 50,  113 => 49,  153 => 45,  145 => 61,  139 => 60,  127 => 33,  112 => 21,  87 => 13,  94 => 14,  91 => 13,  23 => 3,  170 => 62,  125 => 31,  103 => 19,  82 => 9,  549 => 162,  543 => 161,  538 => 158,  530 => 155,  526 => 153,  522 => 151,  512 => 149,  505 => 148,  502 => 147,  497 => 146,  491 => 144,  488 => 143,  483 => 142,  473 => 134,  469 => 132,  466 => 131,  460 => 130,  455 => 129,  450 => 126,  444 => 122,  441 => 121,  437 => 120,  434 => 119,  429 => 116,  423 => 112,  420 => 111,  416 => 110,  413 => 109,  408 => 106,  394 => 105,  390 => 103,  375 => 101,  365 => 99,  362 => 98,  359 => 97,  355 => 95,  348 => 91,  344 => 90,  330 => 89,  327 => 88,  321 => 86,  307 => 85,  302 => 84,  295 => 81,  287 => 80,  279 => 78,  271 => 77,  264 => 118,  256 => 73,  251 => 71,  247 => 107,  239 => 69,  231 => 78,  219 => 74,  201 => 66,  147 => 51,  143 => 49,  134 => 36,  131 => 34,  122 => 35,  102 => 24,  92 => 25,  84 => 32,  72 => 27,  48 => 7,  35 => 5,  29 => 3,  76 => 7,  69 => 5,  54 => 14,  51 => 8,  31 => 7,  205 => 85,  199 => 91,  190 => 66,  182 => 57,  179 => 67,  175 => 58,  168 => 57,  164 => 47,  156 => 43,  148 => 47,  138 => 44,  123 => 30,  117 => 28,  108 => 27,  83 => 24,  71 => 10,  64 => 16,  110 => 20,  89 => 12,  65 => 14,  63 => 23,  58 => 10,  34 => 5,  227 => 92,  224 => 91,  221 => 90,  207 => 82,  197 => 74,  195 => 65,  192 => 72,  189 => 63,  186 => 60,  181 => 67,  178 => 83,  173 => 82,  162 => 58,  158 => 69,  155 => 55,  152 => 42,  142 => 47,  136 => 41,  133 => 35,  130 => 28,  120 => 25,  105 => 25,  100 => 18,  75 => 20,  53 => 19,  39 => 7,  98 => 15,  80 => 8,  78 => 28,  46 => 16,  44 => 9,  36 => 6,  32 => 8,  60 => 21,  57 => 9,  40 => 12,  114 => 25,  109 => 48,  106 => 19,  101 => 20,  88 => 12,  85 => 10,  77 => 12,  67 => 9,  47 => 6,  28 => 4,  25 => 5,  55 => 19,  43 => 7,  38 => 6,  26 => 8,  24 => 3,  50 => 7,  27 => 2,  22 => 2,  19 => 1,  232 => 82,  226 => 78,  222 => 76,  215 => 89,  211 => 84,  208 => 95,  202 => 65,  196 => 64,  193 => 65,  187 => 62,  183 => 60,  180 => 59,  171 => 54,  166 => 51,  163 => 50,  160 => 49,  157 => 43,  149 => 42,  146 => 40,  140 => 35,  137 => 37,  129 => 36,  124 => 35,  121 => 24,  118 => 29,  115 => 39,  111 => 26,  107 => 28,  104 => 47,  97 => 16,  93 => 16,  90 => 13,  81 => 12,  70 => 6,  66 => 13,  62 => 11,  59 => 15,  56 => 12,  52 => 10,  49 => 17,  45 => 6,  41 => 9,  37 => 14,  33 => 4,  30 => 3,);
    }
}


/* LaplaceUserBundle:System:manage-sites.html.twig */
class __TwigTemplate_903ade8664053f4063b9c6b5e8564e92_266321725 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("LaplaceCommonBundle::admin-page.html.twig");

        $this->blocks = array(
            'PageTitle' => array($this, 'block_PageTitle'),
            'ContentTitle' => array($this, 'block_ContentTitle'),
            'ContentBody' => array($this, 'block_ContentBody'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "LaplaceCommonBundle::admin-page.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_PageTitle($context, array $blocks = array())
    {
        $this->displayParentBlock("PageTitle", $context, $blocks);
        echo " - Sites";
    }

    // line 7
    public function block_ContentTitle($context, array $blocks = array())
    {
        echo "Sites";
    }

    // line 9
    public function block_ContentBody($context, array $blocks = array())
    {
        // line 10
        echo "
<h3>Renommer</h3>

";
        // line 13
        if (isset($context["edit_forms"])) { $_edit_forms_ = $context["edit_forms"]; } else { $_edit_forms_ = null; }
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($_edit_forms_);
        foreach ($context['_seq'] as $context["_key"] => $context["edit_form"]) {
            // line 14
            echo "
";
            // line 15
            if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
            echo             $this->env->getExtension('form')->renderer->renderBlock($_edit_form_, 'form_start');
            echo "

<fieldset>

    ";
            // line 19
            if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_edit_form_, "site"), 'rest');
            echo "

    ";
            // line 21
            if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($_edit_form_, 'rest');
            echo "

</fieldset>

";
            // line 25
            if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
            echo             $this->env->getExtension('form')->renderer->renderBlock($_edit_form_, 'form_end');
            echo "

";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['edit_form'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 28
        echo "
<hr />

";
        // line 32
        echo "
<h3>Créer / Supprimer / Remplacer</h3>

";
        // line 35
        if (isset($context["create_form"])) { $_create_form_ = $context["create_form"]; } else { $_create_form_ = null; }
        echo         $this->env->getExtension('form')->renderer->renderBlock($_create_form_, 'form_start');
        echo "

<fieldset>

    <legend>Créer un site</legend>

    ";
        // line 41
        if (isset($context["create_form"])) { $_create_form_ = $context["create_form"]; } else { $_create_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_create_form_, "site"), 'rest');
        echo "

    ";
        // line 43
        if (isset($context["create_form"])) { $_create_form_ = $context["create_form"]; } else { $_create_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($_create_form_, 'rest');
        echo "

</fieldset>

";
        // line 47
        if (isset($context["create_form"])) { $_create_form_ = $context["create_form"]; } else { $_create_form_ = null; }
        echo         $this->env->getExtension('form')->renderer->renderBlock($_create_form_, 'form_end');
        echo "



";
        // line 51
        if (isset($context["delete_form"])) { $_delete_form_ = $context["delete_form"]; } else { $_delete_form_ = null; }
        echo         $this->env->getExtension('form')->renderer->renderBlock($_delete_form_, 'form_start');
        echo "

<fieldset>

    <legend>Supprimer un site</legend>

    ";
        // line 57
        if (isset($context["delete_form"])) { $_delete_form_ = $context["delete_form"]; } else { $_delete_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_delete_form_, "site"), 'row');
        echo "

    ";
        // line 59
        if (isset($context["delete_form"])) { $_delete_form_ = $context["delete_form"]; } else { $_delete_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_delete_form_, "do"), 'row', array("attr" => array("class" => "btn-danger")));
        echo "

    ";
        // line 61
        if (isset($context["delete_form"])) { $_delete_form_ = $context["delete_form"]; } else { $_delete_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($_delete_form_, 'rest');
        echo "

</fieldset>

";
        // line 65
        if (isset($context["delete_form"])) { $_delete_form_ = $context["delete_form"]; } else { $_delete_form_ = null; }
        echo         $this->env->getExtension('form')->renderer->renderBlock($_delete_form_, 'form_end');
        echo "


";
        // line 68
        if (isset($context["replace_form"])) { $_replace_form_ = $context["replace_form"]; } else { $_replace_form_ = null; }
        echo         $this->env->getExtension('form')->renderer->renderBlock($_replace_form_, 'form_start');
        echo "

<fieldset>

    <legend>Remplacer un site par un autre</legend>

    ";
        // line 74
        if (isset($context["replace_form"])) { $_replace_form_ = $context["replace_form"]; } else { $_replace_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_replace_form_, "from"), 'row');
        echo "

    ";
        // line 76
        if (isset($context["replace_form"])) { $_replace_form_ = $context["replace_form"]; } else { $_replace_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_replace_form_, "to"), 'row');
        echo "

    ";
        // line 78
        if (isset($context["replace_form"])) { $_replace_form_ = $context["replace_form"]; } else { $_replace_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_replace_form_, "do"), 'row', array("attr" => array("class" => "btn-danger")));
        echo "

    ";
        // line 80
        if (isset($context["replace_form"])) { $_replace_form_ = $context["replace_form"]; } else { $_replace_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($_replace_form_, 'rest');
        echo "

</fieldset>

";
        // line 84
        if (isset($context["replace_form"])) { $_replace_form_ = $context["replace_form"]; } else { $_replace_form_ = null; }
        echo         $this->env->getExtension('form')->renderer->renderBlock($_replace_form_, 'form_end');
        echo "

";
    }

    public function getTemplateName()
    {
        return "LaplaceUserBundle:System:manage-sites.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  245 => 84,  237 => 80,  225 => 76,  209 => 68,  194 => 61,  150 => 41,  95 => 14,  128 => 37,  214 => 96,  191 => 87,  185 => 85,  42 => 15,  293 => 127,  286 => 125,  277 => 123,  267 => 119,  255 => 111,  238 => 102,  229 => 97,  220 => 100,  212 => 88,  188 => 59,  161 => 57,  144 => 44,  135 => 32,  126 => 32,  172 => 51,  165 => 47,  151 => 65,  116 => 50,  113 => 49,  153 => 45,  145 => 61,  139 => 60,  127 => 33,  112 => 21,  87 => 13,  94 => 14,  91 => 13,  23 => 3,  170 => 62,  125 => 31,  103 => 19,  82 => 9,  549 => 162,  543 => 161,  538 => 158,  530 => 155,  526 => 153,  522 => 151,  512 => 149,  505 => 148,  502 => 147,  497 => 146,  491 => 144,  488 => 143,  483 => 142,  473 => 134,  469 => 132,  466 => 131,  460 => 130,  455 => 129,  450 => 126,  444 => 122,  441 => 121,  437 => 120,  434 => 119,  429 => 116,  423 => 112,  420 => 111,  416 => 110,  413 => 109,  408 => 106,  394 => 105,  390 => 103,  375 => 101,  365 => 99,  362 => 98,  359 => 97,  355 => 95,  348 => 91,  344 => 90,  330 => 89,  327 => 88,  321 => 86,  307 => 85,  302 => 84,  295 => 81,  287 => 80,  279 => 78,  271 => 77,  264 => 118,  256 => 73,  251 => 71,  247 => 107,  239 => 69,  231 => 78,  219 => 74,  201 => 66,  147 => 51,  143 => 49,  134 => 36,  131 => 34,  122 => 35,  102 => 24,  92 => 25,  84 => 32,  72 => 27,  48 => 7,  35 => 5,  29 => 3,  76 => 7,  69 => 5,  54 => 14,  51 => 8,  31 => 7,  205 => 85,  199 => 91,  190 => 66,  182 => 57,  179 => 67,  175 => 58,  168 => 57,  164 => 47,  156 => 43,  148 => 47,  138 => 44,  123 => 30,  117 => 28,  108 => 27,  83 => 24,  71 => 10,  64 => 16,  110 => 20,  89 => 12,  65 => 14,  63 => 23,  58 => 10,  34 => 5,  227 => 92,  224 => 91,  221 => 90,  207 => 82,  197 => 74,  195 => 65,  192 => 72,  189 => 63,  186 => 60,  181 => 67,  178 => 83,  173 => 82,  162 => 58,  158 => 69,  155 => 55,  152 => 42,  142 => 47,  136 => 41,  133 => 35,  130 => 28,  120 => 25,  105 => 25,  100 => 18,  75 => 20,  53 => 19,  39 => 7,  98 => 15,  80 => 8,  78 => 28,  46 => 16,  44 => 9,  36 => 6,  32 => 8,  60 => 21,  57 => 9,  40 => 12,  114 => 25,  109 => 48,  106 => 19,  101 => 20,  88 => 12,  85 => 10,  77 => 12,  67 => 9,  47 => 6,  28 => 4,  25 => 5,  55 => 19,  43 => 7,  38 => 6,  26 => 8,  24 => 3,  50 => 7,  27 => 2,  22 => 2,  19 => 1,  232 => 82,  226 => 78,  222 => 76,  215 => 89,  211 => 84,  208 => 95,  202 => 65,  196 => 64,  193 => 65,  187 => 62,  183 => 60,  180 => 59,  171 => 54,  166 => 51,  163 => 50,  160 => 49,  157 => 43,  149 => 42,  146 => 40,  140 => 35,  137 => 37,  129 => 36,  124 => 35,  121 => 24,  118 => 29,  115 => 39,  111 => 26,  107 => 28,  104 => 47,  97 => 16,  93 => 16,  90 => 13,  81 => 12,  70 => 6,  66 => 13,  62 => 11,  59 => 15,  56 => 12,  52 => 10,  49 => 17,  45 => 6,  41 => 9,  37 => 14,  33 => 4,  30 => 3,);
    }
}
