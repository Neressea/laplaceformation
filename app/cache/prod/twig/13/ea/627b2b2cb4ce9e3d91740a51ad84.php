<?php

/* LaplaceCommonBundle:Form:fields.html.twig */
class __TwigTemplate_13ea627b2b2cb4ce9e3d91740a51ad84 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'form_widget_simple' => array($this, 'block_form_widget_simple'),
            'textarea_widget' => array($this, 'block_textarea_widget'),
            'choice_widget_expanded' => array($this, 'block_choice_widget_expanded'),
            'radio_label' => array($this, 'block_radio_label'),
            'choice_widget_collapsed' => array($this, 'block_choice_widget_collapsed'),
            'choice_widget_options' => array($this, 'block_choice_widget_options'),
            'button_widget' => array($this, 'block_button_widget'),
            'submit_widget' => array($this, 'block_submit_widget'),
            'date_widget' => array($this, 'block_date_widget'),
            'time_widget' => array($this, 'block_time_widget'),
            'user_selector_widget' => array($this, 'block_user_selector_widget'),
            'form_row' => array($this, 'block_form_row'),
            'form_label' => array($this, 'block_form_label'),
            'checkbox_row' => array($this, 'block_checkbox_row'),
            'button_row' => array($this, 'block_button_row'),
            'submit_row' => array($this, 'block_submit_row'),
            'datetime_row' => array($this, 'block_datetime_row'),
            'form_start' => array($this, 'block_form_start'),
            'form_errors' => array($this, 'block_form_errors'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
";
        // line 3
        echo "

";
        // line 6
        $this->displayBlock('form_widget_simple', $context, $blocks);
        // line 11
        echo "
";
        // line 13
        $this->displayBlock('textarea_widget', $context, $blocks);
        // line 17
        echo "
";
        // line 19
        $this->displayBlock('choice_widget_expanded', $context, $blocks);
        // line 30
        echo "
";
        // line 32
        $this->displayBlock('radio_label', $context, $blocks);
        // line 51
        echo "
";
        // line 53
        $this->displayBlock('choice_widget_collapsed', $context, $blocks);
        // line 73
        echo "
";
        // line 75
        echo "
";
        // line 76
        $this->displayBlock('choice_widget_options', $context, $blocks);
        // line 90
        echo "
";
        // line 92
        echo "

";
        // line 95
        $this->displayBlock('button_widget', $context, $blocks);
        // line 104
        echo "
";
        // line 106
        $this->displayBlock('submit_widget', $context, $blocks);
        // line 113
        echo "
";
        // line 185
        echo "
";
        // line 186
        $this->displayBlock('date_widget', $context, $blocks);
        // line 206
        echo "
";
        // line 207
        $this->displayBlock('time_widget', $context, $blocks);
        // line 222
        echo "
";
        // line 223
        $this->displayBlock('user_selector_widget', $context, $blocks);
        // line 227
        echo "


";
        // line 230
        $this->displayBlock('form_row', $context, $blocks);
        // line 242
        echo "
";
        // line 243
        $this->displayBlock('form_label', $context, $blocks);
        // line 266
        echo "
";
        // line 267
        $this->displayBlock('checkbox_row', $context, $blocks);
        // line 288
        echo "
";
        // line 290
        $this->displayBlock('button_row', $context, $blocks);
        // line 297
        echo "
";
        // line 299
        $this->displayBlock('submit_row', $context, $blocks);
        // line 304
        echo "

";
        // line 306
        $this->displayBlock('datetime_row', $context, $blocks);
        // line 319
        echo "

";
        // line 322
        echo "
";
        // line 323
        $this->displayBlock('form_start', $context, $blocks);
        // line 340
        echo "
";
        // line 341
        $this->displayBlock('form_errors', $context, $blocks);
    }

    // line 6
    public function block_form_widget_simple($context, array $blocks = array())
    {
        // line 7
        echo "    ";
        if (isset($context["type"])) { $_type_ = $context["type"]; } else { $_type_ = null; }
        $context["type"] = ((array_key_exists("type", $context)) ? (_twig_default_filter($_type_, "text")) : ("text"));
        // line 8
        echo "    ";
        if (isset($context["attr"])) { $_attr_ = $context["attr"]; } else { $_attr_ = null; }
        $context["attr"] = twig_array_merge($_attr_, array("class" => trim(((($this->getAttribute($_attr_, "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($_attr_, "class"), "")) : ("")) . " input-block-level"))));
        // line 9
        echo "    <input type=\"";
        if (isset($context["type"])) { $_type_ = $context["type"]; } else { $_type_ = null; }
        echo twig_escape_filter($this->env, $_type_, "html", null, true);
        echo "\" ";
        $this->displayBlock("widget_attributes", $context, $blocks);
        echo " ";
        if (isset($context["value"])) { $_value_ = $context["value"]; } else { $_value_ = null; }
        if ((!twig_test_empty($_value_))) {
            echo "value=\"";
            if (isset($context["value"])) { $_value_ = $context["value"]; } else { $_value_ = null; }
            echo twig_escape_filter($this->env, $_value_, "html", null, true);
            echo "\" ";
        }
        echo " />
";
    }

    // line 13
    public function block_textarea_widget($context, array $blocks = array())
    {
        // line 14
        echo "    ";
        if (isset($context["attr"])) { $_attr_ = $context["attr"]; } else { $_attr_ = null; }
        $context["attr"] = twig_array_merge($_attr_, array("class" => trim(((($this->getAttribute($_attr_, "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($_attr_, "class"), "")) : ("")) . " input-block-level")), "rows" => (($this->getAttribute($_attr_, "rows", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($_attr_, "rows"), 8)) : (8))));
        // line 15
        echo "    <textarea ";
        $this->displayBlock("widget_attributes", $context, $blocks);
        echo ">";
        if (isset($context["value"])) { $_value_ = $context["value"]; } else { $_value_ = null; }
        echo twig_escape_filter($this->env, $_value_, "html", null, true);
        echo "</textarea>
";
    }

    // line 19
    public function block_choice_widget_expanded($context, array $blocks = array())
    {
        // line 20
        echo "
    ";
        // line 21
        if (isset($context["multiple"])) { $_multiple_ = $context["multiple"]; } else { $_multiple_ = null; }
        if ($_multiple_) {
            // line 22
            echo "
    ";
        } else {
            // line 24
            echo "        ";
            if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($_form_);
            foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                // line 25
                echo "            ";
                if (isset($context["child"])) { $_child_ = $context["child"]; } else { $_child_ = null; }
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($_child_, 'label');
                echo "
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 27
            echo "    ";
        }
        // line 28
        echo "
";
    }

    // line 32
    public function block_radio_label($context, array $blocks = array())
    {
        // line 33
        echo "
    ";
        // line 34
        if (isset($context["compound"])) { $_compound_ = $context["compound"]; } else { $_compound_ = null; }
        if ((!$_compound_)) {
            // line 35
            echo "        ";
            if (isset($context["label_attr"])) { $_label_attr_ = $context["label_attr"]; } else { $_label_attr_ = null; }
            if (isset($context["id"])) { $_id_ = $context["id"]; } else { $_id_ = null; }
            $context["label_attr"] = twig_array_merge($_label_attr_, array("for" => $_id_));
            // line 36
            echo "    ";
        }
        // line 37
        echo "
    ";
        // line 38
        if (isset($context["label_attr"])) { $_label_attr_ = $context["label_attr"]; } else { $_label_attr_ = null; }
        $context["label_attr"] = twig_array_merge($_label_attr_, array("class" => trim(((($this->getAttribute($_label_attr_, "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($_label_attr_, "class"), "")) : ("")) . " radio"))));
        // line 39
        echo "    ";
        if (isset($context["attr"])) { $_attr_ = $context["attr"]; } else { $_attr_ = null; }
        if (($this->getAttribute($_attr_, "inline", array(), "any", true, true) && $this->getAttribute($_attr_, "inline"))) {
            // line 40
            echo "        ";
            if (isset($context["label_attr"])) { $_label_attr_ = $context["label_attr"]; } else { $_label_attr_ = null; }
            $context["label_attr"] = twig_array_merge($_label_attr_, array("class" => trim(((($this->getAttribute($_label_attr_, "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($_label_attr_, "class"), "")) : ("")) . " inline"))));
            // line 41
            echo "    ";
        }
        // line 42
        echo "
    ";
        // line 43
        if (isset($context["label"])) { $_label_ = $context["label"]; } else { $_label_ = null; }
        if (twig_test_empty($_label_)) {
            // line 44
            echo "        ";
            if (isset($context["name"])) { $_name_ = $context["name"]; } else { $_name_ = null; }
            $context["label"] = $this->env->getExtension('form')->renderer->humanize($_name_);
            // line 45
            echo "    ";
        }
        // line 46
        echo "
    <label";
        // line 47
        if (isset($context["label_attr"])) { $_label_attr_ = $context["label_attr"]; } else { $_label_attr_ = null; }
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($_label_attr_);
        foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
            echo " ";
            if (isset($context["attrname"])) { $_attrname_ = $context["attrname"]; } else { $_attrname_ = null; }
            echo twig_escape_filter($this->env, $_attrname_, "html", null, true);
            echo "=\"";
            if (isset($context["attrvalue"])) { $_attrvalue_ = $context["attrvalue"]; } else { $_attrvalue_ = null; }
            echo twig_escape_filter($this->env, $_attrvalue_, "html", null, true);
            echo "\"";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo ">
        ";
        // line 48
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($_form_, 'widget');
        echo " ";
        if (isset($context["label"])) { $_label_ = $context["label"]; } else { $_label_ = null; }
        if (isset($context["translation_domain"])) { $_translation_domain_ = $context["translation_domain"]; } else { $_translation_domain_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($_label_, array(), $_translation_domain_), "html", null, true);
        echo "
    </label>
";
    }

    // line 53
    public function block_choice_widget_collapsed($context, array $blocks = array())
    {
        // line 54
        echo "
    ";
        // line 55
        if (isset($context["attr"])) { $_attr_ = $context["attr"]; } else { $_attr_ = null; }
        $context["attr"] = twig_array_merge($_attr_, array("class" => trim(((($this->getAttribute($_attr_, "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($_attr_, "class"), "")) : ("")) . " input-block-level"))));
        // line 56
        echo "
    <select ";
        // line 57
        $this->displayBlock("widget_attributes", $context, $blocks);
        if (isset($context["multiple"])) { $_multiple_ = $context["multiple"]; } else { $_multiple_ = null; }
        if ($_multiple_) {
            echo " multiple=\"multiple\"";
        }
        echo ">
        ";
        // line 58
        if (isset($context["empty_value"])) { $_empty_value_ = $context["empty_value"]; } else { $_empty_value_ = null; }
        if ((!(null === $_empty_value_))) {
            // line 59
            echo "            <option ";
            if (isset($context["required"])) { $_required_ = $context["required"]; } else { $_required_ = null; }
            if ($_required_) {
                echo " disabled=\"disabled\"";
                if (isset($context["value"])) { $_value_ = $context["value"]; } else { $_value_ = null; }
                if (twig_test_empty($_value_)) {
                    echo " selected=\"selected\"";
                }
            } else {
                echo " value=\"\"";
            }
            echo ">";
            if (isset($context["empty_value"])) { $_empty_value_ = $context["empty_value"]; } else { $_empty_value_ = null; }
            if (isset($context["translation_domain"])) { $_translation_domain_ = $context["translation_domain"]; } else { $_translation_domain_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($_empty_value_, array(), $_translation_domain_), "html", null, true);
            echo "</option>
        ";
        }
        // line 61
        echo "        ";
        if (isset($context["preferred_choices"])) { $_preferred_choices_ = $context["preferred_choices"]; } else { $_preferred_choices_ = null; }
        if ((twig_length_filter($this->env, $_preferred_choices_) > 0)) {
            // line 62
            echo "            ";
            if (isset($context["preferred_choices"])) { $_preferred_choices_ = $context["preferred_choices"]; } else { $_preferred_choices_ = null; }
            $context["options"] = $_preferred_choices_;
            // line 63
            echo "            ";
            $this->displayBlock("choice_widget_options", $context, $blocks);
            echo "
            ";
            // line 64
            if (isset($context["choices"])) { $_choices_ = $context["choices"]; } else { $_choices_ = null; }
            if (isset($context["separator"])) { $_separator_ = $context["separator"]; } else { $_separator_ = null; }
            if (((twig_length_filter($this->env, $_choices_) > 0) && (!(null === $_separator_)))) {
                // line 65
                echo "                <option disabled=\"disabled\">";
                if (isset($context["separator"])) { $_separator_ = $context["separator"]; } else { $_separator_ = null; }
                echo twig_escape_filter($this->env, $_separator_, "html", null, true);
                echo "</option>
            ";
            }
            // line 67
            echo "        ";
        }
        // line 68
        echo "        ";
        if (isset($context["choices"])) { $_choices_ = $context["choices"]; } else { $_choices_ = null; }
        $context["options"] = $_choices_;
        // line 69
        echo "        ";
        $this->displayBlock("choice_widget_options", $context, $blocks);
        echo "
    </select>

";
    }

    // line 76
    public function block_choice_widget_options($context, array $blocks = array())
    {
        // line 77
        ob_start();
        // line 78
        echo "    ";
        if (isset($context["options"])) { $_options_ = $context["options"]; } else { $_options_ = null; }
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($_options_);
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["group_label"] => $context["choice"]) {
            // line 79
            echo "        ";
            if (isset($context["choice"])) { $_choice_ = $context["choice"]; } else { $_choice_ = null; }
            if (twig_test_iterable($_choice_)) {
                // line 80
                echo "            <optgroup label=\"";
                if (isset($context["group_label"])) { $_group_label_ = $context["group_label"]; } else { $_group_label_ = null; }
                if (isset($context["translation_domain"])) { $_translation_domain_ = $context["translation_domain"]; } else { $_translation_domain_ = null; }
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($_group_label_, array(), $_translation_domain_), "html", null, true);
                echo "\" value=\"0\">
                ";
                // line 81
                if (isset($context["choice"])) { $_choice_ = $context["choice"]; } else { $_choice_ = null; }
                $context["options"] = $_choice_;
                // line 82
                echo "                ";
                $this->displayBlock("choice_widget_options", $context, $blocks);
                echo "
            </optgroup>
        ";
            } else {
                // line 85
                echo "            <option value=\"";
                if (isset($context["choice"])) { $_choice_ = $context["choice"]; } else { $_choice_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($_choice_, "value"), "html", null, true);
                echo "\"";
                if (isset($context["choice"])) { $_choice_ = $context["choice"]; } else { $_choice_ = null; }
                if (isset($context["value"])) { $_value_ = $context["value"]; } else { $_value_ = null; }
                if ($this->env->getExtension('form')->isSelectedChoice($_choice_, $_value_)) {
                    echo " selected=\"selected\"";
                }
                echo ">";
                if (isset($context["choice"])) { $_choice_ = $context["choice"]; } else { $_choice_ = null; }
                if (isset($context["translation_domain"])) { $_translation_domain_ = $context["translation_domain"]; } else { $_translation_domain_ = null; }
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute($_choice_, "label"), array(), $_translation_domain_), "html", null, true);
                echo "</option>
        ";
            }
            // line 87
            echo "    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['group_label'], $context['choice'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 95
    public function block_button_widget($context, array $blocks = array())
    {
        // line 96
        echo "
    ";
        // line 97
        if (isset($context["label"])) { $_label_ = $context["label"]; } else { $_label_ = null; }
        if (twig_test_empty($_label_)) {
            // line 98
            echo "        ";
            if (isset($context["name"])) { $_name_ = $context["name"]; } else { $_name_ = null; }
            $context["label"] = $this->env->getExtension('form')->renderer->humanize($_name_);
            // line 99
            echo "    ";
        }
        // line 100
        echo "    ";
        if (isset($context["attr"])) { $_attr_ = $context["attr"]; } else { $_attr_ = null; }
        $context["attr"] = twig_array_merge($_attr_, array("class" => trim(((($this->getAttribute($_attr_, "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($_attr_, "class"), "")) : ("")) . " btn"))));
        // line 101
        echo "    <button type=\"";
        if (isset($context["type"])) { $_type_ = $context["type"]; } else { $_type_ = null; }
        echo twig_escape_filter($this->env, ((array_key_exists("type", $context)) ? (_twig_default_filter($_type_, "button")) : ("button")), "html", null, true);
        echo "\" ";
        $this->displayBlock("button_attributes", $context, $blocks);
        echo ">";
        if (isset($context["label"])) { $_label_ = $context["label"]; } else { $_label_ = null; }
        if (isset($context["translation_domain"])) { $_translation_domain_ = $context["translation_domain"]; } else { $_translation_domain_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($_label_, array(), $_translation_domain_), "html", null, true);
        echo "</button>

";
    }

    // line 106
    public function block_submit_widget($context, array $blocks = array())
    {
        // line 107
        echo "
    ";
        // line 108
        if (isset($context["type"])) { $_type_ = $context["type"]; } else { $_type_ = null; }
        $context["type"] = ((array_key_exists("type", $context)) ? (_twig_default_filter($_type_, "submit")) : ("submit"));
        // line 109
        echo "    ";
        if (isset($context["attr"])) { $_attr_ = $context["attr"]; } else { $_attr_ = null; }
        $context["attr"] = twig_array_merge($_attr_, array("class" => trim(((($this->getAttribute($_attr_, "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($_attr_, "class"), "")) : ("")) . " btn-primary"))));
        // line 110
        echo "    ";
        $this->displayBlock("button_widget", $context, $blocks);
        echo "

";
    }

    // line 186
    public function block_date_widget($context, array $blocks = array())
    {
        // line 187
        echo "
    ";
        // line 188
        if (isset($context["widget"])) { $_widget_ = $context["widget"]; } else { $_widget_ = null; }
        if (($_widget_ == "single_text")) {
            // line 189
            echo "\t\t";
            $context["type"] = "text";
            // line 190
            echo "        ";
            if (isset($context["attr"])) { $_attr_ = $context["attr"]; } else { $_attr_ = null; }
            $context["attr"] = twig_array_merge($_attr_, array("placeholder" => (($this->getAttribute($_attr_, "placeholder", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($_attr_, "placeholder"), "Date au format jj/mm/aa")) : ("Date au format jj/mm/aa"))));
            // line 191
            echo "\t\t";
            if (isset($context["attr"])) { $_attr_ = $context["attr"]; } else { $_attr_ = null; }
            $context["attr"] = twig_array_merge($_attr_, array("title" => (($this->getAttribute($_attr_, "title", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($_attr_, "title"), "Date au format jj/mm/aa")) : ("Date au format jj/mm/aa"))));
            // line 192
            echo "        ";
            $this->displayBlock("form_widget_simple", $context, $blocks);
            echo "
    ";
        } else {
            // line 194
            echo "
        <div ";
            // line 195
            $this->displayBlock("widget_container_attributes", $context, $blocks);
            echo ">
            ";
            // line 196
            if (isset($context["date_pattern"])) { $_date_pattern_ = $context["date_pattern"]; } else { $_date_pattern_ = null; }
            if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
            echo strtr($_date_pattern_, array("{{ year }}" =>             // line 197
$this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_form_, "year"), 'widget'), "{{ month }}" =>             // line 198
$this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_form_, "month"), 'widget'), "{{ day }}" =>             // line 199
$this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_form_, "day"), 'widget')));
            // line 200
            echo "
        </div>

    ";
        }
        // line 204
        echo "
";
    }

    // line 207
    public function block_time_widget($context, array $blocks = array())
    {
        // line 208
        ob_start();
        // line 209
        echo "    ";
        if (isset($context["widget"])) { $_widget_ = $context["widget"]; } else { $_widget_ = null; }
        if (($_widget_ == "single_text")) {
            // line 210
            echo "\t\t";
            $context["type"] = "text";
            // line 211
            echo "        ";
            if (isset($context["attr"])) { $_attr_ = $context["attr"]; } else { $_attr_ = null; }
            $context["attr"] = twig_array_merge($_attr_, array("placeholder" => (($this->getAttribute($_attr_, "placeholder", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($_attr_, "placeholder"), "Heure au format hh:mm")) : ("Heure au format hh:mm"))));
            // line 212
            echo "\t\t";
            if (isset($context["attr"])) { $_attr_ = $context["attr"]; } else { $_attr_ = null; }
            $context["attr"] = twig_array_merge($_attr_, array("title" => (($this->getAttribute($_attr_, "title", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($_attr_, "title"), "Heure au format hh:mm")) : ("Heure au format hh:mm"))));
            // line 213
            echo "        ";
            $this->displayBlock("form_widget_simple", $context, $blocks);
            echo "
    ";
        } else {
            // line 215
            echo "        ";
            if (isset($context["widget"])) { $_widget_ = $context["widget"]; } else { $_widget_ = null; }
            $context["vars"] = ((($_widget_ == "text")) ? (array("attr" => array("size" => 1))) : (array()));
            // line 216
            echo "        <div ";
            $this->displayBlock("widget_container_attributes", $context, $blocks);
            echo ">
            ";
            // line 217
            if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
            if (isset($context["vars"])) { $_vars_ = $context["vars"]; } else { $_vars_ = null; }
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_form_, "hour"), 'widget', $_vars_);
            if (isset($context["with_minutes"])) { $_with_minutes_ = $context["with_minutes"]; } else { $_with_minutes_ = null; }
            if ($_with_minutes_) {
                echo ":";
                if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
                if (isset($context["vars"])) { $_vars_ = $context["vars"]; } else { $_vars_ = null; }
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_form_, "minute"), 'widget', $_vars_);
            }
            if (isset($context["with_seconds"])) { $_with_seconds_ = $context["with_seconds"]; } else { $_with_seconds_ = null; }
            if ($_with_seconds_) {
                echo ":";
                if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
                if (isset($context["vars"])) { $_vars_ = $context["vars"]; } else { $_vars_ = null; }
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_form_, "second"), 'widget', $_vars_);
            }
            // line 218
            echo "        </div>
    ";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 223
    public function block_user_selector_widget($context, array $blocks = array())
    {
        // line 224
        echo "    ";
        if (isset($context["attr"])) { $_attr_ = $context["attr"]; } else { $_attr_ = null; }
        $context["attr"] = twig_array_merge($_attr_, array("class" => trim(((($this->getAttribute($_attr_, "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($_attr_, "class"), "")) : ("")) . " username-typeahead")), "autocomplete" => "off"));
        // line 225
        echo "    ";
        $this->displayBlock("form_widget_simple", $context, $blocks);
        echo "
";
    }

    // line 230
    public function block_form_row($context, array $blocks = array())
    {
        // line 231
        echo "
<div class=\"control-group\">
    ";
        // line 233
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($_form_, 'label');
        echo "

    <div class=\"controls\">
    ";
        // line 236
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($_form_, 'widget');
        echo "
    ";
        // line 237
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($_form_, 'errors');
        echo "
    </div>
</div>

";
    }

    // line 243
    public function block_form_label($context, array $blocks = array())
    {
        // line 244
        echo "
";
        // line 245
        if (isset($context["label"])) { $_label_ = $context["label"]; } else { $_label_ = null; }
        if ((!($_label_ === false))) {
            // line 246
            echo "    ";
            if (isset($context["compound"])) { $_compound_ = $context["compound"]; } else { $_compound_ = null; }
            if ((!$_compound_)) {
                // line 247
                echo "        ";
                if (isset($context["label_attr"])) { $_label_attr_ = $context["label_attr"]; } else { $_label_attr_ = null; }
                if (isset($context["id"])) { $_id_ = $context["id"]; } else { $_id_ = null; }
                $context["label_attr"] = twig_array_merge($_label_attr_, array("for" => $_id_));
                // line 248
                echo "    ";
            }
            // line 249
            echo "
    ";
            // line 250
            if (isset($context["label"])) { $_label_ = $context["label"]; } else { $_label_ = null; }
            if (twig_test_empty($_label_)) {
                // line 251
                echo "        ";
                if (isset($context["name"])) { $_name_ = $context["name"]; } else { $_name_ = null; }
                $context["label"] = $this->env->getExtension('form')->renderer->humanize($_name_);
                // line 252
                echo "    ";
            }
            // line 253
            echo "
    ";
            // line 255
            echo "    ";
            if (isset($context["label_attr"])) { $_label_attr_ = $context["label_attr"]; } else { $_label_attr_ = null; }
            $context["label_attr"] = twig_array_merge($_label_attr_, array("class" => trim(((($this->getAttribute($_label_attr_, "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($_label_attr_, "class"), "")) : ("")) . " control-label"))));
            // line 256
            echo "
    <label";
            // line 257
            if (isset($context["label_attr"])) { $_label_attr_ = $context["label_attr"]; } else { $_label_attr_ = null; }
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($_label_attr_);
            foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
                echo " ";
                if (isset($context["attrname"])) { $_attrname_ = $context["attrname"]; } else { $_attrname_ = null; }
                echo twig_escape_filter($this->env, $_attrname_, "html", null, true);
                echo "=\"";
                if (isset($context["attrvalue"])) { $_attrvalue_ = $context["attrvalue"]; } else { $_attrvalue_ = null; }
                echo twig_escape_filter($this->env, $_attrvalue_, "html", null, true);
                echo "\"";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo ">
        ";
            // line 258
            if (isset($context["required"])) { $_required_ = $context["required"]; } else { $_required_ = null; }
            if ($_required_) {
                // line 259
                echo "        <i class=\"icon-asterisk\" title=\"Ce champ est obligatoire\"></i>
        ";
            }
            // line 261
            echo "        ";
            if (isset($context["label"])) { $_label_ = $context["label"]; } else { $_label_ = null; }
            if (isset($context["translation_domain"])) { $_translation_domain_ = $context["translation_domain"]; } else { $_translation_domain_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($_label_, array(), $_translation_domain_), "html", null, true);
            echo "
    </label>
";
        }
        // line 264
        echo "
";
    }

    // line 267
    public function block_checkbox_row($context, array $blocks = array())
    {
        // line 268
        echo "
<div class=\"control-group\">
    <div class=\"controls\">

        ";
        // line 272
        if (isset($context["label"])) { $_label_ = $context["label"]; } else { $_label_ = null; }
        if (twig_test_empty($_label_)) {
            // line 273
            echo "            ";
            if (isset($context["name"])) { $_name_ = $context["name"]; } else { $_name_ = null; }
            $context["label"] = $this->env->getExtension('form')->renderer->humanize($_name_);
            // line 274
            echo "        ";
        }
        // line 275
        echo "
        ";
        // line 277
        echo "        ";
        if (isset($context["label_attr"])) { $_label_attr_ = $context["label_attr"]; } else { $_label_attr_ = null; }
        if (isset($context["id"])) { $_id_ = $context["id"]; } else { $_id_ = null; }
        $context["label_attr"] = twig_array_merge($_label_attr_, array("for" => $_id_, "class" => trim(((($this->getAttribute($_label_attr_, "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($_label_attr_, "class"), "")) : ("")) . " checkbox"))));
        // line 278
        echo "        <label";
        if (isset($context["label_attr"])) { $_label_attr_ = $context["label_attr"]; } else { $_label_attr_ = null; }
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($_label_attr_);
        foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
            echo " ";
            if (isset($context["attrname"])) { $_attrname_ = $context["attrname"]; } else { $_attrname_ = null; }
            echo twig_escape_filter($this->env, $_attrname_, "html", null, true);
            echo "=\"";
            if (isset($context["attrvalue"])) { $_attrvalue_ = $context["attrvalue"]; } else { $_attrvalue_ = null; }
            echo twig_escape_filter($this->env, $_attrvalue_, "html", null, true);
            echo "\"";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo ">
            ";
        // line 279
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($_form_, 'widget');
        echo "
            ";
        // line 280
        if (isset($context["label"])) { $_label_ = $context["label"]; } else { $_label_ = null; }
        if (isset($context["translation_domain"])) { $_translation_domain_ = $context["translation_domain"]; } else { $_translation_domain_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($_label_, array(), $_translation_domain_), "html", null, true);
        echo "
        </label>
        ";
        // line 282
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($_form_, 'errors');
        echo "

    </div>
</div>

";
    }

    // line 290
    public function block_button_row($context, array $blocks = array())
    {
        // line 291
        echo "
<div class=\"form-actions\">
    ";
        // line 293
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($_form_, 'widget');
        echo "
</div>

";
    }

    // line 299
    public function block_submit_row($context, array $blocks = array())
    {
        // line 300
        echo "
";
        // line 301
        $this->displayBlock("button_row", $context, $blocks);
        echo "

";
    }

    // line 306
    public function block_datetime_row($context, array $blocks = array())
    {
        // line 307
        echo "
<div class=\"control-group\">
    ";
        // line 309
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($_form_, 'label');
        echo "

    <div class=\"controls\">
    ";
        // line 312
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_form_, "date"), 'widget');
        echo "
    ";
        // line 313
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_form_, "time"), 'widget');
        echo "
    ";
        // line 314
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($_form_, 'errors');
        echo "
    </div>
</div>

";
    }

    // line 323
    public function block_form_start($context, array $blocks = array())
    {
        // line 324
        echo "
    ";
        // line 325
        if (isset($context["method"])) { $_method_ = $context["method"]; } else { $_method_ = null; }
        $context["method"] = twig_upper_filter($this->env, $_method_);
        // line 326
        echo "    ";
        if (isset($context["method"])) { $_method_ = $context["method"]; } else { $_method_ = null; }
        if (twig_in_filter($_method_, array(0 => "GET", 1 => "POST"))) {
            // line 327
            echo "        ";
            if (isset($context["method"])) { $_method_ = $context["method"]; } else { $_method_ = null; }
            $context["form_method"] = $_method_;
            // line 328
            echo "    ";
        } else {
            // line 329
            echo "        ";
            $context["form_method"] = "POST";
            // line 330
            echo "    ";
        }
        // line 331
        echo "
    ";
        // line 333
        echo "    ";
        if (isset($context["attr"])) { $_attr_ = $context["attr"]; } else { $_attr_ = null; }
        $context["attr"] = twig_array_merge($_attr_, array("class" => trim(((($this->getAttribute($_attr_, "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($_attr_, "class"), "")) : ("")) . " form-horizontal"))));
        // line 334
        echo "    <form method=\"";
        if (isset($context["form_method"])) { $_form_method_ = $context["form_method"]; } else { $_form_method_ = null; }
        echo twig_escape_filter($this->env, twig_lower_filter($this->env, $_form_method_), "html", null, true);
        echo "\" action=\"";
        if (isset($context["action"])) { $_action_ = $context["action"]; } else { $_action_ = null; }
        echo twig_escape_filter($this->env, $_action_, "html", null, true);
        echo "\"";
        if (isset($context["attr"])) { $_attr_ = $context["attr"]; } else { $_attr_ = null; }
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($_attr_);
        foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
            echo " ";
            if (isset($context["attrname"])) { $_attrname_ = $context["attrname"]; } else { $_attrname_ = null; }
            echo twig_escape_filter($this->env, $_attrname_, "html", null, true);
            echo "=\"";
            if (isset($context["attrvalue"])) { $_attrvalue_ = $context["attrvalue"]; } else { $_attrvalue_ = null; }
            echo twig_escape_filter($this->env, $_attrvalue_, "html", null, true);
            echo "\"";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        if (isset($context["multipart"])) { $_multipart_ = $context["multipart"]; } else { $_multipart_ = null; }
        if ($_multipart_) {
            echo " enctype=\"multipart/form-data\"";
        }
        echo ">
    ";
        // line 335
        if (isset($context["form_method"])) { $_form_method_ = $context["form_method"]; } else { $_form_method_ = null; }
        if (isset($context["method"])) { $_method_ = $context["method"]; } else { $_method_ = null; }
        if (($_form_method_ != $_method_)) {
            // line 336
            echo "        <input type=\"hidden\" name=\"_method\" value=\"";
            if (isset($context["method"])) { $_method_ = $context["method"]; } else { $_method_ = null; }
            echo twig_escape_filter($this->env, $_method_, "html", null, true);
            echo "\" />
    ";
        }
        // line 338
        echo "
";
    }

    // line 341
    public function block_form_errors($context, array $blocks = array())
    {
        // line 342
        echo "    ";
        if (isset($context["errors"])) { $_errors_ = $context["errors"]; } else { $_errors_ = null; }
        if ((twig_length_filter($this->env, $_errors_) > 0)) {
            // line 343
            echo "    <p class=\"text-error\">
    <i class=\"icon-warning-sign\"></i>
    ";
            // line 345
            if (isset($context["errors"])) { $_errors_ = $context["errors"]; } else { $_errors_ = null; }
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($_errors_);
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 346
                echo "    ";
                if (isset($context["error"])) { $_error_ = $context["error"]; } else { $_error_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($_error_, "message"), "html", null, true);
                echo "
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 348
            echo "    </p>
    ";
        }
        // line 350
        echo "
";
    }

    public function getTemplateName()
    {
        return "LaplaceCommonBundle:Form:fields.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  1013 => 350,  1009 => 348,  999 => 346,  994 => 345,  990 => 343,  986 => 342,  983 => 341,  978 => 338,  971 => 336,  967 => 335,  938 => 334,  934 => 333,  931 => 331,  928 => 330,  925 => 329,  922 => 328,  918 => 327,  914 => 326,  911 => 325,  908 => 324,  905 => 323,  895 => 314,  885 => 312,  878 => 309,  874 => 307,  871 => 306,  864 => 301,  861 => 300,  858 => 299,  849 => 293,  845 => 291,  842 => 290,  831 => 282,  824 => 280,  819 => 279,  800 => 278,  792 => 275,  789 => 274,  785 => 273,  782 => 272,  776 => 268,  773 => 267,  768 => 264,  759 => 261,  734 => 257,  731 => 256,  727 => 255,  721 => 252,  717 => 251,  714 => 250,  711 => 249,  708 => 248,  703 => 247,  699 => 246,  696 => 245,  693 => 244,  680 => 237,  675 => 236,  668 => 233,  664 => 231,  661 => 230,  654 => 225,  647 => 223,  622 => 217,  617 => 216,  613 => 215,  607 => 213,  599 => 211,  596 => 210,  592 => 209,  590 => 208,  587 => 207,  582 => 204,  576 => 200,  574 => 199,  573 => 198,  572 => 197,  569 => 196,  565 => 195,  562 => 194,  552 => 191,  545 => 189,  542 => 188,  524 => 109,  521 => 108,  518 => 107,  515 => 106,  500 => 101,  493 => 99,  489 => 98,  486 => 97,  480 => 95,  463 => 87,  436 => 81,  404 => 77,  401 => 76,  392 => 69,  388 => 68,  385 => 67,  369 => 63,  361 => 61,  328 => 56,  325 => 55,  319 => 53,  259 => 38,  242 => 33,  234 => 28,  299 => 97,  217 => 64,  167 => 9,  910 => 400,  904 => 397,  896 => 393,  890 => 313,  884 => 389,  875 => 384,  872 => 383,  870 => 382,  867 => 381,  856 => 372,  853 => 371,  848 => 368,  843 => 364,  840 => 362,  836 => 360,  828 => 354,  822 => 350,  804 => 347,  795 => 277,  777 => 345,  771 => 341,  755 => 259,  752 => 258,  748 => 336,  742 => 335,  724 => 253,  710 => 322,  707 => 321,  704 => 320,  701 => 318,  697 => 316,  690 => 243,  683 => 310,  653 => 284,  650 => 224,  648 => 282,  645 => 281,  640 => 218,  633 => 274,  625 => 270,  611 => 260,  608 => 259,  606 => 258,  603 => 212,  598 => 253,  591 => 250,  583 => 246,  577 => 244,  571 => 242,  556 => 192,  553 => 230,  551 => 229,  548 => 190,  536 => 186,  528 => 110,  514 => 207,  511 => 206,  509 => 205,  506 => 204,  503 => 202,  492 => 195,  487 => 192,  481 => 188,  475 => 184,  461 => 176,  454 => 171,  451 => 170,  446 => 85,  443 => 166,  432 => 162,  428 => 160,  425 => 79,  412 => 150,  409 => 149,  406 => 78,  403 => 147,  398 => 144,  393 => 140,  389 => 137,  371 => 133,  357 => 128,  338 => 122,  333 => 121,  320 => 114,  312 => 111,  284 => 97,  275 => 94,  236 => 81,  233 => 80,  230 => 79,  154 => 45,  378 => 65,  370 => 143,  356 => 137,  346 => 131,  316 => 120,  310 => 119,  290 => 99,  274 => 90,  254 => 87,  248 => 35,  240 => 91,  203 => 70,  132 => 299,  213 => 71,  206 => 71,  119 => 266,  306 => 98,  301 => 100,  278 => 95,  266 => 40,  262 => 39,  200 => 59,  141 => 50,  539 => 187,  533 => 240,  525 => 236,  519 => 234,  513 => 232,  504 => 227,  501 => 226,  499 => 200,  496 => 100,  485 => 215,  482 => 214,  476 => 210,  472 => 208,  467 => 204,  464 => 202,  457 => 199,  442 => 192,  439 => 82,  422 => 158,  414 => 178,  402 => 174,  387 => 163,  384 => 162,  382 => 161,  379 => 160,  374 => 64,  367 => 131,  342 => 59,  339 => 58,  337 => 134,  334 => 109,  329 => 129,  322 => 54,  300 => 112,  297 => 111,  292 => 109,  289 => 47,  283 => 45,  280 => 102,  276 => 43,  269 => 92,  261 => 81,  250 => 89,  241 => 73,  184 => 65,  176 => 53,  73 => 76,  99 => 25,  79 => 14,  257 => 100,  249 => 74,  253 => 36,  235 => 69,  198 => 67,  177 => 62,  174 => 54,  169 => 40,  68 => 25,  61 => 22,  341 => 124,  336 => 101,  331 => 57,  326 => 103,  324 => 101,  317 => 97,  314 => 122,  311 => 99,  305 => 104,  303 => 95,  298 => 104,  291 => 85,  288 => 84,  281 => 96,  273 => 42,  270 => 41,  265 => 102,  263 => 90,  260 => 89,  258 => 80,  252 => 77,  244 => 76,  223 => 67,  218 => 75,  159 => 7,  96 => 23,  86 => 15,  246 => 109,  210 => 69,  245 => 34,  237 => 81,  225 => 77,  209 => 72,  194 => 62,  150 => 49,  95 => 186,  128 => 36,  214 => 52,  191 => 65,  185 => 13,  42 => 10,  293 => 96,  286 => 46,  277 => 107,  267 => 119,  255 => 78,  238 => 81,  229 => 79,  220 => 70,  212 => 63,  188 => 14,  161 => 57,  144 => 322,  135 => 41,  126 => 29,  172 => 51,  165 => 52,  151 => 65,  116 => 29,  113 => 28,  153 => 39,  145 => 43,  139 => 43,  127 => 290,  112 => 230,  87 => 106,  94 => 15,  91 => 20,  23 => 3,  170 => 48,  125 => 35,  103 => 22,  82 => 95,  549 => 162,  543 => 224,  538 => 158,  530 => 155,  526 => 153,  522 => 151,  512 => 149,  505 => 148,  502 => 147,  497 => 146,  491 => 144,  488 => 143,  483 => 96,  473 => 134,  469 => 180,  466 => 179,  460 => 130,  455 => 129,  450 => 196,  444 => 122,  441 => 121,  437 => 190,  434 => 189,  429 => 80,  423 => 112,  420 => 111,  416 => 110,  413 => 109,  408 => 176,  394 => 105,  390 => 103,  375 => 134,  365 => 62,  362 => 139,  359 => 149,  355 => 95,  348 => 127,  344 => 125,  330 => 120,  327 => 119,  321 => 86,  307 => 48,  302 => 106,  295 => 103,  287 => 80,  279 => 44,  271 => 77,  264 => 82,  256 => 37,  251 => 86,  247 => 77,  239 => 32,  231 => 27,  219 => 75,  201 => 60,  147 => 323,  143 => 37,  134 => 304,  131 => 39,  122 => 267,  102 => 222,  92 => 185,  84 => 104,  72 => 10,  48 => 18,  35 => 7,  29 => 5,  76 => 7,  69 => 5,  54 => 14,  51 => 19,  31 => 11,  205 => 20,  199 => 61,  190 => 58,  182 => 60,  179 => 58,  175 => 58,  168 => 53,  164 => 50,  156 => 6,  148 => 47,  138 => 306,  123 => 30,  117 => 243,  108 => 23,  83 => 13,  71 => 10,  64 => 23,  110 => 28,  89 => 113,  65 => 53,  63 => 17,  58 => 15,  34 => 12,  227 => 58,  224 => 77,  221 => 25,  207 => 82,  197 => 74,  195 => 62,  192 => 15,  189 => 58,  186 => 53,  181 => 53,  178 => 55,  173 => 53,  162 => 47,  158 => 48,  155 => 36,  152 => 341,  142 => 44,  136 => 43,  133 => 40,  130 => 33,  120 => 30,  105 => 223,  100 => 207,  75 => 90,  53 => 13,  39 => 15,  98 => 19,  80 => 12,  78 => 92,  46 => 16,  44 => 10,  36 => 6,  32 => 8,  60 => 32,  57 => 30,  40 => 10,  114 => 242,  109 => 26,  106 => 29,  101 => 20,  88 => 19,  85 => 18,  77 => 7,  67 => 73,  47 => 11,  28 => 18,  25 => 7,  55 => 19,  43 => 11,  38 => 1,  26 => 8,  24 => 3,  50 => 13,  27 => 2,  22 => 6,  19 => 2,  232 => 68,  226 => 74,  222 => 80,  215 => 24,  211 => 22,  208 => 21,  202 => 19,  196 => 59,  193 => 65,  187 => 58,  183 => 56,  180 => 55,  171 => 54,  166 => 50,  163 => 8,  160 => 49,  157 => 43,  149 => 340,  146 => 51,  140 => 319,  137 => 42,  129 => 297,  124 => 288,  121 => 33,  118 => 32,  115 => 31,  111 => 22,  107 => 227,  104 => 26,  97 => 206,  93 => 19,  90 => 34,  81 => 12,  70 => 75,  66 => 13,  62 => 51,  59 => 17,  56 => 21,  52 => 17,  49 => 17,  45 => 6,  41 => 3,  37 => 9,  33 => 11,  30 => 10,);
    }
}
