<?php

/* LaplaceTrainingBundle:Need:view.html.twig */
class __TwigTemplate_ad75e41e0767e6d589044c6fec593211 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        if (isset($context["admin"])) { $_admin_ = $context["admin"]; } else { $_admin_ = null; }
        $context["container"] = (($_admin_) ? ("LaplaceCommonBundle::admin-page.html.twig") : ("LaplaceCommonBundle::user-page.html.twig"));
        // line 6
        echo "

";
        // line 8
        $this->env->loadTemplate("LaplaceTrainingBundle:Need:view.html.twig", "1101408982")->display(array_merge($context, array("page" => array(0 => "need", 1 => "view"))));
    }

    public function getTemplateName()
    {
        return "LaplaceTrainingBundle:Need:view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  184 => 54,  176 => 51,  73 => 29,  99 => 22,  79 => 7,  257 => 78,  249 => 74,  253 => 74,  235 => 64,  198 => 46,  177 => 42,  174 => 41,  169 => 40,  68 => 29,  61 => 27,  341 => 118,  336 => 101,  331 => 97,  326 => 102,  324 => 101,  317 => 97,  314 => 96,  311 => 95,  305 => 104,  303 => 95,  298 => 92,  291 => 85,  288 => 84,  281 => 86,  273 => 80,  270 => 79,  265 => 107,  263 => 91,  260 => 90,  258 => 79,  252 => 77,  244 => 77,  223 => 67,  218 => 66,  159 => 46,  96 => 40,  86 => 15,  246 => 109,  210 => 69,  245 => 70,  237 => 72,  225 => 76,  209 => 68,  194 => 61,  150 => 35,  95 => 14,  128 => 39,  214 => 52,  191 => 87,  185 => 85,  42 => 15,  293 => 127,  286 => 125,  277 => 123,  267 => 119,  255 => 78,  238 => 81,  229 => 97,  220 => 75,  212 => 88,  188 => 59,  161 => 57,  144 => 44,  135 => 32,  126 => 29,  172 => 49,  165 => 38,  151 => 65,  116 => 30,  113 => 28,  153 => 44,  145 => 43,  139 => 60,  127 => 25,  112 => 27,  87 => 11,  94 => 118,  91 => 17,  23 => 3,  170 => 62,  125 => 26,  103 => 19,  82 => 9,  549 => 162,  543 => 161,  538 => 158,  530 => 155,  526 => 153,  522 => 151,  512 => 149,  505 => 148,  502 => 147,  497 => 146,  491 => 144,  488 => 143,  483 => 142,  473 => 134,  469 => 132,  466 => 131,  460 => 130,  455 => 129,  450 => 126,  444 => 122,  441 => 121,  437 => 120,  434 => 119,  429 => 116,  423 => 112,  420 => 111,  416 => 110,  413 => 109,  408 => 106,  394 => 105,  390 => 103,  375 => 101,  365 => 99,  362 => 98,  359 => 97,  355 => 95,  348 => 91,  344 => 90,  330 => 89,  327 => 88,  321 => 86,  307 => 85,  302 => 84,  295 => 91,  287 => 80,  279 => 84,  271 => 77,  264 => 118,  256 => 73,  251 => 71,  247 => 107,  239 => 68,  231 => 78,  219 => 74,  201 => 60,  147 => 34,  143 => 49,  134 => 33,  131 => 29,  122 => 24,  102 => 20,  92 => 17,  84 => 114,  72 => 52,  48 => 18,  35 => 5,  29 => 3,  76 => 7,  69 => 5,  54 => 20,  51 => 19,  31 => 11,  205 => 48,  199 => 91,  190 => 58,  182 => 43,  179 => 67,  175 => 58,  168 => 38,  164 => 47,  156 => 34,  148 => 47,  138 => 34,  123 => 30,  117 => 49,  108 => 23,  83 => 14,  71 => 10,  64 => 25,  110 => 28,  89 => 12,  65 => 14,  63 => 17,  58 => 15,  34 => 12,  227 => 58,  224 => 91,  221 => 54,  207 => 82,  197 => 74,  195 => 65,  192 => 72,  189 => 54,  186 => 53,  181 => 44,  178 => 83,  173 => 40,  162 => 47,  158 => 69,  155 => 36,  152 => 30,  142 => 47,  136 => 30,  133 => 40,  130 => 28,  120 => 27,  105 => 24,  100 => 22,  75 => 53,  53 => 13,  39 => 7,  98 => 19,  80 => 8,  78 => 42,  46 => 16,  44 => 10,  36 => 6,  32 => 8,  60 => 21,  57 => 9,  40 => 16,  114 => 25,  109 => 48,  106 => 19,  101 => 20,  88 => 12,  85 => 10,  77 => 7,  67 => 27,  47 => 24,  28 => 18,  25 => 7,  55 => 14,  43 => 17,  38 => 7,  26 => 8,  24 => 3,  50 => 7,  27 => 2,  22 => 6,  19 => 2,  232 => 79,  226 => 77,  222 => 76,  215 => 65,  211 => 84,  208 => 95,  202 => 65,  196 => 59,  193 => 65,  187 => 44,  183 => 60,  180 => 59,  171 => 54,  166 => 51,  163 => 50,  160 => 49,  157 => 43,  149 => 29,  146 => 40,  140 => 32,  137 => 42,  129 => 36,  124 => 37,  121 => 24,  118 => 33,  115 => 32,  111 => 57,  107 => 45,  104 => 19,  97 => 17,  93 => 16,  90 => 12,  81 => 12,  70 => 5,  66 => 13,  62 => 24,  59 => 23,  56 => 21,  52 => 10,  49 => 17,  45 => 6,  41 => 7,  37 => 20,  33 => 4,  30 => 3,);
    }
}


/* LaplaceTrainingBundle:Need:view.html.twig */
class __TwigTemplate_ad75e41e0767e6d589044c6fec593211_1101408982 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'PageTitle' => array($this, 'block_PageTitle'),
            'ContentTitle' => array($this, 'block_ContentTitle'),
            'ContentBody' => array($this, 'block_ContentBody'),
        );
    }

    protected function doGetParent(array $context)
    {
        return $this->env->resolveTemplate($this->getContext($context, "container"));
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 10
    public function block_PageTitle($context, array $blocks = array())
    {
        $this->displayParentBlock("PageTitle", $context, $blocks);
        echo " - ";
        if (isset($context["need"])) { $_need_ = $context["need"]; } else { $_need_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_need_, "title"), "html", null, true);
    }

    // line 12
    public function block_ContentTitle($context, array $blocks = array())
    {
        if (isset($context["need"])) { $_need_ = $context["need"]; } else { $_need_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_need_, "title"), "html", null, true);
    }

    // line 16
    public function block_ContentBody($context, array $blocks = array())
    {
        // line 17
        echo "
";
        // line 19
        if (isset($context["need"])) { $_need_ = $context["need"]; } else { $_need_ = null; }
        if (isset($context["laplace_training"])) { $_laplace_training_ = $context["laplace_training"]; } else { $_laplace_training_ = null; }
        $this->env->loadTemplate("LaplaceTrainingBundle::category-breadcrumb.html.twig")->display(array_merge($context, array("reference" => $_need_, "link" => $this->env->getExtension('routing')->getPath(($_laplace_training_ . "view_need"), array("id" => $this->getAttribute($_need_, "id"))))));
        // line 26
        echo "
<h5>Description du besoin</h5>

";
        // line 30
        if (isset($context["admin"])) { $_admin_ = $context["admin"]; } else { $_admin_ = null; }
        if ($_admin_) {
            // line 31
            echo "    <p>
        Par <a href=\"";
            // line 32
            if (isset($context["laplace_user"])) { $_laplace_user_ = $context["laplace_user"]; } else { $_laplace_user_ = null; }
            if (isset($context["need"])) { $_need_ = $context["need"]; } else { $_need_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath(($_laplace_user_ . "view_profile"), array("username" => $this->getAttribute($this->getAttribute($_need_, "author"), "username"))), "html", null, true);
            echo "\">";
            if (isset($context["need"])) { $_need_ = $context["need"]; } else { $_need_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_need_, "author"), "fullName"), "html", null, true);
            echo "</a>,
        le ";
            // line 33
            if (isset($context["need"])) { $_need_ = $context["need"]; } else { $_need_ = null; }
            echo twig_escape_filter($this->env, twig_localized_date_filter($this->env, $this->getAttribute($_need_, "issueDate"), "long", "short"), "html", null, true);
            echo ".
    </p>
";
        }
        // line 36
        echo "
";
        // line 38
        if (isset($context["need"])) { $_need_ = $context["need"]; } else { $_need_ = null; }
        if (twig_test_empty($this->getAttribute($_need_, "description"))) {
            // line 39
            echo "    <p>
        <em>Aucune description</em>.
    </p>
";
        } else {
            // line 43
            echo "    <pre>";
            if (isset($context["need"])) { $_need_ = $context["need"]; } else { $_need_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_need_, "description"), "html", null, true);
            echo "</pre>
";
        }
        // line 45
        echo "
";
        // line 47
        if (isset($context["admin"])) { $_admin_ = $context["admin"]; } else { $_admin_ = null; }
        if ($_admin_) {
            // line 48
            echo "    <p class=\"text-right\">
        <i class=\"icon-edit\"></i>
        <a href=\"";
            // line 50
            if (isset($context["laplace_training"])) { $_laplace_training_ = $context["laplace_training"]; } else { $_laplace_training_ = null; }
            if (isset($context["need"])) { $_need_ = $context["need"]; } else { $_need_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath(($_laplace_training_ . "edit_need"), array("id" => $this->getAttribute($_need_, "id"))), "html", null, true);
            echo "\">Modifier</a>

        <br />

        <i class=\"icon-trash\"></i>
        <a href=\"";
            // line 55
            if (isset($context["laplace_training"])) { $_laplace_training_ = $context["laplace_training"]; } else { $_laplace_training_ = null; }
            if (isset($context["need"])) { $_need_ = $context["need"]; } else { $_need_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath(($_laplace_training_ . "delete_need"), array("id" => $this->getAttribute($_need_, "id"))), "html", null, true);
            echo "\">Supprimer</a>
    </p>
";
        }
        // line 58
        echo "


<h5>Inscriptions</h5>
<p class=\"clearfix\">
";
        // line 63
        if (isset($context["subscriptions"])) { $_subscriptions_ = $context["subscriptions"]; } else { $_subscriptions_ = null; }
        if ((array_key_exists("subscriptions", $context) && (!twig_test_empty($_subscriptions_)))) {
            // line 64
            echo "    <ul>
    ";
            // line 65
            if (isset($context["subscriptions"])) { $_subscriptions_ = $context["subscriptions"]; } else { $_subscriptions_ = null; }
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($_subscriptions_);
            foreach ($context['_seq'] as $context["_key"] => $context["subinfo"]) {
                // line 66
                echo "        <li>
            <span class=\"label label-info\">";
                // line 68
                if (isset($context["subinfo"])) { $_subinfo_ = $context["subinfo"]; } else { $_subinfo_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($_subinfo_, "subscription"), "type"), "name"), "html", null, true);
                // line 69
                echo "</span>

            <a href=\"";
                // line 71
                if (isset($context["laplace_user"])) { $_laplace_user_ = $context["laplace_user"]; } else { $_laplace_user_ = null; }
                if (isset($context["subinfo"])) { $_subinfo_ = $context["subinfo"]; } else { $_subinfo_ = null; }
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath(($_laplace_user_ . "view_profile"), array("username" => $this->getAttribute($this->getAttribute($_subinfo_, "user"), "username"))), "html", null, true);
                echo "\">";
                if (isset($context["subinfo"])) { $_subinfo_ = $context["subinfo"]; } else { $_subinfo_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_subinfo_, "user"), "fullName"), "html", null, true);
                echo "</a>
            (";
                // line 72
                if (isset($context["subinfo"])) { $_subinfo_ = $context["subinfo"]; } else { $_subinfo_ = null; }
                echo twig_escape_filter($this->env, twig_localized_date_filter($this->env, $this->getAttribute($this->getAttribute($_subinfo_, "subscription"), "subscriptionDate"), "long", "none"), "html", null, true);
                echo ")
        </li>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subinfo'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 75
            echo "    </ul>
";
        } else {
            // line 77
            echo "    ";
            if (isset($context["need"])) { $_need_ = $context["need"]; } else { $_need_ = null; }
            $context["count"] = $this->getAttribute($this->getAttribute($_need_, "subscriptions"), "count", array(), "method");
            // line 78
            echo "    Ce besoin a été ajouté par
    <span class=\"badge";
            // line 79
            if (isset($context["count"])) { $_count_ = $context["count"]; } else { $_count_ = null; }
            if (($_count_ > 0)) {
                echo " badge-info";
            }
            echo "\">";
            if (isset($context["count"])) { $_count_ = $context["count"]; } else { $_count_ = null; }
            echo twig_escape_filter($this->env, $_count_, "html", null, true);
            echo "</span>
    personne(s).
";
        }
        // line 82
        echo "</p>

";
        // line 85
        echo "
<hr />

";
        // line 89
        echo "
";
        // line 90
        if (isset($context["admin"])) { $_admin_ = $context["admin"]; } else { $_admin_ = null; }
        if ((!$_admin_)) {
            // line 91
            echo "
<h3>Gestion</h3>

    ";
            // line 94
            if (isset($context["subscription"])) { $_subscription_ = $context["subscription"]; } else { $_subscription_ = null; }
            if ((array_key_exists("subscription", $context) && (!(null === $_subscription_)))) {
                // line 95
                echo "
<p>
    Vous avez ajouté ce besoin à votre liste personnelle
    le <strong>";
                // line 98
                if (isset($context["subscription"])) { $_subscription_ = $context["subscription"]; } else { $_subscription_ = null; }
                echo twig_escape_filter($this->env, twig_localized_date_filter($this->env, $this->getAttribute($_subscription_, "subscriptionDate"), "long", "short"), "html", null, true);
                echo "</strong>
    avec le type
    <span class=\"label label-info\"
          title=\"";
                // line 101
                if (isset($context["subscription"])) { $_subscription_ = $context["subscription"]; } else { $_subscription_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_subscription_, "type"), "nameWithDescription"), "html", null, true);
                echo "\">";
                // line 102
                if (isset($context["subscription"])) { $_subscription_ = $context["subscription"]; } else { $_subscription_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_subscription_, "type"), "name"), "html", null, true);
                // line 103
                echo "</span>.
</p>

    ";
            }
            // line 107
            echo "
";
            // line 109
            echo "
    ";
            // line 110
            if (array_key_exists("edit_form", $context)) {
                // line 111
                echo "
";
                // line 112
                if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
                echo                 $this->env->getExtension('form')->renderer->renderBlock($_edit_form_, 'form_start');
                echo "

<fieldset>
    <legend>Modifier</legend>

    <p>
        Ce formulaire vous permet de changer le type de besoin.
    </p>


    ";
                // line 122
                if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($_edit_form_, 'rest');
                echo "

</fieldset>

";
                // line 126
                if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
                echo                 $this->env->getExtension('form')->renderer->renderBlock($_edit_form_, 'form_end');
                echo "

    ";
            }
            // line 129
            echo "


";
            // line 133
            echo "
    ";
            // line 134
            if (array_key_exists("satisfied_form", $context)) {
                // line 135
                echo "
";
                // line 136
                if (isset($context["satisfied_form"])) { $_satisfied_form_ = $context["satisfied_form"]; } else { $_satisfied_form_ = null; }
                echo                 $this->env->getExtension('form')->renderer->renderBlock($_satisfied_form_, 'form_start');
                echo "

<fieldset>
    <legend>Indiquer que le besoin est satisfait</legend>

    <p>
        Ce formulaire vous permet d'indiquer que ce besoin est pourvu.
        <strong>Attention !</strong> Il ne sera alors plus possible de
        modifier cette inscription.
        Les besoins pourvus restent inscrits dans votre historique.
    </p>


    ";
                // line 149
                if (isset($context["satisfied_form"])) { $_satisfied_form_ = $context["satisfied_form"]; } else { $_satisfied_form_ = null; }
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($_satisfied_form_, 'rest');
                echo "

</fieldset>

";
                // line 153
                if (isset($context["satisfied_form"])) { $_satisfied_form_ = $context["satisfied_form"]; } else { $_satisfied_form_ = null; }
                echo                 $this->env->getExtension('form')->renderer->renderBlock($_satisfied_form_, 'form_end');
                echo "

    ";
            }
            // line 156
            echo "


";
            // line 160
            echo "
    ";
            // line 161
            if (array_key_exists("unsubscribe_form", $context)) {
                // line 162
                echo "
";
                // line 163
                if (isset($context["unsubscribe_form"])) { $_unsubscribe_form_ = $context["unsubscribe_form"]; } else { $_unsubscribe_form_ = null; }
                echo                 $this->env->getExtension('form')->renderer->renderBlock($_unsubscribe_form_, 'form_start');
                echo "

<fieldset>
    <legend>Retirer ce besoin</legend>

    <p>
        Si vous avez par erreur ajouté ce besoin à votre liste, vous pouvez
        le retirer en utilisant ce formulaire.
        Le besoin disparaîtra également de votre historique.
    </p>

    ";
                // line 174
                if (isset($context["unsubscribe_form"])) { $_unsubscribe_form_ = $context["unsubscribe_form"]; } else { $_unsubscribe_form_ = null; }
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_unsubscribe_form_, "unsubscribe"), 'row');
                echo "

    ";
                // line 176
                if (isset($context["unsubscribe_form"])) { $_unsubscribe_form_ = $context["unsubscribe_form"]; } else { $_unsubscribe_form_ = null; }
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_unsubscribe_form_, "do"), 'row', array("attr" => array("class" => "btn-danger")));
                echo "

    ";
                // line 178
                if (isset($context["unsubscribe_form"])) { $_unsubscribe_form_ = $context["unsubscribe_form"]; } else { $_unsubscribe_form_ = null; }
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($_unsubscribe_form_, 'rest');
                echo "

</fieldset>

";
                // line 182
                if (isset($context["unsubscribe_form"])) { $_unsubscribe_form_ = $context["unsubscribe_form"]; } else { $_unsubscribe_form_ = null; }
                echo                 $this->env->getExtension('form')->renderer->renderBlock($_unsubscribe_form_, 'form_end');
                echo "

    ";
            }
            // line 185
            echo "


";
            // line 189
            echo "
    ";
            // line 190
            if (array_key_exists("subscribe_form", $context)) {
                // line 191
                echo "
";
                // line 192
                if (isset($context["subscribe_form"])) { $_subscribe_form_ = $context["subscribe_form"]; } else { $_subscribe_form_ = null; }
                echo                 $this->env->getExtension('form')->renderer->renderBlock($_subscribe_form_, 'form_start');
                echo "
<fieldset>
    <legend>Ajouter ce besoin</legend>

";
                // line 196
                if (isset($context["subscribe_form"])) { $_subscribe_form_ = $context["subscribe_form"]; } else { $_subscribe_form_ = null; }
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($_subscribe_form_, 'rest');
                echo "

</fieldset>
";
                // line 199
                if (isset($context["subscribe_form"])) { $_subscribe_form_ = $context["subscribe_form"]; } else { $_subscribe_form_ = null; }
                echo                 $this->env->getExtension('form')->renderer->renderBlock($_subscribe_form_, 'form_end');
                echo "

    ";
            }
            // line 202
            echo "
";
            // line 204
            echo "
<hr />

";
            // line 208
            echo "
";
        }
        // line 210
        echo "

<h3>Discussions</h3>

";
        // line 214
        if (isset($context["admin"])) { $_admin_ = $context["admin"]; } else { $_admin_ = null; }
        if ((!$_admin_)) {
            // line 215
            echo "<p>
    Cet espace vous permet d'échanger de manière privée avec le correspondant
    formation. Pour poser une question ou ajouter une remarque concernant ce
    besoin, ouvrez une nouvelle discussion à l'aide du formulaire ci-dessous.
    Les messages publiés ne seront visibles que par vous,
    le correspondant formation, et les autres personnes autorisées
    individuellement à participer à la discussion.
</p>
";
        }
        // line 224
        echo "
";
        // line 225
        $this->env->loadTemplate("LaplaceTrainingBundle:Thread:table.html.twig")->display($context);
        // line 226
        echo "
";
        // line 227
        if (isset($context["thread_form"])) { $_thread_form_ = $context["thread_form"]; } else { $_thread_form_ = null; }
        echo         $this->env->getExtension('form')->renderer->renderBlock($_thread_form_, 'form_start');
        echo "

<fieldset>
    <legend>Ouvrir une nouvelle discussion</legend>

    ";
        // line 232
        if (isset($context["thread_form"])) { $_thread_form_ = $context["thread_form"]; } else { $_thread_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_thread_form_, "thread"), 'rest');
        echo "

    ";
        // line 234
        if (isset($context["thread_form"])) { $_thread_form_ = $context["thread_form"]; } else { $_thread_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_thread_form_, "message"), 'rest');
        echo "

    ";
        // line 236
        if (isset($context["thread_form"])) { $_thread_form_ = $context["thread_form"]; } else { $_thread_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($_thread_form_, 'rest');
        echo "

</fieldset>

";
        // line 240
        if (isset($context["thread_form"])) { $_thread_form_ = $context["thread_form"]; } else { $_thread_form_ = null; }
        echo         $this->env->getExtension('form')->renderer->renderBlock($_thread_form_, 'form_end');
        echo "

";
        // line 243
        echo "
";
    }

    public function getTemplateName()
    {
        return "LaplaceTrainingBundle:Need:view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  539 => 243,  533 => 240,  525 => 236,  519 => 234,  513 => 232,  504 => 227,  501 => 226,  499 => 225,  496 => 224,  485 => 215,  482 => 214,  476 => 210,  472 => 208,  467 => 204,  464 => 202,  457 => 199,  442 => 192,  439 => 191,  422 => 182,  414 => 178,  402 => 174,  387 => 163,  384 => 162,  382 => 161,  379 => 160,  374 => 156,  367 => 153,  342 => 136,  339 => 135,  337 => 134,  334 => 133,  329 => 129,  322 => 126,  300 => 112,  297 => 111,  292 => 109,  289 => 107,  283 => 103,  280 => 102,  276 => 101,  269 => 98,  261 => 94,  250 => 89,  241 => 82,  184 => 65,  176 => 51,  73 => 29,  99 => 22,  79 => 7,  257 => 78,  249 => 74,  253 => 90,  235 => 64,  198 => 46,  177 => 42,  174 => 41,  169 => 40,  68 => 29,  61 => 27,  341 => 118,  336 => 101,  331 => 97,  326 => 102,  324 => 101,  317 => 97,  314 => 122,  311 => 95,  305 => 104,  303 => 95,  298 => 92,  291 => 85,  288 => 84,  281 => 86,  273 => 80,  270 => 79,  265 => 107,  263 => 91,  260 => 90,  258 => 79,  252 => 77,  244 => 77,  223 => 67,  218 => 75,  159 => 46,  96 => 40,  86 => 15,  246 => 109,  210 => 69,  245 => 85,  237 => 72,  225 => 76,  209 => 68,  194 => 61,  150 => 35,  95 => 14,  128 => 39,  214 => 52,  191 => 87,  185 => 85,  42 => 15,  293 => 127,  286 => 125,  277 => 123,  267 => 119,  255 => 78,  238 => 81,  229 => 79,  220 => 75,  212 => 88,  188 => 59,  161 => 57,  144 => 44,  135 => 32,  126 => 29,  172 => 49,  165 => 38,  151 => 65,  116 => 30,  113 => 28,  153 => 50,  145 => 43,  139 => 60,  127 => 38,  112 => 27,  87 => 16,  94 => 118,  91 => 17,  23 => 3,  170 => 62,  125 => 26,  103 => 19,  82 => 9,  549 => 162,  543 => 161,  538 => 158,  530 => 155,  526 => 153,  522 => 151,  512 => 149,  505 => 148,  502 => 147,  497 => 146,  491 => 144,  488 => 143,  483 => 142,  473 => 134,  469 => 132,  466 => 131,  460 => 130,  455 => 129,  450 => 196,  444 => 122,  441 => 121,  437 => 190,  434 => 189,  429 => 185,  423 => 112,  420 => 111,  416 => 110,  413 => 109,  408 => 176,  394 => 105,  390 => 103,  375 => 101,  365 => 99,  362 => 98,  359 => 149,  355 => 95,  348 => 91,  344 => 90,  330 => 89,  327 => 88,  321 => 86,  307 => 85,  302 => 84,  295 => 110,  287 => 80,  279 => 84,  271 => 77,  264 => 95,  256 => 91,  251 => 71,  247 => 107,  239 => 68,  231 => 78,  219 => 74,  201 => 60,  147 => 34,  143 => 45,  134 => 33,  131 => 29,  122 => 24,  102 => 30,  92 => 17,  84 => 114,  72 => 52,  48 => 18,  35 => 5,  29 => 3,  76 => 7,  69 => 5,  54 => 20,  51 => 19,  31 => 11,  205 => 48,  199 => 71,  190 => 58,  182 => 43,  179 => 67,  175 => 58,  168 => 38,  164 => 47,  156 => 34,  148 => 47,  138 => 34,  123 => 30,  117 => 33,  108 => 32,  83 => 14,  71 => 10,  64 => 25,  110 => 28,  89 => 12,  65 => 14,  63 => 17,  58 => 15,  34 => 12,  227 => 58,  224 => 91,  221 => 54,  207 => 82,  197 => 74,  195 => 69,  192 => 68,  189 => 66,  186 => 53,  181 => 64,  178 => 63,  173 => 40,  162 => 47,  158 => 69,  155 => 36,  152 => 30,  142 => 47,  136 => 43,  133 => 40,  130 => 39,  120 => 27,  105 => 31,  100 => 22,  75 => 53,  53 => 13,  39 => 7,  98 => 19,  80 => 12,  78 => 42,  46 => 16,  44 => 10,  36 => 6,  32 => 8,  60 => 21,  57 => 9,  40 => 16,  114 => 25,  109 => 48,  106 => 19,  101 => 20,  88 => 12,  85 => 10,  77 => 7,  67 => 27,  47 => 24,  28 => 18,  25 => 7,  55 => 14,  43 => 17,  38 => 7,  26 => 8,  24 => 3,  50 => 7,  27 => 2,  22 => 6,  19 => 2,  232 => 79,  226 => 78,  222 => 77,  215 => 65,  211 => 84,  208 => 72,  202 => 65,  196 => 59,  193 => 65,  187 => 44,  183 => 60,  180 => 59,  171 => 58,  166 => 51,  163 => 55,  160 => 49,  157 => 43,  149 => 48,  146 => 47,  140 => 32,  137 => 42,  129 => 36,  124 => 36,  121 => 24,  118 => 33,  115 => 32,  111 => 57,  107 => 45,  104 => 19,  97 => 26,  93 => 19,  90 => 17,  81 => 12,  70 => 5,  66 => 13,  62 => 24,  59 => 23,  56 => 21,  52 => 10,  49 => 17,  45 => 6,  41 => 7,  37 => 20,  33 => 4,  30 => 3,);
    }
}
