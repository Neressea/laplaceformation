<?php

/* LaplaceTrainingBundle:Request:view.html.twig */
class __TwigTemplate_e0e8141e6c9a5de301e5f5acf3253401 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        $context["container"] = (($this->getContext($context, "admin")) ? ("LaplaceCommonBundle::admin-page.html.twig") : ("LaplaceCommonBundle::user-page.html.twig"));
        // line 6
        echo "

";
        // line 8
        $this->env->loadTemplate("LaplaceTrainingBundle:Request:view.html.twig", "584899402")->display(array_merge($context, array("page" => array(0 => "request", 1 => "view"))));
    }

    public function getTemplateName()
    {
        return "LaplaceTrainingBundle:Request:view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  188 => 63,  334 => 148,  327 => 143,  315 => 137,  306 => 131,  284 => 122,  279 => 120,  274 => 119,  272 => 118,  262 => 114,  239 => 103,  206 => 85,  186 => 62,  280 => 103,  255 => 90,  245 => 88,  150 => 48,  487 => 243,  482 => 240,  475 => 236,  470 => 234,  465 => 232,  454 => 226,  452 => 225,  449 => 224,  438 => 215,  436 => 214,  426 => 208,  421 => 204,  418 => 202,  412 => 199,  406 => 196,  399 => 192,  396 => 191,  394 => 190,  391 => 189,  386 => 185,  380 => 182,  373 => 178,  363 => 174,  349 => 163,  346 => 162,  344 => 161,  336 => 156,  323 => 149,  307 => 136,  304 => 135,  302 => 129,  288 => 123,  281 => 122,  265 => 111,  263 => 110,  260 => 109,  251 => 89,  233 => 94,  223 => 89,  185 => 72,  170 => 66,  180 => 58,  174 => 56,  65 => 27,  234 => 101,  225 => 76,  237 => 102,  230 => 100,  221 => 95,  213 => 58,  207 => 54,  200 => 67,  110 => 25,  34 => 20,  335 => 118,  330 => 153,  325 => 97,  320 => 139,  318 => 101,  311 => 97,  308 => 96,  299 => 133,  297 => 95,  292 => 92,  289 => 91,  282 => 84,  275 => 100,  273 => 84,  259 => 107,  257 => 107,  249 => 102,  240 => 98,  238 => 77,  231 => 72,  222 => 75,  218 => 85,  211 => 65,  198 => 80,  194 => 75,  184 => 59,  179 => 71,  152 => 46,  77 => 7,  63 => 17,  58 => 23,  53 => 21,  227 => 99,  215 => 79,  210 => 77,  205 => 68,  226 => 85,  219 => 94,  195 => 69,  172 => 58,  202 => 62,  190 => 75,  146 => 69,  113 => 29,  59 => 23,  267 => 94,  261 => 125,  254 => 111,  244 => 106,  228 => 91,  212 => 70,  204 => 75,  197 => 88,  191 => 48,  175 => 69,  167 => 53,  159 => 49,  127 => 39,  118 => 27,  134 => 41,  129 => 50,  100 => 22,  178 => 58,  161 => 48,  104 => 20,  102 => 30,  23 => 3,  181 => 68,  153 => 51,  148 => 45,  124 => 36,  97 => 28,  90 => 17,  76 => 7,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 227,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 210,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 176,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 160,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 126,  285 => 85,  283 => 88,  278 => 86,  268 => 117,  264 => 79,  258 => 113,  252 => 79,  247 => 78,  241 => 86,  235 => 95,  229 => 73,  224 => 71,  220 => 81,  214 => 74,  208 => 70,  169 => 60,  143 => 35,  140 => 44,  132 => 51,  128 => 29,  119 => 46,  107 => 28,  71 => 10,  177 => 60,  165 => 54,  160 => 62,  135 => 42,  126 => 39,  114 => 36,  84 => 18,  70 => 5,  67 => 15,  61 => 13,  28 => 5,  87 => 19,  201 => 78,  196 => 65,  183 => 91,  171 => 55,  166 => 65,  163 => 51,  158 => 67,  156 => 48,  151 => 57,  142 => 53,  138 => 34,  136 => 37,  121 => 37,  117 => 33,  105 => 32,  91 => 39,  62 => 25,  49 => 19,  31 => 8,  21 => 6,  25 => 8,  93 => 18,  88 => 16,  78 => 14,  44 => 10,  94 => 20,  89 => 12,  85 => 10,  75 => 53,  68 => 29,  56 => 22,  27 => 18,  38 => 15,  24 => 7,  46 => 18,  26 => 6,  19 => 2,  79 => 8,  72 => 52,  69 => 5,  47 => 17,  40 => 11,  37 => 11,  22 => 2,  246 => 107,  157 => 48,  145 => 39,  139 => 65,  131 => 40,  123 => 37,  120 => 29,  115 => 32,  111 => 25,  108 => 40,  101 => 20,  98 => 19,  96 => 16,  83 => 15,  74 => 14,  66 => 27,  55 => 17,  52 => 21,  50 => 20,  43 => 24,  41 => 16,  35 => 9,  32 => 11,  29 => 6,  209 => 87,  203 => 78,  199 => 71,  193 => 68,  189 => 66,  187 => 54,  182 => 60,  176 => 87,  173 => 54,  168 => 52,  164 => 41,  162 => 49,  154 => 47,  149 => 58,  147 => 44,  144 => 42,  141 => 43,  133 => 43,  130 => 33,  125 => 31,  122 => 35,  116 => 26,  112 => 31,  109 => 30,  106 => 26,  103 => 54,  99 => 17,  95 => 19,  92 => 17,  86 => 16,  82 => 9,  80 => 8,  73 => 30,  64 => 14,  60 => 20,  57 => 11,  54 => 27,  51 => 20,  48 => 11,  45 => 17,  42 => 13,  39 => 10,  36 => 5,  33 => 7,  30 => 10,);
    }
}


/* LaplaceTrainingBundle:Request:view.html.twig */
class __TwigTemplate_e0e8141e6c9a5de301e5f5acf3253401_584899402 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'PageTitle' => array($this, 'block_PageTitle'),
            'ContentTitle' => array($this, 'block_ContentTitle'),
            'ContentBody' => array($this, 'block_ContentBody'),
        );
    }

    protected function doGetParent(array $context)
    {
        return $this->env->resolveTemplate($this->getContext($context, "container"));
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 10
    public function block_PageTitle($context, array $blocks = array())
    {
        $this->displayParentBlock("PageTitle", $context, $blocks);
        echo " - ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "title"), "html", null, true);
    }

    // line 12
    public function block_ContentTitle($context, array $blocks = array())
    {
        // line 13
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "title"), "html", null, true);
        echo "
    ";
        // line 14
        if ((!$this->getAttribute($this->getContext($context, "request"), "validated"))) {
            echo "<i class=\"icon-eye-close\"></i>";
        }
        // line 15
        echo "    ";
        if ($this->getAttribute($this->getContext($context, "request"), "close")) {
            echo "<i class=\"icon-lock\"></i>";
        }
    }

    // line 20
    public function block_ContentBody($context, array $blocks = array())
    {
        // line 21
        echo "
";
        // line 23
        $this->env->loadTemplate("LaplaceTrainingBundle::category-breadcrumb.html.twig")->display(array_merge($context, array("reference" => $this->getContext($context, "request"), "link" => $this->env->getExtension('routing')->getPath(($this->getContext($context, "laplace_training") . "view_request"), array("id" => $this->getAttribute($this->getContext($context, "request"), "id"))))));
        // line 30
        echo "
";
        // line 31
        if ((!$this->getAttribute($this->getContext($context, "request"), "validated"))) {
            // line 32
            echo "<p class=\"text-warning\">
    ";
            // line 33
            if ($this->getContext($context, "admin")) {
                // line 34
                echo "    Cette demande n'est pas encore validée et n'est visible que par son auteur.
    Une fois modifiée, elle deviendra accessible à tous les utilisateurs.
    ";
            } else {
                // line 37
                echo "    Cette demande de formation n'a pas encore été validée par le correspondant
    formation. Pour le moment, elle n'apparait donc pas dans la liste de toutes
    les demandes.
    ";
            }
            // line 41
            echo "</p>
";
        }
        // line 43
        echo "
<h5>Description de la demande</h5>

";
        // line 47
        if ($this->getContext($context, "admin")) {
            // line 48
            echo "    <p>
        Par <a href=\"";
            // line 49
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath(($this->getContext($context, "laplace_user") . "view_profile"), array("username" => $this->getAttribute($this->getAttribute($this->getContext($context, "request"), "author"), "username"))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "request"), "author"), "fullName"), "html", null, true);
            echo "</a>,
        le ";
            // line 50
            echo twig_escape_filter($this->env, twig_localized_date_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "issueDate"), "long", "short"), "html", null, true);
            echo ".
    </p>
    ";
            // line 52
            if ((!(null === $this->getAttribute($this->getContext($context, "request"), "processingDate")))) {
                // line 53
                echo "    <p>
        <strong>Demande traitée le ";
                // line 54
                echo twig_escape_filter($this->env, twig_localized_date_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "processingDate"), "long", "none"), "html", null, true);
                echo ".</strong>
    </p>
    ";
            }
        }
        // line 58
        echo "
";
        // line 60
        if (twig_test_empty($this->getAttribute($this->getContext($context, "request"), "description"))) {
            // line 61
            echo "    <p>
        <em>Aucune description</em>.
    </p>
";
        } else {
            // line 65
            echo "    <pre>";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "description"), "html", null, true);
            echo "</pre>
";
        }
        // line 67
        echo "
<dl>

    ";
        // line 70
        if ($this->getAttribute($this->getContext($context, "request"), "trainingDate")) {
            // line 71
            echo "        <dt>Date de la formation :</dt>
        <dd>";
            // line 72
            echo twig_escape_filter($this->env, twig_localized_date_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "trainingDate"), "full", "short"), "html", null, true);
            echo "</dd>
    ";
        }
        // line 74
        echo "
    ";
        // line 75
        if ($this->getAttribute($this->getContext($context, "request"), "trainingDuration")) {
            // line 76
            echo "        <dt>Durée de la formation (en nombre d'heures) :</dt>
        <dd>";
            // line 77
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "trainingDuration"), "html", null, true);
            echo "
    ";
        }
        // line 79
        echo "
    ";
        // line 80
        if ($this->getAttribute($this->getContext($context, "request"), "subscriptionDeadline")) {
            // line 81
            echo "        <dt>Date d'inscription limite :</dt>
        <dd>";
            // line 82
            echo twig_escape_filter($this->env, twig_localized_date_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "subscriptionDeadline"), "full", "none"), "html", null, true);
            echo "</dd>
    ";
        }
        // line 84
        echo "
    ";
        // line 85
        if ($this->getAttribute($this->getContext($context, "request"), "availablePlacesCount")) {
            // line 86
            echo "        <dt>Nombre de places disponibles :</dt>
        <dd>";
            // line 87
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "availablePlacesCount"), "html", null, true);
            echo "
    ";
        }
        // line 89
        echo "
    ";
        // line 90
        if ($this->getAttribute($this->getContext($context, "request"), "agentsPrioritaires")) {
            // line 91
            echo "        <dt>Agents prioritaires :</dt>
        <dd>";
            // line 92
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "agentsPrioritaires"), "html", null, true);
            echo "
    ";
        }
        // line 94
        echo "
    ";
        // line 95
        if ($this->getAttribute($this->getContext($context, "request"), "funding")) {
            // line 96
            echo "        <dt>Financement :</dt>
        <dd>";
            // line 97
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "funding"), "html", null, true);
            echo "
    ";
        }
        // line 99
        echo "
</dl>

";
        // line 103
        if ($this->getContext($context, "admin")) {
            // line 104
            echo "    <p class=\"text-right\">
        <i class=\"icon-edit\"></i>
        <a href=\"";
            // line 106
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath(($this->getContext($context, "laplace_training") . "edit_request"), array("id" => $this->getAttribute($this->getContext($context, "request"), "id"))), "html", null, true);
            echo "\">Modifier</a>

        <br />

        <i class=\"icon-trash\"></i>
        <a href=\"";
            // line 111
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath(($this->getContext($context, "laplace_training") . "delete_request"), array("id" => $this->getAttribute($this->getContext($context, "request"), "id"))), "html", null, true);
            echo "\">Supprimer</a>
    </p>
";
        }
        // line 114
        echo "


<h5>Inscriptions</h5>
<p class=\"clearfix\">
";
        // line 119
        if ((array_key_exists("subscriptions", $context) && (!twig_test_empty($this->getContext($context, "subscriptions"))))) {
            // line 120
            echo "    <ul>
    ";
            // line 121
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getContext($context, "subscriptions"));
            foreach ($context['_seq'] as $context["_key"] => $context["subinfo"]) {
                // line 122
                echo "        <li>
            <span class=\"label label-info\">";
                // line 124
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "subinfo"), "subscription"), "type"), "name"), "html", null, true);
                // line 125
                echo "</span>

            <a href=\"";
                // line 127
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath(($this->getContext($context, "laplace_user") . "view_profile"), array("username" => $this->getAttribute($this->getAttribute($this->getContext($context, "subinfo"), "user"), "username"))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "subinfo"), "user"), "fullName"), "html", null, true);
                echo "</a>
            (";
                // line 128
                echo twig_escape_filter($this->env, twig_localized_date_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "subinfo"), "subscription"), "subscriptionDate"), "long", "none"), "html", null, true);
                echo ")
        </li>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subinfo'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 131
            echo "    </ul>
";
        } else {
            // line 133
            echo "    ";
            $context["count"] = $this->getAttribute($this->getAttribute($this->getContext($context, "request"), "subscriptions"), "count", array(), "method");
            // line 134
            echo "    Le nombre de personnes inscrites à cette demande est
    <span class=\"badge";
            // line 135
            if (($this->getContext($context, "count") > 0)) {
                echo " badge-info";
            }
            echo "\">";
            echo twig_escape_filter($this->env, $this->getContext($context, "count"), "html", null, true);
            echo "</span>.
";
        }
        // line 137
        echo "</p>

";
        // line 140
        echo "
<hr />

";
        // line 144
        echo "
<h3>Gestion</h3>

";
        // line 147
        if ((!$this->getContext($context, "admin"))) {
            // line 148
            echo "
    ";
            // line 149
            if ($this->getAttribute($this->getContext($context, "request"), "close")) {
                // line 150
                echo "
<p class=\"text-warning\">
    Cette demande de formation est close. Il n'est désormais plus possible
    de s'y inscrire, mais vous pouvez encore modifier et gérer une
    inscription en cours.
</p>

    ";
            }
            // line 158
            echo "
    ";
            // line 159
            if ((array_key_exists("subscription", $context) && (!(null === $this->getContext($context, "subscription"))))) {
                // line 160
                echo "
<p>
    Vous vous êtes inscrit(e) à cette formation le <strong>";
                // line 162
                echo twig_escape_filter($this->env, twig_localized_date_filter($this->env, $this->getAttribute($this->getContext($context, "subscription"), "subscriptionDate"), "long", "short"), "html", null, true);
                echo "</strong>
    avec le type
    <span class=\"label label-info\"
          title=\"";
                // line 165
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "subscription"), "type"), "nameWithDescription"), "html", null, true);
                echo "\">";
                // line 166
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "subscription"), "type"), "name"), "html", null, true);
                // line 167
                echo "</span>.
</p>

        ";
                // line 170
                if (($this->getAttribute($this->getContext($context, "subscription"), "accepted") === null)) {
                    // line 171
                    echo "<p>
    Cette demande est <strong>en attente d'acceptation</strong> par le
    correspondant formation.
</p>
        ";
                } elseif (($this->getAttribute($this->getContext($context, "subscription"), "accepted") === true)) {
                    // line 176
                    echo "<p>
    Cette demande a été <strong>acceptée</strong> par le correspondant formation.
</p>
            ";
                    // line 179
                    if (($this->getAttribute($this->getContext($context, "subscription"), "attended") === null)) {
                        // line 180
                        echo "<p>
    Vous n'avez <strong>pas encore indiqué</strong> avoir assisté à la formation.
</p>
            ";
                    } elseif (($this->getAttribute($this->getContext($context, "subscription"), "attended") === true)) {
                        // line 184
                        echo "<p>
    Vous avez indiqué <strong>avoir assisté</strong> à la formation.
</p>
            ";
                    } else {
                        // line 188
                        echo "<p>
    Vous avez indiqué <strong>n'avoir pas assisté</strong> à la formation.
</p>
            ";
                    }
                    // line 192
                    echo "

        ";
                } else {
                    // line 195
                    echo "<p>
    Votre inscription à cette demande a été <strong>refusée</strong> par
    le correspondant formation.
</p>
        ";
                }
                // line 200
                echo "
    ";
            }
            // line 202
            echo "
";
            // line 204
            echo "
    ";
            // line 205
            if (array_key_exists("edit_form", $context)) {
                // line 206
                echo "
";
                // line 207
                echo                 $this->env->getExtension('form')->renderer->renderBlock($this->getContext($context, "edit_form"), 'form_start');
                echo "

<fieldset>
    <legend>Modifier</legend>

    <p>
        Ce formulaire vous permet de changer le type de demande.
    </p>


    ";
                // line 217
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "edit_form"), 'rest');
                echo "

</fieldset>

";
                // line 221
                echo                 $this->env->getExtension('form')->renderer->renderBlock($this->getContext($context, "edit_form"), 'form_end');
                echo "

    ";
            }
            // line 224
            echo "


";
            // line 228
            echo "
    ";
            // line 229
            if (array_key_exists("unsubscribe_form", $context)) {
                // line 230
                echo "
";
                // line 231
                echo                 $this->env->getExtension('form')->renderer->renderBlock($this->getContext($context, "unsubscribe_form"), 'form_start');
                echo "

<fieldset>
    <legend>Retirer cette demande</legend>

    <p>
        Si vous vous êtes inscrit(e) par erreur à cette demande, vous pouvez
        la retirer en utilisant ce formulaire.
        L'inscription disparaîtra également de votre historique.
    </p>

    ";
                // line 242
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "unsubscribe_form"), "unsubscribe"), 'row');
                echo "

    ";
                // line 244
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "unsubscribe_form"), "do"), 'row', array("attr" => array("class" => "btn-danger")));
                echo "

    ";
                // line 246
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "unsubscribe_form"), 'rest');
                echo "

</fieldset>

";
                // line 250
                echo                 $this->env->getExtension('form')->renderer->renderBlock($this->getContext($context, "unsubscribe_form"), 'form_end');
                echo "

    ";
            }
            // line 253
            echo "


";
            // line 257
            echo "
    ";
            // line 258
            if (array_key_exists("attended_form", $context)) {
                // line 259
                echo "
";
                // line 260
                echo                 $this->env->getExtension('form')->renderer->renderBlock($this->getContext($context, "attended_form"), 'form_start');
                echo "

<fieldset>
    <legend>Indiquer sa présence</legend>

    <p>
        Ce formulaire vous permet d'indiquer que vous avez suivi ou non la
        formation.
    </p>

    ";
                // line 270
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "attended_form"), 'rest');
                echo "

</fieldset>

";
                // line 274
                echo                 $this->env->getExtension('form')->renderer->renderBlock($this->getContext($context, "attended_form"), 'form_end');
                echo "

    ";
            }
            // line 277
            echo "


";
            // line 281
            echo "
    ";
            // line 282
            if (array_key_exists("subscribe_form", $context)) {
                // line 283
                echo "
";
                // line 284
                echo                 $this->env->getExtension('form')->renderer->renderBlock($this->getContext($context, "subscribe_form"), 'form_start');
                echo "
<fieldset>
    <legend>S'inscrire à cette demande</legend>

<p>
    Si vous souhaitez <strong>participer à cette formation</strong>,
    inscrivez-vous ci-dessous. Par la suite, le correspondant formation
    <strong>validera ou refusera</strong> votre inscription à cette
    demande de formation.
</p>

<p>
    <strong>Rappel des formalités administratives</strong>. Une fois votre
    demande validée, n'oubliez pas de :
    <ul>
        <li>remplir le formulaire de <em>Demande d'inscription à une formation</em> ;</li>
        <li>remplir le formulaire de <em>Demande d'ordre de mission</em> s'il y a déplacement ;</li>
        <li>faire signer votre demande par le correspondant formation si la formation est dispensée par le CNRS ;</li>
        <li>prévenir le secrétariat.</li>
    </ul>
</p>

<p class=\"text-right\">
    <a href=\"http://intranet.laplace.univ-tlse.fr/spip.php?rubrique137\">Voir l'ensemble des démarches à effectuer</a>
</p>

";
                // line 310
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "subscribe_form"), 'rest');
                echo "

</fieldset>
";
                // line 313
                echo                 $this->env->getExtension('form')->renderer->renderBlock($this->getContext($context, "subscribe_form"), 'form_end');
                echo "

    ";
            }
            // line 316
            echo "
";
        } else {
            // line 318
            echo "
";
            // line 320
            echo "
";
            // line 321
            if ((!twig_test_empty($this->getContext($context, "subscriptions")))) {
                // line 322
                echo "
<fieldset>
    <legend>Gérer les inscriptions</legend>

<p>
    Pour chacune des demandes ci-dessous, vous pouvez <strong>accepter,
    refuser ou mettre en attente</strong> l'inscription.
    La participation est ré-initialisée à \"Aucune indication\" si l'état
    de la demande n'est pas \"Demande acceptée\".
</p>

<ul class=\"nav nav-pills\">
    ";
                // line 334
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getContext($context, "subscriptions"));
                $context['loop'] = array(
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                );
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["_key"] => $context["subinfo"]) {
                    // line 335
                    echo "    <li class=\"";
                    echo (($this->getAttribute($this->getContext($context, "loop"), "first")) ? ("active") : (""));
                    echo "\">
        <a href=\"#manage_";
                    // line 336
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "loop"), "index"), "html", null, true);
                    echo "\" data-toggle=\"tab\">";
                    // line 337
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "subinfo"), "user"), "fullName"), "html", null, true);
                    // line 338
                    echo "</a>
    </li>
    ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subinfo'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 341
                echo "</ul>

<div class=\"tab-content\">

    ";
                // line 345
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getContext($context, "subscriptions"));
                $context['loop'] = array(
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                );
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["_key"] => $context["subinfo"]) {
                    // line 346
                    echo "    <div id=\"manage_";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "loop"), "index"), "html", null, true);
                    echo "\" class=\"tab-pane fade ";
                    echo (($this->getAttribute($this->getContext($context, "loop"), "first")) ? ("active in") : (""));
                    echo "\">
    ";
                    // line 347
                    echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("LaplaceTrainingBundle:Request:manageSubscriptionForm", array("subinfo" => $this->getContext($context, "subinfo"))));
                    echo "
    </div>
    ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subinfo'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 350
                echo "
</div>

";
            } else {
                // line 354
                echo "
<p>
    <em>Aucune inscription à gérer.</em>
</p>

";
            }
            // line 360
            echo "
";
        }
        // line 362
        echo "
";
        // line 364
        echo "
<hr />

";
        // line 368
        echo "
<h3>Discussions</h3>

";
        // line 371
        if ((!$this->getContext($context, "admin"))) {
            // line 372
            echo "<p>
    Cet espace vous permet d'échanger de manière privée avec le correspondant
    formation. Pour poser une question ou ajouter une remarque concernant cette
    demande, ouvrez une nouvelle discussion à l'aide du formulaire ci-dessous.
    Les messages publiés ne seront visibles que par vous,
    le correspondant formation, et les autres personnes autorisées
    individuellement à participer à la discussion.
</p>
";
        }
        // line 381
        echo "
";
        // line 382
        $this->env->loadTemplate("LaplaceTrainingBundle:Thread:table.html.twig")->display($context);
        // line 383
        echo "
";
        // line 384
        echo         $this->env->getExtension('form')->renderer->renderBlock($this->getContext($context, "thread_form"), 'form_start');
        echo "

<fieldset>
    <legend>Ouvrir une nouvelle discussion</legend>

    ";
        // line 389
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "thread_form"), "thread"), 'rest');
        echo "

    ";
        // line 391
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "thread_form"), "message"), 'rest');
        echo "

    ";
        // line 393
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "thread_form"), 'rest');
        echo "

</fieldset>

";
        // line 397
        echo         $this->env->getExtension('form')->renderer->renderBlock($this->getContext($context, "thread_form"), 'form_end');
        echo "

";
        // line 400
        echo "
";
    }

    public function getTemplateName()
    {
        return "LaplaceTrainingBundle:Request:view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  828 => 400,  823 => 397,  816 => 393,  811 => 391,  806 => 389,  798 => 384,  795 => 383,  793 => 382,  790 => 381,  779 => 372,  777 => 371,  772 => 368,  767 => 364,  764 => 362,  760 => 360,  752 => 354,  746 => 350,  729 => 347,  722 => 346,  705 => 345,  699 => 341,  683 => 338,  681 => 337,  678 => 336,  673 => 335,  656 => 334,  642 => 322,  640 => 321,  637 => 320,  634 => 318,  630 => 316,  624 => 313,  618 => 310,  589 => 284,  586 => 283,  584 => 282,  581 => 281,  576 => 277,  570 => 274,  563 => 270,  550 => 260,  547 => 259,  545 => 258,  542 => 257,  537 => 253,  531 => 250,  524 => 246,  519 => 244,  514 => 242,  500 => 231,  497 => 230,  495 => 229,  492 => 228,  481 => 221,  458 => 206,  456 => 205,  450 => 202,  446 => 200,  439 => 195,  434 => 192,  428 => 188,  422 => 184,  416 => 180,  414 => 179,  400 => 170,  395 => 167,  390 => 165,  378 => 159,  375 => 158,  358 => 147,  353 => 144,  348 => 140,  332 => 134,  329 => 133,  316 => 128,  310 => 127,  301 => 122,  271 => 106,  250 => 95,  242 => 92,  216 => 82,  137 => 48,  81 => 13,  188 => 63,  334 => 148,  327 => 143,  315 => 137,  306 => 125,  284 => 122,  279 => 111,  274 => 119,  272 => 118,  262 => 114,  239 => 91,  206 => 85,  186 => 62,  280 => 103,  255 => 97,  245 => 88,  150 => 48,  487 => 224,  482 => 240,  475 => 236,  470 => 234,  465 => 232,  454 => 226,  452 => 225,  449 => 224,  438 => 215,  436 => 214,  426 => 208,  421 => 204,  418 => 202,  412 => 199,  406 => 196,  399 => 192,  396 => 191,  394 => 190,  391 => 189,  386 => 185,  380 => 160,  373 => 178,  363 => 149,  349 => 163,  346 => 162,  344 => 137,  336 => 156,  323 => 149,  307 => 136,  304 => 124,  302 => 129,  288 => 123,  281 => 122,  265 => 103,  263 => 110,  260 => 99,  251 => 89,  233 => 94,  223 => 89,  185 => 70,  170 => 66,  180 => 67,  174 => 65,  65 => 27,  234 => 89,  225 => 76,  237 => 90,  230 => 100,  221 => 84,  213 => 81,  207 => 54,  200 => 76,  110 => 32,  34 => 20,  335 => 135,  330 => 153,  325 => 131,  320 => 139,  318 => 101,  311 => 97,  308 => 96,  299 => 133,  297 => 121,  292 => 119,  289 => 91,  282 => 84,  275 => 100,  273 => 84,  259 => 107,  257 => 107,  249 => 102,  240 => 98,  238 => 77,  231 => 72,  222 => 75,  218 => 85,  211 => 80,  198 => 75,  194 => 75,  184 => 59,  179 => 71,  152 => 46,  77 => 7,  63 => 17,  58 => 23,  53 => 21,  227 => 99,  215 => 79,  210 => 77,  205 => 68,  226 => 86,  219 => 94,  195 => 74,  172 => 58,  202 => 62,  190 => 72,  146 => 50,  113 => 33,  59 => 23,  267 => 104,  261 => 125,  254 => 111,  244 => 106,  228 => 91,  212 => 70,  204 => 75,  197 => 88,  191 => 48,  175 => 69,  167 => 53,  159 => 49,  127 => 39,  118 => 27,  134 => 41,  129 => 50,  100 => 21,  178 => 58,  161 => 48,  104 => 20,  102 => 30,  23 => 3,  181 => 68,  153 => 53,  148 => 45,  124 => 36,  97 => 20,  90 => 15,  76 => 7,  480 => 162,  474 => 217,  469 => 158,  461 => 207,  457 => 227,  453 => 204,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 210,  427 => 143,  423 => 142,  413 => 134,  409 => 176,  407 => 131,  402 => 171,  398 => 129,  393 => 166,  387 => 122,  384 => 162,  381 => 120,  379 => 119,  374 => 116,  368 => 176,  365 => 150,  362 => 110,  360 => 148,  355 => 106,  341 => 160,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 120,  285 => 114,  283 => 88,  278 => 86,  268 => 117,  264 => 79,  258 => 113,  252 => 96,  247 => 94,  241 => 86,  235 => 95,  229 => 87,  224 => 85,  220 => 81,  214 => 74,  208 => 79,  169 => 60,  143 => 35,  140 => 49,  132 => 51,  128 => 29,  119 => 46,  107 => 28,  71 => 10,  177 => 60,  165 => 54,  160 => 62,  135 => 47,  126 => 41,  114 => 36,  84 => 18,  70 => 10,  67 => 15,  61 => 13,  28 => 5,  87 => 19,  201 => 78,  196 => 65,  183 => 91,  171 => 55,  166 => 60,  163 => 58,  158 => 67,  156 => 54,  151 => 52,  142 => 53,  138 => 34,  136 => 37,  121 => 37,  117 => 33,  105 => 30,  91 => 39,  62 => 25,  49 => 19,  31 => 8,  21 => 6,  25 => 8,  93 => 18,  88 => 16,  78 => 12,  44 => 10,  94 => 20,  89 => 12,  85 => 10,  75 => 53,  68 => 29,  56 => 22,  27 => 18,  38 => 15,  24 => 7,  46 => 18,  26 => 6,  19 => 2,  79 => 8,  72 => 52,  69 => 5,  47 => 17,  40 => 11,  37 => 11,  22 => 2,  246 => 107,  157 => 48,  145 => 39,  139 => 65,  131 => 40,  123 => 37,  120 => 37,  115 => 34,  111 => 25,  108 => 31,  101 => 20,  98 => 19,  96 => 16,  83 => 15,  74 => 14,  66 => 27,  55 => 17,  52 => 21,  50 => 20,  43 => 24,  41 => 16,  35 => 9,  32 => 11,  29 => 6,  209 => 87,  203 => 77,  199 => 71,  193 => 68,  189 => 66,  187 => 71,  182 => 60,  176 => 87,  173 => 54,  168 => 61,  164 => 41,  162 => 49,  154 => 47,  149 => 58,  147 => 44,  144 => 42,  141 => 43,  133 => 43,  130 => 43,  125 => 31,  122 => 35,  116 => 26,  112 => 31,  109 => 30,  106 => 26,  103 => 23,  99 => 17,  95 => 19,  92 => 17,  86 => 14,  82 => 9,  80 => 8,  73 => 30,  64 => 14,  60 => 20,  57 => 11,  54 => 27,  51 => 20,  48 => 11,  45 => 17,  42 => 13,  39 => 10,  36 => 5,  33 => 7,  30 => 10,);
    }
}
