<?php

/* LaplaceUserBundle:UserProfile:view-all.html.twig */
class __TwigTemplate_06403ee1d63ab60fded775c471649aed extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "

";
        // line 3
        $this->env->loadTemplate("LaplaceUserBundle:UserProfile:view-all.html.twig", "1436418835")->display(array_merge($context, array("page" => array(0 => "profile", 1 => "all"))));
    }

    public function getTemplateName()
    {
        return "LaplaceUserBundle:UserProfile:view-all.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  202 => 100,  190 => 95,  146 => 69,  113 => 52,  59 => 23,  267 => 127,  261 => 125,  254 => 123,  244 => 118,  228 => 107,  212 => 97,  204 => 92,  197 => 88,  191 => 85,  175 => 72,  167 => 67,  159 => 62,  127 => 60,  118 => 32,  134 => 37,  129 => 35,  100 => 20,  178 => 65,  161 => 82,  104 => 22,  102 => 48,  23 => 3,  181 => 63,  153 => 51,  148 => 42,  124 => 31,  97 => 17,  90 => 12,  76 => 7,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 144,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 112,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 105,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 90,  285 => 89,  283 => 88,  278 => 86,  268 => 85,  264 => 84,  258 => 81,  252 => 80,  247 => 78,  241 => 77,  235 => 111,  229 => 73,  224 => 71,  220 => 102,  214 => 69,  208 => 68,  169 => 60,  143 => 45,  140 => 55,  132 => 51,  128 => 49,  119 => 30,  107 => 25,  71 => 17,  177 => 65,  165 => 83,  160 => 61,  135 => 44,  126 => 34,  114 => 26,  84 => 35,  70 => 10,  67 => 15,  61 => 13,  28 => 3,  87 => 14,  201 => 92,  196 => 96,  183 => 91,  171 => 85,  166 => 58,  163 => 70,  158 => 67,  156 => 47,  151 => 57,  142 => 59,  138 => 38,  136 => 41,  121 => 46,  117 => 44,  105 => 49,  91 => 16,  62 => 23,  49 => 19,  31 => 5,  21 => 6,  25 => 8,  93 => 17,  88 => 15,  78 => 32,  44 => 16,  94 => 18,  89 => 12,  85 => 14,  75 => 23,  68 => 14,  56 => 9,  27 => 4,  38 => 6,  24 => 3,  46 => 7,  26 => 6,  19 => 1,  79 => 12,  72 => 28,  69 => 5,  47 => 17,  40 => 8,  37 => 14,  22 => 2,  246 => 119,  157 => 56,  145 => 46,  139 => 65,  131 => 39,  123 => 34,  120 => 56,  115 => 43,  111 => 25,  108 => 50,  101 => 23,  98 => 47,  96 => 31,  83 => 25,  74 => 14,  66 => 27,  55 => 15,  52 => 19,  50 => 10,  43 => 6,  41 => 15,  35 => 5,  32 => 4,  29 => 3,  209 => 82,  203 => 78,  199 => 89,  193 => 73,  189 => 71,  187 => 84,  182 => 66,  176 => 87,  173 => 62,  168 => 84,  164 => 52,  162 => 50,  154 => 54,  149 => 43,  147 => 58,  144 => 68,  141 => 51,  133 => 61,  130 => 35,  125 => 44,  122 => 31,  116 => 36,  112 => 29,  109 => 26,  106 => 36,  103 => 20,  99 => 19,  95 => 34,  92 => 14,  86 => 12,  82 => 34,  80 => 19,  73 => 19,  64 => 14,  60 => 6,  57 => 11,  54 => 10,  51 => 14,  48 => 8,  45 => 17,  42 => 7,  39 => 9,  36 => 5,  33 => 4,  30 => 7,);
    }
}


/* LaplaceUserBundle:UserProfile:view-all.html.twig */
class __TwigTemplate_06403ee1d63ab60fded775c471649aed_1436418835 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("LaplaceCommonBundle::admin-page.html.twig");

        $this->blocks = array(
            'PageTitle' => array($this, 'block_PageTitle'),
            'ContentTitle' => array($this, 'block_ContentTitle'),
            'ContentBody' => array($this, 'block_ContentBody'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "LaplaceCommonBundle::admin-page.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 6
    public function block_PageTitle($context, array $blocks = array())
    {
        $this->displayParentBlock("PageTitle", $context, $blocks);
        echo " - Tous les profils (";
        echo twig_escape_filter($this->env, $this->getContext($context, "requested"), "html", null, true);
        echo ")";
    }

    // line 8
    public function block_ContentTitle($context, array $blocks = array())
    {
        echo "Tous les profils de l'année ";
        echo twig_escape_filter($this->env, $this->getContext($context, "requested"), "html", null, true);
    }

    // line 12
    public function block_ContentBody($context, array $blocks = array())
    {
        // line 13
        echo "
";
        // line 14
        $this->env->loadTemplate("::pagination.html.twig")->display(array_merge($context, array("route_name" => ($this->getContext($context, "laplace_user") . "all_profiles"), "route_args" => array())));
        // line 16
        echo "
";
        // line 18
        $this->env->loadTemplate("LaplaceUserBundle:UserProfile:table.html.twig")->display(array_merge($context, array("show_role_admin" => true, "show_role_user" => true, "show_role_limited" => true)));
        // line 24
        echo "
";
        // line 25
        $this->env->loadTemplate("::pagination.html.twig")->display(array_merge($context, array("route_name" => ($this->getContext($context, "laplace_user") . "all_profiles"), "route_args" => array())));
        // line 27
        echo "

";
        // line 29
        echo         $this->env->getExtension('form')->renderer->renderBlock($this->getContext($context, "search_form"), 'form_start');
        echo "

<fieldset>

    <legend>Rechercher un utilisateur</legend>

    ";
        // line 35
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "search_form"), "user"), 'rest');
        echo "

    ";
        // line 37
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "search_form"), 'rest');
        echo "

</fieldset>

";
        // line 41
        echo         $this->env->getExtension('form')->renderer->renderBlock($this->getContext($context, "search_form"), 'form_end');
        echo "

";
    }

    public function getTemplateName()
    {
        return "LaplaceUserBundle:UserProfile:view-all.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  202 => 100,  190 => 95,  146 => 69,  113 => 52,  59 => 23,  267 => 127,  261 => 125,  254 => 123,  244 => 118,  228 => 107,  212 => 97,  204 => 92,  197 => 88,  191 => 85,  175 => 72,  167 => 67,  159 => 62,  127 => 60,  118 => 32,  134 => 37,  129 => 41,  100 => 20,  178 => 65,  161 => 82,  104 => 27,  102 => 25,  23 => 3,  181 => 63,  153 => 51,  148 => 42,  124 => 31,  97 => 18,  90 => 12,  76 => 7,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 144,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 112,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 105,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 90,  285 => 89,  283 => 88,  278 => 86,  268 => 85,  264 => 84,  258 => 81,  252 => 80,  247 => 78,  241 => 77,  235 => 111,  229 => 73,  224 => 71,  220 => 102,  214 => 69,  208 => 68,  169 => 60,  143 => 45,  140 => 55,  132 => 51,  128 => 49,  119 => 30,  107 => 25,  71 => 17,  177 => 65,  165 => 83,  160 => 61,  135 => 44,  126 => 34,  114 => 26,  84 => 35,  70 => 6,  67 => 15,  61 => 13,  28 => 3,  87 => 14,  201 => 92,  196 => 96,  183 => 91,  171 => 85,  166 => 58,  163 => 70,  158 => 67,  156 => 47,  151 => 57,  142 => 59,  138 => 38,  136 => 41,  121 => 46,  117 => 35,  105 => 49,  91 => 16,  62 => 23,  49 => 19,  31 => 5,  21 => 6,  25 => 8,  93 => 17,  88 => 15,  78 => 32,  44 => 16,  94 => 16,  89 => 13,  85 => 14,  75 => 23,  68 => 14,  56 => 9,  27 => 4,  38 => 6,  24 => 3,  46 => 7,  26 => 6,  19 => 1,  79 => 8,  72 => 28,  69 => 5,  47 => 17,  40 => 8,  37 => 14,  22 => 2,  246 => 119,  157 => 56,  145 => 46,  139 => 65,  131 => 39,  123 => 34,  120 => 56,  115 => 43,  111 => 25,  108 => 29,  101 => 23,  98 => 47,  96 => 31,  83 => 25,  74 => 14,  66 => 27,  55 => 15,  52 => 19,  50 => 10,  43 => 6,  41 => 15,  35 => 5,  32 => 4,  29 => 3,  209 => 82,  203 => 78,  199 => 89,  193 => 73,  189 => 71,  187 => 84,  182 => 66,  176 => 87,  173 => 62,  168 => 84,  164 => 52,  162 => 50,  154 => 54,  149 => 43,  147 => 58,  144 => 68,  141 => 51,  133 => 61,  130 => 35,  125 => 44,  122 => 37,  116 => 36,  112 => 29,  109 => 26,  106 => 36,  103 => 20,  99 => 24,  95 => 34,  92 => 14,  86 => 12,  82 => 34,  80 => 19,  73 => 19,  64 => 14,  60 => 6,  57 => 11,  54 => 10,  51 => 14,  48 => 8,  45 => 17,  42 => 7,  39 => 9,  36 => 5,  33 => 4,  30 => 7,);
    }
}
