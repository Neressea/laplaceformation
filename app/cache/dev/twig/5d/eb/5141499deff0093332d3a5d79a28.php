<?php

/* WebProfilerBundle:Collector:logger.html.twig */
class __TwigTemplate_5deb5141499deff0093332d3a5d79a28 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("@WebProfiler/Profiler/layout.html.twig");

        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        $context["logger"] = $this;
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_toolbar($context, array $blocks = array())
    {
        // line 6
        echo "    ";
        if (($this->getAttribute($this->getContext($context, "collector"), "counterrors") || $this->getAttribute($this->getContext($context, "collector"), "countdeprecations"))) {
            // line 7
            echo "        ";
            ob_start();
            // line 8
            echo "            <img width=\"15\" height=\"28\" alt=\"Logs\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAcCAYAAABoMT8aAAAA4klEQVQ4y2P4//8/AyWYYXgYwOPp6Xnc3t7+P7EYpB6k7+zZs2ADNEjRjIwDAgKWgAywIUfz8+fPVzg7O/8AGeCATQEQnAfi/SAah/wcV1dXvAYUgORANA75ehcXl+/4DHAABRIe+ZrhbgAhTHsDiEgHBA0glA6GfSDiw5mZma+A+sphBlhVVFQ88vHx+Xfu3Ll7QP5haOjjwtuAuGHv3r3NIMNABqh8+/atsaur666vr+9XUlwSHx//AGQANxCbAnEWyGQicRMQ9wBxIQM0qjiBWAFqkB00/glhayBWHwb1AgB38EJsUtxtWwAAAABJRU5ErkJggg==\">
            ";
            // line 9
            if ($this->getAttribute($this->getContext($context, "collector"), "counterrors")) {
                // line 10
                echo "                ";
                $context["status_color"] = "red";
                // line 11
                echo "            ";
            } else {
                // line 12
                echo "                ";
                $context["status_color"] = "yellow";
                // line 13
                echo "            ";
            }
            // line 14
            echo "            ";
            $context["error_count"] = ($this->getAttribute($this->getContext($context, "collector"), "counterrors") + $this->getAttribute($this->getContext($context, "collector"), "countdeprecations"));
            // line 15
            echo "            <span class=\"sf-toolbar-status sf-toolbar-status-";
            echo twig_escape_filter($this->env, $this->getContext($context, "status_color"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getContext($context, "error_count"), "html", null, true);
            echo "</span>
        ";
            $context["icon"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
            // line 17
            echo "        ";
            ob_start();
            // line 18
            echo "            ";
            if ($this->getAttribute($this->getContext($context, "collector"), "counterrors")) {
                // line 19
                echo "                <div class=\"sf-toolbar-info-piece\">
                    <b>Exception</b>
                    <span class=\"sf-toolbar-status sf-toolbar-status-red\">";
                // line 21
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "collector"), "counterrors"), "html", null, true);
                echo "</span>
                </div>
            ";
            }
            // line 24
            echo "            ";
            if ($this->getAttribute($this->getContext($context, "collector"), "countdeprecations")) {
                // line 25
                echo "                <div class=\"sf-toolbar-info-piece\">
                    <b>Deprecated Calls</b>
                    <span class=\"sf-toolbar-status sf-toolbar-status-yellow\">";
                // line 27
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "collector"), "countdeprecations"), "html", null, true);
                echo "</span>
                </div>
            ";
            }
            // line 30
            echo "        ";
            $context["text"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
            // line 31
            echo "        ";
            $this->env->loadTemplate("@WebProfiler/Profiler/toolbar_item.html.twig")->display(array_merge($context, array("link" => $this->getContext($context, "profiler_url"))));
            // line 32
            echo "    ";
        }
    }

    // line 35
    public function block_menu($context, array $blocks = array())
    {
        // line 36
        echo "<span class=\"label\">
    <span class=\"icon\"><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABoAAAAgCAYAAAAMq2gFAAABjElEQVRIx2MIDw+vd3R0/GFvb/+fGtjFxeVJSUmJ1f///5nv37/PAMMMzs7OVLMEhoODgy/k5+cHJCYmagAtZAJbRG1L0DEwxCYALeOgiUXbt2+/X1NT8xTEdnd3/wi0SI4mFgHBDCBeCLXoF5BtwkCEpvNAvB8JnydCTwgQR0It+g1kWxNjUQEQOyDhAiL0gNUiWWRDjEUOyMkUZsCoRaMWjVpEvEVkFkGjFmEUqgc+fvx4hVYWIReqzi9evKileaoDslnu3LkTNLQtGk3edLPIycnpL9Bge5pb1NXVdQNosDmGRcAm7F+QgKur6783b95cBQoeRGv1kII3QPOdAoZF8+fPP4PUqnx55syZVKCEI1rLh1hsAbWEZ8aMGaUoFoFcMG3atKdIjfSPISEhawICAlaQgwMDA1f6+/sfB5rzE2Sej4/PD3C7DkjoAHHVoUOHLpSVlX3w8vL6Sa34Alr6Z8WKFaCoMARZxAHEoFZ/HBD3A/FyIF4BxMvIxCC964F4G6hZDMTxQCwJAGWE8pur5kFDAAAAAElFTkSuQmCC\" alt=\"Logger\"></span>
    <strong>Logs</strong>
    ";
        // line 39
        if (($this->getAttribute($this->getContext($context, "collector"), "counterrors") || $this->getAttribute($this->getContext($context, "collector"), "countdeprecations"))) {
            // line 40
            echo "        ";
            $context["error_count"] = ($this->getAttribute($this->getContext($context, "collector"), "counterrors") + $this->getAttribute($this->getContext($context, "collector"), "countdeprecations"));
            // line 41
            echo "        <span class=\"count\">
            <span>";
            // line 42
            echo twig_escape_filter($this->env, $this->getContext($context, "error_count"), "html", null, true);
            echo "</span>
        </span>
    ";
        }
        // line 45
        echo "</span>
";
    }

    // line 48
    public function block_panel($context, array $blocks = array())
    {
        // line 49
        echo "    <h2>Logs</h2>

    ";
        // line 51
        $context["priority"] = $this->getAttribute($this->getAttribute($this->getContext($context, "request"), "query"), "get", array(0 => "priority", 1 => 0), "method");
        // line 52
        echo "
    <table>
        <tr>
            <th>Filter</th>
            <td>
                <form id=\"priority-form\" action=\"\" method=\"get\" style=\"display: inline\">
                    <input type=\"hidden\" name=\"panel\" value=\"logger\">
                    <label for=\"priority\">Priority</label>
                    <select id=\"priority\" name=\"priority\" onchange=\"document.getElementById('priority-form').submit(); \">
                        ";
        // line 62
        echo "                        ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable(array(100 => "DEBUG", 200 => "INFO", 250 => "NOTICE", 300 => "WARNING", 400 => "ERROR", 500 => "CRITICAL", 550 => "ALERT", 600 => "EMERGENCY", "-100" => "DEPRECATION only"));
        foreach ($context['_seq'] as $context["value"] => $context["text"]) {
            // line 63
            echo "                            <option value=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "value"), "html", null, true);
            echo "\"";
            echo ((($this->getContext($context, "value") == $this->getContext($context, "priority"))) ? (" selected") : (""));
            echo ">";
            echo twig_escape_filter($this->env, $this->getContext($context, "text"), "html", null, true);
            echo "</option>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['value'], $context['text'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 65
        echo "                    </select>
                    <noscript>
                        <input type=\"submit\" value=\"refresh\">
                    </noscript>
                </form>
            </td>
        </tr>
    </table>

    ";
        // line 74
        if ($this->getAttribute($this->getContext($context, "collector"), "logs")) {
            // line 75
            echo "        <ul class=\"alt\">
            ";
            // line 76
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "collector"), "logs"));
            $context['_iterated'] = false;
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            foreach ($context['_seq'] as $context["_key"] => $context["log"]) {
                if (((($this->getContext($context, "priority") >= 0) && ($this->getAttribute($this->getContext($context, "log"), "priority") >= $this->getContext($context, "priority"))) || (($this->getContext($context, "priority") < 0) && ((($this->getAttribute($this->getAttribute($this->getContext($context, "log", true), "context", array(), "any", false, true), "type", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute($this->getContext($context, "log", true), "context", array(), "any", false, true), "type"), 0)) : (0)) == $this->getContext($context, "priority"))))) {
                    // line 77
                    echo "                <li class=\"";
                    echo twig_escape_filter($this->env, twig_cycle(array(0 => "odd", 1 => "even"), $this->getAttribute($this->getContext($context, "loop"), "index")), "html", null, true);
                    if (($this->getAttribute($this->getContext($context, "log"), "priority") >= 400)) {
                        echo " error";
                    } elseif (($this->getAttribute($this->getContext($context, "log"), "priority") >= 300)) {
                        echo " warning";
                    }
                    echo "\">
                    ";
                    // line 78
                    echo $context["logger"]->getdisplay_message($this->getAttribute($this->getContext($context, "loop"), "index"), $this->getContext($context, "log"));
                    echo "
                </li>
            ";
                    $context['_iterated'] = true;
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                }
            }
            if (!$context['_iterated']) {
                // line 81
                echo "                <li><em>No logs available for this priority.</em></li>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['log'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 83
            echo "        </ul>
    ";
        } else {
            // line 85
            echo "        <p>
            <em>No logs available.</em>
        </p>
    ";
        }
    }

    // line 92
    public function getdisplay_message($_log_index = null, $_log = null)
    {
        $context = $this->env->mergeGlobals(array(
            "log_index" => $_log_index,
            "log" => $_log,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 93
            echo "    ";
            if ((twig_constant("Symfony\\Component\\HttpKernel\\Debug\\ErrorHandler::TYPE_DEPRECATION") == (($this->getAttribute($this->getAttribute($this->getContext($context, "log", true), "context", array(), "any", false, true), "type", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute($this->getContext($context, "log", true), "context", array(), "any", false, true), "type"), 0)) : (0)))) {
                // line 94
                echo "        DEPRECATION -  ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "log"), "message"), "html", null, true);
                echo "
        ";
                // line 95
                $context["id"] = ("sf-call-stack-" . $this->getContext($context, "log_index"));
                // line 96
                echo "        <a href=\"#\" onclick=\"Sfjs.toggle('";
                echo twig_escape_filter($this->env, $this->getContext($context, "id"), "html", null, true);
                echo "', document.getElementById('";
                echo twig_escape_filter($this->env, $this->getContext($context, "id"), "html", null, true);
                echo "-on'), document.getElementById('";
                echo twig_escape_filter($this->env, $this->getContext($context, "id"), "html", null, true);
                echo "-off')); return false;\">
            <img class=\"toggle\" id=\"";
                // line 97
                echo twig_escape_filter($this->env, $this->getContext($context, "id"), "html", null, true);
                echo "-off\" alt=\"-\" src=\"data:image/gif;base64,R0lGODlhEgASAMQSANft94TG57Hb8GS44ez1+mC24IvK6ePx+Wa44dXs92+942e54o3L6W2844/M6dnu+P/+/l614P///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAEAABIALAAAAAASABIAQAVCoCQBTBOd6Kk4gJhGBCTPxysJb44K0qD/ER/wlxjmisZkMqBEBW5NHrMZmVKvv9hMVsO+hE0EoNAstEYGxG9heIhCADs=\" style=\"display:none\">
            <img class=\"toggle\" id=\"";
                // line 98
                echo twig_escape_filter($this->env, $this->getContext($context, "id"), "html", null, true);
                echo "-on\" alt=\"+\" src=\"data:image/gif;base64,R0lGODlhEgASAMQTANft99/v+Ga44bHb8ITG52S44dXs9+z1+uPx+YvK6WC24G+944/M6W28443L6dnu+Ge54v/+/l614P///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAEAABMALAAAAAASABIAQAVS4DQBTiOd6LkwgJgeUSzHSDoNaZ4PU6FLgYBA5/vFID/DbylRGiNIZu74I0h1hNsVxbNuUV4d9SsZM2EzWe1qThVzwWFOAFCQFa1RQq6DJB4iIQA7\" style=\"display:inline\">
        </a>
        ";
                // line 100
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getContext($context, "log"), "context"), "stack"));
                foreach ($context['_seq'] as $context["index"] => $context["call"]) {
                    if (($this->getContext($context, "index") > 1)) {
                        // line 101
                        echo "            ";
                        if (($this->getContext($context, "index") == 2)) {
                            // line 102
                            echo "                <ul class=\"sf-call-stack\" id=\"";
                            echo twig_escape_filter($this->env, $this->getContext($context, "id"), "html", null, true);
                            echo "\" style=\"display: none\">
            ";
                        }
                        // line 104
                        echo "            ";
                        if ($this->getAttribute($this->getContext($context, "call", true), "class", array(), "any", true, true)) {
                            // line 105
                            echo "                ";
                            $context["from"] = (($this->env->getExtension('code')->abbrClass($this->getAttribute($this->getContext($context, "call"), "class")) . "::") . $this->env->getExtension('code')->abbrMethod($this->getAttribute($this->getContext($context, "call"), "function")));
                            // line 106
                            echo "            ";
                        } elseif ($this->getAttribute($this->getContext($context, "call", true), "function", array(), "any", true, true)) {
                            // line 107
                            echo "                ";
                            $context["from"] = $this->env->getExtension('code')->abbrMethod($this->getAttribute($this->getContext($context, "call"), "function"));
                            // line 108
                            echo "            ";
                        } elseif ($this->getAttribute($this->getContext($context, "call", true), "file", array(), "any", true, true)) {
                            // line 109
                            echo "                ";
                            $context["from"] = $this->getAttribute($this->getContext($context, "call"), "file");
                            // line 110
                            echo "            ";
                        } else {
                            // line 111
                            echo "                ";
                            $context["from"] = "-";
                            // line 112
                            echo "            ";
                        }
                        // line 113
                        echo "
            <li>Called from ";
                        // line 114
                        echo ((($this->getAttribute($this->getContext($context, "call", true), "file", array(), "any", true, true) && $this->getAttribute($this->getContext($context, "call", true), "line", array(), "any", true, true))) ? ($this->env->getExtension('code')->formatFile($this->getAttribute($this->getContext($context, "call"), "file"), $this->getAttribute($this->getContext($context, "call"), "line"), $this->getContext($context, "from"))) : ($this->getContext($context, "from")));
                        echo "</li>

            ";
                        // line 116
                        echo ((($this->getContext($context, "index") == (twig_length_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "log"), "context"), "stack")) - 1))) ? ("</ul>") : (""));
                        echo "
        ";
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['index'], $context['call'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 118
                echo "    ";
            } else {
                // line 119
                echo "        ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "log"), "priorityName"), "html", null, true);
                echo " - ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "log"), "message"), "html", null, true);
                echo "
        ";
                // line 120
                if (($this->getAttribute($this->getContext($context, "log", true), "context", array(), "any", true, true) && (!twig_test_empty($this->getAttribute($this->getContext($context, "log"), "context"))))) {
                    // line 121
                    echo "            <br />
            <small>
                <strong>Context</strong>: ";
                    // line 123
                    echo twig_escape_filter($this->env, twig_jsonencode_filter($this->getAttribute($this->getContext($context, "log"), "context"), (64 | 256)), "html", null, true);
                    echo "
            </small>
        ";
                }
                // line 126
                echo "    ";
            }
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:logger.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  357 => 123,  324 => 113,  291 => 102,  431 => 189,  429 => 188,  415 => 180,  408 => 176,  401 => 172,  351 => 120,  338 => 135,  300 => 105,  270 => 102,  256 => 96,  389 => 160,  371 => 156,  361 => 146,  345 => 147,  340 => 145,  331 => 140,  328 => 139,  326 => 138,  321 => 112,  296 => 121,  293 => 120,  290 => 119,  276 => 111,  269 => 107,  253 => 100,  792 => 485,  788 => 484,  784 => 482,  771 => 481,  706 => 472,  702 => 470,  694 => 468,  690 => 467,  686 => 466,  682 => 465,  675 => 463,  621 => 452,  616 => 450,  602 => 449,  565 => 414,  525 => 408,  520 => 406,  515 => 404,  356 => 328,  342 => 137,  339 => 316,  20 => 1,  892 => 350,  888 => 348,  879 => 346,  875 => 345,  871 => 343,  868 => 342,  865 => 341,  860 => 338,  854 => 336,  852 => 335,  829 => 334,  826 => 333,  820 => 330,  817 => 329,  814 => 328,  808 => 326,  803 => 487,  800 => 323,  791 => 314,  787 => 313,  783 => 312,  773 => 307,  770 => 306,  763 => 301,  757 => 299,  749 => 293,  745 => 476,  742 => 475,  732 => 282,  727 => 280,  723 => 473,  707 => 278,  704 => 277,  701 => 275,  698 => 469,  695 => 273,  693 => 272,  687 => 268,  684 => 267,  679 => 264,  672 => 261,  668 => 259,  666 => 258,  651 => 257,  648 => 256,  645 => 460,  639 => 252,  636 => 251,  631 => 249,  628 => 248,  625 => 453,  622 => 246,  620 => 245,  617 => 244,  614 => 243,  605 => 237,  601 => 236,  595 => 233,  591 => 231,  588 => 230,  578 => 224,  575 => 223,  568 => 218,  558 => 217,  553 => 216,  544 => 213,  541 => 212,  538 => 211,  535 => 210,  532 => 209,  530 => 410,  527 => 409,  522 => 204,  516 => 200,  513 => 198,  512 => 197,  511 => 196,  507 => 195,  504 => 194,  498 => 192,  489 => 189,  484 => 187,  473 => 110,  468 => 108,  462 => 202,  447 => 100,  441 => 196,  433 => 95,  404 => 85,  397 => 82,  369 => 78,  367 => 155,  364 => 76,  352 => 68,  343 => 146,  333 => 62,  313 => 58,  303 => 106,  295 => 275,  286 => 112,  248 => 94,  243 => 92,  232 => 88,  236 => 80,  155 => 47,  828 => 400,  823 => 331,  816 => 393,  811 => 327,  806 => 488,  798 => 384,  795 => 383,  793 => 382,  790 => 381,  779 => 372,  777 => 309,  772 => 368,  767 => 364,  764 => 362,  760 => 300,  752 => 354,  746 => 350,  729 => 347,  722 => 346,  705 => 345,  699 => 341,  683 => 338,  681 => 337,  678 => 464,  673 => 462,  656 => 461,  642 => 253,  640 => 321,  637 => 320,  634 => 250,  630 => 455,  624 => 313,  618 => 451,  589 => 284,  586 => 283,  584 => 282,  581 => 225,  576 => 277,  570 => 274,  563 => 270,  550 => 215,  547 => 411,  545 => 258,  542 => 257,  537 => 253,  531 => 250,  524 => 246,  519 => 244,  514 => 199,  500 => 231,  497 => 230,  495 => 191,  492 => 190,  481 => 186,  458 => 206,  456 => 205,  450 => 101,  446 => 197,  439 => 195,  434 => 192,  428 => 188,  422 => 184,  416 => 87,  414 => 179,  400 => 170,  395 => 81,  390 => 80,  378 => 157,  375 => 158,  358 => 151,  353 => 121,  348 => 140,  332 => 116,  329 => 131,  316 => 128,  310 => 127,  301 => 55,  271 => 47,  250 => 95,  242 => 92,  216 => 79,  137 => 44,  81 => 23,  188 => 90,  334 => 141,  327 => 114,  315 => 110,  306 => 107,  284 => 122,  279 => 111,  274 => 97,  272 => 97,  262 => 98,  239 => 81,  206 => 102,  186 => 92,  280 => 103,  255 => 93,  245 => 88,  150 => 55,  487 => 188,  482 => 240,  475 => 236,  470 => 109,  465 => 107,  454 => 226,  452 => 225,  449 => 198,  438 => 215,  436 => 96,  426 => 208,  421 => 204,  418 => 202,  412 => 199,  406 => 196,  399 => 192,  396 => 191,  394 => 168,  391 => 189,  386 => 159,  380 => 160,  373 => 156,  363 => 126,  349 => 67,  346 => 162,  344 => 119,  336 => 63,  323 => 128,  307 => 128,  304 => 124,  302 => 125,  288 => 101,  281 => 114,  265 => 96,  263 => 95,  260 => 43,  251 => 40,  233 => 87,  223 => 110,  185 => 74,  170 => 56,  180 => 13,  174 => 65,  65 => 11,  234 => 89,  225 => 76,  237 => 35,  230 => 100,  221 => 27,  213 => 78,  207 => 75,  200 => 72,  110 => 21,  34 => 18,  335 => 134,  330 => 61,  325 => 129,  320 => 127,  318 => 111,  311 => 97,  308 => 96,  299 => 133,  297 => 104,  292 => 119,  289 => 113,  282 => 99,  275 => 105,  273 => 84,  259 => 103,  257 => 42,  249 => 102,  240 => 36,  238 => 82,  231 => 83,  222 => 83,  218 => 85,  211 => 69,  198 => 20,  194 => 70,  184 => 63,  179 => 88,  152 => 46,  77 => 28,  63 => 21,  58 => 14,  53 => 12,  227 => 86,  215 => 79,  210 => 77,  205 => 68,  226 => 84,  219 => 108,  195 => 19,  172 => 62,  202 => 77,  190 => 76,  146 => 56,  113 => 38,  59 => 14,  267 => 101,  261 => 94,  254 => 41,  244 => 136,  228 => 91,  212 => 78,  204 => 75,  197 => 71,  191 => 69,  175 => 58,  167 => 82,  159 => 79,  127 => 35,  118 => 49,  134 => 47,  129 => 297,  100 => 39,  178 => 64,  161 => 63,  104 => 32,  102 => 33,  23 => 3,  181 => 65,  153 => 56,  148 => 45,  124 => 61,  97 => 29,  90 => 27,  76 => 17,  480 => 162,  474 => 217,  469 => 158,  461 => 207,  457 => 227,  453 => 199,  444 => 99,  440 => 148,  437 => 147,  435 => 146,  430 => 210,  427 => 143,  423 => 142,  413 => 134,  409 => 176,  407 => 131,  402 => 171,  398 => 129,  393 => 166,  387 => 164,  384 => 162,  381 => 120,  379 => 119,  374 => 116,  368 => 176,  365 => 150,  362 => 110,  360 => 148,  355 => 143,  341 => 118,  337 => 103,  322 => 101,  314 => 99,  312 => 109,  309 => 108,  305 => 95,  298 => 120,  294 => 103,  285 => 114,  283 => 100,  278 => 98,  268 => 46,  264 => 79,  258 => 94,  252 => 96,  247 => 94,  241 => 90,  235 => 85,  229 => 85,  224 => 81,  220 => 81,  214 => 74,  208 => 76,  169 => 60,  143 => 51,  140 => 58,  132 => 54,  128 => 27,  119 => 40,  107 => 20,  71 => 13,  177 => 49,  165 => 60,  160 => 62,  135 => 47,  126 => 41,  114 => 49,  84 => 24,  70 => 19,  67 => 18,  61 => 15,  28 => 3,  87 => 34,  201 => 21,  196 => 92,  183 => 90,  171 => 84,  166 => 54,  163 => 53,  158 => 62,  156 => 62,  151 => 59,  142 => 70,  138 => 306,  136 => 48,  121 => 50,  117 => 39,  105 => 34,  91 => 37,  62 => 27,  49 => 11,  31 => 8,  21 => 2,  25 => 8,  93 => 29,  88 => 25,  78 => 18,  44 => 9,  94 => 21,  89 => 30,  85 => 24,  75 => 19,  68 => 30,  56 => 16,  27 => 3,  38 => 7,  24 => 3,  46 => 10,  26 => 3,  19 => 1,  79 => 21,  72 => 18,  69 => 17,  47 => 8,  40 => 8,  37 => 6,  22 => 2,  246 => 93,  157 => 77,  145 => 52,  139 => 49,  131 => 45,  123 => 42,  120 => 31,  115 => 56,  111 => 47,  108 => 31,  101 => 31,  98 => 30,  96 => 30,  83 => 33,  74 => 35,  66 => 25,  55 => 13,  52 => 12,  50 => 22,  43 => 9,  41 => 8,  35 => 6,  32 => 5,  29 => 3,  209 => 103,  203 => 73,  199 => 93,  193 => 95,  189 => 66,  187 => 75,  182 => 87,  176 => 63,  173 => 85,  168 => 61,  164 => 81,  162 => 59,  154 => 60,  149 => 340,  147 => 54,  144 => 42,  141 => 51,  133 => 66,  130 => 46,  125 => 42,  122 => 41,  116 => 39,  112 => 36,  109 => 35,  106 => 51,  103 => 48,  99 => 31,  95 => 34,  92 => 27,  86 => 10,  82 => 19,  80 => 32,  73 => 20,  64 => 17,  60 => 20,  57 => 39,  54 => 19,  51 => 37,  48 => 16,  45 => 10,  42 => 7,  39 => 6,  36 => 5,  33 => 4,  30 => 3,);
    }
}
