<?php

/* WebProfilerBundle:Profiler:toolbar.html.twig */
class __TwigTemplate_3ef3ecc35c63a51459d36f1b17397217 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!-- START of Symfony2 Web Debug Toolbar -->
";
        // line 2
        if (("normal" != $this->getContext($context, "position"))) {
            // line 3
            echo "    <style>
        ";
            // line 4
            $this->env->loadTemplate("@WebProfiler/Profiler/toolbar.css.twig")->display(array_merge($context, array("position" => $this->getContext($context, "position"), "floatable" => true)));
            // line 5
            echo "    </style>
    <div id=\"sfMiniToolbar-";
            // line 6
            echo twig_escape_filter($this->env, $this->getContext($context, "token"), "html", null, true);
            echo "\" class=\"sf-minitoolbar\">
        <a href=\"javascript:void(0);\" title=\"Show Symfony toolbar\" onclick=\"
            var elem = this.parentNode;
            if (elem.style.display == 'none') {
                document.getElementById('sfToolbarMainContent-";
            // line 10
            echo twig_escape_filter($this->env, $this->getContext($context, "token"), "html", null, true);
            echo "').style.display = 'none';
                document.getElementById('sfToolbarClearer-";
            // line 11
            echo twig_escape_filter($this->env, $this->getContext($context, "token"), "html", null, true);
            echo "').style.display = 'none';
                elem.style.display = 'block';
            } else {
                document.getElementById('sfToolbarMainContent-";
            // line 14
            echo twig_escape_filter($this->env, $this->getContext($context, "token"), "html", null, true);
            echo "').style.display = 'block';
                document.getElementById('sfToolbarClearer-";
            // line 15
            echo twig_escape_filter($this->env, $this->getContext($context, "token"), "html", null, true);
            echo "').style.display = 'block';
                elem.style.display = 'none'
            }

            Sfjs.setPreference('toolbar/displayState', 'block');
        \">
            <img width=\"26\" height=\"28\" alt=\"Symfony\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABoAAAAcCAMAAABIzV/hAAACZFBMVEUwLjL///////////////////////////////////////////////////////////////////+Eg4b///+Ni46Xlpj///////////+op6n///////////////////////////////////////////////////////////9ZWFv///////9qaWz///////+mpaf///////////////9ZWFv///////////////9PTVH///91dHb////////////////////g4OD///9NTE+Ih4r///////+Ni47///////92dHeRkJLk5OTLy8xlY2b///////+cm53///////+5ubr////o6Oj////////U1NT///9DQURsa22rq6ysq61hX2L///+LioxTUVVBP0NEQkZpZ2rGxsf///9ram3////s7O2SkZNfXmFxcHKmpae4uLnKysuXlpiop6l3dXiIh4pYVlmrq6ycm52trK7Nzc48Oj5dW158e36dnJ49Oz/Pz9BiYGPAv8BDQUTQz9BVU1aioaNHRUnBwcJXVVk6ODxJR0t3dnmko6U8Oj6lpKY9Oz+0tLXDwsRQTlF7en1QTlHi4eJhX2LFxcZTUVViYGNwb3J+fX83NTlFQ0dUUlW4t7icm524uLk8Oj5YVlmPjpBLSU2enZ9aWVw/PkFBP0NdW153dnk0MjZQTlE1MzdQT1JdXF9ram15eHqGhYdDQkV5eHo2NThEQkZRUFNFQ0dta244NjpGREhTUVU5NztUUlVhX2JubG9HRUlVU1ZiYGM7OTxIRkk7OT1IR0o8Oj4wLjI9Oz8YdG13AAAAynRSTlMAAAEDBAUGCgsMDQ4QEhMUGRobGx0gISIkJiYnKCktLi8wMjM0NTk6Ozw+P0NFSEpLTE5PUFBTWlteXmBiZGVmaWxtcHBxc3R0dnl5fX+BgoOGi46Pj5CRmZqanZ6eoKeoq6ytsLCwsrO0tbe5urq8vL+/wsTFx8jJycvLy8vM0NHR0tLU1NfX2NnZ2trc3N3d3eHh4uLl5ebm5ubn5+fo6urt7e3u7vDx8/Pz9PT19fX19fX29vf39/j4+fn5+vr6+vv7+/z8/f3+yR5EtwAAAbVJREFUeNpl0mVXVFEYhuF3zhkOFqMjYmCi2MUYYKGIYiJ2YCd2t4IBFqgoKjZ2jg3igI2KyO2f8sTMngGvj/te71r7wyMuk4jofZccAihcMzJKXDYnuYcVotyeYKiktV5LA0faaE7S4s7TyMsBupnMcoH/vO6gmanJaiLV1Py+Xwn5zc0fjCbSi2LI2QdkGdLyFBG+rHwMzz4BD7wyGEfFk8pfsD2TkGmyFNvFHfDj55v02VD6DcteOYylatY8oG7boA2QV4vlklzB8tU3/DIwo+dWv58guY5tRcLMtwSGdi1DkTvAn9Jqsgbu4kafRBqlV4sDFCWuIjdhMsp7yQU49rB28/QPLOu2DuWqLMfmfw6M716GskdGVRMSGDKRsEzpvZ+Qs0lFKDd94s2oArh2F7K3oNQt6ChGr5x6+Dx3J7d2E3Ygqam4PCkngRNnngYIO5cWq4lLb5t+vJ6GiifFR1nbMNqP3fSOCN8PZnQynEW5W/nmHy0PXv79eHpRSjsjtEOtWZcRc9YXlNx7VFKwceGYHjG6s0Ob3iK+X3LqlKnjkvt39rjD6w3W6BhvrCdaVw//ADrWicJIvtkmAAAAAElFTkSuQmCC\">
        </a>
    </div>
    <div id=\"sfToolbarClearer-";
            // line 24
            echo twig_escape_filter($this->env, $this->getContext($context, "token"), "html", null, true);
            echo "\" style=\"clear: both; height: 38px;\"></div>
";
        }
        // line 26
        echo "
<div id=\"sfToolbarMainContent-";
        // line 27
        echo twig_escape_filter($this->env, $this->getContext($context, "token"), "html", null, true);
        echo "\" class=\"sf-toolbarreset\">
    ";
        // line 28
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "templates"));
        foreach ($context['_seq'] as $context["name"] => $context["template"]) {
            // line 29
            echo "        ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "template"), "renderblock", array(0 => "toolbar", 1 => array("collector" => $this->getAttribute($this->getContext($context, "profile"), "getcollector", array(0 => $this->getContext($context, "name")), "method"), "profiler_url" => $this->getContext($context, "profiler_url"), "token" => $this->getAttribute($this->getContext($context, "profile"), "token"), "name" => $this->getContext($context, "name"))), "method"), "html", null, true);
            // line 35
            echo "
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['name'], $context['template'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 37
        echo "
    ";
        // line 38
        if (("normal" != $this->getContext($context, "position"))) {
            // line 39
            echo "        <a class=\"hide-button\" title=\"Close Toolbar\" onclick=\"
            var p = this.parentNode;
            p.style.display = 'none';
            (p.previousElementSibling || p.previousSibling).style.display = 'none';
            document.getElementById('sfMiniToolbar-";
            // line 43
            echo twig_escape_filter($this->env, $this->getContext($context, "token"), "html", null, true);
            echo "').style.display = 'block';
            Sfjs.setPreference('toolbar/displayState', 'none');
        \"></a>
    ";
        }
        // line 47
        echo "</div>
<!-- END of Symfony2 Web Debug Toolbar -->
";
    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:toolbar.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  20 => 1,  892 => 350,  888 => 348,  879 => 346,  875 => 345,  871 => 343,  868 => 342,  865 => 341,  860 => 338,  854 => 336,  852 => 335,  829 => 334,  826 => 333,  820 => 330,  817 => 329,  814 => 328,  808 => 326,  803 => 324,  800 => 323,  791 => 314,  787 => 313,  783 => 312,  773 => 307,  770 => 306,  763 => 301,  757 => 299,  749 => 293,  745 => 291,  742 => 290,  732 => 282,  727 => 280,  723 => 279,  707 => 278,  704 => 277,  701 => 275,  698 => 274,  695 => 273,  693 => 272,  687 => 268,  684 => 267,  679 => 264,  672 => 261,  668 => 259,  666 => 258,  651 => 257,  648 => 256,  645 => 255,  639 => 252,  636 => 251,  631 => 249,  628 => 248,  625 => 247,  622 => 246,  620 => 245,  617 => 244,  614 => 243,  605 => 237,  601 => 236,  595 => 233,  591 => 231,  588 => 230,  578 => 224,  575 => 223,  568 => 218,  558 => 217,  553 => 216,  544 => 213,  541 => 212,  538 => 211,  535 => 210,  532 => 209,  530 => 208,  527 => 207,  522 => 204,  516 => 200,  513 => 198,  512 => 197,  511 => 196,  507 => 195,  504 => 194,  498 => 192,  489 => 189,  484 => 187,  473 => 110,  468 => 108,  462 => 106,  447 => 100,  441 => 98,  433 => 95,  404 => 85,  397 => 82,  369 => 78,  367 => 77,  364 => 76,  352 => 68,  343 => 65,  333 => 62,  313 => 58,  303 => 56,  295 => 53,  286 => 48,  248 => 39,  243 => 37,  232 => 80,  236 => 80,  155 => 50,  828 => 400,  823 => 331,  816 => 393,  811 => 327,  806 => 325,  798 => 384,  795 => 383,  793 => 382,  790 => 381,  779 => 372,  777 => 309,  772 => 368,  767 => 364,  764 => 362,  760 => 300,  752 => 354,  746 => 350,  729 => 347,  722 => 346,  705 => 345,  699 => 341,  683 => 338,  681 => 337,  678 => 336,  673 => 335,  656 => 334,  642 => 253,  640 => 321,  637 => 320,  634 => 250,  630 => 316,  624 => 313,  618 => 310,  589 => 284,  586 => 283,  584 => 282,  581 => 225,  576 => 277,  570 => 274,  563 => 270,  550 => 215,  547 => 259,  545 => 258,  542 => 257,  537 => 253,  531 => 250,  524 => 246,  519 => 244,  514 => 199,  500 => 231,  497 => 230,  495 => 191,  492 => 190,  481 => 186,  458 => 206,  456 => 205,  450 => 101,  446 => 200,  439 => 97,  434 => 192,  428 => 188,  422 => 184,  416 => 87,  414 => 179,  400 => 170,  395 => 81,  390 => 80,  378 => 159,  375 => 158,  358 => 147,  353 => 144,  348 => 140,  332 => 134,  329 => 133,  316 => 128,  310 => 127,  301 => 55,  271 => 47,  250 => 95,  242 => 92,  216 => 107,  137 => 44,  81 => 29,  188 => 62,  334 => 148,  327 => 143,  315 => 59,  306 => 57,  284 => 122,  279 => 111,  274 => 119,  272 => 97,  262 => 44,  239 => 81,  206 => 102,  186 => 92,  280 => 103,  255 => 91,  245 => 88,  150 => 48,  487 => 188,  482 => 240,  475 => 236,  470 => 109,  465 => 107,  454 => 226,  452 => 225,  449 => 224,  438 => 215,  436 => 96,  426 => 208,  421 => 204,  418 => 202,  412 => 199,  406 => 196,  399 => 192,  396 => 191,  394 => 190,  391 => 189,  386 => 185,  380 => 160,  373 => 178,  363 => 149,  349 => 67,  346 => 162,  344 => 137,  336 => 63,  323 => 149,  307 => 112,  304 => 124,  302 => 109,  288 => 123,  281 => 122,  265 => 45,  263 => 110,  260 => 43,  251 => 40,  233 => 94,  223 => 110,  185 => 70,  170 => 66,  180 => 13,  174 => 86,  65 => 24,  234 => 89,  225 => 76,  237 => 35,  230 => 100,  221 => 27,  213 => 105,  207 => 24,  200 => 64,  110 => 21,  34 => 18,  335 => 135,  330 => 61,  325 => 131,  320 => 139,  318 => 101,  311 => 97,  308 => 96,  299 => 133,  297 => 121,  292 => 119,  289 => 91,  282 => 99,  275 => 100,  273 => 84,  259 => 93,  257 => 42,  249 => 102,  240 => 36,  238 => 82,  231 => 72,  222 => 75,  218 => 85,  211 => 69,  198 => 20,  194 => 63,  184 => 54,  179 => 88,  152 => 341,  77 => 28,  63 => 21,  58 => 18,  53 => 15,  227 => 99,  215 => 79,  210 => 68,  205 => 68,  226 => 75,  219 => 108,  195 => 19,  172 => 58,  202 => 62,  190 => 72,  146 => 56,  113 => 40,  59 => 18,  267 => 96,  261 => 94,  254 => 41,  244 => 106,  228 => 91,  212 => 25,  204 => 75,  197 => 88,  191 => 94,  175 => 58,  167 => 82,  159 => 79,  127 => 62,  118 => 57,  134 => 29,  129 => 297,  100 => 36,  178 => 58,  161 => 79,  104 => 37,  102 => 43,  23 => 3,  181 => 89,  153 => 53,  148 => 45,  124 => 61,  97 => 29,  90 => 43,  76 => 27,  480 => 162,  474 => 217,  469 => 158,  461 => 207,  457 => 227,  453 => 204,  444 => 99,  440 => 148,  437 => 147,  435 => 146,  430 => 210,  427 => 143,  423 => 142,  413 => 134,  409 => 176,  407 => 131,  402 => 171,  398 => 129,  393 => 166,  387 => 79,  384 => 162,  381 => 120,  379 => 119,  374 => 116,  368 => 176,  365 => 150,  362 => 110,  360 => 148,  355 => 69,  341 => 64,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 54,  294 => 103,  285 => 114,  283 => 88,  278 => 98,  268 => 46,  264 => 79,  258 => 113,  252 => 96,  247 => 94,  241 => 82,  235 => 34,  229 => 79,  224 => 76,  220 => 81,  214 => 74,  208 => 79,  169 => 60,  143 => 55,  140 => 54,  132 => 54,  128 => 27,  119 => 24,  107 => 20,  71 => 23,  177 => 49,  165 => 82,  160 => 62,  135 => 47,  126 => 41,  114 => 49,  84 => 35,  70 => 26,  67 => 22,  61 => 29,  28 => 3,  87 => 32,  201 => 21,  196 => 97,  183 => 90,  171 => 84,  166 => 55,  163 => 58,  158 => 40,  156 => 78,  151 => 37,  142 => 70,  138 => 306,  136 => 67,  121 => 60,  117 => 50,  105 => 19,  91 => 37,  62 => 24,  49 => 14,  31 => 8,  21 => 6,  25 => 8,  93 => 29,  88 => 16,  78 => 37,  44 => 21,  94 => 38,  89 => 30,  85 => 10,  75 => 28,  68 => 4,  56 => 16,  27 => 4,  38 => 6,  24 => 3,  46 => 14,  26 => 3,  19 => 1,  79 => 29,  72 => 19,  69 => 19,  47 => 15,  40 => 11,  37 => 7,  22 => 2,  246 => 38,  157 => 77,  145 => 34,  139 => 69,  131 => 28,  123 => 37,  120 => 37,  115 => 56,  111 => 48,  108 => 31,  101 => 30,  98 => 34,  96 => 39,  83 => 30,  74 => 35,  66 => 25,  55 => 14,  52 => 23,  50 => 15,  43 => 11,  41 => 11,  35 => 5,  32 => 6,  29 => 5,  209 => 103,  203 => 100,  199 => 98,  193 => 95,  189 => 93,  187 => 92,  182 => 60,  176 => 87,  173 => 85,  168 => 83,  164 => 81,  162 => 42,  154 => 76,  149 => 340,  147 => 72,  144 => 71,  141 => 33,  133 => 66,  130 => 63,  125 => 26,  122 => 25,  116 => 57,  112 => 55,  109 => 47,  106 => 50,  103 => 48,  99 => 17,  95 => 34,  92 => 31,  86 => 10,  82 => 38,  80 => 24,  73 => 27,  64 => 21,  60 => 20,  57 => 20,  54 => 19,  51 => 13,  48 => 16,  45 => 14,  42 => 8,  39 => 10,  36 => 10,  33 => 9,  30 => 5,);
    }
}
