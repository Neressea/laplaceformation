<?php

/* WebProfilerBundle:Collector:request.html.twig */
class __TwigTemplate_7e122d3b13cfbbe0ab382d9a44971ec2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("@WebProfiler/Profiler/layout.html.twig");

        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        ob_start();
        // line 5
        echo "        ";
        if ($this->getAttribute($this->getAttribute($this->getContext($context, "collector", true), "controller", array(), "any", false, true), "class", array(), "any", true, true)) {
            // line 6
            echo "            ";
            $context["link"] = $this->env->getExtension('code')->getFileLink($this->getAttribute($this->getAttribute($this->getContext($context, "collector"), "controller"), "file"), $this->getAttribute($this->getAttribute($this->getContext($context, "collector"), "controller"), "line"));
            // line 7
            echo "            <span class=\"sf-toolbar-info-class sf-toolbar-info-with-next-pointer\">";
            echo $this->env->getExtension('code')->abbrClass($this->getAttribute($this->getAttribute($this->getContext($context, "collector"), "controller"), "class"));
            echo "</span>
            <span class=\"sf-toolbar-info-method\" onclick=\"";
            // line 8
            if ($this->getContext($context, "link")) {
                echo "window.location='";
                echo twig_escape_filter($this->env, $this->getContext($context, "link"), "html", null, true);
                echo "';window.event.stopPropagation();return false;";
            }
            echo "\">
                ";
            // line 9
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "collector"), "controller"), "method"), "html", null, true);
            echo "
            </span>
        ";
        } else {
            // line 12
            echo "            <span class=\"sf-toolbar-info-class\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "collector"), "controller"), "html", null, true);
            echo "</span>
        ";
        }
        // line 14
        echo "    ";
        $context["request_handler"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 15
        echo "    ";
        $context["request_status_code_color"] = (((400 > $this->getAttribute($this->getContext($context, "collector"), "statuscode"))) ? ((((200 == $this->getAttribute($this->getContext($context, "collector"), "statuscode"))) ? ("green") : ("yellow"))) : ("red"));
        // line 16
        echo "    ";
        $context["request_route"] = (($this->getAttribute($this->getContext($context, "collector"), "route")) ? ($this->getAttribute($this->getContext($context, "collector"), "route")) : ("NONE"));
        // line 17
        echo "    ";
        ob_start();
        // line 18
        echo "        <img width=\"28\" height=\"28\" alt=\"Request\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABwAAAAcCAQAAADYBBcfAAACvElEQVR42tVTbUhTYRTerDCnKVoUUr/KCZmypA9Koet0bXNLJ5XazDJ/WFaCUY0pExRZXxYiJgsxWWjkaL+yK+po1gjyR2QfmqWxtBmaBtqWGnabT++c11Fu4l/P4VzOPc95zoHznsNZodIbLDdRcKnc1Bu8DAK45ZsOnykQNMopsNooLxCknb0cDq5vml9FtHiIgpBR0R6iihYyFMTDt2Lg56ObPkI6TMGXSof1EV67IqCwisJSWliFAG/E0CfFIiebdNypcxi/1zgyFiIiZ3sJQr0RQx5frLa6k7SOKRo3oMFNR5t62h2rttKXEOKFqDCxtXNmmBokO2KKTlp3IdWuT2dYRNGKwEXEBCcL172G5FG0aIxC0kR9PBTVH1kkwQn+IqJnCE33EalVzT9GJQS1tAdD3CKicJYFrxqx7W2ejCEdZy1FiC5tZxHhLJKOZaRdQJAyV/YAvDliySALHxmxR4Hqe2iwvaOR/CEuZYJFSgYhVbZRkA8KGdEktrqnqra90NndCdkt77fjIHIhexOrfO6O3bbbOj/rqu5IptgyR3sU93QbOYhquZK4MCDp0Ina/PLsu5JvbCTRaapUdUmIV/RzoMdsk/0hWRNdAvKOmvqlN0drsJbJf1P4YsQ5lGrJeuosiOUgbOC8cto3LfOXTdVd7BqZsQKbse+0jUL6WPcesqs4MNSUTQAxGjwFiC8m3yzmqwHJBWYKBJ9WNqW/dHkpU/osch1Yj5RJfXPfSEe/2UPsN490NPfZG5CKyJmcV5ayHyzy7BMqsXfuHhGK/cjAIeSpR92gehR55D8TcQhDEKJwytBJ4fr4NULvrEM8NszfJPyxDoHYAQ1oPCWmIX4gifmDS/DV2DKeb25FHWr76yEG7/9L4YFPeiQQ4/8LkgJ8Et+NncTCsYqzXAEXa7CWdPZzGWdlyV+vST0JanfPvwAAAABJRU5ErkJggg==\">
        <span class=\"sf-toolbar-status sf-toolbar-status-";
        // line 19
        echo twig_escape_filter($this->env, $this->getContext($context, "request_status_code_color"), "html", null, true);
        echo "\" title=\"";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "collector"), "statustext"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "collector"), "statuscode"), "html", null, true);
        echo "</span>
        <span class=\"sf-toolbar-status sf-toolbar-info-piece-additional\">";
        // line 20
        echo twig_escape_filter($this->env, $this->getContext($context, "request_handler"), "html", null, true);
        echo "</span>
        <span class=\"sf-toolbar-info-piece-additional-detail\">on <i>";
        // line 21
        echo twig_escape_filter($this->env, $this->getContext($context, "request_route"), "html", null, true);
        echo "</i></span>
    ";
        $context["icon"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 23
        echo "    ";
        ob_start();
        // line 24
        echo "        ";
        ob_start();
        // line 25
        echo "            <div class=\"sf-toolbar-info-piece\">
                <b>Status</b>
                <span class=\"sf-toolbar-status sf-toolbar-status-";
        // line 27
        echo twig_escape_filter($this->env, $this->getContext($context, "request_status_code_color"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "collector"), "statuscode"), "html", null, true);
        echo "</span> ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "collector"), "statustext"), "html", null, true);
        echo "
            </div>
            <div class=\"sf-toolbar-info-piece\">
                <b>Controller</b>
                ";
        // line 31
        echo twig_escape_filter($this->env, $this->getContext($context, "request_handler"), "html", null, true);
        echo "
            </div>
            <div class=\"sf-toolbar-info-piece\">
                <b>Route name</b>
                <span>";
        // line 35
        echo twig_escape_filter($this->env, $this->getContext($context, "request_route"), "html", null, true);
        echo "</span>
            </div>
            <div class=\"sf-toolbar-info-piece\">
                <b>Has session</b>
                <span>";
        // line 39
        if (twig_length_filter($this->env, $this->getAttribute($this->getContext($context, "collector"), "sessionmetadata"))) {
            echo "yes";
        } else {
            echo "no";
        }
        echo "</span>
            </div>
        ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        // line 42
        echo "    ";
        $context["text"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 43
        echo "    ";
        $this->env->loadTemplate("@WebProfiler/Profiler/toolbar_item.html.twig")->display(array_merge($context, array("link" => $this->getContext($context, "profiler_url"))));
    }

    // line 46
    public function block_menu($context, array $blocks = array())
    {
        // line 47
        echo "<span class=\"label\">
    <span class=\"icon\"><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAAAcCAQAAACn1QXuAAAD2UlEQVR42p2Ve0zTVxTHKS4+KCBqNomCClgEJJAYkznQQIFaWltAiigsxGUgMy6b45HWV4UKUoP1yaMS0DqniVpngKlEMoMzW2Z0QTf4Ax/bdCzFCpQWq60U+Xp/baG/EoGf3vPH7/b3PffTc++55/w8xg+wji4W3ImDw4S3DgSD5fGhA+wcbRxclqsB+30RnmWcda1JPWn1poj8e3TYlvb/l6edTdSLWvYHgcUIdSwiuduxOOdu/n90WF7350648J+a0ClxYNWECglgahP+OyUOPpm34sDMNt6Ez+QwjniAKSzFgKWTw6L33x/3/yMHzU09l/XKlykj7krlXURNDlsEaVm/a8Fh48trUEEKGY4Zb5SaXUpZH4oROAlKvjijPu9GQfcY6jkOQoBlWIgARCAVVbtNo1rxky9/lqiV/hMmQfwXfRtZQxYVVoItC5aUpO8rDIcvYvUNqcN0n7TfJkyC+5lUdYIH9hlOkn3bCWbVCoJLLX9C9+FZEcoIpj2HYHh9XT92ZbUEFl7XSvfhD2EVI5imFh/DX948+lvWhgAEHL3kBrNhNSOYvImCdSgEb+wbGrmjomCFv46DrWn6hN+2QY6ZDYH8Tt6Dv+c4Yfn9bofbN8ABG/xHjYcMKmNHC0Tw/XOF0Ez3+VaH2BMZ1Ezclaynnm1x8LTDBo7U65Tm0tejrltPwwvzIcQO7EIKFsB3c8uoprAqzZruwQpE1cnpeMVxxZLNc8mFQQy2W9Tb+1xSplbjD18EEvM7sjTjuksp6rXVDBeVN29s5ztjFY1VSILpfJAHZiFkG1lAtyTD+gvZsix5emPSC3flm6v3JGvfxNvn+8zDt/HLFR3XUYI6RFPltERkYFro4j6Itdd5JB6JzaaGBAKUFtorpOsHRNoLveAxU1jRQ6xFQbaVNNFBpICN6YjZ00UpN0swj4KFPK/MtTJBffXKoETk3mouiYw7cmoLpsGzNVFkth+NpTKWgnkjof9MnjOflRYqsy4rfV1udebZatIgHhyB0XmylsyL2VXJjtQReMNWe9uGH5JN3ytMubY6HS7J9HSYTI/L1c9ybQoTQfEwG2HN52p7KixuEQ91PH5wEYkE5sRxUYJaFCCr4g+6o+o2slEMNVHjCYqF+RBjJ87m0OI/2YnvwMVCgnLi2AjCcgQgpGen1Mh1bATSgV4pghGISKKyqT6Gj+CHRUj/grT66sGOp7tIjOpmhGEGqYLxA174DOW4gjZiP6EMn2LWO7pz+O8N2nYcQhGq7v+ITZg3wYcPPghFDKibGUNm3u/qq5hL1PWIxgJEIRZBmE69fQsyes/JMSWb+gAAAABJRU5ErkJggg==\" alt=\"Request\"></span>
    <strong>Request</strong>
</span>
";
    }

    // line 53
    public function block_panel($context, array $blocks = array())
    {
        // line 54
        echo "    <h2>Request GET Parameters</h2>

    ";
        // line 56
        if (twig_length_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "collector"), "requestquery"), "all"))) {
            // line 57
            echo "        ";
            $this->env->loadTemplate("@WebProfiler/Profiler/bag.html.twig")->display(array("bag" => $this->getAttribute($this->getContext($context, "collector"), "requestquery")));
            // line 58
            echo "    ";
        } else {
            // line 59
            echo "        <p>
            <em>No GET parameters</em>
        </p>
    ";
        }
        // line 63
        echo "
    <h2>Request POST Parameters</h2>

    ";
        // line 66
        if (twig_length_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "collector"), "requestrequest"), "all"))) {
            // line 67
            echo "        ";
            $this->env->loadTemplate("@WebProfiler/Profiler/bag.html.twig")->display(array("bag" => $this->getAttribute($this->getContext($context, "collector"), "requestrequest")));
            // line 68
            echo "    ";
        } else {
            // line 69
            echo "        <p>
            <em>No POST parameters</em>
        </p>
    ";
        }
        // line 73
        echo "
    <h2>Request Attributes</h2>

    ";
        // line 76
        if (twig_length_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "collector"), "requestattributes"), "all"))) {
            // line 77
            echo "        ";
            $this->env->loadTemplate("@WebProfiler/Profiler/bag.html.twig")->display(array("bag" => $this->getAttribute($this->getContext($context, "collector"), "requestattributes")));
            // line 78
            echo "    ";
        } else {
            // line 79
            echo "        <p>
            <em>No attributes</em>
        </p>
    ";
        }
        // line 83
        echo "
    <h2>Request Cookies</h2>

    ";
        // line 86
        if (twig_length_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "collector"), "requestcookies"), "all"))) {
            // line 87
            echo "        ";
            $this->env->loadTemplate("@WebProfiler/Profiler/bag.html.twig")->display(array("bag" => $this->getAttribute($this->getContext($context, "collector"), "requestcookies")));
            // line 88
            echo "    ";
        } else {
            // line 89
            echo "        <p>
            <em>No cookies</em>
        </p>
    ";
        }
        // line 93
        echo "
    <h2>Request Headers</h2>

    ";
        // line 96
        $this->env->loadTemplate("@WebProfiler/Profiler/bag.html.twig")->display(array("bag" => $this->getAttribute($this->getContext($context, "collector"), "requestheaders")));
        // line 97
        echo "
    <h2>Request Content</h2>

    ";
        // line 100
        if (($this->getAttribute($this->getContext($context, "collector"), "content") == false)) {
            // line 101
            echo "        <p><em>Request content not available (it was retrieved as a resource).</em></p>
    ";
        } elseif ($this->getAttribute($this->getContext($context, "collector"), "content")) {
            // line 103
            echo "        <pre>";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "collector"), "content"), "html", null, true);
            echo "</pre>
    ";
        } else {
            // line 105
            echo "        <p><em>No content</em></p>
    ";
        }
        // line 107
        echo "
    <h2>Request Server Parameters</h2>

    ";
        // line 110
        $this->env->loadTemplate("@WebProfiler/Profiler/bag.html.twig")->display(array("bag" => $this->getAttribute($this->getContext($context, "collector"), "requestserver")));
        // line 111
        echo "
    <h2>Response Headers</h2>

    ";
        // line 114
        $this->env->loadTemplate("@WebProfiler/Profiler/bag.html.twig")->display(array("bag" => $this->getAttribute($this->getContext($context, "collector"), "responseheaders")));
        // line 115
        echo "
    <h2>Session Metadata</h2>

    ";
        // line 118
        if (twig_length_filter($this->env, $this->getAttribute($this->getContext($context, "collector"), "sessionmetadata"))) {
            // line 119
            echo "    ";
            $this->env->loadTemplate("@WebProfiler/Profiler/table.html.twig")->display(array("data" => $this->getAttribute($this->getContext($context, "collector"), "sessionmetadata")));
            // line 120
            echo "    ";
        } else {
            // line 121
            echo "    <p>
        <em>No session metadata</em>
    </p>
    ";
        }
        // line 125
        echo "
    <h2>Session Attributes</h2>

    ";
        // line 128
        if (twig_length_filter($this->env, $this->getAttribute($this->getContext($context, "collector"), "sessionattributes"))) {
            // line 129
            echo "        ";
            $this->env->loadTemplate("@WebProfiler/Profiler/table.html.twig")->display(array("data" => $this->getAttribute($this->getContext($context, "collector"), "sessionattributes")));
            // line 130
            echo "    ";
        } else {
            // line 131
            echo "        <p>
            <em>No session attributes</em>
        </p>
    ";
        }
        // line 135
        echo "
    <h2>Flashes</h2>

    ";
        // line 138
        if (twig_length_filter($this->env, $this->getAttribute($this->getContext($context, "collector"), "flashes"))) {
            // line 139
            echo "        ";
            $this->env->loadTemplate("@WebProfiler/Profiler/table.html.twig")->display(array("data" => $this->getAttribute($this->getContext($context, "collector"), "flashes")));
            // line 140
            echo "    ";
        } else {
            // line 141
            echo "        <p>
            <em>No flashes</em>
        </p>
    ";
        }
        // line 145
        echo "
    ";
        // line 146
        if ($this->getAttribute($this->getContext($context, "profile"), "parent")) {
            // line 147
            echo "        <h2><a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_profiler", array("token" => $this->getAttribute($this->getAttribute($this->getContext($context, "profile"), "parent"), "token"))), "html", null, true);
            echo "\">Parent request: ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "profile"), "parent"), "token"), "html", null, true);
            echo "</a></h2>

        ";
            // line 149
            $this->env->loadTemplate("@WebProfiler/Profiler/bag.html.twig")->display(array("bag" => $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "profile"), "parent"), "getcollector", array(0 => "request"), "method"), "requestattributes")));
            // line 150
            echo "    ";
        }
        // line 151
        echo "
    ";
        // line 152
        if (twig_length_filter($this->env, $this->getAttribute($this->getContext($context, "profile"), "children"))) {
            // line 153
            echo "        <h2>Sub requests</h2>

        ";
            // line 155
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "profile"), "children"));
            foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                // line 156
                echo "            <h3><a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_profiler", array("token" => $this->getAttribute($this->getContext($context, "child"), "token"))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "child"), "token"), "html", null, true);
                echo "</a></h3>
            ";
                // line 157
                $this->env->loadTemplate("@WebProfiler/Profiler/bag.html.twig")->display(array("bag" => $this->getAttribute($this->getAttribute($this->getContext($context, "child"), "getcollector", array(0 => "request"), "method"), "requestattributes")));
                // line 158
                echo "        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 159
            echo "    ";
        }
        // line 160
        echo "
";
    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:request.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  389 => 160,  371 => 156,  361 => 152,  345 => 147,  340 => 145,  331 => 140,  328 => 139,  326 => 138,  321 => 135,  296 => 121,  293 => 120,  290 => 119,  276 => 111,  269 => 107,  253 => 100,  792 => 485,  788 => 484,  784 => 482,  771 => 481,  706 => 472,  702 => 470,  694 => 468,  690 => 467,  686 => 466,  682 => 465,  675 => 463,  621 => 452,  616 => 450,  602 => 449,  565 => 414,  525 => 408,  520 => 406,  515 => 404,  356 => 328,  342 => 317,  339 => 316,  20 => 1,  892 => 350,  888 => 348,  879 => 346,  875 => 345,  871 => 343,  868 => 342,  865 => 341,  860 => 338,  854 => 336,  852 => 335,  829 => 334,  826 => 333,  820 => 330,  817 => 329,  814 => 328,  808 => 326,  803 => 487,  800 => 323,  791 => 314,  787 => 313,  783 => 312,  773 => 307,  770 => 306,  763 => 301,  757 => 299,  749 => 293,  745 => 476,  742 => 475,  732 => 282,  727 => 280,  723 => 473,  707 => 278,  704 => 277,  701 => 275,  698 => 469,  695 => 273,  693 => 272,  687 => 268,  684 => 267,  679 => 264,  672 => 261,  668 => 259,  666 => 258,  651 => 257,  648 => 256,  645 => 460,  639 => 252,  636 => 251,  631 => 249,  628 => 248,  625 => 453,  622 => 246,  620 => 245,  617 => 244,  614 => 243,  605 => 237,  601 => 236,  595 => 233,  591 => 231,  588 => 230,  578 => 224,  575 => 223,  568 => 218,  558 => 217,  553 => 216,  544 => 213,  541 => 212,  538 => 211,  535 => 210,  532 => 209,  530 => 410,  527 => 409,  522 => 204,  516 => 200,  513 => 198,  512 => 197,  511 => 196,  507 => 195,  504 => 194,  498 => 192,  489 => 189,  484 => 187,  473 => 110,  468 => 108,  462 => 106,  447 => 100,  441 => 98,  433 => 95,  404 => 85,  397 => 82,  369 => 78,  367 => 155,  364 => 76,  352 => 68,  343 => 146,  333 => 62,  313 => 58,  303 => 56,  295 => 275,  286 => 48,  248 => 97,  243 => 37,  232 => 88,  236 => 80,  155 => 47,  828 => 400,  823 => 331,  816 => 393,  811 => 327,  806 => 488,  798 => 384,  795 => 383,  793 => 382,  790 => 381,  779 => 372,  777 => 309,  772 => 368,  767 => 364,  764 => 362,  760 => 300,  752 => 354,  746 => 350,  729 => 347,  722 => 346,  705 => 345,  699 => 341,  683 => 338,  681 => 337,  678 => 464,  673 => 462,  656 => 461,  642 => 253,  640 => 321,  637 => 320,  634 => 250,  630 => 455,  624 => 313,  618 => 451,  589 => 284,  586 => 283,  584 => 282,  581 => 225,  576 => 277,  570 => 274,  563 => 270,  550 => 215,  547 => 411,  545 => 258,  542 => 257,  537 => 253,  531 => 250,  524 => 246,  519 => 244,  514 => 199,  500 => 231,  497 => 230,  495 => 191,  492 => 190,  481 => 186,  458 => 206,  456 => 205,  450 => 101,  446 => 200,  439 => 97,  434 => 192,  428 => 188,  422 => 184,  416 => 87,  414 => 179,  400 => 170,  395 => 81,  390 => 80,  378 => 157,  375 => 158,  358 => 151,  353 => 149,  348 => 140,  332 => 134,  329 => 133,  316 => 128,  310 => 127,  301 => 55,  271 => 47,  250 => 95,  242 => 92,  216 => 79,  137 => 44,  81 => 29,  188 => 90,  334 => 141,  327 => 143,  315 => 131,  306 => 57,  284 => 122,  279 => 111,  274 => 110,  272 => 97,  262 => 44,  239 => 81,  206 => 102,  186 => 92,  280 => 103,  255 => 101,  245 => 88,  150 => 48,  487 => 188,  482 => 240,  475 => 236,  470 => 109,  465 => 107,  454 => 226,  452 => 225,  449 => 224,  438 => 215,  436 => 96,  426 => 208,  421 => 204,  418 => 202,  412 => 199,  406 => 196,  399 => 192,  396 => 191,  394 => 190,  391 => 189,  386 => 159,  380 => 158,  373 => 178,  363 => 153,  349 => 67,  346 => 162,  344 => 318,  336 => 63,  323 => 149,  307 => 128,  304 => 124,  302 => 125,  288 => 118,  281 => 114,  265 => 105,  263 => 110,  260 => 43,  251 => 40,  233 => 94,  223 => 110,  185 => 70,  170 => 56,  180 => 13,  174 => 86,  65 => 11,  234 => 89,  225 => 76,  237 => 35,  230 => 100,  221 => 27,  213 => 78,  207 => 24,  200 => 64,  110 => 21,  34 => 18,  335 => 135,  330 => 61,  325 => 131,  320 => 139,  318 => 101,  311 => 97,  308 => 96,  299 => 133,  297 => 276,  292 => 119,  289 => 91,  282 => 99,  275 => 100,  273 => 84,  259 => 103,  257 => 42,  249 => 102,  240 => 36,  238 => 82,  231 => 72,  222 => 83,  218 => 85,  211 => 69,  198 => 20,  194 => 68,  184 => 63,  179 => 88,  152 => 46,  77 => 28,  63 => 21,  58 => 25,  53 => 15,  227 => 86,  215 => 79,  210 => 77,  205 => 68,  226 => 75,  219 => 108,  195 => 19,  172 => 57,  202 => 62,  190 => 72,  146 => 56,  113 => 48,  59 => 18,  267 => 96,  261 => 94,  254 => 41,  244 => 136,  228 => 91,  212 => 25,  204 => 75,  197 => 69,  191 => 67,  175 => 58,  167 => 82,  159 => 79,  127 => 35,  118 => 49,  134 => 39,  129 => 297,  100 => 39,  178 => 59,  161 => 63,  104 => 37,  102 => 24,  23 => 3,  181 => 89,  153 => 77,  148 => 45,  124 => 61,  97 => 29,  90 => 20,  76 => 17,  480 => 162,  474 => 217,  469 => 158,  461 => 207,  457 => 227,  453 => 204,  444 => 99,  440 => 148,  437 => 147,  435 => 146,  430 => 210,  427 => 143,  423 => 142,  413 => 134,  409 => 176,  407 => 131,  402 => 171,  398 => 129,  393 => 166,  387 => 79,  384 => 162,  381 => 120,  379 => 119,  374 => 116,  368 => 176,  365 => 150,  362 => 110,  360 => 148,  355 => 150,  341 => 64,  337 => 103,  322 => 101,  314 => 99,  312 => 130,  309 => 129,  305 => 95,  298 => 54,  294 => 103,  285 => 114,  283 => 115,  278 => 98,  268 => 46,  264 => 79,  258 => 113,  252 => 96,  247 => 94,  241 => 93,  235 => 89,  229 => 87,  224 => 76,  220 => 81,  214 => 74,  208 => 76,  169 => 60,  143 => 55,  140 => 58,  132 => 54,  128 => 27,  119 => 24,  107 => 20,  71 => 13,  177 => 49,  165 => 83,  160 => 62,  135 => 47,  126 => 41,  114 => 49,  84 => 40,  70 => 15,  67 => 14,  61 => 12,  28 => 3,  87 => 34,  201 => 21,  196 => 92,  183 => 90,  171 => 84,  166 => 54,  163 => 53,  158 => 62,  156 => 78,  151 => 59,  142 => 70,  138 => 306,  136 => 71,  121 => 50,  117 => 50,  105 => 25,  91 => 37,  62 => 27,  49 => 14,  31 => 8,  21 => 2,  25 => 8,  93 => 29,  88 => 16,  78 => 18,  44 => 20,  94 => 21,  89 => 30,  85 => 23,  75 => 28,  68 => 30,  56 => 16,  27 => 3,  38 => 18,  24 => 3,  46 => 13,  26 => 3,  19 => 1,  79 => 18,  72 => 19,  69 => 19,  47 => 8,  40 => 8,  37 => 6,  22 => 2,  246 => 96,  157 => 77,  145 => 74,  139 => 69,  131 => 28,  123 => 61,  120 => 31,  115 => 56,  111 => 47,  108 => 31,  101 => 30,  98 => 45,  96 => 37,  83 => 33,  74 => 35,  66 => 25,  55 => 9,  52 => 23,  50 => 22,  43 => 12,  41 => 19,  35 => 6,  32 => 5,  29 => 3,  209 => 103,  203 => 73,  199 => 93,  193 => 95,  189 => 66,  187 => 92,  182 => 87,  176 => 86,  173 => 85,  168 => 83,  164 => 81,  162 => 42,  154 => 60,  149 => 340,  147 => 43,  144 => 42,  141 => 73,  133 => 66,  130 => 63,  125 => 51,  122 => 25,  116 => 57,  112 => 55,  109 => 27,  106 => 51,  103 => 48,  99 => 23,  95 => 34,  92 => 43,  86 => 10,  82 => 19,  80 => 32,  73 => 16,  64 => 23,  60 => 20,  57 => 39,  54 => 19,  51 => 37,  48 => 16,  45 => 14,  42 => 7,  39 => 6,  36 => 5,  33 => 4,  30 => 3,);
    }
}
