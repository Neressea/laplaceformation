<?php

/* WebProfilerBundle:Router:panel.html.twig */
class __TwigTemplate_633e68aa97ef957ed5f1c56fd0040895 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<h2>Routing for \"";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "pathinfo"), "html", null, true);
        echo "\"</h2>

<ul>
    <li>
        <strong>Route:&nbsp;</strong>
        ";
        // line 6
        if ($this->getAttribute($this->getContext($context, "request"), "route")) {
            // line 7
            echo "            ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "route"), "html", null, true);
            echo "
        ";
        } else {
            // line 9
            echo "            <em>No matching route</em>
        ";
        }
        // line 11
        echo "    </li>
    <li>
        <strong>Route parameters:&nbsp;</strong>
        ";
        // line 14
        if (twig_length_filter($this->env, $this->getAttribute($this->getContext($context, "request"), "routeParams"))) {
            // line 15
            echo "            ";
            $this->env->loadTemplate("@WebProfiler/Profiler/table.html.twig")->display(array("data" => $this->getAttribute($this->getContext($context, "request"), "routeParams"), "class" => "inline"));
            // line 16
            echo "        ";
        } else {
            // line 17
            echo "            <em>No parameters</em>
        ";
        }
        // line 19
        echo "    </li>
    ";
        // line 20
        if ($this->getAttribute($this->getContext($context, "router"), "redirect")) {
            // line 21
            echo "    <li>
        <strong>Redirecting to:&nbsp;</strong> \"";
            // line 22
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "router"), "targetUrl"), "html", null, true);
            echo "\" ";
            if ($this->getAttribute($this->getContext($context, "router"), "targetRoute")) {
                echo "(route: \"";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "router"), "targetRoute"), "html", null, true);
                echo "\")";
            }
            // line 23
            echo "    <li>
    ";
        }
        // line 25
        echo "    <li>
        <strong>Route matching logs</strong>
        <table class=\"routing inline\">
            <tr>
                <th>Route name</th>
                <th>Pattern</th>
                <th>Log</th>
            </tr>
            ";
        // line 33
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "traces"));
        foreach ($context['_seq'] as $context["_key"] => $context["trace"]) {
            // line 34
            echo "                <tr class=\"";
            echo (((1 == $this->getAttribute($this->getContext($context, "trace"), "level"))) ? ("almost") : ((((2 == $this->getAttribute($this->getContext($context, "trace"), "level"))) ? ("matches") : (""))));
            echo "\">
                    <td>";
            // line 35
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "trace"), "name"), "html", null, true);
            echo "</td>
                    <td>";
            // line 36
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "trace"), "path"), "html", null, true);
            echo "</td>
                    <td>";
            // line 37
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "trace"), "log"), "html", null, true);
            echo "</td>
                </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['trace'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 40
        echo "        </table>
        <em><small>Note: The above matching is based on the configuration for the current router which might differ from
        the configuration used while routing this request.</small></em>
    </li>
</ul>
";
    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Router:panel.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  892 => 350,  888 => 348,  879 => 346,  875 => 345,  871 => 343,  868 => 342,  865 => 341,  860 => 338,  854 => 336,  852 => 335,  829 => 334,  826 => 333,  820 => 330,  817 => 329,  814 => 328,  808 => 326,  803 => 324,  800 => 323,  791 => 314,  787 => 313,  783 => 312,  773 => 307,  770 => 306,  763 => 301,  757 => 299,  749 => 293,  745 => 291,  742 => 290,  732 => 282,  727 => 280,  723 => 279,  707 => 278,  704 => 277,  701 => 275,  698 => 274,  695 => 273,  693 => 272,  687 => 268,  684 => 267,  679 => 264,  672 => 261,  668 => 259,  666 => 258,  651 => 257,  648 => 256,  645 => 255,  639 => 252,  636 => 251,  631 => 249,  628 => 248,  625 => 247,  622 => 246,  620 => 245,  617 => 244,  614 => 243,  605 => 237,  601 => 236,  595 => 233,  591 => 231,  588 => 230,  578 => 224,  575 => 223,  568 => 218,  558 => 217,  553 => 216,  544 => 213,  541 => 212,  538 => 211,  535 => 210,  532 => 209,  530 => 208,  527 => 207,  522 => 204,  516 => 200,  513 => 198,  512 => 197,  511 => 196,  507 => 195,  504 => 194,  498 => 192,  489 => 189,  484 => 187,  473 => 110,  468 => 108,  462 => 106,  447 => 100,  441 => 98,  433 => 95,  404 => 85,  397 => 82,  369 => 78,  367 => 77,  364 => 76,  352 => 68,  343 => 65,  333 => 62,  313 => 58,  303 => 56,  295 => 53,  286 => 48,  248 => 39,  243 => 37,  232 => 80,  236 => 80,  155 => 50,  828 => 400,  823 => 331,  816 => 393,  811 => 327,  806 => 325,  798 => 384,  795 => 383,  793 => 382,  790 => 381,  779 => 372,  777 => 309,  772 => 368,  767 => 364,  764 => 362,  760 => 300,  752 => 354,  746 => 350,  729 => 347,  722 => 346,  705 => 345,  699 => 341,  683 => 338,  681 => 337,  678 => 336,  673 => 335,  656 => 334,  642 => 253,  640 => 321,  637 => 320,  634 => 250,  630 => 316,  624 => 313,  618 => 310,  589 => 284,  586 => 283,  584 => 282,  581 => 225,  576 => 277,  570 => 274,  563 => 270,  550 => 215,  547 => 259,  545 => 258,  542 => 257,  537 => 253,  531 => 250,  524 => 246,  519 => 244,  514 => 199,  500 => 231,  497 => 230,  495 => 191,  492 => 190,  481 => 186,  458 => 206,  456 => 205,  450 => 101,  446 => 200,  439 => 97,  434 => 192,  428 => 188,  422 => 184,  416 => 87,  414 => 179,  400 => 170,  395 => 81,  390 => 80,  378 => 159,  375 => 158,  358 => 147,  353 => 144,  348 => 140,  332 => 134,  329 => 133,  316 => 128,  310 => 127,  301 => 55,  271 => 47,  250 => 95,  242 => 92,  216 => 107,  137 => 44,  81 => 36,  188 => 62,  334 => 148,  327 => 143,  315 => 59,  306 => 57,  284 => 122,  279 => 111,  274 => 119,  272 => 97,  262 => 44,  239 => 81,  206 => 102,  186 => 92,  280 => 103,  255 => 91,  245 => 88,  150 => 48,  487 => 188,  482 => 240,  475 => 236,  470 => 109,  465 => 107,  454 => 226,  452 => 225,  449 => 224,  438 => 215,  436 => 96,  426 => 208,  421 => 204,  418 => 202,  412 => 199,  406 => 196,  399 => 192,  396 => 191,  394 => 190,  391 => 189,  386 => 185,  380 => 160,  373 => 178,  363 => 149,  349 => 67,  346 => 162,  344 => 137,  336 => 63,  323 => 149,  307 => 112,  304 => 124,  302 => 109,  288 => 123,  281 => 122,  265 => 45,  263 => 110,  260 => 43,  251 => 40,  233 => 94,  223 => 110,  185 => 70,  170 => 66,  180 => 13,  174 => 86,  65 => 22,  234 => 89,  225 => 76,  237 => 35,  230 => 100,  221 => 27,  213 => 105,  207 => 24,  200 => 64,  110 => 21,  34 => 18,  335 => 135,  330 => 61,  325 => 131,  320 => 139,  318 => 101,  311 => 97,  308 => 96,  299 => 133,  297 => 121,  292 => 119,  289 => 91,  282 => 99,  275 => 100,  273 => 84,  259 => 93,  257 => 42,  249 => 102,  240 => 36,  238 => 82,  231 => 72,  222 => 75,  218 => 85,  211 => 69,  198 => 20,  194 => 63,  184 => 54,  179 => 88,  152 => 341,  77 => 25,  63 => 30,  58 => 23,  53 => 17,  227 => 99,  215 => 79,  210 => 68,  205 => 68,  226 => 75,  219 => 108,  195 => 19,  172 => 58,  202 => 62,  190 => 72,  146 => 56,  113 => 40,  59 => 18,  267 => 96,  261 => 94,  254 => 41,  244 => 106,  228 => 91,  212 => 25,  204 => 75,  197 => 88,  191 => 94,  175 => 58,  167 => 82,  159 => 79,  127 => 62,  118 => 57,  134 => 29,  129 => 297,  100 => 36,  178 => 58,  161 => 79,  104 => 37,  102 => 49,  23 => 3,  181 => 89,  153 => 53,  148 => 45,  124 => 61,  97 => 29,  90 => 43,  76 => 8,  480 => 162,  474 => 217,  469 => 158,  461 => 207,  457 => 227,  453 => 204,  444 => 99,  440 => 148,  437 => 147,  435 => 146,  430 => 210,  427 => 143,  423 => 142,  413 => 134,  409 => 176,  407 => 131,  402 => 171,  398 => 129,  393 => 166,  387 => 79,  384 => 162,  381 => 120,  379 => 119,  374 => 116,  368 => 176,  365 => 150,  362 => 110,  360 => 148,  355 => 69,  341 => 64,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 54,  294 => 103,  285 => 114,  283 => 88,  278 => 98,  268 => 46,  264 => 79,  258 => 113,  252 => 96,  247 => 94,  241 => 82,  235 => 34,  229 => 79,  224 => 76,  220 => 81,  214 => 74,  208 => 79,  169 => 60,  143 => 55,  140 => 54,  132 => 54,  128 => 27,  119 => 24,  107 => 20,  71 => 10,  177 => 49,  165 => 82,  160 => 62,  135 => 47,  126 => 41,  114 => 49,  84 => 104,  70 => 32,  67 => 73,  61 => 29,  28 => 6,  87 => 33,  201 => 21,  196 => 97,  183 => 90,  171 => 84,  166 => 55,  163 => 58,  158 => 40,  156 => 78,  151 => 37,  142 => 70,  138 => 306,  136 => 67,  121 => 60,  117 => 50,  105 => 19,  91 => 34,  62 => 21,  49 => 23,  31 => 5,  21 => 6,  25 => 8,  93 => 29,  88 => 16,  78 => 37,  44 => 21,  94 => 44,  89 => 19,  85 => 10,  75 => 34,  68 => 4,  56 => 15,  27 => 4,  38 => 1,  24 => 4,  46 => 22,  26 => 3,  19 => 1,  79 => 26,  72 => 19,  69 => 19,  47 => 15,  40 => 11,  37 => 19,  22 => 2,  246 => 38,  157 => 77,  145 => 34,  139 => 69,  131 => 28,  123 => 37,  120 => 37,  115 => 56,  111 => 48,  108 => 31,  101 => 30,  98 => 17,  96 => 35,  83 => 9,  74 => 35,  66 => 31,  55 => 14,  52 => 23,  50 => 16,  43 => 10,  41 => 11,  35 => 6,  32 => 17,  29 => 6,  209 => 103,  203 => 100,  199 => 98,  193 => 95,  189 => 93,  187 => 92,  182 => 60,  176 => 87,  173 => 85,  168 => 83,  164 => 81,  162 => 42,  154 => 76,  149 => 340,  147 => 72,  144 => 71,  141 => 33,  133 => 66,  130 => 63,  125 => 26,  122 => 25,  116 => 57,  112 => 55,  109 => 30,  106 => 50,  103 => 48,  99 => 17,  95 => 44,  92 => 31,  86 => 10,  82 => 38,  80 => 24,  73 => 23,  64 => 17,  60 => 20,  57 => 19,  54 => 24,  51 => 13,  48 => 11,  45 => 14,  42 => 10,  39 => 20,  36 => 9,  33 => 7,  30 => 7,);
    }
}
