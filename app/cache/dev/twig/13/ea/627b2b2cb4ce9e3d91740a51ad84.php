<?php

/* LaplaceCommonBundle:Form:fields.html.twig */
class __TwigTemplate_13ea627b2b2cb4ce9e3d91740a51ad84 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'form_widget_simple' => array($this, 'block_form_widget_simple'),
            'textarea_widget' => array($this, 'block_textarea_widget'),
            'choice_widget_expanded' => array($this, 'block_choice_widget_expanded'),
            'radio_label' => array($this, 'block_radio_label'),
            'choice_widget_collapsed' => array($this, 'block_choice_widget_collapsed'),
            'choice_widget_options' => array($this, 'block_choice_widget_options'),
            'button_widget' => array($this, 'block_button_widget'),
            'submit_widget' => array($this, 'block_submit_widget'),
            'date_widget' => array($this, 'block_date_widget'),
            'time_widget' => array($this, 'block_time_widget'),
            'user_selector_widget' => array($this, 'block_user_selector_widget'),
            'form_row' => array($this, 'block_form_row'),
            'form_label' => array($this, 'block_form_label'),
            'checkbox_row' => array($this, 'block_checkbox_row'),
            'button_row' => array($this, 'block_button_row'),
            'submit_row' => array($this, 'block_submit_row'),
            'datetime_row' => array($this, 'block_datetime_row'),
            'form_start' => array($this, 'block_form_start'),
            'form_errors' => array($this, 'block_form_errors'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
";
        // line 3
        echo "

";
        // line 6
        $this->displayBlock('form_widget_simple', $context, $blocks);
        // line 11
        echo "
";
        // line 13
        $this->displayBlock('textarea_widget', $context, $blocks);
        // line 17
        echo "
";
        // line 19
        $this->displayBlock('choice_widget_expanded', $context, $blocks);
        // line 30
        echo "
";
        // line 32
        $this->displayBlock('radio_label', $context, $blocks);
        // line 51
        echo "
";
        // line 53
        $this->displayBlock('choice_widget_collapsed', $context, $blocks);
        // line 73
        echo "
";
        // line 75
        echo "
";
        // line 76
        $this->displayBlock('choice_widget_options', $context, $blocks);
        // line 90
        echo "
";
        // line 92
        echo "

";
        // line 95
        $this->displayBlock('button_widget', $context, $blocks);
        // line 104
        echo "
";
        // line 106
        $this->displayBlock('submit_widget', $context, $blocks);
        // line 113
        echo "
";
        // line 185
        echo "
";
        // line 186
        $this->displayBlock('date_widget', $context, $blocks);
        // line 206
        echo "
";
        // line 207
        $this->displayBlock('time_widget', $context, $blocks);
        // line 222
        echo "
";
        // line 223
        $this->displayBlock('user_selector_widget', $context, $blocks);
        // line 227
        echo "


";
        // line 230
        $this->displayBlock('form_row', $context, $blocks);
        // line 242
        echo "
";
        // line 243
        $this->displayBlock('form_label', $context, $blocks);
        // line 266
        echo "
";
        // line 267
        $this->displayBlock('checkbox_row', $context, $blocks);
        // line 288
        echo "
";
        // line 290
        $this->displayBlock('button_row', $context, $blocks);
        // line 297
        echo "
";
        // line 299
        $this->displayBlock('submit_row', $context, $blocks);
        // line 304
        echo "

";
        // line 306
        $this->displayBlock('datetime_row', $context, $blocks);
        // line 319
        echo "

";
        // line 322
        echo "
";
        // line 323
        $this->displayBlock('form_start', $context, $blocks);
        // line 340
        echo "
";
        // line 341
        $this->displayBlock('form_errors', $context, $blocks);
    }

    // line 6
    public function block_form_widget_simple($context, array $blocks = array())
    {
        // line 7
        echo "    ";
        $context["type"] = ((array_key_exists("type", $context)) ? (_twig_default_filter($this->getContext($context, "type"), "text")) : ("text"));
        // line 8
        echo "    ";
        $context["attr"] = twig_array_merge($this->getContext($context, "attr"), array("class" => trim(((($this->getAttribute($this->getContext($context, "attr", true), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getContext($context, "attr", true), "class"), "")) : ("")) . " input-block-level"))));
        // line 9
        echo "    <input type=\"";
        echo twig_escape_filter($this->env, $this->getContext($context, "type"), "html", null, true);
        echo "\" ";
        $this->displayBlock("widget_attributes", $context, $blocks);
        echo " ";
        if ((!twig_test_empty($this->getContext($context, "value")))) {
            echo "value=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "value"), "html", null, true);
            echo "\" ";
        }
        echo " />
";
    }

    // line 13
    public function block_textarea_widget($context, array $blocks = array())
    {
        // line 14
        echo "    ";
        $context["attr"] = twig_array_merge($this->getContext($context, "attr"), array("class" => trim(((($this->getAttribute($this->getContext($context, "attr", true), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getContext($context, "attr", true), "class"), "")) : ("")) . " input-block-level")), "rows" => (($this->getAttribute($this->getContext($context, "attr", true), "rows", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getContext($context, "attr", true), "rows"), 8)) : (8))));
        // line 15
        echo "    <textarea ";
        $this->displayBlock("widget_attributes", $context, $blocks);
        echo ">";
        echo twig_escape_filter($this->env, $this->getContext($context, "value"), "html", null, true);
        echo "</textarea>
";
    }

    // line 19
    public function block_choice_widget_expanded($context, array $blocks = array())
    {
        // line 20
        echo "
    ";
        // line 21
        if ($this->getContext($context, "multiple")) {
            // line 22
            echo "
    ";
        } else {
            // line 24
            echo "        ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getContext($context, "form"));
            foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                // line 25
                echo "            ";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "child"), 'label');
                echo "
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 27
            echo "    ";
        }
        // line 28
        echo "
";
    }

    // line 32
    public function block_radio_label($context, array $blocks = array())
    {
        // line 33
        echo "
    ";
        // line 34
        if ((!$this->getContext($context, "compound"))) {
            // line 35
            echo "        ";
            $context["label_attr"] = twig_array_merge($this->getContext($context, "label_attr"), array("for" => $this->getContext($context, "id")));
            // line 36
            echo "    ";
        }
        // line 37
        echo "
    ";
        // line 38
        $context["label_attr"] = twig_array_merge($this->getContext($context, "label_attr"), array("class" => trim(((($this->getAttribute($this->getContext($context, "label_attr", true), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getContext($context, "label_attr", true), "class"), "")) : ("")) . " radio"))));
        // line 39
        echo "    ";
        if (($this->getAttribute($this->getContext($context, "attr", true), "inline", array(), "any", true, true) && $this->getAttribute($this->getContext($context, "attr"), "inline"))) {
            // line 40
            echo "        ";
            $context["label_attr"] = twig_array_merge($this->getContext($context, "label_attr"), array("class" => trim(((($this->getAttribute($this->getContext($context, "label_attr", true), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getContext($context, "label_attr", true), "class"), "")) : ("")) . " inline"))));
            // line 41
            echo "    ";
        }
        // line 42
        echo "
    ";
        // line 43
        if (twig_test_empty($this->getContext($context, "label"))) {
            // line 44
            echo "        ";
            $context["label"] = $this->env->getExtension('form')->renderer->humanize($this->getContext($context, "name"));
            // line 45
            echo "    ";
        }
        // line 46
        echo "
    <label";
        // line 47
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "label_attr"));
        foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getContext($context, "attrname"), "html", null, true);
            echo "=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "attrvalue"), "html", null, true);
            echo "\"";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo ">
        ";
        // line 48
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "form"), 'widget');
        echo " ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getContext($context, "label"), array(), $this->getContext($context, "translation_domain")), "html", null, true);
        echo "
    </label>
";
    }

    // line 53
    public function block_choice_widget_collapsed($context, array $blocks = array())
    {
        // line 54
        echo "
    ";
        // line 55
        $context["attr"] = twig_array_merge($this->getContext($context, "attr"), array("class" => trim(((($this->getAttribute($this->getContext($context, "attr", true), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getContext($context, "attr", true), "class"), "")) : ("")) . " input-block-level"))));
        // line 56
        echo "
    <select ";
        // line 57
        $this->displayBlock("widget_attributes", $context, $blocks);
        if ($this->getContext($context, "multiple")) {
            echo " multiple=\"multiple\"";
        }
        echo ">
        ";
        // line 58
        if ((!(null === $this->getContext($context, "empty_value")))) {
            // line 59
            echo "            <option ";
            if ($this->getContext($context, "required")) {
                echo " disabled=\"disabled\"";
                if (twig_test_empty($this->getContext($context, "value"))) {
                    echo " selected=\"selected\"";
                }
            } else {
                echo " value=\"\"";
            }
            echo ">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getContext($context, "empty_value"), array(), $this->getContext($context, "translation_domain")), "html", null, true);
            echo "</option>
        ";
        }
        // line 61
        echo "        ";
        if ((twig_length_filter($this->env, $this->getContext($context, "preferred_choices")) > 0)) {
            // line 62
            echo "            ";
            $context["options"] = $this->getContext($context, "preferred_choices");
            // line 63
            echo "            ";
            $this->displayBlock("choice_widget_options", $context, $blocks);
            echo "
            ";
            // line 64
            if (((twig_length_filter($this->env, $this->getContext($context, "choices")) > 0) && (!(null === $this->getContext($context, "separator"))))) {
                // line 65
                echo "                <option disabled=\"disabled\">";
                echo twig_escape_filter($this->env, $this->getContext($context, "separator"), "html", null, true);
                echo "</option>
            ";
            }
            // line 67
            echo "        ";
        }
        // line 68
        echo "        ";
        $context["options"] = $this->getContext($context, "choices");
        // line 69
        echo "        ";
        $this->displayBlock("choice_widget_options", $context, $blocks);
        echo "
    </select>

";
    }

    // line 76
    public function block_choice_widget_options($context, array $blocks = array())
    {
        // line 77
        ob_start();
        // line 78
        echo "    ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "options"));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["group_label"] => $context["choice"]) {
            // line 79
            echo "        ";
            if (twig_test_iterable($this->getContext($context, "choice"))) {
                // line 80
                echo "            <optgroup label=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getContext($context, "group_label"), array(), $this->getContext($context, "translation_domain")), "html", null, true);
                echo "\" value=\"0\">
                ";
                // line 81
                $context["options"] = $this->getContext($context, "choice");
                // line 82
                echo "                ";
                $this->displayBlock("choice_widget_options", $context, $blocks);
                echo "
            </optgroup>
        ";
            } else {
                // line 85
                echo "            <option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "choice"), "value"), "html", null, true);
                echo "\"";
                if ($this->env->getExtension('form')->isSelectedChoice($this->getContext($context, "choice"), $this->getContext($context, "value"))) {
                    echo " selected=\"selected\"";
                }
                echo ">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute($this->getContext($context, "choice"), "label"), array(), $this->getContext($context, "translation_domain")), "html", null, true);
                echo "</option>
        ";
            }
            // line 87
            echo "    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['group_label'], $context['choice'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 95
    public function block_button_widget($context, array $blocks = array())
    {
        // line 96
        echo "
    ";
        // line 97
        if (twig_test_empty($this->getContext($context, "label"))) {
            // line 98
            echo "        ";
            $context["label"] = $this->env->getExtension('form')->renderer->humanize($this->getContext($context, "name"));
            // line 99
            echo "    ";
        }
        // line 100
        echo "    ";
        $context["attr"] = twig_array_merge($this->getContext($context, "attr"), array("class" => trim(((($this->getAttribute($this->getContext($context, "attr", true), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getContext($context, "attr", true), "class"), "")) : ("")) . " btn"))));
        // line 101
        echo "    <button type=\"";
        echo twig_escape_filter($this->env, ((array_key_exists("type", $context)) ? (_twig_default_filter($this->getContext($context, "type"), "button")) : ("button")), "html", null, true);
        echo "\" ";
        $this->displayBlock("button_attributes", $context, $blocks);
        echo ">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getContext($context, "label"), array(), $this->getContext($context, "translation_domain")), "html", null, true);
        echo "</button>

";
    }

    // line 106
    public function block_submit_widget($context, array $blocks = array())
    {
        // line 107
        echo "
    ";
        // line 108
        $context["type"] = ((array_key_exists("type", $context)) ? (_twig_default_filter($this->getContext($context, "type"), "submit")) : ("submit"));
        // line 109
        echo "    ";
        $context["attr"] = twig_array_merge($this->getContext($context, "attr"), array("class" => trim(((($this->getAttribute($this->getContext($context, "attr", true), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getContext($context, "attr", true), "class"), "")) : ("")) . " btn-primary"))));
        // line 110
        echo "    ";
        $this->displayBlock("button_widget", $context, $blocks);
        echo "

";
    }

    // line 186
    public function block_date_widget($context, array $blocks = array())
    {
        // line 187
        echo "
    ";
        // line 188
        if (($this->getContext($context, "widget") == "single_text")) {
            // line 189
            echo "\t\t";
            $context["type"] = "text";
            // line 190
            echo "        ";
            $context["attr"] = twig_array_merge($this->getContext($context, "attr"), array("placeholder" => (($this->getAttribute($this->getContext($context, "attr", true), "placeholder", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getContext($context, "attr", true), "placeholder"), "Date au format jj/mm/aa")) : ("Date au format jj/mm/aa"))));
            // line 191
            echo "\t\t";
            $context["attr"] = twig_array_merge($this->getContext($context, "attr"), array("title" => (($this->getAttribute($this->getContext($context, "attr", true), "title", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getContext($context, "attr", true), "title"), "Date au format jj/mm/aa")) : ("Date au format jj/mm/aa"))));
            // line 192
            echo "        ";
            $this->displayBlock("form_widget_simple", $context, $blocks);
            echo "
    ";
        } else {
            // line 194
            echo "
        <div ";
            // line 195
            $this->displayBlock("widget_container_attributes", $context, $blocks);
            echo ">
            ";
            // line 196
            echo strtr($this->getContext($context, "date_pattern"), array("{{ year }}" =>             // line 197
$this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "year"), 'widget'), "{{ month }}" =>             // line 198
$this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "month"), 'widget'), "{{ day }}" =>             // line 199
$this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "day"), 'widget')));
            // line 200
            echo "
        </div>

    ";
        }
        // line 204
        echo "
";
    }

    // line 207
    public function block_time_widget($context, array $blocks = array())
    {
        // line 208
        ob_start();
        // line 209
        echo "    ";
        if (($this->getContext($context, "widget") == "single_text")) {
            // line 210
            echo "\t\t";
            $context["type"] = "text";
            // line 211
            echo "        ";
            $context["attr"] = twig_array_merge($this->getContext($context, "attr"), array("placeholder" => (($this->getAttribute($this->getContext($context, "attr", true), "placeholder", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getContext($context, "attr", true), "placeholder"), "Heure au format hh:mm")) : ("Heure au format hh:mm"))));
            // line 212
            echo "\t\t";
            $context["attr"] = twig_array_merge($this->getContext($context, "attr"), array("title" => (($this->getAttribute($this->getContext($context, "attr", true), "title", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getContext($context, "attr", true), "title"), "Heure au format hh:mm")) : ("Heure au format hh:mm"))));
            // line 213
            echo "        ";
            $this->displayBlock("form_widget_simple", $context, $blocks);
            echo "
    ";
        } else {
            // line 215
            echo "        ";
            $context["vars"] = ((($this->getContext($context, "widget") == "text")) ? (array("attr" => array("size" => 1))) : (array()));
            // line 216
            echo "        <div ";
            $this->displayBlock("widget_container_attributes", $context, $blocks);
            echo ">
            ";
            // line 217
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "hour"), 'widget', $this->getContext($context, "vars"));
            if ($this->getContext($context, "with_minutes")) {
                echo ":";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "minute"), 'widget', $this->getContext($context, "vars"));
            }
            if ($this->getContext($context, "with_seconds")) {
                echo ":";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "second"), 'widget', $this->getContext($context, "vars"));
            }
            // line 218
            echo "        </div>
    ";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 223
    public function block_user_selector_widget($context, array $blocks = array())
    {
        // line 224
        echo "    ";
        $context["attr"] = twig_array_merge($this->getContext($context, "attr"), array("class" => trim(((($this->getAttribute($this->getContext($context, "attr", true), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getContext($context, "attr", true), "class"), "")) : ("")) . " username-typeahead")), "autocomplete" => "off"));
        // line 225
        echo "    ";
        $this->displayBlock("form_widget_simple", $context, $blocks);
        echo "
";
    }

    // line 230
    public function block_form_row($context, array $blocks = array())
    {
        // line 231
        echo "
<div class=\"control-group\">
    ";
        // line 233
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "form"), 'label');
        echo "

    <div class=\"controls\">
    ";
        // line 236
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "form"), 'widget');
        echo "
    ";
        // line 237
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "form"), 'errors');
        echo "
    </div>
</div>

";
    }

    // line 243
    public function block_form_label($context, array $blocks = array())
    {
        // line 244
        echo "
";
        // line 245
        if ((!($this->getContext($context, "label") === false))) {
            // line 246
            echo "    ";
            if ((!$this->getContext($context, "compound"))) {
                // line 247
                echo "        ";
                $context["label_attr"] = twig_array_merge($this->getContext($context, "label_attr"), array("for" => $this->getContext($context, "id")));
                // line 248
                echo "    ";
            }
            // line 249
            echo "
    ";
            // line 250
            if (twig_test_empty($this->getContext($context, "label"))) {
                // line 251
                echo "        ";
                $context["label"] = $this->env->getExtension('form')->renderer->humanize($this->getContext($context, "name"));
                // line 252
                echo "    ";
            }
            // line 253
            echo "
    ";
            // line 255
            echo "    ";
            $context["label_attr"] = twig_array_merge($this->getContext($context, "label_attr"), array("class" => trim(((($this->getAttribute($this->getContext($context, "label_attr", true), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getContext($context, "label_attr", true), "class"), "")) : ("")) . " control-label"))));
            // line 256
            echo "
    <label";
            // line 257
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getContext($context, "label_attr"));
            foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
                echo " ";
                echo twig_escape_filter($this->env, $this->getContext($context, "attrname"), "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $this->getContext($context, "attrvalue"), "html", null, true);
                echo "\"";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo ">
        ";
            // line 258
            if ($this->getContext($context, "required")) {
                // line 259
                echo "        <i class=\"icon-asterisk\" title=\"Ce champ est obligatoire\"></i>
        ";
            }
            // line 261
            echo "        ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getContext($context, "label"), array(), $this->getContext($context, "translation_domain")), "html", null, true);
            echo "
    </label>
";
        }
        // line 264
        echo "
";
    }

    // line 267
    public function block_checkbox_row($context, array $blocks = array())
    {
        // line 268
        echo "
<div class=\"control-group\">
    <div class=\"controls\">

        ";
        // line 272
        if (twig_test_empty($this->getContext($context, "label"))) {
            // line 273
            echo "            ";
            $context["label"] = $this->env->getExtension('form')->renderer->humanize($this->getContext($context, "name"));
            // line 274
            echo "        ";
        }
        // line 275
        echo "
        ";
        // line 277
        echo "        ";
        $context["label_attr"] = twig_array_merge($this->getContext($context, "label_attr"), array("for" => $this->getContext($context, "id"), "class" => trim(((($this->getAttribute($this->getContext($context, "label_attr", true), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getContext($context, "label_attr", true), "class"), "")) : ("")) . " checkbox"))));
        // line 278
        echo "        <label";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "label_attr"));
        foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getContext($context, "attrname"), "html", null, true);
            echo "=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "attrvalue"), "html", null, true);
            echo "\"";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo ">
            ";
        // line 279
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "form"), 'widget');
        echo "
            ";
        // line 280
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getContext($context, "label"), array(), $this->getContext($context, "translation_domain")), "html", null, true);
        echo "
        </label>
        ";
        // line 282
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "form"), 'errors');
        echo "

    </div>
</div>

";
    }

    // line 290
    public function block_button_row($context, array $blocks = array())
    {
        // line 291
        echo "
<div class=\"form-actions\">
    ";
        // line 293
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "form"), 'widget');
        echo "
</div>

";
    }

    // line 299
    public function block_submit_row($context, array $blocks = array())
    {
        // line 300
        echo "
";
        // line 301
        $this->displayBlock("button_row", $context, $blocks);
        echo "

";
    }

    // line 306
    public function block_datetime_row($context, array $blocks = array())
    {
        // line 307
        echo "
<div class=\"control-group\">
    ";
        // line 309
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "form"), 'label');
        echo "

    <div class=\"controls\">
    ";
        // line 312
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "date"), 'widget');
        echo "
    ";
        // line 313
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "time"), 'widget');
        echo "
    ";
        // line 314
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "form"), 'errors');
        echo "
    </div>
</div>

";
    }

    // line 323
    public function block_form_start($context, array $blocks = array())
    {
        // line 324
        echo "
    ";
        // line 325
        $context["method"] = twig_upper_filter($this->env, $this->getContext($context, "method"));
        // line 326
        echo "    ";
        if (twig_in_filter($this->getContext($context, "method"), array(0 => "GET", 1 => "POST"))) {
            // line 327
            echo "        ";
            $context["form_method"] = $this->getContext($context, "method");
            // line 328
            echo "    ";
        } else {
            // line 329
            echo "        ";
            $context["form_method"] = "POST";
            // line 330
            echo "    ";
        }
        // line 331
        echo "
    ";
        // line 333
        echo "    ";
        $context["attr"] = twig_array_merge($this->getContext($context, "attr"), array("class" => trim(((($this->getAttribute($this->getContext($context, "attr", true), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getContext($context, "attr", true), "class"), "")) : ("")) . " form-horizontal"))));
        // line 334
        echo "    <form method=\"";
        echo twig_escape_filter($this->env, twig_lower_filter($this->env, $this->getContext($context, "form_method")), "html", null, true);
        echo "\" action=\"";
        echo twig_escape_filter($this->env, $this->getContext($context, "action"), "html", null, true);
        echo "\"";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "attr"));
        foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getContext($context, "attrname"), "html", null, true);
            echo "=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "attrvalue"), "html", null, true);
            echo "\"";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        if ($this->getContext($context, "multipart")) {
            echo " enctype=\"multipart/form-data\"";
        }
        echo ">
    ";
        // line 335
        if (($this->getContext($context, "form_method") != $this->getContext($context, "method"))) {
            // line 336
            echo "        <input type=\"hidden\" name=\"_method\" value=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "method"), "html", null, true);
            echo "\" />
    ";
        }
        // line 338
        echo "
";
    }

    // line 341
    public function block_form_errors($context, array $blocks = array())
    {
        // line 342
        echo "    ";
        if ((twig_length_filter($this->env, $this->getContext($context, "errors")) > 0)) {
            // line 343
            echo "    <p class=\"text-error\">
    <i class=\"icon-warning-sign\"></i>
    ";
            // line 345
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getContext($context, "errors"));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 346
                echo "    ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "error"), "message"), "html", null, true);
                echo "
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 348
            echo "    </p>
    ";
        }
        // line 350
        echo "
";
    }

    public function getTemplateName()
    {
        return "LaplaceCommonBundle:Form:fields.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  892 => 350,  888 => 348,  879 => 346,  875 => 345,  871 => 343,  868 => 342,  865 => 341,  860 => 338,  854 => 336,  852 => 335,  829 => 334,  826 => 333,  820 => 330,  817 => 329,  814 => 328,  808 => 326,  803 => 324,  800 => 323,  791 => 314,  787 => 313,  783 => 312,  773 => 307,  770 => 306,  763 => 301,  757 => 299,  749 => 293,  745 => 291,  742 => 290,  732 => 282,  727 => 280,  723 => 279,  707 => 278,  704 => 277,  701 => 275,  698 => 274,  695 => 273,  693 => 272,  687 => 268,  684 => 267,  679 => 264,  672 => 261,  668 => 259,  666 => 258,  651 => 257,  648 => 256,  645 => 255,  639 => 252,  636 => 251,  631 => 249,  628 => 248,  625 => 247,  622 => 246,  620 => 245,  617 => 244,  614 => 243,  605 => 237,  601 => 236,  595 => 233,  591 => 231,  588 => 230,  578 => 224,  575 => 223,  568 => 218,  558 => 217,  553 => 216,  544 => 213,  541 => 212,  538 => 211,  535 => 210,  532 => 209,  530 => 208,  527 => 207,  522 => 204,  516 => 200,  513 => 198,  512 => 197,  511 => 196,  507 => 195,  504 => 194,  498 => 192,  489 => 189,  484 => 187,  473 => 110,  468 => 108,  462 => 106,  447 => 100,  441 => 98,  433 => 95,  404 => 85,  397 => 82,  369 => 78,  367 => 77,  364 => 76,  352 => 68,  343 => 65,  333 => 62,  313 => 58,  303 => 56,  295 => 53,  286 => 48,  248 => 39,  243 => 37,  232 => 33,  236 => 80,  155 => 50,  828 => 400,  823 => 331,  816 => 393,  811 => 327,  806 => 325,  798 => 384,  795 => 383,  793 => 382,  790 => 381,  779 => 372,  777 => 309,  772 => 368,  767 => 364,  764 => 362,  760 => 300,  752 => 354,  746 => 350,  729 => 347,  722 => 346,  705 => 345,  699 => 341,  683 => 338,  681 => 337,  678 => 336,  673 => 335,  656 => 334,  642 => 253,  640 => 321,  637 => 320,  634 => 250,  630 => 316,  624 => 313,  618 => 310,  589 => 284,  586 => 283,  584 => 282,  581 => 225,  576 => 277,  570 => 274,  563 => 270,  550 => 215,  547 => 259,  545 => 258,  542 => 257,  537 => 253,  531 => 250,  524 => 246,  519 => 244,  514 => 199,  500 => 231,  497 => 230,  495 => 191,  492 => 190,  481 => 186,  458 => 206,  456 => 205,  450 => 101,  446 => 200,  439 => 97,  434 => 192,  428 => 188,  422 => 184,  416 => 87,  414 => 179,  400 => 170,  395 => 81,  390 => 80,  378 => 159,  375 => 158,  358 => 147,  353 => 144,  348 => 140,  332 => 134,  329 => 133,  316 => 128,  310 => 127,  301 => 55,  271 => 47,  250 => 95,  242 => 92,  216 => 82,  137 => 44,  81 => 13,  188 => 62,  334 => 148,  327 => 143,  315 => 59,  306 => 57,  284 => 122,  279 => 111,  274 => 119,  272 => 97,  262 => 44,  239 => 81,  206 => 85,  186 => 15,  280 => 103,  255 => 91,  245 => 88,  150 => 48,  487 => 188,  482 => 240,  475 => 236,  470 => 109,  465 => 107,  454 => 226,  452 => 225,  449 => 224,  438 => 215,  436 => 96,  426 => 208,  421 => 204,  418 => 202,  412 => 199,  406 => 196,  399 => 192,  396 => 191,  394 => 190,  391 => 189,  386 => 185,  380 => 160,  373 => 178,  363 => 149,  349 => 67,  346 => 162,  344 => 137,  336 => 63,  323 => 149,  307 => 112,  304 => 124,  302 => 109,  288 => 123,  281 => 122,  265 => 45,  263 => 110,  260 => 43,  251 => 40,  233 => 94,  223 => 89,  185 => 70,  170 => 66,  180 => 13,  174 => 65,  65 => 53,  234 => 89,  225 => 76,  237 => 35,  230 => 100,  221 => 27,  213 => 69,  207 => 24,  200 => 76,  110 => 32,  34 => 20,  335 => 135,  330 => 61,  325 => 131,  320 => 139,  318 => 101,  311 => 97,  308 => 96,  299 => 133,  297 => 121,  292 => 119,  289 => 91,  282 => 99,  275 => 100,  273 => 84,  259 => 93,  257 => 42,  249 => 102,  240 => 36,  238 => 77,  231 => 72,  222 => 75,  218 => 85,  211 => 80,  198 => 20,  194 => 63,  184 => 59,  179 => 71,  152 => 341,  77 => 7,  63 => 17,  58 => 23,  53 => 21,  227 => 99,  215 => 79,  210 => 68,  205 => 68,  226 => 75,  219 => 73,  195 => 19,  172 => 58,  202 => 62,  190 => 72,  146 => 50,  113 => 33,  59 => 23,  267 => 96,  261 => 94,  254 => 41,  244 => 106,  228 => 91,  212 => 25,  204 => 75,  197 => 88,  191 => 48,  175 => 58,  167 => 53,  159 => 7,  127 => 290,  118 => 27,  134 => 304,  129 => 297,  100 => 207,  178 => 58,  161 => 48,  104 => 20,  102 => 222,  23 => 3,  181 => 68,  153 => 53,  148 => 45,  124 => 288,  97 => 206,  90 => 20,  76 => 7,  480 => 162,  474 => 217,  469 => 158,  461 => 207,  457 => 227,  453 => 204,  444 => 99,  440 => 148,  437 => 147,  435 => 146,  430 => 210,  427 => 143,  423 => 142,  413 => 134,  409 => 176,  407 => 131,  402 => 171,  398 => 129,  393 => 166,  387 => 79,  384 => 162,  381 => 120,  379 => 119,  374 => 116,  368 => 176,  365 => 150,  362 => 110,  360 => 148,  355 => 69,  341 => 64,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 54,  294 => 103,  285 => 114,  283 => 88,  278 => 98,  268 => 46,  264 => 79,  258 => 113,  252 => 96,  247 => 94,  241 => 82,  235 => 34,  229 => 32,  224 => 28,  220 => 81,  214 => 74,  208 => 79,  169 => 60,  143 => 35,  140 => 319,  132 => 299,  128 => 29,  119 => 266,  107 => 227,  71 => 10,  177 => 59,  165 => 9,  160 => 62,  135 => 47,  126 => 41,  114 => 242,  84 => 104,  70 => 75,  67 => 73,  61 => 13,  28 => 5,  87 => 106,  201 => 21,  196 => 65,  183 => 14,  171 => 56,  166 => 55,  163 => 58,  158 => 51,  156 => 6,  151 => 49,  142 => 53,  138 => 306,  136 => 37,  121 => 35,  117 => 243,  105 => 223,  91 => 39,  62 => 51,  49 => 19,  31 => 8,  21 => 6,  25 => 8,  93 => 18,  88 => 16,  78 => 92,  44 => 10,  94 => 20,  89 => 113,  85 => 10,  75 => 90,  68 => 29,  56 => 22,  27 => 18,  38 => 1,  24 => 7,  46 => 18,  26 => 6,  19 => 2,  79 => 8,  72 => 52,  69 => 5,  47 => 11,  40 => 11,  37 => 11,  22 => 2,  246 => 38,  157 => 48,  145 => 39,  139 => 65,  131 => 41,  123 => 37,  120 => 37,  115 => 34,  111 => 25,  108 => 31,  101 => 20,  98 => 25,  96 => 16,  83 => 15,  74 => 14,  66 => 27,  55 => 19,  52 => 17,  50 => 13,  43 => 24,  41 => 3,  35 => 9,  32 => 11,  29 => 6,  209 => 87,  203 => 22,  199 => 71,  193 => 68,  189 => 66,  187 => 71,  182 => 60,  176 => 87,  173 => 54,  168 => 61,  164 => 54,  162 => 8,  154 => 47,  149 => 340,  147 => 323,  144 => 322,  141 => 45,  133 => 43,  130 => 43,  125 => 31,  122 => 267,  116 => 30,  112 => 230,  109 => 30,  106 => 26,  103 => 23,  99 => 17,  95 => 186,  92 => 185,  86 => 14,  82 => 95,  80 => 8,  73 => 76,  64 => 14,  60 => 32,  57 => 30,  54 => 27,  51 => 20,  48 => 11,  45 => 6,  42 => 13,  39 => 10,  36 => 5,  33 => 7,  30 => 10,);
    }
}
