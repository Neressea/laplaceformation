<?php

/* LaplaceUserBundle:UserProfile:view.html.twig */
class __TwigTemplate_7bbe8b682aa927dd61148ec8bae86f01 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        $context["container"] = (($this->getContext($context, "admin")) ? ("LaplaceCommonBundle::admin-page.html.twig") : ("LaplaceCommonBundle::user-page.html.twig"));
        // line 6
        echo "

";
        // line 8
        $this->env->loadTemplate("LaplaceUserBundle:UserProfile:view.html.twig", "603276993")->display(array_merge($context, array("page" => array(0 => "profile", 1 => "view"))));
    }

    public function getTemplateName()
    {
        return "LaplaceUserBundle:UserProfile:view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  134 => 37,  129 => 35,  100 => 20,  178 => 65,  161 => 56,  104 => 22,  102 => 23,  23 => 3,  181 => 63,  153 => 51,  148 => 42,  124 => 31,  97 => 17,  90 => 12,  76 => 7,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 144,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 112,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 105,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 90,  285 => 89,  283 => 88,  278 => 86,  268 => 85,  264 => 84,  258 => 81,  252 => 80,  247 => 78,  241 => 77,  235 => 74,  229 => 73,  224 => 71,  220 => 70,  214 => 69,  208 => 68,  169 => 60,  143 => 45,  140 => 55,  132 => 51,  128 => 49,  119 => 30,  107 => 25,  71 => 17,  177 => 65,  165 => 64,  160 => 61,  135 => 47,  126 => 34,  114 => 26,  84 => 13,  70 => 6,  67 => 15,  61 => 13,  28 => 3,  87 => 14,  201 => 92,  196 => 90,  183 => 70,  171 => 56,  166 => 58,  163 => 70,  158 => 67,  156 => 47,  151 => 57,  142 => 59,  138 => 38,  136 => 41,  121 => 46,  117 => 44,  105 => 40,  91 => 31,  62 => 23,  49 => 19,  31 => 5,  21 => 6,  25 => 8,  93 => 28,  88 => 11,  78 => 11,  44 => 12,  94 => 18,  89 => 12,  85 => 10,  75 => 23,  68 => 14,  56 => 9,  27 => 4,  38 => 6,  24 => 7,  46 => 7,  26 => 6,  19 => 2,  79 => 8,  72 => 16,  69 => 5,  47 => 9,  40 => 8,  37 => 10,  22 => 2,  246 => 32,  157 => 56,  145 => 46,  139 => 39,  131 => 39,  123 => 34,  120 => 33,  115 => 43,  111 => 25,  108 => 37,  101 => 32,  98 => 31,  96 => 31,  83 => 25,  74 => 14,  66 => 15,  55 => 15,  52 => 21,  50 => 10,  43 => 6,  41 => 5,  35 => 5,  32 => 4,  29 => 3,  209 => 82,  203 => 78,  199 => 67,  193 => 73,  189 => 71,  187 => 84,  182 => 66,  176 => 64,  173 => 62,  168 => 66,  164 => 52,  162 => 50,  154 => 54,  149 => 43,  147 => 58,  144 => 41,  141 => 51,  133 => 36,  130 => 35,  125 => 44,  122 => 31,  116 => 36,  112 => 42,  109 => 26,  106 => 36,  103 => 20,  99 => 19,  95 => 34,  92 => 14,  86 => 12,  82 => 9,  80 => 19,  73 => 19,  64 => 14,  60 => 6,  57 => 11,  54 => 10,  51 => 14,  48 => 8,  45 => 17,  42 => 7,  39 => 9,  36 => 5,  33 => 4,  30 => 7,);
    }
}


/* LaplaceUserBundle:UserProfile:view.html.twig */
class __TwigTemplate_7bbe8b682aa927dd61148ec8bae86f01_603276993 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'PageTitle' => array($this, 'block_PageTitle'),
            'ContentTitle' => array($this, 'block_ContentTitle'),
            'ContentBody' => array($this, 'block_ContentBody'),
        );
    }

    protected function doGetParent(array $context)
    {
        return $this->env->resolveTemplate($this->getContext($context, "container"));
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 10
    public function block_PageTitle($context, array $blocks = array())
    {
        $this->displayParentBlock("PageTitle", $context, $blocks);
        echo " - Voir le profil (";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "profile"), "fullName"), "html", null, true);
        echo ")";
    }

    // line 12
    public function block_ContentTitle($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "profile"), "fullName"), "html", null, true);
    }

    // line 14
    public function block_ContentBody($context, array $blocks = array())
    {
        // line 15
        echo "
";
        // line 16
        if ((!$this->getContext($context, "admin"))) {
            // line 17
            echo "<p>
    Les informations ci-dessous sont strictement réservées à l'utilisateur et
    au correspondant formation, qui peuvent les modifier. Elles ne sont pas
    accessibles aux autres utilisateurs.
</p>
";
        } else {
            // line 23
            echo "<p class=\"text-right\">
    <i class=\"icon-edit\"></i>
    <a href=\"";
            // line 25
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath(($this->getContext($context, "laplace_user") . "edit_profile"), array("username" => $this->getAttribute($this->getContext($context, "profile"), "username"))), "html", null, true);
            echo "\">Modifier</a>
    <br />

    <i class=\"icon-trash\"></i>
    <a href=\"";
            // line 29
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath(($this->getContext($context, "laplace_user") . "delete_profile"), array("username" => $this->getAttribute($this->getContext($context, "profile"), "username"))), "html", null, true);
            echo "\">Supprimer</a>
</p>
";
        }
        // line 32
        echo "
<h3>Identité</h3>

<table class=\"table table-striped table-bordered user-profile-table\">

    <tr>
        <th>Login</th>
        <td>";
        // line 39
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "profile"), "username"), "html", null, true);
        echo "</td>
    </tr>

    <tr>
        <th>Adresse email</th>
        <td>";
        // line 44
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "profile"), "emailAddress"), "html", null, true);
        echo "</td>
    </tr>

</table>

<hr />

<h3>Rattachement</h3>

<table class=\"table table-striped table-bordered user-profile-table\">

    <tr>
        <th>Employeur</th>
        <td>";
        // line 57
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "profile"), "tutelle"), "nom"), "html", null, true);
        echo "</td>
    </tr>

    <tr>
        <th>Site</th>
        <td>";
        // line 62
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "profile"), "site"), "name"), "html", null, true);
        echo "</td>
    </tr>

    <tr>
        <th>Téléphone professionnel 1</th>
        <td>";
        // line 67
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "profile"), "telephoneNumber1"), "html", null, true);
        echo "</td>
    </tr>

    <tr>
        <th>Téléphone professionnel 2</th>
        <td>";
        // line 72
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "profile"), "telephoneNumber2"), "html", null, true);
        echo "</td>
    </tr>

</table>

<hr />

<h3>Situation professionnelle</h3>

<table class=\"table table-striped table-bordered user-profile-table\">

    <tr>
        <th>Statut</th>
        <td>";
        // line 85
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "profile"), "userInfo"), "statut"), "intitule"), "html", null, true);
        echo "</td>
    </tr>

";
        // line 88
        if ($this->getAttribute($this->getContext($context, "profile"), "estDoctorant", array(), "method")) {
            // line 89
            echo "
    <tr>
        <th>Début de thèse</th>
        <td>";
            // line 92
            echo twig_escape_filter($this->env, twig_localized_date_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "profile"), "userInfo"), "debutThese"), "short", "none"), "html", null, true);
            echo "</td>
    </tr>

    <tr>
        <th>Responsable</th>
        <td>";
            // line 97
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "profile"), "userInfo"), "responsable"), "html", null, true);
            echo "</td>
    </tr>

    <tr>
        <th>Ecole doctorale</th>
        <td>";
            // line 102
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "profile"), "userInfo"), "ecoleDoctorale"), "html", null, true);
            echo "</td>
    </tr>

    <tr>
        <th>Bourse</th>
        <td>";
            // line 107
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "profile"), "userInfo"), "bourse"), "html", null, true);
            echo "</td>
    </tr>

";
        }
        // line 111
        echo "
</table>

<hr />

<p class=\"text-right\">
    <i class=\"icon-edit\"></i>
    ";
        // line 118
        if ($this->getContext($context, "admin")) {
            // line 119
            echo "    <a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath(($this->getContext($context, "laplace_user") . "edit_profile"), array("username" => $this->getAttribute($this->getContext($context, "profile"), "username"))), "html", null, true);
            echo "\">Modifier ces informations</a>
    <br />

    <i class=\"icon-user\"></i>
    <a href=\"";
            // line 123
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("laplace_common_user_homepage", array("_switch_user" => $this->getAttribute($this->getContext($context, "profile"), "username"))), "html", null, true);
            echo "\">Se connecter en tant que ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "profile"), "fullName"), "html", null, true);
            echo "</a>
    ";
        } else {
            // line 125
            echo "    <a href=\"";
            echo $this->env->getExtension('routing')->getPath(($this->getContext($context, "laplace_user") . "edit_my_profile"));
            echo "\">Modifier ces informations</a>
    ";
        }
        // line 127
        echo "</p>

";
    }

    public function getTemplateName()
    {
        return "LaplaceUserBundle:UserProfile:view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  267 => 127,  261 => 125,  254 => 123,  244 => 118,  228 => 107,  212 => 97,  204 => 92,  197 => 88,  191 => 85,  175 => 72,  167 => 67,  159 => 62,  127 => 39,  118 => 32,  134 => 37,  129 => 35,  100 => 20,  178 => 65,  161 => 56,  104 => 22,  102 => 23,  23 => 3,  181 => 63,  153 => 51,  148 => 42,  124 => 31,  97 => 17,  90 => 12,  76 => 7,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 144,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 112,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 105,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 90,  285 => 89,  283 => 88,  278 => 86,  268 => 85,  264 => 84,  258 => 81,  252 => 80,  247 => 78,  241 => 77,  235 => 111,  229 => 73,  224 => 71,  220 => 102,  214 => 69,  208 => 68,  169 => 60,  143 => 45,  140 => 55,  132 => 51,  128 => 49,  119 => 30,  107 => 25,  71 => 17,  177 => 65,  165 => 64,  160 => 61,  135 => 44,  126 => 34,  114 => 26,  84 => 13,  70 => 10,  67 => 15,  61 => 13,  28 => 3,  87 => 14,  201 => 92,  196 => 90,  183 => 70,  171 => 56,  166 => 58,  163 => 70,  158 => 67,  156 => 47,  151 => 57,  142 => 59,  138 => 38,  136 => 41,  121 => 46,  117 => 44,  105 => 25,  91 => 16,  62 => 23,  49 => 19,  31 => 5,  21 => 6,  25 => 8,  93 => 17,  88 => 15,  78 => 11,  44 => 12,  94 => 18,  89 => 12,  85 => 14,  75 => 23,  68 => 14,  56 => 9,  27 => 4,  38 => 6,  24 => 7,  46 => 7,  26 => 6,  19 => 2,  79 => 12,  72 => 16,  69 => 5,  47 => 9,  40 => 8,  37 => 10,  22 => 2,  246 => 119,  157 => 56,  145 => 46,  139 => 39,  131 => 39,  123 => 34,  120 => 33,  115 => 43,  111 => 25,  108 => 37,  101 => 23,  98 => 31,  96 => 31,  83 => 25,  74 => 14,  66 => 15,  55 => 15,  52 => 21,  50 => 10,  43 => 6,  41 => 5,  35 => 5,  32 => 4,  29 => 3,  209 => 82,  203 => 78,  199 => 89,  193 => 73,  189 => 71,  187 => 84,  182 => 66,  176 => 64,  173 => 62,  168 => 66,  164 => 52,  162 => 50,  154 => 54,  149 => 43,  147 => 58,  144 => 41,  141 => 51,  133 => 36,  130 => 35,  125 => 44,  122 => 31,  116 => 36,  112 => 29,  109 => 26,  106 => 36,  103 => 20,  99 => 19,  95 => 34,  92 => 14,  86 => 12,  82 => 9,  80 => 19,  73 => 19,  64 => 14,  60 => 6,  57 => 11,  54 => 10,  51 => 14,  48 => 8,  45 => 17,  42 => 7,  39 => 9,  36 => 5,  33 => 4,  30 => 7,);
    }
}
