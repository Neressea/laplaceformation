<?php

/* LaplaceTrainingBundle:Statistics:requests-summary.html.twig */
class __TwigTemplate_b4fd04bf3ab88737c5c242151f692e15 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
";
        // line 2
        $this->env->loadTemplate("LaplaceTrainingBundle:Statistics:requests-summary.html.twig", "213757199")->display(array_merge($context, array("page" => array(0 => "system", 1 => "requests-summary"))));
    }

    public function getTemplateName()
    {
        return "LaplaceTrainingBundle:Statistics:requests-summary.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  237 => 74,  230 => 70,  221 => 64,  213 => 58,  207 => 54,  200 => 52,  110 => 21,  34 => 20,  335 => 118,  330 => 101,  325 => 97,  320 => 102,  318 => 101,  311 => 97,  308 => 96,  299 => 104,  297 => 95,  292 => 92,  289 => 91,  282 => 84,  275 => 86,  273 => 84,  259 => 107,  257 => 91,  249 => 78,  240 => 109,  238 => 77,  231 => 72,  222 => 69,  218 => 67,  211 => 65,  198 => 60,  194 => 59,  184 => 46,  179 => 44,  152 => 30,  77 => 112,  63 => 17,  58 => 15,  53 => 13,  227 => 85,  215 => 79,  210 => 77,  205 => 75,  226 => 84,  219 => 80,  195 => 68,  172 => 40,  202 => 62,  190 => 95,  146 => 69,  113 => 22,  59 => 23,  267 => 80,  261 => 125,  254 => 90,  244 => 118,  228 => 107,  212 => 97,  204 => 74,  197 => 88,  191 => 48,  175 => 44,  167 => 42,  159 => 35,  127 => 60,  118 => 24,  134 => 32,  129 => 41,  100 => 17,  178 => 65,  161 => 82,  104 => 19,  102 => 7,  23 => 3,  181 => 63,  153 => 51,  148 => 42,  124 => 31,  97 => 15,  90 => 13,  76 => 7,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 144,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 112,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 105,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 90,  285 => 85,  283 => 88,  278 => 86,  268 => 85,  264 => 79,  258 => 81,  252 => 79,  247 => 78,  241 => 77,  235 => 111,  229 => 73,  224 => 71,  220 => 81,  214 => 66,  208 => 68,  169 => 60,  143 => 35,  140 => 34,  132 => 51,  128 => 29,  119 => 30,  107 => 25,  71 => 17,  177 => 59,  165 => 83,  160 => 40,  135 => 35,  126 => 34,  114 => 26,  84 => 114,  70 => 21,  67 => 15,  61 => 13,  28 => 3,  87 => 14,  201 => 92,  196 => 69,  183 => 91,  171 => 43,  166 => 58,  163 => 51,  158 => 67,  156 => 38,  151 => 57,  142 => 59,  138 => 34,  136 => 41,  121 => 46,  117 => 35,  105 => 49,  91 => 48,  62 => 23,  49 => 19,  31 => 3,  21 => 6,  25 => 5,  93 => 17,  88 => 15,  78 => 42,  44 => 10,  94 => 118,  89 => 116,  85 => 10,  75 => 53,  68 => 14,  56 => 9,  27 => 18,  38 => 7,  24 => 17,  46 => 25,  26 => 6,  19 => 1,  79 => 8,  72 => 52,  69 => 5,  47 => 17,  40 => 8,  37 => 14,  22 => 2,  246 => 77,  157 => 56,  145 => 46,  139 => 65,  131 => 29,  123 => 34,  120 => 24,  115 => 43,  111 => 25,  108 => 13,  101 => 23,  98 => 52,  96 => 16,  83 => 44,  74 => 14,  66 => 27,  55 => 14,  52 => 19,  50 => 10,  43 => 24,  41 => 7,  35 => 5,  32 => 4,  29 => 19,  209 => 76,  203 => 78,  199 => 89,  193 => 73,  189 => 65,  187 => 54,  182 => 61,  176 => 87,  173 => 62,  168 => 84,  164 => 41,  162 => 36,  154 => 54,  149 => 29,  147 => 36,  144 => 41,  141 => 51,  133 => 31,  130 => 30,  125 => 26,  122 => 25,  116 => 22,  112 => 29,  109 => 57,  106 => 20,  103 => 54,  99 => 24,  95 => 34,  92 => 14,  86 => 45,  82 => 9,  80 => 113,  73 => 19,  64 => 14,  60 => 29,  57 => 11,  54 => 27,  51 => 14,  48 => 11,  45 => 17,  42 => 7,  39 => 22,  36 => 5,  33 => 4,  30 => 7,);
    }
}


/* LaplaceTrainingBundle:Statistics:requests-summary.html.twig */
class __TwigTemplate_b4fd04bf3ab88737c5c242151f692e15_213757199 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("LaplaceCommonBundle::admin-page.html.twig");

        $this->blocks = array(
            'PageTitle' => array($this, 'block_PageTitle'),
            'ContentTitle' => array($this, 'block_ContentTitle'),
            'ContentBody' => array($this, 'block_ContentBody'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "LaplaceCommonBundle::admin-page.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_PageTitle($context, array $blocks = array())
    {
        $this->displayParentBlock("PageTitle", $context, $blocks);
        echo " - Bilan global de réalisation";
    }

    // line 7
    public function block_ContentTitle($context, array $blocks = array())
    {
        echo "Bilan global de réalisation";
    }

    // line 9
    public function block_ContentBody($context, array $blocks = array())
    {
        // line 10
        echo "


";
        // line 13
        if (array_key_exists("summary", $context)) {
            // line 14
            echo "
<p>
    Du <strong>";
            // line 16
            echo twig_escape_filter($this->env, twig_localized_date_filter($this->env, $this->getAttribute($this->getContext($context, "summary"), "from"), "full", "short"), "html", null, true);
            echo "</strong> au
    <strong>";
            // line 17
            echo twig_escape_filter($this->env, twig_localized_date_filter($this->env, $this->getAttribute($this->getContext($context, "summary"), "to"), "full", "short"), "html", null, true);
            echo "</strong>.
</p>

";
            // line 20
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "summary"), "types"));
            foreach ($context['_seq'] as $context["type"] => $context["section"]) {
                // line 21
                echo "
<h5>";
                // line 22
                echo twig_escape_filter($this->env, $this->getContext($context, "type"), "html", null, true);
                echo "</h5>

    ";
                // line 24
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "section"), "categories"));
                foreach ($context['_seq'] as $context["category"] => $context["table"]) {
                    // line 25
                    echo "
    <table class=\"table table-striped table-bordered needs-summary-table\">
        <tr>
            <th class=\"needs-summary-cat\">
                ";
                    // line 29
                    if (twig_test_empty($this->getContext($context, "category"))) {
                        // line 30
                        echo "                <em>Non classés</em>
                ";
                    } else {
                        // line 32
                        echo "                ";
                        echo twig_escape_filter($this->env, $this->getContext($context, "category"), "html", null, true);
                        echo "
                ";
                    }
                    // line 34
                    echo "            </th>
            ";
                    // line 35
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "summary"), "columns"));
                    foreach ($context['_seq'] as $context["_key"] => $context["year"]) {
                        // line 36
                        echo "            <th class=\"needs-summary-tutelle\">";
                        echo twig_escape_filter($this->env, $this->getContext($context, "year"), "html", null, true);
                        echo "</th>
            ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['year'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 38
                    echo "        </tr>

        ";
                    // line 40
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "table"), "rows"));
                    foreach ($context['_seq'] as $context["row"] => $context["counters"]) {
                        // line 41
                        echo "        <tr>
            <td>";
                        // line 42
                        echo twig_escape_filter($this->env, $this->getContext($context, "row"), "html", null, true);
                        echo "</td>
            ";
                        // line 43
                        $context['_parent'] = (array) $context;
                        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "summary"), "columns"));
                        foreach ($context['_seq'] as $context["_key"] => $context["year"]) {
                            // line 44
                            echo "            <td>";
                            echo twig_escape_filter($this->env, (($this->getAttribute($this->getContext($context, "counters", true), "get", array(0 => $this->getContext($context, "year")), "method", true, true)) ? (_twig_default_filter($this->getAttribute($this->getContext($context, "counters", true), "get", array(0 => $this->getContext($context, "year")), "method"), "")) : ("")), "html", null, true);
                            echo "</td>
            ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['year'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 46
                        echo "        </tr>
        ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['row'], $context['counters'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 48
                    echo "
    </table>

    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['category'], $context['table'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 52
                echo "
";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['type'], $context['section'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 54
            echo "
<hr />

";
        }
        // line 58
        echo "
<h3>Générer un bilan</h3>

<p>
    Cet outil permet de générer un résumé des demandes réalisées sur la période demandée.
    Seules les demandes validées et pour lesquelles la date de la formation a été renseignée
    seront incluses. Pour qu'une inscription soit comptabilisée, elle doit être acceptée et
    l'utilisateur doit avoir assisté à la formation.
</p>

";
        // line 68
        echo         $this->env->getExtension('form')->renderer->renderBlock($this->getContext($context, "summary_form"), 'form_start');
        echo "

<fielset>

    <legend>Choisir une période</legend>

    ";
        // line 74
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "summary_form"), 'rest');
        echo "

</fieldset>

";
        // line 78
        echo         $this->env->getExtension('form')->renderer->renderBlock($this->getContext($context, "summary_form"), 'form_end');
        echo "

";
    }

    public function getTemplateName()
    {
        return "LaplaceTrainingBundle:Statistics:requests-summary.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  234 => 74,  225 => 68,  237 => 74,  230 => 70,  221 => 64,  213 => 58,  207 => 54,  200 => 52,  110 => 21,  34 => 20,  335 => 118,  330 => 101,  325 => 97,  320 => 102,  318 => 101,  311 => 97,  308 => 96,  299 => 104,  297 => 95,  292 => 92,  289 => 91,  282 => 84,  275 => 86,  273 => 84,  259 => 107,  257 => 91,  249 => 78,  240 => 109,  238 => 77,  231 => 72,  222 => 69,  218 => 67,  211 => 65,  198 => 60,  194 => 59,  184 => 46,  179 => 44,  152 => 30,  77 => 112,  63 => 17,  58 => 15,  53 => 13,  227 => 85,  215 => 79,  210 => 77,  205 => 75,  226 => 84,  219 => 80,  195 => 68,  172 => 40,  202 => 62,  190 => 95,  146 => 69,  113 => 22,  59 => 23,  267 => 80,  261 => 125,  254 => 90,  244 => 118,  228 => 107,  212 => 97,  204 => 74,  197 => 88,  191 => 48,  175 => 44,  167 => 42,  159 => 35,  127 => 60,  118 => 24,  134 => 32,  129 => 41,  100 => 17,  178 => 65,  161 => 82,  104 => 19,  102 => 7,  23 => 3,  181 => 63,  153 => 51,  148 => 42,  124 => 31,  97 => 15,  90 => 13,  76 => 7,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 144,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 112,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 105,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 90,  285 => 85,  283 => 88,  278 => 86,  268 => 85,  264 => 79,  258 => 81,  252 => 79,  247 => 78,  241 => 78,  235 => 111,  229 => 73,  224 => 71,  220 => 81,  214 => 66,  208 => 68,  169 => 60,  143 => 35,  140 => 34,  132 => 51,  128 => 29,  119 => 30,  107 => 25,  71 => 17,  177 => 59,  165 => 83,  160 => 40,  135 => 35,  126 => 34,  114 => 26,  84 => 114,  70 => 21,  67 => 15,  61 => 13,  28 => 3,  87 => 14,  201 => 92,  196 => 69,  183 => 91,  171 => 43,  166 => 58,  163 => 51,  158 => 67,  156 => 38,  151 => 57,  142 => 59,  138 => 34,  136 => 41,  121 => 46,  117 => 35,  105 => 49,  91 => 48,  62 => 23,  49 => 19,  31 => 3,  21 => 6,  25 => 5,  93 => 17,  88 => 15,  78 => 42,  44 => 10,  94 => 118,  89 => 116,  85 => 10,  75 => 53,  68 => 14,  56 => 9,  27 => 18,  38 => 7,  24 => 17,  46 => 25,  26 => 6,  19 => 1,  79 => 8,  72 => 52,  69 => 5,  47 => 17,  40 => 8,  37 => 14,  22 => 2,  246 => 77,  157 => 56,  145 => 46,  139 => 65,  131 => 29,  123 => 34,  120 => 24,  115 => 43,  111 => 25,  108 => 13,  101 => 23,  98 => 52,  96 => 16,  83 => 44,  74 => 14,  66 => 27,  55 => 14,  52 => 19,  50 => 10,  43 => 24,  41 => 7,  35 => 5,  32 => 4,  29 => 19,  209 => 76,  203 => 78,  199 => 89,  193 => 73,  189 => 65,  187 => 54,  182 => 61,  176 => 87,  173 => 62,  168 => 84,  164 => 41,  162 => 36,  154 => 54,  149 => 29,  147 => 36,  144 => 41,  141 => 51,  133 => 31,  130 => 30,  125 => 26,  122 => 25,  116 => 22,  112 => 29,  109 => 57,  106 => 20,  103 => 54,  99 => 24,  95 => 34,  92 => 14,  86 => 45,  82 => 9,  80 => 113,  73 => 19,  64 => 14,  60 => 29,  57 => 11,  54 => 27,  51 => 14,  48 => 11,  45 => 17,  42 => 7,  39 => 22,  36 => 5,  33 => 4,  30 => 7,);
    }
}
