<?php

/* LaplaceUserBundle:UserProfile:create.html.twig */
class __TwigTemplate_e6f6d7d7b297cf61208df77b948faf18 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
";
        // line 2
        $this->env->loadTemplate("LaplaceUserBundle:UserProfile:create.html.twig", "823156139")->display(array_merge($context, array("tab" => "new-profile")));
    }

    public function getTemplateName()
    {
        return "LaplaceUserBundle:UserProfile:create.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 144,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 112,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 105,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 90,  285 => 89,  283 => 88,  278 => 86,  268 => 85,  264 => 84,  258 => 81,  252 => 80,  247 => 78,  241 => 77,  235 => 74,  229 => 73,  224 => 71,  220 => 70,  214 => 69,  208 => 68,  169 => 60,  143 => 56,  140 => 55,  132 => 51,  128 => 49,  119 => 42,  107 => 36,  71 => 17,  177 => 65,  165 => 64,  160 => 61,  135 => 47,  126 => 45,  114 => 42,  84 => 28,  70 => 20,  67 => 15,  61 => 13,  28 => 3,  87 => 20,  201 => 92,  196 => 90,  183 => 70,  171 => 61,  166 => 71,  163 => 70,  158 => 67,  156 => 58,  151 => 57,  142 => 59,  138 => 57,  136 => 56,  121 => 46,  117 => 44,  105 => 40,  91 => 31,  62 => 23,  49 => 19,  31 => 5,  21 => 2,  25 => 4,  93 => 28,  88 => 6,  78 => 21,  44 => 12,  94 => 22,  89 => 20,  85 => 25,  75 => 23,  68 => 14,  56 => 9,  27 => 4,  38 => 6,  24 => 4,  46 => 7,  26 => 6,  19 => 1,  79 => 18,  72 => 16,  69 => 25,  47 => 9,  40 => 8,  37 => 10,  22 => 2,  246 => 32,  157 => 56,  145 => 46,  139 => 50,  131 => 42,  123 => 47,  120 => 40,  115 => 43,  111 => 37,  108 => 37,  101 => 32,  98 => 31,  96 => 31,  83 => 25,  74 => 14,  66 => 15,  55 => 15,  52 => 21,  50 => 10,  43 => 6,  41 => 5,  35 => 5,  32 => 4,  29 => 3,  209 => 82,  203 => 78,  199 => 67,  193 => 73,  189 => 71,  187 => 84,  182 => 66,  176 => 64,  173 => 74,  168 => 66,  164 => 59,  162 => 62,  154 => 54,  149 => 51,  147 => 58,  144 => 53,  141 => 51,  133 => 55,  130 => 41,  125 => 44,  122 => 43,  116 => 36,  112 => 42,  109 => 41,  106 => 36,  103 => 37,  99 => 30,  95 => 34,  92 => 33,  86 => 28,  82 => 22,  80 => 19,  73 => 19,  64 => 14,  60 => 6,  57 => 11,  54 => 10,  51 => 14,  48 => 8,  45 => 17,  42 => 7,  39 => 9,  36 => 5,  33 => 4,  30 => 7,);
    }
}


/* LaplaceUserBundle:UserProfile:create.html.twig */
class __TwigTemplate_e6f6d7d7b297cf61208df77b948faf18_823156139 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("LaplaceCommonBundle::common-page.html.twig");

        $this->blocks = array(
            'PageTitle' => array($this, 'block_PageTitle'),
            'ContentTitle' => array($this, 'block_ContentTitle'),
            'ContentBody' => array($this, 'block_ContentBody'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "LaplaceCommonBundle::common-page.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_PageTitle($context, array $blocks = array())
    {
        $this->displayParentBlock("PageTitle", $context, $blocks);
        echo " - Inscription";
    }

    // line 6
    public function block_ContentTitle($context, array $blocks = array())
    {
        echo "Inscription";
    }

    // line 8
    public function block_ContentBody($context, array $blocks = array())
    {
        // line 9
        echo "
";
        // line 10
        if ((!array_key_exists("new_profile_form", $context))) {
            // line 11
            echo "
<p class=\"lead\">
    Cliquez sur le lien correspondant à votre situation :
    <ul>
        <li>
            <a href=\"";
            // line 16
            echo $this->env->getExtension('routing')->getPath("laplace_user_new_profile_docto");
            echo "\">Vous êtes un doctorant</a>
        </li>
        <li>
            <a href=\"";
            // line 19
            echo $this->env->getExtension('routing')->getPath("laplace_user_new_profile_agent");
            echo "\">Vous êtes un agent</a>
        </li>
    </ul>
</p>
";
        } else {
            // line 24
            echo "
";
            // line 25
            echo             $this->env->getExtension('form')->renderer->renderBlock($this->getContext($context, "new_profile_form"), 'form_start');
            echo "

<fieldset>
    <legend>Nouveau profil</legend>

    ";
            // line 30
            if (array_key_exists("bad_credentials", $context)) {
                // line 31
                echo "    <p class=\"text-warning\">
        Vos identifiants ne sont pas valides. Veuillez essayer à nouveau.
    </p>
    ";
            }
            // line 35
            echo "
    ";
            // line 36
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getContext($context, "new_profile_form"), "identity"), "username"), 'row');
            echo "

    ";
            // line 38
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "new_profile_form"), "password"), 'row');
            echo "

    ";
            // line 40
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "new_profile_form"), "identity"), 'rest');
            echo "

    ";
            // line 42
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "new_profile_form"), "user"), 'rest');
            echo "

    ";
            // line 44
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "new_profile_form"), "userinfo"), 'rest');
            echo "

    <p>
        Vous pouvez laisser un message d'informations complémentaires pour
        le correspondant formation.
        Utilisez ce champ s'il manque un choix dans les champs ci-dessus.
    </p>

    ";
            // line 52
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "new_profile_form"), 'rest');
            echo "

</fieldset>

";
            // line 56
            echo             $this->env->getExtension('form')->renderer->renderBlock($this->getContext($context, "new_profile_form"), 'form_end');
            echo "

<p>
    <i class=\"icon-asterisk\"></i> : champ obligatoire
</p>

";
        }
        // line 63
        echo "
";
    }

    public function getTemplateName()
    {
        return "LaplaceUserBundle:UserProfile:create.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  181 => 63,  153 => 44,  148 => 42,  124 => 31,  97 => 16,  90 => 11,  76 => 6,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 144,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 112,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 105,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 90,  285 => 89,  283 => 88,  278 => 86,  268 => 85,  264 => 84,  258 => 81,  252 => 80,  247 => 78,  241 => 77,  235 => 74,  229 => 73,  224 => 71,  220 => 70,  214 => 69,  208 => 68,  169 => 60,  143 => 40,  140 => 55,  132 => 51,  128 => 49,  119 => 42,  107 => 36,  71 => 17,  177 => 65,  165 => 64,  160 => 61,  135 => 47,  126 => 45,  114 => 25,  84 => 28,  70 => 20,  67 => 15,  61 => 13,  28 => 3,  87 => 20,  201 => 92,  196 => 90,  183 => 70,  171 => 56,  166 => 71,  163 => 70,  158 => 67,  156 => 58,  151 => 57,  142 => 59,  138 => 38,  136 => 56,  121 => 46,  117 => 44,  105 => 40,  91 => 31,  62 => 23,  49 => 19,  31 => 5,  21 => 2,  25 => 4,  93 => 28,  88 => 10,  78 => 21,  44 => 12,  94 => 22,  89 => 20,  85 => 9,  75 => 23,  68 => 14,  56 => 9,  27 => 4,  38 => 6,  24 => 4,  46 => 7,  26 => 6,  19 => 1,  79 => 18,  72 => 16,  69 => 4,  47 => 9,  40 => 8,  37 => 10,  22 => 2,  246 => 32,  157 => 56,  145 => 46,  139 => 50,  131 => 42,  123 => 47,  120 => 40,  115 => 43,  111 => 24,  108 => 37,  101 => 32,  98 => 31,  96 => 31,  83 => 25,  74 => 14,  66 => 15,  55 => 15,  52 => 21,  50 => 10,  43 => 6,  41 => 5,  35 => 5,  32 => 4,  29 => 3,  209 => 82,  203 => 78,  199 => 67,  193 => 73,  189 => 71,  187 => 84,  182 => 66,  176 => 64,  173 => 74,  168 => 66,  164 => 52,  162 => 62,  154 => 54,  149 => 51,  147 => 58,  144 => 53,  141 => 51,  133 => 36,  130 => 35,  125 => 44,  122 => 30,  116 => 36,  112 => 42,  109 => 41,  106 => 36,  103 => 19,  99 => 30,  95 => 34,  92 => 33,  86 => 28,  82 => 8,  80 => 19,  73 => 19,  64 => 14,  60 => 6,  57 => 11,  54 => 10,  51 => 14,  48 => 8,  45 => 17,  42 => 7,  39 => 9,  36 => 5,  33 => 4,  30 => 7,);
    }
}
