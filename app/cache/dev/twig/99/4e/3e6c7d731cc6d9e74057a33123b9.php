<?php

/* LaplaceTrainingBundle:Need:view-all.html.twig */
class __TwigTemplate_994e3e6c7d731cc6d9e74057a33123b9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        $context["container"] = (($this->getContext($context, "admin")) ? ("LaplaceCommonBundle::admin-page.html.twig") : ("LaplaceCommonBundle::user-page.html.twig"));
        // line 6
        echo "

";
        // line 8
        $this->env->loadTemplate("LaplaceTrainingBundle:Need:view-all.html.twig", "1694549702")->display(array_merge($context, array("page" => array(0 => "need", 1 => "all"))));
    }

    public function getTemplateName()
    {
        return "LaplaceTrainingBundle:Need:view-all.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  487 => 243,  482 => 240,  475 => 236,  470 => 234,  465 => 232,  454 => 226,  452 => 225,  449 => 224,  438 => 215,  436 => 214,  426 => 208,  421 => 204,  418 => 202,  412 => 199,  406 => 196,  399 => 192,  396 => 191,  394 => 190,  391 => 189,  386 => 185,  380 => 182,  373 => 178,  363 => 174,  349 => 163,  346 => 162,  344 => 161,  336 => 156,  323 => 149,  307 => 136,  304 => 135,  302 => 134,  288 => 126,  281 => 122,  265 => 111,  263 => 110,  260 => 109,  251 => 103,  233 => 94,  223 => 89,  185 => 72,  170 => 66,  180 => 58,  174 => 54,  65 => 28,  234 => 74,  225 => 68,  237 => 74,  230 => 70,  221 => 64,  213 => 58,  207 => 54,  200 => 52,  110 => 25,  34 => 20,  335 => 118,  330 => 153,  325 => 97,  320 => 102,  318 => 101,  311 => 97,  308 => 96,  299 => 133,  297 => 95,  292 => 92,  289 => 91,  282 => 84,  275 => 86,  273 => 84,  259 => 107,  257 => 107,  249 => 102,  240 => 98,  238 => 77,  231 => 72,  222 => 69,  218 => 85,  211 => 65,  198 => 77,  194 => 75,  184 => 46,  179 => 71,  152 => 46,  77 => 7,  63 => 17,  58 => 24,  53 => 21,  227 => 85,  215 => 79,  210 => 77,  205 => 75,  226 => 90,  219 => 80,  195 => 68,  172 => 40,  202 => 62,  190 => 95,  146 => 69,  113 => 22,  59 => 23,  267 => 80,  261 => 125,  254 => 90,  244 => 118,  228 => 91,  212 => 97,  204 => 79,  197 => 88,  191 => 48,  175 => 69,  167 => 42,  159 => 35,  127 => 39,  118 => 24,  134 => 47,  129 => 41,  100 => 45,  178 => 65,  161 => 63,  104 => 24,  102 => 32,  23 => 3,  181 => 63,  153 => 51,  148 => 55,  124 => 56,  97 => 30,  90 => 19,  76 => 7,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 227,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 210,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 176,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 160,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 129,  285 => 85,  283 => 88,  278 => 86,  268 => 112,  264 => 79,  258 => 81,  252 => 79,  247 => 78,  241 => 78,  235 => 95,  229 => 73,  224 => 71,  220 => 81,  214 => 82,  208 => 68,  169 => 60,  143 => 35,  140 => 50,  132 => 51,  128 => 29,  119 => 39,  107 => 28,  71 => 30,  177 => 59,  165 => 83,  160 => 40,  135 => 42,  126 => 34,  114 => 36,  84 => 16,  70 => 10,  67 => 15,  61 => 13,  28 => 3,  87 => 17,  201 => 78,  196 => 69,  183 => 91,  171 => 43,  166 => 65,  163 => 64,  158 => 67,  156 => 38,  151 => 57,  142 => 59,  138 => 34,  136 => 48,  121 => 46,  117 => 38,  105 => 23,  91 => 48,  62 => 27,  49 => 19,  31 => 11,  21 => 6,  25 => 8,  93 => 42,  88 => 16,  78 => 12,  44 => 10,  94 => 118,  89 => 12,  85 => 10,  75 => 53,  68 => 29,  56 => 9,  27 => 18,  38 => 7,  24 => 7,  46 => 18,  26 => 6,  19 => 2,  79 => 8,  72 => 52,  69 => 5,  47 => 17,  40 => 8,  37 => 14,  22 => 2,  246 => 101,  157 => 56,  145 => 46,  139 => 65,  131 => 45,  123 => 37,  120 => 29,  115 => 32,  111 => 25,  108 => 33,  101 => 20,  98 => 19,  96 => 16,  83 => 37,  74 => 14,  66 => 27,  55 => 23,  52 => 19,  50 => 10,  43 => 24,  41 => 7,  35 => 5,  32 => 4,  29 => 19,  209 => 76,  203 => 78,  199 => 89,  193 => 73,  189 => 65,  187 => 54,  182 => 61,  176 => 87,  173 => 68,  168 => 84,  164 => 41,  162 => 49,  154 => 58,  149 => 29,  147 => 44,  144 => 41,  141 => 43,  133 => 31,  130 => 30,  125 => 43,  122 => 25,  116 => 22,  112 => 30,  109 => 28,  106 => 48,  103 => 54,  99 => 31,  95 => 17,  92 => 26,  86 => 12,  82 => 9,  80 => 8,  73 => 19,  64 => 14,  60 => 25,  57 => 11,  54 => 27,  51 => 20,  48 => 11,  45 => 17,  42 => 17,  39 => 16,  36 => 5,  33 => 12,  30 => 7,);
    }
}


/* LaplaceTrainingBundle:Need:view-all.html.twig */
class __TwigTemplate_994e3e6c7d731cc6d9e74057a33123b9_1694549702 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'PageTitle' => array($this, 'block_PageTitle'),
            'ContentTitle' => array($this, 'block_ContentTitle'),
            'Sidebar' => array($this, 'block_Sidebar'),
            'ContentBody' => array($this, 'block_ContentBody'),
        );
    }

    protected function doGetParent(array $context)
    {
        return $this->env->resolveTemplate($this->getContext($context, "container"));
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 10
    public function block_PageTitle($context, array $blocks = array())
    {
        $this->displayParentBlock("PageTitle", $context, $blocks);
        echo " - Tous les besoins";
    }

    // line 14
    public function block_ContentTitle($context, array $blocks = array())
    {
        echo "Tous les besoins";
    }

    // line 18
    public function block_Sidebar($context, array $blocks = array())
    {
        // line 19
        echo "
";
        // line 20
        $this->displayParentBlock("Sidebar", $context, $blocks);
        echo "

<li class=\"divider\"></li>

<li class=\"nav-header\">Domaines de connaissances</li>
";
        // line 25
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "domains"));
        foreach ($context['_seq'] as $context["_key"] => $context["domain"]) {
            // line 26
            echo "<li>
    <a href=\"#dom_";
            // line 27
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "domain"), "id"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "domain"), "name"), "html", null, true);
            echo "</a>
</li>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['domain'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 30
        echo "
";
    }

    // line 35
    public function block_ContentBody($context, array $blocks = array())
    {
        // line 36
        echo "
<p>
    Cliquez sur un besoin de formation pour <strong>voir sa description complète</strong> ou pour <strong>l'ajouter
    à votre liste</strong> de besoins personnels. Si un besoin ne figure pas dans cette
    liste, vous pouvez en <a href=\"";
        // line 40
        echo $this->env->getExtension('routing')->getPath(($this->getContext($context, "laplace_training") . "create_need"));
        echo "\">créer un nouveau</a>.
</p>

";
        // line 43
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "domains"));
        foreach ($context['_seq'] as $context["_key"] => $context["domain"]) {
            // line 44
            echo "<h3 id=\"dom_";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "domain"), "id"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "domain"), "name"), "html", null, true);
            echo "</h3>

<table class=\"table table-striped table-bordered table-condensed cat-table\">

    ";
            // line 48
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "domain"), "categories"));
            foreach ($context['_seq'] as $context["_key"] => $context["cat"]) {
                // line 49
                echo "    <tr>
        <th>";
                // line 50
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "cat"), "name"), "html", null, true);
                echo "</th>
    </tr>

    ";
                // line 53
                if (($this->getAttribute($this->getContext($context, "groups", true), $this->getAttribute($this->getContext($context, "cat"), "id"), array(), "array", true, true) && (!twig_test_empty($this->getAttribute($this->getContext($context, "groups"), $this->getAttribute($this->getContext($context, "cat"), "id"), array(), "array"))))) {
                    // line 54
                    echo "    ";
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "groups"), $this->getAttribute($this->getContext($context, "cat"), "id"), array(), "array"));
                    foreach ($context['_seq'] as $context["_key"] => $context["needinfo"]) {
                        // line 55
                        echo "    <tr>
        <td>
            <span class=\"badge";
                        // line 57
                        if (($this->getAttribute($this->getContext($context, "needinfo"), "subscriptionCount") > 0)) {
                            echo " badge-info";
                        }
                        echo "\"
                  title=\"Nombre de personnes intéressées\">";
                        // line 58
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "needinfo"), "subscriptionCount"), "html", null, true);
                        echo "</span>
            <a href=\"";
                        // line 59
                        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath(($this->getContext($context, "laplace_training") . "view_need"), array("id" => $this->getAttribute($this->getAttribute($this->getContext($context, "needinfo"), "need"), "id"))), "html", null, true);
                        echo "\">";
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "needinfo"), "need"), "title"), "html", null, true);
                        echo "</a>
        </td>
    </tr>
    ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['needinfo'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 63
                    echo "    ";
                } else {
                    // line 64
                    echo "    <tr>
        <td><em>Vide</em></td>
    </tr>
    ";
                }
                // line 68
                echo "
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['cat'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 70
            echo "
</table>

<hr />
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['domain'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 75
        echo "
";
        // line 76
        if ((!twig_test_empty($this->getAttribute($this->getContext($context, "groups"), null, array(), "array")))) {
            // line 77
            echo "<h3 id=\"dom_other\">Autre</h3>

<table class=\"table table-striped table-bordered table-condensed cat-table\">

    <tr>
        <th>Besoins non classés</th>
    </tr>

    ";
            // line 85
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "groups"), null, array(), "array"));
            foreach ($context['_seq'] as $context["_key"] => $context["needinfo"]) {
                // line 86
                echo "    <tr>
        <td>
            <span class=\"badge";
                // line 88
                if (($this->getAttribute($this->getContext($context, "needinfo"), "subscriptionCount") > 0)) {
                    echo " badge-info";
                }
                echo "\"
                  title=\"Nombre de personnes intéressées\">";
                // line 89
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "needinfo"), "subscriptionCount"), "html", null, true);
                echo "</span>
            <a href=\"";
                // line 90
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath(($this->getContext($context, "laplace_training") . "view_need"), array("id" => $this->getAttribute($this->getAttribute($this->getContext($context, "needinfo"), "need"), "id"))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "needinfo"), "need"), "title"), "html", null, true);
                echo "</a>
        </td>
    </tr>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['needinfo'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 94
            echo "
</table>

<hr />

";
        }
        // line 100
        echo "
<p class=\"text-right\">
    <i class=\"icon-plus\"></i>
    <a href=\"";
        // line 103
        echo $this->env->getExtension('routing')->getPath(($this->getContext($context, "laplace_training") . "create_need"));
        echo "\">Ajouter un besoin qui ne figure pas dans cette liste</a>
</p>

<div class=\"clearfix\"></div>

";
    }

    public function getTemplateName()
    {
        return "LaplaceTrainingBundle:Need:view-all.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  280 => 103,  255 => 90,  245 => 88,  150 => 48,  487 => 243,  482 => 240,  475 => 236,  470 => 234,  465 => 232,  454 => 226,  452 => 225,  449 => 224,  438 => 215,  436 => 214,  426 => 208,  421 => 204,  418 => 202,  412 => 199,  406 => 196,  399 => 192,  396 => 191,  394 => 190,  391 => 189,  386 => 185,  380 => 182,  373 => 178,  363 => 174,  349 => 163,  346 => 162,  344 => 161,  336 => 156,  323 => 149,  307 => 136,  304 => 135,  302 => 134,  288 => 126,  281 => 122,  265 => 111,  263 => 110,  260 => 109,  251 => 89,  233 => 94,  223 => 89,  185 => 72,  170 => 55,  180 => 58,  174 => 57,  65 => 28,  234 => 74,  225 => 76,  237 => 85,  230 => 70,  221 => 64,  213 => 58,  207 => 54,  200 => 52,  110 => 25,  34 => 20,  335 => 118,  330 => 153,  325 => 97,  320 => 102,  318 => 101,  311 => 97,  308 => 96,  299 => 133,  297 => 95,  292 => 92,  289 => 91,  282 => 84,  275 => 100,  273 => 84,  259 => 107,  257 => 107,  249 => 102,  240 => 98,  238 => 77,  231 => 72,  222 => 75,  218 => 85,  211 => 65,  198 => 77,  194 => 75,  184 => 59,  179 => 71,  152 => 46,  77 => 7,  63 => 17,  58 => 24,  53 => 21,  227 => 77,  215 => 79,  210 => 77,  205 => 68,  226 => 90,  219 => 80,  195 => 68,  172 => 40,  202 => 62,  190 => 95,  146 => 69,  113 => 22,  59 => 23,  267 => 94,  261 => 125,  254 => 90,  244 => 118,  228 => 91,  212 => 70,  204 => 79,  197 => 88,  191 => 48,  175 => 69,  167 => 42,  159 => 35,  127 => 39,  118 => 24,  134 => 47,  129 => 41,  100 => 45,  178 => 65,  161 => 63,  104 => 24,  102 => 26,  23 => 3,  181 => 63,  153 => 51,  148 => 55,  124 => 36,  97 => 30,  90 => 20,  76 => 7,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 227,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 210,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 176,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 160,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 129,  285 => 85,  283 => 88,  278 => 86,  268 => 112,  264 => 79,  258 => 81,  252 => 79,  247 => 78,  241 => 86,  235 => 95,  229 => 73,  224 => 71,  220 => 81,  214 => 82,  208 => 68,  169 => 60,  143 => 35,  140 => 44,  132 => 51,  128 => 29,  119 => 39,  107 => 28,  71 => 10,  177 => 59,  165 => 54,  160 => 40,  135 => 42,  126 => 34,  114 => 36,  84 => 18,  70 => 10,  67 => 15,  61 => 13,  28 => 3,  87 => 19,  201 => 78,  196 => 63,  183 => 91,  171 => 43,  166 => 65,  163 => 53,  158 => 67,  156 => 38,  151 => 57,  142 => 59,  138 => 34,  136 => 43,  121 => 35,  117 => 38,  105 => 27,  91 => 48,  62 => 27,  49 => 19,  31 => 11,  21 => 6,  25 => 8,  93 => 42,  88 => 16,  78 => 14,  44 => 10,  94 => 118,  89 => 12,  85 => 10,  75 => 53,  68 => 29,  56 => 9,  27 => 18,  38 => 7,  24 => 7,  46 => 18,  26 => 6,  19 => 2,  79 => 8,  72 => 52,  69 => 5,  47 => 17,  40 => 8,  37 => 14,  22 => 2,  246 => 101,  157 => 50,  145 => 46,  139 => 65,  131 => 45,  123 => 37,  120 => 29,  115 => 32,  111 => 25,  108 => 33,  101 => 20,  98 => 25,  96 => 16,  83 => 37,  74 => 14,  66 => 27,  55 => 23,  52 => 19,  50 => 10,  43 => 24,  41 => 7,  35 => 5,  32 => 4,  29 => 19,  209 => 76,  203 => 78,  199 => 64,  193 => 73,  189 => 65,  187 => 54,  182 => 61,  176 => 87,  173 => 68,  168 => 84,  164 => 41,  162 => 49,  154 => 49,  149 => 29,  147 => 44,  144 => 41,  141 => 43,  133 => 31,  130 => 40,  125 => 43,  122 => 25,  116 => 30,  112 => 30,  109 => 28,  106 => 48,  103 => 54,  99 => 31,  95 => 17,  92 => 26,  86 => 12,  82 => 9,  80 => 8,  73 => 19,  64 => 14,  60 => 25,  57 => 11,  54 => 27,  51 => 20,  48 => 11,  45 => 17,  42 => 17,  39 => 16,  36 => 5,  33 => 12,  30 => 7,);
    }
}
