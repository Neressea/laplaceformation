<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appDevUrlMatcher
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);

        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_wdt')), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if (rtrim($pathinfo, '/') === '/_profiler') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_profiler_home');
                    }

                    return array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                }

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ($pathinfo === '/_profiler/search') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ($pathinfo === '/_profiler/search_bar') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_purge
                if ($pathinfo === '/_profiler/purge') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:purgeAction',  '_route' => '_profiler_purge',);
                }

                if (0 === strpos($pathinfo, '/_profiler/i')) {
                    // _profiler_info
                    if (0 === strpos($pathinfo, '/_profiler/info') && preg_match('#^/_profiler/info/(?P<about>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_info')), array (  '_controller' => 'web_profiler.controller.profiler:infoAction',));
                    }

                    // _profiler_import
                    if ($pathinfo === '/_profiler/import') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:importAction',  '_route' => '_profiler_import',);
                    }

                }

                // _profiler_export
                if (0 === strpos($pathinfo, '/_profiler/export') && preg_match('#^/_profiler/export/(?P<token>[^/\\.]++)\\.txt$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_export')), array (  '_controller' => 'web_profiler.controller.profiler:exportAction',));
                }

                // _profiler_phpinfo
                if ($pathinfo === '/_profiler/phpinfo') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_search_results')), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler')), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_router')), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception')), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception_css')), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

            if (0 === strpos($pathinfo, '/_configurator')) {
                // _configurator_home
                if (rtrim($pathinfo, '/') === '/_configurator') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_configurator_home');
                    }

                    return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::checkAction',  '_route' => '_configurator_home',);
                }

                // _configurator_step
                if (0 === strpos($pathinfo, '/_configurator/step') && preg_match('#^/_configurator/step/(?P<index>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_configurator_step')), array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::stepAction',));
                }

                // _configurator_final
                if ($pathinfo === '/_configurator/final') {
                    return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::finalAction',  '_route' => '_configurator_final',);
                }

            }

        }

        // login
        if ($pathinfo === '/connexion') {
            return array (  '_controller' => 'Laplace\\UserBundle\\Controller\\SecurityController::loginAction',  '_route' => 'login',);
        }

        // login_check
        if ($pathinfo === '/authentification') {
            return array('_route' => 'login_check');
        }

        // logout
        if ($pathinfo === '/deconnexion') {
            return array('_route' => 'logout');
        }

        // laplace_common_homepage
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'laplace_common_homepage');
            }

            return array (  '_controller' => 'Laplace\\CommonBundle\\Controller\\DefaultController::homepageAction',  '_route' => 'laplace_common_homepage',);
        }

        // laplace_common_user_homepage
        if ($pathinfo === '/espace-perso') {
            return array (  '_controller' => 'Laplace\\CommonBundle\\Controller\\DefaultController::userHomepageAction',  '_route' => 'laplace_common_user_homepage',);
        }

        if (0 === strpos($pathinfo, '/a')) {
            if (0 === strpos($pathinfo, '/admin')) {
                // laplace_common_adm_homepage
                if ($pathinfo === '/admin') {
                    return array (  '_controller' => 'Laplace\\CommonBundle\\Controller\\DefaultController::adminHomepageAction',  '_route' => 'laplace_common_adm_homepage',);
                }

                // laplace_common_adm_dashboard
                if ($pathinfo === '/admin/dashboard') {
                    return array (  '_controller' => 'Laplace\\CommonBundle\\Controller\\SystemController::dashboardAction',  '_route' => 'laplace_common_adm_dashboard',);
                }

                // laplace_common_adm_api_delete_alert
                if ($pathinfo === '/admin/api/alertes/supprimer') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_laplace_common_adm_api_delete_alert;
                    }

                    return array (  '_controller' => 'Laplace\\CommonBundle\\Controller\\SystemController::deleteAlertAction',  '_route' => 'laplace_common_adm_api_delete_alert',);
                }
                not_laplace_common_adm_api_delete_alert:

            }

            // laplace_user_api_find_users
            if ($pathinfo === '/api/utilisateurs/rechercher') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_laplace_user_api_find_users;
                }

                return array (  '_controller' => 'Laplace\\UserBundle\\Controller\\UserProfileController::findUsersAction',  '_route' => 'laplace_user_api_find_users',);
            }
            not_laplace_user_api_find_users:

        }

        if (0 === strpos($pathinfo, '/inscription')) {
            // laplace_user_new_profile
            if ($pathinfo === '/inscription') {
                return array (  '_controller' => 'Laplace\\UserBundle\\Controller\\UserProfileController::createAction',  '_route' => 'laplace_user_new_profile',);
            }

            // laplace_user_new_profile_agent
            if ($pathinfo === '/inscription/agent') {
                return array (  '_controller' => 'Laplace\\UserBundle\\Controller\\UserProfileController::createAction',  'doctorant' => false,  '_route' => 'laplace_user_new_profile_agent',);
            }

            // laplace_user_new_profile_docto
            if ($pathinfo === '/inscription/doctorant') {
                return array (  '_controller' => 'Laplace\\UserBundle\\Controller\\UserProfileController::createAction',  'doctorant' => true,  '_route' => 'laplace_user_new_profile_docto',);
            }

        }

        if (0 === strpos($pathinfo, '/espace-perso/profil')) {
            // laplace_user_view_my_profile
            if ($pathinfo === '/espace-perso/profil/voir') {
                return array (  '_controller' => 'Laplace\\UserBundle\\Controller\\UserProfileController::viewAction',  'admin' => false,  '_route' => 'laplace_user_view_my_profile',);
            }

            // laplace_user_edit_my_profile
            if ($pathinfo === '/espace-perso/profil/modifier') {
                return array (  '_controller' => 'Laplace\\UserBundle\\Controller\\UserProfileController::editAction',  'admin' => false,  '_route' => 'laplace_user_edit_my_profile',);
            }

        }

        if (0 === strpos($pathinfo, '/admin')) {
            if (0 === strpos($pathinfo, '/admin/systeme')) {
                // laplace_user_adm_tutelles
                if ($pathinfo === '/admin/systeme/tutelles') {
                    return array (  '_controller' => 'Laplace\\UserBundle\\Controller\\SystemController::manageTutellesAction',  '_route' => 'laplace_user_adm_tutelles',);
                }

                if (0 === strpos($pathinfo, '/admin/systeme/s')) {
                    // laplace_user_adm_sites
                    if ($pathinfo === '/admin/systeme/sites') {
                        return array (  '_controller' => 'Laplace\\UserBundle\\Controller\\SystemController::manageSitesAction',  '_route' => 'laplace_user_adm_sites',);
                    }

                    if (0 === strpos($pathinfo, '/admin/systeme/statuts-')) {
                        // laplace_user_adm_statuts_agents
                        if ($pathinfo === '/admin/systeme/statuts-agents') {
                            return array (  '_controller' => 'Laplace\\UserBundle\\Controller\\SystemController::manageStatutsAgentsAction',  '_route' => 'laplace_user_adm_statuts_agents',);
                        }

                        // laplace_user_adm_statuts_doctorants
                        if ($pathinfo === '/admin/systeme/statuts-doctorants') {
                            return array (  '_controller' => 'Laplace\\UserBundle\\Controller\\SystemController::manageStatutsDoctorantsAction',  '_route' => 'laplace_user_adm_statuts_doctorants',);
                        }

                    }

                }

            }

            if (0 === strpos($pathinfo, '/admin/profil')) {
                if (0 === strpos($pathinfo, '/admin/profils')) {
                    // laplace_user_adm_limited_profiles
                    if (0 === strpos($pathinfo, '/admin/profils/limites') && preg_match('#^/admin/profils/limites(?:/(?P<year>\\d+))?$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'laplace_user_adm_limited_profiles')), array (  '_controller' => 'Laplace\\UserBundle\\Controller\\UserProfileController::viewLimitedAction',  'year' => NULL,));
                    }

                    // laplace_user_adm_all_profiles
                    if (0 === strpos($pathinfo, '/admin/profils/tous-les-profils') && preg_match('#^/admin/profils/tous\\-les\\-profils(?:/(?P<year>\\d+))?$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'laplace_user_adm_all_profiles')), array (  '_controller' => 'Laplace\\UserBundle\\Controller\\UserProfileController::viewAllAction',  'year' => NULL,));
                    }

                    if (0 === strpos($pathinfo, '/admin/profils/nouveau-profil-local')) {
                        // laplace_user_adm_create_local_profile
                        if ($pathinfo === '/admin/profils/nouveau-profil-local') {
                            return array (  '_controller' => 'Laplace\\UserBundle\\Controller\\UserProfileController::createLocalAction',  '_route' => 'laplace_user_adm_create_local_profile',);
                        }

                        // laplace_user_adm_create_local_profile_agent
                        if ($pathinfo === '/admin/profils/nouveau-profil-local/agent') {
                            return array (  '_controller' => 'Laplace\\UserBundle\\Controller\\UserProfileController::createLocalAction',  'doctorant' => false,  '_route' => 'laplace_user_adm_create_local_profile_agent',);
                        }

                        // laplace_user_adm_create_local_profile_docto
                        if ($pathinfo === '/admin/profils/nouveau-profil-local/doctorant') {
                            return array (  '_controller' => 'Laplace\\UserBundle\\Controller\\UserProfileController::createLocalAction',  'doctorant' => true,  '_route' => 'laplace_user_adm_create_local_profile_docto',);
                        }

                    }

                    // laplace_user_adm_view_profile
                    if (0 === strpos($pathinfo, '/admin/profils/voir') && preg_match('#^/admin/profils/voir/(?P<username>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'laplace_user_adm_view_profile')), array (  '_controller' => 'Laplace\\UserBundle\\Controller\\UserProfileController::viewAction',  'admin' => true,));
                    }

                    // laplace_user_adm_edit_profile
                    if (0 === strpos($pathinfo, '/admin/profils/modifier') && preg_match('#^/admin/profils/modifier/(?P<username>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'laplace_user_adm_edit_profile')), array (  '_controller' => 'Laplace\\UserBundle\\Controller\\UserProfileController::editAction',  'admin' => true,));
                    }

                }

                // laplace_user_adm_delete_profile
                if (0 === strpos($pathinfo, '/admin/profil/supprimer') && preg_match('#^/admin/profil/supprimer/(?P<username>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'laplace_user_adm_delete_profile')), array (  '_controller' => 'Laplace\\UserBundle\\Controller\\UserProfileController::deleteAction',));
                }

            }

        }

        if (0 === strpos($pathinfo, '/espace-perso')) {
            if (0 === strpos($pathinfo, '/espace-perso/besoins')) {
                // laplace_training_personal_needs
                if ($pathinfo === '/espace-perso/besoins/besoins-personnels') {
                    return array (  '_controller' => 'Laplace\\TrainingBundle\\Controller\\NeedController::viewActiveSubscriptionsAction',  '_route' => 'laplace_training_personal_needs',);
                }

                // laplace_training_all_needs
                if ($pathinfo === '/espace-perso/besoins/tous-les-besoins') {
                    return array (  '_controller' => 'Laplace\\TrainingBundle\\Controller\\NeedController::viewAllAction',  'admin' => false,  '_route' => 'laplace_training_all_needs',);
                }

                // laplace_training_view_need
                if (0 === strpos($pathinfo, '/espace-perso/besoins/voir') && preg_match('#^/espace\\-perso/besoins/voir/(?P<id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'laplace_training_view_need')), array (  '_controller' => 'Laplace\\TrainingBundle\\Controller\\NeedController::viewAction',  'admin' => false,));
                }

                // laplace_training_edit_need
                if (0 === strpos($pathinfo, '/espace-perso/besoins/modifier') && preg_match('#^/espace\\-perso/besoins/modifier/(?P<id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'laplace_training_edit_need')), array (  '_controller' => 'Laplace\\TrainingBundle\\Controller\\NeedController::editAction',  'admin' => false,));
                }

                // laplace_training_create_need
                if ($pathinfo === '/espace-perso/besoins/nouveau-besoin') {
                    return array (  '_controller' => 'Laplace\\TrainingBundle\\Controller\\NeedController::createAction',  'admin' => false,  '_route' => 'laplace_training_create_need',);
                }

            }

            // laplace_training_personal_requests
            if ($pathinfo === '/espace-perso/mes-demandes') {
                return array (  '_controller' => 'Laplace\\TrainingBundle\\Controller\\RequestController::viewActiveSubscriptionsAction',  '_route' => 'laplace_training_personal_requests',);
            }

            if (0 === strpos($pathinfo, '/espace-perso/demandes')) {
                // laplace_training_all_requests
                if (0 === strpos($pathinfo, '/espace-perso/demandes/toutes-les-demandes') && preg_match('#^/espace\\-perso/demandes/toutes\\-les\\-demandes(?:/(?P<page>\\d+))?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'laplace_training_all_requests')), array (  '_controller' => 'Laplace\\TrainingBundle\\Controller\\RequestController::viewAllAction',  'page' => 1,  'admin' => false,));
                }

                // laplace_training_view_request
                if (0 === strpos($pathinfo, '/espace-perso/demandes/voir') && preg_match('#^/espace\\-perso/demandes/voir/(?P<id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'laplace_training_view_request')), array (  '_controller' => 'Laplace\\TrainingBundle\\Controller\\RequestController::viewAction',  'admin' => false,));
                }

                // laplace_training_edit_request
                if (0 === strpos($pathinfo, '/espace-perso/demandes/modifier') && preg_match('#^/espace\\-perso/demandes/modifier/(?P<id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'laplace_training_edit_request')), array (  '_controller' => 'Laplace\\TrainingBundle\\Controller\\RequestController::editAction',  'admin' => false,));
                }

                // laplace_training_create_request
                if ($pathinfo === '/espace-perso/demandes/nouvelle-demande') {
                    return array (  '_controller' => 'Laplace\\TrainingBundle\\Controller\\RequestController::createAction',  'admin' => false,  '_route' => 'laplace_training_create_request',);
                }

            }

            if (0 === strpos($pathinfo, '/espace-perso/historique')) {
                // laplace_training_full_history
                if (0 === strpos($pathinfo, '/espace-perso/historique/tout') && preg_match('#^/espace\\-perso/historique/tout(?:/(?P<year>\\d+))?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'laplace_training_full_history')), array (  '_controller' => 'Laplace\\TrainingBundle\\Controller\\EventController::fullHistoryAction',  'year' => NULL,));
                }

                // laplace_training_need_history
                if (0 === strpos($pathinfo, '/espace-perso/historique/besoins') && preg_match('#^/espace\\-perso/historique/besoins(?:/(?P<year>\\d+))?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'laplace_training_need_history')), array (  '_controller' => 'Laplace\\TrainingBundle\\Controller\\EventController::needHistoryAction',  'year' => NULL,));
                }

                // laplace_training_request_history
                if (0 === strpos($pathinfo, '/espace-perso/historique/demandes') && preg_match('#^/espace\\-perso/historique/demandes(?:/(?P<year>\\d+))?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'laplace_training_request_history')), array (  '_controller' => 'Laplace\\TrainingBundle\\Controller\\EventController::requestHistoryAction',  'year' => NULL,));
                }

            }

            if (0 === strpos($pathinfo, '/espace-perso/discussions')) {
                // laplace_training_all_threads
                if ($pathinfo === '/espace-perso/discussions') {
                    return array (  '_controller' => 'Laplace\\TrainingBundle\\Controller\\ThreadController::viewAllAction',  'admin' => false,  '_route' => 'laplace_training_all_threads',);
                }

                // laplace_training_view_thread
                if (0 === strpos($pathinfo, '/espace-perso/discussions/voir') && preg_match('#^/espace\\-perso/discussions/voir/(?P<id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'laplace_training_view_thread')), array (  '_controller' => 'Laplace\\TrainingBundle\\Controller\\ThreadController::viewAction',  'admin' => false,));
                }

                // laplace_training_delete_message
                if (0 === strpos($pathinfo, '/espace-perso/discussions/supprimer/message') && preg_match('#^/espace\\-perso/discussions/supprimer/message/(?P<id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'laplace_training_delete_message')), array (  '_controller' => 'Laplace\\TrainingBundle\\Controller\\ThreadController::deleteMessageAction',  'admin' => false,));
                }

            }

        }

        if (0 === strpos($pathinfo, '/admin')) {
            if (0 === strpos($pathinfo, '/admin/discussions')) {
                // laplace_training_adm_all_threads
                if ($pathinfo === '/admin/discussions') {
                    return array (  '_controller' => 'Laplace\\TrainingBundle\\Controller\\ThreadController::viewAllAction',  'admin' => true,  '_route' => 'laplace_training_adm_all_threads',);
                }

                // laplace_training_adm_view_thread
                if (0 === strpos($pathinfo, '/admin/discussions/voir') && preg_match('#^/admin/discussions/voir/(?P<id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'laplace_training_adm_view_thread')), array (  '_controller' => 'Laplace\\TrainingBundle\\Controller\\ThreadController::viewAction',  'admin' => true,));
                }

                // laplace_training_adm_edit_thread
                if (0 === strpos($pathinfo, '/admin/discussions/modifier') && preg_match('#^/admin/discussions/modifier/(?P<id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'laplace_training_adm_edit_thread')), array (  '_controller' => 'Laplace\\TrainingBundle\\Controller\\ThreadController::editAction',));
                }

                if (0 === strpos($pathinfo, '/admin/discussions/supprimer')) {
                    // laplace_training_adm_delete_thread
                    if (preg_match('#^/admin/discussions/supprimer/(?P<id>\\d+)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'laplace_training_adm_delete_thread')), array (  '_controller' => 'Laplace\\TrainingBundle\\Controller\\ThreadController::deleteAction',));
                    }

                    // laplace_training_adm_delete_message
                    if (0 === strpos($pathinfo, '/admin/discussions/supprimer/message') && preg_match('#^/admin/discussions/supprimer/message/(?P<id>\\d+)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'laplace_training_adm_delete_message')), array (  '_controller' => 'Laplace\\TrainingBundle\\Controller\\ThreadController::deleteMessageAction',  'admin' => true,));
                    }

                }

            }

            if (0 === strpos($pathinfo, '/admin/systeme')) {
                // laplace_training_adm_domains
                if ($pathinfo === '/admin/systeme/domaines') {
                    return array (  '_controller' => 'Laplace\\TrainingBundle\\Controller\\SystemController::manageDomainsAction',  '_route' => 'laplace_training_adm_domains',);
                }

                // laplace_training_adm_categories
                if ($pathinfo === '/admin/systeme/categories') {
                    return array (  '_controller' => 'Laplace\\TrainingBundle\\Controller\\SystemController::manageCategoriesAction',  '_route' => 'laplace_training_adm_categories',);
                }

                // laplace_training_adm_types
                if ($pathinfo === '/admin/systeme/types') {
                    return array (  '_controller' => 'Laplace\\TrainingBundle\\Controller\\SystemController::manageTypesAction',  '_route' => 'laplace_training_adm_types',);
                }

                if (0 === strpos($pathinfo, '/admin/systeme/bilans/b')) {
                    // laplace_training_adm_needs_summary
                    if ($pathinfo === '/admin/systeme/bilans/besoins-non-pourvus') {
                        return array (  '_controller' => 'Laplace\\TrainingBundle\\Controller\\StatisticsController::needsSummaryAction',  '_route' => 'laplace_training_adm_needs_summary',);
                    }

                    // laplace_training_adm_requests_summary
                    if ($pathinfo === '/admin/systeme/bilans/bilan-de-realisation') {
                        return array (  '_controller' => 'Laplace\\TrainingBundle\\Controller\\StatisticsController::requestsSummaryAction',  '_route' => 'laplace_training_adm_requests_summary',);
                    }

                }

            }

            if (0 === strpos($pathinfo, '/admin/besoins')) {
                // laplace_training_adm_all_needs
                if ($pathinfo === '/admin/besoins/tous-les-besoins') {
                    return array (  '_controller' => 'Laplace\\TrainingBundle\\Controller\\NeedController::viewAllAction',  'admin' => true,  '_route' => 'laplace_training_adm_all_needs',);
                }

                // laplace_training_adm_view_need
                if (0 === strpos($pathinfo, '/admin/besoins/voir') && preg_match('#^/admin/besoins/voir/(?P<id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'laplace_training_adm_view_need')), array (  '_controller' => 'Laplace\\TrainingBundle\\Controller\\NeedController::viewAction',  'admin' => true,));
                }

                // laplace_training_adm_edit_need
                if (0 === strpos($pathinfo, '/admin/besoins/modifier') && preg_match('#^/admin/besoins/modifier/(?P<id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'laplace_training_adm_edit_need')), array (  '_controller' => 'Laplace\\TrainingBundle\\Controller\\NeedController::editAction',  'admin' => true,));
                }

                // laplace_training_adm_create_need
                if ($pathinfo === '/admin/besoins/nouveau-besoin') {
                    return array (  '_controller' => 'Laplace\\TrainingBundle\\Controller\\NeedController::createAction',  'admin' => true,  '_route' => 'laplace_training_adm_create_need',);
                }

                // laplace_training_adm_delete_need
                if (0 === strpos($pathinfo, '/admin/besoins/supprimer') && preg_match('#^/admin/besoins/supprimer/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'laplace_training_adm_delete_need')), array (  '_controller' => 'Laplace\\TrainingBundle\\Controller\\NeedController::deleteAction',));
                }

            }

            if (0 === strpos($pathinfo, '/admin/demandes')) {
                // laplace_training_adm_pending_requests
                if ($pathinfo === '/admin/demandes/demandes-en-attente') {
                    return array (  '_controller' => 'Laplace\\TrainingBundle\\Controller\\RequestController::viewPendingRequestsAction',  '_route' => 'laplace_training_adm_pending_requests',);
                }

                // laplace_training_adm_pending_subscriptions
                if ($pathinfo === '/admin/demandes/inscriptions-en-attente') {
                    return array (  '_controller' => 'Laplace\\TrainingBundle\\Controller\\RequestController::viewPendingSubscriptionsAction',  '_route' => 'laplace_training_adm_pending_subscriptions',);
                }

                // laplace_training_adm_all_requests
                if ($pathinfo === '/admin/demandes/toutes-les-demandes') {
                    return array (  '_controller' => 'Laplace\\TrainingBundle\\Controller\\RequestController::viewAllAction',  'admin' => true,  '_route' => 'laplace_training_adm_all_requests',);
                }

                // laplace_training_adm_view_request
                if (0 === strpos($pathinfo, '/admin/demandes/voir') && preg_match('#^/admin/demandes/voir/(?P<id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'laplace_training_adm_view_request')), array (  '_controller' => 'Laplace\\TrainingBundle\\Controller\\RequestController::viewAction',  'admin' => true,));
                }

                // laplace_training_adm_edit_request
                if (0 === strpos($pathinfo, '/admin/demandes/modifier') && preg_match('#^/admin/demandes/modifier/(?P<id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'laplace_training_adm_edit_request')), array (  '_controller' => 'Laplace\\TrainingBundle\\Controller\\RequestController::editAction',  'admin' => true,));
                }

                // laplace_training_adm_create_request
                if ($pathinfo === '/admin/demandes/nouvelle-demande') {
                    return array (  '_controller' => 'Laplace\\TrainingBundle\\Controller\\RequestController::createAction',  'admin' => true,  '_route' => 'laplace_training_adm_create_request',);
                }

                // laplace_training_adm_delete_request
                if (0 === strpos($pathinfo, '/admin/demandes/supprimer') && preg_match('#^/admin/demandes/supprimer/(?P<id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'laplace_training_adm_delete_request')), array (  '_controller' => 'Laplace\\TrainingBundle\\Controller\\RequestController::deleteAction',));
                }

                // laplace_training_adm_manage_request_sub
                if (0 === strpos($pathinfo, '/admin/demandes/inscriptions') && preg_match('#^/admin/demandes/inscriptions/(?P<request_id>\\d+)/(?P<user_id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'laplace_training_adm_manage_request_sub')), array (  '_controller' => 'Laplace\\TrainingBundle\\Controller\\RequestController::manageSubscriptionAction',));
                }

            }

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
