<?php

namespace Laplace\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

use Laplace\UserBundle\Model\AbstractUserIdentity;

/**
 * Un utilisateur est soit un Agent, soit un Doctorant. Cette classe
 * regroupe les informations communes aux deux. Les informations complémentaires
 * relatives à un doctorant ou à un agent sont stockées dans une entité
 * UserInfo liée par une relation OneToOne.
 *
 * @ORM\Entity(repositoryClass="Laplace\UserBundle\Entity\UserRepository")
 * @ORM\Table(name="`user`")
 * @UniqueEntity("username")
 */
class User extends AbstractUserIdentity implements \Serializable
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=64, unique=true)
     * @Assert\Length(max=64)
     * @Assert\NotBlank()
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=64)
     * @Assert\Length(max=64)
     * @Assert\Email()
     */
    private $emailAddress;

    /**
     * @ORM\Column(type="smallint")
     * @Assert\Choice(choices = {0, 1})
     */
    private $gender;

    /**
     * @ORM\Column(type="string", length=64)
     * @Assert\Length(max=64)
     */
    private $name;
    
    /**
     * @ORM\Column(type="string", length=64)
     * @Assert\Length(max=64)
     */
    private $firstname;

    /**
     * Le rôle représente les autorisations d'une personne.
     *
     * @ORM\Column(type="string", length=64)
     */
    private $role;

    /**
     * Date à laquelle l'utilisateur est enregistré dans la base de données.
     *
     * @ORM\Column(type="datetime")
     */
    private $since;

    /**
     * @ORM\ManyToOne(targetEntity="Laplace\UserBundle\Entity\Tutelle")
     * @ORM\JoinColumn(nullable=false)
     */
    private $tutelle;

    /**
     * @ORM\ManyToOne(targetEntity="Laplace\UserBundle\Entity\Site")
     * @ORM\JoinColumn(nullable=false)
     */
    private $site;

    /**
     * @ORM\Column(type="string", length=16, nullable=true)
     * @Assert\Length(max=16)
     */
    private $telephoneNumber1;

    /**
     * @ORM\Column(type="string", length=16, nullable=true)
     * @Assert\Length(max=16)
     */
    private $telephoneNumber2;

    /**
     * @ORM\OneToOne(targetEntity="Laplace\UserBundle\Entity\UserInfo", cascade={"all"})
     */
    private $userInfo;

    public function __construct()
    {
        $this->role         = 'ROLE_LIMITED';
        $this->since        = new \DateTime();
    }

    // From Serializable

    /**
     * La sérialisation est utilisée pour enregistrer un utilisateur dans
     * l'objet de session. On ne garde donc que les informations utiles.
     */
    final public function serialize()
    {
        return serialize(
            array(
                'id'        => $this->id,
                'username'  => $this->username,
            )
        );
    }

    final public function unserialize($serialized)
    {
        $data = unserialize($serialized);

        $this->id           = $data['id'];
        $this->username     = $data['username'];
    }

    // From UserInterface

    public function getRoles()
    {
        return array($this->role);
    }

    public function getPassword()
    {
        return '';
    }

    public function getSalt()
    {
        return '';
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function eraseCredentials()
    {
    }

    // Autre

    public function estAgent()
    {
        return $this->getUserInfo() instanceOf InfoAgent;
    }

    public function estDoctorant()
    {
        return $this->getUserInfo() instanceOf InfoDoctorant;
    }

    // Méthodes auto-générées

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param  string $username
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Set emailAddress
     *
     * @param  string $emailAddress
     * @return User
     */
    public function setEmailAddress($emailAddress)
    {
        $this->emailAddress = $emailAddress;

        return $this;
    }

    /**
     * Get emailAddress
     *
     * @return string
     */
    public function getEmailAddress()
    {
        return $this->emailAddress;
    }

    /**
     * Set gender
     *
     * @param  integer $gender
     * @return User
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return integer
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set name
     *
     * @param  string $name
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }
    
    public function setFirstName($firstname){
        $this->firstname = $firstname;
        
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    public function getFirstName(){
        return $this->firstname;
    }

    /**
     * Set role
     *
     * @param  string $role
     * @return User
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set since
     *
     * @param  \DateTime $since
     * @return User
     */
    public function setSince($since)
    {
        $this->since = $since;

        return $this;
    }

    /**
     * Get since
     *
     * @return \DateTime
     */
    public function getSince()
    {
        return $this->since;
    }

    /**
     * Set telephoneNumber1
     *
     * @param  string $telephoneNumber1
     * @return User
     */
    public function setTelephoneNumber1($telephoneNumber1)
    {
        $this->telephoneNumber1 = $telephoneNumber1;

        return $this;
    }

    /**
     * Get telephoneNumber1
     *
     * @return string
     */
    public function getTelephoneNumber1()
    {
        return $this->telephoneNumber1;
    }

    /**
     * Set telephoneNumber2
     *
     * @param  string $telephoneNumber2
     * @return User
     */
    public function setTelephoneNumber2($telephoneNumber2)
    {
        $this->telephoneNumber2 = $telephoneNumber2;

        return $this;
    }

    /**
     * Get telephoneNumber2
     *
     * @return string
     */
    public function getTelephoneNumber2()
    {
        return $this->telephoneNumber2;
    }

    /**
     * Set tutelle
     *
     * @param  \Laplace\UserBundle\Entity\Tutelle $tutelle
     * @return User
     */
    public function setTutelle(\Laplace\UserBundle\Entity\Tutelle $tutelle)
    {
        $this->tutelle = $tutelle;

        return $this;
    }

    /**
     * Get tutelle
     *
     * @return \Laplace\UserBundle\Entity\Tutelle
     */
    public function getTutelle()
    {
        return $this->tutelle;
    }

    /**
     * Set site
     *
     * @param  \Laplace\UserBundle\Entity\Site $site
     * @return User
     */
    public function setSite(\Laplace\UserBundle\Entity\Site $site)
    {
        $this->site = $site;

        return $this;
    }

    /**
     * Get site
     *
     * @return \Laplace\UserBundle\Entity\Site
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * Set userInfo
     *
     * @param  \Laplace\UserBundle\Entity\UserInfo $userInfo
     * @return User
     */
    public function setUserInfo(\Laplace\UserBundle\Entity\UserInfo $userInfo = null)
    {
        $this->userInfo = $userInfo;

        return $this;
    }

    /**
     * Get userInfo
     *
     * @return \Laplace\UserBundle\Entity\UserInfo
     */
    public function getUserInfo()
    {
        return $this->userInfo;
    }
}
