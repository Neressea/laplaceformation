<?php

namespace Laplace\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="`infoagent`")
 */
class InfoAgent extends UserInfo
{

    /**
     * @ORM\ManyToOne(targetEntity="Laplace\UserBundle\Entity\StatutAgent")
     * @ORM\JoinColumn(nullable=false)
     */
    private $statut;

    public function __construct(StatutAgent $statut = null)
    {
        parent::__construct();

        $this->statut = $statut;
    }

    /**
     * Set statut
     *
     * @param  \Laplace\UserBundle\Entity\StatutAgent $statut
     * @return InfoAgent
     */
    public function setStatut(\Laplace\UserBundle\Entity\StatutAgent $statut)
    {
        $this->statut = $statut;

        return $this;
    }

    /**
     * Get statut
     *
     * @return \Laplace\UserBundle\Entity\StatutAgent
     */
    public function getStatut()
    {
        return $this->statut;
    }
}