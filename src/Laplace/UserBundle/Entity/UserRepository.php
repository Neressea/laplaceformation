<?php

namespace Laplace\UserBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\QueryBuilder;

use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;

use Laplace\UserBundle\Model\AbstractUserIdentity;
use Laplace\UserBundle\Model\UserIdentityImpl;

class UserRepository extends EntityRepository implements UserProviderInterface
{

    /**
     * Trouve tous les profils inscrits durant l'année spécifiée. Si $role
     * est donné, la recherche est limitée à ce rôle.
     *
     * @param  int                    $year
     * @param  string|null            $role
     * @return AbstractUserIdentity[]
     */
    public function fetchByYearAndRole($year, $role)
    {
        // calcul de la période
        // (on procède ainsi car il n'y a pas d'opérateur YEAR portable)
        $new_years_day = new \DateTime();
        $new_years_day->setDate($year, 1, 1);
        $new_years_day->setTime(0, 0, 0);

        $new_years_eve = clone $new_years_day;
        $new_years_eve->add(new \DateInterval('P1Y'));

        // préparation de la requête
        $query_builder = $this->_em->createQueryBuilder();
        $query_builder
            ->from($this->_entityName, 'user')
            ->andWhere('user.since >= :start')
            ->andWhere('user.since <= :end')
            ->orderBy('user.name')
            ->setParameter('start', $new_years_day)
            ->setParameter('end', $new_years_eve);

        self::addSelect($query_builder, 'user', true);

        if ($role != null) {
            $query_builder
                ->andWhere('user.role = :role')
                ->setParameter('role', $role);
        }

        $identities     = array();
        $results        = $query_builder->getQuery()->getScalarResult();

        foreach ($results as &$result) {
            $identities[] = new UserIdentityImpl($result);
        }

        return $identities;
    }

    /**
     * Trouve tous les profils ayant un certain rôle.
     *
     * @param  string                 $role
     * @return AbstractUserIdentity[]
     */
    public function fetchByRole($role)
    {
        // préparation de la requête
        $query_builder = $this->_em->createQueryBuilder();
        $query_builder
            ->from($this->_entityName, 'user')
            ->where('user.role = :role')
            ->setParameter('role', $role);

        self::addSelect($query_builder, 'user', true);

        $identities     = array();
        $results        = $query_builder->getQuery()->getScalarResult();

        foreach ($results as &$result) {
            $identities[] = new UserIdentityImpl($result);
        }

        return $identities;
    }

    /**
     * Trouve tous les profils correspondant à une liste de valeurs données.
     * Pour chaque valeur de $name_values, il doit exister une correspondance
     * entre cette valeur et le nom de famille, le prénom ou le nom
     * d'utilisateur. Chaque valeur doit comporter plus d'un caractère, sinon
     * elle est ignorée.
     *
     * Le nombre d'entrées renvoyées est lilmité à $max_results. Si $name_values
     * est vide ou s'il n'y a aucun critère valide, un tableau vide est renvoyé.
     *
     * @param  string[]               $name_values
     * @param  int                    $max_results
     * @return AbstractUserIdentity[]
     */
    public function fetchByName($name_values, $max_results = 10)
    {
        foreach ($name_values as $i => $name) {
            if (mb_strlen($name) > 1) {
                $name_values[$i] = addcslashes(mb_strtolower($name), '%_');
            } else {
                unset($name_values[$i]);
            }
        }

        if (empty($name_values)) {
            return array();
        }

        $query_builder = $this->_em->createQueryBuilder();
        $expr = $query_builder->expr();

        $query_builder
            ->from($this->_entityName, 'user')
            ->orderBy('user.username')
            ->setMaxResults($max_results);

        self::addSelect($query_builder, 'user', false);

        foreach ($name_values as $i => $name) {
            $query_builder->andWhere(
                $expr->orX(
                    $expr->like($expr->lower('user.username'), "?$i"),
                    $expr->like($expr->lower('user.name'), "?$i")
                )
            )
            ->setParameter($i, '%' . $name . '%');
        }

        $identities     = array();
        $results        = $query_builder->getQuery()->getScalarResult();

        foreach ($results as &$result) {
            $identities[] = new UserIdentityImpl($result);
        }

        return $identities;
    }

    /**
     * Retourne la plus ancienne date d'inscription d'un utilisateur, ou null
     * s'il n'y a aucun utilisateur.
     *
     * @return DateTime|null
     */
    public function getEarliestRegistrationDate()
    {
        $query_builder = $this->createQueryBuilder('user');
        $query_builder->select('MIN(user.since)');

        try {
            $date = $query_builder->getQuery()->getSingleScalarResult();

            return new \DateTime($date);
        } catch (NoResultException $e) {
            return null;
        }
    }

    public function loadUserByUsername($username)
    {
        $query_builder = $this->createQueryBuilder('user');
        $query_builder
            ->innerJoin('user.userInfo', 'info')
            ->addSelect('info')
            ->where('user.username = :username')
            ->setParameter('username', $username);

        try {
            // La méthode Query::getSingleResult() lève une exception
            // s'il n'y a pas d'entrée correspondante aux critères.
            $user = $query_builder->getQuery()->getSingleResult();
        } catch (NoResultException $e) {
            // Dans ce cas, on lève une exception du type approprié à la place.
            throw new UsernameNotFoundException('Username not found.', 0, $e);
        }

        return $user;
    }

    public function refreshUser(UserInterface $user)
    {
        $class = get_class($user);
        if (!$this->supportsClass($class)) {
            throw new UnsupportedUserException(
                sprintf('Instances of "%s" are not supported.', $class)
            );
        }

        return $this->loadUserByUsername($user->getUsername());
    }

    public function supportsClass($class)
    {
        return $this->getEntityName() === $class
                || is_subclass_of($class, $this->getEntityName());
    }

    /**
     * Ajoute des clauses SELECT au query_builder permettant de construire
     * un objet AbstractUserIdentity (id, username, gender, name).
     * Si $full_identity est vrai, les champs emailAddress, since et role sont
     * également sélectionnés.
     *
     * @param  QueryBuilder $query_builder
     * @param  string       $alias
     * @param  bool         $full_identity
     * @return void
     */
    public static function addSelect(
        QueryBuilder $query_builder,
        $alias,
        $full_identity
    ) {
        $query_builder
            ->addSelect("$alias.id")
            ->addSelect("$alias.username")
            ->addSelect("$alias.firstname")
            ->addSelect("$alias.gender")
            ->addSelect("$alias.name");

        if (!$full_identity) {
            return;
        }

        $query_builder
            ->addSelect("$alias.emailAddress")
            ->addSelect("$alias.since")
            ->addSelect("$alias.role");
    }
}
