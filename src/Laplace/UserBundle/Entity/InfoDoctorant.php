<?php

namespace Laplace\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="`infodoctorant`")
 */
class InfoDoctorant extends UserInfo
{

    /**
     * @ORM\ManyToOne(targetEntity="Laplace\UserBundle\Entity\StatutDoctorant")
     * @ORM\JoinColumn(nullable=false)
     */
    private $statut;

    /**
     * @ORM\Column(type="date")
     * @Assert\Date()
     * @Assert\NotNull()
     */
    private $debutThese;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     * @Assert\Length(max=64)
     */
    private $responsable;

    /**
     * @ORM\Column(type="string", length=128, nullable=true)
     * @Assert\Length(max=128)
     */
    private $ecoleDoctorale;

    /**
     * @ORM\Column(type="string", length=128, nullable=true)
     * @Assert\Length(max=128)
     */
    private $bourse;

    public function __construct(StatutDoctorant $statut = null)
    {
        parent::__construct();

        $this->statut = $statut;
    }

    /**
     * Set debutThese
     *
     * @param  \DateTime     $debutThese
     * @return InfoDoctorant
     */
    public function setDebutThese($debutThese)
    {
        $this->debutThese = $debutThese;

        return $this;
    }

    /**
     * Get debutThese
     *
     * @return \DateTime
     */
    public function getDebutThese()
    {
        return $this->debutThese;
    }

    /**
     * Set ecoleDoctorale
     *
     * @param  string        $ecoleDoctorale
     * @return InfoDoctorant
     */
    public function setEcoleDoctorale($ecoleDoctorale)
    {
        $this->ecoleDoctorale = $ecoleDoctorale;

        return $this;
    }

    /**
     * Get ecoleDoctorale
     *
     * @return string
     */
    public function getEcoleDoctorale()
    {
        return $this->ecoleDoctorale;
    }

    /**
     * Set bourse
     *
     * @param  string        $bourse
     * @return InfoDoctorant
     */
    public function setBourse($bourse)
    {
        $this->bourse = $bourse;

        return $this;
    }

    /**
     * Get bourse
     *
     * @return string
     */
    public function getBourse()
    {
        return $this->bourse;
    }

    /**
     * Set statut
     *
     * @param  \Laplace\UserBundle\Entity\StatutDoctorant $statut
     * @return InfoDoctorant
     */
    public function setStatut(\Laplace\UserBundle\Entity\StatutDoctorant $statut)
    {
        $this->statut = $statut;

        return $this;
    }

    /**
     * Get statut
     *
     * @return \Laplace\UserBundle\Entity\StatutDoctorant
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * Set responsable
     *
     * @param string $responsable
     * @return InfoDoctorant
     */
    public function setResponsable($responsable)
    {
        $this->responsable = $responsable;
    
        return $this;
    }

    /**
     * Get responsable
     *
     * @return string 
     */
    public function getResponsable()
    {
        return $this->responsable;
    }
}
