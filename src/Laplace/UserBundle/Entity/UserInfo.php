<?php

namespace Laplace\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="`userinfo`")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="subclass", type="smallint")
 * @ORM\DiscriminatorMap({"InfoAgent", "InfoDoctorant"})
 */
abstract class UserInfo
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    public function __construct()
    {
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}