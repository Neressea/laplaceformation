<?php

namespace Laplace\UserBundle\Mailer;

use Symfony\Bridge\Doctrine\RegistryInterface as Doctrine;

use Swift_Message as Message;
use Swift_Mailer as Mailer;

use Laplace\UserBundle\Entity\User;

class LaplaceMailer
{
    private $mngr;
    private $mailer;

    private $reply_to;
    private $slug;

    public function __construct(Doctrine $doctrine, Mailer $mailer, $reply_to)
    {
        $this->mngr     = $doctrine->getManager();
        $this->mailer   = $mailer;

        $this->reply_to = $reply_to;
        $this->slug     = '';
    }

    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * Envoie un email au(x) destinataire(s).
     *
     * Si $from est non null, le nom et l'adresse email de cet utilisateur sont
     * utilisés comme expéditeur de l'email. Sinon, c'est l'adresse reply-to
     * qui est utilisée.
     *
     * Si $to est une instance de User, son nom et son adresse email sont
     * utilisés comme unique destinataire de l'email.
     * Si $to est de type string, elle doit représenter un rôle. L'email est
     * alors envoyé à tous les utilisateurs ayant ce role (par exemple :
     * 'ROLE_ADMIN').
     * Si $to est un tableau, chaque valeur doit être soit une instance de
     * User, soit une adresse email valide (dans ce cas, si la clef est une
     * chaîne de caractères, elle est utilisée comme nom du destinataire).
     *
     * $subject est le sujet de l'email, précédé du slug s'il est défini.
     *
     * $content représente le corps de l'email au format html. Une version sans
     * html sera incluse avec le type text/plain.
     *
     * @param User|null         $from
     * @param User|string|array $to
     * @param string            $subject
     * @param string            $body
     */
    public function send($from, $to, $subject, $body)
    {
        $message = Message::newInstance();
        
        //
        // Expéditeur
        //

        if ($from != null) {
            $message->setFrom(
                array(
                    $from->getEmailAddress() => $from->getFullName(),
                )
            );
        } else {
            $message->setFrom($this->reply_to);
        }

        //
        // Destinataire(s)
        //

        if ($to instanceof User) {
            $message->addTo($to->getEmailAddress(), $to->getFullName());
        } elseif (is_string($to)) {
            
            //Si on envoie à 'CORRESP_FORM', on n'envoie qu'aux admins qui ont indiqué vouloir recevoir les mails dans leurs paramètres
            if($to == 'CORRESP_FORM'){
                
                //On ouvre le fichier de configuration de l'intranet
                $file = fopen("../laplace_config.json", "r") or die("Unable to open file!");
                $read = fread($file,filesize("../laplace_config.json"));
                fclose($file);
                
                //On le transforme en objet
                $config = json_decode($read);
                
                //On récupère la liste des admins voulanbt recevoir les mails
                $list = $config->admin_mailer;
                print $list;
                
                //On les ajoute au mail
                foreach($list as $l){
                    $message->addTo($l);
                }
                
            }else { //Autrement on envoie à tous ceux concernés par le mail : par rôle
                $recipients = $this->mngr
                    ->getRepository('LaplaceUserBundle:User')
                    ->fetchByRole($to);

                foreach ($recipients as $recipient) {
                    $message->addTo(
                        $recipient->getEmailAddress(),
                        $recipient->getFullName()
                    );
                }
            }
        } elseif (is_array($to)) {
            if (empty($to)) {
                return;
            }

            foreach ($to as $i => $recipient) {
                if ($recipient instanceof User) {
                    $message->addTo(
                        $recipient->getEmailAddress(),
                        $recipient->getFullName()
                    );
                } elseif (is_string($recipient)) {
                    $message->addTo($recipient);
                } else {
                    throw new \Exception("Invalid recipient at index $i.");
                }
            }
        } else {
            throw new \Exception(
                'Recipient is neither an instance of User, ' .
                'nor a string, nor an array.'
            );
        }

        //
        // Sujet
        //

        $message->setSubject($this->slug . $subject);

        //
        // Corps du message
        //

        $message->setBody($body, 'text/html');
        $message->addPart(strip_tags($body), 'text/plain');
        
        $this->mailer->send($message);
    }

}
