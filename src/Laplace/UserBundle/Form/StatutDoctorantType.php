<?php

namespace Laplace\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class StatutDoctorantType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('intitule', 'text',
            array(
                'label' => 'Intitulé',
            )
        );
        $builder->add('groupe', 'text',
            array(
                'label' => 'Groupe',
            )
        );
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'Laplace\UserBundle\Entity\StatutDoctorant',
            )
        );
    }

    public function getName()
    {
        return 'laplace_userbundle_statutdoctoranttype';
    }
}
