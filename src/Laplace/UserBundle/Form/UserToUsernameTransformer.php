<?php

namespace Laplace\UserBundle\Form;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Doctrine\Common\Persistence\ObjectManager;

class UserToUsernameTransformer implements DataTransformerInterface
{

    private $mngr;

    public function __construct(ObjectManager $mngr)
    {
        $this->mngr = $mngr;
    }

    /**
     * Transforme $user en nom d'utilisateur.
     *
     * @param User $user
     */
    public function transform($user)
    {
        if ($user != null) {
            return $user->getUsername();
        } else {
            return '';
        }
    }


    /**
     * Transforme $username en une entité User.
     *
     * @param string $username
     */
    public function reverseTransform($username)
    {
        if (!$username) {
            return null;
        }

        $user = $this->mngr
            ->getRepository('LaplaceUserBundle:User')
            ->findOneByUsername($username);

        if ($user == null) {
            throw new TransformationFailedException();
        }

        return $user;
    }

}
