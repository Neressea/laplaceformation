<?php

namespace Laplace\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Assert;

class InfoDoctorantType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('statut', 'entity',
            array(
                'class'         => 'LaplaceUserBundle:StatutDoctorant',
                'property'      => 'intitule',
                'group_by'      => 'groupe',
                'constraints'   => new Assert\NotNull(),
                'label'         => 'Corps',
            )
        );
        $builder->add('responsable', 'text',
            array(
                'label'         => 'Nom du responsable Laplace',
            )
        );
        $builder->add('debutThese', 'date',
            array(
                'widget'    => 'single_text',
                'format'    => 'dd/MM/yy',
                'label'     => 'Date de début de thèse',
            )
        );
        $builder->add('ecoleDoctorale', 'text',
            array(
                'required'  => false,
                'label'     => 'Ecole Doctorale',
            )
        );
        $builder->add('bourse', 'text',
            array(
                'required'  => false,
                'label'     => 'Bourse',
            )
        );
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'Laplace\UserBundle\Entity\InfoDoctorant',
            )
        );

        $resolver->setRequired(
            array('em')
        );

        $resolver->setAllowedTypes(
            array(
                'em' => 'Doctrine\Common\Persistence\ObjectManager',
            )
        );
    }

    public function getName()
    {
        return 'laplace_userbundle_infodoctoranttype';
    }
}
