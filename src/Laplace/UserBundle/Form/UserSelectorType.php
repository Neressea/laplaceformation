<?php

namespace Laplace\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserSelectorType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mngr           = $options['em'];
        $transformer    = new UserToUsernameTransformer($mngr);

        $builder->addModelTransformer($transformer);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'invalid_message' =>
                    'Ce nom d\'utilisateur n\'est pas valide.',
                'attr' => array(
                    'placeholder' =>
                        'Champ de recherche : nom, prénom, site, tutelle, ...',
                ),
            )
        );
        
        $resolver->setRequired(array('em'));

        $resolver->setAllowedTypes(
            array(
                'em' => 'Doctrine\Common\Persistence\ObjectManager',
            )
        );
    }

    public function getParent()
    {
        return 'text';
    }

    public function getName()
    {
        return 'user_selector';
    }
}
