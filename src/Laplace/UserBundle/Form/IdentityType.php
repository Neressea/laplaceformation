<?php

namespace Laplace\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Assert;

use Laplace\UserBundle\Entity\User;

class IdentityType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        if ($options['show_disabled']) {
            $builder->add('name', 'text',
                array(
                    'label'     => 'Nom',
                    'disabled'  => !$options['allow_change_name'],
                    'required'  => false,
                )
            );
            $builder->add('emailAddress', 'email',
                array(
                    'label'     => 'Adresse email',
                    'disabled'  => !$options['allow_change_email'],
                    'required'  => false,
                )
            );
        }

        if ($options['ask_username']) {
            $builder->add('username', 'text',
                array(
                    'label'     => 'Login Laplace (en minuscules)',
                )
            );
        }

        $builder->add('gender', 'choice',
            array(
                'label'         => 'Sexe',
                'choices'       => array(
                    User::MALE      => 'Homme',
                    User::FEMALE    => 'Femme',
                ),
                'constraints'   => new Assert\NotNull(),
                'expanded'      => true,
            )
        );

        if ($options['allow_change_role']) {
            $builder->add('role', 'choice',
                array(
                    'choices'   => array(
                        'ROLE_LIMITED'  => 'Limité',
                        'ROLE_USER'     => 'Utilisateur',
                        'ROLE_ADMIN'    => 'Administrateur',
                    ),
                    'label'     => 'Rôle',
                )
            );
        }
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class'            => 'Laplace\UserBundle\Entity\User',
                'allow_change_role'     => false,
                'ask_username'          => false,
                'show_disabled'         => true,
                'allow_change_name'     => false,
                'allow_change_email'    => false,
            )
        );

        $resolver->setOptional(
            array(
                'allow_change_role',
                'ask_username',
                'show_disabled',
                'allow_change_name',
                'allow_change_email',
            )
        );

        $resolver->setAllowedTypes(
            array(
                'allow_change_role'     => 'bool',
                'ask_username'          => 'bool',
                'show_disabled'         => 'bool',
                'allow_change_name'     => 'bool',
                'allow_change_email'    => 'bool',
            )
        );
    }

    public function getName()
    {
        return 'laplace_userbundle_identitytype';
    }
}
