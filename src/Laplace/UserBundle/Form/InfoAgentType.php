<?php

namespace Laplace\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Assert;

class InfoAgentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('statut', 'entity',
            array(
                'class'         => 'LaplaceUserBundle:StatutAgent',
                'property'      => 'intitule',
                'group_by'      => 'groupe',
                'constraints'   => new Assert\NotNull(),
                'label'         => 'Corps',
            )
        );
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'Laplace\UserBundle\Entity\InfoAgent',
            )
        );
    }

    public function getName()
    {
        return 'laplace_userbundle_infoagenttype';
    }
}
