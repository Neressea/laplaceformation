<?php

namespace Laplace\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Assert;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('tutelle', 'entity',
            array(
                'class'         => 'LaplaceUserBundle:Tutelle',
                'property'      => 'nom',
                'constraints'   => new Assert\NotNull(),
                'label'         => 'Employeur',
            )
        );
        $builder->add('site', 'entity',
            array(
                'class'         => 'LaplaceUserBundle:Site',
                'property'      => 'name',
                'constraints'   => new Assert\NotNull(),
                'label'         => 'Site',
            )
        );
        $builder->add('telephoneNumber1', 'text',
            array(
                'label'         => 'Téléphone pro. 1',
                'required'      => false,
            )
        );
        $builder->add('telephoneNumber2', 'text',
            array(
                'label'         => 'Téléphone pro. 2',
                'required'      => false,
            )
        );
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class'    => 'Laplace\UserBundle\Entity\User',
            )
        );
    }

    public function getName()
    {
        return 'laplace_userbundle_usertype';
    }
}
