<?php

namespace Laplace\UserBundle\Security\Http\EntryPoint;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\EntryPoint\AuthenticationEntryPointInterface;
use Symfony\Component\Security\Http\HttpUtils;

class LdapAuthenticationEntryPoint implements AuthenticationEntryPointInterface
{
    private $http_utils;
    private $redirect_path;

    public function __construct(HttpUtils $http_utils, $redirect_path)
    {
        $this->http_utils       = $http_utils;
        $this->redirect_path    = $redirect_path;
    }

    /**
     * {@inheritdoc}
     */
    public function start(
        Request $request,
        AuthenticationException $auth_exception = null
    ) {
        return $this->http_utils
            ->createRedirectResponse($request, $this->redirect_path);
    }
}
