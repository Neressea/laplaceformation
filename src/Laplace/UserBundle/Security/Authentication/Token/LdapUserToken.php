<?php

namespace Laplace\UserBundle\Security\Authentication\Token;

class LdapUserToken extends AbstractLdapUserToken
{

    private $password;

    public function __construct(array $roles = array())
    {
        parent::__construct($roles);
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

}
