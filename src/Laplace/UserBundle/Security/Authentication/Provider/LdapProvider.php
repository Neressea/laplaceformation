<?php

namespace Laplace\UserBundle\Security\Authentication\Provider;

use Symfony\Component\Security\Core\Authentication\Provider\AuthenticationProviderInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserProviderInterface;

use Laplace\CommonBundle\Ldap\LaplaceLdap;
use Laplace\UserBundle\Security\Authentication\Token\AbstractLdapUserToken;
use Laplace\UserBundle\Security\Authentication\Token\LdapUserLoggedInToken;

class LdapProvider implements AuthenticationProviderInterface
{
    private $user_provider;
    private $laplace_ldap;

    public function __construct(
        UserProviderInterface $user_provider,
        LaplaceLdap $laplace_ldap
    ) {
        $this->user_provider    = $user_provider;
        $this->laplace_ldap     = $laplace_ldap;
    }

    public function authenticate(TokenInterface $token)
    {
        if ($token instanceof LdapUserLoggedInToken) {
            $user = $token->getUser();

            $authenticated_token = new LdapUserLoggedInToken($user->getRoles());
            $authenticated_token->setUser($user);

            return $authenticated_token;
        }

        $username = $token->getUsername();
        $password = $token->getPassword();

        try {
            $authenticated =
                $this->laplace_ldap->authenticate($username, $password);
        } catch (\Exception $e) {
            throw new AuthenticationException('Problème de connexion LDAP.', null, 0, $e);
        }

        if (!$authenticated) {
            throw new AuthenticationException('Identifiants invalides.');
        }

        try {
            $user = $this->user_provider->loadUserByUsername($username);

            $authenticated_token = new LdapUserLoggedInToken($user->getRoles());
            $authenticated_token->setUser($user);

            return $authenticated_token;
        } catch (UsernameNotFoundException $e) {
            throw new AuthenticationException(
                'Vous n\'avez pas créé votre profil sur ce service.'
            );
        }
    }

    public function supports(TokenInterface $token)
    {
        return $token instanceof AbstractLdapUserToken;
    }

}
