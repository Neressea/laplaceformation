<?php

namespace Laplace\UserBundle\Security\Firewall;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Security\Http\Firewall\ListenerInterface;
use Symfony\Component\Security\Http\EntryPoint\AuthenticationEntryPointInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;

use Laplace\UserBundle\Security\Authentication\Token\LdapUserToken;

/**
 * Cette classe écoute les événements du firewall pour authentifier un
 * utilisateur de la même manière que form_login.
 * Si l'URI correspond à `$login_path`, on essaye d'authentifier l'utilisateur.
 */
class LdapListener implements ListenerInterface
{

    private $security_context;
    private $auth_manager;
    private $auth_entry_point;
    private $check_path;

    public function __construct(
        SecurityContextInterface $security_context,
        AuthenticationManagerInterface $auth_manager,
        AuthenticationEntryPointInterface $auth_entry_point,
        $check_path
    ) {
        $this->security_context     = $security_context;
        $this->auth_manager         = $auth_manager;
        $this->auth_entry_point     = $auth_entry_point;
        $this->check_path           = $check_path;
    }

    public function handle(GetResponseEvent $event)
    {
        $request = $event->getRequest();

        if ($request->getUri() != $request->getUriForPath($this->check_path)) {
            return;
        }

        $session = $request->getSession();

        $username = $request->request->get('_username');
        $password = $request->request->get('_password');

        if (!is_string($username) || !is_string($password)) {
            return;
        }

        $token = new LdapUserToken();
        $token->setUser($username);
        $token->setPassword($password);

        try {
            // Authentification
            $authToken = $this->auth_manager->authenticate($token);
            $this->security_context->setToken($authToken);

            // Redirection
            $target = $session->get('_security.private_area.target_path');
            $session->remove('_security.private_area.target_path');

            if (empty($target)) {
                $target = $request->getScheme() . '://' . $request->getHttpHost();
            }

            $response = new RedirectResponse($target);
            $event->setResponse($response);
        } catch (AuthenticationException $failed) {
            // Ajout des erreurs dans la session
            $session->set(
                SecurityContextInterface::AUTHENTICATION_ERROR,
                $failed->getMessage()
            );
            $session->set(
                SecurityContextInterface::LAST_USERNAME,
                $username
            );

            // Effacer le token
            $this->security_context->setToken(null);

            // Redirection
            $response = $this->auth_entry_point->start($request);
            $event->setResponse($response);
        }
    }
}
