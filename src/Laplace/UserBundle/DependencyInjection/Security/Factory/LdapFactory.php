<?php

namespace Laplace\UserBundle\DependencyInjection\Security\Factory;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\DependencyInjection\DefinitionDecorator;
use Symfony\Component\Config\Definition\Builder\NodeDefinition;
use Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory\SecurityFactoryInterface;

class LdapFactory implements SecurityFactoryInterface
{
    public function create(ContainerBuilder $container, $id, $config, $userProvider, $defaultEntryPoint)
    {
        // Entry Point
        $entryPointId = 'security.authentication.entry_point.ldap.'.$id;
        $container
            ->setDefinition(
                $entryPointId,
                new DefinitionDecorator('ldap.security.authentication.entry_point')
            )
            ->addArgument(new Reference('security.http_utils'))
            ->addArgument($config['login_path'])
        ;

        // Listener
        $listenerId = 'security.authentication.listener.ldap.'.$id;
        $container
            ->setDefinition(
                $listenerId,
                new DefinitionDecorator('ldap.security.authentication.listener')
            )
            ->addArgument(new Reference($entryPointId))
            ->addArgument($config['check_path'])
        ;

        // Provider
        $providerId = 'security.authentication.provider.ldap.'.$id;
        $container
            ->setDefinition(
                $providerId,
                new DefinitionDecorator('ldap.security.authentication.provider')
            )
            ->replaceArgument(0, new Reference($userProvider))
        ;

        return array($providerId, $listenerId, $entryPointId);
    }

    public function getPosition()
    {
        return 'pre_auth';
    }

    public function getKey()
    {
        return 'ldap';
    }

    public function addConfiguration(NodeDefinition $node)
    {
        $node
            ->children()
            ->scalarNode('login_path')
            ->end();
        $node
            ->children()
            ->scalarNode('check_path')
            ->end();
    }
}
