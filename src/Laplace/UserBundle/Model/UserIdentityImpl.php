<?php

namespace Laplace\UserBundle\Model;

/**
 * Cette classe est utilisée lorsque des informations sur les utilisateurs
 * doivent être récupérées sans que l'on souhaite exécuter une requête sur
 * l'ensemble de l'entité User.
 * En effet, Doctrine charge automatiquement les valeurs du champ UserInfo
 * lorsque l'on récupère une entité User. La solution est donc de ne récupérer
 * que les champs importants de User et d'utiliser cette classe pour manipuler
 * ces valeurs.
 */
final class UserIdentityImpl extends AbstractUserIdentity
{

    private $_data = array();

    public function __construct($data)
    {
        $this->_data = $data;
    }

    public function getId()
    {
        return isset($this->_data['id']) ?
            $this->_data['id'] : null;
    }

    public function getUsername()
    {
        return isset($this->_data['username']) ?
            $this->_data['username'] : null;
    }

    public function getEmailAddress()
    {
        return isset($this->_data['emailAddress']) ?
            $this->_data['emailAddress'] : null;
    }

    public function getGender()
    {
        return isset($this->_data['gender']) ?
            $this->_data['gender'] : null;
    }

    public function getName()
    {
        return isset($this->_data['name']) ?
            $this->_data['name'] : null;
    }
    
    public function getFirstName(){
        return isset($this->_data['firstname']) ?
            $this->_data['firstname'] : null;
    }

    public function getRole()
    {
        return isset($this->_data['role']) ?
            $this->_data['role'] : null;
    }

    public function getSince()
    {
        return isset($this->_data['since']) ?
            $this->_data['since'] : null;
    }

    public function getRoles() {}

    public function getPassword() {}

    public function getSalt() {}

    public function eraseCredentials() {}

}
