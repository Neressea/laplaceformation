<?php

namespace Laplace\UserBundle\Model;

use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Représente les informations basiques les plus souvent utilisées à propos d'un
 * utilisateur.
 *
 * Pourquoi AbstractUserIdentity implémente UserInterface ?
 * AbstractUserIdentity doit déclarer getUsername() mais getUsername() existe
 * déjà dans UserInterface, ce qui pose problème au niveau de la classe
 * User. En effet, php refuse d'implémenter une méthode héritée à la fois
 * d'une interface et d'une classe abstraite.
 */
abstract class AbstractUserIdentity implements UserInterface
{
    const MALE      = 0;
    const FEMALE    = 1;

    public function __construct()
    {
    }

    abstract public function getId();

    //public abstract function getUsername();

    abstract public function getEmailAddress();

    abstract public function getGender();

    abstract public function getName();
    
    abstract public function getFirstName();

    abstract public function getSince();

    abstract public function getRole();

    public function getFullName()
    {
        $titles = array(
            self::MALE      => 'M.',
            self::FEMALE    => 'Mme',
        );

        return  $titles[$this->getGender()] . ' ' . $this->getName() . ' ' . $this->getFirstName();
    }

}
