<?php

namespace Laplace\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Validator\Constraints as Assert;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use Laplace\CommonBundle\Controller\AbstractBaseController;

use Laplace\UserBundle\Entity\User;
use Laplace\UserBundle\Entity\InfoAgent;
use Laplace\UserBundle\Entity\InfoDoctorant;
use Laplace\UserBundle\Form\IdentityType;
use Laplace\UserBundle\Form\UserType;
use Laplace\UserBundle\Form\InfoAgentType;
use Laplace\UserBundle\Form\InfoDoctorantType;
use Laplace\UserBundle\Security\Authentication\Token\LdapUserLoggedInToken;

use Laplace\TrainingBundle\Entity\Event;

class UserProfileController extends AbstractBaseController
{

    /**
     * Affiche la page d'inscription.
     *
     * @param  bool|null     $doctorant
     * @return Response
     */
    public function createAction($doctorant = null)
    {
        $mngr           = $this->getDoctrine()->getManager();

        $parameters     = array();
        $profile        = new User();

        if ($doctorant !== null) {

            if ($doctorant) {
                $profile->setUserInfo(new InfoDoctorant());
            } else {
                $profile->setUserInfo(new InfoAgent());
            }

            // données initiales du formulaire
            $form_name      = 'new_profile_form';
            $data           = array(
                'password'  => null,
                'identity'  => $profile,
                'user'      => $profile,
                'userinfo'  => $profile->getUserInfo(),
                'comment'   => null,
            );

            //construction du formulaire
            $builder = $this->createNamedFormBuilder($form_name, $data);
            $builder->add('password', 'password',
                array(
                    'label'         => 'Mot de passe Laplace',
                    'constraints'   => new Assert\NotBlank(),
                )
            );
            $builder->add('identity', new IdentityType(),
                array(
                    'allow_change_role'     => false,
                    'ask_username'          => true,
                    'show_disabled'         => false,
                )
            );
            $builder->add('user', new UserType());
            if ($profile->estDoctorant()) {
                $builder->add('userinfo', new InfoDoctorantType(),
                    array(
                        'em' => $mngr
                    )
                );
            } else {
                $builder->add('userinfo', new InfoAgentType());
            }
            $builder->add('comment', 'textarea',
                array(
                    'required'  => false,
                    'label'     => 'Commentaires',
                )
            );
            $builder->add('Envoyer', 'submit');

            // validation
            $form = $builder->getForm();
            if ($this->isNamedFormSubmitted($form_name, $form)) {
                if ($form->isValid()) {

                    $data = $form->getData();

                    $username   = $profile->getUsername();
                    $password   = $data['password'];
                    $comment    = $data['comment'];

                    $ldap_data = $this
                        ->get('laplace.ldap')
                        ->fetch($username, $password);

                    $user_url = $this->generateUrl(
                        'laplace_user_view_my_profile',
                        array()
                    );
                    $admin_url = $this->generateUrl(
                        'laplace_user_adm_view_profile',
                        array(
                            'username' => $username,
                        ),
                        true
                    );

                    if ($ldap_data != null) {
                        $email = $ldap_data->getEmailAddress();
                        if ($email == null) {
                            $email = $username . '@laplace.univ-tlse.fr';
                        }

                        $profile->setName($ldap_data->getName());
                        $profile->setEmailAddress($email);

                        $event = Event::create(
                            Event::USER_ACCOUNT_CREATED,
                            $profile
                        );

                        $mngr->persist($profile);
                        $mngr->persist($event);
                        $mngr->flush();

                        $this->flashInfo('Votre profil a été créé.');

                        // notification
                        $this->get('laplace.alert')->important(
                            $admin_url,
                            '%s a créé son profil.',
                            $profile->getFullName()
                        );

                        // email
                        $this->get('laplace.mailer')->send(
                            $profile,
                            'CORRESP_FORM',
                            'Nouveau profil',
                            $this->renderView(
                                'LaplaceUserBundle:Mail:new-profile.html.twig',
                                array(
                                    'profile'   => $profile,
                                    'comment'   => $comment,
                                    'url'       => $admin_url
                                )
                            )
                        );

                        // authentification automatique
                        $token = new LdapUserLoggedInToken();
                        $token->setUser($profile);
                        $this->get('security.context')->setToken($token);

                        return $this->redirect($user_url);
                    } else {
                        $parameters['bad_credentials'] = true;
                    }

                } else {
                    // no $this->flashFormError()!
                }
            }

            // échec de la validation : création de la vue
            $parameters[$form_name] = $form->createView();
        }

        return $this->render(
            'LaplaceUserBundle:UserProfile:create.html.twig',
            $parameters
        );
    }
    
    /**
     * Page d'inscription d'un compte local (réservé aux administrateurs)
     *
     * @param  bool|null     $doctorant
     * @return Response
     */
    public function createLocalAction($doctorant = null)
    {
        $mngr           = $this->getDoctrine()->getManager();

        $parameters     = array();
        $profile        = new User();
        
        $profile->setEmailAddress('');
        $profile->setRole('ROLE_USER');

        if ($doctorant !== null) {

            if ($doctorant) {
                $profile->setUserInfo(new InfoDoctorant());
            } else {
                $profile->setUserInfo(new InfoAgent());
            }

            // données initiales du formulaire
            $form_name      = 'new_profile_form';
            $data           = array(
                'name'      => null,
                'identity'  => $profile,
                'user'      => $profile,
                'userinfo'  => $profile->getUserInfo(),
            );

            // construction du formulaire
            $builder = $this->createNamedFormBuilder($form_name, $data);
            
            $builder->add('name', 'text',
                array(
                    'label' =>
                        'Nom',
                    'required' =>
                        true,
                    'constraints' =>
                        new Assert\Length(
                            array(
                                // attention à prendre en compte la longueur du suffixe,
                                // la longueur finale ne doit pas dépasser 64 caractères
                                'max'   => 32,
                            )
                        ),
                )
            );
            
            $builder->add('firstname', 'text',
                array(
                    'label' =>
                        'Prénom',
                    'required' =>
                        true,
                    'constraints' =>
                        new Assert\Length(
                            array(
                                // attention à prendre en compte la longueur du suffixe,
                                // la longueur finale ne doit pas dépasser 64 caractères
                                'max'   => 32,
                            )
                        ),
                )
            );
            
            $builder->add('identity', new IdentityType(),
                array(
                    'allow_change_role'     => true,
                    'ask_username'          => true,
                    'show_disabled'         => false,
                )
            );
            $builder->add('user', new UserType());
            if ($profile->estDoctorant()) {
                $builder->add('userinfo', new InfoDoctorantType(),
                    array(
                        'em' => $mngr
                    )
                );
            } else {
                $builder->add('userinfo', new InfoAgentType());
            }
            $builder->add('Envoyer', 'submit');

            // validation
            $form = $builder->getForm();
            if ($this->isNamedFormSubmitted($form_name, $form)) {
                if ($form->isValid()) {

                    $data = $form->getData();

                    $name       = $data['name'];
                    $firstname = $data['firstname'];
                    $username   = $profile->getUsername();
                    
                    $admin_url = $this->generateUrl(
                        'laplace_user_adm_view_profile',
                        array(
                            'username' => $username,
                        ),
                        true
                    );

                    $profile->setName("$name");
                    $profile->setFirstName("$firstname (local)");

                    $mngr->persist($profile);
                    $mngr->flush();

                    $this->flashInfo('Le profil local a été créé.');

                    return $this->redirect($admin_url);
                } else {
                    $this->flashFormError();
                }
            }

            // échec de la validation : création de la vue
            $parameters[$form_name] = $form->createView();
        }

        return $this->render(
            'LaplaceUserBundle:UserProfile:create-local.html.twig',
            $parameters
        );
    }

    /**
     * Affiche un profil. Un utilisateur peut voir seulement son propre profil.
     * Un administrateur peut voir n'importe quel profil. Cette contrainte
     * n'est pas vérifiée ici mais est imposée par le firewall.
     *
     * @param  string   $username
     * @return Response
     *
     * @ParamConverter("profile", options={"mapping":{"username":"username"}})
     */
    public function viewAction($admin, User $profile = null)
    {
        $is_admin = 0;
        $notif = 0;
        
        if (!$admin) {
            $profile = $this->getUser();
        }

        $this->check($profile);
        
        if($profile->getRole() == 'ROLE_ADMIN'){
                
            $is_admin = 1;

            //On récupère le ficghier de config
            $file = fopen("../laplace_config.json", "r") or die("Unable to open file!");
            $read = fread($file,filesize("../laplace_config.json"));
            fclose($file);

            //On transforme le json en objet PHP
            $config = json_decode($read);

            //On regarde si l'admin veut recevoir les emails
            $notif = in_array($profile->getEmailAddress(), $config->admin_mailer) ? 1 : 0;
                
        }

        return $this->render(
            'LaplaceUserBundle:UserProfile:view.html.twig',
            array(
                'profile'   => $profile,
                'admin'     => $admin,
                'is_admin' => $is_admin, //Pour ne pas trop changer l'affichage, mais quand même un peu
                'notif' => $notif,
            )
        );
    }

    /**
     * Affiche tous les profils inscrits durant l'année spécifiée
     *
     * @return Response
     */
    public function viewAllAction(){
        return $this->sendView('all');
    }
    
    /**
     * Affiche tous les utilisateurs (peu importe leur rôle) étant présent en permanence (CDI, ...).
     * 
     * @return Response
     */
    public function viewPermanentsAction(){
        return $this->sendView('permanents');
    }
    
    /**
     * Affiche tous les utilisateurs (peu importe leur rôle) ayant un contrat temporaire (stagiaire, doctorant, ...).
     */
    public function viewContractuelsAction(){
        return $this->sendView('contractuels');
    }

    /**
     * Affiche tous les profils inscrits durant l'année spécifiée et
     * qui ont le rôle `ROLE_LIMITED` (profils désactivés ou en attente
     * de validation).
     *
     * @return Response
     */
    public function viewLimitedAction(){
        return $this->sendView('limited');        
    }
    
    /**
     * Charge et renvoie la vue au client en fonction de la requête
     * 
     * @param $search Type de recherches effectuée
     * @return Response
     */
    public function sendView($search){
        $parameters = array();
        $parameters['search'] = $search;
        
        //On commence par créer le formulaire de recherche
        $name_form = 'search_form';
        
        $this->getProfileList($parameters);

        return $this->render(
            'LaplaceUserBundle:UserProfile:view-all.html.twig',
            $parameters
        );
    }

    /**
     * Affiche la page "Modifier un profil". Un utilisateur peut modifier son
     * propre profil. Un administrateur peut éditer n'importe quel profil.
     * Cette contrainte n'est pas vérifiée ici mais est imposée par le firewall.
     *
     * @param  bool     $admin
     * @param  User     $user
     * @return Response
     *
     * @ParamConverter("profile", options={"mapping":{"username":"username"}})
     */
    public function editAction($admin, User $profile = null)
    {
        $is_admin = 0;
        $notif = 0; 
        
        if (!$admin) {
            $profile = $this->getUser();
        }

        $this->check($profile);
        
        if($profile->getRole() == 'ROLE_ADMIN'){
                
            $is_admin = 1;

            //On récupère le ficghier de config
            $file = fopen("../laplace_config.json", "r") or die("Unable to open file!");
            $read = fread($file,filesize("../laplace_config.json"));
            fclose($file);

            //On transforme le json en objet PHP
            $config = json_decode($read);

            //On regarde si l'admin veut recevoir les emails
            $notif = in_array($profile->getEmailAddress(), $config->admin_mailer) ? 1 : 0;
                
        }

        // paramètres de la vue
        $parameters = array(
            'profile'   => $profile,
            'admin'     => $admin,
            'is_admin' => $is_admin
        );

        $redirect = false;

        // validation des formulaires
        // (on pourrait tout écrire avec des || mais cette forme semble plus
        // lisible)
        if($this->doEditAdminForm($parameters, 'admin_form', $is_admin, $notif)){
            $redirect = true;
        } elseif ($this->doEditIdentityForm($parameters, 'identity_form')) {
            $redirect = true;
        } elseif ($this->doEditUserForm($parameters, 'user_form')) {
            $redirect = true;
        } elseif ($this->doEditUserInfoForm($parameters, 'userinfo_form')) {
            $redirect = true;
        }

        // on doit préparer l'url après la validation des formulaires à cause
        // du cas où l'utilisateur change de nom d'utilisateur
        if ($admin) {
            $url = $this->generateUrl(
                'laplace_user_adm_edit_profile',
                array(
                    'username' => $profile->getUsername(),
                )
            );
        } else {
            $url = $this->generateUrl(
                'laplace_user_edit_my_profile',
                array()
            );
        }

        if ($redirect) {
            return $this->redirect($url);
        }

        // affichage de la vue
        return $this->render(
            'LaplaceUserBundle:UserProfile:edit.html.twig',
            $parameters
        );
    }
    
    /**
     * Modifie les droits d'un utilisateur (admin / user / limité).
     * Cette méthode est appelée en AJAX sur la page d'administration des profils.
     * Seul un administrateur peut appeler cette méthode.
     *
     * @param  bool     $admin
     * @param  User     $user
     * @return Response réponse envoyée au javascript en JSON
     *
     * @ParamConverter("profile", options={"mapping":{"username":"username"}})
     */
    public function changeDroitsAction($admin, User $profile = null, Request $req)
    {
        if (!$admin) {
            return new JsonResponse('Vous devez être administrateur !', 404);
        }

        $this->check($profile);
        
        $new_droits = $req->get('droits');
        
        $sql = 'LIMITED';
        if($new_droits == 'Admin'){
            $sql = 'ADMIN';
        }elseif($new_droits == 'User'){
            $sql = 'USER';
        }
        
        //La requete est compliquée à faire avec doctrine avec les jointure et tout et tout,
        //on préférera donc du SQL pur.
        $request = "UPDATE public.user "
                . "SET role='ROLE_" . $sql . "' "
                . "WHERE username='" . $profile->getUserName() . "';";
        
        //On récupère le gestionnaire de BD
        $mngr = $this->getDoctrine()->getManager();
        $stmt = $mngr->getConnection()->prepare($request);
        $stmt->execute();
        
        $choix1 = '';
        $choix2 = '';
        
        switch($new_droits){
            case 'Admin':
                $choix1 = 'User';
                $choix2 = 'Limité';
                break;
            case 'User':
                $choix1 = 'Limité';
                $choix2 = 'Admin';
                break;
            default:
                //On est en limité
                $choix1 = 'User';
                $choix2 = 'Admin';
        }
        
        return new JsonResponse(array('name' => $profile->getName(), 'droits' => $new_droits, 'choix1' => $choix1, 'choix2' => $choix2), 200);
    }
    
    /**
     * Modifie la présence d'un utilisateur (permanent / contractuel).
     * Cette méthode est appelée en AJAX sur la page d'administration des profils.
     * Seul un administrateur peut appeler cette méthode.
     *
     * @param  bool     $admin
     * @param  User     $user
     * @return Response réponse envoyée au javascript en JSON
     *
     * @ParamConverter("profile", options={"mapping":{"username":"username"}})
     */
    public function changePresenceAction($admin, User $profile = null)
    {
        if (!$admin) {
            return new JsonResponse('Vous devez être administrateur !', 404);
        }

        $this->check($profile);
        
        $new_state = $profile->estDoctorant() ? 0 : 1;
        
        //La requete est compliquée à faire avec doctrine avec les jointure et tout et tout,
        //on préférera donc du SQL pur.
        $request = "UPDATE userinfo "
                . "SET subclass = " . $new_state . " "
                . "WHERE userinfo.id = ("
                    . "SELECT id "
                    . "FROM public.user "
                    . "WHERE username = '" . $profile->getUserName() . "' "
                . ");";
        
        //On récupère le gestionnaire de BD
        $mngr = $this->getDoctrine()->getManager();
        $stmt = $mngr->getConnection()->prepare($request);
        $stmt->execute();

        $new_presence = $new_state == 0 ? 'Permanent' : 'Contractuel';
        $new_list = $new_state == 0 ? 'Contractuel' : 'Permanent';
        
        return new JsonResponse(array('name' => $profile->getName(), 'presence' => $new_presence, 'list' => $new_list), 200);
    }

    /**
     * Affiche la page "Supprimer un profil".
     *
     * Il y a un problème de conception ici : le UserBundle ne devrait pas
     * dépendre de TrainingBundle, ce qui est le cas puisque l'on supprime
     * les besoins, demandes et fils de discussions. Le code métier aurait
     * mérité d'être intégré à un ou plusieurs services bien distincts.
     *
     * @param  User     $user
     * @return Response
     *
     * @ParamConverter("profile", options={"mapping":{"username":"username"}})
     */
    public function deleteAction(User $profile = null)
    {
        $this->check($profile);

        if ($this->getUser() == $profile) {
            throw new AccessDeniedHttpException(
                'Vous ne pouvez pas supprimer votre propre profil.'
            );
        }

        $mngr = $this->getDoctrine()->getManager();

        // données initiales du formulaires
        $form_name = 'delete_form';
        $data = array(
            'delete' => false,
        );

        // construction du formulaire
        $builder = $this->createNamedFormBuilder($form_name, $data);
        $builder->add('delete', 'checkbox',
            array(
                'label' =>
                    'Supprimer l\'utilisateur et tout ce qui lui est rattaché',
                'constraints' =>
                    new Assert\True(),
            )
        );
        $builder->add('do', 'submit',
            array(
                'label' => 'Supprimer',
            )
        );

        $url = $this->generateUrl('laplace_user_adm_all_profiles', array());

        // validation
        $form = $builder->getForm();
        if ($this->isNamedFormSubmitted($form_name, $form)) {
            if ($form->isValid()) {
                // suppression de l'historique
                $event_repo = $mngr
                    ->getRepository('LaplaceTrainingBundle:Event');

                $event_repo->deleteEvents(
                    $event_repo->fetchBy(array('user' => $profile))
                );

                // suppression des inscriptions (besoins + demandes)
                $mngr
                    ->getRepository('LaplaceTrainingBundle:NeedSubscription')
                    ->deleteByUser($profile);

                $mngr
                    ->getRepository('LaplaceTrainingBundle:RequestSubscription')
                    ->deleteByUser($profile);

                // suppression des messages et des discussions
                $mngr
                    ->getRepository('LaplaceTrainingBundle:Message')
                    ->deleteByUser($profile);

                $mngr
                    ->getRepository('LaplaceTrainingBundle:Thread')
                    ->deleteByUser($profile);

                // pour les besoins, demandes on remplace l'auteur
                // supprimé par l'administrateur qui le supprime
                $mngr
                    ->getRepository('LaplaceTrainingBundle:Need')
                    ->replaceAuthor($profile, $this->getUser());

                $mngr
                    ->getRepository('LaplaceTrainingBundle:Request')
                    ->replaceAuthor($profile, $this->getUser());

                // enfin, on supprime l'utilisateur
                $mngr->remove($profile);

                // c'est la fin

                $mngr->flush();

                $this->flashInfo('L\'utilisateur a été supprimé.');

                return $this->redirect($url);
            } else {
                $this->flashFormError();
            }

        }

        // échec de la validation : création de la vue
        $parameters = array(
            'profile'   => $profile,
            $form_name  => $form->createView(),
        );

        return $this->render(
            'LaplaceUserBundle:UserProfile:delete.html.twig',
            $parameters
        );
    }

    /**
     * Retourne une liste d'utilisateurs dont le nom, prénom ou nom
     * d'utilisateur correspondent à la valeur demandée. Cette action est
     * appelée en AJAX lorsqu'un client tape un nom d'utilisateur.
     * La requête doit contenir une valeur pour la clef `query`. Cette valeur
     * est découpée en tableau au niveau des caractères d'espacement. Chaque
     * morceau non-vide consistue un critère.
     *
     * @param  Request      $request
     * @return JsonResponse Un tableau JSON d'objets { username:?, fullname:? }
     */
    public function findUsersAction(Request $request)
    {
        $mngr       = $this->getDoctrine()->getManager();

        $data       = array();
        $query      = $request->request->get('query');

        $names = preg_split('/\s+/', $query);

        if (count($names) > 0) {
            $identities = $mngr
                    ->getRepository('LaplaceUserBundle:User')
                    ->fetchByName($names);

            foreach ($identities as &$identity) {
                $data[] = array(
                    'username'  => $identity->getUsername(),
                    'fullname'  => $identity->getFullName(),
                );
            }
        }

        return new JsonResponse($data);
    }

    /**********************************************************************/

    private function check(User $user = null)
    {
        if ($user == null) {
            throw $this->createNotFoundException('Utilisateur inconnu.');
        }
    }
    
    private function doEditAdminForm(&$parameters, $form_name, $admin, $notif){

        if($admin == 0) return false;

        // données initiales du formulaire
        $data = array(
            'email_notif' => $notif == 1,
        );

        // construction du formulaire
        $builder = $this->createNamedFormBuilder($form_name, $data);
        $builder->add('email_notif', 'checkbox',
            array(
                'label' => 'Recevoir les e-mails de notification ?',
                'required' => false,
            )
        );
        $builder->add('Envoyer', 'submit');

        // validation
        $form = $builder->getForm();
        if ($this->isNamedFormSubmitted($form_name, $form)) {
            if ($form->isValid()) {
                
                $data   = $form->getData();
                $notif_d = $data['email_notif'];

                //On récupère le fichier de config
                $file = fopen("../laplace_config.json", "r") or die("Unable to open file!");
                $read = fread($file,filesize("../laplace_config.json"));
                fclose($file);

                //On transforme le json en objet PHP
                $config = json_decode($read);

                //On regarde si l'admin veut recevoir les emails
                $notif = in_array($parameters['profile']->getEmailAddress(), $config->admin_mailer) ? 1 : 0;
                
                //L'utilisateur est passé de non-receveur à receveur
                if($notif == 0 && $notif_d){
                    array_push($config->admin_mailer, $parameters['profile']->getEmailAddress());
                }elseif($notif == 1 && !$notif_d){
                    //L'utilisateur est passé de receveur à non-receveur
                    $config->admin_mailer = array(array_diff($config->admin_mailer, array($parameters['profile']->getEmailAddress())));
                    $config->admin_mailer = $config->admin_mailer[0];
                }
                
                //On récupère le ficghier de config
                file_put_contents("../laplace_config.json", json_encode($config));

                return true;
            } else {
                $this->flashFormError();
                $mngr->refresh($parameters['profile']);
            }
        }
        
        $parameters[$form_name] = $form->createView();
        
        return false;
    }

    private function doEditIdentityForm(&$parameters, $form_name)
    {
        $mngr           = $this->getDoctrine()->getManager();

        $profile        = $parameters['profile'];
        $admin          = $parameters['admin'];

        // données initiales du formulaire
        $data = array(
            'identity' => $profile,
        );

        // construction du formulaire
        $builder = $this->createNamedFormBuilder($form_name, $data);
        $builder->add('identity', new IdentityType(),
            array(
                'ask_username'          => $admin,
                'allow_change_role'     => $admin,
                'allow_change_name'     => $admin,
                'allow_change_email'    => $admin,
            )
        );
        $builder->add('Envoyer', 'submit');

        // validation
        $form = $builder->getForm();
        if ($this->isNamedFormSubmitted($form_name, $form)) {
            if ($form->isValid()) {

                if ($profile->getName() === null) {
                    $profile->setName('');
                }

                if ($profile->getEmailAddress() === null) {
                    $profile->setEmailAddress('');
                }

                $mngr->persist($profile);
                $mngr->flush();

                $this->flashInfo('Identité mise à jour.');

                return true;
            } else {
                $this->flashFormError();
                $mngr->refresh($profile);
            }
        }

        // échec de la validation : création de la vue
        $parameters[$form_name] = $form->createView();

        return false;
    }

    private function doEditUserForm(&$parameters, $form_name)
    {
        $mngr = $this->getDoctrine()->getManager();

        $profile = $parameters['profile'];

        // données initiales du formulaire
        $data = array(
            'user' => $profile,
        );

        // construction du formulaire
        $builder = $this->createNamedFormBuilder($form_name, $data);
        $builder->add('user', new UserType());
        $builder->add('Envoyer', 'submit');

        // validation
        $form = $builder->getForm();
        if ($this->isNamedFormSubmitted($form_name, $form)) {
            if ($form->isValid()) {
                $mngr->persist($profile);
                $mngr->flush();

                $this->flashInfo('Rattachement mis à jour.');

                return true;
            } else {
                $this->flashFormError();
                $mngr->refresh($profile);
            }
        }

        // échec de la validation : création de la vue
        $parameters[$form_name] = $form->createView();

        return false;
    }

    private function doEditUserInfoForm(&$parameters, $form_name)
    {
        $mngr           = $this->getDoctrine()->getManager();

        $profile        = $parameters['profile'];
        $userinfo       = $profile->getUserInfo();

        // données initiales du formulaire
        $data = array(
            'userinfo' => $userinfo,
        );

        // construction du formulaire
        $builder = $this->createNamedFormBuilder($form_name, $data);
        if ($profile->estDoctorant()) {
            $builder->add('userinfo', new InfoDoctorantType(),
                array(
                    'em' => $mngr
                )
            );
        } else {
            $builder->add('userinfo', new InfoAgentType());
        }
        $builder->add('Envoyer', 'submit');

        // validation
        $form = $builder->getForm();
        if ($this->isNamedFormSubmitted($form_name, $form)) {
            if ($form->isValid()) {
                $mngr->persist($userinfo);
                $mngr->flush();

                $this->flashInfo('Situation professionnelle mise à jour.');

                return true;
            } else {
                $this->flashFormError();
                $mngr->refresh($userinfo);
            }
        }

        // échec de la validation : création de la vue
        $parameters[$form_name] = $form->createView();

        return false;
    }

    /**
     * Recherche des profils inscrits durant une année donnée et qui ont un
     * certain rôle.
     *
     * @param array $parameters Un tableau de paramètres augmenté
     * des valeurs `users` (UserInfo[], les profils trouvés), `years` (int[],
     * la plage d'années à afficher) et `requested` (int, l'année demandée).
     * @param int|null    $year Une année ou l'année actuelle si null
     * @param string|null $role
     */
    private function getProfileList(&$parameters)
    {   
        
        $mngr = $this
            ->getDoctrine()
            ->getManager();

        // récupération des utilisateurs
        if($parameters['search'] == 'all' || $parameters['search'] == 'contractuels' || $parameters['search'] == 'permanents'){
            $parameters['users'] = $mngr
                ->getRepository('LaplaceUserBundle:User')
                ->findAll();
            
            /** //On met toutes les données de la table à jour pour les prénoms
            for($i = 0; $i < count($parameters['users']); $i++){
                
                $name = split(' ', $parameters['users'][$i]->getFirstName()); 
                
                //Il y a des noms composés avec des espaces...
                $firstname = $name[count($name) - 1];
                
                $parameters['users'][$i]->setFirstName($firstname);

                unset($name[count($name) - 1]);
                $name = implode(' ', $name);
                
                $parameters['users'][$i]->setName($name);
                
                //On enregistre le tout
                $mngr->persist($parameters['users'][$i]);
                $mngr->flush();
                
            }*/
            
        }else if($parameters['search'] == 'limited'){
            $parameters['users'] = $mngr
                ->getRepository('LaplaceUserBundle:User')
                ->fetchByRole('ROLE_LIMITED');
        }
    }

}
