<?php

namespace Laplace\UserBundle\Controller;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\DBAL\DBALException;

use Laplace\CommonBundle\Controller\AbstractBaseController;
use Laplace\UserBundle\Entity\Tutelle;
use Laplace\UserBundle\Entity\Site;
use Laplace\UserBundle\Entity\StatutAgent;
use Laplace\UserBundle\Entity\StatutDoctorant;
use Laplace\UserBundle\Form\TutelleType;
use Laplace\UserBundle\Form\SiteType;
use Laplace\UserBundle\Form\StatutAgentType;
use Laplace\UserBundle\Form\StatutDoctorantType;

class SystemController extends AbstractBaseController
{

    /**
     * Administration des tutelles.
     */
    public function manageTutellesAction()
    {
        $mngr = $this->getDoctrine()->getManager();

        $tutelles = $mngr
            ->getRepository('LaplaceUserBundle:Tutelle')
            ->findBy(array(), array('nom' => 'ASC'));

        $url = $this->generateUrl('laplace_user_adm_tutelles');

        $parameters = array();

        // Formulaire de création

        if ($this->doCreateTutelleForm($parameters, 'create_form')) {
            return $this->redirect($url);
        }

        // Formulaire de suppression

        if ($this->doDeleteTutelleForm($parameters, 'delete_form')) {
            return $this->redirect($url);
        }

        // Formulaire de remplacement

        if ($this->doReplaceTutelleForm($parameters, 'replace_form')) {
            return $this->redirect($url);
        }

        // Formulaires d'édition

        $parameters['edit_forms'] = array();
        foreach ($tutelles as $tutelle) {
            // données initiales du formulaire
            $form_name  = 'edit_form_' . $tutelle->getId();
            $data       = array(
                'tutelle' => $tutelle,
            );

            // construction du formulaire
            $builder = $this->createNamedFormBuilder($form_name, $data);
            $builder->add('tutelle', new TutelleType());
            $builder->add('Envoyer', 'submit');

            // validation
            $form = $builder->getForm();
            if ($this->isNamedFormSubmitted($form_name, $form)) {
                if ($form->isValid()) {
                    $mngr->persist($tutelle);
                    $mngr->flush();

                    $this->flashInfo('La tutelle a été mise à jour.');

                    return $this->redirect($url);
                } else {
                    $this->flashFormError();
                }
            }

            // échec de la validation : création de la vue
            $parameters['edit_forms'][] = $form->createView();
        }

        return $this->render(
            'LaplaceUserBundle:System:manage-tutelles.html.twig',
            $parameters
        );
    }

    /**
     * Administration des sites.
     */
    public function manageSitesAction()
    {
        $mngr = $this->getDoctrine()->getManager();

        $sites = $mngr
            ->getRepository('LaplaceUserBundle:Site')
            ->findBy(array(), array('name' => 'ASC'));

        $url = $this->generateUrl('laplace_user_adm_sites');

        $parameters = array();

        // Formulaire de création

        if ($this->doCreateSiteForm($parameters, 'create_form')) {
            return $this->redirect($url);
        }

        // Formulaire de suppression

        if ($this->doDeleteSiteForm($parameters, 'delete_form')) {
            return $this->redirect($url);
        }

        // Formulaire de remplacement

        if ($this->doReplaceSiteForm($parameters, 'replace_form')) {
            return $this->redirect($url);
        }

        // Formulaires d'édition

        $parameters['edit_forms'] = array();
        foreach ($sites as $site) {
            // données initiales du formulaire
            $form_name  = 'edit_form_' . $site->getId();
            $data       = array(
                'site' => $site,
            );

            // construction du formulaire
            $builder = $this->createNamedFormBuilder($form_name, $data);
            $builder->add('site', new SiteType());
            $builder->add('Envoyer', 'submit');

            // validation
            $form = $builder->getForm();
            if ($this->isNamedFormSubmitted($form_name, $form)) {
                if ($form->isValid()) {
                    $mngr->persist($site);
                    $mngr->flush();

                    $this->flashInfo('Le site a été mis à jour.');

                    return $this->redirect($url);
                } else {
                    $this->flashFormError();
                }
            }

            // échec de la validation : création de la vue
            $parameters['edit_forms'][] = $form->createView();
        }

        return $this->render(
            'LaplaceUserBundle:System:manage-sites.html.twig',
            $parameters
        );
    }

    /**
     * Administration des statuts des agents.
     */
    public function manageStatutsAgentsAction()
    {
        $mngr = $this->getDoctrine()->getManager();

        $statuts = $mngr
            ->getRepository('LaplaceUserBundle:StatutAgent')
            ->findBy(array(),
                array(
                    'groupe'    => 'ASC',
                    'intitule'  => 'ASC',
                )
            );

        $url = $this->generateUrl('laplace_user_adm_statuts_agents');

        $parameters = array();

        // Formulaire de création

        if ($this->doCreateStatutAgentForm($parameters, 'create_form')) {
            return $this->redirect($url);
        }

        // Formulaire de suppression

        if ($this->doDeleteStatutAgentForm($parameters, 'delete_form')) {
            return $this->redirect($url);
        }

        // Formulaire de remplacement

        if ($this->doReplaceStatutAgentForm($parameters, 'replace_form')) {
            return $this->redirect($url);
        }

        // Formulaires d'édition

        $parameters['edit_forms'] = array();
        foreach ($statuts as $statut) {
            // données initiales du formulaire
            $form_name  = 'edit_form_' . $statut->getId();
            $data       = array(
                'statut' => $statut,
            );

            // construction du formulaire
            $builder = $this->createNamedFormBuilder($form_name, $data);
            $builder->add('statut', new StatutAgentType());
            $builder->add('Envoyer', 'submit');

            // validation
            $form = $builder->getForm();
            if ($this->isNamedFormSubmitted($form_name, $form)) {
                if ($form->isValid()) {
                    $mngr->persist($statut);
                    $mngr->flush();

                    $this->flashInfo('Le statut a été mis à jour.');

                    return $this->redirect($url);
                } else {
                    $this->flashFormError();
                }
            }

            // échec de la validation : création de la vue
            $parameters['edit_forms'][] = $form->createView();
        }

        return $this->render(
            'LaplaceUserBundle:System:manage-statuts-agents.html.twig',
            $parameters
        );
    }

    /**
     * Administration des statuts des doctorants.
     */
    public function manageStatutsDoctorantsAction()
    {
        $mngr = $this->getDoctrine()->getManager();

        $statuts = $mngr
            ->getRepository('LaplaceUserBundle:StatutDoctorant')
            ->findBy(array(),
                array(
                    'groupe'    => 'ASC',
                    'intitule'  => 'ASC',
                )
            );

        $url = $this->generateUrl('laplace_user_adm_statuts_doctorants');

        $parameters = array();

        // Formulaire de création

        if ($this->doCreateStatutDoctorantForm($parameters, 'create_form')) {
            return $this->redirect($url);
        }

        // Formulaire de suppression

        if ($this->doDeleteStatutDoctorantForm($parameters, 'delete_form')) {
            return $this->redirect($url);
        }

        // Formulaire de remplacement

        if ($this->doReplaceStatutDoctorantForm($parameters, 'replace_form')) {
            return $this->redirect($url);
        }

        // Formulaires d'édition

        $parameters['edit_forms'] = array();
        foreach ($statuts as $statut) {
            // données initiales du formulaire
            $form_name  = 'edit_form_' . $statut->getId();
            $data       = array(
                'statut' => $statut,
            );

            // construction du formulaire
            $builder = $this->createNamedFormBuilder($form_name, $data);
            $builder->add('statut', new StatutDoctorantType());
            $builder->add('Envoyer', 'submit');

            // validation
            $form = $builder->getForm();
            if ($this->isNamedFormSubmitted($form_name, $form)) {
                if ($form->isValid()) {
                    $mngr->persist($statut);
                    $mngr->flush();

                    $this->flashInfo('Le statut a été mis à jour.');

                    return $this->redirect($url);
                } else {
                    $this->flashFormError();
                }
            }

            // échec de la validation : création de la vue
            $parameters['edit_forms'][] = $form->createView();
        }

        return $this->render(
            'LaplaceUserBundle:System:manage-statuts-doctorants.html.twig',
            $parameters
        );
    }

    /**********************************************************************/
    /*                                                                    */
    /* TUTELLES                                                           */
    /*                                                                    */
    /**********************************************************************/

    private function doCreateTutelleForm(&$parameters, $form_name)
    {
        $mngr = $this->getDoctrine()->getManager();

        // données initiales du formulaire
        $data = array(
            'tutelle' => new Tutelle(),
        );

        // construction du formulaire
        $builder = $this->createNamedFormBuilder($form_name, $data);
        $builder->add('tutelle', new TutelleType());
        $builder->add('Envoyer', 'submit');

        // validation
        $form = $builder->getForm();
        if ($this->isNamedFormSubmitted($form_name, $form)) {
            if ($form->isValid()) {
                $data       = $form->getData();
                $tutelle    = $data['tutelle'];

                $mngr->persist($tutelle);
                $mngr->flush();

                $this->flashInfo('La tutelle a été créée.');

                return true;
            } else {
                $this->flashFormError();
            }
        }

        // échec de la validation : création de la vue
        $parameters[$form_name] = $form->createView();

        return false;
    }

    private function doDeleteTutelleForm(&$parameters, $form_name)
    {
        $mngr = $this->getDoctrine()->getManager();

        // données initiales du formulaire
        $data = array(
            'tutelle' => null,
        );

        // construction du formulaire
        $builder = $this->createNamedFormBuilder($form_name, $data);
        $builder->add('tutelle', 'entity',
            array(
                'class'         => 'LaplaceUserBundle:Tutelle',
                'property'      => 'nom',
                'constraints'   => new Assert\NotNull(),
                'label'         => 'Tutelle',
            )
        );
        $builder->add('do', 'submit',
            array(
                'label' => 'Supprimer',
            )
        );

        // validation
        $form = $builder->getForm();
        if ($this->isNamedFormSubmitted($form_name, $form)) {
            if ($form->isValid()) {
                $data       = $form->getData();
                $tutelle    = $data['tutelle'];

                try {
                    $mngr->remove($tutelle);
                    $mngr->flush();

                    $this->flashInfo('La tutelle a été supprimée.');

                    return true;
                } catch (DBALException $e) {
                    $this->flashError(
                        'Impossible de supprimer cette tutelle ' .
                        'car elle est encore utilisée.'
                    );
                }
            } else {
                $this->flashFormError();
            }
        }

        // échec de la validation : création de la vue
        $parameters[$form_name] = $form->createView();

        return false;
    }

    private function doReplaceTutelleForm(&$parameters, $form_name)
    {
        $mngr = $this->getDoctrine()->getManager();

        // données initiales du formulaire
        $data = array(
            'from'  => null,
            'to'    => null,
        );

        // construction du formulaire
        $builder = $this->createNamedFormBuilder($form_name, $data);
        $builder->add('from', 'entity',
            array(
                'class'         => 'LaplaceUserBundle:Tutelle',
                'property'      => 'nom',
                'constraints'   => new Assert\NotNull(),
                'label'         => 'Remplacer',
            )
        );
        $builder->add('to', 'entity',
            array(
                'class'         => 'LaplaceUserBundle:Tutelle',
                'property'      => 'nom',
                'constraints'   => new Assert\NotNull(),
                'label'         => 'Par',
            )
        );
        $builder->add('do', 'submit',
            array(
               'label' => 'Remplacer',
            )
        );

        // validation
        $form = $builder->getForm();
        if ($this->isNamedFormSubmitted($form_name, $form)) {
            if ($form->isValid()) {
                $data       = $form->getData();
                $from       = $data['from'];
                $to         = $data['to'];

                try {
                    $rows = $mngr
                        ->createQueryBuilder()
                        ->update('LaplaceUserBundle:User', 'user')
                        ->set('user.tutelle', ':to')
                        ->where('user.tutelle = :from')
                        ->setParameter('from', $from)
                        ->setParameter('to', $to)
                        ->getQuery()
                        ->execute();

                    $this->flashInfo(
                        'La tutelle "%s" a été remplacée par "%s"' .
                        ' [%d ligne(s) affectée(s)].',
                        $from->getNom(), $to->getNom(), $rows
                    );

                    return true;
                } catch (DBALException $e) {
                    $this->flashError('Erreur lors du remplacement.');
                }
            } else {
                $this->flashFormError();
            }
        }

        // échec de la validation : création de la vue
        $parameters[$form_name] = $form->createView();

        return false;
    }

    /**********************************************************************/
    /*                                                                    */
    /* SITES                                                              */
    /*                                                                    */
    /**********************************************************************/

    private function doCreateSiteForm(&$parameters, $form_name)
    {
        $mngr = $this->getDoctrine()->getManager();

        // données initiales du formulaire
        $data = array(
            'site' => new Site(),
        );

        // construction du formulaire
        $builder = $this->createNamedFormBuilder($form_name, $data);
        $builder->add('site', new SiteType());
        $builder->add('Envoyer', 'submit');

        // validation
        $form = $builder->getForm();
        if ($this->isNamedFormSubmitted($form_name, $form)) {
            if ($form->isValid()) {
                $data       = $form->getData();
                $site       = $data['site'];

                $mngr->persist($site);
                $mngr->flush();

                $this->flashInfo('Le site a été créé.');

                return true;
            } else {
                $this->flashFormError();
            }
        }

        // échec de la validation : création de la vue
        $parameters[$form_name] = $form->createView();

        return false;
    }

    private function doDeleteSiteForm(&$parameters, $form_name)
    {
        $mngr = $this->getDoctrine()->getManager();

        // données initiales du formulaire
        $data = array(
            'site' => null,
        );

        // construction du formulaire
        $builder = $this->createNamedFormBuilder($form_name, $data);
        $builder->add('site', 'entity',
            array(
                'class'         => 'LaplaceUserBundle:Site',
                'property'      => 'name',
                'constraints'   => new Assert\NotNull(),
                'label'         => 'Site',
            )
        );
        $builder->add('do', 'submit',
            array(
                'label' => 'Supprimer',
            )
        );

        // validation
        $form = $builder->getForm();
        if ($this->isNamedFormSubmitted($form_name, $form)) {
            if ($form->isValid()) {
                $data       = $form->getData();
                $site       = $data['site'];

                try {
                    $mngr->remove($site);
                    $mngr->flush();

                    $this->flashInfo('Le site a été supprimé.');

                    return true;
                } catch (DBALException $e) {
                    $this->flashError(
                        'Impossible de supprimer ce site ' .
                        'car il est encore utilisé.'
                    );
                }
            } else {
                $this->flashFormError();
            }
        }

        // échec de la validation : création de la vue
        $parameters[$form_name] = $form->createView();

        return false;
    }

    private function doReplaceSiteForm(&$parameters, $form_name)
    {
        $mngr = $this->getDoctrine()->getManager();

        // données initiales du formulaire
        $data = array(
            'from'  => null,
            'to'    => null,
        );

        // construction du formulaire
        $builder = $this->createNamedFormBuilder($form_name, $data);
        $builder->add('from', 'entity',
            array(
                'class'         => 'LaplaceUserBundle:Site',
                'property'      => 'name',
                'constraints'   => new Assert\NotNull(),
                'label'         => 'Remplacer',
            )
        );
        $builder->add('to', 'entity',
            array(
                'class' => 'LaplaceUserBundle:Site',
                'property' => 'name',
                'constraints' => new Assert\NotNull(),
                'label' => 'Par',
            )
        );
        $builder->add('do', 'submit',
            array(
                'label' => 'Remplacer',
            )
        );

        // validation
        $form = $builder->getForm();
        if ($this->isNamedFormSubmitted($form_name, $form)) {
            if ($form->isValid()) {
                $data       = $form->getData();
                $from       = $data['from'];
                $to         = $data['to'];

                try {
                    $rows = $mngr
                        ->createQueryBuilder()
                        ->update('LaplaceUserBundle:User', 'user')
                        ->set('user.site', ':to')
                        ->where('user.site = :from')
                        ->setParameter('from', $from)
                        ->setParameter('to', $to)
                        ->getQuery()
                        ->execute();

                    $this->flashInfo(
                        'Le site "%s" a été remplacé par "%s"' .
                        ' [%d ligne(s) affectée(s)].',
                        $from->getName(), $to->getName(), $rows
                    );

                    return true;
                } catch (DBALException $e) {
                    $this->flashError('Erreur lors du remplacement.');
                }
            } else {
                $this->flashFormError();
            }
        }

        // échec de la validation : création de la vue
        $parameters[$form_name] = $form->createView();

        return false;
    }

    /**********************************************************************/
    /*                                                                    */
    /* STATUTS AGENTS                                                     */
    /*                                                                    */
    /**********************************************************************/

    private function doCreateStatutAgentForm(&$parameters, $form_name)
    {
        $mngr = $this->getDoctrine()->getManager();

        // données initiales du formulaire
        $data = array(
            'statut' => new StatutAgent(),
        );

        // construction du formulaire
        $builder = $this->createNamedFormBuilder($form_name, $data);
        $builder->add('statut', new StatutAgentType());
        $builder->add('Envoyer', 'submit');

        // validation
        $form = $builder->getForm();
        if ($this->isNamedFormSubmitted($form_name, $form)) {
            if ($form->isValid()) {
                $data       = $form->getData();
                $statut     = $data['statut'];

                $mngr->persist($statut);
                $mngr->flush();

                $this->flashInfo('Le statut a été créé.');

                return true;
            } else {
                $this->flashFormError();
            }
        }

        // échec de la validation : création de la vue
        $parameters[$form_name] = $form->createView();

        return false;
    }

    private function doDeleteStatutAgentForm(&$parameters, $form_name)
    {
        $mngr = $this->getDoctrine()->getManager();

        // données initiales du formulaire
        $data = array(
            'statut' => null,
        );

        // construction du formulaire
        $builder = $this->createNamedFormBuilder($form_name, $data);
        $builder->add('statut', 'entity',
            array(
                'class'         => 'LaplaceUserBundle:StatutAgent',
                'property'      => 'intitule',
                'group_by'      => 'groupe',
                'constraints'   => new Assert\NotNull(),
                'label'         => 'Statut',
            )
        );
        $builder->add('do', 'submit',
            array(
                'label' => 'Supprimer'
            )
        );

        // validation
        $form = $builder->getForm();
        if ($this->isNamedFormSubmitted($form_name, $form)) {
            if ($form->isValid()) {
                $data       = $form->getData();
                $statut     = $data['statut'];

                try {
                    $mngr->remove($statut);
                    $mngr->flush();

                    $this->flashInfo('Le statut a été supprimé.');

                    return true;
                } catch (DBALException $e) {
                    $this->flashError(
                        'Impossible de supprimer ce statut ' .
                        'car il est encore utilisé.'
                    );
                }
            } else {
                $this->flashFormError();
            }
        }

        // échec de la validation : création de la vue
        $parameters[$form_name] = $form->createView();

        return false;
    }

    private function doReplaceStatutAgentForm(&$parameters, $form_name)
    {
        $mngr = $this->getDoctrine()->getManager();

        // données initiales du formulaire
        $data = array(
            'from'  => null,
            'to'    => null,
        );

        // construction du formulaire
        $builder = $this->createNamedFormBuilder($form_name, $data);
        $builder->add('from', 'entity',
            array(
                'class'         => 'LaplaceUserBundle:StatutAgent',
                'property'      => 'intitule',
                'group_by'      => 'groupe',
                'constraints'   => new Assert\NotNull(),
                'label'         => 'Remplacer',
            )
        );
        $builder->add('to', 'entity',
            array(
                'class'         => 'LaplaceUserBundle:StatutAgent',
                'property'      => 'intitule',
                'group_by'      => 'groupe',
                'constraints'   => new Assert\NotNull(),
                'label'         => 'Par',
            )
        );
        $builder->add('do', 'submit',
            array(
                'label' => 'Remplacer',
            )
        );

        // validation
        $form = $builder->getForm();
        if ($this->isNamedFormSubmitted($form_name, $form)) {
            if ($form->isValid()) {
                $data       = $form->getData();
                $from       = $data['from'];
                $to         = $data['to'];

                try {
                    $rows = $mngr
                        ->createQueryBuilder()
                        ->update('LaplaceUserBundle:InfoAgent', 'info')
                        ->set('info.statut', ':to')
                        ->where('info.statut = :from')
                        ->setParameter('from', $from)
                        ->setParameter('to', $to)
                        ->getQuery()
                        ->execute();

                    $this->flashInfo(
                        'Le statut agent "%s" a été remplacé par "%s"' .
                        ' [%d ligne(s) affectée(s)].',
                        $from->getIntitule(), $to->getIntitule(), $rows
                    );

                    return true;
                } catch (DBALException $e) {
                    $this->flashError('Erreur lors du remplacement.');
                }
            } else {
                $this->flashFormError();
            }
        }

        // échec de la validation : création de la vue
        $parameters[$form_name] = $form->createView();

        return false;
    }

    /**********************************************************************/
    /*                                                                    */
    /* STATUTS DOCTORANTS                                                 */
    /*                                                                    */
    /**********************************************************************/

    private function doCreateStatutDoctorantForm(&$parameters, $form_name)
    {
        $mngr = $this->getDoctrine()->getManager();

        // données initiales du formulaire
        $data = array(
            'statut' => new StatutDoctorant(),
        );

        // construction du formulaire
        $builder = $this->createNamedFormBuilder($form_name, $data);
        $builder->add('statut', new StatutDoctorantType());
        $builder->add('Envoyer', 'submit');

        // validation
        $form = $builder->getForm();
        if ($this->isNamedFormSubmitted($form_name, $form)) {
            if ($form->isValid()) {
                $data       = $form->getData();
                $statut     = $data['statut'];

                $mngr->persist($statut);
                $mngr->flush();

                $this->flashInfo('Le statut a été créé.');

                return true;
            } else {
                $this->flashFormError();
            }
        }

        // échec de la validation : création de la vue
        $parameters[$form_name] = $form->createView();

        return false;
    }

    private function doDeleteStatutDoctorantForm(&$parameters, $form_name)
    {
        $mngr = $this->getDoctrine()->getManager();

        // données initiales du formulaire
        $data = array(
            'statut' => null,
        );

        // construction du formulaire
        $builder = $this->createNamedFormBuilder($form_name, $data);
        $builder->add('statut', 'entity',
            array(
                'class'         => 'LaplaceUserBundle:StatutDoctorant',
                'property'      => 'intitule',
                'group_by'      => 'groupe',
                'constraints'   => new Assert\NotNull(),
                'label'         => 'Statut',
            )
        );
        $builder->add('do', 'submit',
            array(
                'label' => 'Supprimer',
            )
        );

        // validation
        $form = $builder->getForm();
        if ($this->isNamedFormSubmitted($form_name, $form)) {
            if ($form->isValid()) {
                $data       = $form->getData();
                $statut     = $data['statut'];

                try {
                    $mngr->remove($statut);
                    $mngr->flush();

                    $this->flashInfo('Le statut a été supprimé.');

                    return true;
                } catch (DBALException $e) {
                    $this->flashError(
                        'Impossible de supprimer ce statut ' .
                        'car il est encore utilisé.'
                    );
                }
            } else {
                $this->flashFormError();
            }
        }

        // échec de la validation : création de la vue
        $parameters[$form_name] = $form->createView();

        return false;
    }

    private function doReplaceStatutDoctorantForm(&$parameters, $form_name)
    {
        $mngr = $this->getDoctrine()->getManager();

        // données initiales du formulaire
        $data = array(
            'from'  => null,
            'to'    => null,
        );

        // construction du formulaire
        $builder = $this->createNamedFormBuilder($form_name, $data);
        $builder->add('from', 'entity',
            array(
                'class'         => 'LaplaceUserBundle:StatutDoctorant',
                'property'      => 'intitule',
                'group_by'      => 'groupe',
                'constraints'   => new Assert\NotNull(),
                'label'         => 'Remplacer',
            )
        );
        $builder->add('to', 'entity',
            array(
                'class'         => 'LaplaceUserBundle:StatutDoctorant',
                'property'      => 'intitule',
                'group_by'      => 'groupe',
                'constraints'   => new Assert\NotNull(),
                'label'         => 'Par',
            )
        );
        $builder->add('do', 'submit',
            array(
                'label' => 'Remplacer',
            )
        );

        // validation
        $form = $builder->getForm();
        if ($this->isNamedFormSubmitted($form_name, $form)) {
            if ($form->isValid()) {
                $data       = $form->getData();
                $from       = $data['from'];
                $to         = $data['to'];

                try {
                    $rows = $mngr
                        ->createQueryBuilder()
                        ->update('LaplaceUserBundle:InfoDoctorant', 'info')
                        ->set('info.statut', ':to')
                        ->where('info.statut = :from')
                        ->setParameter('from', $from)
                        ->setParameter('to', $to)
                        ->getQuery()
                        ->execute();

                    $this->flashInfo(
                        'Le statut doctorant "%s" a été remplacé par "%s"' .
                        ' [%d ligne(s) affectée(s)].',
                        $from->getIntitule(), $to->getIntitule(), $rows
                    );

                    return true;
                } catch (DBALException $e) {
                    $this->flashError('Erreur lors du remplacement.');
                }
            } else {
                $this->flashFormError();
            }
        }

        // échec de la validation : création de la vue
        $parameters[$form_name] = $form->createView();

        return false;
    }

}
