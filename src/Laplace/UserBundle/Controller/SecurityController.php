<?php

namespace Laplace\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\SecurityContext as SC;

use Laplace\CommonBundle\Controller\AbstractBaseController;

/**
 * Cette classe définit le contrôleur qui gère le formulaire d'authenfication
 * d'un utilisateur.
 */
class SecurityController extends AbstractBaseController
{

    /**
     * Gère le formulaire de connexion.
     *
     * Affiche un formulaire permettant à un utilisateur de se connecter. Si
     * l'authentification échoue, le formulaire est affiché de nouveau, sinon
     * le client est redirigé vers le point d'accès de la partie utilisateur.
     *
     * @param  Request  $request
     * @return Response
     */
    public function loginAction(Request $request)
    {
        $security   = $this->get('security.context');
        $session    = $request->getSession();

        // si l'utilisateur est déjà connecté, on le redirige
        if ($security->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return $this->redirect(
                $this->generateUrl('laplace_common_user_homepage')
            );
        }

        // on vérifie s'il y a des erreurs d'une précédente soumission
        // du formulaire
        if ($request->attributes->has(SC::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SC::AUTHENTICATION_ERROR);
        } else {
            $error = $session->get(SC::AUTHENTICATION_ERROR);
            $session->remove(SC::AUTHENTICATION_ERROR);
        }

        $last_username = $session->get(SC::LAST_USERNAME);

        return $this->render(
            'LaplaceUserBundle:Security:login.html.twig',
            array(
                'last_username' => $last_username,
                'error' => $error,
            )
        );
    }

}
