<?php

namespace Laplace;

/**
 * <h1>Introduction</h1>
 * <p>
 * Cette interface a été réalisée durant un stage de 6 semaines de 2A INFO.
 * </p>
 *
 * <h1>Conventions</h1>
 * <p>
 * Le code source suit les conventions d'écriture de Symfony2 (PSR-0, PSR-1,
 * PSR-2). Les fichiers sources peuvent être en partie reformattés selon ces
 * critères en utilisant `php-cs-fixer` :
 * </p>
 *
 * <pre>
 *  sudo php-cs-fixer fix ./src
 * </pre>
 *
 * <p>
 * https://github.com/fabpot/PHP-CS-Fixer
 * </p>
 *
 * <h1>Dépendances</h1>
 * <p>
 * Ce projet utilise *composer* pour gérer les dépendances. Pour mettre à jour
 * les dépendances :
 * </p>
 *
 * <pre>
 *  sudo php ./bin/composer.phar update
 * </pre>
 *
 * <h1>Configuration de Symfony2</h1>
 * <p>
 * Symfony2 a besoin d'un répertoire pour le cache et les logs. Ces répertoires
 * nécessitent les autorisations suivantes :
 * </p>
 *
 * <pre>
 *  sudo setfacl -R -m u:www-data:rwX -m u:`whoami`:rwX app/cache app/logs
 *  sudo setfacl -dR -m u:www-data:rwx -m u:`whoami`:rwx app/cache app/logs
 * </pre>
 *
 * <p>
 * La commande setfacl est disponible dans le package `acl`.
 * </p>
 *
 * <h1>Configuration de la base de données</h1>
 * <p>
 * La connexion à la base de données doit être configurer dans
 * `./app/config/parameters.yml`.
 * </p>
 *
 * <p>
 * Pour créer la base de données :
 * </p>
 * <pre>
 *  php app/console doctrine:database:create
 * </pre>
 *
 * <p>
 * Pour créer les tables :
 * </p>
 * <pre>
 *  php app/console doctrine:schema:create
 * </pre>
 *
 * <p>
 * Pour remplir les tables avec les données par défaut :
 * </p>
 * <pre>
 *  php app/console doctrine:fixtures:load
 * </pre>
 *
 * <h1>Documentation</h1>
 *
 * <p>
 * La documentation est générée avec Sami. Pour régénérer la documentation :
 * </p>
 * <pre>
 *  sudo php ./bin/sami.php update ./app/SamiConfiguration.php
 * </pre>
 *
 * La documentation sera créée dans le dossier `./web/doc`.
 *
 * <h1>Informations diverses</h1>
 *
 * <p>
 * Pour vider le cache, deux options :
 * </p>
 *
 * <pre>
 *  php app/console cache:clear --env=prod
 * </pre>
 * <pre>
 *  sudo rm -rf ./app/cache/
 * </pre>
 *
 * <p><strong>Attention !</strong> Doctrine utilise l'extension APC pour mettre
 * en cache le schéma des entités. Ainsi, il est impératif de vider ce cache
 * après avoir modifié les entités sinon les modifications ne seront pas prises
 * en compte en mode production. Pour le vider, il suffit de redémarrer le
 * serveur :
 *
 * <pre>
 *  sudo /etc/init.d/apache2 restart
 * </pre>
 */
abstract class ReadMe
{
}
