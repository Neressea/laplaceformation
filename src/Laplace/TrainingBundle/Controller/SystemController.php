<?php

namespace Laplace\TrainingBundle\Controller;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\DBAL\DBALException;

use Laplace\CommonBundle\Controller\AbstractBaseController;

use Laplace\TrainingBundle\Entity\Domain;
use Laplace\TrainingBundle\Entity\Category;
use Laplace\TrainingBundle\Entity\Type;
use Laplace\TrainingBundle\Form\DomainType;
use Laplace\TrainingBundle\Form\CategoryType;
use Laplace\TrainingBundle\Form\TypeType;

/**
 * Contrôleur de la partie administration relative à ce bundle.
 */
class SystemController extends AbstractBaseController
{

    public function manageDomainsAction()
    {
        $mngr = $this->getDoctrine()->getManager();

        $domains = $mngr
            ->getRepository('LaplaceTrainingBundle:Domain')
            ->findBy(array(), array('code' => 'ASC'));

        $url = $this->generateUrl('laplace_training_adm_domains');

        $parameters = array();

        // Formulaire de création

        if ($this->doCreateDomainForm($parameters, 'create_form')) {
            return $this->redirect($url);
        }

        // Formulaire de suppression

        if ($this->doDeleteDomainForm($parameters, 'delete_form')) {
            return $this->redirect($url);
        }

        // Formulaires d'édition

        $parameters['edit_forms'] = array();
        foreach ($domains as $domain) {
            // données initiales du formulaire
            $form_name  = 'edit_form_' . $domain->getId();
            $data       = array(
                'domain' => $domain,
            );

            // construction du formulaire
            $builder = $this->createNamedFormBuilder($form_name, $data);
            $builder->add('domain', new DomainType());
            $builder->add('Envoyer', 'submit');

            // validation
            $form = $builder->getForm();
            if ($this->isNamedFormSubmitted($form_name, $form)) {
                if ($form->isValid()) {
                    $mngr->persist($domain);
                    $mngr->flush();

                    $this->flashInfo(
                        'Le domaine "%s" a été mis à jour.',
                        $domain->getName()
                    );

                    return $this->redirect($url);
                } else {
                    $this->flashFormError();
                }
            }

            // échec de la validation : création de la vue
            $parameters['edit_forms'][] = $form->createView();
        }

        return $this->render(
            'LaplaceTrainingBundle:System:manage-domains.html.twig',
            $parameters
        );
    }

    public function manageCategoriesAction()
    {
        $mngr = $this->getDoctrine()->getManager();

        $categories = $mngr
            ->getRepository('LaplaceTrainingBundle:Category')
            ->findBy(array(), array('code' => 'ASC'));

        $url = $this->generateUrl('laplace_training_adm_categories');

        $parameters = array();

        // Formulaire de création

        if ($this->doCreateCategoryForm($parameters, 'create_form')) {
            return $this->redirect($url);
        }

        // Formulaire de suppression

        if ($this->doDeleteCategoryForm($parameters, 'delete_form')) {
            return $this->redirect($url);
        }

        // Formulaires d'édition

        $parameters['edit_forms'] = array();
        foreach ($categories as $category) {
            // données initiales du formulaire
            $form_name  = 'edit_form_' . $category->getId();
            $data       = array(
                'category' => $category,
            );

            // construction du formulaire
            $builder = $this->createNamedFormBuilder($form_name, $data);
            $builder->add('category', new CategoryType());
            $builder->add('Envoyer', 'submit');

            // validation
            $form = $builder->getForm();
            if ($this->isNamedFormSubmitted($form_name, $form)) {
                if ($form->isValid()) {
                    $mngr->persist($category);
                    $mngr->flush();

                    $this->flashInfo(
                        'La catégorie "%s" a été mise à jour.',
                        $category->getName()
                    );

                    return $this->redirect($url);
                } else {
                    $this->flashFormError();
                }
            }

            // échec de la validation : création de la vue
            $parameters['edit_forms'][] = $form->createView();
        }

        return $this->render(
            'LaplaceTrainingBundle:System:manage-categories.html.twig',
            $parameters
        );
    }

    public function manageTypesAction()
    {
        $mngr = $this->getDoctrine()->getManager();

        $types = $mngr
            ->getRepository('LaplaceTrainingBundle:Type')
            ->findBy(array(), array('name' => 'ASC'));

        $url = $this->generateUrl('laplace_training_adm_types');

        $parameters = array();

        // Formulaire de création

        if ($this->doCreateTypeForm($parameters, 'create_form')) {
            return $this->redirect($url);
        }

        // Formulaire de suppression

        if ($this->doDeleteTypeForm($parameters, 'delete_form')) {
            return $this->redirect($url);
        }

        // Formulaire de remplacement

        if ($this->doReplaceTypeForm($parameters, 'replace_form')) {
            return $this->redirect($url);
        }

        // Formulaires d'édition

        $parameters['edit_forms'] = array();
        foreach ($types as $type) {
            // données initiales du formulaire
            $form_name  = 'edit_form_' . $type->getId();
            $data       = array(
                'type' => $type,
            );

            // construction du formulaire
            $builder = $this->createNamedFormBuilder($form_name, $data);
            $builder->add('type', new TypeType());
            $builder->add('Envoyer', 'submit');

            // validation
            $form = $builder->getForm();
            if ($this->isNamedFormSubmitted($form_name, $form)) {
                if ($form->isValid()) {
                    $mngr->persist($type);
                    $mngr->flush();

                    $this->flashInfo(
                        'Le type "%s" a été mis à jour.',
                        $type->getName()
                    );

                    return $this->redirect($url);
                } else {
                    $this->flashFormError();
                }
            }

            // échec de la validation : création de la vue
            $parameters['edit_forms'][] = $form->createView();
        }

        return $this->render(
            'LaplaceTrainingBundle:System:manage-types.html.twig',
            $parameters
        );
    }

    /**********************************************************************/
    /*                                                                    */
    /* DOMAINS                                                            */
    /*                                                                    */
    /**********************************************************************/

    private function doCreateDomainForm(&$parameters, $form_name)
    {
        $mngr = $this->getDoctrine()->getManager();

        // données initiales du formulaire
        $data = array(
            'domain' => new Domain(),
        );

        // construction du formulaire
        $builder = $this->createNamedFormBuilder($form_name, $data);
        $builder->add('domain', new DomainType());
        $builder->add('Envoyer', 'submit');

        // validation
        $form = $builder->getForm();
        if ($this->isNamedFormSubmitted($form_name, $form)) {
            if ($form->isValid()) {
                $data       = $form->getData();
                $domain     = $data['domain'];

                $mngr->persist($domain);
                $mngr->flush();

                $this->flashInfo(
                    'Le domaine "%s" a été créé.',
                    $domain->getName()
                );

                return true;
            } else {
                $this->flashFormError();
            }
        }

        // échec de la validation : création de la vue
        $parameters[$form_name] = $form->createView();

        return false;
    }

    private function doDeleteDomainForm(&$parameters, $form_name)
    {
        $mngr = $this->getDoctrine()->getManager();

        // données initiales du formulaire
        $data = array(
            'domain' => null,
        );

        // construction du formulaire
        $builder = $this->createNamedFormBuilder($form_name, $data);
        $builder->add('domain', 'entity',
            array(
                'class'             => 'LaplaceTrainingBundle:Domain',
                'query_builder'     => function ($er) {
                    return $er->createQueryBuilder('dom')->orderBy('dom.code');
                },
                'property'          => 'name',
                'constraints'       => new Assert\NotNull(),
                'label'             => 'Domaine',
            )
        );
        $builder->add('do', 'submit',
            array(
                'label' => 'Supprimer',
            )
        );

        // validation
        $form = $builder->getForm();
        if ($this->isNamedFormSubmitted($form_name, $form)) {
            if ($form->isValid()) {
                $data       = $form->getData();
                $domain     = $data['domain'];

                try {
                    $mngr->remove($domain);
                    $mngr->flush();

                    $this->flashInfo(
                        'Le domaine "%s" a été supprimé.',
                        $domain->getName()
                    );

                    return true;
                } catch (DBALException $e) {
                    $this->flashError(
                        'Impossible de supprimer le domaine "%s" ' .
                        'car il est encore utilisé.',
                        $domain->getName()
                    );
                }
            } else {
                $this->flashFormError();
            }
        }

        // échec de la validation : création de la vue
        $parameters[$form_name] = $form->createView();

        return false;
    }

    /**********************************************************************/
    /*                                                                    */
    /* CATEGORIES                                                         */
    /*                                                                    */
    /**********************************************************************/

    private function doCreateCategoryForm(&$parameters, $form_name)
    {
        $mngr = $this->getDoctrine()->getManager();

        // données initiales du formulaire
        $data = array(
            'category' =>  new Category(),
        );

        // construction du formulaire
        $builder = $this->createNamedFormBuilder($form_name, $data);
        $builder->add('category', new CategoryType());
        $builder->add('Envoyer', 'submit');

        // validation
        $form = $builder->getForm();
        if ($this->isNamedFormSubmitted($form_name, $form)) {
            if ($form->isValid()) {
                $data       = $form->getData();
                $category   = $data['category'];

                $mngr->persist($category);
                $mngr->flush();

                $this->flashInfo(
                    'La catégorie "%s" a été créée.',
                    $category->getName()
                );

                return true;
            } else {
                $this->flashFormError();
            }
        }

        // échec de la validation : création de la vue
        $parameters[$form_name] = $form->createView();

        return false;
    }

    private function doDeleteCategoryForm(&$parameters, $form_name)
    {
        $mngr = $this->getDoctrine()->getManager();

        // données initiales du formulaire
        $data = array(
            'category' => null,
        );

        // construction du formulaire
        $builder = $this->createNamedFormBuilder($form_name, $data);
        $builder->add('category', 'entity',
            array(
                'class'             => 'LaplaceTrainingBundle:Category',
                'query_builder'     => function ($er) {
                        return $er
                            ->createQueryBuilder('cat')
                            ->orderBy('cat.code');
                },
                'property'          => 'name',
                'group_by'          => 'domain.name',
                'constraints'       => new Assert\NotNull(),
                'label'             => 'Catégorie',
            )
        );
        $builder->add('do', 'submit',
            array(
                'label' => 'Supprimer',
            )
        );

        // validation
        $form = $builder->getForm();
        if ($this->isNamedFormSubmitted($form_name, $form)) {
            if ($form->isValid()) {
                $data       = $form->getData();
                $category   = $data['category'];

                try {
                    $mngr->remove($category);
                    $mngr->flush();

                    $this->flashInfo(
                        'La catégorie "%s" a été supprimée.',
                        $category->getName()
                    );

                    return true;
                } catch (DBALException $e) {
                    $this->flashError(
                        'Impossible de supprimer la catégorie "%s" ' .
                        'car elle est encore utilisée.',
                        $category->getName()
                    );
                }
            } else {
                $this->flashFormError();
            }
        }

        // échec de la validation : création de la vue
        $parameters[$form_name] = $form->createView();

        return false;
    }

    /**********************************************************************/
    /*                                                                    */
    /* TYPES                                                              */
    /*                                                                    */
    /**********************************************************************/

    private function doCreateTypeForm(&$parameters, $form_name)
    {
        $mngr = $this->getDoctrine()->getManager();

        // données initiales du formulaire
        $data = array(
            'type' => new Type(),
        );

        // construction du formulaire
        $builder = $this->createNamedFormBuilder($form_name, $data);
        $builder->add('type', new TypeType());
        $builder->add('Envoyer', 'submit');

        // validation
        $form = $builder->getForm();
        if ($this->isNamedFormSubmitted($form_name, $form)) {
            if ($form->isValid()) {
                $data       = $form->getData();
                $type       = $data['type'];

                $mngr->persist($type);
                $mngr->flush();

                $this->flashInfo(
                    'Le type "%s" a été créé.',
                    $type->getNameWithDescription()
                );

                return true;
            } else {
                $this->flashFormError();
            }
        }

        // échec de la validation : création de la vue
        $parameters[$form_name] = $form->createView();

        return false;
    }

    private function doDeleteTypeForm(&$parameters, $form_name)
    {
        $mngr = $this->getDoctrine()->getManager();

        // données initiales du formulaire
        $data = array(
            'type' => null,
        );

        // construction du formulaire
        $builder = $this->createNamedFormBuilder($form_name, $data);
        $builder->add('type', 'entity',
            array(
                'class'             => 'LaplaceTrainingBundle:Type',
                'query_builder'     => function ($er) {
                    return $er
                            ->createQueryBuilder('type')
                            ->orderBy('type.name');
                },
                'property'          => 'nameWithDescription',
                'constraints'       => new Assert\NotNull(),
                'label'             => 'Type',
            )
        );
        $builder->add('do', 'submit',
            array(
                'label' => 'Supprimer',
            )
        );

        // validation
        $form = $builder->getForm();
        if ($this->isNamedFormSubmitted($form_name, $form)) {
            if ($form->isValid()) {
                $data       = $form->getData();
                $type       = $data['type'];

                try {
                    $mngr->remove($type);
                    $mngr->flush();

                    $this->flashInfo(
                        'Le type "%s" a été supprimé.',
                        $type->getNameWithDescription()
                    );

                    return true;
                } catch (DBALException $e) {
                    $this->flashError(
                        'Impossible de supprimer le type "%s" ' .
                        'car il est encore utilisé.',
                        $type->getNameWithDescription()
                    );
                }
            } else {
                $this->flashFormError();
            }
        }

        // échec de la validation : création de la vue
        $parameters[$form_name] = $form->createView();

        return false;
    }

    private function doReplaceTypeForm(&$parameters, $form_name)
    {
        $mngr = $this->getDoctrine()->getManager();

        // données initiales du formulaire
        $data = array(
            'from'  => null,
            'to'    => null,
        );

        // construction du formulaire
        $builder = $this->createNamedFormBuilder($form_name, $data);
        $builder->add('from', 'entity',
            array(
                'class'             => 'LaplaceTrainingBundle:Type',
                'query_builder'     => function ($er) {
                    return $er
                            ->createQueryBuilder('type')
                            ->orderBy('type.name');
                },
                'property'          => 'nameWithDescription',
                'constraints'       => new Assert\NotNull(),
                'label'             => 'Remplacer',
            )
        );
        $builder->add('to', 'entity',
            array(
                'class'             => 'LaplaceTrainingBundle:Type',
                'query_builder'     => function ($er) {
                    return $er
                            ->createQueryBuilder('type')
                            ->orderBy('type.name');
                },
                'property'          => 'nameWithDescription',
                'constraints'       => new Assert\NotNull(),
                'label'             => 'Par',
            )
        );
        $builder->add('do', 'submit',
            array(
                'label' => 'Remplacer',
            )
        );

        // validation
        $form = $builder->getForm();
        if ($this->isNamedFormSubmitted($form_name, $form)) {
            if ($form->isValid()) {
                $data       = $form->getData();
                $from       = $data['from'];
                $to         = $data['to'];

                try {
                    $rows = $mngr
                        ->createQueryBuilder()
                        ->update(
                            'LaplaceTrainingBundle:NeedSubscription',
                            'need'
                        )
                        ->set('need.type', ':to')
                        ->where('need.type = :from')
                        ->setParameter('from', $from)
                        ->setParameter('to', $to)
                        ->getQuery()
                        ->execute();
                    $rows += $mngr
                        ->createQueryBuilder()
                        ->update(
                            'LaplaceTrainingBundle:RequestSubscription',
                            'request'
                        )
                        ->set('request.type', ':to')
                        ->where('request.type = :from')
                        ->setParameter('from', $from)
                        ->setParameter('to', $to)
                        ->getQuery()
                        ->execute();

                    $this->flashInfo(
                        'Le type "%s" a été remplacé par "%s"' .
                        ' [%d ligne(s) affectée(s)].',
                        $from->getNameWithDescription(),
                        $to->getNameWithDescription(),
                        $rows
                    );

                    return true;
                } catch (DBALException $e) {
                    $this->flashError('Erreur lors du remplacement.');
                }
            } else {
                $this->flashFormError();
            }
        }

        // échec de la validation : création de la vue
        $parameters[$form_name] = $form->createView();

        return false;
    }
}
