<?php

namespace Laplace\TrainingBundle\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;

use Laplace\CommonBundle\Controller\AbstractBaseController;
use Laplace\UserBundle\Entity\User;
use Laplace\UserBundle\Form\UserSelectorType;
use Laplace\TrainingBundle\Entity\Request;
use Laplace\TrainingBundle\Entity\Thread;
use Laplace\TrainingBundle\Entity\Message;
use Laplace\TrainingBundle\Form\ThreadType;
use Laplace\TrainingBundle\Form\MessageType;

/**
 * Cette classe abstraite permet de regrouper le code commun entre les besoins
 * et les demandes concernant l'affichage des fils de discussion associés à
 * une page et leur création.
 */
abstract class AbstractThreadAwareController extends AbstractBaseController
{

    /**
     * Appelle la méthode `ThreadRepository#fetchByParticipantAndReference()`.
     *
     * @param  User|null         $participant
     * @param  Need|Request|null $reference
     * @return ThreadInfo[]
     */
    protected function getThreadList($participant, $reference)
    {
        return $this->getDoctrine()->getManager()
            ->getRepository('LaplaceTrainingBundle:Thread')
            ->fetchByParticipantAndReference($participant, $reference);
    }

    /**
     * Construit et valide un formulaire de création d'un fil de discussion.
     * Si un fil de discussion est créé, une alerte et un email sont envoyés.
     * Le formulaire créé est sauvegardé dans `$parameters` (passé par
     * référence) avec la clef `$form_name`.
     * `$form_name` est également utilisé comme nom pour le formulaire.
     * `$parameters` doit contenir une valeur pour la clef `admin`.
     *
     * @param  array                 $parameters
     * @param  string                $form_name
     * @return RedirectResponse|null Retourne un objet RedirectResponse si le
     * fil a été créé, null sinon
     */
    protected function createThreadView(&$parameters, $form_name)
    {
        $admin = $parameters['admin'];

        // traitement du formulaire de création d'un nouveau
        // fil de discussion (commun utilisateurs / admins)
        $thread = $this->doCreateThreadForm($parameters, $form_name);
        if ($thread == null) {
            return null;
        }

        $user_url = $this->generateUrl(
            'laplace_training_view_thread',
            array(
                'id' => $thread->getId(),
            ),
            true
        );
        $admin_url = $this->generateUrl(
            'laplace_training_adm_view_thread',
            array(
                'id' =>
                    $thread->getId(),
            ),
            true
        );
        $url = $admin ? $admin_url : $user_url;

        // notification système
        $this->get('laplace.alert')->information(
            $admin_url,
            'Une nouvelle discussion, intitulée "%s" a été créée par %s.',
            $thread->getTitle(),
            $this->getUser()->getFullName()
        );

        // email
        $this->get('laplace.mailer')->send(
            // from
            $this->getUser(),
            // to
            $admin ? $thread->getParticipants()->toArray() : 'CORRESP_FORM',
            // subject
            'Nouvelle discussion',
            // body
            $this->renderView(
                'LaplaceTrainingBundle:Mail:new-thread.html.twig',
                array(
                    'thread' => $thread,
                    // ATTENTION aux URL entre admin et user !
                    'url' => $admin ? $user_url : $admin_url
                )
            )
        );

        // redirection
        return $this->redirect($url);
    }

    /**
     * Traite la création et la validation du formulaire.
     *
     * @param  array       $parameters Un tableau de paramètres
     * @param  string      $form_name  Le nom du formulaire
     * @return Thread|null Le fil créé ou null
     */
    private function doCreateThreadForm(&$parameters, $form_name)
    {
        $mngr           = $this->getDoctrine()->getManager();
        $user           = $this->getUser();

        $admin          = $parameters['admin'];
        $need           = isset($parameters['need']) ?
            $parameters['need'] : null;
        $request        = isset($parameters['request']) ?
            $parameters['request'] : null;

        // données initiales du formulaire
        $data = array(
            'thread'        => new Thread($user),
            'participant'   => null,
            'message'       => new Message(),
        );

        // construction du formulaire
        $builder = $this->createNamedFormBuilder($form_name, $data);
        $builder->add('thread', new ThreadType());
        $builder->add('message', new MessageType());

        if ($admin) {
            $builder->add('participant', new UserSelectorType(),
                array(
                    'em'        => $mngr,
                    'label'     => 'Participant',
                    'required'  => false,
                )
            );
        }

        $builder->add('Envoyer', 'submit');

        // validation
        $form = $builder->getForm();
        if ($this->isNamedFormSubmitted($form_name, $form)) {
            if ($form->isValid()) {
                $data = $form->getData();

                $thread = $data['thread'];

                if ($need != null) {
                    $thread->setNeed($need);
                }

                if ($request != null) {
                    $thread->setRequest($request);
                }

                if ($admin) {
                    $participant = $data['participant'];
                    if ($participant != null) {
                        $thread->addParticipant($participant);
                    }
                } else {
                    $thread->addParticipant($user);
                }

                $message = $data['message'];
                $message->setThread($thread);
                $message->setAuthor($user);

                $mngr->persist($thread);
                $mngr->persist($message);
                $mngr->flush();

                $this->flashInfo('La discussion a été créée.');

                return $thread;
            } else {
                $this->flashFormError();
            }
        }

        // échec de la validation : création de la vue
        $parameters[$form_name] = $form->createView();

        return null;
    }

}
