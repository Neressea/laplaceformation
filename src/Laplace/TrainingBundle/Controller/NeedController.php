<?php

namespace Laplace\TrainingBundle\Controller;

use Doctrine\DBAL\DBALException;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Validator\Constraints as Assert;

use Laplace\TrainingBundle\Entity\Need;
use Laplace\TrainingBundle\Entity\NeedSubscription;
use Laplace\TrainingBundle\Entity\Event;

use Laplace\TrainingBundle\Form\NeedType;
use Laplace\TrainingBundle\Form\TypeSelectorType;

/**
 * Contrôleur de la partie "Besoins".
 */
class NeedController extends AbstractThreadAwareController
{

    /**
     * Affiche la page "Créer un nouveau besoin".
     *
     * @param  boolean  $admin
     * @return Response
     */
    public function createAction($admin)
    {
        $mngr = $this->getDoctrine()->getManager();

        // données initiales du formulaire
        $data = array(
            'need' => new Need(),
            'type' => null,
        );

        // construction du formulaire
        $builder = $this->createNamedFormBuilder('create_need', $data);
        $builder->add('need', new NeedType());

        if (!$admin) {
            $builder->add('type', new TypeSelectorType());
        }

        $builder->add('Envoyer', 'submit');

        // validation
        $form = $builder->getForm();
        if ($this->isNamedFormSubmitted('create_need', $form)) {
            if ($form->isValid()) {
                $data   = $form->getData();
                $user   = $this->getUser();

                // besoin
                $need = $data['need'];
                $need->setAuthor($this->getUser());
                $mngr->persist($need);
                $mngr->flush();

                // inscription
                if (!$admin) {
                    $subscription = new NeedSubscription();
                    $subscription->setNeed($need);
                    $subscription->setUser($this->getUser());
                    $subscription->setType($data['type']);
                    $mngr->persist($subscription);

                    // création d'un événement
                    $event = Event::create(Event::NEED_ADDED, $user, $need);
                    $mngr->persist($event);

                    // flush
                    $mngr->flush();
                }

                $this->flashInfo(
                    $admin ?
                        'Le besoin a été créé.' :
                        'Le besoin a été créé et ajouté à votre liste.'
                );

                $user_url = $this->generateUrl(
                    'laplace_training_view_need',
                    array(
                        'id' => $need->getId(),
                    )
                );
                $admin_url = $this->generateUrl(
                    'laplace_training_adm_view_need',
                    array(
                        'id' => $need->getId(),
                    )
                );
                $url = $admin ? $admin_url : $user_url;

                // notification
                $this->get('laplace.alert')->information(
                    $admin_url,
                    'Un nouveau besoin, intitulé "%s" ' .
                    'a été créé par %s.',
                    $need->getTitle(),
                    $this->getUser()->getFullName()
                );

                return $this->redirect($url);
            } else {
                $this->flashFormError();
            }
        }

        // échec de la validation : création de la vue
        return $this->render(
            'LaplaceTrainingBundle:Need:create.html.twig',
            array(
                'form' =>   $form->createView(),
                'admin' =>  $admin,
            )
        );
    }

    /**
     * Affiche la page "Voir un besoin".
     *
     * @param  type                                $admin
     * @param  \Laplace\TrainingBundle\Entity\Need $need
     * @return Response
     */
    public function viewAction($admin, Need $need = null)
    {
        $this->check($need);

        $mngr = $this->getDoctrine()->getManager();

        $parameters = array(
            'need' =>   $need,
            'admin' =>  $admin,
        );

        $thread_redirect = $this->createThreadView($parameters, 'thread_form');
        if ($thread_redirect != null) {
            return $thread_redirect;
        }

        //
        // Lorsqu'un utilisateur consulte la page, on doit lui afficher l'espace
        // échange avec les correspondants formation et lui proposer de gérer
        // son inscription à ce besoin (ajout, retrait, édition, etc.)
        //
        // Lorsqu'un administrateur consulte la page, il faut lui afficher
        // l'ensemble des discussions
        //

        $do_forms = array();

        if (!$admin) {
            $user = $this->getUser();
            $subscription = $mngr
                ->getRepository('LaplaceTrainingBundle:NeedSubscription')
                ->fetchOneByUserAndNeed($user, $need);

            $parameters['subscription'] = $subscription;

            // on affiche un formulaire différent si l'utilisateur a déjà
            // ajouté le besoin
            if ($subscription) {
                // il l'a déjà ajouté, on lui permet d'indiquer que ce besoin
                // est pourvu, de modifier le type ou de le retirer

                $do_forms[] = 'satisfied';
                $do_forms[] = 'edit';
                $do_forms[] = 'unsubscribe';
            } else {
                // il ne l'a pas déjà ajouté, on lui permet de le faire

                $do_forms[] = 'subscribe';
            }

            // récupération des discussions
            $parameters['threads'] = $mngr
                ->getRepository('LaplaceTrainingBundle:Thread')
                ->fetchByParticipantAndReference($user, $need);
        } else {
            // $admin is true
            $parameters['subscriptions'] = $mngr
                ->getRepository('LaplaceTrainingBundle:NeedSubscription')
                ->fetchByNeed($need);

            // récupération des discussions
            $parameters['threads'] = $mngr
                ->getRepository('LaplaceTrainingBundle:Thread')
                ->fetchByParticipantAndReference(null, $need);
        }

        $user_url = $this->generateUrl(
            'laplace_training_view_need',
            array(
                'id' => $need->getId(),
            )
        );
        $admin_url = $this->generateUrl(
            'laplace_training_adm_view_need',
            array(
                'id' => $need->getId(),
            )
        );
        $url = $admin ? $admin_url : $user_url;

        // Formulaire d'inscription
        if (in_array('subscribe', $do_forms)) {
            if ($this->doSubscribeForm($parameters, 'subscribe_form')) {
                $this->get('laplace.alert')->notice(
                    $admin_url,
                    '%s a ajouté le besoin "%s".',
                    $this->getUser()->getFullName(),
                    $need->getTitle()
                );

                return $this->redirect($url);
            }
        }

        // Formulaire d'édition d'une inscription
        if (in_array('edit', $do_forms)) {
            if ($this->doEditSubscriptionForm($parameters, 'edit_form')) {
                return $this->redirect($url);
            }
        }

        // Formulaire de retrait d'une inscription
        if (in_array('unsubscribe', $do_forms)) {
            if ($this->doUnsubscribeForm($parameters, 'unsubscribe_form')) {
                $this->get('laplace.alert')->notice(
                    $admin_url,
                    '%s a retiré le besoin "%s".',
                    $this->getUser()->getFullName(),
                    $need->getTitle()
                );

                return $this->redirect($url);
            }
        }

        // Formulaire pour indiquer que le besoin est pourvu
        if (in_array('satisfied', $do_forms)) {
            if ($this->doSatisfiedForm($parameters, 'satisfied_form')) {
                $this->get('laplace.alert')->notice(
                    $admin_url,
                    '%s a indiqué que le besoin "%s" était pourvu.',
                    $this->getUser()->getFullName(),
                    $need->getTitle()
                );

                return $this->redirect($url);
            }
        }

        return $this->render(
            'LaplaceTrainingBundle:Need:view.html.twig',
            $parameters
        );
    }

    /**
     * Affiche la page "Tous les besoins".
     *
     * @param  boolean  $admin
     * @return Response
     */
    public function viewAllAction($admin)
    {
        $mngr = $this->getDoctrine()->getManager();

        $domains = $mngr
            ->getRepository('LaplaceTrainingBundle:Domain')
            ->fetchAll();

        $needs = $mngr
            ->getRepository('LaplaceTrainingBundle:Need')
            ->fetchAll();

        // regrouper les besoins par catégories et domaines
        $groups = array(
            null => array(),
        );
        foreach ($needs as $need_info) {
            $cat = $need_info->getNeed()->getCategory();

            if ($cat == null) {
                $groups[null][] = $need_info;
            } elseif (isset($groups[$cat->getId()])) {
                $groups[$cat->getId()][] = $need_info;
            } else {
                $groups[$cat->getId()] = array($need_info);
            }
        }

        return $this->render(
            'LaplaceTrainingBundle:Need:view-all.html.twig',
            array(
                'domains'   => $domains,
                'groups'    => $groups,
                'admin'     => $admin,
            )
        );
    }

    /**
     * Affiche la page "Besoins personnels".
     *
     * @return Response
     */
    public function viewActiveSubscriptionsAction()
    {
        $mngr = $this->getDoctrine()->getManager();

        $subscriptions = $mngr
            ->getRepository('LaplaceTrainingBundle:NeedSubscription')
            ->fetchByUser($this->getUser());

        return $this->render(
            'LaplaceTrainingBundle:Need:view-active-subscriptions.html.twig',
            array(
                'subscriptions' => $subscriptions,
            )
        );
    }

    /**
     * Affiche la page "Modifier un besoin".
     *
     * @return Response
     */
    public function editAction($admin, Need $need = null)
    {
        $this->check($need);

        if (!$admin && $this->getUser() != $need->getAuthor()) {
            throw new AccessDeniedHttpException(
                'Vous ne pouvez pas modifier ce besoin.'
            );
        }

        $mngr = $this->getDoctrine()->getManager();

        $parameters = array(
            'admin' => $admin,
            'need'  => $need,
        );

        $user_url = $this->generateUrl(
            'laplace_training_view_need',
            array(
                'id' => $need->getId(),
            )
        );
        $admin_url = $this->generateUrl(
            'laplace_training_adm_view_need',
            array(
                'id' => $need->getId(),
            )
        );
        $url = $admin ? $admin_url : $user_url;

        //
        // Formulaire d'édition
        //

        // données initiales du formulaire
        $form_name  = 'edit_form';
        $data       = array(
            'need' => $need,
        );

        // construction du formulaire
        $builder = $this->createNamedFormBuilder($form_name, $data);
        $builder->add('need', new NeedType());
        $builder->add('Envoyer', 'submit');

        // validation
        $form = $builder->getForm();
        if ($this->isNamedFormSubmitted($form_name, $form)) {
            if ($form->isValid()) {
                $need->setAuthor($this->getUser());
                $mngr->persist($need);
                $mngr->flush();

                $this->flashInfo('Le besoin a été mis à jour.');

                return $this->redirect($url);
            } else {
                $this->flashFormError();
            }
        }

        // échec de la validation : création de la vue
        $parameters[$form_name] = $form->createView();

        return $this->render(
            'LaplaceTrainingBundle:Need:edit.html.twig',
            $parameters
        );
    }

    /**
     * Affiche la page "Supprimer un besoin" (administrateur seulement).
     *
     * @return Response
     */
    public function deleteAction(Need $need = null)
    {
        $this->check($need);

        $mngr   = $this->getDoctrine()->getManager();
        $url    = $this->generateUrl('laplace_training_adm_all_needs');

        $parameters = array(
            'need' => $need,
        );

        //
        // Formulaire de suppression
        //

        // données initiales du formulaire
        $form_name  = 'delete_form';
        $data       = array(
            'delete' => false
        );

        // construction du formulaire
        $builder = $this->createNamedFormBuilder($form_name, $data);
        $builder->add('delete', 'checkbox',
            array(
                'constraints'   => new Assert\True(),
                'label'         =>
                    'Supprimer ce besoin et tout ce qui lui est rattaché'
            )
        );
        $builder->add('do', 'submit', array('label' => 'Supprimer'));

        // validation
        $form = $builder->getForm();
        if ($this->isNamedFormSubmitted($form_name, $form)) {
            if ($form->isValid()) {
                try {
                    $mngr->getRepository('LaplaceTrainingBundle:Need')
                        ->deleteNeeds(array($need));

                    $this->flashInfo(
                        'Le besoin "%s" a été supprimé.',
                        $need->getTitle()
                    );

                    return $this->redirect($url);
                } catch (\Exception $e) {
                    if ($e instanceOf DBALException) {
                        $this->flashError(
                            'Erreur lors de la suppression du besoin "%s" '.
                            '[%s]',
                            $need->getTitle(),
                            $e->getMessage()
                        );
                    } else {
                        throw $e;
                    }
                }
            } else {
                $this->flashFormError();
            }
        }

        // échec de la validation : création de la vue
        $parameters[$form_name] = $form->createView();

        return $this->render(
            'LaplaceTrainingBundle:Need:delete.html.twig',
            $parameters
        );
    }

/******************************************************************************/

    /**
     * Lève une exception si le besoin n'existe pas, c'est-à-dire si l'objet
     * passé est null.
     *
     * @param  Need                  $need
     * @throws NotFoundHttpException
     */
    private function check(Need $need = null)
    {
         if ($need == null) {
            throw $this->createNotFoundException('Ce besoin n\'existe pas.');
         }
    }

    /**
     * Formulaire d'inscription à un besoin.
     *
     * @param  array   $parameters
     * @param  string  $form_name
     * @return boolean true si l'inscription a réussi, false sinon
     */
    private function doSubscribeForm(&$parameters, $form_name)
    {
        $mngr = $this->getDoctrine()->getManager();

        $need = $parameters['need'];

        // données initiales du formulaire
        $data = array(
            'type' => null,
        );

        // création du formulaire
        $builder = $this->createNamedFormBuilder($form_name, $data);
        $builder->add('type', new TypeSelectorType());
        $builder->add('Envoyer', 'submit');

        // validation
        $form = $builder->getForm();
        if ($this->isNamedFormSubmitted($form_name, $form)) {
            if ($form->isValid()) {
                $data   = $form->getData();
                $user   = $this->getUser();

                $subscription = new NeedSubscription();
                $subscription->setNeed($need);
                $subscription->setUser($user);
                $subscription->setType($data['type']);

                $event = Event::create(Event::NEED_ADDED, $user, $need);

                $mngr->persist($subscription);
                $mngr->persist($event);
                $mngr->flush();

                $this->flashInfo('Votre inscription a été enregistrée.');

                return true;
            } else {
                $this->flashFormError();
            }
        }

        // échec de la validation : création de la vue
        $parameters[$form_name] = $form->createView();

        return false;
    }

    /**
     * Formulaire permettant d'indiquer que le besoin est pourvu.
     *
     * @param  array   $parameters
     * @param  string  $form_name
     * @return boolean true si le formulaire a été validé, false sinon
     */
    private function doSatisfiedForm(&$parameters, $form_name)
    {
        $mngr           = $this->getDoctrine()->getManager();

        $need           = $parameters['need'];
        $subscription   = $parameters['subscription'];

        // données initiales du formulaire
        $data = array(
            'satisfied' => false,
        );

        // création du formulaire
        $builder = $this->createNamedFormBuilder($form_name, $data);
        $builder->add('satisfied', 'checkbox',
            array(
                'label'         => 'Ce besoin est pourvu',
                'constraints'   => new Assert\True(),
            )
        );
        $builder->add('Envoyer', 'submit');

        // validation
        $form = $builder->getForm();
        if ($this->isNamedFormSubmitted($form_name, $form)) {
            if ($form->isValid()) {
                $data   = $form->getData();
                $user   = $this->getUser();

                if ($data['satisfied']) {
                    $mngr->remove($subscription);

                    $event = Event::create(Event::NEED_SATISFIED, $user, $need);
                    $mngr->persist($event);
                }

                $mngr->flush();

                $this->flashInfo('Votre inscription a été mise à jour.');

                return true;
            } else {
                $this->flashFormError();
            }
        }

        // échec de la validation : création de la vue
        $parameters[$form_name] = $form->createView();

        return false;
    }

    /**
     * Formulaire de modification d'une inscription.
     *
     * @param  array   $parameters
     * @param  string  $form_name
     * @return boolean true si l'inscription a été modifiée, false sinon
     */
    private function doEditSubscriptionForm(&$parameters, $form_name)
    {
        $mngr = $this->getDoctrine()->getManager();

        $subscription = $parameters['subscription'];

        // données initiales du formulaire
        $data = array(
            'type' => $subscription->getType(),
        );

        // création du formulaire
        $builder = $this->createNamedFormBuilder($form_name, $data);
        $builder->add('type', new TypeSelectorType());
        $builder->add('Envoyer', 'submit');

        // validation
        $form = $builder->getForm();
        if ($this->isNamedFormSubmitted($form_name, $form)) {
            if ($form->isValid()) {
                $data = $form->getData();

                $subscription->setType($data['type']);
                $subscription->setSubscriptionDate(new \DateTime());

                $mngr->merge($subscription);
                $mngr->flush();

                $this->flashInfo('Votre inscription a été mise à jour.');

                return true;
            } else {
                $this->flashFormError();
            }
        }

        // échec de la validation : création de la vue
        $parameters[$form_name] = $form->createView();

        return false;
    }

    /**
     * Formulaire de désincription d'un besoin.
     *
     * @param  array   $parameters
     * @param  string  $form_name
     * @return boolean true si l'utilisateur a été désinscrit, false sinon
     */
    private function doUnsubscribeForm(&$parameters, $form_name)
    {
        $mngr = $this->getDoctrine()->getManager();

        $need           = $parameters['need'];
        $subscription   = $parameters['subscription'];

        // données initiales du formulaires
        $data = array(
            'unsubscribe' => false,
        );

        // construction du formulaire
        $builder = $this->createNamedFormBuilder($form_name, $data);
        $builder->add('unsubscribe', 'checkbox',
            array(
                'label' =>  'Retirer ce besoin',
                'constraints' => new Assert\True(),
            )
        );
        $builder->add('do', 'submit',
            array(
                'label' => 'Retirer',
            )
        );

        // validation
        $form = $builder->getForm();
        if ($this->isNamedFormSubmitted($form_name, $form)) {
            if ($form->isValid()) {
                $event_repo = $mngr
                    ->getRepository('LaplaceTrainingBundle:Event');

                $events = $event_repo->fetchBy(
                    array(
                        'user'          => $this->getUser(),
                        'mask'          => Event::MASK_NEED,
                        'reference'     => $need,
                    )
                );

                $event_repo->deleteEvents($events);

                $mngr->remove($subscription);
                $mngr->flush();

                $this->flashInfo('Votre inscription a été retirée.');

                return true;
            } else {
                $this->flashFormError();
            }

        }

        // échec de la validation : création de la vue
        $parameters[$form_name] = $form->createView();

        return false;
    }

}
