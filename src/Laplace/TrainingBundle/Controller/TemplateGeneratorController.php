<?php

namespace Laplace\TrainingBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Laplace\CommonBundle\Controller\AbstractBaseController;
use Laplace\UserBundle\Entity\User;

use FPDI;
use TCPDF;
use Laplace\TrainingBundle\Controller\DataDictionary;

/**
 * Contrôle la génération des formulaires.
 * 
 * @author albert
 */

class TemplateGeneratorController extends AbstractBaseController{
    
    private $path = '../templates/';
    
    public function downloadAction($admin, $id, $template_name, Request $request){
        
        $mngr = $this->getDoctrine()->getManager();
        $training = $mngr->getRepository('LaplaceTrainingBundle:Request')->findOneById($id);
        
        //On récupère l'utilisateur faisant la demande
        $user = $this->getUser();
        
        //On crée le dictionnaire
        $dictionary = new DataDictionary($user, $training, $mngr);
        
        //On commence par récupérer la configuration concerné.
        $template = $this->getConfig($template_name);
        
        //On charge le PDF recherché.
        $pdf = $this->load($template_name);
        
        //On récupère les diemnsions en 'points' (unité des PDFs)
        $dim_points = $pdf->getPageDimensions();
        
        //On parcourt toutes les pages du PDF
        for($i = 0; $i < $template->nb_pages; $i++){
            
            //On charge la page
            $pdf = $this->page($pdf, $i+1);
            
            //On ajoute les champs de texte sur les PDFs.
            $this->fillTexts($pdf, $template, $i, $dim_points);

            //On ajoute les données.
            $this->fillData($pdf, $template, $i, $dictionary, $dim_points);
            
        }
        
        //On renvoie le PDF complété.
        $pdf->Output();
    }
    
    private function fillData($pdf, $template, $num_page, DataDictionary $dictionary, $dim_points){
        
        //On calcule le nomre de points de l'unité PDF
        $width_points = $dim_points['wk'];
        $height_points = $dim_points['hk'];
        
        //On parcourt toutes les données du template
        for($i = 0; $i < count($template->data); $i++){
            
            //Si la donnée courante apparait sur la page courante, alors on l'ajoute
            if($template->data[$i]->pos->page == $num_page){
                
                //On convertit les proportions du JSON en points PDF.
                $pdf->SetX($template->data[$i]->pos->x * $width_points, true);
                $pdf->SetY($template->data[$i]->pos->y * $height_points + 1, false, true);
                
                $pdf->Cell($template->data[$i]->width * $width_points, 0, $dictionary->get($template->data[$i]->name), 0, 0, 'L', false, '', 1);
            }
            
        }
        
    }
    
    private function fillTexts($pdf, $template, $num_page, $dim_points){
        
        //On calcule le nomre de points de l'unité PDF
        $width_points = $dim_points['wk'];
        $height_points = $dim_points['hk'];
        
        //On parcourt tous les textes du template
        for($i = 0; $i < count($template->texts); $i++){
            
            //Si le texte courant apparait sur la page courante, alors on l'ajoute
            if($template->texts[$i]->pos->page == $num_page){
                
                //On convertit les proportions du JSON en points PDF.
                $pdf->SetX($template->texts[$i]->pos->x * $width_points, true);
                $pdf->SetY($template->texts[$i]->pos->y * $height_points + 5, false, true);
                
                $pdf->Write(0, $template->texts[$i]->value);
            }
            
        }
    }
    
    private function load($template_name){
        $pdf = new FPDI();
        $pdf->setSourceFile( $this->path . $template_name . '.pdf');
        return $pdf;
    }
    
    private function page($pdf, $num){
        $pdf->addPage();
        $tplIdx = $pdf->importPage($num);
        $pdf->useTemplate($tplIdx);
        $pdf->SetFont('courier', '', 10);
        $pdf->SetTextColor(0);
        return $pdf;
    }
    
    /**
     * Cherche dans le fichier de configuration le template concerné et 
     * le renvoie en objet PHP.
     * 
     * @param type $template_name
     */
    private function getConfig($template_name){
        
        //On commence par charger le fichier.
        $file = fopen("../laplace_config.json", "r") or die("Unable to open file!");
        $read = fread($file,filesize("../laplace_config.json"));
        fclose($file);

        //On transforme le json en tableau associatif
        $config = json_decode($read);
        
        //On cherche l'objet concernant le template
        $found = false;
        $i = 0;
        
        while($i < count($config->templates) && $found == false){
            
            if($config->templates[$i]->name == $template_name)
                $found = $config->templates[$i];
                    
            $i++;
        }
        
        return $found;
    }
    
}