<?php

namespace Laplace\TrainingBundle\Controller;

use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Validator\Constraints as Assert;

use Doctrine\DBAL\DBALException;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use Laplace\UserBundle\Entity\User;
use Laplace\TrainingBundle\Entity\Request;
use Laplace\TrainingBundle\Entity\RequestSubscription;
use Laplace\TrainingBundle\Entity\Event;
use Laplace\TrainingBundle\Form\RequestType;
use Laplace\TrainingBundle\Form\RequestSubscriptionType;
use Laplace\TrainingBundle\Form\TypeSelectorType;
use Laplace\TrainingBundle\Model\SubscriptionInfo;

/**
 * Contrôleur de la partie "Demandes".
 */
class RequestController extends AbstractThreadAwareController
{
    /**
     * Page "Création d'une nouvelle demande"
     *
     * @param boolean $admin
     */
    public function createAction($admin)
    {
        $mngr = $this->getDoctrine()->getManager();

        // données initiales du formulaire
        $data = array(
            'request'   => new Request(),
            'type'      => null,
        );

        // construction du formulaire
        $builder = $this->createNamedFormBuilder('create_request', $data);
        $builder->add('request', new RequestType());

        if (!$admin) {
            $builder->add('type', new TypeSelectorType());
        }

        $builder->add('Envoyer', 'submit');

        // validation
        $form = $builder->getForm();
        if ($this->isNamedFormSubmitted('create_request', $form)) {
            if ($form->isValid()) {
                $data   = $form->getData();
                $user   = $this->getUser();

                // demande
                $request = $data['request'];
                $request->setAuthor($this->getUser());
                $request->setValidated($admin);
                $mngr->persist($request);
                $mngr->flush();

                // inscription
                if (!$admin) {
                    $subscription = new RequestSubscription();
                    $subscription->setRequest($request);
                    $subscription->setUser($this->getUser());
                    $subscription->setType($data['type']);
                    $mngr->persist($subscription);

                    // création d'un événement
                    $event = Event::create(Event::REQUEST_ADDED, $user,
                        $request
                    );
                    $mngr->persist($event);

                    // flush
                    $mngr->flush();
                }

                $this->flashInfo(
                    $admin ?
                        'La demande a été créée.' :
                        'La demande a été créée et ajoutée à votre liste.'
                );

                $user_url = $this->generateUrl(
                    'laplace_training_view_request',
                    array(
                        'id' => $request->getId(),
                    ),
                    true
                );
                $admin_url = $this->generateUrl(
                    'laplace_training_adm_view_request',
                    array(
                        'id' => $request->getId(),
                    ),
                    true
                );

                // notification
                $this->get('laplace.alert')->important(
                    $admin_url,
                    'Une nouvelle demande, intitulée "%s" ' .
                    'a été créée par %s.',
                    $request->getTitle(),
                    $this->getUser()->getFullName()
                );

                if ($admin) {
                    return $this->redirect($admin_url);
                }

                // email
                $this->get('laplace.mailer')->send(
                    $this->getUser(),
                    'CORRESP_FORM',
                    'Nouvelle demande',
                    $this->renderView(
                        'LaplaceTrainingBundle:Mail:new-request.html.twig',
                        array(
                            'request'   => $request,
                            'url'       => $admin_url
                        )
                    )
                );

                return $this->redirect($user_url);
            } else {
                $this->flashFormError();
            }
        }

        // échec de la validation : création de la vue
        return $this->render(
            'LaplaceTrainingBundle:Request:create.html.twig',
            array(
                'form'      => $form->createView(),
                'admin'     => $admin
            )
        );
    }
    
    /**
     * Affiche la page de création d'une nouvelle demande à partir d'une demande préexistante
     * (titre, description, catégorie, durée de la formation)
     * 
     * @param type $admin
     * @param \Laplace\TrainingBundle\Entity\Request $request
     * @return type
     */
    public function createSameAction($admin, Request $request = null){
        $mngr = $this->getDoctrine()->getManager();

        // données initiales du formulaire
        $data = array(
            'request'   => $request,
            'type'      => null,
        );

        // construction du formulaire
        $builder = $this->createNamedFormBuilder('create_request', $data);
        $builder->add('request', new RequestType());

        if (!$admin) {
            $builder->add('type', new TypeSelectorType());
        }

        $builder->add('Envoyer', 'submit');

        // validation
        $form = $builder->getForm();
        if ($this->isNamedFormSubmitted('create_request', $form)) {
            if ($form->isValid()) {
                $data   = $form->getData();
                $user   = $this->getUser();

                // demande
                $request = $data['request'];
                $request->setAuthor($this->getUser());
                $request->setValidated($admin);
                $mngr->persist($request);
                $mngr->flush();

                // inscription
                if (!$admin) {
                    $subscription = new RequestSubscription();
                    $subscription->setRequest($request);
                    $subscription->setUser($this->getUser());
                    $subscription->setType($data['type']);
                    $mngr->persist($subscription);

                    // création d'un événement
                    $event = Event::create(Event::REQUEST_ADDED, $user,
                        $request
                    );
                    $mngr->persist($event);

                    // flush
                    $mngr->flush();
                }

                $this->flashInfo(
                    $admin ?
                        'La demande a été créée.' :
                        'La demande a été créée et ajoutée à votre liste.'
                );

                $user_url = $this->generateUrl(
                    'laplace_training_view_request',
                    array(
                        'id' => $request->getId(),
                    ),
                    true
                );
                $admin_url = $this->generateUrl(
                    'laplace_training_adm_view_request',
                    array(
                        'id' => $request->getId(),
                    ),
                    true
                );

                // notification
                $this->get('laplace.alert')->important(
                    $admin_url,
                    'Une nouvelle demande, intitulée "%s" ' .
                    'a été créée par %s.',
                    $request->getTitle(),
                    $this->getUser()->getFullName()
                );

                if ($admin) {
                    return $this->redirect($admin_url);
                }

                // email
                $this->get('laplace.mailer')->send(
                    $this->getUser(),
                    'CORRESP_FORM',
                    'Nouvelle demande',
                    $this->renderView(
                        'LaplaceTrainingBundle:Mail:new-request.html.twig',
                        array(
                            'request'   => $request,
                            'url'       => $admin_url
                        )
                    )
                );

                return $this->redirect($user_url);
            } else {
                $this->flashFormError();
            }
        }

        // échec de la validation : création de la vue
        return $this->render(
            'LaplaceTrainingBundle:Request:create.html.twig',
            array(
                'form'      => $form->createView(),
                'admin'     => $admin
            )
        );
    }

    /**
     * Page "Voir Demande"
     */
    public function viewAction($admin, Request $request = null)
    {
        $this->check($request);

        if (   !$admin && !$request->getValidated()
            && $request->getAuthor() != $this->getUser()
        ) {
            throw new AccessDeniedHttpException(
                'Vous ne pouvez pas voir cette demande.'
            );
        }

        $mngr = $this->getDoctrine()->getManager();

        $parameters = array(
            'request'   => $request,
            'admin'     => $admin,
        );

        $thread_redirect = $this->createThreadView($parameters, 'thread_form');
        if ($thread_redirect != null) {
            return $thread_redirect;
        }

        //
        // Lorsqu'un utilisateur consulte la page, il faut lui proposer de gérer
        // son inscription.
        //
        // Lorsqu'un administrateur consulte la page, il faut lui proposer de
        // gérer les inscriptions en attente de validation.
        //
        // Dans les deux cas, il faudra afficher et gérer les discussions.
        //

        $do_forms = array();

        if (!$admin) {
            $user = $this->getUser();
            $subscription = $mngr
                ->getRepository('LaplaceTrainingBundle:RequestSubscription')
                ->fetchOneByUserAndRequest($user, $request);

            $parameters['subscription'] = $subscription;

            // si l'utilisateur est inscrit
            if ($subscription) {
                $accepted = $subscription->getAccepted();
                if ($accepted === null) {
                    // et qu'il n'a pas encore été accepté ou refusé :
                    // on lui propose de modifier son inscription

                    $do_forms[] = 'edit';
                    $do_forms[] = 'unsubscribe';
                } elseif ($accepted) {
                    // sinon s'il est accepté et qu'il n'a pas indiqué
                    // s'il avait participé ou non à la formation,
                    // on lui demande

                    if ($subscription->getAttended() === null) {
                        $do_forms[] = 'attended';
                    }
                }
            } elseif (!$request->getClose()) {
                // sinon, si l'utilisateur n'est pas inscrit et que
                // la demande est encore ouverte, on lui permet de
                // s'inscrire

                $do_forms[] = 'subscribe';
            }

            // récupération des discussions
            $parameters['threads'] = $this->getThreadList($user, $request);
        } else {
            // $admin is true
            $parameters['subscriptions'] = $mngr
                ->getRepository('LaplaceTrainingBundle:RequestSubscription')
                ->fetchByRequest($request);

            // récupération des discussions
            $parameters['threads'] = $this->getThreadList(null, $request);
        }

        $user_url = $this->generateUrl(
            'laplace_training_view_request',
            array(
                'id' => $request->getId(),
            ),
            true
        );
        $admin_url = $this->generateUrl(
            'laplace_training_adm_view_request',
            array(
                'id' => $request->getId(),
            ),
            true
        );
        $url = $admin ? $admin_url : $user_url;

        // formulaire d'inscription
        if (in_array('subscribe', $do_forms)) {
            $sub = null;
            if ($this->doSubscribeForm($parameters, 'subscribe_form', $sub)) {
                // notification
                $this->get('laplace.alert')->notice(
                    $admin_url,
                    '%s a ajouté la demande "%s".',
                    $user->getFullName(),
                    $request->getTitle()
                );

                if ($admin) {
                    return $this->redirect($url);
                }

                // email
                $this->get('laplace.mailer')->send(
                    $this->getUser(),
                    'CORRESP_FORM',
                    'Nouvelle inscription à une demande',
                    $this->renderView(
                        'LaplaceTrainingBundle:Mail:'.
                        'request-sub.html.twig',
                        array(
                            'subscription'  => $sub,
                            'url'           => $admin_url,
                        )
                    )
                );

                return $this->redirect($url);
            }
        }

        // formulaire d'édition d'une inscription
        if (in_array('edit', $do_forms)) {
            if ($this->doEditSubscriptionForm($parameters, 'edit_form')) {
                return $this->redirect($url);
            }
        }

        // formulaire de retrait d'une inscription
        if (in_array('unsubscribe', $do_forms)) {
            if ($this->doUnsubscribeForm($parameters, 'unsubscribe_form')) {
                // notification
                $this->get('laplace.alert')->information(
                    $admin_url,
                    '%s a retiré la demande "%s".',
                    $this->getUser()->getFullName(),
                    $request->getTitle()
                );

                return $this->redirect($url);
            }
        }

        // formulaire de participation à une formation
        if (in_array('attended', $do_forms)) {
            if ($this->doAttendedForm($parameters, 'attended_form')) {
                return $this->redirect($url);
            }
        }
        
        //On ajoute aux paramètres la liste des formulaires disponibles
        $file = fopen("../laplace_config.json", "r") or die("Unable to open file!");
        $read = fread($file,filesize("../laplace_config.json"));
        fclose($file);
        $config = json_decode($read);
        
        $parameters['templates'] = $config->templates;

        return $this->render(
            'LaplaceTrainingBundle:Request:view.html.twig',
            $parameters
        );
    }
    
    public function changeDate($admin){
        
    }

    /**
     * Page "Toutes les demandes".
     *
     * @param boolean $admin
     */
    public function viewAllAction($admin)
    {
        $parameters = array();
        $parameters['admin'] = $admin;
        $parameters['profile'] = $this->getUser();
        $mngr = $this->getDoctrine()->getManager();

        $domains = $mngr
            ->getRepository('LaplaceTrainingBundle:Domain')
            ->fetchAll();
        
        $parameters['domains'] = $domains;

        $requests = $mngr
            ->getRepository('LaplaceTrainingBundle:Request')
            ->fetchAll(null);

        // regrouper les demandes par catégories et domaines
        $groups = array(
            null => array(),
        );
        
        foreach ($requests as $request_info) {
            $cat = $request_info->getRequest()->getCategory();

            if ($cat == null) {
                $groups[null][] = $request_info;
            } elseif (isset($groups[$cat->getId()])) {
                $groups[$cat->getId()][] = $request_info;
            } else {
                $groups[$cat->getId()] = array($request_info);
            }
        }
        
        $parameters['groups'] = $groups;
        
        //On récupère dans la config le nombre d'éléments ou la date à partir de laquelle on veut les afficher
        $file = fopen("../laplace_config.json", "r") or die("Unable to open file!");
        $read = fread($file,filesize("../laplace_config.json"));
        fclose($file);
        
        //On transforme le json en objet PHP
        $config = json_decode($read);
        $parameters['config'] = $config;
        
        //Si l'utilisateur est admin, il peut modifier la date à partir de laquelle les demandes apparaissent
        if($admin){
            
            $form_name = 'change_date_form';
            $data = array(
                'datepicker'  => $config->requests_display->type == 'date' ? date('d/m/Y', strtotime($config->requests_display->val)) : date('d/m/Y', strtotime('2015/01/01')),
                'numberpicker' => $config->requests_display->type == 'number' ? $config->requests_display->val : '30'
            );
            
            $builder = $this->createNamedFormBuilder($form_name, $data);
            
            $builder->add('type_val', 'choice',
                array(
                    'label'         => 'Critère de sélection des demandes',
                    'choices'       => array(
                        'date'      => 'Date',
                        'number'    => 'Nombre de formations',
                    ),
                    'data' => $config->requests_display->type,
                    'constraints'   => new Assert\NotNull(),
                    'expanded'      => true,
                )
            );
            
            $builder->add('datepicker', 'text',
                array(
                    'label'         => 'Date des formations',
                    'constraints'   => new Assert\NotBlank(),
                    'attr' => array('class' => 'calendar'),
                )
            );
            
            $builder->add('numberpicker', 'integer',
                array(
                    'label'         => 'Nombre de formations à afficher',
                    'constraints'   => new Assert\NotBlank(),   
                )
            );
            
            $builder->add('Envoyer', 'submit');
            
            $form = $builder->getForm();
            if ($this->isNamedFormSubmitted($form_name, $form)) {
                if ($form->isValid()) {
                    $data = $form->getData();
                    
                    //On change le type de tri
                    $type = $data['type_val'];
                    $config->requests_display->type = $type;
                    
                    $new_val = 0;
                    if($type == 'date'){
                        $datas = split('/', $data['datepicker']);
                        $d = $datas[2] . '/' . $datas[1] . '/' . $datas[0];
                        $new_val = $d;
                    }else{
                        $new_val = $data['numberpicker'];
                    }
                    
                    $config->requests_display->val = $new_val;
                    $str = json_encode($config);
                    
                    //on écrit la nouvelle date dans le fichier
                    $file = fopen("../laplace_config.json", "w") or die("Unable to open file!");
                    fwrite($file, $str);
                    fclose($file);
                    
                    $this->flashInfo('Le critère d\'affichage a été changée.');
                }
            }
            
            $parameters[$form_name] = $form->createView();
            $parameters['type'] = $config->requests_display->type;
        }

        return $this->render(
            'LaplaceTrainingBundle:Request:view-all.html.twig',
            $parameters
        );
    }

    /**
     * Page "Demandes Personnelless".
     */
    public function viewActiveSubscriptionsAction()
    {
        $mngr = $this->getDoctrine()->getManager();

        $subscriptions = $mngr
            ->getRepository('LaplaceTrainingBundle:RequestSubscription')
            ->fetchByUser($this->getUser());

        return $this->render(
            'LaplaceTrainingBundle:Request:view-active-subscriptions.html.twig',
            array(
                'subscriptions' => $subscriptions,
            )
        );
    }

    /**
     * Page "Modifier une demande".
     */
    public function editAction($admin, Request $request = null)
    {
        $this->check($request);

        if (!$admin && $this->getUser() != $request->getAuthor()) {
            throw new AccessDeniedHttpException(
                'Vous ne pouvez pas modifier cette demande.'
            );
        }

        $mngr = $this->getDoctrine()->getManager();

        $parameters = array(
            'admin'     => $admin,
            'request'   => $request,
        );

        $user_url = $this->generateUrl(
            'laplace_training_view_request',
            array(
                'id' => $request->getId(),
            )
        );
        $admin_url = $this->generateUrl(
            'laplace_training_adm_view_request',
            array(
                'id' => $request->getId(),
            )
        );
        $url = $admin ? $admin_url : $user_url;

        //
        // Formulaire d'édition
        //

        // données initiales du formulaire
        $form_name  = 'edit_form';
        $data       = array(
            'request' => $request,
        );

        // si la demande n'est pas encore validée, on pré-remplit le champ
        // 'date de traitement' avec la date d'aujourd'hui
        if ($admin && $request->getProcessingDate() == null) {
            $request->setProcessingDate(new \DateTime());
        }

        // construction du formulaire
        $builder = $this->createNamedFormBuilder($form_name, $data);
        $builder->add('request', new RequestType(),
            array(
                'admin' => $admin,
            )
        );
        $builder->add('Envoyer', 'submit');

        // validation
        $form = $builder->getForm();
        if ($this->isNamedFormSubmitted($form_name, $form)) {
            if ($form->isValid()) {
                $request->setAuthor($this->getUser());
                $request->setValidated($admin);
                $mngr->persist($request);
                $mngr->flush();

                $this->flashInfo('La demande a été mise à jour.');

                return $this->redirect($url);
            } else {
                $this->flashFormError();
            }
        }

        // échec de la validation : création de la vue
        $parameters[$form_name] = $form->createView();

        return $this->render(
            'LaplaceTrainingBundle:Request:edit.html.twig',
            $parameters
        );
    }

    /**
     * Page "Supprimer une demande" (administrateur).
     */
    public function deleteAction(Request $request = null)
    {
        $this->check($request);

        $mngr   = $this->getDoctrine()->getManager();
        $url    = $this->generateUrl('laplace_training_adm_all_requests');

        $parameters = array(
            'request' => $request,
        );

        //
        // Formulaire de suppression
        //

        // données initiales du formulaire
        $form_name  = 'delete_form';
        $data       = array(
            'delete' => false,
        );

        // construction du formulaire
        $builder = $this->createNamedFormBuilder($form_name, $data);
        $builder->add('delete', 'checkbox',
            array(
                'constraints' =>
                    new Assert\True(),
                'label' =>
                    'Supprimer cette demande et tout ce qui lui est rattaché'
            )
        );
        $builder->add('do', 'submit',
            array(
                'label' => 'Supprimer',
            )
        );

        // validation
        $form = $builder->getForm();
        if ($this->isNamedFormSubmitted($form_name, $form)) {
            if ($form->isValid()) {
                try {
                    $mngr->getRepository('LaplaceTrainingBundle:Request')
                        ->deleteRequests(array($request));

                    $this->flashInfo(
                        'La demande "%s" a été supprimée.',
                        $request->getTitle()
                    );

                    return $this->redirect($url);
                } catch (\Exception $e) {
                    if ($e instanceOf DBALException) {
                        $this->flashError(
                            'Erreur lors de la suppression ' .
                            'de la demande "%s" [%s]',
                            $request->getTitle(),
                            $e->getMessage()
                        );
                    } else {
                        throw $e;
                    }
                }
            } else {
                $this->flashFormError();
            }
        }

        // échec de la validation : création de la vue
        $parameters[$form_name] = $form->createView();

        return $this->render(
            'LaplaceTrainingBundle:Request:delete.html.twig',
            $parameters
        );
    }

    /**
     * @ParamConverter("request", options={"mapping":{"request_id":"id"}})
     * @ParamConverter("user", options={"mapping":{"user_id":"id"}})
     */
    public function manageSubscriptionAction(
        Request $request = null,
        User $user = null
    ) {
        $mngr = $this->getDoctrine()->getManager();

        // données initiales du formulaire
        $subscription = null;
        if ($request != null && $user != null) {
            $subscription = $mngr
                ->getRepository('LaplaceTrainingBundle:RequestSubscription')
                ->fetchOneByUserAndRequest($user, $request);
        }

        if ($subscription == null) {
            throw $this->createNotFoundException(
                'Cette inscription n\'existe pas, ou plus.'
            );
        }

        $prev_accepted = $subscription->getAccepted();

        $user_url = $this->generateUrl(
            'laplace_training_view_request',
            array(
                'id' => $request->getId(),
            ),
            true
        );
        $admin_url = $this->generateUrl(
            'laplace_training_adm_view_request',
            array(
                'id' => $request->getId(),
            ),
            true
        );

        // construction
        $builder = $this->createManageSubscriptionFormBuilder($subscription);

        // validation
        $form = $builder->getForm();
        $form->handleRequest($this->getRequest());
        if ($form->isValid()) {
            $data = $form->getData();

            // $subscription est modifié par hydratation,
            // on doit quand même définir $subscription->attended à null
            // si l'inscription n'est pas acceptée.
            if (!$subscription->getAccepted()) {
                // accepted vaut null ou false
                $subscription->setAttended(null);
            }

            // gestion des événements

            $event_repo = $mngr->getRepository('LaplaceTrainingBundle:Event');

            // pour que l'historique de l'utilisateur reste cohérent avec
            // les modifications successives, on va d'abord supprimer tous les
            // événements autre que REQUEST_ADDED puis on ajoute de nouveaux
            // événements selon les valeurs données par l'administrateur

            $events = $event_repo->fetchBy(
                array(
                    'user' => $user,
                    'mask' => Event::MASK_REQUEST,
                    'types' => array(
                        Event::REQUEST_ACCEPTED,
                        Event::REQUEST_DENIED,
                        Event::REQUEST_ATTENDED,
                        Event::REQUEST_NOT_ATTENDED,
                    ),
                    'reference' => $request,
                )
            );
            $event_repo->deleteEvents($events);

            $accepted   = $subscription->getAccepted();
            $attended   = $subscription->getAttended();

            if ($accepted !== null) {
                // l'admin a validé ou refusé la demande, on ajoute l'événement
                // correspondant

                $mngr->persist(
                    Event::create(
                        $accepted ?
                            Event::REQUEST_ACCEPTED :
                            Event::REQUEST_DENIED,
                        $user,
                        $request
                    )
                );

                if ($attended !== null) {
                    // l'admin a indiqué si l'utilisateur avait participé ou non
                    // à la formation, on ajoute également l'événement

                    $mngr->persist(
                        Event::create(
                            $attended ?
                                Event::REQUEST_ATTENDED :
                                Event::REQUEST_NOT_ATTENDED,
                            $user,
                            $request
                        )
                    );
                }
            }

            $mngr->flush();

            $this->flashInfo(
                'L\'inscription de %s a été modifiée.',
                $user->getFullName()
            );

            if ($data['email'] && $prev_accepted !== $accepted) {
                $this->get('laplace.mailer')->send(
                    $this->getUser(),
                    $user,
                    'Mise à jour de votre demande de formation',
                    $this->renderView(
                        'LaplaceTrainingBundle:Mail:'.
                        'request-sub-edited.html.twig',
                        array(
                            'subscription'  => $subscription,
                            'url'           => $user_url,
                        )
                    )
                );

                $this->flashInfo(
                    'Un email a été envoyé à %s.',
                    $user->getFullName()
                );
            }
        } else {
            $this->flashError(
                'Erreur lors de la modification de l\'inscription de %s.',
                $user->getFullName()
            );
        }

        return $this->redirect($admin_url);
    }

    public function manageSubscriptionFormAction(SubscriptionInfo $subinfo)
    {
        $user           = $subinfo->getUser();
        $subscription   = $subinfo->getSubscription();

        $action = $this->generateUrl(
            'laplace_training_adm_manage_request_sub',
            array(
                'request_id'    => $subscription->getRequest()->getId(),
                'user_id'       => $user->getId(),
            )
        );

        $builder = $this->createManageSubscriptionFormBuilder($subscription);
        $builder->setAction($action);

        $form = $builder->getForm();

        $parameters = array(
            'user'          => $user,
            'subscription'  => $subscription,
            'manage_form'   => $form->createView(),
        );

        return $this->render(
            'LaplaceTrainingBundle:Request:'.
            'manage-subscription-form.html.twig',
            $parameters
        );
    }

/******************************************************************************/

    private function check(Request $request = null)
    {
        if ($request == null) {
            throw $this->createNotFoundException(
                'Cette demande n\'existe pas.'
            );
        }
    }

    private function doSubscribeForm(&$parameters, $form_name, &$subscription)
    {
        $mngr = $this->getDoctrine()->getManager();

        $request = $parameters['request'];

        // données initiales du formulaire
        $data = array(
            'type' => null,
        );

        // création du formulaire
        $builder = $this->createNamedFormBuilder($form_name, $data);
        $builder->add('type', new TypeSelectorType());
        $builder->add('Envoyer', 'submit');

        // validation
        $form = $builder->getForm();
        if ($this->isNamedFormSubmitted($form_name, $form)) {
            if ($form->isValid()) {
                $data   = $form->getData();
                $user   = $this->getUser();

                $subscription = new RequestSubscription();
                $subscription->setRequest($request);
                $subscription->setUser($user);
                $subscription->setType($data['type']);

                $event = Event::create(Event::REQUEST_ADDED, $user, $request);

                $mngr->persist($subscription);
                $mngr->persist($event);
                $mngr->flush();

                $this->flashInfo('Votre inscription a été enregistrée.');

                return  true;
            } else {
                $this->flashFormError();
            }
        }

        // échec de la validation : création de la vue
        $parameters[$form_name] = $form->createView();

        return false;
    }

    private function doAttendedForm(&$parameters, $form_name)
    {
        $mngr           = $this->getDoctrine()->getManager();

        $request        = $parameters['request'];
        $subscription   = $parameters['subscription'];

        // données initiales du formulaire
        $data = array(
            'attended' => $subscription->getAttended(),
        );

        // création du formulaire
        $builder = $this->createNamedFormBuilder($form_name, $data);
        $builder->add('attended', 'choice',
            array(
                'choices' => array(
                    true => 'Oui',
                    false => 'Non'
                ),
                'expanded' => true,
                'label' => 'Avez-vous assisté à la formation ?',
            )
        );
        $builder->add('Envoyer', 'submit');

        // validation
        $form = $builder->getForm();
        if ($this->isNamedFormSubmitted($form_name, $form)) {
            if ($form->isValid()) {
                $data   = $form->getData();
                $user   = $this->getUser();

                $subscription->setAttended($data['attended']);
                $event = Event::create(
                    $data['attended'] ?
                        Event::REQUEST_ATTENDED :
                        Event::REQUEST_NOT_ATTENDED,
                    $user,
                    $request
                );

                $mngr->persist($event);
                $mngr->flush();

                $this->flashInfo('Votre inscription a été mise à jour.');

                return true;
            } else {
                $this->flashFormError();
            }
        }

        // échec de la validation : création de la vue
        $parameters[$form_name] = $form->createView();

        return false;
    }

    /**
     * Formulaire de modification d'une inscription.
     *
     * @param  array   $parameters
     * @param  string  $form_name
     * @return boolean true si l'inscription a été modifiée, false sinon
     */
    private function doEditSubscriptionForm(&$parameters, $form_name)
    {
        $mngr = $this->getDoctrine()->getManager();

        $subscription = $parameters['subscription'];

        // données initiales du formulaire
        $data = array(
            'type' => $subscription->getType(),
        );

        // création du formulaire
        $builder = $this->createNamedFormBuilder($form_name, $data);
        $builder->add('type', new TypeSelectorType());
        $builder->add('Envoyer', 'submit');

        // validation
        $form = $builder->getForm();
        if ($this->isNamedFormSubmitted($form_name, $form)) {
            if ($form->isValid()) {
                $data = $form->getData();

                $subscription->setType($data['type']);
                $subscription->setSubscriptionDate(new \DateTime());

                $mngr->merge($subscription);
                $mngr->flush();

                $this->flashInfo('Votre inscription a été mise à jour.');

                return true;
            } else {
                $this->flashFormError();
            }
        }

        // échec de la validation : création de la vue
        $parameters[$form_name] = $form->createView();

        return false;
    }

    private function doUnsubscribeForm(&$parameters, $form_name)
    {
        $mngr = $this->getDoctrine()->getManager();

        $request        = $parameters['request'];
        $subscription   = $parameters['subscription'];

        // données initiales du formulaires
        $data = array(
            'unsubscribe' => false,
        );

        // construction du formulaire
        $builder = $this->createNamedFormBuilder($form_name, $data);
        $builder->add('unsubscribe', 'checkbox',
            array(
                'label'         => 'Retirer cette demande',
                'constraints'   => new Assert\True(),
            )
        );
        $builder->add('do', 'submit',
            array(
                'label' => 'Retirer',
            )
        );

        // validation
        $form = $builder->getForm();
        if ($this->isNamedFormSubmitted($form_name, $form)) {
            if ($form->isValid()) {
                $event_repo = $mngr
                    ->getRepository('LaplaceTrainingBundle:Event');

                $events = $event_repo->fetchBy(
                    array(
                        'user'          => $this->getUser(),
                        'mask'          => Event::MASK_REQUEST,
                        'reference'     => $request
                    )
                );

                $event_repo->deleteEvents($events);

                $mngr->remove($subscription);
                $mngr->flush();

                $this->flashInfo('Votre inscription a été retirée.');

                return true;
            } else {
                $this->flashFormError();
            }

        }

        // échec de la validation : création de la vue
        $parameters[$form_name] = $form->createView();

        return false;
    }

    private function createManageSubscriptionFormBuilder(
        RequestSubscription $subscription
    ) {
        $data = array(
            'subscription'  => $subscription,
            'email'         => true,
        );

        $builder = $this->createFormBuilder($data,
            array(
                'cascade_validation' => true,
            )
        );
        $builder->add('subscription', new RequestSubscriptionType());
        $builder->add('email', 'checkbox',
            array(
                'label'     =>
                    'Envoyer un email si l\'état de la demande a changé',
                'required'  =>
                    false,
            )
        );
        $builder->add('Envoyer', 'submit');

        return $builder;
    }

}
