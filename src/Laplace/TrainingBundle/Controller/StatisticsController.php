<?php

namespace Laplace\TrainingBundle\Controller;

use Symfony\Component\HttpFoundation\Response;

use Laplace\CommonBundle\Controller\AbstractBaseController;

use Symfony\Component\HttpFoundation\JsonResponse;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Contrôleur de la partie administration relative à ce bundle.
 */
class StatisticsController extends AbstractBaseController
{
    
    /**
     * Créer un résumé des besoins et le transforme en CSV avant de l'envoyer au client.
     * Le client se voit offrir la possibilité de télécharger le fichier.
     */
    public function needsCSVAction(){
        
        $summary = $this->buildNeedsSummary();
        
        //On envoie le CSV direct
        $summary->sendCSV();
        
        return new JsonResponse('', 200);
    }
    
    /**
     * Créer un résumé des demandes et le transforme en CSV avant de l'envoyer au client.
     * Le client se voit offrir la possibilité de télécharger le fichier.
     */
    public function requestsCSVAction($from, $to, $type, $tutelle){

        $tutelle = split('\|', $tutelle);
        
        $from = split('-', $from);
        $to = split('-', $to);
        
        $from_date = new \DateTime();
        $to_date = new \DateTime();
        
        $from_date->setDate($from[2], $from[1], $from[0]);
        $to_date->setDate($to[2], $to[1], $to[0]);
        
        //On récupère la liste des tutelles
        $mngr = $this->getDoctrine()->getManager();
        $query_builder = $mngr->createQueryBuilder();
        $query_builder
            ->select('tut')
            ->from('LaplaceUserBundle:Tutelle', 'tut')
            // ordre
            ->orderBy('tut.nom', 'ASC');
        $query = $query_builder->getQuery();
        $tutelles = $query->getResult();
        
        //On parcourt toutes les tutelles de la BD
        $t = array();
        for($i = 0; $i < count($tutelles); $i++){
            if(in_array($tutelles[$i]->getNom(), $tutelle)){
                $t[$tutelles[$i]->getNom()] = true;
            }else{
                $t[$tutelles[$i]->getNom()] = false;
            }
        }
        
        $summary = null;
        if($type == 'tutelle'){
            $summary = $this->buildRequestsSummaryByTutelle($from_date, $to_date, $t);
        }elseif($type == 'statut'){
            $summary = $this->buildRequestsSummaryByStatut($from_date, $to_date, $t);
        }else{
            $summary = $this->buildRequestsSummaryByYears($from_date, $to_date, $t);
        }

        //On envoie le CSV direct
        $summary->sendCSV($from_date->format('Y'), $to_date->format('Y'));
        
        return new JsonResponse('', 200);
    }

    /**
     * Construit un résumé de tous les besoins qui n'ont pas été pourvus.
     * Cela correspond à toutes les inscriptions dans la base. Les résultats
     * sont classés en premier lieu par type, puis par catégorie. On compte
     * pour chaque ligne, c'est-à-dire pour chaque besoin, le nombre d'inscrits
     * par tutelle.
     *
     * @return Response
     */
    public function needsSummaryAction()
    {
        $parameters = array();

        $parameters['summary'] = $this->buildNeedsSummary();

        return $this->render(
            'LaplaceTrainingBundle:Statistics:needs-summary.html.twig',
            $parameters
        );
    }

    public function requestsSummaryAction()
    {
        $parameters = array();

        // On pré-configure la période à l'année actuelle
        $year = intval(date('Y'));

        $new_years_day = new \DateTime();
        $new_years_day->setDate($year, 1, 1);
        $new_years_day->setTime(0, 0, 0);

        $new_years_eve = clone $new_years_day;
        $new_years_eve->add(new \DateInterval('P1Y'));

        // données initiales du formulaire
        $form_name  = 'summary_form';
        $data       = array(
            'from'  => $new_years_day,
            'to'    => $new_years_eve,
        );

        // création du formulaire
        $builder = $this->createNamedFormBuilder($form_name, $data);
        
        $builder->add('from', 'date',
            array(
               'widget'     => 'single_text',
                'format'    => 'dd/MM/yy',
                'label'     => 'Date de début',
            )
        );
        
        $builder->add('to', 'date',
            array(
               'widget'     => 'single_text',
                'format'    => 'dd/MM/yy',
                'label'     => 'Date de fin',
            )
        );
        
        $builder->add('type', 'choice', 
            array(
                'label'         => 'Type de bilan',
                'choices'       => array(
                    'year'      => 'Par année',
                    'tutelle'    => 'Par tutelle',
                    'statut'    => 'Par statut',
                ),
                'data' => 'date',
                'constraints'   => new Assert\NotNull(),
            )
        );
        
        //On récupère la liste des tutelles
        $mngr = $this->getDoctrine()->getManager();
        $query_builder = $mngr->createQueryBuilder();
        $query_builder
            ->select('tut')
            ->from('LaplaceUserBundle:Tutelle', 'tut')
            // ordre
            ->orderBy('tut.nom', 'ASC');
        $query = $query_builder->getQuery();
        $tutelles = $query->getResult();
        
        for($i = 0; $i < count($tutelles); $i++){
            $builder->add('tutelle' . $i, 'checkbox', 
                array(
                    'label'         => $tutelles[$i]->getNom(),
                    'required'   => false,
                )
            );
        }
        
        $builder->add('Envoyer', 'submit');

        // validation
        $form = $builder->getForm();
        if ($this->isNamedFormSubmitted($form_name, $form) &&  $form->isValid()) {
            
            $data   = $form->getData();
            
            //On récupère les tutelles utilisées
            $tutelle = array();
            for($i = 0; $i < count($tutelles); $i++){
                $tutelle[$tutelles[$i]->getNom()] = $data['tutelle' . $i];
            }
            
            if($data['type'] == 'year')
                $parameters['summary'] = $this->buildRequestsSummaryByYears($data['from'], $data['to'], $tutelle);
            elseif($data['type'] == 'tutelle')
                $parameters['summary'] = $this->buildRequestsSummaryByTutelle($data['from'], $data['to'], $tutelle);
            elseif($data['type'] == 'statut')
                $parameters['summary'] = $this->buildRequestsSummaryByStatut($data['from'], $data['to'], $tutelle);
            else
                $parameters['summary'] = null;
            
        }

        $parameters[$form_name] = $form->createView();

        return $this->render(
            'LaplaceTrainingBundle:Statistics:requests-summary.html.twig',
            $parameters
        );
    }

    private function buildNeedsSummary()
    {
        $mngr = $this->getDoctrine()->getManager();
        
        //On ordonne les types
        $summary = new Summary();
        
        $mngr = $this->getDoctrine()->getManager();
        $query_builder = $mngr->createQueryBuilder();
        $query_builder
            ->select('type')
            ->from('LaplaceTrainingBundle:Type', 'type')
            // ordre
            ->orderBy('type.name', 'ASC');
        $query = $query_builder->getQuery();
        $types = $query->getResult();
        
        for($i = 0; $i<count($types); $i++)
            $summary->get($types[$i]->getName() . ' : ' . $types[$i]->getDescription());

        // on récupère tous les types, tous les domaines et catégories
        // les entités deviendront managées et il n'y aura pas de requêtes
        // supplémentaires lors de l'affichage des résultats

        // on récupère toutes les tutelles
        $tutelles = $mngr
            ->getRepository('LaplaceUserBundle:Tutelle')
            ->findBy(array(), array('nom' => 'ASC'));

        $columns = array();
        foreach ($tutelles as $tutelle) {
            $columns[] = $tutelle->getNom();
        }

        // on récupère les inscriptions
        $query_builder = $mngr->createQueryBuilder();
        $query_builder
            ->select(array('sub', 'need', 'user', 'info'))
            ->from('LaplaceTrainingBundle:NeedSubscription', 'sub')
            ->innerJoin('sub.need', 'need')
            ->innerJoin('sub.user', 'user')
            ->innerJoin('user.userInfo', 'info')
            ->innerJoin('need.category', 'cat')
            // ordre
            ->orderBy('cat.code', 'ASC');

        $subscriptions = $query_builder->getQuery()->getResult();

        // tableau associatif need_id => array( tutelle => count )
        $summary->setColumns($columns);
        $summary->type = 'besoins';

        foreach ($subscriptions as &$sub) {
            $type       = $sub->getType()->getNameWithDescription();
            $cat        = $sub->getNeed()->getCategory();
            $need       = $sub->getNeed()->getTitle();
            $tutelle    = $sub->getUser()->getTutelle()->getNom();

            $summary
                ->get($type)
                ->get($cat != null ? $cat->getName() : '')
                ->get($need)
                ->incr($tutelle);
        }

        return $summary;
    }

    /**
     * Construit un formulaire présentant le nombre de formation par année pour chaque catégorie.
     * 
     * @param type $from Date de début
     * @param type $to Date de fin 
     * @return \Laplace\TrainingBundle\Controller\Summary
     */
    private function buildRequestsSummaryByYears($from, $to, $tutelle)
    {
        //On récupère les informations sur les inscriptions et on construit l'objet sommaire de base
        list($summary, $subscriptions) = $this->getRequestsInformation($from, $to, $tutelle);
        $summary->type = 'annee';
        
        //On indique les colonnes du bilan
        $min_year = (int) $from->format('Y');
        $max_year = (int) $to->format('Y');
        $summary->setColumns(range($min_year, $max_year));

        foreach ($subscriptions as &$sub) {
            $type       = $sub->getType()->getNameWithDescription();
            $cat        = $sub->getRequest()->getCategory();
            $request    = $sub->getRequest()->getTitle();
            $year       = $sub->getRequest()->getTrainingDate()->format('Y');

            $summary
                ->get($type)
                ->get($cat != null ? $cat->getName() : '')
                ->get($request)
                ->incr($year);
            
            if($sub->getAttended() == true)
                $summary
                ->get($type)
                ->get($cat != null ? $cat->getName() : '')
                ->get($request)
                ->incrAttended($year);
        }

        return $summary;
    }
    
    /**
     * Construit un formulaire présentant le nombre de formation par tutelle pour chaque catégorie.
     * 
     * @param type $from Date de début
     * @param type $to Date de fin 
     * @return \Laplace\TrainingBundle\Controller\Summary
     */
    private function buildRequestsSummaryByTutelle($from, $to, $tutelle){
        //On récupère les informations sur les inscriptions et on construit l'objet sommaire de base
        list($summary, $subscriptions) = $this->getRequestsInformation($from, $to, $tutelle);
        $summary->type = 'tutelle';
        
        //On récupère toutes les tutelles de l'appli
        $mngr = $this->getDoctrine()->getManager();
        $tutelles = $mngr
            ->getRepository('LaplaceUserBundle:Tutelle')
            ->findBy(array(), array('nom' => 'ASC'));
        
        //On convertit en string
        $columns = array();
        foreach($tutelles as $tut){
            if($tutelle[$tut->getNom()])
                array_push($columns, $tut->getNom());
        }
        
        //On indique les colonnes du bilan
        $summary->setColumns($columns);
        
        foreach ($subscriptions as &$sub) {
            $type       = $sub->getType()->getNameWithDescription();
            $cat        = $sub->getRequest()->getCategory();
            $request    = $sub->getRequest()->getTitle();
            $tut       = $sub->getUser()->getTutelle()->getNom();

            $summary
                ->get($type)
                ->get($cat != null ? $cat->getName() : '')
                ->get($request)
                ->incr($tut);
            
            if($sub->getAttended() == true)
                $summary
                ->get($type)
                ->get($cat != null ? $cat->getName() : '')
                ->get($request)
                ->incrAttended($tut);
        }

        return $summary;
    }
    
    /**
     * Construit un formulaire présentant le nombre de formation par tutelle pour chaque statut d'agent.
     * 
     * @param type $from Date de début
     * @param type $to Date de fin 
     * @return \Laplace\TrainingBundle\Controller\Summary
     */
    private function buildRequestsSummaryByStatut($from, $to, $tutelle){
        //On récupère les informations sur les inscriptions et on construit l'objet sommaire de base
        list($summary, $subscriptions) = $this->getRequestsInformation($from, $to, $tutelle);
        $summary->type = 'statut';
        
        //On récupère toutes les tutelles de l'appli
        $mngr = $this->getDoctrine()->getManager();
        $statuts = $mngr
            ->getRepository('LaplaceUserBundle:StatutAgent')
            ->findBy(array(), array('intitule' => 'ASC'));
        
        //On convertit en string
        $columns = array();
        foreach($statuts as $stat){
            array_push($columns, $stat->getIntitule());
        }
        
        //On indique les colonnes du bilan
        $summary->setColumns($columns);
        
        foreach ($subscriptions as &$sub) {
            $type       = $sub->getType()->getNameWithDescription();
            $cat        = $sub->getRequest()->getCategory();
            $request    = $sub->getRequest()->getTitle();
            $stat       = $sub->getUser()->getUserInfo()->getStatut()->getIntitule();

            $summary
                ->get($type)
                ->get($cat != null ? $cat->getName() : '')
                ->get($request)
                ->incr($stat);
            
            if($sub->getAttended() == true)
                $summary
                ->get($type)
                ->get($cat != null ? $cat->getName() : '')
                ->get($request)
                ->incrAttended($stat);
        }

        return $summary;
    }
    
    
    
    /**
     * Renvoie les données nécessaires à la création de tout type de bilan et l'objet summary de base.
     * Les fonctions appelantes seront en charge de mettre cela en forme en fonction du
     * type de formulaire souhaité par l'utilisateur.
     * 
     * @param type $from
     * @param type $to
     * @return Le bilan vide et ses informations
     */
    private function getRequestsInformation($from, $to, $tutelle){
        $mngr = $this->getDoctrine()->getManager();

        // on récupère les inscriptions
        $query_builder = $mngr->createQueryBuilder();
        $query_builder
            ->select(array('sub', 'request', 'user', 'info'))
            ->from('LaplaceTrainingBundle:RequestSubscription', 'sub')
            ->innerJoin('sub.request', 'request')
            ->innerJoin('sub.user', 'user')
            ->innerJoin('user.userInfo', 'info')
            ->innerJoin('request.category', 'cat')
            ->andWhere('request.validated = true')
            ->andWhere('sub.accepted IS NOT null')
            ->andWhere('sub.attended IS NOT null')
            ->andWhere('sub.accepted = true')
            ->andWhere('sub.attended = true OR sub.attended = false')
            // date
            ->andWhere('request.trainingDate IS NOT null')
            ->andWhere('request.trainingDate >= :from')
            ->andWhere('request.trainingDate < :to')
            // ordre
            ->orderBy('cat.code', 'ASC')
            ->setParameter('from', $from->format('d/m/y'))
            ->setParameter('to', $to->format('d/m/y'));
        $query = $query_builder->getQuery();
        $subscriptions = $query->getResult();

        $summary = new Summary();
        $summary->setFrom($from);
        $summary->setTo($to);
        
        //On supprime les champs qui n'ont pas la bonne tutelle
        $filtered = array();
        for($i = 0; $i < count($subscriptions); $i++){
            if($tutelle[$subscriptions[$i]->getUser()->getTutelle()->getNom()]){
                array_push($filtered, $subscriptions[$i]);
            }
        }
        
         //On récupère la liste des types
        $mngr = $this->getDoctrine()->getManager();
        $query_builder = $mngr->createQueryBuilder();
        $query_builder
            ->select('type')
            ->from('LaplaceTrainingBundle:Type', 'type')
            // ordre
            ->orderBy('type.name', 'ASC');
        $query = $query_builder->getQuery();
        $types = $query->getResult();
        
        for($i = 0; $i<count($types); $i++)
            $summary->get($types[$i]->getName() . ' : ' . $types[$i]->getDescription());
        
        return array($summary, $filtered);
    }

}

class Summary
{

    private $types = array();
    private $columns;
    private $from;
    private $to;
    public $type;

    public function get($type_name)
    {
        if (!array_key_exists($type_name, $this->types)) {
            $this->types[$type_name] = new TypeSection();
        }

        return $this->types[$type_name];
    }

    public function getTypes()
    {
        return $this->types;
    }
    
    public function getTypesNames(){
        return array_keys($this->types);
    }

    public function getFrom()
    {
        return $this->from;
    }

    public function getTo()
    {
        return $this->to;
    }

    public function getColumns()
    {
        return $this->columns;
    }

    public function setFrom($from)
    {
        $this->from = $from;

        return $this;
    }

    public function setTo($to)
    {
        $this->to = $to;

        return $this;
    }

    public function setColumns($columns)
    {
        $this->columns = $columns;

        return $this;
    }
    
    /**
     * Fonction convertissant le bilan en fichier CSV et l'envoyant directement au client
     */
    public function sendCSV($from = '', $to = ''){
        
        //On ouvre le fichier de sortie standard
        header('Content-Type: application/csv; charset=utf-8');
        if($this->type == 'besoins')
            header('Content-Disposition: attachment; filename=bilan-'.$this->type.'.csv');
        else
            header('Content-Disposition: attachment; filename=bilan-'.$this->type.'-'.$from.'-'.$to.'.csv');
        $output = fopen('php://output', 'w');
        
        //On commence par la mettre la ligne du type (T1, T2, T3)
        $types_names = $this->getTypesNames();
        foreach($types_names as $type_name){
            
            $columns = array_map('utf8_decode', $this->getColumns());
            array_unshift($columns, utf8_decode($type_name));
            fputcsv($output, $columns, ';');
            
            $type = $this->get(utf8_decode($type_name));
            
            //Puis on ajoute le nom du domaine
            $categ_names = $type->getCategoriesNames();
            foreach($categ_names as $categ_name){
                $categorie = $type->get($categ_name);
                
                //Enfin, on ajoute les besoins
                $rows_names = $categorie->getRowsNames();
                foreach ($rows_names as $row_name){
                    $row = $categorie->get($row_name);
                    $tab = array();
                    
                    for ($i = 1; $i<count($columns); $i++){
                        array_push($tab, $row->get($columns[$i]) == null ? 0 : $row->get($columns[$i]));
                    }
                    
                    array_unshift($tab, utf8_decode($row_name));
                    fputcsv($output, $tab, ';');
                }
            }
        }
    }

}

class TypeSection
{

    private $categories = array();

    public function get($cat_name)
    {
        if (!array_key_exists($cat_name, $this->categories)) {
            $this->categories[$cat_name] = new CategoryTable();
        }

        return $this->categories[$cat_name];
    }

    public function getCategories()
    {
        return $this->categories;
    }
    
    public function getCategoriesNames(){
       return array_keys($this->categories);
    }

}

class CategoryTable
{

    private $rows = array();

    public function get($row)
    {
        if (!array_key_exists($row, $this->rows)) {
            $this->rows[$row] = new RowCounters();
        }

        return $this->rows[$row];
    }

    public function getRows()
    {
        return $this->rows;
    }
    
    public function getRowsNames(){
        return array_keys($this->rows);
    }
}

class RowCounters
{
    private $values = array();
    private $attended = array();

    public function get($key)
    {
        if (!array_key_exists($key, $this->values)) {
            return null;
        }

        return $this->values[$key];
    }
    
    public function getAttended($key){
        if (!array_key_exists($key, $this->attended)) {
            return 0;
        }

        return $this->attended[$key];
    }

    public function set($key, $value)
    {
        $this->values[$key] = $value;
    }

    public function incr($key)
    {
        if (!array_key_exists($key, $this->values)) {
            $this->values[$key] = 1;
        } else {
            $this->values[$key]++;
        }
    }
    
    public function incrAttended($key){
        if (!array_key_exists($key, $this->attended)) {
            $this->attended[$key] = 1;
        } else {
            $this->attended[$key]++;
        }
    }

    public function getValues(){
        return $this->values;
    }
}
