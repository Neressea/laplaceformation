<?php

namespace Laplace\TrainingBundle\Controller;

use Laplace\CommonBundle\Controller\AbstractBaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Imagick;
use Laplace\TrainingBundle\Controller\DataDictionary;

/**
 * Description of TemplateController
 *
 * @author albert
 */
class TemplateController extends AbstractBaseController{
    
    //Lien vers les templates pdf
    private $path = '../templates/';
    
    //Lien vers les images png des pdfs.
    private $png_path = '../web/images/thumbnails/';
    
    public function viewAllAction($admin, Request $request){
        
        if(!$admin) return;
        
        $parameters = array();
        $parameters['admin'] = $admin;
        
        //on crée un formulaire pour uploader les fichiers
        $form = $this->createFormBuilder()->getForm();
        $form->handleRequest($request);
        
        //On charge l'objet de configuration
        $file = fopen("../laplace_config.json", "r") or die("Unable to open file!");
        $read = fread($file,filesize("../laplace_config.json"));
        fclose($file);

        //On transforme le json en tableau associatif
        $config = json_decode($read);
            
        if ($form->isValid()) {
            
            //on récupère le fichier envoyé par Dropzone
            $file = $request->files->get('file');
            
            //Ensuite on déplace le fichier dans le dossier des templates.
            $file->move($this->path, $file->getClientOriginalName() . '.tmp');
            
            //On le convertit en PDF 1.4 (au-delà de 1.4, les PDFs ne sont pas gérés par TCPDF)
            $pdf = $this->path . $file->getClientOriginalName();
            shell_exec("ghostscript -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dNOPAUSE -dQUIET -dBATCH -sOutputFile=".$pdf." ".$pdf.".tmp");
            unlink($pdf.".tmp");
            
            //On crée les vignettes du pdf.
            $png_folder = $this->png_path . basename($pdf, '.pdf');
            $nb_pages = $this->pdfToThumbnail($pdf, $png_folder);
            
            //Si le pdf existe déjà dans le fichier de configuration, on le supprime.
            $i = 0;
            while($i < count($config->templates) && $i != -1){

                if($config->templates[$i]->name == basename($pdf, '.pdf')){
                    unset($config->templates[$i]);
                    $i = -2;
                }

                $i++;
            }
        
            //On ajoute le nouveau modèle au fichier de configuration : les data et textes sont initialement vides.
            $new_pdf = array('name' => basename($pdf, '.pdf'), 'nb_pages' => $nb_pages, 'data' => array(), 'texts' => array());
            array_push($config->templates, $new_pdf);
            
            //On MàJ le fichier de config
            file_put_contents("../laplace_config.json", json_encode($config));
            
            //On renvoie le nouveau fichier
            return new JsonResponse(array('png' => basename($pdf, '.pdf'), 'pdf' => basename($pdf), 'nbpages' => $nb_pages), 200);
        }
        
        $parameters['form'] = $form->createView();
        
        //On va créer les miniatures des pdfs
        $parameters['pdf_thumb'] = array();
            
        //On parcourt tous les fichiers recensés dans le fichier de configuration
        for($i = 0; $i < count($config->templates); $i++){
            
            $current = $config->templates[$i];
            array_push($parameters['pdf_thumb'], array('png' => $current->name, 'pdf' => $current->name . '.pdf', 'nbpages' => $current->nb_pages));
            
        }
        
        return $this->render(
            'LaplaceTrainingBundle:System:templates.html.twig',
            $parameters
        );
    }
    
    /**
     * Affiche la page du dictionnaire de données.
     * 
     * @param type $admin
     * @param \Symfony\Component\HttpFoundation\Request $request
     */
    public function viewDataAction($admin, Request $request){
        if(!$admin) return;
        
        return $this->render(
            'LaplaceTrainingBundle:System:data-dictionnary.html.twig'
        );
    }
    
    /**
     * Renvoie les données du dictionnaire en JSON.
     * 
     * @param type $admin
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getDataAction($admin, Request $request){
        if(!$admin) return;
        
        //On charge l'objet de configuration
        $file = fopen("../laplace_config.json", "r") or die("Unable to open file!");
        $read = fread($file,filesize("../laplace_config.json"));
        fclose($file);
        
        //On transforme le json en objet
        $config = json_decode($read);
        
        //on récupère le json de la partie voulue
       $json = json_encode($config->dictionnary);
        
        return new JsonResponse($json, 200);
    }
    
    /**
     * Sauvegarde les modifications effectuées sur la page du dictionnaire de données en AJAX.
     */
    public function saveDataAction($admin, Request $request){
        if(!$admin) return;
        
        $json = $request->request->get('dico');
        
        //On convertir le json en objet
        $dico = json_decode($json);
        
        //On récupère l'objet de configuration
        $file = fopen("../laplace_config.json", "r") or die("Unable to open file!");
        $read = fread($file,filesize("../laplace_config.json"));
        fclose($file);
        $config = json_decode($read);
        
        //On remplace le dictionnaire
        $config->dictionnary = $dico;
                
        //On sauvegarde le fichier
        file_put_contents("../laplace_config.json", json_encode($config));
        
        //Tout s'est bien passé
        return new JsonResponse('Les données ont bien été enregistrées', 200);
    }
    
    /**
     * Supprime un fichier modèle du serveur. Métode appelée en AJAX.
     * 
     * @param type $admin
     * @param \Symfony\Component\HttpFoundation\Request $request
     */
    public function deleteAction($admin, Request $request){
        if(!$admin) return new JsonResponse('Page non trouvée', 404);
        
        $pdf = $request->request->get('pdfname');
        
        //On supprime le pdf du dossier des templates
        unlink($this->path . $pdf);
        
        $path_to_thumbnails = $this->png_path . basename($pdf, '.pdf');
        
        //On supprime le dossier des miniatures du pdf
        if(file_exists($path_to_thumbnails)){
            
            //On supprime le contenu du dossier
            foreach (glob($path_to_thumbnails . "/*.png") as $png) {
                unlink($png);
            }
            
            //Enfin on supprime le dossier
            rmdir($path_to_thumbnails);
        }
        
        //On récupère le nouveau JSON de configuration
        $json = $request->request->get('new_config');
        
        //On le transforme en objet
        $templates = json_decode($json);
        
        //On charge l'objet de configuration
        $file = fopen("../laplace_config.json", "r") or die("Unable to open file!");
        $read = fread($file,filesize("../laplace_config.json"));
        fclose($file);
        
        //On transforme le json
        $config = json_decode($read);
        
        //On remplace la config
        $config->templates = $templates;
        
        //On transforme l'objet modifié en JSON
        $json = json_encode($config);
        
        //On écrase le fichier de configuration
        file_put_contents("../laplace_config.json", $json);
        
        return new JsonResponse('Le modèle a bien été supprimé.', 200);
    }
    
    /**
     * Sauvegarde la configuration des templates dans le fichier.
     * 
     * @param type $admin
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function saveAction($admin, Request $request){
        
        //On récupère le JSON des modèles
        $json = $request->request->get('templates');
        
        //On le transforme en objet
        $templates = json_decode($json);
        
        //On charge le fichier de configuration
        $file = fopen("../laplace_config.json", "r") or die("Unable to open file!");
        $read = fread($file,filesize("../laplace_config.json"));
        fclose($file);

        //On le transforme en objet
        $config = json_decode($read);
        
        //On modifie sa composante templates
        $config->templates = $templates;
        
        //On transforme l'objet modifié en JSON
        $json = json_encode($config);
        
        //On écrase le fichier de configuration
        file_put_contents("../laplace_config.json", $json);
        
        return new JsonResponse('Les modèles ont été correctement enregistrés !', 200);
    }
    
    /**
     * Renvoie le fichier de configuration des modèles en JSON.
     * 
     * @param type $admin
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getConfigAction($admin, Request $request){
        if(!$admin) return;
        
        //On charge le fichier de configuration
        $file = fopen("../laplace_config.json", "r") or die("Unable to open file!");
        $read = fread($file,filesize("../laplace_config.json"));
        fclose($file);
        
        //On le transforme en objet
        $config = json_decode($read);
        
        //On transforme la partie des templates en JSON
        $json = json_encode($config->templates);
        $dico = json_encode(DataDictionary::data());
        
        return new JsonResponse(array( 'templates' => $json, 'dico' => $dico ), 200);
    }
    
    /**
     * Charge le formulaire que l'utilisateur désire, et le modifie en fonction
     * des données du fichier de configuration avant de l'envoyer.
     * 
     * @param type $admin
     * @param \Symfony\Component\HttpFoundation\Request $request
     */
    public function generatePDF($admin, Request $request){
        
    }
    
    /**
     * Crée les miniatures pour chaque page d'un pdf.
     * 
     * @param type $path_to_pdf Chemin vers le pdf
     * @param type $path_to_thumbnails Chemin vers la miniature du pdf
     * 
     * @return Le nombre de pages qui a été créé
     */
    private function pdfToThumbnail($path_to_pdf, $path_to_thumbnails){
        
        //On supprime le contenu du dossier s'il existe déjà
        foreach (glob($path_to_thumbnails . "/*.png") as $png) {
            unlink($png);
        }
        
        //Puis on supprime le dossier s'il existe déjà
        if(file_exists($path_to_thumbnails)){
            rmdir($path_to_thumbnails);
        }
        
        //Puis on recrée le dossier
        mkdir($path_to_thumbnails);
        
        //On charge le pdf
        $im = new Imagick($path_to_pdf);
        
        $npages = $im->getnumberimages();
        
        //On transforme tout le pdf en png
        for ($i = 0; $i < $npages; $i++){
            $im->setIteratorIndex($i);
        
            //On compresse un peu l'image
            $im->setCompression(Imagick::COMPRESSION_LZW);
            $im->setCompressionQuality(90);

            //On crée l'image
            $im->writeImage($path_to_thumbnails . '/' . $i . '.png');
        }
        
        return $npages;
    }
}
