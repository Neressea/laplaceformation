<?php

namespace Laplace\TrainingBundle\Controller;

use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Validator\Constraints as Assert;

use Laplace\CommonBundle\Controller\AbstractBaseController;

use Laplace\UserBundle\Form\UserSelectorType;

use Laplace\TrainingBundle\Entity\Thread;
use Laplace\TrainingBundle\Entity\Message;
use Laplace\TrainingBundle\Form\MessageType;
use Laplace\TrainingBundle\Form\ThreadType;

/**
 * Contrôleur de la partie 'Discussions'.
 */
class ThreadController extends AbstractBaseController
{
    public function viewAllAction($admin)
    {
        $mngr   = $this->getDoctrine()->getManager();
        $user   = $this->getUser();

        if ($admin) {
            $threads = $mngr
                ->getRepository('LaplaceTrainingBundle:Thread')
                ->fetchAll();
        } else {
            $threads = $mngr
                ->getRepository('LaplaceTrainingBundle:Thread')
                ->fetchByParticipantAndReference($user, null);
        }

        return $this->render(
            'LaplaceTrainingBundle:Thread:view-all.html.twig',
            array(
                'threads'   => $threads,
                'admin'     => $admin,
            )
        );
    }

    public function viewAction($admin, Thread $thread = null)
    {
        $this->check($thread);

        $mngr = $this->getDoctrine()->getManager();

        // on vérifie que l'utilisateur fait bien partie des participants
        $participants = $mngr
            ->getRepository('LaplaceTrainingBundle:Thread')
            ->fetchParticipantsIdentity($thread);

        $user       = $this->getUser();
        $allowed    = $admin;

        foreach ($participants as &$participant) {
            if ($participant->getId() == $user->getId()) {
                $allowed = true;
            }
        }

        if (!$allowed) {
            throw new AccessDeniedHttpException(
                'Vous ne pouvez pas voir cette discussion.'
            );
        }

        // récupération des messages avec l'identité des auteurs
        // on pourrait récupérer les messages avec $thread->getMessages()
        // mais il faudrait émettre de nouvelles requêtes pour récupérer les
        // identités des auteurs
        $messages = $mngr
            ->getRepository('LaplaceTrainingBundle:Thread')
            ->fetchMessages($thread);

        $delete_msg_forms = array();
        foreach ($messages as &$msg_info) {
            $msg        = $msg_info->getMessage();
            $author     = $msg_info->getAuthor();

            if ($admin || $user->getId() == $author->getId()) {
                $delete_msg_forms[$msg->getId()] = $this
                    ->createDeleteMessageForm($msg, $admin)
                    ->createView();
            } else {
                $delete_msg_forms[$msg->getId()] = null;
            }
        }

        // paramètres de la vue
        $parameters = array(
            'thread'            => $thread,
            'participants'      => $participants,
            'messages'          => $messages,
            'delete_msg_forms'  => $delete_msg_forms,
            'admin'             => $admin,
        );

        //
        // Gestion du formulaire permettant l'ajout d'un message
        //

        $message = null;
        if ($this->doPostMessageForm($parameters, 'message_form', $message)) {
            $user_url = $this->generateUrl(
                'laplace_training_view_thread',
                array(
                    'id' => $thread->getId(),
                ),
                true
            );
            $admin_url = $this->generateUrl(
                'laplace_training_adm_view_thread',
                array(
                    'id' => $thread->getId(),
                ),
                true
            );
            $url = $admin ? $admin_url : $user_url;

            // notification
            $this->get('laplace.alert')->notice(
                $admin_url,
                'Nouvelle réponse de %s dans la discussion "%s".',
                $this->getUser()->getFullName(),
                $thread->getTitle()
            );

            // email
            $this->get('laplace.mailer')->send(
                // from
                $this->getUser(),
                // to
                $admin ? $thread->getParticipants()->toArray() : 'CORRESP_FORM',
                // subject
                'Nouvelle réponse',
                // body
                $this->renderView(
                    'LaplaceTrainingBundle:Mail:new-message.html.twig',
                    array(
                        'thread' => $thread,
                        'message' => $message,
                        // ATTENTION à l'inversion ici !
                        'url' => $admin ? $user_url : $admin_url
                    )
                )
            );

            // redirection
            return $this->redirect($url);
        }

        return $this->render(
            'LaplaceTrainingBundle:Thread:view.html.twig',
            $parameters
        );
    }

    /**
     * Cette action est appelée lorsqu'une personne souhaite supprimer un
     * message. Elle n'affiche aucune page mais redirige vers le fil de
     * discussion associé au message supprimé.
     */
    public function deleteMessageAction($admin, Message $message = null)
    {
        $mngr = $this->getDoctrine()->getManager();

        if ($message == null) {
            throw $this->createNotFoundException(
                'Ce message n\'existe pas.'
            );
        }

        $thread = $message->getThread();

        if (!$admin) {
            if ($message->getAuthor() != $this->getUser()) {
                throw new AccessDeniedHttpException(
                    'Vous ne pouvez pas supprimer ce message.'
                );
            }
        }

        $admin_url = $this->generateUrl(
            'laplace_training_adm_view_thread',
            array(
                'id' => $thread->getId()
            )
        );
        $user_url = $this->generateUrl(
            'laplace_training_view_thread',
            array(
                'id' => $thread->getId()
            )
        );
        $url = $admin ? $admin_url : $user_url;

        $form = $this->createDeleteMessageForm($message, $admin);

        $form->handleRequest($this->get('request'));
        if ($form->isValid()) {
            $mngr->remove($message);
            $mngr->flush();

            $this->flashInfo('Le message a été supprimé.');

            // notification
            $this->get('laplace.alert')->notice(
                $admin_url,
                '%s a supprimé un de ses messages '.
                'dans la discussion "%s".',
                $this->getUser()->getFullName(),
                $thread->getTitle()
            );
        } else {
            $this->flashError('Erreur lors de la suppression du message.');
        }

        return $this->redirect($url);
    }

    /**
     * Page "Modifier une discussion" (administrateur).
     */
    public function editAction(Thread $thread = null)
    {
        $this->check($thread);

        $mngr = $this->getDoctrine()->getManager();

        // on récupère les participants
        $participants = $mngr
            ->getRepository('LaplaceTrainingBundle:Thread')
            ->fetchParticipantsIdentity($thread);

        $parameters = array(
            'thread'        => $thread,
            'participants'  => $participants,
        );

        $url = $this->generateUrl(
            'laplace_training_adm_edit_thread',
            array(
                'id' => $thread->getId(),
            )
        );

        // Formulaire d'édition
        if ($this->doEditThreadForm($parameters, 'edit_form')) {
            return $this->redirect($url);
        }

        // Formulaire d'ajout de participant
        if ($this->doAddParticipantForm($parameters, 'add_participant_form')) {
            return $this->redirect($url);
        }

        // Formulaire de retrait de participant
        if ($this->doRemoveParticipantForm($parameters, 'rem_participant_form')) {
            return $this->redirect($url);
        }

        return $this->render(
            'LaplaceTrainingBundle:Thread:edit.html.twig',
            $parameters
        );
    }

    /**
     * Page "Supprimer une discussion" (administrateur).
     */
    public function deleteAction(Thread $thread = null)
    {
        $this->check($thread);

        $parameters = array(
            'thread' => $thread,
        );

        if ($thread->getNeed() != null) {
            $url = $this->generateUrl(
                'laplace_training_adm_view_need',
                array(
                    'id' => $thread->getNeed()->getId(),
                )
            );
        } elseif ($thread->getRequest() != null) {
            $url = $this->generateUrl(
                'laplace_training_adm_view_request',
                array(
                    'id' => $thread->getRequest()->getId(),
                )
            );
        } else {
            $url = $this->generateUrl('laplace_training_adm_threads');
        }

        // Formulaire de suppression
        if ($this->doDeleteThreadForm($parameters, 'delete_form')) {
            return $this->redirect($url);
        }

        return $this->render(
            'LaplaceTrainingBundle:Thread:delete.html.twig',
            $parameters
        );
    }

    /**********************************************************************/

    private function check(Thread $thread = null)
    {
        if ($thread == null) {
            throw $this->createNotFoundException(
                    'Cette discussion n\'existe pas.'
            );
        }
    }

    private function createDeleteMessageForm(Message $message, $admin)
    {
        $builder = $this->createFormBuilder(array());
        $builder->setAction(
            $this->generateUrl(
                $admin ?
                    'laplace_training_adm_delete_message' :
                    'laplace_training_delete_message',
                array(
                    'id' => $message->getId(),
                )
            )
        );
        $builder->add('do', 'submit',
            array(
                'label' => 'Supprimer',
            )
        );

        return $builder->getForm();
    }

    private function doPostMessageForm(&$parameters, $form_name, &$message)
    {
        $mngr   = $this->getDoctrine()->getManager();
        $user   = $this->getUser();

        $thread = $parameters['thread'];

        // données initiales du formulaire
        $data = array(
            'message' => new Message(),
        );

        // construction du formulaire
        $builder = $this->createNamedFormBuilder($form_name, $data);
        $builder->add('message', new MessageType());
        $builder->add('Envoyer', 'submit');

        // validation
        $form = $builder->getForm();
        if ($this->isNamedFormSubmitted($form_name, $form)) {
            if ($form->isValid()) {
                $data = $form->getData();

                $message = $data['message'];
                $message->setThread($thread);
                $message->setAuthor($user);

                $mngr->persist($message);
                $mngr->flush();

                $this->flashInfo('Le message a été enregistré.');

                return true;
            } else {
                $this->flashFormError();
            }
        }

        // échec de la validation : création de la vue
        $parameters['message_form'] = $form->createView();

        return false;
    }

    private function doEditThreadForm(&$parameters, $form_name)
    {
        $mngr = $this->getDoctrine()->getManager();

        $thread = $parameters['thread'];

        // données initiales du formulaire
        $data = array(
            'thread' => $thread,
        );

        // construction du formulaire
        $builder = $this->createNamedFormBuilder($form_name, $data);
        $builder->add('thread', new ThreadType());
        $builder->add('Envoyer', 'submit');

        // validation
        $form = $builder->getForm();
        if ($this->isNamedFormSubmitted($form_name, $form)) {
            if ($form->isValid()) {
                $mngr->persist($thread);
                $mngr->flush();

                $this->flashInfo('La discussion a été mise à jour.');

                return true;
            } else {
                $this->flashFormError();
            }
        }

        // échec de la validation : création de la vue
        $parameters[$form_name] = $form->createView();

        return false;
    }

    private function doAddParticipantForm(&$parameters, $form_name)
    {
        $mngr = $this->getDoctrine()->getManager();

        $thread = $parameters['thread'];

        // données initiales du formulaire
        $data = array(
            'participant' => null,
        );

        // construction du formulaire
        $builder = $this->createNamedFormBuilder($form_name, $data);
        $builder->add('participant', new UserSelectorType(),
            array(
                'em' => $mngr,
                'label' => 'Participant',
            )
        );
        $builder->add('Envoyer', 'submit');

        // validation
        $form = $builder->getForm();
        if ($this->isNamedFormSubmitted($form_name, $form)) {
            if ($form->isValid()) {
                $data = $form->getData();

                $participant = $data['participant'];
                if ($thread->getParticipants()->contains($participant)) {
                    $this->flashInfo(
                        '"%s" participe déjà à la discussion "%s".',
                        $participant->getFullName(),
                        $thread->getTitle()
                    );
                } else {
                    $thread->addParticipant($participant);
                    $mngr->flush();

                    $this->flashInfo(
                        '"%s" a été ajouté(e) à la discussion "%s".',
                        $participant->getFullName(),
                        $thread->getTitle()
                    );
                }

                return true;
            } else {
                $this->flashFormError();
            }
        }

        // échec de la validation : création de la vue
        $parameters[$form_name] = $form->createView();

        return false;
    }

    private function doRemoveParticipantForm(&$parameters, $form_name)
    {
        $mngr = $this->getDoctrine()->getManager();

        $thread         = $parameters['thread'];
        $participants   = $parameters['participants'];

        // données initiales du formulaire
        $data = array(
            'participant' => null,
        );

        if (empty($participants)) {
            $parameters[$form_name] = null;

            return false;
        }

        $choices = array();
        foreach ($participants as $participant) {
            $choices[$participant->getId()] = $participant->getFullName();
        }

        // construction du formulaire
        $builder = $this->createNamedFormBuilder($form_name, $data);
        $builder->add('participant', 'choice',
            array(
                'choices'   => $choices,
                'expanded'  => true,
                'label'     => 'Choisir une personne',
            )
        );
        $builder->add('do', 'submit',
            array(
                'label' => 'Retirer',
            )
        );

        // validation
        $form = $builder->getForm();
        if ($this->isNamedFormSubmitted($form_name, $form)) {
            if ($form->isValid()) {
                $data = $form->getData();

                $participant = $mngr->find(
                    'LaplaceUserBundle:User',
                    $data['participant']
                );

                $thread->removeParticipant($participant);
                $mngr->flush();

                $this->flashInfo(
                    '"%s" ne participe plus à la discussion "%s".',
                    $participant->getFullName(),
                    $thread->getTitle()
                );

                return true;
            } else {
                $this->flashFormError();
            }
        }

        // échec de la validation : création de la vue
        $parameters[$form_name] = $form->createView();

        return false;
    }

    private function doDeleteThreadForm(&$parameters, $form_name)
    {
        $mngr = $this->getDoctrine()->getManager();

        $thread = $parameters['thread'];

        // données initiales du formulaire
        $data = array(
            'delete' => false,
        );

        // construction du formulaire
        $builder = $this->createNamedFormBuilder($form_name, $data);
        $builder->add('delete', 'checkbox',
            array(
                'constraints'   => new Assert\True(),
                'label'         => 'Supprimer la discussion',
            )
        );
        $builder->add('do', 'submit',
            array(
                'label' => 'Supprimer',
            )
        );

        // validation
        $form = $builder->getForm();
        if ($this->isNamedFormSubmitted($form_name, $form)) {
            if ($form->isValid()) {
                // Suppression
                $mngr->getConnection()->beginTransaction();
                try {
                    $mngr->getRepository('LaplaceTrainingBundle:Thread')
                        ->deleteThreads(array($thread));

                    $mngr->flush();
                    $mngr->getConnection()->commit();

                    $this->flashInfo(
                        'La discussion "%s" a été supprimée.',
                        $thread->getTitle()
                    );
                } catch (Exception $e) {
                    $mngr->getConnection()->rollback();
                    $mngr->close();

                    if ($e instanceOf DBALException) {
                        $this->flashError(
                            'Erreur lors de la suppression ' .
                            'de la discussion [%s]',
                            $e->getMessage()
                        );
                    } else {
                        throw $e;
                    }
                }

                return true;
            } else {
                $this->flashFormError();
            }
        }

        // échec de la validation : création de la vue
        $parameters[$form_name] = $form->createView();

        return false;
    }

}
