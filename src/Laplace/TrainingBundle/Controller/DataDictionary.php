<?php

namespace Laplace\TrainingBundle\Controller;
use Laplace\UserBundle\Entity\User;
use Laplace\TrainingBundle\Entity\Request;
use Laplace\UserBundle\Entity\InfoAgent;
use Laplace\UserBundle\Entity\InfoDoctorant;
use Laplace\UserBundle\Form\IdentityType;
use Laplace\UserBundle\Form\UserType;
use Laplace\UserBundle\Form\InfoAgentType;
use Laplace\UserBundle\Form\InfoDoctorantType;


/**
 * Fait la relation entre les puces de données et les textes affichés
 *
 * @author albert
 */
class DataDictionary{
    
    /**
     * Utilisateur qui sera utilisé par l'objet.
     * 
     * @var type 
     */
    private $user;
    
    /**
     * Demande de formation.
     * 
     * @var type 
     */
    private $training;
    
    /**
     * Gestionnaire de base de données.
     * @var type 
     */
    private $mngr;
    
    /**
     * Tableau associant chaque nom de donnée à la fonction retournant le résultat.
     * Quand on accèdera aux champs du tableau, on aura accès aux fonctions à partir de leur nom.
     * Les clés seront utilisés comme texte des puces de données.
     * 
     * @var type 
     */
    private static $datatable = array(
        'nom' => 'getName',
        'prenom' => 'getFirstName',
        'sexe' => 'getGender',
        'date actuelle' => 'getCurrentDate',
        'tel' => 'getPhone',
        'email' => 'getEmail',
        'type' => 'getType',
        'tutelle' => 'getTutelle',
        'site agent' => 'getUserSite',
        'titre formation' => 'getTrainingTitle',
        'description' => 'getDescription',
        'date formation' => 'getTrainingDate',
        'durée formation' => 'getTrainingDuration',
        'catégorie formation' => 'getCategory',
        'domaine formation' => 'getDomain'
    );
    
    /**
     * Constructeur de la classe
     * 
     * @param User $user Utilisateur utilisé par l'objet
     */
    public function __construct(User $user, Request $training, $manager){
        $this->user = $user;
        $this->training = $training;
        $this->mngr = $manager;
    }
    
    /**
     * Renvoie toutes les clés du tableau principal (nom des puces de données).
     * 
     * @return type
     */
    public static function data(){
        return array_keys(DataDictionary::$datatable);
    }
    
    /**
     * Renvoie la donnée dont le nom est passé en paramètre pour l'utilisateur passé dans 
     * le constructeur.
     * 
     * @param type $data_name Nom de la donnée recherchée.
     * @return null | String texte à afficher sur le formulaire.
     */
    public function get($data_name){
        
        //Si le nom de la donnée n'est pas une clé du tableau, on renvoie null
        if(!array_key_exists($data_name, DataDictionary::$datatable)) return null;
        
        //Autrement on renvoie le réulstat de la fonction associée à la donnée
        $func_name = DataDictionary::$datatable[$data_name];
        return $this->$func_name();
    }
    
    private function getName(){
        return $this->user->getName();
    }
    
    private function getFirstName(){
        return $this->user->getFirstName();
    }
    
    private function getGender(){
        return ($this->user->getGender() == 0) ? 'Homme' : 'Femme';
    }
    
    private function getCurrentDate(){
        return date('d/m/Y', time());
    }
    
    private function getPhone(){
        return $this->user->getTelephoneNumber1();
    }
    
    private function getEmail(){
        return $this->user->getEmailAddress();
    }
    
    private function getType(){
        return $this->mngr->getRepository('LaplaceTrainingBundle:RequestSubscription')->fetchOneByUserAndRequest($this->user, $this->training)->getType()->getName();
    }
    
    private function getTutelle(){
        return $this->user->getTutelle()->getNom();
    }
    
    private function getUserSite(){
        $this->user->getUserInfo();
        return $this->user->getSite()->getName();
    }
    
    private function getTrainingTitle(){
        return $this->training->getTitle();
    }
    
    private function getDescription(){
        $desc = $this->training->getDescription();
        return $desc != null ? $desc : 'Aucune description.';
    }
    
    private function getTrainingDate(){
        return $this->training->getTrainingDate()->format('d/m/Y');
    }
    
    private function getTrainingDuration(){
        return $this->training->getTrainingDuration();
    }
    
    private function getCategory(){
        return $this->training->getCategory()->getName();
    }
    
    private function getDomain(){
        return $this->training->getCategory()->getDomain()->getName();
    }
}
