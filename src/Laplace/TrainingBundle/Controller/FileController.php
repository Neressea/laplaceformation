<?php

namespace Laplace\TrainingBundle\Controller;

use Laplace\CommonBundle\Controller\AbstractBaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use ZipArchive;
use RecursiveIteratorIterator;
use RecursiveDirectoryIterator;

/**
 * Description of File
 *
 * @author albert
 */
class FileController extends AbstractBaseController{
    
    /**
     * Retourne le chemin vers les fichiers utilisateurs, en fonction du type d'utilisateur.
     * @param $admin Booléen indiquant si l'utilisateur est admin ou non
     * @param $username Nom de l'utilisateur
     */
    private function getPath($admin){
        $user = $this->getUser();
        $username = $user->getUsername();
        
        return '../userfiles' . ($admin ? '' : '/' . $username) . '/' ;
    }
    
    public function viewAllAction($admin, Request $request){
        $parameters = array();
        $parameters['admin'] = $admin;
        $parameters['profile'] = $this->getUser();
        
        //on crée un formulaire pour uploader les fichiers
        $form = $this->createFormBuilder()->getForm();
        $form->handleRequest($request);
        
        //On récupère les fichiers de l'utilisateur et on l'envoie en JSON
        $path = $this->getPath($admin);
        
        //Si l'utilisateur n'a pas encore de dossier, on lui en crée un.
        if(!is_dir($path))
            mkdir($path);
        
        $files = $this->exploreUserFiles($path);
        $parameters['files'] = $this->filesToJSON($files, $path);
            
        if ($form->isValid()) {
            $file = $request->files->get('file');
            $subpath = $request->get('path');
            
            //Ensuite on déplace le fichier dans son dossier.
            $file->move($path . $subpath, $file->getClientOriginalName());
            
            $info = pathinfo($path . $subpath . $file->getClientOriginalName());
            
            //On renvoie le nouveau fichier
            $json_file = $this->fileToJSON($info);
            return new JsonResponse($json_file, 200);
        }
        
        $parameters['form'] = $form->createView();
        
        return $this->render(
            'LaplaceTrainingBundle:File:view-all.html.twig',
            $parameters
        );
    }
    
    public function deleteAction($admin, Request $request){
        //files est un tableau contenant le nom de tous les fichiers
        $files = $request->request->get('files');
        $subpath = $request->request->get('subpath');
      
        foreach ($files as $file){
            $path = $this->getPath($admin) . $subpath . $file;
            $this->deleteDirectory($path);
        }

        return new JsonResponse('L\'élément a bien été supprimé', 200);
    }
    
    function downloadAction($admin, $files, $current_path){
        //files est une chaine contenant tous les fichiers spéarés par |
        $files = split('\|', $files);
        
        $to_send = null;
        
        //Si on n'a aucun fichier, ben on fait rien
        if(count($files) == 0) return;
        
        $path = $this->getPath($admin);
        
        //Si un seul fichier est demandé, on l'envoie direct .
        if(count($files) == 1 && !is_dir($path . $current_path . $files[0])){
            $to_send = $path . $current_path . $files[0];
            header('Content-Type: ' . mime_content_type($to_send));
        }else{
            //Sinon, on crée une archive.
            header('Content-Type: application/zip');
            $zipname = 'laplace-formation-fichiers.zip';
            $zip = new ZipArchive();
            $zip->open($zipname, ZipArchive::OVERWRITE);
            
            //On supprime le fichier s'il existe déjà

            //On crée une archive avec tous les fichiers
            foreach ($files as $file){
                //On enlève le path du nom afin de ne pas reproduire tout le chemin danss le zip quand ce n'est pas nécessaire.
                $newname = str_replace($current_path, '', $file);
                $total_path = $path . $current_path . $file;
                
                if(is_dir($total_path)){
                    $iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($total_path), RecursiveIteratorIterator::LEAVES_ONLY);
                    foreach ($iterator as $name => $f) {
                        $new_filename = substr($name,strrpos($name,'/') + 1);
                        $zip->addFile($f, $new_filename);
                    }
                }else{
                    $zip->addFile($total_path, $newname);
                }
            }

            $zip->close();
            $to_send = $zipname;
        }
        
        //On met en place les headers pour télécharger les fichiers 
        error_reporting(0); //Errors may corrupt download
        ob_start();
        header('Content-Description: File Transfer');
        header('Content-Disposition: inline; filename='.$to_send);
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Length: ' . filesize($to_send));
        ob_clean();   
        ob_end_flush();
        
        readfile($to_send);
    }
    
    function createFolderAction($admin, Request $request){
        $name = $request->get('folder_name');
        $path = $request->get('path');
        
        //on reconstitue le chemin
        $total_path = $this->getPath($admin) . $path . '/' . $name;
        
        //on crée le dossier
        mkdir($total_path);
        
        //On retourne le json du nouveau dossier
        $json = '{"name": "' . $name . '", "type": "dir", "children": []}';
        return new JsonResponse($json, 200);
    }
    
    function renameAction($admin, Request $request){
        $path = $request->get('path');
        $old = $request->get('old_name');
        $new = $request->get('new_name');
        
        //On déplace le fichier
        rename($this->getPath($admin) . $path . $old, $this->getPath($admin) . $path . $new);
        
        return new JsonResponse('ok.', 200);
    }
    
    function deleteDirectory($dir) {
        if (!file_exists($dir)) {
            return true;
        }

        if (!is_dir($dir)) {
            return unlink($dir);
        }

        foreach (scandir($dir) as $item) {
            if ($item == '.' || $item == '..') {
                continue;
            }

            if (!$this->deleteDirectory($dir . DIRECTORY_SEPARATOR . $item)) {
                return false;
            }

        }

        return rmdir($dir);
    }
    
    public function fileToJSON($info){
        $type = 'image';
        if($info['extension'] == 'pdf'){
            $type = 'pdf';
        }
        return '{"type":"' . $type . '", "name":"' . $info['filename'] . '.' . $info['extension'] . '", "children":[]}';
    }
    
    public function filesToJSON($files, $path){
        $json = '[';
        
        //On supprime les fichirs inutiles
        unset($files[array_search('.', $files)]);
        unset($files[array_search('..', $files)]);
        
        $i = count($files);
        
        foreach($files as $file){
            //On supprime les données inutiles
            $file_path = $path . '/' . $file;
                
            $info = pathinfo($file_path);

            if(is_dir($file_path)){
                $json .= '{"type":"dir", "name":"' . $info['filename'] . '", "children" : ';

                //On parcourt tous les enfants
                $subfiles = scandir($file_path);

                $json .= $this->filesToJSON($subfiles, $file_path);

                $json .= '}';
            }elseif(is_file($file_path)){
                if($info['extension'] == 'pdf'){
                    $json .= '{"type":"pdf", "name":"' . $info['filename'] . '.'. $info['extension'] .'", "children" : []}';
                }else{
                    //On a une image
                    $json .= '{"type":"image", "name":"' . $info['filename'] . '.'. $info['extension'] . '", "children" : []}';
                }
            }
            
            if(--$i){
                $json .= ',';
            }
        }
        
        return $json . ']';
    }
    
    public function exploreUserFiles($username){     
        
        //Si le dossier de l'utilisateur existe, on le renvoie sous forme de tableau
        if(($folder = scandir('../userfiles/' . $username)) != false){
            return $folder;
        }
        
        return null;
    }
}
