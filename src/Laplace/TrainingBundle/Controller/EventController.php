<?php

namespace Laplace\TrainingBundle\Controller;

use Symfony\Component\HttpFoundation\Response;

use Laplace\CommonBundle\Controller\AbstractBaseController;
use Laplace\TrainingBundle\Entity\Event;

/**
 * Contrôleur de la partie 'Historique' d'un utilisateur.
 */
class EventController extends AbstractBaseController
{

    /**
     * Affiche la section "Historique complet" d'une année.
     *
     * @param  int|null $year Une année ou null pour l'année actuelle
     * @return Response
     */
    public function fullHistoryAction($year)
    {
        return $this->render(
            'LaplaceTrainingBundle:Event:full-history.html.twig',
            $this->getHistory($year, null)
        );
    }

    /**
     * Affiche la section "Historique des besoins" d'une année.
     *
     * @param  int|null $year Une année ou null pour l'année actuelle
     * @return Response
     */
    public function needHistoryAction($year)
    {
        return $this->render(
            'LaplaceTrainingBundle:Event:need-history.html.twig',
            $this->getHistory($year, Event::MASK_NEED)
        );
    }

    /**
     * Affiche la section "Historique des demandes" d'une année.
     *
     * @param  int|null $year Une année ou null pour l'année actuelle
     * @return Response
     */
    public function requestHistoryAction($year)
    {
        return $this->render(
            'LaplaceTrainingBundle:Event:request-history.html.twig',
            $this->getHistory($year, Event::MASK_REQUEST)
        );
    }

    /**
     * Récupère l'historique de l'utilisateur pour une année donnée. Si l'année
     * n'est pas spécifiée, l'année actuelle est prise en compte.
     *
     * @param  int|null $year
     * @param  int|null $type Un masque de type (constante `Event::MASK_*`)
     * @return array
     * @see Event
     */
    private function getHistory($year, $type)
    {
        $mngr   = $this->getDoctrine()->getManager();
        $user   = $this->getUser();

        $oldest = $mngr
            ->getRepository('LaplaceTrainingBundle:Event')
            ->getEarliestUserEventDate($user);

        // année actuelle
        $current = intval(date('Y'));

        // année demandée
        if ($year == null) {
            $requested = $current;
        } else {
            $requested = intval($year);
        }

        // année de l'évènement le plus ancien
        if ($oldest == null) {
            $start = $current;
        } else {
            $date = $oldest;
            $start = intval($date->format('Y'));
        }

        // récupération des événements
        $events = $mngr
            ->getRepository('LaplaceTrainingBundle:Event')
            ->fetchByUserAndYear($user, $requested, $type);

        return array(
            'events'    => $events,
            'years'     => range($start, $current),
            'requested' => $requested,
        );
    }

}
