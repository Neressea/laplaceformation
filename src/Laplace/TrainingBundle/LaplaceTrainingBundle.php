<?php

namespace Laplace\TrainingBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Ce bundle regroupe tout ce qui est relatif aux formations (besoins, demandes,
 * inscriptions, discussions associées, etc).
 */
class LaplaceTrainingBundle extends Bundle
{
}
