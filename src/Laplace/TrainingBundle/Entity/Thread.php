<?php

namespace Laplace\TrainingBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

use Laplace\UserBundle\Entity\User;

/**
 * Représente un fil de discussion attaché à une formation ou à un besoin.
 *
 * @ORM\Entity(repositoryClass="Laplace\TrainingBundle\Entity\ThreadRepository")
 * @ORM\Table(name="`thread`")
 */
class Thread
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Laplace\UserBundle\Entity\User", fetch="LAZY")
     * @ORM\JoinColumn(nullable=false)
     */
    private $author;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\DateTime()
     */
    private $date;

    /**
     * @ORM\Column(type="string", length=128)
     * @Assert\NotBlank()
     * @Assert\Length(max=128)
     */
    private $title;

    /**
     * @ORM\ManyToOne(targetEntity="Laplace\TrainingBundle\Entity\Need")
     */
    private $need;

    /**
     * @ORM\ManyToOne(targetEntity="Laplace\TrainingBundle\Entity\Request")
     */
    private $request;

    /**
     * @ORM\ManyToMany(targetEntity="Laplace\UserBundle\Entity\User", fetch="EXTRA_LAZY")
     */
    private $participants;

    /**
     * @ORM\OneToMany(targetEntity="Laplace\TrainingBundle\Entity\Message", mappedBy="thread", fetch="EXTRA_LAZY")
     */
    private $messages;

    public function __construct(User $author = null, $title = null)
    {
        $this->author           = $author;
        $this->title            = $title;
        $this->date             = new \DateTime();
        $this->participants     = new ArrayCollection();
        $this->messages         = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param  \DateTime $date
     * @return Thread
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set title
     *
     * @param  string $title
     * @return Thread
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set author
     *
     * @param  \Laplace\UserBundle\Entity\User $author
     * @return Thread
     */
    public function setAuthor(\Laplace\UserBundle\Entity\User $author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \Laplace\UserBundle\Entity\User
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set need
     *
     * @param  \Laplace\TrainingBundle\Entity\Need $need
     * @return Thread
     */
    public function setNeed(\Laplace\TrainingBundle\Entity\Need $need = null)
    {
        $this->need = $need;

        return $this;
    }

    /**
     * Get need
     *
     * @return \Laplace\TrainingBundle\Entity\Need
     */
    public function getNeed()
    {
        return $this->need;
    }

    /**
     * Set request
     *
     * @param  \Laplace\TrainingBundle\Entity\Request $request
     * @return Thread
     */
    public function setRequest(\Laplace\TrainingBundle\Entity\Request $request = null)
    {
        $this->request = $request;

        return $this;
    }

    /**
     * Get request
     *
     * @return \Laplace\TrainingBundle\Entity\Request
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * Add participants
     *
     * @param  \Laplace\UserBundle\Entity\User $participants
     * @return Thread
     */
    public function addParticipant(\Laplace\UserBundle\Entity\User $participants)
    {
        $this->participants[] = $participants;

        return $this;
    }

    /**
     * Remove participants
     *
     * @param \Laplace\UserBundle\Entity\User $participants
     */
    public function removeParticipant(\Laplace\UserBundle\Entity\User $participants)
    {
        $this->participants->removeElement($participants);
    }

    /**
     * Get participants
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getParticipants()
    {
        return $this->participants;
    }

    /**
     * Add messages
     *
     * @param  \Laplace\TrainingBundle\Entity\Message $messages
     * @return Thread
     */
    public function addMessage(\Laplace\TrainingBundle\Entity\Message $messages)
    {
        $this->messages[] = $messages;

        return $this;
    }

    /**
     * Remove messages
     *
     * @param \Laplace\TrainingBundle\Entity\Message $messages
     */
    public function removeMessage(\Laplace\TrainingBundle\Entity\Message $messages)
    {
        $this->messages->removeElement($messages);
    }

    /**
     * Get messages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMessages()
    {
        return $this->messages;
    }
}
