<?php

namespace Laplace\TrainingBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;

use Laplace\UserBundle\Entity\User;
use Laplace\UserBundle\Entity\UserRepository;
use Laplace\UserBundle\Model\UserIdentityImpl;
use Laplace\TrainingBundle\Model\SubscriptionInfo;

class NeedSubscriptionRepository extends EntityRepository
{

    /**
     * Recherche une inscription à un besoin à partir d'un utilisateur et d'un
     * besoin.
     * Retourne null si aucune inscription correspondante n'a été trouvée.
     *
     * @param  User                  $user
     * @param  Need                  $need
     * @return NeedSubscription|null
     */
    public function fetchOneByUserAndNeed(User $user, Need $need)
    {
        $query_builder = $this->createQueryBuilder('sub');
        $query_builder
            ->innerJoin('sub.type', 'type')
            ->addSelect('type')
            ->where('sub.user = :user')
            ->andWhere('sub.need = :need')
            ->setParameter('user', $user)
            ->setParameter('need', $need);

        try {
            return $query_builder->getQuery()->getSingleResult();
        } catch (NoResultException $e) {
            return null;
        }
    }

    /**
     * Recherche toutes les inscriptions à un besoin d'un utilisateur.
     *
     * @param  User               $user
     * @return NeedSubscription[]
     */
    public function fetchByUser(User $user)
    {
        $query_builder = $this->createQueryBuilder('sub');
        $query_builder
            ->innerJoin('sub.need', 'need')
            ->innerJoin('sub.type', 'type')
            ->leftJoin('need.category', 'cat')
            ->leftJoin('cat.domain', 'dom')
            ->addSelect('need')
            ->addSelect('type')
            ->addSelect('cat')
            ->addSelect('dom')
            ->addOrderBy('sub.subscriptionDate', 'DESC')
            ->where('sub.user = :user')
            ->setParameter('user', $user);

        return $query_builder->getQuery()->getResult();
    }

    /**
     * Recherche toutes les inscriptions associées à un besoin.
     *
     * @param  Need               $need
     * @return SubscriptionInfo[]
     */
    public function fetchByNeed(Need $need)
    {
        $query_builder = $this->createQueryBuilder('sub');
        $query_builder
            ->innerJoin('sub.user', 'user')
            ->innerJoin('sub.type', 'type')
            ->addSelect('type')
            ->where('sub.need = :need')
            ->setParameter('need', $need);

        UserRepository::addSelect($query_builder, 'user', false);

        $results        = $query_builder->getQuery()->getResult();
        $subscriptions  = array();

        foreach ($results as &$result) {
            $subscription   = $result[0];
            $user           = new UserIdentityImpl($result);

            $subscriptions[] = new SubscriptionInfo($subscription, $user);
        }

        return $subscriptions;
    }

    public function deleteByNeed(Need $need)
    {
        $this->_em
            ->createQueryBuilder()
            ->delete('LaplaceTrainingBundle:NeedSubscription', 'sub')
            ->where('sub.need = :need')
            ->setParameter('need', $need)
            ->getQuery()
            ->execute();
    }

    public function deleteByUser(User $user)
    {
        $this->_em
            ->createQueryBuilder()
            ->delete($this->_entityName, 'sub')
            ->where('sub.user = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->execute();
    }

}
