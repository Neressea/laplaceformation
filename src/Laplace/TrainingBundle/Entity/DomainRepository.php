<?php

namespace Laplace\TrainingBundle\Entity;

use Doctrine\ORM\EntityRepository;

class DomainRepository extends EntityRepository
{

    /**
     * Retourne tous les domaines classés par code. Les catégories sont
     * également récupérées, classées par code.
     *
     * @return Domain[]
     */
    public function fetchAll()
    {
        $builder = $this
            ->createQueryBuilder('dom')
            ->leftJoin('dom.categories', 'cat')
            ->addSelect('cat')
            ->addOrderBy('dom.code')
            ->addOrderBy('cat.code');

        return $builder->getQuery()->getResult();
    }

}
