<?php

namespace Laplace\TrainingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Une catégorie appartient à un domaine.
 *
 * @ORM\Entity
 * @ORM\Table(name="`category`")
 * @UniqueEntity(fields="name")
 */
class Category
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=128, unique=true)
     * @Assert\NotBlank()
     * @Assert\Length(max=128)
     */
    private $name;

    /**
     * Les codes sont des nombres quelconques utilisés pour ordonner les
     * catégories. Ils peuvent être modifiés par le correspondant formation.
     *
     * @ORM\Column(type="smallint")
     */
    private $code;

    /**
     * @ORM\ManyToOne(targetEntity="Laplace\TrainingBundle\Entity\Domain", inversedBy="categories", fetch="EAGER")
     * @ORM\JoinColumn(nullable=false)
     */
    private $domain;

    public function __construct(
        $name = null,
        Domain $domain = null,
        $code = null
    ) {
        $this->name     = $name;
        $this->code     = $code;
        $this->domain   = $domain;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param  string   $name
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set domain
     *
     * @param  \Laplace\TrainingBundle\Entity\Domain $domain
     * @return Category
     */
    public function setDomain(\Laplace\TrainingBundle\Entity\Domain $domain)
    {
        $this->domain = $domain;

        return $this;
    }

    /**
     * Get domain
     *
     * @return \Laplace\TrainingBundle\Entity\Domain
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * Set code
     *
     * @param  integer  $code
     * @return Category
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return integer
     */
    public function getCode()
    {
        return $this->code;
    }
}
