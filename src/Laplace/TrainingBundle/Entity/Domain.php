<?php

namespace Laplace\TrainingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Un domaine est un ensemble de catégories.
 *
 * @ORM\Entity(repositoryClass="Laplace\TrainingBundle\Entity\DomainRepository")
 * @ORM\Table(name="`domain`")
 * @UniqueEntity(fields="name")
 */
class Domain
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=128, unique=true)
     * @Assert\NotBlank()
     * @Assert\Length(max=128)
     */
    private $name;

    /**
     * Les codes sont des nombres quelconques utilisés pour ordonner les
     * domaines. Ils peuvent être modifiés par le correspondant formation.
     *
     * @ORM\Column(type="smallint")
     */
    private $code;

    /**
     * @ORM\OneToMany(targetEntity="Laplace\TrainingBundle\Entity\Category", mappedBy="domain")
     */
    private $categories;

    public function __construct($name = null, $code = null)
    {
        $this->name         = $name;
        $this->code         = $code;
        $this->categories   = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param  string $name
     * @return Domain
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add categories
     *
     * @param  \Laplace\TrainingBundle\Entity\Category $categories
     * @return Domain
     */
    public function addCategorie(\Laplace\TrainingBundle\Entity\Category $categories)
    {
        $this->categories[] = $categories;

        return $this;
    }

    /**
     * Remove categories
     *
     * @param \Laplace\TrainingBundle\Entity\Category $categories
     */
    public function removeCategorie(\Laplace\TrainingBundle\Entity\Category $categories)
    {
        $this->categories->removeElement($categories);
    }

    /**
     * Get categories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Set code
     *
     * @param  integer $code
     * @return Domain
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return integer
     */
    public function getCode()
    {
        return $this->code;
    }
}
