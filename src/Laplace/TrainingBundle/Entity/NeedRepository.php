<?php

namespace Laplace\TrainingBundle\Entity;

use Doctrine\ORM\EntityRepository;

use Laplace\UserBundle\Entity\User;
use Laplace\TrainingBundle\Model\NeedInfo;

class NeedRepository extends EntityRepository
{

    public function findOneById($id)
    {
        $query_builder = $this->createQueryBuilder('need');
        $query_builder
                ->leftJoin('need.category', 'cat')
                ->leftJoin('cat.domain', 'dom')
                ->addSelect('cat')
                ->addSelect('dom')
                ->where('need.id = :id')
                ->orderBy('need.title')
                ->setParameter('id', $id);

        return $query_builder->getQuery()->getSingleResult();
    }

    /**
     * Récupère tous les besoins ainsi que le nombre d'inscriptions pour
     * chacun.
     *
     * @return NeedInfo[]
     */
    public function fetchAll()
    {
        $query = $this->_em->createQuery(
            'SELECT need AS _need, COUNT(subs) AS _count ' .
            'FROM LaplaceTrainingBundle:Need need '.
            'LEFT JOIN need.subscriptions subs '.
            'GROUP BY need'
        );

        $needs      = array();
        $results    = $query->getResult();

        foreach ($results as &$result) {
            $need   = $result['_need'];
            $count  = $result['_count'];

            $needs[] = new NeedInfo($need, $count);
        }

        return $needs;
    }

    public function deleteNeeds($needs)
    {
        $mngr = $this->_em;

        // Suppression
        $mngr->getConnection()->beginTransaction();
        try {
            foreach ($needs as &$need) {
                // threads and messages
                $mngr->getRepository('LaplaceTrainingBundle:Thread')
                    ->deleteByNeed($need);


                // events
                $mngr->getRepository('LaplaceTrainingBundle:Event')
                    ->deleteByNeed($need);

                // subscriptions
                $mngr->getRepository('LaplaceTrainingBundle:NeedSubscription')
                    ->deleteByNeed($need);

                // need
                $mngr->remove($need);
            }

            // flush & commit
            $mngr->flush();
            $mngr->getConnection()->commit();

        } catch (Exception $e) {
            $mngr->getConnection()->rollback();
            $mngr->close();

            throw $e;
        }
    }

    public function replaceAuthor(User $old, User $new)
    {
        $query = $this->_em->createQuery(
            'UPDATE LaplaceTrainingBundle:Need need '.
            'SET need.author = :new '.
            'WHERE need.author = :old'
        );
        $query->setParameter('old', $old);
        $query->setParameter('new', $new);

        return $query->execute();
    }

}
