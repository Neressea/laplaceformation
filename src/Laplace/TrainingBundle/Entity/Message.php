<?php

namespace Laplace\TrainingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Représente un commentaire sur une formation ou sur un besoin (cf entités
 * TrainingComment et NeedComment).
 *
 * @ORM\Entity(repositoryClass="Laplace\TrainingBundle\Entity\MessageRepository")
 * @ORM\Table(name="`message`")
 */
class Message
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Laplace\TrainingBundle\Entity\Thread", inversedBy="messages", fetch="LAZY")
     * @ORM\JoinColumn(nullable=false)
     */
    private $thread;

    /**
     * @ORM\ManyToOne(targetEntity="Laplace\UserBundle\Entity\User", fetch="LAZY")
     * @ORM\JoinColumn(nullable=false)
     */
    private $author;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\DateTime()
     */
    private $date;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     */
    private $text;

    public function __construct()
    {
        $this->date = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param  \DateTime $date
     * @return Message
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set text
     *
     * @param  string  $text
     * @return Message
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set thread
     *
     * @param  \Laplace\TrainingBundle\Entity\Thread $thread
     * @return Message
     */
    public function setThread(\Laplace\TrainingBundle\Entity\Thread $thread)
    {
        $this->thread = $thread;

        return $this;
    }

    /**
     * Get thread
     *
     * @return \Laplace\TrainingBundle\Entity\Thread
     */
    public function getThread()
    {
        return $this->thread;
    }

    /**
     * Set author
     *
     * @param  \Laplace\UserBundle\Entity\User $author
     * @return Message
     */
    public function setAuthor(\Laplace\UserBundle\Entity\User $author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \Laplace\UserBundle\Entity\User
     */
    public function getAuthor()
    {
        return $this->author;
    }
}
