<?php

namespace Laplace\TrainingBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;

use Laplace\UserBundle\Entity\User;
use Laplace\UserBundle\Entity\UserRepository;
use Laplace\UserBundle\Model\UserIdentityImpl;
use Laplace\TrainingBundle\Entity\Need;
use Laplace\TrainingBundle\Entity\Request;
use Laplace\TrainingBundle\Model\ThreadInfo;
use Laplace\TrainingBundle\Model\MessageInfo;

class ThreadRepository extends EntityRepository
{

    public function findOneById($id)
    {
        $query_builder = $this->createQueryBuilder('thread');
        $query_builder
            ->innerJoin('thread.author', 'author')
            ->leftJoin('thread.participants', 'participants')
            ->leftJoin('thread.need', 'need')
            ->leftJoin('thread.request', 'request')
            ->addSelect('author')
            ->addSelect('need')
            ->addSelect('request')
            ->where('thread.id = :id')
            ->setParameter('id', $id);

        try {
            return $query_builder->getQuery()->getSingleResult();
        } catch (NoResultException $e) {
            return null;
        }
    }

    /**
     * Recherche des fils de discussion, classés du plus récent au moins récent.
     * L'identité des auteurs est jointe directement.
     *
     * Si $participant est donné, les résultats sont limités aux discussions où
     * cet utilisateur figure dans la liste des participants.
     *
     * Si $reference est donnée, la recherche est limitée à ce besoin ou à
     * cette demande de formation.
     *
     * @param  User|null         $participant
     * @param  Need|Request|null $reference
     * @return ThreadInfo[]
     */
    public function fetchByParticipantAndReference($participant, $reference)
    {
        $query_builder = $this->createQueryBuilder('thread');
        $query_builder
            ->innerJoin('thread.author', 'author')
            ->addOrderBy('thread.date', 'DESC');

        UserRepository::addSelect($query_builder, 'author', false);

        // première condition : sur $user
        if ($participant != null) {
            $query_builder
                ->leftJoin('thread.participants', 'participants')
                ->andWhere('participants = :user')
                ->setParameter('user', $participant);
        }

        // deuxième condition : sur $reference
        if ($reference instanceOf Need) {
            $query_builder
                ->andWhere('thread.need = :ref')
                ->setParameter('ref', $reference);
        } elseif ($reference instanceOf Request) {
            $query_builder
                ->andWhere('thread.request = :ref')
                ->setParameter('ref', $reference);
        }

        $results   = $query_builder->getQuery()->getResult();
        $threads   = array();

        foreach ($results as &$result) {
            $thread = $result[0];
            $author = new UserIdentityImpl($result);

            $threads[] = new ThreadInfo($thread, $author);
        }

        return $threads;
    }

    /**
     * Retourne toutes les discussions, de la plus récente à la moins récente,
     * avec les identités des auteurs.
     *
     * @return ThreadInfo[]
     */
    public function fetchAll()
    {
        return $this->fetchByParticipantAndReference(null, null);
    }

    /**
     * Retourne, pour une discussion donnée, l'identité de tous
     * les participants.
     *
     * @param  Thread             $thread
     * @return UserIdentityImpl[]
     */
    public function fetchParticipantsIdentity($thread)
    {
        $query_builder = $this->_em->createQueryBuilder();
        $query_builder
            ->from($this->_entityName, 'thread')
            ->innerJoin('thread.participants', 'participants')
            ->where('thread = ?1')
            ->setParameter(1, $thread);

        UserRepository::addSelect($query_builder, 'participants', false);

        $results        = $query_builder->getQuery()->getScalarResult();
        $participants   = array();

        foreach ($results as &$result) {
            $participants[] = new UserIdentityImpl($result);
        }

        return $participants;
    }

    /**
     * Retourne tous les messages associés à la discussion demandée ainsi que
     * les identités des auteurs des messages.
     *
     * @param  Thread        $thread
     * @return MessageInfo[]
     */
    public function fetchMessages(Thread $thread)
    {
        $query_builder = $this->_em->createQueryBuilder();
        $query_builder
            ->select('msg')
            ->from('LaplaceTrainingBundle:Message', 'msg')
            ->innerJoin('msg.thread', 'thread')
            ->innerJoin('msg.author', 'author')
            ->where('thread = ?1')
            ->orderBy('msg.date')
            ->setParameter(1, $thread);

        UserRepository::addSelect($query_builder, 'author', false);

        $results    = $query_builder->getQuery()->getResult();
        $messages   = array();

        foreach ($results as &$result) {
            $message    = $result[0];
            $author     = new UserIdentityImpl($result);

            $messages[] = new MessageInfo($message, $author);
        }

        return $messages;
    }

    /**
     * Supprime un ensemble de discussions.
     *
     * @param  Thread[] $threads
     * @return int
     */
    public function deleteThreads($threads)
    {
        if (empty($threads))
            return 0;

        $this->_em
            ->createQueryBuilder()
            ->delete('LaplaceTrainingBundle:Message', 'msg')
            ->where('msg.thread IN (:threads)')
            ->setParameter('threads', $threads)
            ->getQuery()
            ->execute();

        foreach ($threads as &$thread) {
            $this->_em->remove($thread);
        }
    }

    public function deleteByNeed(Need $need)
    {
        $query_builder = $this->createQueryBuilder('thread');
        $query_builder->select('thread.id');
        $query_builder->where('thread.need = ?1');
        $query_builder->setParameter(1, $need);

        $results   = $query_builder->getQuery()->getScalarResult();
        $threads   = array();

        foreach ($results as $result) {
            $threads[] = $this->_em->getPartialReference(
                $this->_entityName,
                $result['id']
            );
        }

        $this->deleteThreads($threads);
    }

    public function deleteByRequest(Request $request)
    {
        $query_builder = $this->createQueryBuilder('thread');
        $query_builder->select('thread.id');
        $query_builder->where('thread.request = ?1');
        $query_builder->setParameter(1, $request);

        $results   = $query_builder->getQuery()->getScalarResult();
        $threads   = array();

        foreach ($results as $result) {
            $threads[] = $this->_em->getPartialReference(
                $this->_entityName,
                $result['id']
            );
        }

        $this->deleteThreads($threads);
    }

    public function deleteByUser(User $user)
    {
        $this->_em
            ->createQueryBuilder()
            ->delete($this->_entityName, 'thread')
            ->where('thread.author = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->execute();
    }

    public function replaceAuthor(User $old, User $new)
    {
        $query = $this->_em->createQuery(
            'UPDATE LaplaceTrainingBundle:Thread thread '.
            'SET thread.author = :new '.
            'WHERE thread.author = :old'
        );
        $query->setParameter('old', $old);
        $query->setParameter('new', $new);

        return $query->execute();
    }

}
