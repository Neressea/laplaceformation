<?php

namespace Laplace\TrainingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

use Laplace\UserBundle\Entity\User;

/**
 * Cette entité représente une demande de formation. Une demande de formation
 * peut être proposée par le correspondant formation ou un utilisateur.
 * Elle est soumise à la validation du correspondant formation.
 * Tout comme pour un besoin, la catégorie et le type sont définis par le
 * correspondant formation.
 * Le correspondant formation peut clore la demande pour interdire de nouvelles
 * inscriptions. Une inscription à une demande de formation est représentée
 * par l'entité RequestSubscription.
 *
 * @ORM\Entity(repositoryClass="Laplace\TrainingBundle\Entity\RequestRepository")
 * @ORM\Table(name="`request`")
 */
class Request
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Laplace\UserBundle\Entity\User", fetch="LAZY")
     * @ORM\JoinColumn(nullable=false)
     */
    private $author;

    /**
     * Date à laquelle la demande a été enregistrée dans la base de données.
     *
     * @ORM\Column(type="datetime")
     * @Assert\DateTime()
     */
    private $issueDate;

    /**
     * @ORM\Column(type="string", length=128)
     * @Assert\NotBlank()
     * @Assert\Length(max=128)
     */
    private $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="Laplace\TrainingBundle\Entity\Category", fetch="LAZY")
     * @ORM\JoinColumn(nullable=true)
     */
    private $category;

    /**
     * @ORM\OneToMany(targetEntity="Laplace\TrainingBundle\Entity\RequestSubscription", mappedBy="request", fetch="EXTRA_LAZY")
     */
    private $subscriptions;

    /**
     * Indique si le correspondant formation a validé ou non cette demande.
     *
     * @ORM\Column(type="boolean")
     */
    private $validated;

    /**
     * Indique si le correspondant formation a clos cette demande. Si c'est le
     * cas, il n'est plus possible de s'inscrire à cette formation.
     *
     * @ORM\Column(type="boolean")
     */
    private $close;

    /**
     * Date à laquelle le correspondant formation a traité cette demande.
     *
     * @ORM\Column(type="datetime", nullable=true)
     * @Assert\DateTime()
     */
    private $processingDate;

    /**
     * Date à laquelle la formation aura lieu.
     *
     * @ORM\Column(type="datetime", nullable=true)
     * @Assert\DateTime()
     */
    private $trainingDate;

    /**
     * Durée de la formation en nombre d'heures.
     *
     * @ORM\Column(type="smallint", nullable=true)
     * @Assert\GreaterThanOrEqual(value=0)
     */
    private $trainingDuration;

    /**
     * Indique la date limite pour s'inscrire à la demande de formation. Cet
     * attribut est utilisé à titre indicatif. En effet, les membres peuvent
     * s'inscrire aux demandes de formation a posteriori.
     *
     * @ORM\Column(type="datetime", nullable=true)
     * @Assert\DateTime()
     */
    private $subscriptionDeadline;

    /**
     * Nombre de places disponibles. Cette information est utilisée à titre
     * indicatif.
     *
     * @ORM\Column(type="smallint", nullable=true)
     * @Assert\GreaterThanOrEqual(value=0)
     */
    private $availablePlacesCount;

    /**
     * @ORM\Column(type="string", length=128, nullable=true)
     * @Assert\Length(max=128)
     */
    private $agentsPrioritaires;

    /**
     * Financement.
     *
     * @ORM\Column(type="string", length=128, nullable=true)
     * @Assert\Length(max=128)
     */
    private $funding;

    public function __construct(User $author = null, $title = null)
    {
        $this->author               = $author;
        $this->title                = $title;
        $this->issueDate            = new \DateTime();
        $this->subscriptions        = new ArrayCollection();
        $this->validated            = false;
        $this->close                = false;
    }

    // Méthodes auto-générées

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set issueDate
     *
     * @param  \DateTime $issueDate
     * @return Request
     */
    public function setIssueDate($issueDate)
    {
        $this->issueDate = $issueDate;

        return $this;
    }

    /**
     * Get issueDate
     *
     * @return \DateTime
     */
    public function getIssueDate()
    {
        return $this->issueDate;
    }

    /**
     * Set title
     *
     * @param  string  $title
     * @return Request
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param  string  $description
     * @return Request
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set validated
     *
     * @param  boolean $validated
     * @return Request
     */
    public function setValidated($validated)
    {
        $this->validated = $validated;

        return $this;
    }

    /**
     * Get validated
     *
     * @return boolean
     */
    public function getValidated()
    {
        return $this->validated;
    }

    /**
     * Set close
     *
     * @param  boolean $close
     * @return Request
     */
    public function setClose($close)
    {
        $this->close = $close;

        return $this;
    }

    /**
     * Get close
     *
     * @return boolean
     */
    public function getClose()
    {
        return $this->close;
    }

    /**
     * Set processingDate
     *
     * @param  \DateTime $processingDate
     * @return Request
     */
    public function setProcessingDate($processingDate)
    {
        $this->processingDate = $processingDate;

        return $this;
    }

    /**
     * Get processingDate
     *
     * @return \DateTime
     */
    public function getProcessingDate()
    {
        return $this->processingDate;
    }

    /**
     * Set trainingDate
     *
     * @param  \DateTime $trainingDate
     * @return Request
     */
    public function setTrainingDate($trainingDate)
    {
        $this->trainingDate = $trainingDate;

        return $this;
    }

    /**
     * Get trainingDate
     *
     * @return \DateTime
     */
    public function getTrainingDate()
    {
        return $this->trainingDate;
    }

    /**
     * Set trainingDuration
     *
     * @param  integer $trainingDuration
     * @return Request
     */
    public function setTrainingDuration($trainingDuration)
    {
        $this->trainingDuration = $trainingDuration;

        return $this;
    }

    /**
     * Get trainingDuration
     *
     * @return integer
     */
    public function getTrainingDuration()
    {
        return $this->trainingDuration;
    }

    /**
     * Set subscriptionDeadline
     *
     * @param  \DateTime $subscriptionDeadline
     * @return Request
     */
    public function setSubscriptionDeadline($subscriptionDeadline)
    {
        $this->subscriptionDeadline = $subscriptionDeadline;

        return $this;
    }

    /**
     * Get subscriptionDeadline
     *
     * @return \DateTime
     */
    public function getSubscriptionDeadline()
    {
        return $this->subscriptionDeadline;
    }

    /**
     * Set availablePlacesCount
     *
     * @param  integer $availablePlacesCount
     * @return Request
     */
    public function setAvailablePlacesCount($availablePlacesCount)
    {
        $this->availablePlacesCount = $availablePlacesCount;

        return $this;
    }

    /**
     * Get availablePlacesCount
     *
     * @return integer
     */
    public function getAvailablePlacesCount()
    {
        return $this->availablePlacesCount;
    }

    /**
     * Set agentsPrioritaires
     *
     * @param  string  $agentsPrioritaires
     * @return Request
     */
    public function setAgentsPrioritaires($agentsPrioritaires)
    {
        $this->agentsPrioritaires = $agentsPrioritaires;

        return $this;
    }

    /**
     * Get agentsPrioritaires
     *
     * @return string
     */
    public function getAgentsPrioritaires()
    {
        return $this->agentsPrioritaires;
    }

    /**
     * Set funding
     *
     * @param  string  $funding
     * @return Request
     */
    public function setFunding($funding)
    {
        $this->funding = $funding;

        return $this;
    }

    /**
     * Get funding
     *
     * @return string
     */
    public function getFunding()
    {
        return $this->funding;
    }

    /**
     * Set author
     *
     * @param  \Laplace\UserBundle\Entity\User $author
     * @return Request
     */
    public function setAuthor(\Laplace\UserBundle\Entity\User $author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \Laplace\UserBundle\Entity\User
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set category
     *
     * @param  \Laplace\TrainingBundle\Entity\Category $category
     * @return Request
     */
    public function setCategory(\Laplace\TrainingBundle\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Laplace\TrainingBundle\Entity\Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Add subscriptions
     *
     * @param  \Laplace\TrainingBundle\Entity\RequestSubscription $subscriptions
     * @return Request
     */
    public function addSubscription(\Laplace\TrainingBundle\Entity\RequestSubscription $subscriptions)
    {
        $this->subscriptions[] = $subscriptions;

        return $this;
    }

    /**
     * Remove subscriptions
     *
     * @param \Laplace\TrainingBundle\Entity\RequestSubscription $subscriptions
     */
    public function removeSubscription(\Laplace\TrainingBundle\Entity\RequestSubscription $subscriptions)
    {
        $this->subscriptions->removeElement($subscriptions);
    }

    /**
     * Get subscriptions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubscriptions()
    {
        return $this->subscriptions;
    }
}
