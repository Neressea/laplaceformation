<?php

namespace Laplace\TrainingBundle\Entity;

use Doctrine\ORM\EntityRepository;

use Laplace\UserBundle\Entity\User;
use Laplace\TrainingBundle\Model\RequestInfo;

class RequestRepository extends EntityRepository
{

    public function findOneById($id)
    {
        $query_builder = $this->createQueryBuilder('request');
        $query_builder
            ->leftJoin('request.category', 'cat')
            ->leftJoin('cat.domain', 'dom')
            ->addSelect('cat')
            ->addSelect('dom')
            ->where('request.id = :id')
            ->orderBy('request.title')
            ->setParameter('id', $id);

        return $query_builder->getQuery()->getSingleResult();
    }

    /**
     * Récupère toutes les requêtes ainsi que le nombre d'inscriptions pour
     * chacune.
     * Si $validated est non nul, seules les demandes dont le champ validated
     * correspond à la valeur booléenne spécifiée sont récupérés.
     *
     * @param  bool|null     $validated
     * @return RequestInfo[]
     */
    public function fetchAll($validated)
    {
        $where = !is_null($validated);

        $dql = array();
        $dql[] = 'SELECT request AS _request, COUNT(subs) AS _count';
        $dql[] = 'FROM LaplaceTrainingBundle:Request request';
        $dql[] = 'LEFT JOIN request.subscriptions subs';

        if ($where) {
            $dql[] = 'WHERE request.validated = :validated';
        }

        $dql[] = 'GROUP BY request';

        $query = $this->_em->createQuery(implode(' ', $dql));

        if ($where) {
            $query->setParameter('validated', $validated);
        }

        $requests   = array();
        $results    = $query->getResult();

        foreach ($results as &$result) {
            $request    = $result['_request'];
            $count      = $result['_count'];

            $requests[] = new RequestInfo($request, $count);
        }

        return $requests;
    }

    public function deleteRequests($requests)
    {
        $mngr = $this->_em;

        // Suppression
        $mngr->getConnection()->beginTransaction();
        try {
            foreach ($requests as &$request) {
                // threads and messages
                $mngr->getRepository('LaplaceTrainingBundle:Thread')
                    ->deleteByRequest($request);

                // events
                $mngr->getRepository('LaplaceTrainingBundle:Event')
                    ->deleteByRequest($request);

                // subscriptions
                $mngr->getRepository('LaplaceTrainingBundle:RequestSubscription')
                    ->deleteByRequest($request);

                // request
                $mngr->remove($request);
            }

            // flush & commit
            $mngr->flush();
            $mngr->getConnection()->commit();

        } catch (Exception $e) {
            $mngr->getConnection()->rollback();
            $mngr->close();

            throw $e;
        }
    }

    public function replaceAuthor(User $old, User $new)
    {
        $query = $this->_em->createQuery(
            'UPDATE LaplaceTrainingBundle:Request request '.
            'SET request.author = :new '.
            'WHERE request.author = :old'
        );
        $query->setParameter('old', $old);
        $query->setParameter('new', $new);

        return $query->execute();
    }

}
