<?php

namespace Laplace\TrainingBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;

use Laplace\UserBundle\Entity\User;
use Laplace\TrainingBundle\Entity\Need;
use Laplace\TrainingBundle\Entity\Request;

class EventRepository extends EntityRepository
{

    /**
     * Recherche l'événement le plus récent étant donné un ensemble de
     * conditions (toutes optionnelles).
     * Les événements renvoyés respectent toutes les conditions spécifiées.
     *
     *  - $conditions['user'] (User) : l'utilisateur associé à l'événement
     *  - $conditions['type'] (int) : le type d'événement
     *  - $conditions['types'] (array) : une liste de types possibles
     *  - $conditions['mask'] : un masque de type
     *  - $conditions['reference'] : l'objet de référence relatif à l'événement
     *  - $conditions['max_results'] : nombre maximum de résultats
     *
     * @param  array      $conditions
     * @return Event|null
     */
    public function fetchBy($conditions)
    {
        $user = isset($conditions['user']) ?
            $conditions['user'] : null;
        $type = isset($conditions['type']) ?
            $conditions['type'] : null;
        $types = isset($conditions['types']) ?
            $conditions['types'] : array();
        $mask = isset($conditions['mask']) ?
            $conditions['mask'] : null;
        $reference = isset($conditions['reference']) ?
            $conditions['reference'] : null;
        $max_results = isset($conditions['max_results']) ?
            $conditions['max_results'] : null;

        $query_builder = $this->createQueryBuilder('evt');
        $query_builder->orderBy('evt.date', 'DESC');

        if ($user !== null) {
            $query_builder->andWhere('evt.user = :user');
            $query_builder->setParameter('user', $user);
        }

        if ($type !== null) {
            $query_builder->andWhere('evt.type = :type');
            $query_builder->setParameter('type', $type);
        }

        if (!empty($types)) {
            $query_builder->andWhere('evt.type IN (:types)');
            $query_builder->setParameter('types', $types);
        }

        if ($mask !== null) {
            $query_builder->andWhere('MOD(evt.type, 16) = :mask');
            $query_builder->setParameter('mask', $mask);

        }

        if ($reference !== null) {
            $query_builder->andWhere('evt.referenceId = :ref');
            $query_builder->setParameter('ref', $reference->getId());
        }

        if ($max_results !== null) {
            $query_builder->setMaxResults($max_results);
        }

        return $query_builder->getQuery()->getResult();
    }

    /**
     * Recherche tous les événements relatifs à un utilisateur, sur une période
     * donnée (une année) avec éventuellement une contrainte sur le type.
     * Si $type_mask est null, il n'y a aucun filtrage sur le type. Les valeurs
     * possibles pour $type_mask sont les valeurs des constantes Event::MASK_*.
     *
     * @param  User     $user
     * @param  int      $year
     * @param  int|null $type_mask
     * @return Event[]
     */
    public function fetchByUserAndYear(User $user, $year, $type_mask)
    {
        // calcul de la période
        // (on procède ainsi car il n'y a pas d'opérateur YEAR portable)
        $new_years_day = new \DateTime();
        $new_years_day->setDate($year, 1, 1);
        $new_years_day->setTime(0, 0, 0);

        $new_years_eve = clone $new_years_day;
        $new_years_eve->add(new \DateInterval('P1Y'));

        // préparation de la requête
        $query_builder = $this->createQueryBuilder('evt');
        $query_builder
            ->orderBy('evt.date', 'ASC')
            ->where('evt.user = :user')
            ->andWhere('evt.date >= :start')
            ->andWhere('evt.date < :end')
            ->setParameter('user', $user)
            ->setParameter('start', $new_years_day)
            ->setParameter('end', $new_years_eve);

        if ($type_mask !== null) {
            $query_builder->andWhere('MOD(evt.type, 16) = :mask');
            $query_builder->setParameter('mask', $type_mask);
        }

        return $query_builder->getQuery()->getResult();
    }

    /**
     *
     * @param  Need|Request $reference
     * @return Event[]
     */
    public function fetchByReference($reference)
    {
        $query_builder = $this->createQueryBuilder('evt');
        $query_builder
            ->where('evt.referenceId = :id')
            ->andWhere('MOD(evt.type, 16) = :mask');

        $query_builder->setParameter('id', $reference->getId());

        if ($reference instanceOf Need) {
            $query_builder->setParameter('mask', Event::MASK_NEED);
        } elseif ($reference instanceOf Request) {
            $query_builder->setParameter('mask', Event::MASK_REQUEST);
        }

        return $query_builder->getQuery()->getResult();
    }

    /**
     * Cherche l'année de l'événement le plus ancien pour un utilisateur donné.
     *
     * @param  User           $user
     * @return \DateTime|null
     */
    public function getEarliestUserEventDate(User $user)
    {
        $query_builder = $this->createQueryBuilder('evt');
        $query_builder->where('evt.user = ?1');
        $query_builder->select('MIN(evt.date)');

        $query_builder->setParameter(1, $user);

        try {
            $date = $query_builder->getQuery()->getSingleScalarResult();

            return new \DateTime($date);
        } catch (NoResultException $e) {
            return null;
        }
    }

    public function deleteEvents($events)
    {
        foreach ($events as &$event) {
            $this->_em->remove($event);
        }
    }

    public function deleteByNeed(Need $need)
    {
        $this->deleteEvents($this->fetchByReference($need));
    }

    public function deleteByRequest(Request $request)
    {
        $this->deleteEvents($this->fetchByReference($request));
    }

}
