<?php

namespace Laplace\TrainingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

use Laplace\UserBundle\Entity\User;

/**
 * Cette entité représente les besoins qui peuvent précéder une demande de
 * formation. Les entités Besoin et Formation partagent certains attributs en
 * commun mais un besoin est moins précis qu'une formation.
 * La catégorie et le type d'un besoin sont définis par le correspondant
 * formation. L'utilisateur ne peut donc pas éditer ces attributs.
 * Un besoin n'est pas soumis à la validation du correspondant formation.
 * Une inscription à un besoin est représentée par l'entité NeedSubscription.
 *
 * @ORM\Entity(repositoryClass="Laplace\TrainingBundle\Entity\NeedRepository")
 * @ORM\Table(name="`need`")
 * @UniqueEntity(fields="title")
 */
class Need
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Laplace\UserBundle\Entity\User", fetch="LAZY")
     * @ORM\JoinColumn(nullable=false)
     */
    private $author;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\DateTime()
     */
    private $issueDate;

    /**
     * @ORM\Column(type="string", length=128, unique=true)
     * @Assert\NotBlank()
     * @Assert\Length(max=128)
     */
    private $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="Laplace\TrainingBundle\Entity\Category", fetch="LAZY")
     * @ORM\JoinColumn(nullable=true)
     */
    private $category;

    /**
     * @ORM\OneToMany(targetEntity="Laplace\TrainingBundle\Entity\NeedSubscription", mappedBy="need", fetch="EXTRA_LAZY")
     */
    private $subscriptions;

    public function __construct(User $author = null, $title = null)
    {
        $this->author               = $author;
        $this->title                = $title;
        $this->issueDate            = new \DateTime();
        $this->subscriptions        = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set issueDate
     *
     * @param  \DateTime $issueDate
     * @return Need
     */
    public function setIssueDate($issueDate)
    {
        $this->issueDate = $issueDate;

        return $this;
    }

    /**
     * Get issueDate
     *
     * @return \DateTime
     */
    public function getIssueDate()
    {
        return $this->issueDate;
    }

    /**
     * Set title
     *
     * @param  string $title
     * @return Need
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param  string $description
     * @return Need
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set author
     *
     * @param  \Laplace\UserBundle\Entity\User $author
     * @return Need
     */
    public function setAuthor(\Laplace\UserBundle\Entity\User $author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \Laplace\UserBundle\Entity\User
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set category
     *
     * @param  \Laplace\TrainingBundle\Entity\Category $category
     * @return Need
     */
    public function setCategory(\Laplace\TrainingBundle\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Laplace\TrainingBundle\Entity\Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Add subscriptions
     *
     * @param  \Laplace\TrainingBundle\Entity\NeedSubscription $subscriptions
     * @return Need
     */
    public function addSubscription(\Laplace\TrainingBundle\Entity\NeedSubscription $subscriptions)
    {
        $this->subscriptions[] = $subscriptions;

        return $this;
    }

    /**
     * Remove subscriptions
     *
     * @param \Laplace\TrainingBundle\Entity\NeedSubscription $subscriptions
     */
    public function removeSubscription(\Laplace\TrainingBundle\Entity\NeedSubscription $subscriptions)
    {
        $this->subscriptions->removeElement($subscriptions);
    }

    /**
     * Get subscriptions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubscriptions()
    {
        return $this->subscriptions;
    }
}
