<?php

namespace Laplace\TrainingBundle\Entity;

use Doctrine\ORM\EntityRepository;

use Laplace\UserBundle\Entity\User;

class MessageRepository extends EntityRepository
{

    public function deleteByUser(User $user)
    {
        $this->_em
            ->createQueryBuilder()
            ->delete($this->_entityName, 'msg')
            ->where('msg.author = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->execute();
    }

}
