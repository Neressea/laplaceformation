<?php

namespace Laplace\TrainingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

use Laplace\TrainingBundle\Model\AbstractSubscription;

/**
 * @ORM\Entity(repositoryClass="Laplace\TrainingBundle\Entity\RequestSubscriptionRepository")
 * @ORM\Table(name="`requestsubscription`")
 */
class RequestSubscription extends AbstractSubscription
{

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Laplace\TrainingBundle\Entity\Request", inversedBy="subscriptions")
     */
    private $request;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Laplace\UserBundle\Entity\User")
     */
    private $user;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\DateTime()
     */
    private $subscriptionDate;

    /**
     * @ORM\ManyToOne(targetEntity="Laplace\TrainingBundle\Entity\Type")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\Valid()
     */
    private $type;

    /**
     * true si le correspondant a acceptée la demande de formation,
     * false si le correspondant a refusé la demande,
     * null s'il ne s'est pas encore prononcé.
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $accepted;

    /**
     * true si l'utilisateur a suivi la formation,
     * false si l'utilisateur n'a pas suivi la formation,
     * null s'il n'a encore rien indiqué.
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $attended;

    public function __construct()
    {
        $this->subscriptionDate     = new \DateTime();
        $this->accepted             = null;
        $this->attended             = null;
    }

    // Méthodes auto-générées

    /**
     * Set subscriptionDate
     *
     * @param  \DateTime           $subscriptionDate
     * @return RequestSubscription
     */
    public function setSubscriptionDate($subscriptionDate)
    {
        $this->subscriptionDate = $subscriptionDate;

        return $this;
    }

    /**
     * Get subscriptionDate
     *
     * @return \DateTime
     */
    public function getSubscriptionDate()
    {
        return $this->subscriptionDate;
    }

    /**
     * Set accepted
     *
     * @param  boolean             $accepted
     * @return RequestSubscription
     */
    public function setAccepted($accepted)
    {
        $this->accepted = $accepted;

        return $this;
    }

    /**
     * Get accepted
     *
     * @return boolean
     */
    public function getAccepted()
    {
        return $this->accepted;
    }

    /**
     * Set attended
     *
     * @param  boolean             $attended
     * @return RequestSubscription
     */
    public function setAttended($attended)
    {
        $this->attended = $attended;

        return $this;
    }

    /**
     * Get attended
     *
     * @return boolean
     */
    public function getAttended()
    {
        return $this->attended;
    }

    /**
     * Set request
     *
     * @param  \Laplace\TrainingBundle\Entity\Request $request
     * @return RequestSubscription
     */
    public function setRequest(\Laplace\TrainingBundle\Entity\Request $request)
    {
        $this->request = $request;

        return $this;
    }

    /**
     * Get request
     *
     * @return \Laplace\TrainingBundle\Entity\Request
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * Set user
     *
     * @param  \Laplace\UserBundle\Entity\User $user
     * @return RequestSubscription
     */
    public function setUser(\Laplace\UserBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Laplace\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set type
     *
     * @param  \Laplace\TrainingBundle\Entity\Type $type
     * @return RequestSubscription
     */
    public function setType(\Laplace\TrainingBundle\Entity\Type $type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \Laplace\TrainingBundle\Entity\Type
     */
    public function getType()
    {
        return $this->type;
    }
}
