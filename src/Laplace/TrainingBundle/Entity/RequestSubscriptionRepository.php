<?php

namespace Laplace\TrainingBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;

use Laplace\UserBundle\Entity\User;
use Laplace\UserBundle\Entity\UserRepository;
use Laplace\UserBundle\Model\UserIdentityImpl;
use Laplace\TrainingBundle\Model\SubscriptionInfo;

class RequestSubscriptionRepository extends EntityRepository
{

    /**
     * Recherche une inscription à une demande à partir d'un utilisateur
     * et d'une demande.
     * Retourne null si aucune inscription correspondante n'a été trouvée.
     *
     * @param  User                     $user
     * @param  Request                  $request
     * @return RequestSubscription|null
     */
    public function fetchOneByUserAndRequest(User $user, Request $request)
    {
        $query_builder = $this->createQueryBuilder('sub');
        $query_builder
            ->innerJoin('sub.type', 'type')
            ->addSelect('type')
            ->where('sub.user = :user')
            ->andWhere('sub.request = :request')
            ->setParameter('user', $user)
            ->setParameter('request', $request);

        try {
            return $query_builder->getQuery()->getSingleResult();
        } catch (NoResultException $e) {
            return null;
        }
    }

    /**
     * Recherche toutes les inscriptions à une demande d'un utilisateur.
     *
     * @param  User                  $user
     * @return RequestSubscription[]
     */
    public function fetchByUser(User $user)
    {
        $query_builder = $this->createQueryBuilder('sub');
        $query_builder
            ->innerJoin('sub.request', 'request')
            ->innerJoin('sub.type', 'type')
            ->leftJoin('request.category', 'cat')
            ->leftJoin('cat.domain', 'dom')
            ->addSelect('request')
            ->addSelect('type')
            ->addSelect('cat')
            ->addSelect('dom')
            ->addOrderBy('sub.subscriptionDate', 'DESC')
            ->where('sub.user = :user')
            ->setParameter('user', $user);

        return $query_builder->getQuery()->getResult();
    }

    /**
     * Recherche toutes les inscriptions associées à une demande.
     *
     * @param  Request            $request
     * @return SubscriptionInfo[]
     */
    public function fetchByRequest(Request $request)
    {
        $query_builder = $this->createQueryBuilder('sub');
        $query_builder
            ->innerJoin('sub.user', 'user')
            ->innerJoin('sub.type', 'type')
            ->addSelect('type')
            ->where('sub.request = :request')
            ->setParameter('request', $request);

        UserRepository::addSelect($query_builder, 'user', false);

        $results        = $query_builder->getQuery()->getResult();
        $subscriptions  = array();

        foreach ($results as &$result) {
            $subscription   = $result[0];
            $user           = new UserIdentityImpl($result);

            $subscriptions[] = new SubscriptionInfo($subscription, $user);
        }

        return $subscriptions;
    }

    public function deleteByRequest(Request $request)
    {
        $this->_em
            ->createQueryBuilder()
            ->delete('LaplaceTrainingBundle:RequestSubscription', 'sub')
            ->where('sub.request = :request')
            ->setParameter('request', $request)
            ->getQuery()
            ->execute();
    }

    public function deleteByUser(User $user)
    {
        $this->_em
            ->createQueryBuilder()
            ->delete($this->_entityName, 'sub')
            ->where('sub.user = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->execute();
    }

}
