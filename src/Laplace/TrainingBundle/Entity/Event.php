<?php

namespace Laplace\TrainingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Un événement représente une ligne dans l'historique d'un utilisateur.
 * Un événement est relatif à une formation ou un besoin par exemple. L'objet
 * associé à un événement est référencé par son identifiant. Le type de cet
 * objet dépend du type d'événement.
 * L'entité Evenement possède deux dates, dont une est optionnelle. La première,
 * attribut 'date', représente la date à laquelle l'événement a été créé et
 * enregistré dans la base de données. La seconde, attribut 'referenceDate',
 * est égale à la date de l'objet référencé si c'est possible (date de la
 * formation, ou date d'ajout du besoin, etc).
 *
 * @ORM\Entity(repositoryClass="Laplace\TrainingBundle\Entity\EventRepository")
 * @ORM\Table(name="`event`")
 */
class Event
{
    const MASK_USER                 = 0xA;
    const MASK_NEED                 = 0xB;
    const MASK_REQUEST              = 0xC;

    // Types relatifs au membre du personnel, lui-même
    const USER_ACCOUNT_CREATED      = 0x1A;
    const USER_ACCOUNT_VALIDATED    = 0x2A; // TODO utiliser ces événements
    const USER_ACCOUNT_DISABLED     = 0x3A;

    // Types relatifs à un Besoin
    const NEED_ADDED                = 0x1B;
    const NEED_SATISFIED            = 0x2B;

    // Types relatifs à une Formation
    const REQUEST_ADDED             = 0x1C;
    const REQUEST_ACCEPTED          = 0x2C;
    const REQUEST_DENIED            = 0x3C;
    const REQUEST_ATTENDED          = 0x4C;
    const REQUEST_NOT_ATTENDED      = 0x5C;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Laplace\UserBundle\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * Date à laquelle l'événement est enregistré dans la base de données.
     *
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * Indique le type d'événement.
     *
     * @ORM\Column(type="smallint")
     */
    private $type;

    /**
     * Id d'une entité (besoin, formation, etc.)
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $referenceId;

    /**
     * @ORM\Column(type="text")
     */
    private $message;

    public function __construct()
    {
        $this->date = new \DateTime();
    }

    public function isUserRelated()
    {
        return $this->type % 0x10 == self::MASK_USER;
    }

    public function isNeedRelated()
    {
        return $this->type % 0x10 == self::MASK_NEED;
    }

    public function isRequestRelated()
    {
        return $this->type % 0x10 == self::MASK_REQUEST;
    }

    public static function create($type, $user, $ref = null)
    {
        $event = new Event();
        $event->setUser($user);
        $event->setType($type);

        if ($ref) {
            $event->setReferenceId($ref->getId());
        }

        switch ($type) {
            case Event::USER_ACCOUNT_CREATED:
                $msg = 'Création de votre compte.';
                break;

            case Event::USER_ACCOUNT_VALIDATED:
                $msg = 'Validation de votre compte.';
                break;

            case Event::USER_ACCOUNT_DISABLED:
                $msg = 'Désactivation de votre compte';
                break;

            case Event::NEED_ADDED:
                $msg = sprintf('Ajout du besoin "%s".', $ref->getTitle());
                break;

            case Event::NEED_SATISFIED:
                $msg = sprintf('Le besoin "%s" a été pourvu.',
                    $ref->getTitle()
                );
                break;

            case Event::REQUEST_ADDED:
                $msg = sprintf('Inscription à la demande "%s".',
                    $ref->getTitle()
                );
                break;

            case Event::REQUEST_ACCEPTED:
                $msg = sprintf(
                    'Votre inscription à la demande de formation "%s" '.
                    'a été acceptée.',
                    $ref->getTitle()
                );
                break;

            case Event::REQUEST_DENIED:
                $msg = sprintf(
                    'Votre inscription à la demande de formation "%s" '.
                    'a été refusée.',
                    $ref->getTitle()
                );
                break;

            case Event::REQUEST_ATTENDED:
                $msg = sprintf(
                    'Vous avez indiqué avoir assisté à la formation "%s".',
                    $ref->getTitle()
                );
                break;

            case Event::REQUEST_NOT_ATTENDED:
                $msg = sprintf(
                    'Vous avez indiqué n\'avoir pas assisté '.
                    'à la formation "%s".', $ref->getTitle()
                );
                break;

            default:
                throw new \Exception('Invalid Event type.');
        }

        $event->setMessage($msg);

        return $event;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param  \DateTime $date
     * @return Event
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set type
     *
     * @param  integer $type
     * @return Event
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set referenceId
     *
     * @param  integer $referenceId
     * @return Event
     */
    public function setReferenceId($referenceId)
    {
        $this->referenceId = $referenceId;

        return $this;
    }

    /**
     * Get referenceId
     *
     * @return integer
     */
    public function getReferenceId()
    {
        return $this->referenceId;
    }

    /**
     * Set message
     *
     * @param  string $message
     * @return Event
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set user
     *
     * @param  \Laplace\UserBundle\Entity\User $user
     * @return Event
     */
    public function setUser(\Laplace\UserBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Laplace\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
