<?php

namespace Laplace\TrainingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

use Laplace\TrainingBundle\Model\AbstractSubscription;

/**
 * @ORM\Entity(repositoryClass="Laplace\TrainingBundle\Entity\NeedSubscriptionRepository")
 * @ORM\Table(name="`needsubscription`")
 */
class NeedSubscription extends AbstractSubscription
{

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Laplace\TrainingBundle\Entity\Need", inversedBy="subscriptions")
     */
    private $need;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Laplace\UserBundle\Entity\User")
     */
    private $user;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\DateTime()
     */
    private $subscriptionDate;

    /**
     * @ORM\ManyToOne(targetEntity="Laplace\TrainingBundle\Entity\Type")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\Valid()
     */
    private $type;

    public function __construct()
    {
        $this->subscriptionDate     = new \DateTime();
    }

    /**
     * Set subscriptionDate
     *
     * @param  \DateTime        $subscriptionDate
     * @return NeedSubscription
     */
    public function setSubscriptionDate($subscriptionDate)
    {
        $this->subscriptionDate = $subscriptionDate;

        return $this;
    }

    /**
     * Get subscriptionDate
     *
     * @return \DateTime
     */
    public function getSubscriptionDate()
    {
        return $this->subscriptionDate;
    }

    /**
     * Set need
     *
     * @param  \Laplace\TrainingBundle\Entity\Need $need
     * @return NeedSubscription
     */
    public function setNeed(\Laplace\TrainingBundle\Entity\Need $need)
    {
        $this->need = $need;

        return $this;
    }

    /**
     * Get need
     *
     * @return \Laplace\TrainingBundle\Entity\Need
     */
    public function getNeed()
    {
        return $this->need;
    }

    /**
     * Set user
     *
     * @param  \Laplace\UserBundle\Entity\User $user
     * @return NeedSubscription
     */
    public function setUser(\Laplace\UserBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Laplace\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set type
     *
     * @param  \Laplace\TrainingBundle\Entity\Type $type
     * @return NeedSubscription
     */
    public function setType(\Laplace\TrainingBundle\Entity\Type $type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \Laplace\TrainingBundle\Entity\Type
     */
    public function getType()
    {
        return $this->type;
    }
}
