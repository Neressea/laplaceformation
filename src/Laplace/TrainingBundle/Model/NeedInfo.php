<?php

namespace Laplace\TrainingBundle\Model;

use Laplace\TrainingBundle\Entity\Need;

class NeedInfo
{

    private $_need;
    private $_count;

    public function __construct(Need $need, $subscription_count)
    {
        $this->_need        = $need;
        $this->_count       = $subscription_count;
    }

    public function getNeed()
    {
        return $this->_need;
    }

    public function getSubscriptionCount()
    {
        return $this->_count;
    }

}
