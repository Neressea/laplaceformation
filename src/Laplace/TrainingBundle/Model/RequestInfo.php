<?php

namespace Laplace\TrainingBundle\Model;

use Laplace\TrainingBundle\Entity\Request;

class RequestInfo
{

    private $_request;
    private $_count;

    public function __construct(Request $request, $subscription_count)
    {
        $this->_request     = $request;
        $this->_count       = $subscription_count;
    }

    public function getRequest()
    {
        return $this->_request;
    }

    public function getSubscriptionCount()
    {
        return $this->_count;
    }

}
