<?php

namespace Laplace\TrainingBundle\Model;

use Laplace\UserBundle\Model\AbstractUserIdentity;
use Laplace\TrainingBundle\Entity\Message;

class MessageInfo
{

    private $_message;
    private $_author;

    public function __construct(Message $message, AbstractUserIdentity $author)
    {
        $this->_message = $message;
        $this->_author = $author;
    }

    public function getMessage()
    {
        return $this->_message;
    }

    public function getAuthor()
    {
        return $this->_author;
    }

}
