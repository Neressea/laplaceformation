<?php

namespace Laplace\TrainingBundle\Model;

use Laplace\UserBundle\Model\AbstractUserIdentity;
use Laplace\TrainingBundle\Model\AbstractSubscription;

class SubscriptionInfo
{

    private $_subscription;
    private $_user;

    public function __construct(
            AbstractSubscription $subscription,
            AbstractUserIdentity $user
    ) {
        $this->_subscription    = $subscription;
        $this->_user            = $user;
    }

    public function getSubscription()
    {
        return $this->_subscription;
    }

    public function getUser()
    {
        return $this->_user;
    }

}
