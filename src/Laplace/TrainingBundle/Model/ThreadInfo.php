<?php

namespace Laplace\TrainingBundle\Model;

use Laplace\UserBundle\Model\AbstractUserIdentity;
use Laplace\TrainingBundle\Entity\Thread;

class ThreadInfo
{

    private $_thread;
    private $_author;

    public function __construct(Thread $thread, AbstractUserIdentity $author)
    {
        $this->_thread = $thread;
        $this->_author = $author;
    }

    public function getThread()
    {
        return $this->_thread;
    }

    public function getAuthor()
    {
        return $this->_author;
    }

}
