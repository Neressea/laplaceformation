<?php

namespace Laplace\TrainingBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Laplace\CommonBundle\Form\TriboolToTextTransformer;

use Laplace\TrainingBundle\Form\TypeSelectorType;

class RequestSubscriptionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $accepted = $builder->create('accepted', 'choice',
            array(
                'choices'   => array(
                    'null'      => 'En attente',
                    'true'      => 'Demande acceptée',
                    'false'     => 'Demande refusée'
                ),
                'label'     => 'Etat de la demande',
            )
        );
        $attended = $builder->create('attended', 'choice',
            array(
                'choices'   => array(
                    'null'      => 'Aucune indication',
                    'true'      => 'A assisté à la formation',
                    'false'     => 'N\'a pas assisté à la formation'
                ),
                'label'     => 'Participation',
            )
        );

        $transformer = new TriboolToTextTransformer();

        $accepted->addModelTransformer($transformer);
        $attended->addModelTransformer($transformer);

        $builder->add('type', new TypeSelectorType(),
            array(
                'expanded' => false,
            )
        );
        $builder->add($accepted);
        $builder->add($attended);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' =>
                    'Laplace\TrainingBundle\Entity\RequestSubscription',
            )
        );
    }

    public function getName()
    {
        return 'laplace_trainingbundle_requestsubscriptiontype';
    }
}
