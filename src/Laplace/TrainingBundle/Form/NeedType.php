<?php

namespace Laplace\TrainingBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Représente le type d'entité Need pour la validation de formulaire.
 */
class NeedType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', 'text',
            array(
                'label' => 'Titre',
            )
        );
        $builder->add('description', 'textarea',
            array(
                'label'     => 'Description',
                'required'  => false,
            )
        );
        $builder->add('category', 'entity',
            array(
                'class'     => 'LaplaceTrainingBundle:Category',
                'property'  => 'name',
                'group_by'  => 'domain.name',
                'label'     => 'Catégorie',
                'required'  => false,
            )
        );
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'Laplace\TrainingBundle\Entity\Need',
            )
        );
    }

    public function getName()
    {
        return 'laplace_trainingbundle_needtype';
    }
}
