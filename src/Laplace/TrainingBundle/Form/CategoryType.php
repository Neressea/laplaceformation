<?php

namespace Laplace\TrainingBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Assert;

class CategoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', 'text',
            array(
                'label' => 'Nom de la catégorie',
            )
        );
        $builder->add('code', 'integer',
            array(
                'label' => 'Code',
            )
        );
        $builder->add('domain', 'entity',
            array(
                'class'             => 'LaplaceTrainingBundle:Domain',
                'query_builder'     => function ($er) {
                    return $er->createQueryBuilder('dom')->orderBy('dom.code');
                },
                'property'          => 'name',
                'constraints'       => new Assert\NotNull(),
                'label'             => 'Domaine',
            )
        );
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'Laplace\TrainingBundle\Entity\Category',
            )
        );
    }

    public function getName()
    {
        return 'laplace_trainingbundle_categorytype';
    }
}
