<?php

namespace Laplace\TrainingBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TypeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', 'text',
            array(
                'label' => 'Nom du type',
            )
        );
        $builder->add('description', 'text',
            array(
                'label' => 'Description',
            )
        );
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'Laplace\TrainingBundle\Entity\Type',
            )
        );
    }

    public function getName()
    {
        return 'laplace_trainingbundle_typetype';
    }
}
