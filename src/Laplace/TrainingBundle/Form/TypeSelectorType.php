<?php

namespace Laplace\TrainingBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Assert;

class TypeSelectorType extends AbstractType
{

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class'        => null,
                'class'             => 'LaplaceTrainingBundle:Type',
                'property'          => 'nameWithDescription',
                'query_builder'     => function ($er) {
                    return $er
                        ->createQueryBuilder('type')
                        ->orderBy('type.name');
                },
                'expanded'          => true,
                'label'             => 'Choisissez un type',
                'constraints'       => new Assert\NotNull()
            )
        );
    }

    public function getParent()
    {
        return 'entity';
    }

    public function getName()
    {
        return 'laplace_trainingbundle_messagetype';
    }
}
