<?php

namespace Laplace\TrainingBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Représente le type d'entité Request pour la validation de formulaire.
 */
class RequestType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', 'text',
            array(
                'label' => 'Titre',
            )
            );
        $builder->add('description', 'textarea',
            array(
                'label'     => 'Description',
                'required'  => false,
            )
        );
        $builder->add('category', 'entity',
            array(
                'class'     => 'LaplaceTrainingBundle:Category',
                'property'  => 'name',
                'group_by'  => 'domain.name',
                'label'     => 'Catégorie',
                'required'  => false,
            )
        );
        // --- Modification du 10 Septembre 2013
        // Pour demander l'heure de la formation, décommenter ci-dessous
        //
        //$builder->add('trainingDate', 'datetime',
        //    array(
        //        'date_widget'   => 'single_text',
        //        'time_widget'   => 'single_text',
        //        'date_format'   => 'dd/MM/yy',
        //        'label'         => 'Date de la formation',
        //        'required'      => false,
        //    )
        //);

        $builder->add('trainingDate', 'date',
            array(
                'widget'        => 'single_text',
                'format'        => 'dd/MM/yy',
                'label'         => 'Date de la formation',
                'required'      => false,
            )
        );
        $builder->add('trainingDuration', 'integer',
            array(
                'label'     => 'Durée de la formation',
                'attr'      => array(
                    'placeholder' => 'Indiquez un nombre d\'heures',
                ),
                'required'  => false,
            )
        );
        $builder->add('subscriptionDeadline', 'date',
            array(
                'widget'    => 'single_text',
                'format'    => 'dd/MM/yy',
                'label'     => 'Date limite d\'inscription',
                'required'  => false,
            )
        );
        $builder->add('availablePlacesCount', 'integer',
            array(
                'label'     => 'Nombre max. de participants',
                'required'  => false,
            )
        );
        $builder->add('agentsPrioritaires', 'text',
            array(
                'label'     => 'Agents Prioritaires',
                'required'  => false,
            )
        );
        $builder->add('funding', 'text',
            array(
                'label'     => 'Financement',
                'required'  => false,
            )
        );

        if (!$options['admin']) {
            return;
        }

        $builder->add('processingDate', 'date',
            array(
                'widget'    => 'single_text',
                'format'    => 'dd/MM/yy',
                'label'     => 'Date de traitement',
                'required'  => false,
            )
        );

        $builder->add('close', 'choice',
            array(
                'choices'       => array(
                    false =>
                        'Aucun (autoriser les inscriptions)',
                    true =>
                        'Activé (interdire les nouvelles inscriptions)',
                ),
                'constraints'   => new Assert\NotNull(),
                'expanded'      => true,
                'label'         => 'Verrouillage',
            )
        );

    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class'    => 'Laplace\TrainingBundle\Entity\Request',
                'admin'         => false,
            )
        );

        $resolver->setOptional(
            array(
                'admin'
            )
        );

        $resolver->setAllowedTypes(
            array(
                'admin' => 'bool',
            )
        );
    }

    public function getName()
    {
        return 'laplace_trainingbundle_requesttype';
    }
}
