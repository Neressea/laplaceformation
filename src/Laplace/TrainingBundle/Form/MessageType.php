<?php

namespace Laplace\TrainingBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MessageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('text', 'textarea',
            array(
                'label' => 'Contenu du message',
            )
        );
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'Laplace\TrainingBundle\Entity\Message',
            )
        );
    }

    public function getName()
    {
        return 'laplace_trainingbundle_messagetype';
    }
}
