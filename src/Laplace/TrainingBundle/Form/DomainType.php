<?php

namespace Laplace\TrainingBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DomainType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', 'text',
            array(
                'label' => 'Nom du domaine',
            )
        );
        $builder->add('code', 'integer',
            array(
                'label' => 'Code',
            )
        );
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'Laplace\TrainingBundle\Entity\Domain',
            )
        );
    }

    public function getName()
    {
        return 'laplace_trainingbundle_domaintype';
    }
}
