<?php

namespace Laplace\CommonBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\Yaml\Yaml;

use Laplace\UserBundle\Entity\Site;
use Laplace\UserBundle\Entity\StatutAgent;
use Laplace\UserBundle\Entity\StatutDoctorant;
use Laplace\UserBundle\Entity\Tutelle;

use Laplace\TrainingBundle\Entity\Category;
use Laplace\TrainingBundle\Entity\Domain;
use Laplace\TrainingBundle\Entity\Type;

class LoadPresetsData extends ContainerAware implements FixtureInterface
{

    private $mngr;

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $mngr)
    {
        $path = $this->container->get('kernel')->getRootDir() .
            '/config/presets.yml';

        $yaml = Yaml::parse(file_get_contents($path));

        $sites                  = array();
        $statuts_agents         = array();
        $statuts_doctorants     = array();
        $tutelles               = array();
        $domains                = array();
        $categories             = array();
        $types                  = array();

        //
        // Sites
        //

        foreach ($yaml['sites'] as $name) {
            $sites[] = new Site($name);
        }

        //
        // Statuts
        //

        foreach ($yaml['statuts']['agents'] as $groupe => &$intitules) {
            foreach ($intitules as $intitule) {
                $statuts_agents[] = new StatutAgent($intitule, $groupe);
            }
        }

        foreach ($yaml['statuts']['doctorants'] as $groupe => &$intitules) {
            foreach ($intitules as $intitule) {
                $statuts_doctorants[] = new StatutDoctorant($intitule, $groupe);
            }
        }

        //
        // Tutelles
        //

        foreach ($yaml['tutelles'] as $nom) {
            $tutelles[] = new Tutelle($nom);
        }

        //
        // Domaines et Catégories
        //

        foreach ($yaml['domaines'] as &$dom_info) {
            $name = $dom_info['nom'];
            $code = $dom_info['code'];

            $domains[] = $domain = new Domain($name, $code);

            if (empty($dom_info['categories']))
                continue;

            foreach ($dom_info['categories'] as $i => $name) {
                $categories[] = new Category($name, $domain, $code + $i + 1);
            }
        }

        //
        // Types
        //

        foreach ($yaml['types'] as $name => $description) {
            $types[] = new Type($name, $description);
        }

        //
        // Persist
        //

        $this->mngr = $mngr;
        $this->persistAll($sites);
        $this->persistAll($statuts_agents);
        $this->persistAll($statuts_doctorants);
        $this->persistAll($tutelles);
        $this->persistAll($domains);
        $this->persistAll($categories);
        $this->persistAll($types);
    }

    private function persistAll($entities)
    {
        foreach ($entities as $entity) {
            $this->mngr->persist($entity);
        }
        $this->mngr->flush();
    }
}
