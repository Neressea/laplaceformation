<?php

namespace Laplace\CommonBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Ce bundle contient toutes les ressources et les classes qui ne sont pas
 * spécifiques à un autre bundle.
 * On y trouve, par exemple, le service d'alertes, les templates twig qui
 * définissent la structure générale du site ainsi que les widgets des
 * formulaires, la base abstraite de tous les contrôleurs et la partie
 * autentification.
 */
class LaplaceCommonBundle extends Bundle
{
}
