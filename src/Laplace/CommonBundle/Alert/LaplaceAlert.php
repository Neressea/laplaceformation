<?php

namespace Laplace\CommonBundle\Alert;

use Symfony\Component\HttpKernel\Log\LoggerInterface;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Bridge\Doctrine\RegistryInterface;

use Laplace\CommonBundle\Entity\Alert;

/**
 * Service gérant les alertes visibles depuis le journal système.
 *
 * Les alertes sont de courts messages indiquant qu'il s'est passé quelque
 * chose dans le système comme un nouveau besoin, une inscription à une
 * demande, un message dans une discussion, etc.
 *
 * Ce service permet d'enregistrer des alertes de différentes importances,
 * de les récupérer et de les supprimer, par plage ou bien une par une.
 *
 * Un service de journalisation peut être spécifié, dans ce cas, les alertes
 * y sont également consignées.
 *
 * Enfin, il est possible de filtrer les alertes selon le rôle de l'utilisateur
 * actuel (nécessite le service `security.context`). Ceci permet de ne pas émettre
 * des alertes lorsqu'un administrateur utilise le système.
 */
class LaplaceAlert
{
    private $mngr;
    private $logger;

    private $security_context;
    private $filters = array();

    /**
     * Constructeur. Ce service nécessite un accès au gestionnaire d'entités
     * pour enregistrer les alertes.
     *
     * param RegistryInterface $doctrine
     */
    public function __construct(RegistryInterface $doctrine)
    {
        $this->mngr = $doctrine->getManager();
    }

    /**
     * Définit ou désactive le service de journalisation.
     *
     * @param  LoggerInterface|null $logger
     * @return LaplaceAlert         $this
     */
    public function setLogger(LoggerInterface $logger = null)
    {
        $this->logger = $logger;

        return $this;
    }

    /**
     * Définit ou désactive le service `security.context`.
     *
     * @param  SecurityContextInterface $security_ctx
     * @return LaplaceAlert             $this
     */
    public function setSecurityContext(
        SecurityContextInterface $security_ctx = null
    ) {
        $this->security_context = $security_ctx;

        return $this;
    }

    /**
     * Ajoute un rôle à la liste des rôles filtrés.
     *
     * @param  string       $role
     * @return LaplaceAlert $this
     */
    public function addFilter($role)
    {
        $this->filters[] = $role;

        return $this;
    }

    /**
     * Renvoie la liste des rôles filtrés.
     *
     * @return array
     */
    public function getFilters()
    {
        return $this->filters;
    }

    /**
     * Vide la liste des rôles filtrés.
     *
     * @return LaplaceAlert
     */
    public function resetFilters()
    {
        $this->filters = array();

        return $this;
    }

    /**
     * Enregistre une alerte de type `NOTICE`. Cette fonction accepte des
     * arguments supplémentaires qui seront utilisés pour formatter $message
     * selon le format de `printf()`.
     *
     * @param  string       $link
     * @param  string       $message
     * @return LaplaceAlert $this
     */
    public function notice($link, $message)
    {
        $alert = $this->postAlert(
            Alert::NOTICE,
            $link,
            $message,
            array_slice(func_get_args(), 2)
        );
        $this->log($alert);

        return $this;
    }

    /**
     * Enregistre une alerte de type `INFORMATION`. Cette fonction
     * accepte des arguments supplémentaires qui seront utilisés pour formatter
     * $message selon le format de `printf()`.
     *
     * @param  string       $link
     * @param  string       $message
     * @return LaplaceAlert $this
     */
    public function information($link, $message)
    {
        $alert = $this->postAlert(
            Alert::INFORMATION,
            $link,
            $message,
            array_slice(func_get_args(), 2)
        );
        $this->log($alert);

        return $this;
    }

    /**
     * Enregistre une alerte de type `IMPORTANT`. Cette fonction accepte
     * des arguments supplémentaires qui seront utilisés pour formatter $message
     * selon le format de `printf()`.
     *
     * @param  string       $link
     * @param  string       $message
     * @return LaplaceAlert $this
     */
    public function important($link, $message)
    {
        $alert = $this->postAlert(
            Alert::IMPORTANT,
            $link,
            $message,
            array_slice(func_get_args(), 2)
        );
        $this->log($alert);

        return $this;
    }

    /**
     * Supprime $count alertes antérieures à la date correspondant au timestamp
     * unix `$before`.
     *
     * @param  int $before
     * @param  int $count
     * @return int Le nombre d'alertes supprimées
     */
    public function deleteAlerts($before, $count)
    {
        $since = new \DateTime();
        $since->setTimestamp($before);

        // REMARQUE :
        // il est impossible de limiter une requête DELETE avec doctrine,
        // on récupère donc les entités et on utilise #remove() à la place
        // (on pourrait ne récupérer que les ids et utiliser getReference())

        $query_builder = $this->mngr->createQueryBuilder();
        $query_builder->select('alert')
            ->from('LaplaceCommonBundle:Alert', 'alert')
            ->where('alert.date < :date')
            ->setParameter('date', $since)
            ->setMaxResults($count);
        $alerts = $query_builder->getQuery()->getResult();

        foreach ($alerts as $alert) {
            $this->mngr->remove($alert);
        }

        $this->mngr->flush();

        return count($alerts);
    }

    /**
     * Supprime toutes les alertes plus ancienne que l'âge maximum spécifié.
     *
     * @param  \DateInterval $max_age
     * @return int           Le nombre d'alertes supprimées
     */
    public function deleteOldAlerts(\DateInterval $max_age)
    {
        $since = new \DateTime();
        $since->sub($max_age);

        $rows = $this->mngr
            ->createQueryBuilder()
            ->delete('LaplaceCommonBundle:Alert', 'alert')
            ->where('alert.date < :date')
            ->setParameter('date', $since)
            ->getQuery()
            ->execute();
        $this->mngr->flush();

        return $rows;
    }

    /**
     * Supprime une unique alerte étant donné son identifiant.
     *
     * @param  int  $id
     * @return bool Vrai si l'alerte a été supprimée
     */
    public function deleteSingleAlert($id)
    {
        $rows = $this->mngr
            ->createQueryBuilder()
            ->delete('LaplaceCommonBundle:Alert', 'alert')
            ->where('alert.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->execute();
        $this->mngr->flush();

        return (bool) $rows;
    }

    /**
     * Récupère toutes les alertes.
     *
     * @return Alert[] Les alertes classées par date décroissante
     */
    public function getAlerts($max_results = null)
    {
        return $this->mngr
            ->getRepository('LaplaceCommonBundle:Alert')
            ->findBy(array(), array('date' => 'DESC'));
    }

    /**
     * Fabrique un objet Alert d'après les paramètres passés et persiste
     * l'entité créée.
     *
     * @param  int    $severity
     * @param  string $link
     * @param  string $format
     * @param  array  $args
     * @return Alert
     */
    private function postAlert($severity, $link, $format, $args)
    {
        $msg    = vsprintf($format, $args);
        $alert  = new Alert($severity);

        if (strlen($link > 128)) {
            $msg .= '\n\n' . $link;
        } else {
            $alert->setLink($link);
        }

        $alert->setMessage($msg);

        // on regarde si le responsable de l'alerte est filtré ou pas
        $filtered = false;
        if ($this->security_context != null) {
            foreach ($this->filters as $filter) {
                if ($this->security_context->isGranted($filter)) {
                    $filtered = true;
                    break;
                }
            }
        }

        if (!$filtered) {
            $this->mngr->persist($alert);
            $this->mngr->flush();
        }

        return $alert;
    }

    /**
     * Journalise une alerte si le service de journalisation a été spécifié.
     *
     * @param Alert $alert
     */
    private function log(Alert $alert)
    {
        if ($this->logger == null) {
            return;
        }

        $msg = $alert->getMessage();

        switch ($alert->getSeverity()) {
            case Alert::NOTICE:
                $this->logger->notice($msg);
                break;

            case Alert::INFORMATION:
                $this->logger->info($msg);
                break;

            case Alert::IMPORTANT:
                $this->logger->warn($msg);
                break;

            default:
                $this->logger->alert($msg);
                break;
        }
    }

}
