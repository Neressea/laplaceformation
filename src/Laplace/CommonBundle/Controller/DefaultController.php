<?php

namespace Laplace\CommonBundle\Controller;

use Symfony\Component\HttpFoundation\Response;

use Laplace\UserBundle\Entity as User;
use Laplace\TrainingBundle\Entity as Training;

/**
 * Ce contrôleur est utilisé pour définir les points d'accès aux différentes
 * parties du sites : accueil et inscription, partie utilisateur et partie
 * administrateur.
 */
class DefaultController extends AbstractBaseController
{

    /**
     * Affiche la page d'accueil du site.
     *
     * @return Response
     */
    public function homepageAction()
    {
        return $this->render('LaplaceCommonBundle:Default:homepage.html.twig');
    }

    /**
     * Point d'accès de la partie utilisateur.
     *
     * Redirige vers la page d'accueil de la partie utilisateur ("voir mon
     * profil").
     *
     * @return Response
     */
    public function userHomepageAction()
    {
        return $this->redirect(
            $this->generateUrl('laplace_user_view_my_profile')
        );
    }

    /**
     * Point d'accès de la partie administrateur.
     *
     * Redirige vers la page d'accueil de la partie administrateur ("tableau
     * de bord").
     *
     * @return Response
     */
    public function adminHomepageAction()
    {
        return $this->redirect(
            $this->generateUrl('laplace_common_adm_dashboard')
        );
    }

    //
    // Le code suivant permet de remplir la base avec des utilisateurs
    // aléatoires lors du développement
    //
/*
    public function initDatabaseAction()
    {
        if (!$this->get('kernel')->isDebug())
            return new Response('<body>Error</body>');

        $factory = $this->get('security.encoder_factory');

        $mngr = $this->getDoctrine()->getManager();

        $domains = $mngr
            ->getRepository('LaplaceTrainingBundle:Domain')
            ->findAll();
        $categories = $mngr
            ->getRepository('LaplaceTrainingBundle:Category')
            ->findAll();
        $types = $mngr
            ->getRepository('LaplaceTrainingBundle:Type')
            ->findAll();
        $tutelles = $mngr
            ->getRepository('LaplaceUserBundle:Tutelle')
            ->findAll();
        $sites = $mngr
            ->getRepository('LaplaceUserBundle:Site')
            ->findAll();

        $statuts = $mngr
            ->getRepository('LaplaceUserBundle:StatutAgent')
            ->findAll();

        $agent1 = new User\User();
        $agent1 ->setUsername('jdupont')
                ->setEmailAddress('jdupont@laplace.enseeiht.fr')
                ->setGender(User\User::MALE)
                ->setName('Jean Dupont')
                ->setTutelle($tutelles[2])
                ->setSite($sites[1])
                ->setTelephoneNumber1('0692283012')
                ->setUserInfo(new User\InfoAgent($statuts[3]));

        $agent2 = new User\User();
        $agent2 ->setUsername('jb')
                ->setEmailAddress('jb@laplace.enseeiht.fr')
                ->setGender(User\User::MALE)
                ->setName('Juste Leblanc')
                ->setRole('ROLE_ADMIN')
                ->setTutelle($tutelles[2])
                ->setSite($sites[2])
                ->setTelephoneNumber1('0892287712')
                ->setUserInfo(new User\InfoAgent($statuts[1]));

        //$users = array($agent1, $agent2);
        $users = array($agent1);

        ///

        $prenoms = array(
                'Camille',
                'Dominique',
                'Véronique',
                'Claude',
                'Maxime',
                'Charlie',
                'Alex'
            );

        $noms = array(
                'Martin',
                'Bernard',
                'Dubois',
                'Thomas',
                'Robert',
                'Richard',
                'Petit',
                'Durand',
                'Leroy',
                'Moreau'
            );

        $debut = strtotime('1 January 2008');
        $fin = time();

        foreach ($prenoms as $prenom) {
            foreach ($noms as $nom) {
                switch ($prenom) {
                    case 'Claude':
                        $num = 2;
                        break;

                    case 'Charlie':
                        $num = 3;
                        break;

                    default:
                        $num = '';
                }

                $username   = strtolower($prenom[0] . $nom . $num);
                $tutelle    = $tutelles[rand(1, count($tutelles))-1];
                $site       = $sites[rand(1, count($sites))-1];
                $statut     = $statuts[rand(1, count($statuts))-1];

                $since      = new \DateTime();
                $since->setTimestamp(rand($debut, $fin));

                $agent = new User\User();
                $agent  ->setUsername($username)
                        ->setRole('ROLE_USER')
                        ->setEmailAddress("$username@laplace.n7.fr")
                        ->setGender(rand(0, 1))
                        ->setName("$prenom $nom")
                        ->setTutelle($tutelle)
                        ->setSite($site)
                        ->setTelephoneNumber1(rand(10000, 1000000))
                        ->setUserInfo(new User\InfoAgent($statut))
                        ->setSince($since);

                $users[] = $agent;
            }
        }

//        $statuts[] = new User\StatutDoctorant('Doctorant', 'Doctorants');
//        $docto = new User\User();
//        $docto  ->setUsername('ddeschamps')
//                ->setEmailAddress('ddeschamps@laplace.enseeiht.fr')
//                ->setGender(User\User::MALE)
//                ->setSurname('Deschamps')
//                ->setForename('Damien')
//                ->setRole('ROLE_USER')
//                ->setTutelle($tutelles[2])
//                ->setSite($sites[2])
//                ->setTelephoneNumber1('0992287712');
//        $info = new User\InfoDoctorant(end($statuts));
//        $info   ->setDebutThese(new \DateTime('11-09-03'))
//                ->setResponsable('Mme La Responsable')
//                ->setEcoleDoctorale('Exemple école doctorale')
//                ->setBourse('Exemple bourse');
//        $docto  ->setUserInfo($info);
//
//        $users[] = $docto;

        //$need1 = new Training\Need($agent1, 'Matrices');
        //$need1->setCategory($categories[1]);

        //$need2 = new Training\Need($agent1, 'Equa. diff.');
        //$need2->setCategory($categories[1]);

        //$need3 = new Training\Need($docto, 'Manip. laser');
        //$need3->setCategory($categories[2]);

        //$need4 = new Training\Need($docto, 'Test');

        //$need5 = new Training\Need($agent2, 'Intro génie log.');
        //$need5->setCategory($categories[5]);

        //$needs = array($need1, $need2, $need5);

        //$events[] = Training\Event::create(Training\Event::USER_ACCOUNT_CREATED, $agent1);
        //$events[] = Training\Event::create(Training\Event::USER_ACCOUNT_VALIDATED, $agent1);
        //$events[] = Training\Event::create(Training\Event::USER_ACCOUNT_CREATED, $agent2);
        //$events[] = Training\Event::create(Training\Event::USER_ACCOUNT_VALIDATED, $agent2);
        //$events[] = Training\Event::create(Training\Event::USER_ACCOUNT_CREATED, $docto);
        //$events[] = Training\Event::create(Training\Event::USER_ACCOUNT_VALIDATED, $docto);

        //$events[2]->setDate(new \DateTime('2011/06/20'));
        //$events[3]->setDate(new \DateTime('2011/06/23'));

        $entities = array_merge(
                $users
                //$needs,
                //$events
        );

        $em = $this->getDoctrine()->getManager();
        foreach ($entities as $e) {
            $em->persist($e);
        }
        $em->flush();

        return new Response('<body>Ok</body>');
    }
*/
}
