<?php

namespace Laplace\CommonBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\Form;

/**
 * Cette classe abstraite définie des méthodes utilisées partout dans
 * les contrôleurs de l'application.
 */
abstract class AbstractBaseController extends Controller
{

    /**
     * Ajoute un message d'information au flashbag de la session. Les paramètres
     * supplémentaires seront utilisés pour formatter le message (format
     * printf).
     *
     * @param string $msg
     */
    protected function flashInfo($msg)
    {
        if (func_num_args() > 1) {
            $args = func_get_args();
            $msg = call_user_func_array('sprintf', $args);
        }

        $this->get('session')->getFlashBag()->add('info', $msg);
    }

    /**
     * Ajoute un message d'erreur au flashbag de la session. Les paramètres
     * supplémentaires seront utilisés pour formatter le message (format
     * printf).
     *
     * @param string $msg
     */
    protected function flashError($msg)
    {
        if (func_num_args() > 1) {
            $args = func_get_args();
            $msg = call_user_func_array('sprintf', $args);
        }

        $this->get('session')->getFlashBag()->add('error', $msg);
    }

    /**
     * Ajoute un message d'erreur générique au flashbag de la session indiquant
     * qu'un formulaire n'a pas été validé à cause d'erreurs.
     */
    protected function flashFormError()
    {
        $this->flashError(
            'Les données soumises ne sont pas valides. ' .
            'Veuillez vérifier le formulaire.'
        );
    }

    /**
     * Construit un FormBuilder nommé, utile dans le cas où il y a plusieurs
     * formulaires à traiter dans la même action. L'objet renvoyé cascadera
     * automatiquement la validation des données.
     *
     * @param  string      $form_name Le nom du formulaire
     * @param  array       $data      Les données initiales du formulaires
     * @return FormBuilder
     */
    protected function createNamedFormBuilder($form_name, $data)
    {
        return $this->get('form.factory')->createNamedBuilder(
            $form_name, 'form', $data, array(
                'cascade_validation' => true,
            )
        );
    }

    /**
     * Cette méthode regarde si des données ont été soumises et si ces données
     * sont bien associées au formulaire nommé. Si c'est, la validation est
     * exécutée. Dans ce cas, il faudra par la suite tester si les données
     * sont bien valides avec `$form->isValid()`.
     *
     * @param  string  $form_name Le nom du formulaire
     * @param  Form    $form      L'objet formulaire
     * @return boolean Vrai si la requête contient des donnés qui ont été
     * traitées
     */
    protected function isNamedFormSubmitted($form_name, $form)
    {
        $req = $this->getRequest();

        if ($req->getMethod() == 'POST' && $req->request->has($form_name)) {
            $form->handleRequest($req);

            return true;
        }

        return false;
    }

}
