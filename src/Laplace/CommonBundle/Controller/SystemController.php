<?php

namespace Laplace\CommonBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Contrôleur de l'espace administrateur.
 */
class SystemController extends AbstractBaseController
{

    /**
     * Affiche le panneau de contrôle de l'espace administrateur. Il s'agit
     * d'une liste d'alertes récentes avec la possibilité de les supprimer
     * une par une ou par plage.
     *
     * @return Response
     */
    public function dashboardAction()
    {
        $laplace_alert = $this->get('laplace.alert');

        $parameters = array();

        //
        // Formulaire de suppression des alertes
        //

        $url = $this->generateUrl('laplace_common_adm_dashboard', array());

        // données initiales du formulaire
        $form_name = 'delete_alerts_form';
        $data = array(
            'before' => time(),
            'count' => null,
        );

        // construction du formulaire
        $builder = $this->createNamedFormBuilder($form_name, $data);
        $builder->add('before', 'hidden', array());
        $builder->add('count', 'hidden', array());
        $builder->add('do', 'submit',
            array(
                'label' => 'Supprimer',
            )
        );

        // validation
        $form = $builder->getForm();
        if ($this->isNamedFormSubmitted($form_name, $form)) {
            if ($form->isValid()) {
                $data = $form->getData();

                $laplace_alert->deleteAlerts($data['before'], $data['count']);

                $this->flashInfo('Les alertes ont été supprimées.');

                return $this->redirect($url);
            } else {
                $this->flashFormError();
            }
        }

        // échec de la validation : création de la vue
        $parameters[$form_name] = $form->createView();

        $laplace_alert->deleteOldAlerts(new \DateInterval('P1M'));
        $alerts = $laplace_alert->getAlerts();

        $parameters['alerts'] = $alerts;

        return $this->render(
            'LaplaceCommonBundle:Default:dashboard.html.twig',
            $parameters
        );
    }

    /**
     * Supprime une unique alerte. La requête doit contenir une valeur
     * `alert` représentant l'id de l'alerte à supprimer.
     *
     * @param  Request      $request
     * @return JsonResponse Un objet JSON contenant la valeur 'ok'
     */
    public function deleteAlertAction(Request $request)
    {
        $id = $request->request->get('alert');

        $this->get('laplace.alert')->deleteSingleAlert($id);

        return new JsonResponse('ok');
    }

}
