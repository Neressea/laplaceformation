<?php

namespace Laplace\CommonBundle\Form;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

/**
 * Cette classe représente un convertisseur entre une valeur booléenne nullable
 * et une chaîne de caractères (dont les valeurs admissibles sont 'null',
 * 'true', 'false').
 */
class TriboolToTextTransformer implements DataTransformerInterface
{

    /**
     * Transforme un booléen nullable en string.
     *
     * @param bool|null $bool
     *
     * @return string 'null', 'true' ou 'false'
     */
    public function transform($bool)
    {
        if ($bool === null) {
            return 'null';
        }

        return $bool ? 'true' : 'false';
    }

    /**
     * Transforme une string en un booléen nullable.
     *
     * @param string $value 'null', 'true' ou 'false'
     *
     * @return bool|null
     *
     * @throws TransformationFailedException Si `$value` n'est pas valide
     */
    public function reverseTransform($value)
    {
        if ($value === 'null') {
            return null;
        } elseif ($value === 'false') {
            return false;
        } elseif ($value === 'true') {
            return true;
        } else {
            throw new TransformationFailedException();
        }
    }

}
