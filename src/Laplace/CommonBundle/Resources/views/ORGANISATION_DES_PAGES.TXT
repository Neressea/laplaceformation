

La variable 'page' passée à "admin-page" ou à "user-page" est un tableau de
la forme [ onglet, section ].


ONGLET          SECTION         ADMIN   UTILISATEUR     VARIABLES

system
                dashboard       X       -
                tutelles        X       -
                sites           X       -
                statuts_
                    agents      X       -
                statuts_
                    doctorants  X       -
                domains         X       -
                categories      X       -
                types           X       -



profile
                view            X       X               profile (si admin)
                edit            X       X               profile (si admin)
                delete          -       X               profile
                all             X       -               requested
                limited         X       -               requested

need
                view            X       X               need
                edit            X       X               need
                delete          X       -               need
                all             X       -
                create          X       -
                personal        -       X

request
                view            X       X               request
                edit            X       X               request
                delete          X       -               request
                all             X       -
                create          X       -
                personal        -       X


thread
                view            X       X               thread
                edit            X       X               thread
                all             X       X

history
                all             -       X               requested
                needs           -       X               requested
                requests        -       X               requested









