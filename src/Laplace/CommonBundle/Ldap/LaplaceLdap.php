<?php

namespace Laplace\CommonBundle\Ldap;

class LaplaceLdap
{
    private $host;
    private $base_dn;

    public function __construct($host, $base_dn)
    {
        $this->host     = $host;
        $this->base_dn  = $base_dn;
    }

    public function authenticate($uid, $password)
    {
        /*$ldap_conn = ldap_connect($this->host);

        ldap_set_option($ldap_conn, LDAP_OPT_PROTOCOL_VERSION, 3);

        // anonymous binding
        if (!ldap_bind($ldap_conn)) {
            throw new \Exception('LDAP bind failed while authenticating user.');
        }

        $filter = 'uid=' . $this->escapeValue($uid);

        $results = ldap_search(
            $ldap_conn,
            $this->base_dn,
            $filter,
            array('dn')
        );

        if (!$results) {
            throw new \Exception(
                'LDAP search failed while authenticating user.'
            );
        }

        $entries = ldap_get_entries($ldap_conn, $results);

        if ($entries['count'] > 0) {
            $dn = $entries[0]['dn'];
            $ok = @ldap_bind($ldap_conn, $dn, $password);
        } else {
            $ok = false;
        }

        ldap_close($ldap_conn);*/

        return true; //$ok;
    }

    /**
     * Récupère les informations associées à un nom d'utilisateur.
     * Si `$password` est spécifié, on vérifie que le mot de passe est valide.
     * Si ce n'est pas le cas, null est retourné.
     *
     * @param  string        $uid
     * @param  string|nul    $password
     * @return LdapData|null
     * @throws \Exception    En cas de problème avec la connexion LDAP
     */
    public function fetch($uid, $password = null)
    {
        $ldap_conn = ldap_connect($this->host);

        ldap_set_option($ldap_conn, LDAP_OPT_PROTOCOL_VERSION, 3);

        // anonymous binding
        if (!@ldap_bind($ldap_conn)) {
            throw new \Exception('LDAP bind failed while authenticating user.');
        }

        $filter = 'uid=' . $this->escapeValue($uid);

        $results = ldap_search(
            $ldap_conn,
            $this->base_dn,
            $filter,
            array('dn', 'mail', 'cn')
        );

        if (!$results) {
            throw new \Exception(
                'LDAP search failed while fetching user data.'
            );
        }

        $entries = ldap_get_entries($ldap_conn, $results);

        if ($entries['count'] > 0) {
            $dn     = $entries[0]['dn'];
            $mail   = $entries[0]['mail'];
            $cn     = $entries[0]['cn'];

            $data = new LdapData();

            if ($mail['count'] > 0) {
                $data->setEmailAddress($mail[0]);
            }

            if ($cn['count'] > 0) {
                $data->setName($cn[0]);
            }

            if ($password !== null) {
                if (!@ldap_bind($ldap_conn, $dn, $password)) {
                    $data = null;
                }
            }
        } else {
            $data = null;
        }

        ldap_close($ldap_conn);

        return $data;
    }

    /**
     * Encode les caractères spéciaux par leur notation hexadécimal selon la
     * RFC 2254. Les caractères spéciaux sont :
     *      * ( ) \ NUL
     * @param  string $value
     * @return string
     */
    public function escapeValue($value)
    {
        $value = mb_ereg_replace('/\\x5C/', '\\x5C', $value); // \\

        $value = mb_ereg_replace('/\\x00/', '\\x00', $value); // \0
        $value = mb_ereg_replace('/\\x2A/', '\\x2A', $value); // *
        $value = mb_ereg_replace('/\\x28/', '\\x28', $value); // (
        $value = mb_ereg_replace('/\\x29/', '\\x29', $value); // )

        return $value;
    }

    public function getHost()
    {
        return $this->host;
    }

    public function setHost($host)
    {
        $this->host = $host;

        return $this;
    }

    public function getBaseDN()
    {
        return $this->base_dn;
    }

    public function setBaseDN($base_dn)
    {
        $this->base_dn = $base_dn;

        return $this;
    }
}

class LdapData
{
    private $emailAddress;
    private $name;

    public function __construct()
    {
    }

    public function getEmailAddress()
    {
        return $this->emailAddress;
    }

    public function setEmailAddress($emailAddress)
    {
        $this->emailAddress = $emailAddress;

        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

}
