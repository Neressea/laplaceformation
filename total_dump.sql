--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: alert; Type: TABLE; Schema: public; Owner: laplace_formation; Tablespace: 
--

CREATE TABLE alert (
    id integer NOT NULL,
    date timestamp(0) without time zone NOT NULL,
    severity smallint NOT NULL,
    link character varying(128) NOT NULL,
    message text NOT NULL
);


ALTER TABLE public.alert OWNER TO laplace_formation;

--
-- Name: alert_id_seq; Type: SEQUENCE; Schema: public; Owner: laplace_formation
--

CREATE SEQUENCE alert_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.alert_id_seq OWNER TO laplace_formation;

--
-- Name: alert_id_seq; Type: SEQUENCE SET; Schema: public; Owner: laplace_formation
--

SELECT pg_catalog.setval('alert_id_seq', 1264, true);


--
-- Name: category; Type: TABLE; Schema: public; Owner: laplace_formation; Tablespace: 
--

CREATE TABLE category (
    id integer NOT NULL,
    domain_id integer NOT NULL,
    name character varying(128) NOT NULL,
    code smallint NOT NULL
);


ALTER TABLE public.category OWNER TO laplace_formation;

--
-- Name: category_id_seq; Type: SEQUENCE; Schema: public; Owner: laplace_formation
--

CREATE SEQUENCE category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.category_id_seq OWNER TO laplace_formation;

--
-- Name: category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: laplace_formation
--

SELECT pg_catalog.setval('category_id_seq', 72, true);


--
-- Name: domain; Type: TABLE; Schema: public; Owner: laplace_formation; Tablespace: 
--

CREATE TABLE domain (
    id integer NOT NULL,
    name character varying(128) NOT NULL,
    code smallint NOT NULL
);


ALTER TABLE public.domain OWNER TO laplace_formation;

--
-- Name: domain_id_seq; Type: SEQUENCE; Schema: public; Owner: laplace_formation
--

CREATE SEQUENCE domain_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.domain_id_seq OWNER TO laplace_formation;

--
-- Name: domain_id_seq; Type: SEQUENCE SET; Schema: public; Owner: laplace_formation
--

SELECT pg_catalog.setval('domain_id_seq', 12, true);


--
-- Name: event; Type: TABLE; Schema: public; Owner: laplace_formation; Tablespace: 
--

CREATE TABLE event (
    id integer NOT NULL,
    user_id integer NOT NULL,
    date timestamp(0) without time zone NOT NULL,
    type smallint NOT NULL,
    referenceid integer,
    message text NOT NULL
);


ALTER TABLE public.event OWNER TO laplace_formation;

--
-- Name: event_id_seq; Type: SEQUENCE; Schema: public; Owner: laplace_formation
--

CREATE SEQUENCE event_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.event_id_seq OWNER TO laplace_formation;

--
-- Name: event_id_seq; Type: SEQUENCE SET; Schema: public; Owner: laplace_formation
--

SELECT pg_catalog.setval('event_id_seq', 2813, true);


--
-- Name: infoagent; Type: TABLE; Schema: public; Owner: laplace_formation; Tablespace: 
--

CREATE TABLE infoagent (
    id integer NOT NULL,
    statut_id integer NOT NULL
);


ALTER TABLE public.infoagent OWNER TO laplace_formation;

--
-- Name: infodoctorant; Type: TABLE; Schema: public; Owner: laplace_formation; Tablespace: 
--

CREATE TABLE infodoctorant (
    id integer NOT NULL,
    statut_id integer NOT NULL,
    debutthese date NOT NULL,
    responsable character varying(64) DEFAULT NULL::character varying,
    ecoledoctorale character varying(128) DEFAULT NULL::character varying,
    bourse character varying(128) DEFAULT NULL::character varying
);


ALTER TABLE public.infodoctorant OWNER TO laplace_formation;

--
-- Name: message; Type: TABLE; Schema: public; Owner: laplace_formation; Tablespace: 
--

CREATE TABLE message (
    id integer NOT NULL,
    thread_id integer NOT NULL,
    author_id integer NOT NULL,
    date timestamp(0) without time zone NOT NULL,
    text text NOT NULL
);


ALTER TABLE public.message OWNER TO laplace_formation;

--
-- Name: message_id_seq; Type: SEQUENCE; Schema: public; Owner: laplace_formation
--

CREATE SEQUENCE message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.message_id_seq OWNER TO laplace_formation;

--
-- Name: message_id_seq; Type: SEQUENCE SET; Schema: public; Owner: laplace_formation
--

SELECT pg_catalog.setval('message_id_seq', 47, true);


--
-- Name: need; Type: TABLE; Schema: public; Owner: laplace_formation; Tablespace: 
--

CREATE TABLE need (
    id integer NOT NULL,
    author_id integer NOT NULL,
    category_id integer,
    issuedate timestamp(0) without time zone NOT NULL,
    title character varying(128) NOT NULL,
    description text
);


ALTER TABLE public.need OWNER TO laplace_formation;

--
-- Name: need_id_seq; Type: SEQUENCE; Schema: public; Owner: laplace_formation
--

CREATE SEQUENCE need_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.need_id_seq OWNER TO laplace_formation;

--
-- Name: need_id_seq; Type: SEQUENCE SET; Schema: public; Owner: laplace_formation
--

SELECT pg_catalog.setval('need_id_seq', 106, true);


--
-- Name: needsubscription; Type: TABLE; Schema: public; Owner: laplace_formation; Tablespace: 
--

CREATE TABLE needsubscription (
    need_id integer NOT NULL,
    user_id integer NOT NULL,
    type_id integer NOT NULL,
    subscriptiondate timestamp(0) without time zone NOT NULL
);


ALTER TABLE public.needsubscription OWNER TO laplace_formation;

--
-- Name: request; Type: TABLE; Schema: public; Owner: laplace_formation; Tablespace: 
--

CREATE TABLE request (
    id integer NOT NULL,
    author_id integer NOT NULL,
    category_id integer,
    issuedate timestamp(0) without time zone NOT NULL,
    title character varying(128) NOT NULL,
    description text,
    validated boolean NOT NULL,
    close boolean NOT NULL,
    processingdate timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
    trainingdate timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
    trainingduration smallint,
    subscriptiondeadline timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
    availableplacescount smallint,
    agentsprioritaires character varying(128) DEFAULT NULL::character varying,
    funding character varying(128) DEFAULT NULL::character varying
);


ALTER TABLE public.request OWNER TO laplace_formation;

--
-- Name: request_id_seq; Type: SEQUENCE; Schema: public; Owner: laplace_formation
--

CREATE SEQUENCE request_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.request_id_seq OWNER TO laplace_formation;

--
-- Name: request_id_seq; Type: SEQUENCE SET; Schema: public; Owner: laplace_formation
--

SELECT pg_catalog.setval('request_id_seq', 503, true);


--
-- Name: requestsubscription; Type: TABLE; Schema: public; Owner: laplace_formation; Tablespace: 
--

CREATE TABLE requestsubscription (
    request_id integer NOT NULL,
    user_id integer NOT NULL,
    type_id integer NOT NULL,
    subscriptiondate timestamp(0) without time zone NOT NULL,
    accepted boolean,
    attended boolean
);


ALTER TABLE public.requestsubscription OWNER TO laplace_formation;

--
-- Name: site; Type: TABLE; Schema: public; Owner: laplace_formation; Tablespace: 
--

CREATE TABLE site (
    id integer NOT NULL,
    name character varying(32) NOT NULL
);


ALTER TABLE public.site OWNER TO laplace_formation;

--
-- Name: site_id_seq; Type: SEQUENCE; Schema: public; Owner: laplace_formation
--

CREATE SEQUENCE site_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.site_id_seq OWNER TO laplace_formation;

--
-- Name: site_id_seq; Type: SEQUENCE SET; Schema: public; Owner: laplace_formation
--

SELECT pg_catalog.setval('site_id_seq', 4, true);


--
-- Name: statutagent; Type: TABLE; Schema: public; Owner: laplace_formation; Tablespace: 
--

CREATE TABLE statutagent (
    id integer NOT NULL,
    intitule character varying(32) NOT NULL,
    groupe character varying(32) NOT NULL
);


ALTER TABLE public.statutagent OWNER TO laplace_formation;

--
-- Name: statutagent_id_seq; Type: SEQUENCE; Schema: public; Owner: laplace_formation
--

CREATE SEQUENCE statutagent_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.statutagent_id_seq OWNER TO laplace_formation;

--
-- Name: statutagent_id_seq; Type: SEQUENCE SET; Schema: public; Owner: laplace_formation
--

SELECT pg_catalog.setval('statutagent_id_seq', 19, true);


--
-- Name: statutdoctorant; Type: TABLE; Schema: public; Owner: laplace_formation; Tablespace: 
--

CREATE TABLE statutdoctorant (
    id integer NOT NULL,
    intitule character varying(32) NOT NULL,
    groupe character varying(32) NOT NULL
);


ALTER TABLE public.statutdoctorant OWNER TO laplace_formation;

--
-- Name: statutdoctorant_id_seq; Type: SEQUENCE; Schema: public; Owner: laplace_formation
--

CREATE SEQUENCE statutdoctorant_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.statutdoctorant_id_seq OWNER TO laplace_formation;

--
-- Name: statutdoctorant_id_seq; Type: SEQUENCE SET; Schema: public; Owner: laplace_formation
--

SELECT pg_catalog.setval('statutdoctorant_id_seq', 4, true);


--
-- Name: thread; Type: TABLE; Schema: public; Owner: laplace_formation; Tablespace: 
--

CREATE TABLE thread (
    id integer NOT NULL,
    author_id integer NOT NULL,
    need_id integer,
    request_id integer,
    date timestamp(0) without time zone NOT NULL,
    title character varying(128) NOT NULL
);


ALTER TABLE public.thread OWNER TO laplace_formation;

--
-- Name: thread_id_seq; Type: SEQUENCE; Schema: public; Owner: laplace_formation
--

CREATE SEQUENCE thread_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.thread_id_seq OWNER TO laplace_formation;

--
-- Name: thread_id_seq; Type: SEQUENCE SET; Schema: public; Owner: laplace_formation
--

SELECT pg_catalog.setval('thread_id_seq', 23, true);


--
-- Name: thread_user; Type: TABLE; Schema: public; Owner: laplace_formation; Tablespace: 
--

CREATE TABLE thread_user (
    thread_id integer NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.thread_user OWNER TO laplace_formation;

--
-- Name: tutelle; Type: TABLE; Schema: public; Owner: laplace_formation; Tablespace: 
--

CREATE TABLE tutelle (
    id integer NOT NULL,
    nom character varying(32) NOT NULL
);


ALTER TABLE public.tutelle OWNER TO laplace_formation;

--
-- Name: tutelle_id_seq; Type: SEQUENCE; Schema: public; Owner: laplace_formation
--

CREATE SEQUENCE tutelle_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.tutelle_id_seq OWNER TO laplace_formation;

--
-- Name: tutelle_id_seq; Type: SEQUENCE SET; Schema: public; Owner: laplace_formation
--

SELECT pg_catalog.setval('tutelle_id_seq', 5, true);


--
-- Name: type; Type: TABLE; Schema: public; Owner: laplace_formation; Tablespace: 
--

CREATE TABLE type (
    id integer NOT NULL,
    name character varying(32) NOT NULL,
    description character varying(32) NOT NULL
);


ALTER TABLE public.type OWNER TO laplace_formation;

--
-- Name: type_id_seq; Type: SEQUENCE; Schema: public; Owner: laplace_formation
--

CREATE SEQUENCE type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.type_id_seq OWNER TO laplace_formation;

--
-- Name: type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: laplace_formation
--

SELECT pg_catalog.setval('type_id_seq', 3, true);


--
-- Name: user; Type: TABLE; Schema: public; Owner: laplace_formation; Tablespace: 
--

CREATE TABLE "user" (
    id integer NOT NULL,
    tutelle_id integer NOT NULL,
    site_id integer NOT NULL,
    username character varying(64) NOT NULL,
    emailaddress character varying(64) NOT NULL,
    gender smallint NOT NULL,
    name character varying(64) NOT NULL,
    role character varying(64) NOT NULL,
    since timestamp(0) without time zone NOT NULL,
    telephonenumber1 character varying(16) DEFAULT NULL::character varying,
    telephonenumber2 character varying(16) DEFAULT NULL::character varying,
    userinfo_id integer,
    firstname character varying(64) DEFAULT 'prenom'::character varying NOT NULL
);


ALTER TABLE public."user" OWNER TO laplace_formation;

--
-- Name: user_id_seq; Type: SEQUENCE; Schema: public; Owner: laplace_formation
--

CREATE SEQUENCE user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.user_id_seq OWNER TO laplace_formation;

--
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: laplace_formation
--

SELECT pg_catalog.setval('user_id_seq', 276, true);


--
-- Name: userinfo; Type: TABLE; Schema: public; Owner: laplace_formation; Tablespace: 
--

CREATE TABLE userinfo (
    id integer NOT NULL,
    subclass smallint NOT NULL
);


ALTER TABLE public.userinfo OWNER TO laplace_formation;

--
-- Name: userinfo_id_seq; Type: SEQUENCE; Schema: public; Owner: laplace_formation
--

CREATE SEQUENCE userinfo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.userinfo_id_seq OWNER TO laplace_formation;

--
-- Name: userinfo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: laplace_formation
--

SELECT pg_catalog.setval('userinfo_id_seq', 276, true);


--
-- Data for Name: alert; Type: TABLE DATA; Schema: public; Owner: laplace_formation
--

COPY alert (id, date, severity, link, message) FROM stdin;
\.


--
-- Data for Name: category; Type: TABLE DATA; Schema: public; Owner: laplace_formation
--

COPY category (id, domain_id, name, code) FROM stdin;
1	1	Interdisciplinaire	101
2	1	Mathématiques	102
3	1	Physique nucléaire et corpusculaire	103
4	1	Physique	104
5	1	Chimie	105
6	1	Sciences de la vie	106
7	1	Sciences de l'univers	107
8	1	Sciences pour l'ingénieur	108
9	1	Sciences de l'information et de la communication	109
10	1	Sciences humaines	110
11	1	Sciences sociales	111
21	3	Analyse et échange de pratiques professionnelles	301
22	3	Matériel, téléphonie	302
23	3	Réseaux, systèmes, sécurité	303
24	3	Langages et programmation	304
25	3	Génie logiciel	305
26	3	Conception développement de bases de données	306
27	3	Développement pour le web, internet	307
28	4	Bureautique	401
29	4	Utilisation de bases de données	402
30	4	Utilisation d'internet	403
37	6	Analyse et échange de pratiques pro. H&S, Médecine, AS	601
38	6	Hygiène et sécurité, habilitations	602
39	6	Médecine de prévention et action sociale	603
40	6	Ethique	604
43	7	Analyse et échange de pratiques professionnelles P&V / Qualité	701
44	7	Propriété industrielle, intellectuelle, littéraire et artistique / licences	702
45	7	Création d'entreprise	703
46	7	International / Europe	704
47	8	Analyse et échange de pratiques professionnelles finances et comptabilité	801
48	8	Comptabilité publique, budget, réglementation administrative et financière	802
49	8	Achats publics	803
50	8	Droit lié pour comptabilité, finances, fiscalité	804
51	9	Analyse et échange de pratiques professionnelles en ressources humaines	901
52	9	Méthodes et outils de gestion des ressources humaines	902
53	9	Droit administratif et réglementation du personnel	903
54	10	Management des hommes et des unités / Stratégie et pilotage	1001
55	10	Qualité et management des projets	1002
56	11	Connaissance du CNRS et de ses partenaires	1101
57	11	Gestion de son parcours professionnel	1102
58	11	Compétences transversales	1103
59	12	Anglais	1201
14	2	Sciences du vivant et de la nature	202
15	2	Sciences chimiques et sciences des matériaux	203
66	11	Connaissances générales	1104
68	11	Environnement Culturel	1105
12	2	Réseau métiers	201
16	2	Sciences de l'ingénieur et instrumentation scientifique	204
17	2	Sciences humaines et sociales	205
18	2	Documentation, édition, communication	206
19	2	Patrimoine et logistique	207
20	2	Gestion scientifique et technique	208
62	12	Langues européennes hors anglais	1202
60	12	Langues hors Europe	1203
61	12	Français pour personnels étrangers	1204
41	6	Sauveteur Secoursiste du Travail	605
42	6	ACMO - Agent préventeur - CHS/CHSCT	606
71	9	Correspondance Formation des personnels	904
69	4	Administration, organisation	407
31	4	CAO	404
72	4	PAO	405
32	4	Systèmes d'exploitation	408
70	4	DAO - Simulation	406
\.


--
-- Data for Name: domain; Type: TABLE DATA; Schema: public; Owner: laplace_formation
--

COPY domain (id, name, code) FROM stdin;
1	Connaissances Scientifiques	100
2	Techniques Scientifiques	200
3	Informatique	300
4	Bureautique	400
6	Hygiène & Sécurité / Ethique / Prévention	600
7	Partenariat & Valorisation	700
8	Finances, Comptabilité, Droit	800
9	Ressources Humaines	900
10	Management / Qualité	1000
11	Culture Institutionnelle et Efficacité Personnelle	1100
12	Langues	1200
\.


--
-- Data for Name: event; Type: TABLE DATA; Schema: public; Owner: laplace_formation
--

COPY event (id, user_id, date, type, referenceid, message) FROM stdin;
1	1	2013-07-25 13:19:34	26	\N	Création de votre compte.
2	2	2013-08-01 23:04:05	26	\N	Création de votre compte.
81	3	2013-09-03 18:04:05	26	\N	Création de votre compte.
4	2	2013-08-22 08:32:13	28	1	Inscription à la demande "Kit SPIP Contributeur".
5	2	2013-08-22 08:35:11	44	1	Votre inscription à la demande de formation "Kit SPIP Contributeur" a été acceptée.
6	2	2013-08-22 08:35:11	76	1	Vous avez indiqué avoir assisté à la formation "Kit SPIP Contributeur".
10	2	2013-08-22 08:44:42	28	3	Inscription à la demande "Réunion 2010 des correspondants formation du CNRS DR14".
13	2	2013-08-22 08:46:40	28	4	Inscription à la demande "Formation des coorespondants formation".
85	4	2013-09-09 16:07:17	26	\N	Création de votre compte.
86	5	2013-09-10 14:49:59	26	\N	Création de votre compte.
18	2	2013-08-22 08:50:20	44	3	Votre inscription à la demande de formation "Réunion 2010 des correspondants formation du CNRS DR14" a été acceptée.
19	2	2013-08-22 08:50:20	76	3	Vous avez indiqué avoir assisté à la formation "Réunion 2010 des correspondants formation du CNRS DR14".
20	2	2013-08-22 08:50:35	44	4	Votre inscription à la demande de formation "Formation des coorespondants formation" a été acceptée.
21	2	2013-08-22 08:50:35	76	4	Vous avez indiqué avoir assisté à la formation "Formation des coorespondants formation".
22	2	2013-08-22 08:54:57	28	5	Inscription à la demande "Recyclage 2012 de l’Habilitation électrique du personnel non électricien".
25	2	2013-08-22 08:55:33	44	5	Votre inscription à la demande de formation "Recyclage 2012 de l’Habilitation électrique du personnel non électricien" a été acceptée.
26	2	2013-08-22 08:55:33	76	5	Vous avez indiqué avoir assisté à la formation "Recyclage 2012 de l’Habilitation électrique du personnel non électricien".
27	2	2013-08-22 08:58:11	28	6	Inscription à la demande "CEM des équipements électroniques".
28	2	2013-08-22 08:58:57	44	6	Votre inscription à la demande de formation "CEM des équipements électroniques" a été acceptée.
29	2	2013-08-22 08:59:06	76	6	Vous avez indiqué avoir assisté à la formation "CEM des équipements électroniques".
65	1	2013-08-22 10:58:11	27	34	Ajout du besoin "Calculs scientfiques parallèles".
66	1	2013-08-22 10:58:17	43	34	Le besoin "Calculs scientfiques parallèles" a été pourvu.
87	6	2013-09-16 10:30:08	28	9	Inscription à la demande "Recyclage Sauveteur Secours du Travail 2009".
88	6	2013-09-16 10:31:56	28	10	Inscription à la demande "Excel - Se mettre à niveau sur les bases (tableaux et graphiques)".
89	6	2013-09-16 10:32:21	28	11	Inscription à la demande "Excel - Se perfectionner dans les calculs, lier les tableaux, faire des simulations".
90	6	2013-09-16 10:32:48	28	12	Inscription à la demande "Excel - Gérer une base de données, créer des tableaux croisés".
91	6	2013-09-16 10:34:04	44	9	Votre inscription à la demande de formation "Recyclage Sauveteur Secours du Travail 2009" a été acceptée.
92	6	2013-09-16 10:34:04	76	9	Vous avez indiqué avoir assisté à la formation "Recyclage Sauveteur Secours du Travail 2009".
101	6	2013-09-16 10:36:31	44	11	Votre inscription à la demande de formation "Excel - Se perfectionner dans les calculs, lier les tableaux, faire des simulations" a été acceptée.
102	6	2013-09-16 10:36:31	76	11	Vous avez indiqué avoir assisté à la formation "Excel - Se perfectionner dans les calculs, lier les tableaux, faire des simulations".
103	6	2013-09-16 10:36:53	44	10	Votre inscription à la demande de formation "Excel - Se mettre à niveau sur les bases (tableaux et graphiques)" a été acceptée.
104	6	2013-09-16 10:36:53	76	10	Vous avez indiqué avoir assisté à la formation "Excel - Se mettre à niveau sur les bases (tableaux et graphiques)".
105	6	2013-09-16 10:37:24	44	12	Votre inscription à la demande de formation "Excel - Gérer une base de données, créer des tableaux croisés" a été acceptée.
106	6	2013-09-16 10:37:24	76	12	Vous avez indiqué avoir assisté à la formation "Excel - Gérer une base de données, créer des tableaux croisés".
107	8	2013-09-16 10:42:38	28	13	Inscription à la demande "Initiation en droit".
108	8	2013-09-16 10:43:53	28	14	Inscription à la demande "Réunion correspondants formation 2009".
109	8	2013-09-16 10:44:37	28	15	Inscription à la demande "L'état actuel de la mondialisation".
110	8	2013-09-16 10:45:11	28	16	Inscription à la demande "Mieux connaitre le fonctionnement de l'Union Européenne".
111	8	2013-09-16 10:45:56	44	13	Votre inscription à la demande de formation "Initiation en droit" a été acceptée.
112	8	2013-09-16 10:45:56	76	13	Vous avez indiqué avoir assisté à la formation "Initiation en droit".
127	8	2013-09-16 10:50:01	44	14	Votre inscription à la demande de formation "Réunion correspondants formation 2009" a été acceptée.
128	8	2013-09-16 10:50:01	76	14	Vous avez indiqué avoir assisté à la formation "Réunion correspondants formation 2009".
129	8	2013-09-16 10:50:12	44	15	Votre inscription à la demande de formation "L'état actuel de la mondialisation" a été acceptée.
130	8	2013-09-16 10:50:12	76	15	Vous avez indiqué avoir assisté à la formation "L'état actuel de la mondialisation".
131	8	2013-09-16 10:50:24	44	16	Votre inscription à la demande de formation "Mieux connaitre le fonctionnement de l'Union Européenne" a été acceptée.
132	8	2013-09-16 10:50:24	76	16	Vous avez indiqué avoir assisté à la formation "Mieux connaitre le fonctionnement de l'Union Européenne".
133	8	2013-09-16 10:51:41	28	17	Inscription à la demande "Mieux comprendre la décentralisation".
134	8	2013-09-16 10:52:16	28	3	Inscription à la demande "Réunion 2010 des correspondants formation du CNRS DR14".
135	2	2013-09-16 10:55:39	28	18	Inscription à la demande "Réunion 2011 des correspondants formation du CNRS DR14".
136	2	2013-09-16 10:58:26	28	19	Inscription à la demande "Réunion 2012 des correspondants formation du CNRS DR14".
137	2	2013-09-16 11:03:43	44	19	Votre inscription à la demande de formation "Réunion 2012 des correspondants formation du CNRS DR14" a été acceptée.
138	2	2013-09-16 11:03:43	76	19	Vous avez indiqué avoir assisté à la formation "Réunion 2012 des correspondants formation du CNRS DR14".
139	8	2013-09-16 11:04:24	44	17	Votre inscription à la demande de formation "Mieux comprendre la décentralisation" a été acceptée.
140	8	2013-09-16 11:04:24	76	17	Vous avez indiqué avoir assisté à la formation "Mieux comprendre la décentralisation".
141	2	2013-09-16 11:04:57	44	18	Votre inscription à la demande de formation "Réunion 2011 des correspondants formation du CNRS DR14" a été acceptée.
292	35	2013-09-19 17:40:33	28	73	Inscription à la demande "Formation Caméra thermique".
142	2	2013-09-16 11:04:57	76	18	Vous avez indiqué avoir assisté à la formation "Réunion 2011 des correspondants formation du CNRS DR14".
143	2	2013-09-16 11:11:46	28	20	Inscription à la demande "Sécurité incendie - manipulation d'extincteur 2011".
144	2	2013-09-16 11:12:49	28	21	Inscription à la demande "Rencontres Régionales des Electroniciens - 2011".
145	2	2013-09-16 11:13:24	44	20	Votre inscription à la demande de formation "Sécurité incendie - manipulation d'extincteur 2011" a été acceptée.
146	2	2013-09-16 11:13:24	76	20	Vous avez indiqué avoir assisté à la formation "Sécurité incendie - manipulation d'extincteur 2011".
147	2	2013-09-16 11:13:49	44	21	Votre inscription à la demande de formation "Rencontres Régionales des Electroniciens - 2011" a été acceptée.
148	2	2013-09-16 11:13:49	76	21	Vous avez indiqué avoir assisté à la formation "Rencontres Régionales des Electroniciens - 2011".
149	9	2013-09-16 11:17:26	28	22	Inscription à la demande "SIFAC Missions - 2009".
150	9	2013-09-16 11:18:24	28	23	Inscription à la demande "SIFAC Dépenses (1) - 2009".
151	9	2013-09-16 11:19:04	28	24	Inscription à la demande "SIFAC Référentiel - 2009".
152	9	2013-09-16 11:20:02	28	25	Inscription à la demande "SIFAC Missions Métiers - 2010".
153	9	2013-09-16 11:20:45	28	26	Inscription à la demande "SIFAC Budget - 2010".
154	9	2013-09-16 11:21:59	28	27	Inscription à la demande "La fiscalité de l'UPS (1) - 2011".
155	9	2013-09-16 11:22:22	28	28	Inscription à la demande "La fiscalité de l'UPS (2) - 2011".
156	9	2013-09-16 11:23:28	44	27	Votre inscription à la demande de formation "La fiscalité de l'UPS (1) - 2011" a été acceptée.
157	9	2013-09-16 11:23:28	76	27	Vous avez indiqué avoir assisté à la formation "La fiscalité de l'UPS (1) - 2011".
158	9	2013-09-16 11:23:43	44	28	Votre inscription à la demande de formation "La fiscalité de l'UPS (2) - 2011" a été acceptée.
159	9	2013-09-16 11:23:43	76	28	Vous avez indiqué avoir assisté à la formation "La fiscalité de l'UPS (2) - 2011".
160	9	2013-09-16 11:24:03	44	24	Votre inscription à la demande de formation "SIFAC Référentiel - 2009" a été acceptée.
161	9	2013-09-16 11:24:03	76	24	Vous avez indiqué avoir assisté à la formation "SIFAC Référentiel - 2009".
162	9	2013-09-16 11:24:26	44	23	Votre inscription à la demande de formation "SIFAC Dépenses (1) - 2009" a été acceptée.
163	9	2013-09-16 11:24:26	76	23	Vous avez indiqué avoir assisté à la formation "SIFAC Dépenses (1) - 2009".
164	9	2013-09-16 11:24:35	44	25	Votre inscription à la demande de formation "SIFAC Missions Métiers - 2010" a été acceptée.
165	9	2013-09-16 11:24:35	76	25	Vous avez indiqué avoir assisté à la formation "SIFAC Missions Métiers - 2010".
166	9	2013-09-16 11:24:47	44	22	Votre inscription à la demande de formation "SIFAC Missions - 2009" a été acceptée.
167	9	2013-09-16 11:24:47	76	22	Vous avez indiqué avoir assisté à la formation "SIFAC Missions - 2009".
170	10	2013-09-16 11:39:07	28	29	Inscription à la demande "Formation initiale ACMO - 2009".
171	10	2013-09-16 11:40:08	28	30	Inscription à la demande "Nanoparticules".
172	10	2013-09-16 11:42:07	28	31	Inscription à la demande "Inventor - Initiation avancée".
173	10	2013-09-16 11:42:35	44	31	Votre inscription à la demande de formation "Inventor - Initiation avancée" a été acceptée.
174	10	2013-09-16 11:42:35	76	31	Vous avez indiqué avoir assisté à la formation "Inventor - Initiation avancée".
175	10	2013-09-16 11:42:48	44	30	Votre inscription à la demande de formation "Nanoparticules" a été acceptée.
176	10	2013-09-16 11:42:48	76	30	Vous avez indiqué avoir assisté à la formation "Nanoparticules".
177	10	2013-09-16 11:43:00	44	29	Votre inscription à la demande de formation "Formation initiale ACMO - 2009" a été acceptée.
178	10	2013-09-16 11:43:00	76	29	Vous avez indiqué avoir assisté à la formation "Formation initiale ACMO - 2009".
179	8	2013-09-16 11:49:53	44	3	Votre inscription à la demande de formation "Réunion 2010 des correspondants formation du CNRS DR14" a été acceptée.
180	8	2013-09-16 11:49:53	76	3	Vous avez indiqué avoir assisté à la formation "Réunion 2010 des correspondants formation du CNRS DR14".
181	5	2013-09-16 15:55:39	27	12	Ajout du besoin "Anglais".
182	5	2013-09-16 15:57:36	28	5	Inscription à la demande "Recyclage 2012 de l’Habilitation électrique du personnel non électricien".
268	34	2013-09-19 16:14:58	28	65	Inscription à la demande "PHP MySQL niveau 1 (2010)".
185	5	2013-09-16 16:03:10	44	5	Votre inscription à la demande de formation "Recyclage 2012 de l’Habilitation électrique du personnel non électricien" a été acceptée.
186	5	2013-09-16 16:03:10	76	5	Vous avez indiqué avoir assisté à la formation "Recyclage 2012 de l’Habilitation électrique du personnel non électricien".
187	11	2013-09-17 10:53:52	26	\N	Création de votre compte.
188	14	2013-09-17 15:52:29	28	32	Inscription à la demande "Anglais classique".
189	14	2013-09-17 15:54:14	44	32	Votre inscription à la demande de formation "Anglais classique" a été acceptée.
190	14	2013-09-17 15:54:14	76	32	Vous avez indiqué avoir assisté à la formation "Anglais classique".
191	13	2013-09-17 16:04:38	28	33	Inscription à la demande "Anglais débutant (avec le PRES)".
192	13	2013-09-17 16:05:43	44	33	Votre inscription à la demande de formation "Anglais débutant (avec le PRES)" a été acceptée.
193	13	2013-09-17 16:05:43	76	33	Vous avez indiqué avoir assisté à la formation "Anglais débutant (avec le PRES)".
194	12	2013-09-17 16:26:36	28	33	Inscription à la demande "Anglais débutant (avec le PRES)".
195	12	2013-09-17 16:27:21	44	33	Votre inscription à la demande de formation "Anglais débutant (avec le PRES)" a été acceptée.
196	12	2013-09-17 16:27:21	76	33	Vous avez indiqué avoir assisté à la formation "Anglais débutant (avec le PRES)".
215	15	2013-09-18 16:53:40	28	33	Inscription à la demande "Anglais débutant (avec le PRES)".
216	15	2013-09-18 16:54:06	44	33	Votre inscription à la demande de formation "Anglais débutant (avec le PRES)" a été acceptée.
217	15	2013-09-18 16:54:06	76	33	Vous avez indiqué avoir assisté à la formation "Anglais débutant (avec le PRES)".
221	19	2013-09-18 17:01:34	28	24	Inscription à la demande "SIFAC Référentiel - 2009".
222	19	2013-09-18 17:01:58	28	23	Inscription à la demande "SIFAC Dépenses (1) - 2009".
223	19	2013-09-18 17:02:26	28	22	Inscription à la demande "SIFAC Missions - 2009".
224	19	2013-09-18 17:04:18	44	24	Votre inscription à la demande de formation "SIFAC Référentiel - 2009" a été acceptée.
225	19	2013-09-18 17:04:18	76	24	Vous avez indiqué avoir assisté à la formation "SIFAC Référentiel - 2009".
226	19	2013-09-18 17:04:28	44	23	Votre inscription à la demande de formation "SIFAC Dépenses (1) - 2009" a été acceptée.
227	19	2013-09-18 17:04:28	76	23	Vous avez indiqué avoir assisté à la formation "SIFAC Dépenses (1) - 2009".
228	19	2013-09-18 17:04:39	44	22	Votre inscription à la demande de formation "SIFAC Missions - 2009" a été acceptée.
229	19	2013-09-18 17:04:39	76	22	Vous avez indiqué avoir assisté à la formation "SIFAC Missions - 2009".
230	5	2013-09-18 17:19:55	28	55	Inscription à la demande "Formation Thermographie".
231	5	2013-09-18 17:21:30	44	55	Votre inscription à la demande de formation "Formation Thermographie 2011" a été acceptée.
232	5	2013-09-18 17:21:30	76	55	Vous avez indiqué avoir assisté à la formation "Formation Thermographie 2011".
233	21	2013-09-18 17:25:41	28	9	Inscription à la demande "Recyclage Sauveteur Secours du Travail 2009".
234	21	2013-09-18 17:26:56	28	56	Inscription à la demande "Recyclage Sauveteur Secours du Travail 2010".
235	21	2013-09-18 17:28:08	44	9	Votre inscription à la demande de formation "Recyclage Sauveteur Secours du Travail 2009" a été acceptée.
236	21	2013-09-18 17:28:08	76	9	Vous avez indiqué avoir assisté à la formation "Recyclage Sauveteur Secours du Travail 2009".
237	21	2013-09-18 17:28:25	44	56	Votre inscription à la demande de formation "Recyclage Sauveteur Secours du Travail 2010" a été acceptée.
238	21	2013-09-18 17:28:25	76	56	Vous avez indiqué avoir assisté à la formation "Recyclage Sauveteur Secours du Travail 2010".
239	22	2013-09-18 17:35:17	28	57	Inscription à la demande "Word - Perfectionnement 2012".
240	22	2013-09-18 17:35:56	28	58	Inscription à la demande "Excel - Perfectionnement 2012".
241	22	2013-09-18 17:36:40	44	57	Votre inscription à la demande de formation "Word - Perfectionnement 2012" a été acceptée.
242	22	2013-09-18 17:36:40	92	57	Vous avez indiqué n'avoir pas assisté à la formation "Word - Perfectionnement 2012".
243	22	2013-09-18 17:37:02	44	58	Votre inscription à la demande de formation "Excel - Perfectionnement 2012" a été acceptée.
244	22	2013-09-18 17:37:02	92	58	Vous avez indiqué n'avoir pas assisté à la formation "Excel - Perfectionnement 2012".
249	33	2013-09-19 12:23:23	28	59	Inscription à la demande "AR14 Sécurité d'emploi des lasers".
250	33	2013-09-19 12:24:47	28	60	Inscription à la demande "Français Langue Etrangère 2009".
251	33	2013-09-19 12:26:06	44	59	Votre inscription à la demande de formation "AR14 Sécurité d'emploi des lasers" a été acceptée.
252	33	2013-09-19 12:26:06	76	59	Vous avez indiqué avoir assisté à la formation "AR14 Sécurité d'emploi des lasers".
253	33	2013-09-19 12:26:30	44	60	Votre inscription à la demande de formation "Français Langue Etrangère 2009" a été acceptée.
254	33	2013-09-19 12:26:30	76	60	Vous avez indiqué avoir assisté à la formation "Français Langue Etrangère 2009".
255	33	2013-09-19 15:55:12	28	61	Inscription à la demande "Français Langue Etrangère (24h) 2009".
258	33	2013-09-19 15:56:28	44	61	Votre inscription à la demande de formation "Français Langue Etrangère (24h) 2009" a été acceptée.
259	33	2013-09-19 15:56:28	76	61	Vous avez indiqué avoir assisté à la formation "Français Langue Etrangère (24h) 2009".
260	34	2013-09-19 15:58:40	28	61	Inscription à la demande "Français Langue Etrangère (24h) 2009".
261	34	2013-09-19 16:01:31	28	62	Inscription à la demande "HTML XHTML CSS (2010)".
262	34	2013-09-19 16:03:56	44	61	Votre inscription à la demande de formation "Français Langue Etrangère (24h) 2009" a été acceptée.
263	34	2013-09-19 16:03:56	76	61	Vous avez indiqué avoir assisté à la formation "Français Langue Etrangère (24h) 2009".
264	34	2013-09-19 16:04:15	44	62	Votre inscription à la demande de formation "HTML XHTML CSS (2010)" a été acceptée.
265	34	2013-09-19 16:04:15	76	62	Vous avez indiqué avoir assisté à la formation "HTML XHTML CSS (2010)".
266	24	2013-09-19 16:12:48	28	63	Inscription à la demande "Perfectionnement thermographie IR (2011)".
267	34	2013-09-19 16:14:10	28	64	Inscription à la demande "Journée Réseau Plasmas Froids 2010".
269	34	2013-09-19 16:16:12	44	65	Votre inscription à la demande de formation "PHP MySQL niveau 1 (2010)" a été acceptée.
270	34	2013-09-19 16:16:12	76	65	Vous avez indiqué avoir assisté à la formation "PHP MySQL niveau 1 (2010)".
271	34	2013-09-19 16:17:00	44	64	Votre inscription à la demande de formation "Journée Réseau Plasmas Froids 2010" a été acceptée.
272	34	2013-09-19 16:17:00	76	64	Vous avez indiqué avoir assisté à la formation "Journée Réseau Plasmas Froids 2010".
273	24	2013-09-19 16:17:21	44	63	Votre inscription à la demande de formation "Perfectionnement thermographie IR (2011)" a été acceptée.
274	24	2013-09-19 16:17:21	76	63	Vous avez indiqué avoir assisté à la formation "Perfectionnement thermographie IR (2011)".
275	8	2013-09-19 16:22:13	28	18	Inscription à la demande "Réunion des correspondants formation du CNRS DR14 (2011)".
276	8	2013-09-19 16:22:34	44	18	Votre inscription à la demande de formation "Réunion des correspondants formation du CNRS DR14 (2011)" a été acceptée.
277	8	2013-09-19 16:22:34	76	18	Vous avez indiqué avoir assisté à la formation "Réunion des correspondants formation du CNRS DR14 (2011)".
278	35	2013-09-19 16:27:15	26	\N	Création de votre compte.
279	28	2013-09-19 17:17:07	28	66	Inscription à la demande "Formation risques chimiques (INPT-2011)".
280	28	2013-09-19 17:17:52	44	66	Votre inscription à la demande de formation "Formation risques chimiques (INPT-2011)" a été acceptée.
281	28	2013-09-19 17:17:52	76	66	Vous avez indiqué avoir assisté à la formation "Formation risques chimiques (INPT-2011)".
282	27	2013-09-19 17:20:03	28	67	Inscription à la demande "Anglais module complémentaire(2009)".
283	27	2013-09-19 17:22:02	44	67	Votre inscription à la demande de formation "Anglais module complémentaire(2009)" a été acceptée.
284	27	2013-09-19 17:22:02	76	67	Vous avez indiqué avoir assisté à la formation "Anglais module complémentaire(2009)".
285	23	2013-09-19 17:28:23	28	68	Inscription à la demande "Anglais débutant (30h avec le PRES) 2010".
286	23	2013-09-19 17:29:23	44	68	Votre inscription à la demande de formation "Anglais débutant (30h avec le PRES) 2010" a été acceptée.
287	23	2013-09-19 17:29:23	76	68	Vous avez indiqué avoir assisté à la formation "Anglais débutant (30h avec le PRES) 2010".
289	35	2013-09-19 17:37:42	28	70	Inscription à la demande "Kit SPIP contributeur".
290	35	2013-09-19 17:38:46	28	71	Inscription à la demande "Inventor".
291	35	2013-09-19 17:39:46	28	72	Inscription à la demande "Recyclage Sauveteur Secours du Travail".
293	35	2013-09-19 17:40:54	28	55	Inscription à la demande "Formation Thermographie 2011".
294	35	2013-09-19 17:41:24	28	31	Inscription à la demande "Inventor - Initiation avancée".
295	35	2013-09-19 17:47:32	44	31	Votre inscription à la demande de formation "Inventor - Initiation avancée" a été acceptée.
296	35	2013-09-19 17:47:59	44	55	Votre inscription à la demande de formation "Formation Thermographie 2011" a été acceptée.
297	35	2013-09-19 17:48:12	44	73	Votre inscription à la demande de formation "Formation Caméra thermique" a été acceptée.
301	36	2013-09-20 09:41:43	26	\N	Création de votre compte.
302	37	2013-09-20 09:43:45	26	\N	Création de votre compte.
303	36	2013-09-20 10:01:33	28	1	Inscription à la demande "Kit SPIP Contributeur 2009".
305	38	2013-09-20 10:02:38	26	\N	Création de votre compte.
306	39	2013-09-20 10:02:51	26	\N	Création de votre compte.
307	36	2013-09-20 10:03:04	28	75	Inscription à la demande "La publication scientifique en réseau: les archives ouvertes et le libre accès (2009)".
308	36	2013-09-20 10:03:40	28	76	Inscription à la demande "Zotero: un logiciel bibliographique (2009)".
309	36	2013-09-20 10:08:34	28	77	Inscription à la demande "Word : création de documents longs et structurés (2010)".
310	36	2013-09-20 10:09:10	28	78	Inscription à la demande "EXCEL 2003 : calculs avancés (2010)".
311	36	2013-09-20 10:09:35	28	79	Inscription à la demande "EXCEL 2003 : Gestion bases de données (2010)".
312	36	2013-09-20 10:10:23	28	80	Inscription à la demande "Rédaction du rapport d'activités (2010)".
313	36	2013-09-20 10:10:54	28	81	Inscription à la demande "Se préparer à l'oral d'un concours (2010)".
314	36	2013-09-20 10:11:30	28	82	Inscription à la demande "Veille et diffusion d'information sur internet avec le RSS (2010)".
315	36	2013-09-20 10:12:28	28	83	Inscription à la demande "Connaissances de l'UPS (2010)".
316	36	2013-09-20 10:13:31	28	84	Inscription à la demande "Formation Archives ouvertes HAL (2011)".
317	36	2013-09-20 10:14:47	28	85	Inscription à la demande "Révision Générale des Politiques Publiques (2011)".
318	36	2013-09-20 10:15:31	28	86	Inscription à la demande "Les évolutions de la Fonction Publique (2011)".
319	36	2013-09-20 10:16:27	28	87	Inscription à la demande "HTML5/CSS3 (2012)".
320	36	2013-09-20 10:16:57	28	88	Inscription à la demande "PHP/MySQL Niveau 1 (2012)".
321	36	2013-09-20 10:17:20	28	89	Inscription à la demande "PHP/MySQL Niveau 2 (2012)".
322	36	2013-09-20 10:18:09	28	90	Inscription à la demande "Catalogage des monographies imprimées (2012)".
323	36	2013-09-20 10:19:13	28	91	Inscription à la demande "Catalogage des monographies imprimées session 2 (2012)".
324	36	2013-09-20 10:20:02	28	92	Inscription à la demande "Horizon – Circulation initiation (2012)".
325	36	2013-09-20 10:20:32	28	93	Inscription à la demande "Horizon – Acquisition initiation (2012)".
326	36	2013-09-20 10:21:06	28	94	Inscription à la demande "Horizon – Catalogage (2012)".
327	36	2013-09-20 10:21:33	28	95	Inscription à la demande "Horizon – Bulletinage (2012)".
328	36	2013-09-20 10:22:44	44	94	Votre inscription à la demande de formation "Horizon – Catalogage (2012)" a été acceptée.
329	36	2013-09-20 10:23:12	44	95	Votre inscription à la demande de formation "Horizon – Bulletinage (2012)" a été acceptée.
330	36	2013-09-20 10:23:48	44	91	Votre inscription à la demande de formation "Catalogage des monographies imprimées session 2 (2012)" a été acceptée.
331	36	2013-09-20 10:24:04	44	92	Votre inscription à la demande de formation "Horizon – Circulation initiation (2012)" a été acceptée.
332	36	2013-09-20 10:24:22	44	93	Votre inscription à la demande de formation "Horizon – Acquisition initiation (2012)" a été acceptée.
333	36	2013-09-20 10:25:01	44	87	Votre inscription à la demande de formation "HTML5/CSS3 (2012)" a été acceptée.
334	36	2013-09-20 10:25:15	44	88	Votre inscription à la demande de formation "PHP/MySQL Niveau 1 (2012)" a été acceptée.
335	36	2013-09-20 10:25:28	44	89	Votre inscription à la demande de formation "PHP/MySQL Niveau 2 (2012)" a été acceptée.
337	36	2013-09-20 10:26:39	44	84	Votre inscription à la demande de formation "Formation Archives ouvertes HAL (2011)" a été acceptée.
338	36	2013-09-20 10:27:04	44	85	Votre inscription à la demande de formation "Révision Générale des Politiques Publiques (2011)" a été acceptée.
339	36	2013-09-20 10:27:19	44	86	Votre inscription à la demande de formation "Les évolutions de la Fonction Publique (2011)" a été acceptée.
340	36	2013-09-20 10:28:37	44	77	Votre inscription à la demande de formation "Word : création de documents longs et structurés (2010)" a été acceptée.
341	36	2013-09-20 10:28:57	44	78	Votre inscription à la demande de formation "EXCEL 2003 : calculs avancés (2010)" a été acceptée.
342	36	2013-09-20 10:29:07	44	79	Votre inscription à la demande de formation "EXCEL 2003 : Gestion bases de données (2010)" a été acceptée.
343	36	2013-09-20 10:29:26	44	80	Votre inscription à la demande de formation "Rédaction du rapport d'activités (2010)" a été acceptée.
344	36	2013-09-20 10:29:40	44	81	Votre inscription à la demande de formation "Se préparer à l'oral d'un concours (2010)" a été acceptée.
345	36	2013-09-20 10:30:02	44	82	Votre inscription à la demande de formation "Veille et diffusion d'information sur internet avec le RSS (2010)" a été acceptée.
346	36	2013-09-20 10:30:20	44	83	Votre inscription à la demande de formation "Connaissances de l'UPS (2010)" a été acceptée.
347	36	2013-09-20 10:31:08	44	1	Votre inscription à la demande de formation "Kit SPIP Contributeur 2009" a été acceptée.
349	36	2013-09-20 10:32:00	44	75	Votre inscription à la demande de formation "La publication scientifique en réseau: les archives ouvertes et le libre accès (2009)" a été acceptée.
350	36	2013-09-20 10:32:15	44	76	Votre inscription à la demande de formation "Zotero: un logiciel bibliographique (2009)" a été acceptée.
351	38	2013-09-20 10:35:44	28	96	Inscription à la demande "Puma, Cougar et marché mission (2009)".
352	38	2013-09-20 10:36:42	28	97	Inscription à la demande "SIFAC REFERENTIEL (2010)".
353	38	2013-09-20 10:37:46	28	98	Inscription à la demande "SIFAC Dépenses (2011)".
355	40	2013-09-20 10:41:49	26	\N	Création de votre compte.
357	38	2013-09-20 10:42:06	44	98	Votre inscription à la demande de formation "SIFAC Dépenses (2011)" a été acceptée.
358	38	2013-09-20 10:42:13	44	97	Votre inscription à la demande de formation "SIFAC REFERENTIEL (2010)" a été acceptée.
359	38	2013-09-20 10:42:21	44	96	Votre inscription à la demande de formation "Puma, Cougar et marché mission (2009)" a été acceptée.
360	39	2013-09-20 10:47:44	28	100	Inscription à la demande "XLAB débutants 1er module  (2009)".
361	39	2013-09-20 10:48:48	28	101	Inscription à la demande "XLAB débutants 2ème module  (2009)".
362	39	2013-09-20 10:50:06	28	102	Inscription à la demande "Rédaction du rapport d'activités session 2 (2010)".
363	39	2013-09-20 10:50:40	28	103	Inscription à la demande "Mieux comprendre aujourd'hui le fonctionnement de l'UE (2010)".
364	39	2013-09-20 10:50:57	28	97	Inscription à la demande "SIFAC REFERENTIEL (2010)".
365	39	2013-09-20 10:51:52	28	104	Inscription à la demande "Excel - Perfectionnement (2011)".
366	39	2013-09-20 10:52:30	44	104	Votre inscription à la demande de formation "Excel - Perfectionnement (2011)" a été acceptée.
367	38	2013-09-20 10:52:56	28	104	Inscription à la demande "Excel - Perfectionnement (2011)".
368	38	2013-09-20 10:53:11	44	104	Votre inscription à la demande de formation "Excel - Perfectionnement (2011)" a été acceptée.
369	39	2013-09-20 10:53:33	44	97	Votre inscription à la demande de formation "SIFAC REFERENTIEL (2010)" a été acceptée.
370	39	2013-09-20 10:53:58	44	103	Votre inscription à la demande de formation "Mieux comprendre aujourd'hui le fonctionnement de l'UE (2010)" a été acceptée.
371	39	2013-09-20 10:54:12	44	102	Votre inscription à la demande de formation "Rédaction du rapport d'activités session 2 (2010)" a été acceptée.
372	39	2013-09-20 10:54:27	44	101	Votre inscription à la demande de formation "XLAB débutants 2ème module  (2009)" a été acceptée.
373	39	2013-09-20 10:54:40	44	100	Votre inscription à la demande de formation "XLAB débutants 1er module  (2009)" a été acceptée.
374	36	2013-09-20 10:56:02	92	95	Vous avez indiqué n'avoir pas assisté à la formation "Horizon – Bulletinage (2012)".
375	36	2013-09-20 10:56:16	92	93	Vous avez indiqué n'avoir pas assisté à la formation "Horizon – Acquisition initiation (2012)".
376	36	2013-09-20 10:56:25	76	1	Vous avez indiqué avoir assisté à la formation "Kit SPIP Contributeur 2009".
378	36	2013-09-20 10:56:37	76	75	Vous avez indiqué avoir assisté à la formation "La publication scientifique en réseau: les archives ouvertes et le libre accès (2009)".
379	36	2013-09-20 10:56:44	76	76	Vous avez indiqué avoir assisté à la formation "Zotero: un logiciel bibliographique (2009)".
380	36	2013-09-20 10:56:50	76	77	Vous avez indiqué avoir assisté à la formation "Word : création de documents longs et structurés (2010)".
381	36	2013-09-20 10:57:02	76	78	Vous avez indiqué avoir assisté à la formation "EXCEL 2003 : calculs avancés (2010)".
382	36	2013-09-20 10:57:12	76	79	Vous avez indiqué avoir assisté à la formation "EXCEL 2003 : Gestion bases de données (2010)".
383	36	2013-09-20 10:57:52	76	80	Vous avez indiqué avoir assisté à la formation "Rédaction du rapport d'activités (2010)".
384	36	2013-09-20 10:57:57	76	81	Vous avez indiqué avoir assisté à la formation "Se préparer à l'oral d'un concours (2010)".
385	36	2013-09-20 10:58:03	76	82	Vous avez indiqué avoir assisté à la formation "Veille et diffusion d'information sur internet avec le RSS (2010)".
386	36	2013-09-20 10:58:08	76	83	Vous avez indiqué avoir assisté à la formation "Connaissances de l'UPS (2010)".
387	36	2013-09-20 10:58:12	76	84	Vous avez indiqué avoir assisté à la formation "Formation Archives ouvertes HAL (2011)".
388	36	2013-09-20 10:58:19	76	85	Vous avez indiqué avoir assisté à la formation "Révision Générale des Politiques Publiques (2011)".
389	36	2013-09-20 10:58:24	76	86	Vous avez indiqué avoir assisté à la formation "Les évolutions de la Fonction Publique (2011)".
390	36	2013-09-20 10:58:30	76	87	Vous avez indiqué avoir assisté à la formation "HTML5/CSS3 (2012)".
391	36	2013-09-20 10:58:34	76	88	Vous avez indiqué avoir assisté à la formation "PHP/MySQL Niveau 1 (2012)".
392	36	2013-09-20 10:58:39	76	89	Vous avez indiqué avoir assisté à la formation "PHP/MySQL Niveau 2 (2012)".
394	36	2013-09-20 10:58:49	76	91	Vous avez indiqué avoir assisté à la formation "Catalogage des monographies imprimées session 2 (2012)".
395	36	2013-09-20 10:58:53	76	92	Vous avez indiqué avoir assisté à la formation "Horizon – Circulation initiation (2012)".
396	36	2013-09-20 10:59:01	76	94	Vous avez indiqué avoir assisté à la formation "Horizon – Catalogage (2012)".
397	39	2013-09-20 11:00:04	28	105	Inscription à la demande "Equipier d'évacuation (INPT-2011)".
398	39	2013-09-20 11:00:54	28	106	Inscription à la demande "Préparation aux concours session 1 (2011)".
399	39	2013-09-20 11:01:19	28	107	Inscription à la demande "Préparation aux concours session 2 (2011)".
400	39	2013-09-20 11:01:42	28	86	Inscription à la demande "Les évolutions de la Fonction Publique (2011)".
401	39	2013-09-20 11:02:27	28	108	Inscription à la demande "Rédaction du rapport d'activités (2011)".
402	39	2013-09-20 11:02:57	44	108	Votre inscription à la demande de formation "Rédaction du rapport d'activités (2011)" a été acceptée.
403	39	2013-09-20 11:03:18	44	86	Votre inscription à la demande de formation "Les évolutions de la Fonction Publique (2011)" a été acceptée.
404	39	2013-09-20 11:03:35	44	107	Votre inscription à la demande de formation "Préparation aux concours session 2 (2011)" a été acceptée.
405	39	2013-09-20 11:03:50	44	106	Votre inscription à la demande de formation "Préparation aux concours session 1 (2011)" a été acceptée.
406	39	2013-09-20 11:04:03	44	105	Votre inscription à la demande de formation "Equipier d'évacuation (INPT-2011)" a été acceptée.
407	39	2013-09-20 11:04:41	28	98	Inscription à la demande "SIFAC Dépenses (2011)".
408	39	2013-09-20 11:05:36	28	109	Inscription à la demande "SIFAC Fiscalité (2012)".
409	39	2013-09-20 11:06:14	28	110	Inscription à la demande "Excel Consolidations et Tableaux Croisés Dynamiques (2013)".
410	39	2013-09-20 11:07:11	44	110	Votre inscription à la demande de formation "Excel Consolidations et Tableaux Croisés Dynamiques (2013)" a été acceptée.
411	39	2013-09-20 11:07:25	44	109	Votre inscription à la demande de formation "SIFAC Fiscalité (2012)" a été acceptée.
412	39	2013-09-20 11:07:37	44	98	Votre inscription à la demande de formation "SIFAC Dépenses (2011)" a été acceptée.
413	40	2013-09-20 11:10:29	28	9	Inscription à la demande "Recyclage Sauveteur Secours du Travail (INPT-2009)".
414	35	2013-09-20 11:14:34	28	111	Inscription à la demande "Journée thématique du vide (2011)".
416	35	2013-09-20 11:15:05	44	111	Votre inscription à la demande de formation "Journée thématique du vide (2011)" a été acceptée.
417	40	2013-09-20 11:15:27	28	111	Inscription à la demande "Journée thématique du vide (2011)".
418	40	2013-09-20 11:15:52	28	31	Inscription à la demande "Inventor - Initiation avancée".
419	40	2013-09-20 11:16:48	28	5	Inscription à la demande "Recyclage de l’Habilitation électrique du personnel non électricien (2012)".
421	40	2013-09-20 11:17:38	44	31	Votre inscription à la demande de formation "Inventor - Initiation avancée (2011)" a été acceptée.
422	40	2013-09-20 11:17:48	44	111	Votre inscription à la demande de formation "Journée thématique du vide (2011)" a été acceptée.
423	41	2013-09-20 11:24:37	26	\N	Création de votre compte.
424	42	2013-09-20 11:36:24	26	\N	Création de votre compte.
425	40	2013-09-20 13:04:49	76	31	Vous avez indiqué avoir assisté à la formation "Inventor - Initiation avancée (2011)".
426	40	2013-09-20 13:05:09	76	111	Vous avez indiqué avoir assisté à la formation "Journée thématique du vide (2011)".
430	35	2013-09-20 13:21:47	28	66	Inscription à la demande "Formation risques chimiques (INPT-2011)".
431	35	2013-09-20 13:22:38	28	113	Inscription à la demande "Journées de l'Optique (2012)".
433	35	2013-09-20 13:23:09	44	66	Votre inscription à la demande de formation "Formation risques chimiques (INPT-2011)" a été acceptée.
434	35	2013-09-20 13:23:47	44	113	Votre inscription à la demande de formation "Journées de l'Optique (2012)" a été acceptée.
435	35	2013-09-20 13:24:26	28	114	Inscription à la demande "Journées Techniques LabVIEW (2012)".
436	35	2013-09-20 13:25:17	28	115	Inscription à la demande "Formation assistant de prévention (2012)".
437	35	2013-09-20 13:26:24	28	116	Inscription à la demande "Préparation aux concours internes (CNRS - 2012)".
438	35	2013-09-20 13:27:06	28	117	Inscription à la demande "Formation Communication assistant de prévention (2012)".
439	35	2013-09-20 13:27:53	44	117	Votre inscription à la demande de formation "Formation Communication assistant de prévention (2012)" a été acceptée.
440	35	2013-09-20 13:28:09	44	116	Votre inscription à la demande de formation "Préparation aux concours internes (CNRS - 2012)" a été acceptée.
441	35	2013-09-20 13:28:24	44	115	Votre inscription à la demande de formation "Formation assistant de prévention (2012)" a été acceptée.
442	35	2013-09-20 13:28:41	44	114	Votre inscription à la demande de formation "Journées Techniques LabVIEW (2012)" a été acceptée.
445	41	2013-09-20 13:30:38	28	1	Inscription à la demande "Kit SPIP Contributeur (2009)".
448	36	2013-09-20 13:33:46	28	118	Inscription à la demande "Kit SPIP administrateur (2009)".
449	36	2013-09-20 13:34:08	44	118	Votre inscription à la demande de formation "Kit SPIP administrateur (2009)" a été acceptée.
450	36	2013-09-20 13:34:08	76	118	Vous avez indiqué avoir assisté à la formation "Kit SPIP administrateur (2009)".
451	41	2013-09-20 13:36:18	28	119	Inscription à la demande "HTML XHTML CSS session 2 (2012)".
453	41	2013-09-20 13:37:57	28	120	Inscription à la demande "Risques Chimiques - Nouvel étiquetage (2010)".
454	41	2013-09-20 13:38:47	28	121	Inscription à la demande "Gestion des déchets chimiques (2011)".
455	41	2013-09-20 13:39:28	28	66	Inscription à la demande "Formation risques chimiques (INPT-2011)".
456	41	2013-09-20 13:40:14	28	122	Inscription à la demande "Journée Nano (2012)".
457	41	2013-09-20 13:40:45	44	122	Votre inscription à la demande de formation "Journée Nano (2012)" a été acceptée.
458	41	2013-09-20 13:40:59	44	66	Votre inscription à la demande de formation "Formation risques chimiques (INPT-2011)" a été acceptée.
459	41	2013-09-20 13:40:59	76	66	Vous avez indiqué avoir assisté à la formation "Formation risques chimiques (INPT-2011)".
460	41	2013-09-20 13:41:14	44	121	Votre inscription à la demande de formation "Gestion des déchets chimiques (2011)" a été acceptée.
461	41	2013-09-20 13:41:28	44	120	Votre inscription à la demande de formation "Risques Chimiques - Nouvel étiquetage (2010)" a été acceptée.
462	42	2013-09-20 13:48:41	28	86	Inscription à la demande "Les évolutions de la Fonction Publique (2011)".
463	42	2013-09-20 13:49:00	28	85	Inscription à la demande "Révision Générale des Politiques Publiques (2011)".
464	42	2013-09-20 13:49:59	28	123	Inscription à la demande "SIFAC Dépenses (INPT-2011)".
465	42	2013-09-20 13:50:40	28	124	Inscription à la demande "SIFAC Missions (INPT - 2011)".
466	42	2013-09-20 13:51:11	28	125	Inscription à la demande "SIFAC Référentiel (2011)".
467	42	2013-09-20 13:52:02	28	126	Inscription à la demande "Du projet professionnel au recrutement : comment réussir ? (2013)".
469	42	2013-09-20 13:52:39	44	125	Votre inscription à la demande de formation "SIFAC Référentiel (2011)" a été acceptée.
470	42	2013-09-20 13:52:53	44	124	Votre inscription à la demande de formation "SIFAC Missions (INPT - 2011)" a été acceptée.
471	42	2013-09-20 13:53:05	44	123	Votre inscription à la demande de formation "SIFAC Dépenses (INPT-2011)" a été acceptée.
473	42	2013-09-20 13:53:26	44	86	Votre inscription à la demande de formation "Les évolutions de la Fonction Publique (2011)" a été acceptée.
474	43	2013-09-20 13:55:32	26	\N	Création de votre compte.
475	43	2013-09-20 14:08:41	28	127	Inscription à la demande "Rencontre régionale du réseau des électronicien DR14 (2010)".
476	43	2013-09-20 14:09:26	28	128	Inscription à la demande "Initiation C embarqué microcontrôleurs (2010)".
477	43	2013-09-20 14:10:55	28	21	Inscription à la demande "Rencontres Régionales des Electroniciens - 2011".
479	43	2013-09-20 14:12:32	44	21	Votre inscription à la demande de formation "Rencontre régionale du réseau des électronicien DR14 (2011)" a été acceptée.
481	37	2013-09-20 14:13:30	28	127	Inscription à la demande "Rencontre régionale du réseau des électronicien DR14 (2010)".
482	37	2013-09-20 14:13:39	28	21	Inscription à la demande "Rencontre régionale du réseau des électronicien DR14 (2011)".
483	37	2013-09-20 14:14:42	28	129	Inscription à la demande "Modelsim 2010 (2010)".
487	44	2013-09-20 14:23:09	26	\N	Création de votre compte.
488	2	2013-09-20 14:23:19	28	130	Inscription à la demande "17èmes Rencontre régionale du réseau des électronicien DR14 (2013)".
489	2	2013-09-20 14:23:40	44	130	Votre inscription à la demande de formation "17èmes Rencontre régionale du réseau des électronicien DR14 (2013)" a été acceptée.
490	2	2013-09-20 14:23:40	76	130	Vous avez indiqué avoir assisté à la formation "17èmes Rencontre régionale du réseau des électronicien DR14 (2013)".
491	43	2013-09-20 14:24:48	28	130	Inscription à la demande "17èmes Rencontre régionale du réseau des électronicien DR14 (2013)".
492	43	2013-09-20 14:25:09	44	130	Votre inscription à la demande de formation "17èmes Rencontre régionale du réseau des électronicien DR14 (2013)" a été acceptée.
493	37	2013-09-20 14:25:24	28	130	Inscription à la demande "17èmes Rencontre régionale du réseau des électronicien DR14 (2013)".
494	37	2013-09-20 14:25:43	44	130	Votre inscription à la demande de formation "17èmes Rencontre régionale du réseau des électronicien DR14 (2013)" a été acceptée.
495	44	2013-09-20 14:26:44	28	131	Inscription à la demande "AR14 Initiation Pic Electronicien (2009)".
496	44	2013-09-20 14:27:20	28	127	Inscription à la demande "14èmes Rencontre régionale du réseau des électronicien DR14 (2010)".
497	44	2013-09-20 14:27:55	28	132	Inscription à la demande "Outils Cadence : ALLEGRO PCB DESIGNER (2010)".
498	44	2013-09-20 14:28:34	28	133	Inscription à la demande "Membre CHS (2010)".
499	44	2013-09-20 14:28:59	28	21	Inscription à la demande "15èmes Rencontre régionale du réseau des électronicien DR14 (2011)".
500	44	2013-09-20 14:29:37	28	134	Inscription à la demande "Rencontres Nationales des Electroniciens (2011)".
501	44	2013-09-20 14:30:07	44	134	Votre inscription à la demande de formation "Rencontres Nationales des Electroniciens (2011)" a été acceptée.
502	44	2013-09-20 14:30:17	44	21	Votre inscription à la demande de formation "15èmes Rencontre régionale du réseau des électronicien DR14 (2011)" a été acceptée.
503	44	2013-09-20 14:31:20	44	133	Votre inscription à la demande de formation "Membre CHS (2010)" a été acceptée.
504	44	2013-09-20 14:31:34	44	132	Votre inscription à la demande de formation "Outils Cadence : ALLEGRO PCB DESIGNER (2010)" a été acceptée.
505	44	2013-09-20 14:31:47	44	127	Votre inscription à la demande de formation "14èmes Rencontre régionale du réseau des électronicien DR14 (2010)" a été acceptée.
506	44	2013-09-20 14:32:04	44	131	Votre inscription à la demande de formation "AR14 Initiation Pic Electronicien (2009)" a été acceptée.
507	35	2013-09-20 15:35:57	76	113	Vous avez indiqué avoir assisté à la formation "Journées de l'Optique (2012)".
509	36	2013-09-20 16:41:20	44	90	Votre inscription à la demande de formation "Catalogage des monographies imprimées session 1 (2012)" a été acceptée.
510	36	2013-09-20 16:41:20	92	90	Vous avez indiqué n'avoir pas assisté à la formation "Catalogage des monographies imprimées session 1 (2012)".
511	41	2013-09-23 08:55:35	76	120	Vous avez indiqué avoir assisté à la formation "Risques Chimiques - Nouvel étiquetage (2010)".
512	46	2013-09-25 13:07:15	26	\N	Création de votre compte.
513	46	2013-09-27 08:47:21	28	136	Inscription à la demande "13èmes Rencontre régionale du réseau des électronicien DR14 (2009)".
514	46	2013-09-27 08:49:48	28	137	Inscription à la demande "Recyclage Sauveteur Secours du Travail, session septembre 2009 (CNRS-2009)".
515	46	2013-09-27 08:50:30	44	136	Votre inscription à la demande de formation "13èmes Rencontre régionale du réseau des électronicien DR14 (2009)" a été acceptée.
516	46	2013-09-27 08:51:00	44	137	Votre inscription à la demande de formation "Recyclage Sauveteur Secours du Travail, session septembre 2009 (CNRS-2009)" a été acceptée.
517	46	2013-09-27 08:52:45	28	138	Inscription à la demande "Anglais classique (CNRS-2009)".
518	46	2013-09-27 08:54:04	28	139	Inscription à la demande "PSIM pour utilisateurs avancés (2011)".
519	46	2013-09-27 08:54:39	44	138	Votre inscription à la demande de formation "Anglais classique (CNRS-2009)" a été acceptée.
521	46	2013-09-27 08:55:35	28	20	Inscription à la demande "Sécurité incendie - manipulation d'extincteur (2011)".
522	46	2013-09-27 08:56:02	28	105	Inscription à la demande "Equipier d'évacuation (INPT-2011)".
523	46	2013-09-27 08:57:20	28	140	Inscription à la demande "CEM des mesures physiques (2012)".
525	46	2013-09-27 08:59:29	28	141	Inscription à la demande "Propriété intellectuelle, propriété industrielle, brevets et valorisation des résultats de la recherche (2012)".
527	46	2013-09-27 09:00:13	44	105	Votre inscription à la demande de formation "Equipier d'évacuation (INPT-2011)" a été acceptée.
528	46	2013-09-27 09:00:26	44	140	Votre inscription à la demande de formation "CEM des mesures physiques (2012)" a été acceptée.
530	46	2013-09-27 09:00:52	44	141	Votre inscription à la demande de formation "Propriété intellectuelle, propriété industrielle, brevets et valorisation des résultats de la recherche (2012)" a été acceptée.
531	47	2013-09-27 15:08:24	26	\N	Création de votre compte.
532	48	2013-10-01 14:13:57	26	\N	Création de votre compte.
533	49	2013-10-03 11:15:45	26	\N	Création de votre compte.
534	50	2013-10-03 11:26:21	26	\N	Création de votre compte.
535	50	2013-10-03 11:41:30	28	143	Inscription à la demande "Journées Techniques LabVIEW (2013)".
536	50	2013-10-03 11:43:34	44	143	Votre inscription à la demande de formation "Journées Techniques LabVIEW (2013)" a été acceptée.
537	51	2013-10-03 15:35:00	26	\N	Création de votre compte.
538	49	2013-10-04 08:38:11	28	145	Inscription à la demande "Base Inventor Professionnel (CNRS - 2010)".
539	49	2013-10-04 08:39:51	28	146	Inscription à la demande "CFAO ESPRIT Fraisage 2 axes 1/2".
540	49	2013-10-04 08:40:22	44	145	Votre inscription à la demande de formation "Base Inventor Professionnel (CNRS - 2010)" a été acceptée.
541	49	2013-10-04 08:40:45	44	146	Votre inscription à la demande de formation "CFAO ESPRIT Fraisage 2 axes 1/2 (2012)" a été acceptée.
542	51	2013-10-04 08:43:58	28	31	Inscription à la demande "Inventor - Initiation avancée (2011)".
543	51	2013-10-04 08:46:17	44	31	Votre inscription à la demande de formation "Inventor - Initiation avancée et perfectionnement (2011)" a été acceptée.
544	51	2013-10-04 08:49:41	28	146	Inscription à la demande "CFAO ESPRIT Fraisage 2 axes 1/2 (2012)".
545	51	2013-10-04 08:49:56	44	146	Votre inscription à la demande de formation "CFAO ESPRIT Fraisage 2 axes 1/2 (2012)" a été acceptée.
546	4	2013-10-04 09:14:55	28	148	Inscription à la demande "Journée PLUME (2009)".
547	4	2013-10-04 09:16:14	28	149	Inscription à la demande "Compilation bases de données pour la science (2010)".
548	4	2013-10-04 09:16:41	28	150	Inscription à la demande "Langage de scripts (2010)".
552	46	2013-10-04 09:22:08	28	72	Inscription à la demande "Recyclage Sauveteur Secours du Travail (CNRS 2010)".
553	46	2013-10-04 09:22:25	44	72	Votre inscription à la demande de formation "Recyclage Sauveteur Secours du Travail (CNRS 2010)" a été acceptée.
554	52	2013-10-04 10:47:17	26	\N	Création de votre compte.
556	52	2013-10-04 11:03:12	28	152	Inscription à la demande "Cours NetApp Fondamentaux d'administration (2012)".
557	52	2013-10-04 11:04:12	28	153	Inscription à la demande "Bonnes Pratiques Organisationnelles pour les Administrateurs Systèmes et Réseaux (ASR) (2013)".
559	52	2013-10-04 11:04:52	44	152	Votre inscription à la demande de formation "Cours NetApp Fondamentaux d'administration (2012)" a été acceptée.
672	57	2013-10-10 16:43:35	28	24	Inscription à la demande "SIFAC Référentiel - (2009)".
560	52	2013-10-04 11:05:06	44	153	Votre inscription à la demande de formation "Bonnes Pratiques Organisationnelles pour les Administrateurs Systèmes et Réseaux (ASR) (2013)" a été acceptée.
561	53	2013-10-04 11:21:37	26	\N	Création de votre compte.
562	53	2013-10-04 14:00:24	28	154	Inscription à la demande "Espagnol (2009)".
563	53	2013-10-04 14:00:57	28	76	Inscription à la demande "Zotero: un logiciel bibliographique (2009)".
564	53	2013-10-04 14:02:03	28	155	Inscription à la demande "Acrobat Pro (2010)".
565	53	2013-10-04 14:02:48	28	156	Inscription à la demande "Environnement administratif des BU (2010)".
566	53	2013-10-04 14:04:06	28	157	Inscription à la demande "Recyclage Sauveteur Secours du Travail (UPS-2010)".
567	53	2013-10-04 14:04:46	28	158	Inscription à la demande "Réseau des correspondants Information/Communication (2010)".
568	53	2013-10-04 14:06:10	28	84	Inscription à la demande "Formation Archives ouvertes HAL (2011)".
569	53	2013-10-04 14:07:13	28	159	Inscription à la demande "Formation correspondants communication (2011)".
570	53	2013-10-04 14:07:46	44	76	Votre inscription à la demande de formation "Zotero: un logiciel bibliographique (2009)" a été acceptée.
571	53	2013-10-04 14:08:04	44	159	Votre inscription à la demande de formation "Formation correspondants communication (2011)" a été acceptée.
572	53	2013-10-04 14:08:16	44	84	Votre inscription à la demande de formation "Formation Archives ouvertes HAL (2011)" a été acceptée.
573	53	2013-10-04 14:08:30	44	158	Votre inscription à la demande de formation "Réseau des correspondants Information/Communication (2010)" a été acceptée.
574	53	2013-10-04 14:08:45	44	157	Votre inscription à la demande de formation "Recyclage Sauveteur Secours du Travail (UPS-2010)" a été acceptée.
575	53	2013-10-04 14:09:00	44	156	Votre inscription à la demande de formation "Environnement administratif des BU (2010)" a été acceptée.
576	53	2013-10-04 14:09:16	44	155	Votre inscription à la demande de formation "Acrobat Pro (2010)" a été acceptée.
577	53	2013-10-04 14:09:31	44	154	Votre inscription à la demande de formation "Espagnol (2009)" a été acceptée.
578	54	2013-10-07 10:38:04	26	\N	Création de votre compte.
579	54	2013-10-07 16:04:04	28	1	Inscription à la demande "Kit SPIP Contributeur (2009)".
580	54	2013-10-07 16:05:03	28	160	Inscription à la demande "HTML XHTML CSS session 1 (2009)".
581	54	2013-10-07 16:05:24	28	138	Inscription à la demande "Anglais classique (CNRS-2009)".
582	54	2013-10-07 16:06:20	28	161	Inscription à la demande "Introduction à la rhéologie et à la rhéométrie".
583	54	2013-10-07 16:07:06	28	20	Inscription à la demande "Sécurité incendie - manipulation d'extincteur (2011)".
584	54	2013-10-07 16:07:27	28	121	Inscription à la demande "Gestion des déchets chimiques (2011)".
585	54	2013-10-07 16:08:14	28	66	Inscription à la demande "Formation risques chimiques (INPT-2011)".
586	54	2013-10-07 16:09:10	28	162	Inscription à la demande "Prise de parole en public (CNRS - 2011)".
587	54	2013-10-07 16:13:32	28	163	Inscription à la demande "Responsable Laser (CNRS - 2012)".
588	54	2013-10-07 16:21:31	44	163	Votre inscription à la demande de formation "Responsable Laser (CNRS - 2012)" a été acceptée.
589	54	2013-10-07 16:21:47	44	162	Votre inscription à la demande de formation "Prise de parole en public (CNRS - 2011)" a été acceptée.
590	54	2013-10-07 16:22:00	44	66	Votre inscription à la demande de formation "Formation risques chimiques (INPT-2011)" a été acceptée.
591	54	2013-10-07 16:22:13	44	121	Votre inscription à la demande de formation "Gestion des déchets chimiques (2011)" a été acceptée.
593	54	2013-10-07 16:22:40	44	161	Votre inscription à la demande de formation "Introduction à la rhéologie et à la rhéométrie" a été acceptée.
594	54	2013-10-07 16:22:51	44	138	Votre inscription à la demande de formation "Anglais classique (CNRS-2009)" a été acceptée.
595	54	2013-10-07 16:23:08	44	160	Votre inscription à la demande de formation "HTML XHTML CSS session 1 (2009)" a été acceptée.
597	55	2013-10-10 11:04:58	28	164	Inscription à la demande "GRAAL, session 1 (2009)".
598	55	2013-10-10 11:05:26	28	165	Inscription à la demande "GRAAL, session 2 (2009)".
599	55	2013-10-10 11:06:07	28	166	Inscription à la demande "AOPGEE 5C (2009)".
600	55	2013-10-10 11:06:40	28	167	Inscription à la demande "Logiciel APOGEE".
601	55	2013-10-10 11:07:28	28	168	Inscription à la demande "Formation membres des instances représentatives des personnels (2009)".
602	55	2013-10-10 11:08:43	28	85	Inscription à la demande "Révision Générale des Politiques Publiques (2011)".
603	55	2013-10-10 11:09:33	28	169	Inscription à la demande "La mobilité internationale des scientifiques étrangers (2011)".
618	56	2013-10-10 11:47:19	28	170	Inscription à la demande "Connaissances de l'UPS (2009)".
621	56	2013-10-10 11:49:03	28	24	Inscription à la demande "SIFAC Référentiel - 2009".
624	56	2013-10-10 11:55:59	28	23	Inscription à la demande "SIFAC Dépenses (1) - 2009".
625	56	2013-10-10 11:56:32	28	22	Inscription à la demande "SIFAC Missions - 2009".
626	56	2013-10-10 11:57:31	28	171	Inscription à la demande "Mettre en œuvre les principes généraux de l'achat public à l'UPS (2009)".
633	56	2013-10-10 11:59:50	28	172	Inscription à la demande "Achat public : la procédure adaptée ou simplifiée (2009)".
634	56	2013-10-10 12:00:26	28	173	Inscription à la demande "Achat public : Formation à l'outil "Achatpublic.com" (2009)".
635	56	2013-10-10 12:01:18	28	174	Inscription à la demande "SIFAC Edition - Consultation (2009)".
642	56	2013-10-10 13:47:41	28	175	Inscription à la demande "Achat public : Le DCE/les éléments et pièces d'un marché (2009)".
643	56	2013-10-10 13:48:43	28	176	Inscription à la demande "Achat public : Formation à l'outil "Achatpublic.com", session 2 (2009)".
648	56	2013-10-10 13:50:48	28	25	Inscription à la demande "SIFAC Missions Métiers - 2010".
649	56	2013-10-10 13:51:12	28	26	Inscription à la demande "SIFAC Budget - 2010".
650	56	2013-10-10 13:51:49	28	177	Inscription à la demande "SIFAC Convention (2010)".
651	56	2013-10-10 13:52:26	28	178	Inscription à la demande "Achat public : Formation à l'outil "Achatpublic.com" (2010)".
660	56	2013-10-10 16:35:11	28	28	Inscription à la demande "La fiscalité de l'UPS (2) - 2011".
661	56	2013-10-10 16:36:15	28	179	Inscription à la demande "La fiscalité de l'UPS, 3 (2011)".
666	56	2013-10-10 16:40:52	28	169	Inscription à la demande "La mobilité internationale des scientifiques étrangers (2011)".
667	56	2013-10-10 16:41:32	28	180	Inscription à la demande "Information et complément de formation des correspondants achats (2011)".
673	57	2013-10-10 16:43:57	28	23	Inscription à la demande "SIFAC Dépenses (1) - (2009)".
680	57	2013-10-10 16:46:04	28	22	Inscription à la demande "SIFAC Missions (2009)".
683	57	2013-10-10 16:48:34	28	171	Inscription à la demande "Mettre en œuvre les principes généraux de l'achat public à l'UPS (2009)".
684	57	2013-10-10 16:48:37	28	172	Inscription à la demande "Achat public : la procédure adaptée ou simplifiée (2009)".
685	57	2013-10-10 16:48:39	28	176	Inscription à la demande "Achat public : Formation à l'outil "Achatpublic.com", session 2 (2009)".
686	57	2013-10-10 16:48:42	28	173	Inscription à la demande "Achat public : Formation à l'outil "Achatpublic.com" (2009)".
869	69	2013-10-14 10:11:40	28	30	Inscription à la demande "Nanoparticules (2010)".
871	94	2013-10-14 10:15:07	28	204	Inscription à la demande "2010- Méthode de conception d'un QCM".
873	76	2013-10-14 10:18:38	28	205	Inscription à la demande "2010- Produire et exploiter des QCM en ligne".
875	95	2013-10-14 10:26:36	28	132	Inscription à la demande "2010- Outils Cadence : ALLEGRO PCB DESIGNER".
931	103	2013-10-14 14:04:59	28	55	Inscription à la demande "2011- Formation Thermographie".
933	104	2013-10-14 14:06:23	28	55	Inscription à la demande "2011- Formation Thermographie".
935	106	2013-10-14 14:17:03	28	55	Inscription à la demande "2011- Formation Thermographie".
937	108	2013-10-14 14:28:20	28	55	Inscription à la demande "2011- Formation Thermographie".
938	82	2013-10-14 14:29:28	28	55	Inscription à la demande "2011- Formation Thermographie".
939	109	2013-10-14 14:30:49	28	55	Inscription à la demande "2011- Formation Thermographie".
940	110	2013-10-14 14:33:21	28	55	Inscription à la demande "2011- Formation Thermographie".
998	38	2013-10-14 15:41:03	76	97	Vous avez indiqué avoir assisté à la formation "2010- SIFAC Référentiel".
1048	71	2013-10-14 16:23:28	28	105	Inscription à la demande "2011- Equipier d'évacuation - INPT".
1049	68	2013-10-14 16:23:52	28	105	Inscription à la demande "2011- Equipier d'évacuation - INPT".
691	57	2013-10-10 16:51:49	28	181	Inscription à la demande "Achat public : Formation à l'outil "Achatpublic.com", session 3 (2009)".
698	57	2013-10-10 16:54:49	28	174	Inscription à la demande "SIFAC Edition - Consultation (2009)".
701	57	2013-10-10 16:55:43	28	175	Inscription à la demande "Achat public : Le DCE/les éléments et pièces d'un marché (2009)".
704	41	2013-10-10 16:57:00	44	1	Votre inscription à la demande de formation "Kit SPIP Contributeur (2009)" a été acceptée.
705	41	2013-10-10 16:57:00	76	1	Vous avez indiqué avoir assisté à la formation "Kit SPIP Contributeur (2009)".
706	54	2013-10-10 16:57:07	44	1	Votre inscription à la demande de formation "Kit SPIP Contributeur (2009)" a été acceptée.
707	54	2013-10-10 16:57:07	76	1	Vous avez indiqué avoir assisté à la formation "Kit SPIP Contributeur (2009)".
708	57	2013-10-10 16:58:09	28	182	Inscription à la demande "Achat public : Les éléments et pièces dans le cadre de l'analyse (2009)".
711	57	2013-10-11 15:19:58	28	183	Inscription à la demande "Achat public : les marchés négociés et la pratique de la négociation (2010)".
712	57	2013-10-11 15:20:26	28	184	Inscription à la demande "Achat public : la gestion des imprévus en phase d'exécution (2010)".
713	57	2013-10-11 15:20:52	28	185	Inscription à la demande "Achat public : formation complémentaire ordonnance (2010)".
720	57	2013-10-11 15:31:53	28	179	Inscription à la demande "La fiscalité de l'UPS, 3 (2011)".
721	57	2013-10-11 15:32:07	28	28	Inscription à la demande "La fiscalité de l'UPS, 2 (2011)".
722	57	2013-10-11 15:32:25	44	28	Votre inscription à la demande de formation "La fiscalité de l'UPS, 2 (2011)" a été acceptée.
723	56	2013-10-11 15:32:33	44	28	Votre inscription à la demande de formation "La fiscalité de l'UPS, 2 (2011)" a été acceptée.
724	56	2013-10-11 15:32:45	44	179	Votre inscription à la demande de formation "La fiscalité de l'UPS, 3 (2011)" a été acceptée.
725	57	2013-10-11 15:32:51	44	179	Votre inscription à la demande de formation "La fiscalité de l'UPS, 3 (2011)" a été acceptée.
726	57	2013-10-11 15:33:45	28	180	Inscription à la demande "Information et complément de formation des correspondants achats (2011)".
727	56	2013-10-11 15:33:58	44	180	Votre inscription à la demande de formation "Information et complément de formation des correspondants achats (2011)" a été acceptée.
728	57	2013-10-11 15:34:03	44	180	Votre inscription à la demande de formation "Information et complément de formation des correspondants achats (2011)" a été acceptée.
729	56	2013-10-11 15:36:06	44	169	Votre inscription à la demande de formation "La mobilité internationale des scientifiques étrangers (2011)" a été acceptée.
731	57	2013-10-11 15:36:32	44	172	Votre inscription à la demande de formation "Achat public : la procédure adaptée ou simplifiée (2009)" a été acceptée.
733	57	2013-10-11 15:36:54	44	176	Votre inscription à la demande de formation "Achat public : Formation à l'outil "Achatpublic.com", session 2 (2009)" a été acceptée.
734	57	2013-10-11 15:37:00	44	183	Votre inscription à la demande de formation "Achat public : les marchés négociés et la pratique de la négociation (2010)" a été acceptée.
736	57	2013-10-11 15:37:18	44	23	Votre inscription à la demande de formation "SIFAC Dépenses (1) - (2009)" a été acceptée.
737	57	2013-10-11 15:38:05	44	182	Votre inscription à la demande de formation "Achat public : Les éléments et pièces dans le cadre de l'analyse (2009)" a été acceptée.
739	57	2013-10-11 15:38:16	44	175	Votre inscription à la demande de formation "Achat public : Le DCE/les éléments et pièces d'un marché (2009)" a été acceptée.
742	57	2013-10-11 15:38:46	44	174	Votre inscription à la demande de formation "SIFAC Edition - Consultation (2009)" a été acceptée.
744	57	2013-10-11 15:39:15	44	24	Votre inscription à la demande de formation "SIFAC Référentiel - (2009)" a été acceptée.
745	57	2013-10-11 15:39:21	44	185	Votre inscription à la demande de formation "Achat public : formation complémentaire ordonnance (2010)" a été acceptée.
746	57	2013-10-11 15:39:28	44	22	Votre inscription à la demande de formation "SIFAC Missions (2009)" a été acceptée.
808	67	2013-10-11 17:38:50	44	192	Votre inscription à la demande de formation "2009- Formation des correspondants formation" a été acceptée.
809	67	2013-10-11 17:38:50	76	192	Vous avez indiqué avoir assisté à la formation "2009- Formation des correspondants formation".
810	67	2013-10-11 17:39:40	28	14	Inscription à la demande "Réunion des correspondants formation du CNRS DR14 (2009)".
867	92	2013-10-14 10:09:48	44	203	Votre inscription à la demande de formation "2010- Atelier ECOMOD" a été acceptée.
868	93	2013-10-14 10:09:55	44	203	Votre inscription à la demande de formation "2010- Atelier ECOMOD" a été acceptée.
870	69	2013-10-14 10:11:54	44	30	Votre inscription à la demande de formation "Nanoparticules (2010)" a été acceptée.
932	77	2013-10-14 14:05:22	28	55	Inscription à la demande "2011- Formation Thermographie".
934	105	2013-10-14 14:15:06	28	55	Inscription à la demande "2011- Formation Thermographie".
936	107	2013-10-14 14:26:51	28	55	Inscription à la demande "2011- Formation Thermographie".
750	57	2013-10-11 15:40:22	44	171	Votre inscription à la demande de formation "Mettre en œuvre les principes généraux de l'achat public à l'UPS (2009)" a été acceptée.
751	57	2013-10-11 15:40:33	44	181	Votre inscription à la demande de formation "Achat public : Formation à l'outil "Achatpublic.com", session 3 (2009)" a été acceptée.
753	57	2013-10-11 15:40:49	44	173	Votre inscription à la demande de formation "Achat public : Formation à l'outil "Achatpublic.com" (2009)" a été acceptée.
755	57	2013-10-11 15:41:00	44	184	Votre inscription à la demande de formation "Achat public : la gestion des imprévus en phase d'exécution (2010)" a été acceptée.
757	9	2013-10-11 15:41:11	44	26	Votre inscription à la demande de formation "SIFAC Budget (2010)" a été acceptée.
758	9	2013-10-11 15:41:11	76	26	Vous avez indiqué avoir assisté à la formation "SIFAC Budget (2010)".
760	58	2013-10-11 16:04:54	28	174	Inscription à la demande "SIFAC Edition - Consultation (2009)".
761	59	2013-10-11 16:15:43	28	186	Inscription à la demande "2009- Conception des convertisseurs d'énergie".
763	60	2013-10-11 16:25:24	28	187	Inscription à la demande "2009- Méthode de conception d'un QCM".
764	60	2013-10-11 16:27:44	44	187	Votre inscription à la demande de formation "2009- Méthode de conception d'un QCM" a été acceptée.
765	61	2013-10-11 16:27:55	28	187	Inscription à la demande "2009- Méthode de conception d'un QCM".
767	62	2013-10-11 16:31:11	28	188	Inscription à la demande "2009- Découverte de Moodle".
769	63	2013-10-11 16:32:50	28	188	Inscription à la demande "2009- Découverte de Moodle".
770	61	2013-10-11 16:33:22	28	188	Inscription à la demande "2009- Découverte de Moodle".
773	60	2013-10-11 16:34:20	28	188	Inscription à la demande "2009- Découverte de Moodle".
774	60	2013-10-11 16:34:36	44	188	Votre inscription à la demande de formation "2009- Découverte de Moodle" a été acceptée.
775	62	2013-10-11 16:34:51	44	188	Votre inscription à la demande de formation "2009- Découverte de Moodle" a été acceptée.
776	62	2013-10-11 16:34:51	76	188	Vous avez indiqué avoir assisté à la formation "2009- Découverte de Moodle".
777	64	2013-10-11 16:44:30	28	148	Inscription à la demande "Journée PLUME (2009)".
778	64	2013-10-11 16:44:59	44	148	Votre inscription à la demande de formation "2009- Journée PLUME" a été acceptée.
779	64	2013-10-11 16:44:59	76	148	Vous avez indiqué avoir assisté à la formation "2009- Journée PLUME".
780	55	2013-10-11 16:48:54	44	164	Votre inscription à la demande de formation "2009- GRAAL, session 1" a été acceptée.
781	55	2013-10-11 16:49:11	44	165	Votre inscription à la demande de formation "2009- GRAAL, session 2" a été acceptée.
782	58	2013-10-11 16:50:06	28	164	Inscription à la demande "2009- GRAAL, session 1".
784	55	2013-10-11 16:51:04	44	167	Votre inscription à la demande de formation "2009- Logiciel APOGEE" a été acceptée.
785	55	2013-10-11 16:51:22	44	166	Votre inscription à la demande de formation "2009- APOGEE 5C" a été acceptée.
786	65	2013-10-11 17:00:18	28	9	Inscription à la demande "2009- Recyclage Sauveteur Secours du Travail - INPT".
788	66	2013-10-11 17:01:18	28	9	Inscription à la demande "2009- Recyclage Sauveteur Secours du Travail - INPT".
790	67	2013-10-11 17:02:12	28	9	Inscription à la demande "2009- Recyclage Sauveteur Secours du Travail - INPT".
792	68	2013-10-11 17:03:09	28	9	Inscription à la demande "2009- Recyclage Sauveteur Secours du Travail - INPT".
793	68	2013-10-11 17:03:21	44	9	Votre inscription à la demande de formation "2009- Recyclage Sauveteur Secours du Travail - INPT" a été acceptée.
794	35	2013-10-11 17:05:00	28	137	Inscription à la demande "Recyclage Sauveteur Secours du Travail, session septembre 2009 (CNRS-2009)".
796	69	2013-10-11 17:08:07	28	137	Inscription à la demande "2009- Recyclage Sauveteur Secours du Travail - CNRS".
797	69	2013-10-11 17:08:40	44	137	Votre inscription à la demande de formation "2009- Recyclage Sauveteur Secours du Travail - CNRS" a été acceptée.
798	70	2013-10-11 17:11:25	28	137	Inscription à la demande "2009- Recyclage Sauveteur Secours du Travail - CNRS".
799	70	2013-10-11 17:11:46	44	137	Votre inscription à la demande de formation "2009- Recyclage Sauveteur Secours du Travail - CNRS" a été acceptée.
800	69	2013-10-11 17:15:50	28	189	Inscription à la demande "2009- Formation initiale ACMO - CNRS".
801	69	2013-10-11 17:16:09	44	189	Votre inscription à la demande de formation "2009- Formation initiale ACMO - CNRS" a été acceptée.
802	71	2013-10-11 17:17:17	28	189	Inscription à la demande "2009- Formation initiale ACMO - CNRS".
803	71	2013-10-11 17:17:44	44	189	Votre inscription à la demande de formation "2009- Formation initiale ACMO - CNRS" a été acceptée.
807	67	2013-10-11 17:38:39	28	192	Inscription à la demande "2009- Formation des correspondants formation".
811	67	2013-10-11 17:39:51	44	14	Votre inscription à la demande de formation "Réunion des correspondants formation du CNRS DR14 (2009)" a été acceptée.
812	67	2013-10-11 17:39:51	76	14	Vous avez indiqué avoir assisté à la formation "Réunion des correspondants formation du CNRS DR14 (2009)".
814	55	2013-10-11 17:53:09	44	168	Votre inscription à la demande de formation "Formation membres des instances représentatives des personnels (2009)" a été acceptée.
815	73	2013-10-11 17:55:10	28	16	Inscription à la demande "2009- Mieux connaitre le fonctionnement de l'Union Européenne".
817	73	2013-10-11 17:57:25	28	193	Inscription à la demande "2009- Matériaux pour l’énergie, Galerne".
819	74	2013-10-11 18:08:54	28	194	Inscription à la demande "2009- Manipulation des extincteurs - UPS".
821	75	2013-10-11 18:10:06	28	194	Inscription à la demande "2009- Manipulation des extincteurs - UPS".
823	76	2013-10-11 18:14:14	28	195	Inscription à la demande "2009- Développer la motivation de l'étudiant".
825	77	2013-10-14 08:47:56	28	196	Inscription à la demande "2009- 11èmes Rencontres Nationales du réseau des électronicien CNRS".
827	78	2013-10-14 08:52:25	28	197	Inscription à la demande "2009- Compétences en radioprotection - CNRS".
829	79	2013-10-14 09:03:27	28	198	Inscription à la demande "2010- Bases statistiques Module 1".
830	79	2013-10-14 09:03:45	44	198	Votre inscription à la demande de formation "2010- Bases statistiques Module 1" a été acceptée.
831	80	2013-10-14 09:14:27	28	199	Inscription à la demande "2010- Microscopie champ proche".
833	81	2013-10-14 09:24:23	28	199	Inscription à la demande "2010- Microscopie champ proche".
834	81	2013-10-14 09:24:44	44	199	Votre inscription à la demande de formation "2010- Microscopie champ proche" a été acceptée.
835	82	2013-10-14 09:31:34	28	200	Inscription à la demande "2010- Spectrométrie de masse".
837	63	2013-10-14 09:34:06	28	201	Inscription à la demande "2010- Atelier chimie poudre immergé dans plasma".
839	83	2013-10-14 09:39:25	28	201	Inscription à la demande "2010- Atelier chimie poudre immergé dans plasma".
840	84	2013-10-14 09:40:06	28	201	Inscription à la demande "2010- Atelier chimie poudre immergé dans plasma".
841	64	2013-10-14 09:40:34	28	201	Inscription à la demande "2010- Atelier chimie poudre immergé dans plasma".
842	64	2013-10-14 09:40:50	44	201	Votre inscription à la demande de formation "2010- Atelier chimie poudre immergé dans plasma" a été acceptée.
845	77	2013-10-14 09:45:36	28	202	Inscription à la demande "2010- 12èmes Rencontres Nationales du réseau des électronicien CNRS".
847	84	2013-10-14 09:49:30	28	64	Inscription à la demande "2010- Journée Réseau Plasmas Froids".
848	85	2013-10-14 09:51:24	28	64	Inscription à la demande "2010- Journée Réseau Plasmas Froids".
849	86	2013-10-14 09:52:07	28	64	Inscription à la demande "2010- Journée Réseau Plasmas Froids".
850	87	2013-10-14 09:54:11	28	64	Inscription à la demande "2010- Journée Réseau Plasmas Froids".
851	88	2013-10-14 09:55:32	28	64	Inscription à la demande "2010- Journée Réseau Plasmas Froids".
852	89	2013-10-14 09:56:20	28	64	Inscription à la demande "2010- Journée Réseau Plasmas Froids".
859	63	2013-10-14 10:03:57	28	203	Inscription à la demande "2010- Atelier ECOMOD".
860	90	2013-10-14 10:05:10	28	203	Inscription à la demande "2010- Atelier ECOMOD".
861	91	2013-10-14 10:07:25	28	203	Inscription à la demande "2010- Atelier ECOMOD".
862	92	2013-10-14 10:08:19	28	203	Inscription à la demande "2010- Atelier ECOMOD".
863	93	2013-10-14 10:09:05	28	203	Inscription à la demande "2010- Atelier ECOMOD".
866	91	2013-10-14 10:09:43	44	203	Votre inscription à la demande de formation "2010- Atelier ECOMOD" a été acceptée.
877	76	2013-10-14 10:44:23	28	206	Inscription à la demande "2010- Plongée sous-marine niveau 2".
879	96	2013-10-14 10:51:00	28	81	Inscription à la demande "2010- Se préparer à l'oral d'un concours".
880	96	2013-10-14 10:51:13	44	81	Votre inscription à la demande de formation "2010- Se préparer à l'oral d'un concours" a été acceptée.
881	96	2013-10-14 10:51:13	76	81	Vous avez indiqué avoir assisté à la formation "2010- Se préparer à l'oral d'un concours".
882	97	2013-10-14 10:58:16	28	207	Inscription à la demande "2010- Techniciens webconférences".
883	97	2013-10-14 10:58:53	44	207	Votre inscription à la demande de formation "2010- Techniciens webconférences" a été acceptée.
884	94	2013-10-14 10:59:15	28	207	Inscription à la demande "2010- Techniciens webconférences".
886	55	2013-10-14 11:06:10	44	169	Votre inscription à la demande de formation "La mobilité internationale des scientifiques étrangers (2011)" a été acceptée.
887	38	2013-10-14 11:09:33	28	208	Inscription à la demande "2011- SIFAC Missions - UPS".
888	38	2013-10-14 11:09:43	44	208	Votre inscription à la demande de formation "2011- SIFAC Missions - UPS" a été acceptée.
889	57	2013-10-14 11:14:29	28	209	Inscription à la demande "2012- ACHAT PUBLIC - Animation Réseau".
890	39	2013-10-14 11:15:26	28	210	Inscription à la demande "2012- SIFAC Fiscalité - INPT".
891	57	2013-10-14 11:15:41	44	209	Votre inscription à la demande de formation "2012- ACHAT PUBLIC - Animation Réseau" a été acceptée.
892	39	2013-10-14 11:15:56	44	210	Votre inscription à la demande de formation "2012- SIFAC Fiscalité - INPT" a été acceptée.
893	74	2013-10-14 11:19:15	28	211	Inscription à la demande "2010- SST Formation initiale - UPS".
895	65	2013-10-14 11:22:03	28	56	Inscription à la demande "2010- Recyclage Sauveteur Secours du Travail - INPT".
896	66	2013-10-14 11:22:46	28	56	Inscription à la demande "2010- Recyclage Sauveteur Secours du Travail - INPT".
897	67	2013-10-14 11:23:11	28	56	Inscription à la demande "2010- Recyclage Sauveteur Secours du Travail - INPT".
898	68	2013-10-14 11:23:56	28	56	Inscription à la demande "2010- Recyclage Sauveteur Secours du Travail - INPT".
899	98	2013-10-14 11:24:53	28	56	Inscription à la demande "2010- Recyclage Sauveteur Secours du Travail - INPT".
903	68	2013-10-14 11:25:38	44	56	Votre inscription à la demande de formation "2010- Recyclage Sauveteur Secours du Travail - INPT" a été acceptée.
905	69	2013-10-14 11:26:56	28	72	Inscription à la demande "2010- Recyclage Sauveteur Secours du Travail - CNRS".
906	69	2013-10-14 11:27:11	44	72	Votre inscription à la demande de formation "2010- Recyclage Sauveteur Secours du Travail - CNRS" a été acceptée.
907	70	2013-10-14 11:27:31	28	72	Inscription à la demande "2010- Recyclage Sauveteur Secours du Travail - CNRS".
908	70	2013-10-14 11:27:45	44	72	Votre inscription à la demande de formation "2010- Recyclage Sauveteur Secours du Travail - CNRS" a été acceptée.
909	82	2013-10-14 11:29:54	28	157	Inscription à la demande "2010- Recyclage Sauveteur Secours du Travail - UPS".
910	78	2013-10-14 11:30:23	28	157	Inscription à la demande "2010- Recyclage Sauveteur Secours du Travail - UPS".
911	99	2013-10-14 11:31:42	28	157	Inscription à la demande "2010- Recyclage Sauveteur Secours du Travail - UPS".
915	89	2013-10-14 11:34:13	28	212	Inscription à la demande "2010- Ethique et Déontologie".
917	100	2013-10-14 11:40:38	28	213	Inscription à la demande "2010- Anglais classique".
919	101	2013-10-14 11:48:42	28	214	Inscription à la demande "2010- Bien-être, Mal-être : Le point sur les risques psychosociaux".
921	102	2013-10-14 11:50:55	28	215	Inscription à la demande "2010- Conduite de réunion".
923	57	2013-10-14 12:09:52	28	216	Inscription à la demande "2011- Mieux connaitre le fonctionnement de l'UE".
924	57	2013-10-14 12:10:11	44	216	Votre inscription à la demande de formation "2011- Mieux connaitre le fonctionnement de l'UE" a été acceptée.
925	71	2013-10-14 12:11:50	28	217	Inscription à la demande "2011- Préparation aux concours internes - CNRS".
926	71	2013-10-14 12:12:03	44	217	Votre inscription à la demande de formation "2011- Préparation aux concours internes - CNRS" a été acceptée.
927	59	2013-10-14 12:14:50	28	21	Inscription à la demande "15èmes Rencontre régionale du réseau des électronicien DR14 (2011)".
929	82	2013-10-14 14:02:12	28	73	Inscription à la demande "2011- Formation Caméra thermique".
930	82	2013-10-14 14:02:28	44	73	Votre inscription à la demande de formation "2011- Formation Caméra thermique" a été acceptée.
941	77	2013-10-14 14:33:37	44	55	Votre inscription à la demande de formation "2011- Formation Thermographie" a été acceptée.
942	82	2013-10-14 14:33:42	44	55	Votre inscription à la demande de formation "2011- Formation Thermographie" a été acceptée.
943	103	2013-10-14 14:33:49	44	55	Votre inscription à la demande de formation "2011- Formation Thermographie" a été acceptée.
944	104	2013-10-14 14:33:54	44	55	Votre inscription à la demande de formation "2011- Formation Thermographie" a été acceptée.
945	105	2013-10-14 14:34:00	44	55	Votre inscription à la demande de formation "2011- Formation Thermographie" a été acceptée.
946	106	2013-10-14 14:34:06	44	55	Votre inscription à la demande de formation "2011- Formation Thermographie" a été acceptée.
947	107	2013-10-14 14:34:11	44	55	Votre inscription à la demande de formation "2011- Formation Thermographie" a été acceptée.
948	108	2013-10-14 14:34:16	44	55	Votre inscription à la demande de formation "2011- Formation Thermographie" a été acceptée.
949	109	2013-10-14 14:34:21	44	55	Votre inscription à la demande de formation "2011- Formation Thermographie" a été acceptée.
950	110	2013-10-14 14:34:26	44	55	Votre inscription à la demande de formation "2011- Formation Thermographie" a été acceptée.
951	82	2013-10-14 14:51:05	28	218	Inscription à la demande "2011- Conception et utilisation d'un réacteur a plasma de type industriel".
952	82	2013-10-14 14:51:34	44	218	Votre inscription à la demande de formation "2011- Conception et utilisation d'un réacteur a plasma de type industriel" a été acceptée.
953	77	2013-10-14 14:55:33	28	219	Inscription à la demande "2011- Electronique et instrumentation pour le vivant".
954	77	2013-10-14 14:56:05	28	220	Inscription à la demande "2011- Séminaire Réseau Plasmas Froids".
955	77	2013-10-14 14:56:20	44	220	Votre inscription à la demande de formation "2011- Séminaire Réseau Plasmas Froids" a été acceptée.
956	77	2013-10-14 14:56:32	44	219	Votre inscription à la demande de formation "2011- Electronique et instrumentation pour le vivant" a été acceptée.
957	111	2013-10-14 14:58:47	28	139	Inscription à la demande "2011- PSIM pour utilisateurs avancés".
958	66	2013-10-14 14:59:12	28	139	Inscription à la demande "2011- PSIM pour utilisateurs avancés".
959	59	2013-10-14 14:59:43	28	139	Inscription à la demande "2011- PSIM pour utilisateurs avancés".
960	112	2013-10-14 15:00:27	28	139	Inscription à la demande "2011- PSIM pour utilisateurs avancés".
964	112	2013-10-14 15:00:59	44	139	Votre inscription à la demande de formation "2011- PSIM pour utilisateurs avancés" a été acceptée.
965	112	2013-10-14 15:00:59	76	139	Vous avez indiqué avoir assisté à la formation "2011- PSIM pour utilisateurs avancés".
966	59	2013-10-14 15:01:04	44	139	Votre inscription à la demande de formation "2011- PSIM pour utilisateurs avancés" a été acceptée.
967	59	2013-10-14 15:01:04	76	139	Vous avez indiqué avoir assisté à la formation "2011- PSIM pour utilisateurs avancés".
968	66	2013-10-14 15:01:08	44	139	Votre inscription à la demande de formation "2011- PSIM pour utilisateurs avancés" a été acceptée.
969	66	2013-10-14 15:01:08	76	139	Vous avez indiqué avoir assisté à la formation "2011- PSIM pour utilisateurs avancés".
970	46	2013-10-14 15:01:12	44	139	Votre inscription à la demande de formation "2011- PSIM pour utilisateurs avancés" a été acceptée.
971	46	2013-10-14 15:01:12	76	139	Vous avez indiqué avoir assisté à la formation "2011- PSIM pour utilisateurs avancés".
972	111	2013-10-14 15:01:17	44	139	Votre inscription à la demande de formation "2011- PSIM pour utilisateurs avancés" a été acceptée.
973	111	2013-10-14 15:01:17	76	139	Vous avez indiqué avoir assisté à la formation "2011- PSIM pour utilisateurs avancés".
974	82	2013-10-14 15:02:29	28	111	Inscription à la demande "Journée thématique du vide (2011)".
975	113	2013-10-14 15:03:29	28	111	Inscription à la demande "Journée thématique du vide (2011)".
976	114	2013-10-14 15:04:44	28	111	Inscription à la demande "Journée thématique du vide (2011)".
977	82	2013-10-14 15:05:02	44	111	Votre inscription à la demande de formation "Journée thématique du vide (2011)" a été acceptée.
980	115	2013-10-14 15:26:12	28	221	Inscription à la demande "2011- Langage C++".
981	115	2013-10-14 15:26:50	44	221	Votre inscription à la demande de formation "2011- Langage C++" a été acceptée.
982	115	2013-10-14 15:26:50	92	221	Vous avez indiqué n'avoir pas assisté à la formation "2011- Langage C++".
983	52	2013-10-14 15:27:41	28	222	Inscription à la demande "2011- Gestion et contribution de contenus avec SharePoint 2010".
984	52	2013-10-14 15:27:55	44	222	Votre inscription à la demande de formation "2011- Gestion et contribution de contenus avec SharePoint 2010" a été acceptée.
985	113	2013-10-14 15:34:24	28	31	Inscription à la demande "2011- Inventor - Initiation avancée et perfectionnement".
986	69	2013-10-14 15:34:53	28	31	Inscription à la demande "2011- Inventor - Initiation avancée et perfectionnement".
987	82	2013-10-14 15:35:45	28	31	Inscription à la demande "2011- Inventor - Initiation avancée et perfectionnement".
988	49	2013-10-14 15:36:22	28	31	Inscription à la demande "2011- Inventor - Initiation avancée et perfectionnement".
989	114	2013-10-14 15:36:44	28	31	Inscription à la demande "2011- Inventor - Initiation avancée et perfectionnement".
990	71	2013-10-14 15:37:11	28	31	Inscription à la demande "2011- Inventor - Initiation avancée et perfectionnement".
993	82	2013-10-14 15:37:36	44	31	Votre inscription à la demande de formation "2011- Inventor - Initiation avancée et perfectionnement" a été acceptée.
994	71	2013-10-14 15:37:48	44	31	Votre inscription à la demande de formation "2011- Inventor - Initiation avancée et perfectionnement" a été acceptée.
995	69	2013-10-14 15:37:53	44	31	Votre inscription à la demande de formation "2011- Inventor - Initiation avancée et perfectionnement" a été acceptée.
996	49	2013-10-14 15:38:06	44	31	Votre inscription à la demande de formation "2011- Inventor - Initiation avancée et perfectionnement" a été acceptée.
997	38	2013-10-14 15:40:33	76	96	Vous avez indiqué avoir assisté à la formation "2009- Puma, Cougar et marché mission".
999	39	2013-10-14 15:41:21	76	100	Vous avez indiqué avoir assisté à la formation "2009- XLAB débutants 1er module".
1000	38	2013-10-14 15:41:24	76	98	Vous avez indiqué avoir assisté à la formation "2011- SIFAC Dépenses".
1002	40	2013-10-14 15:41:58	44	5	Votre inscription à la demande de formation "2012- Recyclage de l’Habilitation électrique du personnel non électricien - INPT" a été acceptée.
1003	40	2013-10-14 15:41:58	76	5	Vous avez indiqué avoir assisté à la formation "2012- Recyclage de l’Habilitation électrique du personnel non électricien - INPT".
1004	39	2013-10-14 15:42:02	76	110	Vous avez indiqué avoir assisté à la formation "Excel Consolidations et Tableaux Croisés Dynamiques (2013)".
1005	39	2013-10-14 15:43:30	76	101	Vous avez indiqué avoir assisté à la formation "2009- XLAB débutants 2ème module".
1006	39	2013-10-14 15:43:51	76	102	Vous avez indiqué avoir assisté à la formation "2010- Rédaction du rapport d'activités, session 2".
1007	38	2013-10-14 15:43:54	76	208	Vous avez indiqué avoir assisté à la formation "2011- SIFAC Missions - UPS".
1008	39	2013-10-14 15:44:03	76	103	Vous avez indiqué avoir assisté à la formation "2010- Mieux comprendre aujourd'hui le fonctionnement de l'UE".
1009	39	2013-10-14 15:44:13	76	97	Vous avez indiqué avoir assisté à la formation "2010- SIFAC Référentiel".
1010	39	2013-10-14 15:44:22	76	104	Vous avez indiqué avoir assisté à la formation "2011- Excel - Perfectionnement".
1011	38	2013-10-14 15:44:47	76	104	Vous avez indiqué avoir assisté à la formation "2011- Excel - Perfectionnement".
1012	111	2013-10-14 15:46:48	28	5	Inscription à la demande "2012- Recyclage de l’Habilitation électrique du personnel non électricien - INPT".
1013	39	2013-10-14 15:46:58	76	106	Vous avez indiqué avoir assisté à la formation "2011- Préparation aux concours session 1".
1014	39	2013-10-14 15:47:37	76	109	Vous avez indiqué avoir assisté à la formation "SIFAC Fiscalité (2012)".
1015	39	2013-10-14 15:47:48	76	86	Vous avez indiqué avoir assisté à la formation "2011- Les évolutions de la Fonction Publique".
1016	116	2013-10-14 15:49:54	28	5	Inscription à la demande "2012- Recyclage de l’Habilitation électrique du personnel non électricien - INPT".
1017	11	2013-10-14 15:50:36	28	5	Inscription à la demande "2012- Recyclage de l’Habilitation électrique du personnel non électricien - INPT".
1018	116	2013-10-14 15:51:17	44	5	Votre inscription à la demande de formation "2012- Recyclage de l’Habilitation électrique du personnel non électricien - INPT" a été acceptée.
1019	116	2013-10-14 15:51:17	76	5	Vous avez indiqué avoir assisté à la formation "2012- Recyclage de l’Habilitation électrique du personnel non électricien - INPT".
1020	111	2013-10-14 15:51:24	44	5	Votre inscription à la demande de formation "2012- Recyclage de l’Habilitation électrique du personnel non électricien - INPT" a été acceptée.
1021	111	2013-10-14 15:51:24	76	5	Vous avez indiqué avoir assisté à la formation "2012- Recyclage de l’Habilitation électrique du personnel non électricien - INPT".
1022	11	2013-10-14 15:51:32	44	5	Votre inscription à la demande de formation "2012- Recyclage de l’Habilitation électrique du personnel non électricien - INPT" a été acceptée.
1023	11	2013-10-14 15:51:32	76	5	Vous avez indiqué avoir assisté à la formation "2012- Recyclage de l’Habilitation électrique du personnel non électricien - INPT".
1024	3	2013-10-14 15:54:56	28	223	Inscription à la demande "2011- Habilitation électrique du personnel non électricien - INPT".
1027	118	2013-10-14 16:07:38	28	223	Inscription à la demande "2011- Habilitation électrique du personnel non électricien - INPT".
1028	119	2013-10-14 16:08:04	28	223	Inscription à la demande "2011- Habilitation électrique du personnel non électricien - INPT".
1029	120	2013-10-14 16:08:40	28	223	Inscription à la demande "2011- Habilitation électrique du personnel non électricien - INPT".
1031	3	2013-10-14 16:17:12	44	223	Votre inscription à la demande de formation "2011- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
1032	3	2013-10-14 16:17:12	76	223	Vous avez indiqué avoir assisté à la formation "2011- Habilitation électrique du personnel non électricien - INPT".
1043	122	2013-10-14 16:21:01	28	223	Inscription à la demande "2011- Habilitation électrique du personnel non électricien - INPT".
1046	111	2013-10-14 16:22:27	28	105	Inscription à la demande "2011- Equipier d'évacuation - INPT".
1047	66	2013-10-14 16:22:56	28	105	Inscription à la demande "2011- Equipier d'évacuation - INPT".
1051	68	2013-10-14 16:24:10	44	105	Votre inscription à la demande de formation "2011- Equipier d'évacuation - INPT" a été acceptée.
1052	71	2013-10-14 16:24:16	44	105	Votre inscription à la demande de formation "2011- Equipier d'évacuation - INPT" a été acceptée.
1053	111	2013-10-14 16:24:24	44	105	Votre inscription à la demande de formation "2011- Equipier d'évacuation - INPT" a été acceptée.
1054	103	2013-10-14 16:30:34	28	66	Inscription à la demande "2011- Formation risques chimiques - INPT".
1055	81	2013-10-14 16:31:09	28	66	Inscription à la demande "2011- Formation risques chimiques - INPT".
1056	53	2013-10-14 16:35:30	76	159	Vous avez indiqué avoir assisté à la formation "2011- Formation correspondants communication".
1057	53	2013-10-14 16:36:02	76	84	Vous avez indiqué avoir assisté à la formation "2011- Formation Archives ouvertes HAL".
1058	53	2013-10-14 16:36:23	76	158	Vous avez indiqué avoir assisté à la formation "2010- Réseau des correspondants Information/Communication".
1059	53	2013-10-14 16:36:45	76	157	Vous avez indiqué avoir assisté à la formation "2010- Recyclage Sauveteur Secours du Travail - UPS".
1060	53	2013-10-14 16:37:03	76	156	Vous avez indiqué avoir assisté à la formation "2010- Environnement administratif des BU".
1061	53	2013-10-14 16:37:24	76	155	Vous avez indiqué avoir assisté à la formation "2010- Acrobat Pro".
1062	53	2013-10-14 16:37:40	76	76	Vous avez indiqué avoir assisté à la formation "2009- Zotero: un logiciel bibliographique".
1063	53	2013-10-14 16:38:05	76	154	Vous avez indiqué avoir assisté à la formation "2009- Espagnol".
1064	110	2013-10-14 17:19:10	28	66	Inscription à la demande "2011- Formation risques chimiques - INPT".
1065	123	2013-10-14 17:29:10	28	66	Inscription à la demande "2011- Formation risques chimiques - INPT".
1066	124	2013-10-14 17:29:33	28	66	Inscription à la demande "2011- Formation risques chimiques - INPT".
1067	125	2013-10-14 17:29:57	28	66	Inscription à la demande "2011- Formation risques chimiques - INPT".
1068	126	2013-10-14 17:30:22	28	66	Inscription à la demande "2011- Formation risques chimiques - INPT".
1069	127	2013-10-14 17:30:52	28	66	Inscription à la demande "2011- Formation risques chimiques - INPT".
1070	128	2013-10-14 17:31:17	28	66	Inscription à la demande "2011- Formation risques chimiques - INPT".
1071	129	2013-10-14 17:31:47	28	66	Inscription à la demande "2011- Formation risques chimiques - INPT".
1072	130	2013-10-14 17:32:26	28	66	Inscription à la demande "2011- Formation risques chimiques - INPT".
1073	131	2013-10-14 17:32:57	28	66	Inscription à la demande "2011- Formation risques chimiques - INPT".
1074	103	2013-10-14 17:33:27	44	66	Votre inscription à la demande de formation "2011- Formation risques chimiques - INPT" a été acceptée.
1075	110	2013-10-14 17:33:33	44	66	Votre inscription à la demande de formation "2011- Formation risques chimiques - INPT" a été acceptée.
1076	81	2013-10-14 17:33:38	44	66	Votre inscription à la demande de formation "2011- Formation risques chimiques - INPT" a été acceptée.
1077	123	2013-10-14 17:33:44	44	66	Votre inscription à la demande de formation "2011- Formation risques chimiques - INPT" a été acceptée.
1078	124	2013-10-14 17:33:51	44	66	Votre inscription à la demande de formation "2011- Formation risques chimiques - INPT" a été acceptée.
1079	125	2013-10-14 17:33:57	44	66	Votre inscription à la demande de formation "2011- Formation risques chimiques - INPT" a été acceptée.
1080	126	2013-10-14 17:34:03	44	66	Votre inscription à la demande de formation "2011- Formation risques chimiques - INPT" a été acceptée.
1081	127	2013-10-14 17:34:10	44	66	Votre inscription à la demande de formation "2011- Formation risques chimiques - INPT" a été acceptée.
1082	128	2013-10-14 17:34:16	44	66	Votre inscription à la demande de formation "2011- Formation risques chimiques - INPT" a été acceptée.
1083	129	2013-10-14 17:34:21	44	66	Votre inscription à la demande de formation "2011- Formation risques chimiques - INPT" a été acceptée.
1084	130	2013-10-14 17:34:27	44	66	Votre inscription à la demande de formation "2011- Formation risques chimiques - INPT" a été acceptée.
1085	131	2013-10-14 17:34:32	44	66	Votre inscription à la demande de formation "2011- Formation risques chimiques - INPT" a été acceptée.
1086	111	2013-10-14 17:36:58	28	20	Inscription à la demande "2011- Sécurité incendie - manipulation d'extincteur - UPS".
1087	116	2013-10-14 17:37:31	28	20	Inscription à la demande "2011- Sécurité incendie - manipulation d'extincteur - INPT".
1088	132	2013-10-14 17:38:50	28	20	Inscription à la demande "2011- Sécurité incendie - manipulation d'extincteur - INPT".
1089	54	2013-10-14 17:39:02	44	20	Votre inscription à la demande de formation "2011- Sécurité incendie - manipulation d'extincteur - INPT" a été acceptée.
1090	54	2013-10-14 17:39:02	76	20	Vous avez indiqué avoir assisté à la formation "2011- Sécurité incendie - manipulation d'extincteur - INPT".
1091	46	2013-10-14 17:39:07	44	20	Votre inscription à la demande de formation "2011- Sécurité incendie - manipulation d'extincteur - INPT" a été acceptée.
1092	46	2013-10-14 17:39:07	76	20	Vous avez indiqué avoir assisté à la formation "2011- Sécurité incendie - manipulation d'extincteur - INPT".
1093	111	2013-10-14 17:39:13	44	20	Votre inscription à la demande de formation "2011- Sécurité incendie - manipulation d'extincteur - INPT" a été acceptée.
1094	111	2013-10-14 17:39:13	76	20	Vous avez indiqué avoir assisté à la formation "2011- Sécurité incendie - manipulation d'extincteur - INPT".
1095	116	2013-10-14 17:39:19	44	20	Votre inscription à la demande de formation "2011- Sécurité incendie - manipulation d'extincteur - INPT" a été acceptée.
1096	116	2013-10-14 17:39:19	76	20	Vous avez indiqué avoir assisté à la formation "2011- Sécurité incendie - manipulation d'extincteur - INPT".
1097	132	2013-10-14 17:39:25	44	20	Votre inscription à la demande de formation "2011- Sécurité incendie - manipulation d'extincteur - INPT" a été acceptée.
1098	132	2013-10-14 17:39:25	76	20	Vous avez indiqué avoir assisté à la formation "2011- Sécurité incendie - manipulation d'extincteur - INPT".
1099	71	2013-10-14 17:40:24	28	225	Inscription à la demande "2011- Communication au profit des ACMO".
1100	71	2013-10-14 17:40:44	44	225	Votre inscription à la demande de formation "2011- Communication au profit des ACMO" a été acceptée.
1101	134	2013-10-14 17:43:58	28	226	Inscription à la demande "2011- Word Perfectionnement".
1103	35	2013-10-14 17:47:27	28	229	Inscription à la demande "2011- Recyclage Sauveteur Secours du Travail - CNRS".
1104	67	2013-10-14 17:47:48	28	227	Inscription à la demande "2011- Recyclage Sauveteur Secours du Travail - INPT".
1105	78	2013-10-14 17:48:07	28	228	Inscription à la demande "2011- Recyclage Sauveteur Secours du Travail - UPS".
1106	35	2013-10-14 17:48:26	44	229	Votre inscription à la demande de formation "2011- Recyclage Sauveteur Secours du Travail - CNRS" a été acceptée.
1107	67	2013-10-14 17:48:32	44	227	Votre inscription à la demande de formation "2011- Recyclage Sauveteur Secours du Travail - INPT" a été acceptée.
1109	82	2013-10-14 17:50:16	28	230	Inscription à la demande "2011- SST Formation initiale - UPS".
1110	74	2013-10-14 17:50:40	28	230	Inscription à la demande "2011- SST Formation initiale - UPS".
1112	82	2013-10-14 17:51:01	44	230	Votre inscription à la demande de formation "2011- SST Formation initiale - UPS" a été acceptée.
1113	115	2013-10-14 17:52:52	28	231	Inscription à la demande "2011- Portugais débutant".
1114	115	2013-10-14 17:53:22	28	232	Inscription à la demande "2011- Japonais débutant".
1115	115	2013-10-14 17:53:34	44	231	Votre inscription à la demande de formation "2011- Portugais débutant" a été acceptée.
1116	115	2013-10-14 17:53:46	44	232	Votre inscription à la demande de formation "2011- Japonais débutant" a été acceptée.
1118	135	2013-10-14 17:58:17	28	33	Inscription à la demande "2011- Anglais débutant, module de 30h - PRES".
1119	135	2013-10-14 17:58:31	44	33	Votre inscription à la demande de formation "2011- Anglais débutant, module de 30h - PRES" a été acceptée.
1120	135	2013-10-14 17:58:31	76	33	Vous avez indiqué avoir assisté à la formation "2011- Anglais débutant, module de 30h - PRES".
1121	116	2013-10-14 17:59:08	28	33	Inscription à la demande "2011- Anglais débutant, module de 30h - PRES".
1122	116	2013-10-14 17:59:21	44	33	Votre inscription à la demande de formation "2011- Anglais débutant, module de 30h - PRES" a été acceptée.
1123	136	2013-10-14 18:00:51	28	234	Inscription à la demande "2011- Anglais".
1124	136	2013-10-14 18:01:04	44	234	Votre inscription à la demande de formation "2011- Anglais" a été acceptée.
1125	137	2013-10-14 18:04:27	28	235	Inscription à la demande "2011- Hygiène et sécurité - Nouveaux entrants - INPT".
1126	137	2013-10-14 18:04:40	44	235	Votre inscription à la demande de formation "2011- Hygiène et sécurité - Nouveaux entrants - INPT" a été acceptée.
1127	82	2013-10-14 18:10:21	28	114	Inscription à la demande "Journées Techniques LabVIEW (2012)".
1128	82	2013-10-14 18:10:39	44	114	Votre inscription à la demande de formation "2012- Journées Techniques LabVIEW" a été acceptée.
1129	138	2013-10-14 18:15:02	28	57	Inscription à la demande "Word - Perfectionnement 2012".
1130	138	2013-10-14 18:15:17	28	58	Inscription à la demande "Excel - Perfectionnement 2012".
1131	138	2013-10-14 18:15:38	44	57	Votre inscription à la demande de formation "Word - Perfectionnement 2012" a été acceptée.
1132	138	2013-10-14 18:15:38	92	57	Vous avez indiqué n'avoir pas assisté à la formation "Word - Perfectionnement 2012".
1133	138	2013-10-14 18:15:57	44	58	Votre inscription à la demande de formation "Excel - Perfectionnement 2012" a été acceptée.
1134	138	2013-10-14 18:15:57	92	58	Vous avez indiqué n'avoir pas assisté à la formation "Excel - Perfectionnement 2012".
1135	35	2013-10-15 14:46:22	27	3	Ajout du besoin "Sauveteur Secouriste du Travail - Recyclage".
1136	128	2013-10-15 14:47:01	27	2	Ajout du besoin "Sauveteur Secouriste du Travail - Formation initiale".
1137	35	2013-10-15 14:51:53	28	236	Inscription à la demande "2009- Journées de l'optique".
1139	139	2013-10-15 14:54:07	28	237	Inscription à la demande "2012- Méthodes numériques pour les problèmes d'optimisation".
1140	139	2013-10-15 14:54:22	44	237	Votre inscription à la demande de formation "2012- Méthodes numériques pour les problèmes d'optimisation" a été acceptée.
1141	137	2013-10-15 14:57:14	28	238	Inscription à la demande "2012- Atelier analyse micro-structurale des couches minces".
1142	86	2013-10-15 14:57:40	28	238	Inscription à la demande "2012- Atelier analyse micro-structurale des couches minces".
1143	86	2013-10-15 14:57:51	44	238	Votre inscription à la demande de formation "2012- Atelier analyse micro-structurale des couches minces" a été acceptée.
1144	137	2013-10-15 14:57:57	44	238	Votre inscription à la demande de formation "2012- Atelier analyse micro-structurale des couches minces" a été acceptée.
1145	71	2013-10-15 15:06:39	28	146	Inscription à la demande "CFAO ESPRIT Fraisage 2 axes 1/2 (2012)".
1146	71	2013-10-15 15:06:54	44	146	Votre inscription à la demande de formation "CFAO ESPRIT Fraisage 2 axes 1/2 (2012)" a été acceptée.
1147	46	2013-10-15 15:18:01	28	239	Inscription à la demande "2012- Recyclage Sauveteur Secours du Travail - CNRS".
1148	46	2013-10-15 15:18:12	44	239	Votre inscription à la demande de formation "2012- Recyclage Sauveteur Secours du Travail - CNRS" a été acceptée.
1149	140	2013-10-15 15:19:56	28	5	Inscription à la demande "2012- Recyclage de l’Habilitation électrique du personnel non électricien - INPT".
1150	141	2013-10-15 15:20:55	28	5	Inscription à la demande "2012- Recyclage de l’Habilitation électrique du personnel non électricien - INPT".
1151	142	2013-10-15 15:22:49	28	5	Inscription à la demande "2012- Recyclage de l’Habilitation électrique du personnel non électricien - INPT".
1152	143	2013-10-15 15:23:44	28	5	Inscription à la demande "2012- Recyclage de l’Habilitation électrique du personnel non électricien - INPT".
1153	143	2013-10-15 15:24:07	44	5	Votre inscription à la demande de formation "2012- Recyclage de l’Habilitation électrique du personnel non électricien - INPT" a été acceptée.
1154	143	2013-10-15 15:24:07	76	5	Vous avez indiqué avoir assisté à la formation "2012- Recyclage de l’Habilitation électrique du personnel non électricien - INPT".
1155	142	2013-10-15 15:24:15	44	5	Votre inscription à la demande de formation "2012- Recyclage de l’Habilitation électrique du personnel non électricien - INPT" a été acceptée.
1156	142	2013-10-15 15:24:15	76	5	Vous avez indiqué avoir assisté à la formation "2012- Recyclage de l’Habilitation électrique du personnel non électricien - INPT".
1157	141	2013-10-15 15:24:22	44	5	Votre inscription à la demande de formation "2012- Recyclage de l’Habilitation électrique du personnel non électricien - INPT" a été acceptée.
1158	141	2013-10-15 15:24:22	76	5	Vous avez indiqué avoir assisté à la formation "2012- Recyclage de l’Habilitation électrique du personnel non électricien - INPT".
1159	140	2013-10-15 15:24:28	44	5	Votre inscription à la demande de formation "2012- Recyclage de l’Habilitation électrique du personnel non électricien - INPT" a été acceptée.
1160	140	2013-10-15 15:24:28	76	5	Vous avez indiqué avoir assisté à la formation "2012- Recyclage de l’Habilitation électrique du personnel non électricien - INPT".
1161	144	2013-10-15 15:26:07	28	240	Inscription à la demande "2012- Formation Netasq".
1163	124	2013-10-15 15:28:13	28	122	Inscription à la demande "Journée Nano (2012)".
1164	104	2013-10-15 15:28:43	28	122	Inscription à la demande "Journée Nano (2012)".
1165	129	2013-10-15 15:29:12	28	122	Inscription à la demande "Journée Nano (2012)".
1166	104	2013-10-15 15:29:40	44	122	Votre inscription à la demande de formation "Journée Nano (2012)" a été acceptée.
1169	69	2013-10-15 15:32:13	28	239	Inscription à la demande "2012- Recyclage Sauveteur Secours du Travail - CNRS".
1170	69	2013-10-15 15:32:25	44	239	Votre inscription à la demande de formation "2012- Recyclage Sauveteur Secours du Travail - CNRS" a été acceptée.
1171	59	2013-10-15 15:34:32	28	6	Inscription à la demande "CEM des équipements électroniques".
1172	59	2013-10-15 15:34:48	44	6	Votre inscription à la demande de formation "CEM des équipements électroniques" a été acceptée.
1173	59	2013-10-15 15:34:48	76	6	Vous avez indiqué avoir assisté à la formation "CEM des équipements électroniques".
1174	69	2013-10-15 15:38:01	28	241	Inscription à la demande "2012- Transport de produits chimiques".
1175	67	2013-10-15 15:39:09	28	242	Inscription à la demande "2012- Recyclage Sauveteur Secours du Travail - INPT".
1176	67	2013-10-15 16:10:10	44	242	Votre inscription à la demande de formation "2012- Recyclage Sauveteur Secours du Travail - INPT" a été acceptée.
1177	145	2013-10-15 16:10:45	28	243	Inscription à la demande "2012- Technologie des cellules photovoltaïques organiques et hybrides".
1178	145	2013-10-15 16:10:59	44	243	Votre inscription à la demande de formation "2012- Technologie des cellules photovoltaïques organiques et hybrides" a été acceptée.
1179	146	2013-10-15 16:12:05	28	244	Inscription à la demande "2012- Sécurité Laser".
1181	71	2013-10-15 16:13:47	28	245	Inscription à la demande "2012- Inventor - Initiation avancée".
1182	71	2013-10-15 16:14:25	44	245	Votre inscription à la demande de formation "2012- Inventor - Initiation avancée" a été acceptée.
1183	112	2013-10-15 16:19:17	28	246	Inscription à la demande "2012- SST Formation initiale - INPT".
1184	116	2013-10-15 16:19:37	28	246	Inscription à la demande "2012- SST Formation initiale - INPT".
1185	132	2013-10-15 16:20:04	28	246	Inscription à la demande "2012- SST Formation initiale - INPT".
1186	112	2013-10-15 16:20:17	44	246	Votre inscription à la demande de formation "2012- SST Formation initiale - INPT" a été acceptée.
1187	116	2013-10-15 16:20:22	44	246	Votre inscription à la demande de formation "2012- SST Formation initiale - INPT" a été acceptée.
1188	132	2013-10-15 16:20:27	44	246	Votre inscription à la demande de formation "2012- SST Formation initiale - INPT" a été acceptée.
1189	104	2013-10-15 16:22:21	28	247	Inscription à la demande "2013- Thermo-Mécanique".
1190	147	2013-10-15 16:23:05	28	247	Inscription à la demande "2013- Thermo-Mécanique".
1193	77	2013-10-15 16:24:36	28	248	Inscription à la demande "2013- Le management pour les responsables d'équipes et les chefs d'équipes".
1194	77	2013-10-15 16:24:46	44	248	Votre inscription à la demande de formation "2013- Le management pour les responsables d'équipes et les chefs d'équipes" a été acceptée.
1195	82	2013-10-15 16:25:48	28	249	Inscription à la demande "2013- Conception d'installations sous vide".
1196	82	2013-10-15 16:26:07	44	249	Votre inscription à la demande de formation "2013- Conception d'installations sous vide" a été acceptée.
1197	71	2013-10-15 16:27:23	28	250	Inscription à la demande "2013- Formation assistant de prévention".
1198	71	2013-10-15 16:27:40	28	144	Inscription à la demande "Journée thématique "Matériaux" (2013)".
1199	71	2013-10-15 16:28:06	44	250	Votre inscription à la demande de formation "2013- Formation assistant de prévention" a été acceptée.
1200	71	2013-10-15 16:28:26	44	144	Votre inscription à la demande de formation "2013- Journée thématique "Matériaux"" a été acceptée.
1201	2	2013-10-15 16:29:44	28	251	Inscription à la demande "2013- Concevoir son alimentation : la partie Puissance".
1202	2	2013-10-15 16:29:56	44	251	Votre inscription à la demande de formation "2013- Concevoir son alimentation : la partie Puissance" a été acceptée.
1203	2	2013-10-15 16:29:56	76	251	Vous avez indiqué avoir assisté à la formation "2013- Concevoir son alimentation : la partie Puissance".
1204	69	2013-10-15 16:30:50	28	252	Inscription à la demande "2013- Préparation à la retraite".
1205	69	2013-10-15 16:31:01	44	252	Votre inscription à la demande de formation "2013- Préparation à la retraite" a été acceptée.
1206	46	2013-10-15 16:33:05	28	147	Inscription à la demande "Inventor, par le réseau des électroniciens DR14 (2013)".
1207	46	2013-10-15 16:33:26	44	147	Votre inscription à la demande de formation "Inventor, par le réseau des électroniciens DR14 (2013)" a été acceptée.
1208	95	2013-10-15 16:33:52	28	147	Inscription à la demande "2013- Inventor, par le réseau des électroniciens DR14".
1209	43	2013-10-15 16:34:21	28	147	Inscription à la demande "2013- Inventor, par le réseau des électroniciens DR14".
1210	112	2013-10-15 16:34:40	28	147	Inscription à la demande "2013- Inventor, par le réseau des électroniciens DR14".
1211	112	2013-10-15 16:35:56	28	253	Inscription à la demande "2013- Altium Designer".
1212	112	2013-10-15 16:36:09	44	253	Votre inscription à la demande de formation "2013- Altium Designer" a été acceptée.
1324	157	2013-12-03 11:04:48	27	54	Ajout du besoin "Formation Microscopie à Force Atomique".
1213	43	2013-10-15 16:36:19	44	147	Votre inscription à la demande de formation "2013- Inventor, par le réseau des électroniciens DR14" a été acceptée.
1214	95	2013-10-15 16:36:26	44	147	Votre inscription à la demande de formation "2013- Inventor, par le réseau des électroniciens DR14" a été acceptée.
1215	112	2013-10-15 16:36:30	44	147	Votre inscription à la demande de formation "2013- Inventor, par le réseau des électroniciens DR14" a été acceptée.
1216	79	2013-10-15 16:46:41	76	198	Vous avez indiqué avoir assisté à la formation "2010- Bases statistiques Module 1".
1221	122	2013-10-15 17:06:50	44	223	Votre inscription à la demande de formation "2011- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
1223	69	2013-10-15 17:40:19	44	241	Votre inscription à la demande de formation "2012- Transport de produits chimiques" a été acceptée.
1224	55	2013-10-15 18:18:14	44	85	Votre inscription à la demande de formation "2011- Révision Générale des Politiques Publiques" a été acceptée.
1225	139	2013-10-15 18:35:22	76	237	Vous avez indiqué avoir assisté à la formation "2012- Méthodes numériques pour les problèmes d'optimisation".
1227	70	2013-10-16 08:16:34	76	137	Vous avez indiqué avoir assisté à la formation "2009- Recyclage Sauveteur Secours du Travail - CNRS".
1228	70	2013-10-16 08:16:42	76	72	Vous avez indiqué avoir assisté à la formation "2010- Recyclage Sauveteur Secours du Travail - CNRS".
1229	44	2013-10-16 08:27:52	28	130	Inscription à la demande "2013- 17èmes Rencontre régionale du réseau des électronicien DR14".
1230	44	2013-10-16 08:28:14	44	130	Votre inscription à la demande de formation "2013- 17èmes Rencontre régionale du réseau des électronicien DR14" a été acceptée.
1231	115	2013-10-16 08:42:59	76	231	Vous avez indiqué avoir assisté à la formation "2011- Portugais débutant".
1232	115	2013-10-16 08:43:06	76	232	Vous avez indiqué avoir assisté à la formation "2011- Japonais débutant".
1233	126	2013-10-16 08:46:20	76	66	Vous avez indiqué avoir assisté à la formation "2011- Formation risques chimiques - INPT".
1234	125	2013-10-16 08:48:00	76	66	Vous avez indiqué avoir assisté à la formation "2011- Formation risques chimiques - INPT".
1235	127	2013-10-16 08:54:13	76	66	Vous avez indiqué avoir assisté à la formation "2011- Formation risques chimiques - INPT".
1236	148	2013-10-16 08:58:43	28	223	Inscription à la demande "2011- Habilitation électrique du personnel non électricien - INPT".
1238	149	2013-10-16 09:12:25	28	223	Inscription à la demande "2011- Habilitation électrique du personnel non électricien - INPT".
1239	149	2013-10-16 09:12:47	44	223	Votre inscription à la demande de formation "2011- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
1240	64	2013-10-16 09:38:37	76	201	Vous avez indiqué avoir assisté à la formation "2010- Atelier chimie poudre immergé dans plasma".
1241	91	2013-10-16 09:42:57	76	203	Vous avez indiqué avoir assisté à la formation "2010- Atelier ECOMOD".
1242	93	2013-10-16 09:51:04	76	203	Vous avez indiqué avoir assisté à la formation "2010- Atelier ECOMOD".
1243	131	2013-10-16 10:08:21	76	66	Vous avez indiqué avoir assisté à la formation "2011- Formation risques chimiques - INPT".
1244	110	2013-10-16 10:14:04	76	55	Vous avez indiqué avoir assisté à la formation "2011- Formation Thermographie".
1245	110	2013-10-16 10:14:38	76	66	Vous avez indiqué avoir assisté à la formation "2011- Formation risques chimiques - INPT".
1246	68	2013-10-16 10:33:07	76	105	Vous avez indiqué avoir assisté à la formation "2011- Equipier d'évacuation - INPT".
1247	68	2013-10-16 10:33:27	76	56	Vous avez indiqué avoir assisté à la formation "2010- Recyclage Sauveteur Secours du Travail - INPT".
1248	68	2013-10-16 10:33:46	76	9	Vous avez indiqué avoir assisté à la formation "2009- Recyclage Sauveteur Secours du Travail - INPT".
1251	57	2013-10-16 10:49:11	76	216	Vous avez indiqué avoir assisté à la formation "2011- Mieux connaitre le fonctionnement de l'UE".
1252	57	2013-10-16 10:49:33	76	209	Vous avez indiqué avoir assisté à la formation "2012- ACHAT PUBLIC - Animation Réseau".
1253	57	2013-10-16 10:50:53	76	180	Vous avez indiqué avoir assisté à la formation "2011- Information et complément de formation des correspondants achats".
1254	57	2013-10-16 10:51:08	76	28	Vous avez indiqué avoir assisté à la formation "2011- La fiscalité de l'UPS, session 2".
1255	57	2013-10-16 10:51:21	76	179	Vous avez indiqué avoir assisté à la formation "2011- La fiscalité de l'UPS, session 3".
1256	57	2013-10-16 10:52:45	76	185	Vous avez indiqué avoir assisté à la formation "2010- Achat public : formation complémentaire ordonnance".
1257	57	2013-10-16 10:53:03	76	184	Vous avez indiqué avoir assisté à la formation "2010- Achat public : la gestion des imprévus en phase d'exécution".
1258	57	2013-10-16 10:53:14	76	183	Vous avez indiqué avoir assisté à la formation "2010- Achat public : les marchés négociés et la pratique de la négociation".
1259	57	2013-10-16 10:53:25	76	175	Vous avez indiqué avoir assisté à la formation "2009- Achat public : Le DCE/les éléments et pièces d'un marché".
1260	57	2013-10-16 10:53:36	76	181	Vous avez indiqué avoir assisté à la formation "2009- Achat public : Formation à l'outil "Achatpublic.com", session 3".
1261	68	2013-10-16 10:53:46	27	3	Ajout du besoin "Sauveteur Secouriste du Travail - Recyclage".
1262	57	2013-10-16 10:53:49	76	174	Vous avez indiqué avoir assisté à la formation "2009- SIFAC Edition - Consultation".
1263	57	2013-10-16 10:54:05	76	182	Vous avez indiqué avoir assisté à la formation "2009- Achat public : Les éléments et pièces dans le cadre de l'analyse".
1264	57	2013-10-16 10:54:19	76	173	Vous avez indiqué avoir assisté à la formation "2009- Achat public : Formation à l'outil "Achatpublic.com"".
1265	57	2013-10-16 10:54:31	76	176	Vous avez indiqué avoir assisté à la formation "2009- Achat public : Formation à l'outil "Achatpublic.com", session 2".
1266	68	2013-10-16 10:56:42	28	227	Inscription à la demande "2011- Recyclage Sauveteur Secours du Travail - INPT".
1267	57	2013-10-16 10:56:51	76	172	Vous avez indiqué avoir assisté à la formation "2009- Achat public : la procédure adaptée ou simplifiée".
1268	68	2013-10-16 10:57:02	44	227	Votre inscription à la demande de formation "2011- Recyclage Sauveteur Secours du Travail - INPT" a été acceptée.
1269	68	2013-10-16 10:57:02	76	227	Vous avez indiqué avoir assisté à la formation "2011- Recyclage Sauveteur Secours du Travail - INPT".
1270	57	2013-10-16 10:57:07	76	22	Vous avez indiqué avoir assisté à la formation "2009- SIFAC Missions".
1271	57	2013-10-16 10:57:22	76	23	Vous avez indiqué avoir assisté à la formation "2009- SIFAC Dépenses (1)".
1272	57	2013-10-16 10:57:33	76	24	Vous avez indiqué avoir assisté à la formation "2009- SIFAC Référentiel".
1273	68	2013-10-16 10:58:50	28	5	Inscription à la demande "2012- Recyclage de l’Habilitation électrique du personnel non électricien - INPT".
1274	68	2013-10-16 10:59:16	44	5	Votre inscription à la demande de formation "2012- Recyclage de l’Habilitation électrique du personnel non électricien - INPT" a été acceptée.
1275	68	2013-10-16 10:59:16	76	5	Vous avez indiqué avoir assisté à la formation "2012- Recyclage de l’Habilitation électrique du personnel non électricien - INPT".
1276	81	2013-10-16 12:05:34	76	66	Vous avez indiqué avoir assisté à la formation "2011- Formation risques chimiques - INPT".
1277	81	2013-10-16 12:05:47	76	199	Vous avez indiqué avoir assisté à la formation "2010- Microscopie champ proche".
1278	44	2013-10-16 12:51:07	76	130	Vous avez indiqué avoir assisté à la formation "2013- 17èmes Rencontre régionale du réseau des électronicien DR14".
1279	44	2013-10-16 12:51:17	76	134	Vous avez indiqué avoir assisté à la formation "2011- 13èmes Rencontres nationales du réseau des électronicien CNRS".
1280	44	2013-10-16 12:51:25	76	21	Vous avez indiqué avoir assisté à la formation "2011- 15èmes Rencontre régionale du réseau des électronicien DR14".
1281	44	2013-10-16 12:51:33	76	133	Vous avez indiqué avoir assisté à la formation "2010- Membre CHS".
1282	44	2013-10-16 12:51:47	76	132	Vous avez indiqué avoir assisté à la formation "2010- Outils Cadence : ALLEGRO PCB DESIGNER".
1283	44	2013-10-16 12:51:54	76	127	Vous avez indiqué avoir assisté à la formation "2010- 14èmes Rencontre régionale du réseau des électronicien DR14".
1284	44	2013-10-16 12:52:04	76	131	Vous avez indiqué avoir assisté à la formation "2009- AR14 Initiation Pic Electronicien".
1285	78	2013-10-16 13:11:43	27	46	Ajout du besoin "Personnel Compétent en Radioprotection - Recyclage".
1286	71	2013-10-16 13:13:36	76	189	Vous avez indiqué avoir assisté à la formation "2009- Formation initiale ACMO - CNRS".
1287	71	2013-10-16 13:13:56	76	217	Vous avez indiqué avoir assisté à la formation "2011- Préparation aux concours internes - CNRS".
1288	71	2013-10-16 13:14:18	76	31	Vous avez indiqué avoir assisté à la formation "2011- Inventor - Initiation avancée et perfectionnement".
1289	71	2013-10-16 13:14:28	76	105	Vous avez indiqué avoir assisté à la formation "2011- Equipier d'évacuation - INPT".
1290	71	2013-10-16 13:14:39	76	225	Vous avez indiqué avoir assisté à la formation "2011- Communication au profit des ACMO".
1291	71	2013-10-16 13:14:51	76	146	Vous avez indiqué avoir assisté à la formation "2012- CFAO ESPRIT Fraisage 2 axes 1/2".
1292	71	2013-10-16 13:15:05	76	245	Vous avez indiqué avoir assisté à la formation "2012- Inventor - Initiation avancée".
1293	71	2013-10-16 13:15:25	76	250	Vous avez indiqué avoir assisté à la formation "2013- Formation assistant de prévention".
1294	67	2013-10-16 13:47:10	27	33	Ajout du besoin "Modélisation orientée objet "Unified Modelling Language" (UML)".
1295	49	2013-10-16 14:19:04	76	31	Vous avez indiqué avoir assisté à la formation "2011- Inventor - Initiation avancée et perfectionnement".
1296	49	2013-10-16 14:22:59	76	146	Vous avez indiqué avoir assisté à la formation "2012- CFAO ESPRIT Fraisage 2 axes 1/2".
1297	49	2013-10-16 14:23:26	76	145	Vous avez indiqué avoir assisté à la formation "2010- Base Inventor Professionnel - CNRS".
1298	149	2013-10-17 15:03:05	76	223	Vous avez indiqué avoir assisté à la formation "2011- Habilitation électrique du personnel non électricien - INPT".
1299	50	2013-10-21 08:01:35	76	143	Vous avez indiqué avoir assisté à la formation "Journées Techniques LabVIEW (2013)".
1300	42	2013-10-25 12:12:12	27	26	Ajout du besoin "Word - Perfectionnement".
1301	42	2013-10-25 12:12:18	27	7	Ajout du besoin "Excel - Perfectionnement".
1302	46	2013-10-25 13:57:40	27	50	Ajout du besoin "thermique et hydraulique pour le dimensionnement de dissipateurs (refroidisseur à air forcé, plaque à eau)".
1375	53	2014-01-20 10:55:41	28	277	Inscription à la demande "Les Réseaux Sociaux pour la Culture Scientifique".
1303	46	2013-10-25 13:58:46	27	51	Ajout du besoin "simulations de géométries pour connaitre les valeurs des éléments parasites d'un câblage (bus bar) défini".
1304	150	2013-11-04 13:22:01	26	\N	Création de votre compte.
1305	50	2013-11-04 13:52:21	27	27	Ajout du besoin "Labview".
1307	152	2013-11-04 14:26:48	26	\N	Création de votre compte.
1308	71	2013-11-12 09:05:42	27	52	Ajout du besoin "Formation CFAO ESPRIT Module 4/5 Axes".
1309	153	2013-11-12 09:51:06	26	\N	Création de votre compte.
1310	49	2013-11-12 12:03:32	27	52	Ajout du besoin "Formation CFAO ESPRIT Module 4/5 Axes".
1311	51	2013-11-12 13:46:55	27	52	Ajout du besoin "Formation CFAO ESPRIT Module 4/5 Axes".
1312	81	2013-11-12 15:19:25	28	257	Inscription à la demande "2013- Formation à l'encadrement de thèse".
1313	81	2013-11-12 15:53:32	44	257	Votre inscription à la demande de formation "2013- Formation à l'encadrement de thèse" a été acceptée.
1314	50	2013-11-14 17:53:24	27	53	Ajout du besoin "ANSYS Maxwell".
1315	50	2013-11-14 17:54:37	27	12	Ajout du besoin "Anglais".
1316	154	2013-11-20 09:53:16	26	\N	Création de votre compte.
1317	155	2013-11-25 13:51:03	26	\N	Création de votre compte.
1318	156	2013-11-25 14:19:05	26	\N	Création de votre compte.
1320	156	2013-11-25 16:42:32	27	5	Ajout du besoin "Habilitation Electrique pour non électricien - Formation initiale".
1321	157	2013-12-02 17:35:03	26	\N	Création de votre compte.
1322	157	2013-12-03 11:01:38	27	12	Ajout du besoin "Anglais".
1323	157	2013-12-03 11:02:15	27	27	Ajout du besoin "Labview".
1325	81	2013-12-04 21:32:11	76	257	Vous avez indiqué avoir assisté à la formation "2013- Formation à l'encadrement de thèse".
1326	48	2013-12-05 09:32:31	27	4	Ajout du besoin "Risques chimiques".
1327	48	2013-12-05 09:33:55	43	4	Le besoin "Risques chimiques" a été pourvu.
1328	48	2013-12-05 09:34:07	27	4	Ajout du besoin "Risques chimiques".
1329	158	2013-12-05 10:58:22	26	\N	Création de votre compte.
1330	158	2013-12-05 15:52:25	28	269	Inscription à la demande "2013- Habilitation électrique du personnel non électricien - INPT".
1332	2	2013-12-11 11:59:22	28	191	Inscription à la demande "2013- Journée "Découverte" du logiciel INVENTOR".
1334	2	2013-12-11 12:00:16	44	191	Votre inscription à la demande de formation "2013- Journée "Découverte" du logiciel INVENTOR" a été acceptée.
1335	2	2013-12-11 12:00:16	76	191	Vous avez indiqué avoir assisté à la formation "2013- Journée "Découverte" du logiciel INVENTOR".
1336	111	2013-12-11 12:10:53	28	191	Inscription à la demande "2013- Journée "Découverte" du logiciel INVENTOR".
1337	103	2013-12-11 17:05:09	28	191	Inscription à la demande "2013- Journée "Découverte" du logiciel INVENTOR".
1338	54	2013-12-11 17:10:22	28	191	Inscription à la demande "2013- Journée "Découverte" du logiciel INVENTOR".
1339	155	2013-12-11 18:02:23	28	269	Inscription à la demande "2013- Habilitation électrique du personnel non électricien - INPT".
1341	103	2013-12-12 10:38:36	44	191	Votre inscription à la demande de formation "2013- Journée "Découverte" du logiciel INVENTOR" a été acceptée.
1342	103	2013-12-12 10:38:36	76	191	Vous avez indiqué avoir assisté à la formation "2013- Journée "Découverte" du logiciel INVENTOR".
1343	111	2013-12-12 10:38:41	44	191	Votre inscription à la demande de formation "2013- Journée "Découverte" du logiciel INVENTOR" a été acceptée.
1344	111	2013-12-12 10:38:41	76	191	Vous avez indiqué avoir assisté à la formation "2013- Journée "Découverte" du logiciel INVENTOR".
1345	54	2013-12-12 10:38:47	44	191	Votre inscription à la demande de formation "2013- Journée "Découverte" du logiciel INVENTOR" a été acceptée.
1346	54	2013-12-12 10:38:47	76	191	Vous avez indiqué avoir assisté à la formation "2013- Journée "Découverte" du logiciel INVENTOR".
1347	11	2013-12-16 17:45:22	28	191	Inscription à la demande "2013- Journée "Découverte" du logiciel INVENTOR".
1348	81	2013-12-16 17:46:05	28	191	Inscription à la demande "2013- Journée "Découverte" du logiciel INVENTOR".
1349	41	2013-12-16 17:46:40	28	191	Inscription à la demande "2013- Journée "Découverte" du logiciel INVENTOR".
1350	154	2013-12-16 17:47:07	28	191	Inscription à la demande "2013- Journée "Découverte" du logiciel INVENTOR".
1351	41	2013-12-16 17:48:05	44	191	Votre inscription à la demande de formation "2013- Journée "Découverte" du logiciel INVENTOR" a été acceptée.
1352	41	2013-12-16 17:48:05	76	191	Vous avez indiqué avoir assisté à la formation "2013- Journée "Découverte" du logiciel INVENTOR".
1353	154	2013-12-16 17:48:21	44	191	Votre inscription à la demande de formation "2013- Journée "Découverte" du logiciel INVENTOR" a été acceptée.
1354	154	2013-12-16 17:48:21	76	191	Vous avez indiqué avoir assisté à la formation "2013- Journée "Découverte" du logiciel INVENTOR".
1355	81	2013-12-16 17:48:34	44	191	Votre inscription à la demande de formation "2013- Journée "Découverte" du logiciel INVENTOR" a été acceptée.
1356	81	2013-12-16 17:48:34	76	191	Vous avez indiqué avoir assisté à la formation "2013- Journée "Découverte" du logiciel INVENTOR".
1357	11	2013-12-16 17:48:47	44	191	Votre inscription à la demande de formation "2013- Journée "Découverte" du logiciel INVENTOR" a été acceptée.
1358	11	2013-12-16 17:48:47	76	191	Vous avez indiqué avoir assisté à la formation "2013- Journée "Découverte" du logiciel INVENTOR".
1359	159	2013-12-16 17:54:35	28	191	Inscription à la demande "2013- Journée "Découverte" du logiciel INVENTOR".
1360	159	2013-12-16 17:54:53	44	191	Votre inscription à la demande de formation "2013- Journée "Découverte" du logiciel INVENTOR" a été acceptée.
1361	159	2013-12-16 17:54:53	76	191	Vous avez indiqué avoir assisté à la formation "2013- Journée "Découverte" du logiciel INVENTOR".
1362	160	2013-12-16 17:57:52	28	191	Inscription à la demande "2013- Journée "Découverte" du logiciel INVENTOR".
1363	160	2013-12-16 17:58:18	44	191	Votre inscription à la demande de formation "2013- Journée "Découverte" du logiciel INVENTOR" a été acceptée.
1364	160	2013-12-16 17:58:18	92	191	Vous avez indiqué n'avoir pas assisté à la formation "2013- Journée "Découverte" du logiciel INVENTOR".
1365	161	2013-12-16 18:00:54	28	191	Inscription à la demande "2013- Journée "Découverte" du logiciel INVENTOR".
1366	161	2013-12-16 18:01:12	44	191	Votre inscription à la demande de formation "2013- Journée "Découverte" du logiciel INVENTOR" a été acceptée.
1367	161	2013-12-16 18:01:12	76	191	Vous avez indiqué avoir assisté à la formation "2013- Journée "Découverte" du logiciel INVENTOR".
1368	49	2013-12-19 11:06:34	28	271	Inscription à la demande "logiciel ESPRIT".
1371	51	2013-12-19 11:22:34	28	271	Inscription à la demande "2014- logiciel ESPRIT 4eme axe".
1373	71	2014-01-13 18:32:37	28	271	Inscription à la demande "2014- logiciel ESPRIT 4eme axe".
1376	53	2014-01-20 10:57:52	44	277	Votre inscription à la demande de formation "2014- Les Réseaux Sociaux pour la Culture Scientifique" a été acceptée.
1377	162	2014-01-21 10:50:59	26	\N	Création de votre compte.
1378	162	2014-01-21 10:54:20	28	269	Inscription à la demande "2013- Habilitation électrique du personnel non électricien - INPT".
1380	162	2014-01-21 10:57:36	44	269	Votre inscription à la demande de formation "2013- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
1697	37	2014-04-09 11:03:06	76	129	Vous avez indiqué avoir assisté à la formation "2010- Modelsim 2010".
1381	162	2014-01-21 10:57:36	76	269	Vous avez indiqué avoir assisté à la formation "2013- Habilitation électrique du personnel non électricien - INPT".
1382	155	2014-01-21 10:57:48	44	269	Votre inscription à la demande de formation "2013- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
1383	155	2014-01-21 10:57:48	76	269	Vous avez indiqué avoir assisté à la formation "2013- Habilitation électrique du personnel non électricien - INPT".
1384	158	2014-01-21 10:57:54	44	269	Votre inscription à la demande de formation "2013- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
1385	158	2014-01-21 10:57:54	76	269	Vous avez indiqué avoir assisté à la formation "2013- Habilitation électrique du personnel non électricien - INPT".
1386	163	2014-01-21 11:23:05	26	\N	Création de votre compte.
1387	163	2014-01-21 11:35:23	28	269	Inscription à la demande "2013- Habilitation électrique du personnel non électricien - INPT".
1388	163	2014-01-21 11:36:38	44	269	Votre inscription à la demande de formation "2013- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
1389	163	2014-01-21 11:36:38	76	269	Vous avez indiqué avoir assisté à la formation "2013- Habilitation électrique du personnel non électricien - INPT".
1390	164	2014-01-21 13:30:00	26	\N	Création de votre compte.
1391	165	2014-01-21 15:38:32	26	\N	Création de votre compte.
1392	165	2014-01-22 08:42:00	28	269	Inscription à la demande "2013- Habilitation électrique du personnel non électricien - INPT".
1393	165	2014-01-22 08:42:20	44	269	Votre inscription à la demande de formation "2013- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
1394	165	2014-01-22 08:42:20	76	269	Vous avez indiqué avoir assisté à la formation "2013- Habilitation électrique du personnel non électricien - INPT".
1395	164	2014-01-22 08:44:10	28	269	Inscription à la demande "2013- Habilitation électrique du personnel non électricien - INPT".
1396	164	2014-01-22 08:44:22	44	269	Votre inscription à la demande de formation "2013- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
1397	164	2014-01-22 08:44:22	76	269	Vous avez indiqué avoir assisté à la formation "2013- Habilitation électrique du personnel non électricien - INPT".
1398	50	2014-01-22 08:45:22	28	269	Inscription à la demande "2013- Habilitation électrique du personnel non électricien - INPT".
1399	50	2014-01-22 08:45:35	44	269	Votre inscription à la demande de formation "2013- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
1400	50	2014-01-22 08:45:35	76	269	Vous avez indiqué avoir assisté à la formation "2013- Habilitation électrique du personnel non électricien - INPT".
1401	92	2014-01-22 08:50:01	76	203	Vous avez indiqué avoir assisté à la formation "2010- Atelier ECOMOD".
1402	69	2014-01-22 09:06:57	76	137	Vous avez indiqué avoir assisté à la formation "2009- Recyclage Sauveteur Secours du Travail - CNRS".
1403	69	2014-01-22 09:07:04	76	189	Vous avez indiqué avoir assisté à la formation "2009- Formation initiale ACMO - CNRS".
1404	69	2014-01-22 09:07:10	76	30	Vous avez indiqué avoir assisté à la formation "2010- Nanoparticules".
1405	69	2014-01-22 09:07:16	76	72	Vous avez indiqué avoir assisté à la formation "2010- Recyclage Sauveteur Secours du Travail - CNRS".
1406	69	2014-01-22 09:07:21	76	31	Vous avez indiqué avoir assisté à la formation "2011- Inventor - Initiation avancée et perfectionnement".
1407	69	2014-01-22 09:07:26	76	239	Vous avez indiqué avoir assisté à la formation "2012- Recyclage Sauveteur Secours du Travail - CNRS".
1408	69	2014-01-22 09:07:31	76	241	Vous avez indiqué avoir assisté à la formation "2012- Transport de produits chimiques".
1409	69	2014-01-22 09:07:37	76	252	Vous avez indiqué avoir assisté à la formation "2013- Préparation à la retraite".
1410	128	2014-01-22 09:08:24	76	66	Vous avez indiqué avoir assisté à la formation "2011- Formation risques chimiques - INPT".
1411	55	2014-01-22 09:20:36	76	164	Vous avez indiqué avoir assisté à la formation "2009- GRAAL, session 1".
1412	55	2014-01-22 09:20:42	76	165	Vous avez indiqué avoir assisté à la formation "2009- GRAAL, session 2".
1413	55	2014-01-22 09:20:47	76	166	Vous avez indiqué avoir assisté à la formation "2009- APOGEE 5C".
1414	55	2014-01-22 09:20:52	76	167	Vous avez indiqué avoir assisté à la formation "2009- Logiciel APOGEE".
1415	55	2014-01-22 09:20:57	76	168	Vous avez indiqué avoir assisté à la formation "2009- Formation membres des instances représentatives des personnels".
1416	55	2014-01-22 09:21:03	76	85	Vous avez indiqué avoir assisté à la formation "2011- Révision Générale des Politiques Publiques".
1417	55	2014-01-22 09:21:09	76	169	Vous avez indiqué avoir assisté à la formation "2011- La mobilité internationale des scientifiques étrangers".
1418	57	2014-01-22 09:50:26	76	171	Vous avez indiqué avoir assisté à la formation "2009- Mettre en œuvre les principes généraux de l'achat public à l'UPS".
1419	97	2014-01-22 11:02:21	76	207	Vous avez indiqué avoir assisté à la formation "2010- Techniciens webconférences".
1420	116	2014-01-22 13:50:38	76	33	Vous avez indiqué avoir assisté à la formation "2011- Anglais débutant, module de 30h - PRES".
1421	116	2014-01-22 13:51:02	76	246	Vous avez indiqué avoir assisté à la formation "2013- SST Formation initiale - INPT".
1422	104	2014-01-22 14:17:53	44	247	Votre inscription à la demande de formation "2013- Thermo-Mécanique" a été acceptée.
1423	104	2014-01-22 14:17:53	76	247	Vous avez indiqué avoir assisté à la formation "2013- Thermo-Mécanique".
1424	147	2014-01-22 14:17:58	44	247	Votre inscription à la demande de formation "2013- Thermo-Mécanique" a été acceptée.
1425	147	2014-01-22 14:17:58	76	247	Vous avez indiqué avoir assisté à la formation "2013- Thermo-Mécanique".
1426	112	2014-01-22 14:24:34	76	246	Vous avez indiqué avoir assisté à la formation "2013- SST Formation initiale - INPT".
1427	112	2014-01-22 14:25:40	76	147	Vous avez indiqué avoir assisté à la formation "2013- Inventor, par le réseau des électroniciens DR14".
1428	41	2014-01-22 16:41:18	92	122	Vous avez indiqué n'avoir pas assisté à la formation "2012- Journée Nano - CNRS".
1429	41	2014-01-22 16:42:23	92	121	Vous avez indiqué n'avoir pas assisté à la formation "2011- Gestion des déchets chimiques - UPS".
1430	60	2014-01-22 18:55:36	76	188	Vous avez indiqué avoir assisté à la formation "2009- Découverte de Moodle".
1431	60	2014-01-22 18:55:48	76	187	Vous avez indiqué avoir assisté à la formation "2009- Méthode de conception d'un QCM".
1432	54	2014-01-24 14:07:52	76	163	Vous avez indiqué avoir assisté à la formation "2012- Responsable Laser - CNRS".
1433	54	2014-01-24 14:08:07	76	162	Vous avez indiqué avoir assisté à la formation "2011- Prise de parole en public - CNRS".
1434	54	2014-01-24 14:08:17	76	66	Vous avez indiqué avoir assisté à la formation "2011- Formation risques chimiques - INPT".
1435	54	2014-01-24 14:08:30	76	121	Vous avez indiqué avoir assisté à la formation "2011- Gestion des déchets chimiques - UPS".
1436	54	2014-01-24 14:08:43	76	161	Vous avez indiqué avoir assisté à la formation "2010- Introduction à la rhéologie et à la rhéométrie".
1437	54	2014-01-24 14:08:52	76	138	Vous avez indiqué avoir assisté à la formation "2009- Anglais classique - CNRS".
1438	54	2014-01-24 14:09:01	76	160	Vous avez indiqué avoir assisté à la formation "2009- HTML XHTML CSS, session 1".
1439	156	2014-01-24 16:11:11	28	269	Inscription à la demande "2013- Habilitation électrique du personnel non électricien - INPT".
1440	156	2014-01-24 16:11:33	44	269	Votre inscription à la demande de formation "2013- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
1441	156	2014-01-24 16:11:33	76	269	Vous avez indiqué avoir assisté à la formation "2013- Habilitation électrique du personnel non électricien - INPT".
1442	52	2014-01-27 09:07:11	76	153	Vous avez indiqué avoir assisté à la formation "2013- Bonnes Pratiques Organisationnelles pour les Administrateurs Systèmes et Réseaux (ASR)".
1443	46	2014-01-30 08:10:14	76	136	Vous avez indiqué avoir assisté à la formation "2009- 13èmes Rencontres régionales du réseau des électronicien DR14".
1444	46	2014-01-30 08:14:19	76	147	Vous avez indiqué avoir assisté à la formation "2013- Inventor, par le réseau des électroniciens DR14".
1445	46	2014-01-30 08:15:58	76	239	Vous avez indiqué avoir assisté à la formation "2012- Recyclage Sauveteur Secours du Travail - CNRS".
1446	46	2014-01-30 08:16:53	76	140	Vous avez indiqué avoir assisté à la formation "2012- CEM des mesures physiques".
1447	46	2014-01-30 08:18:11	76	141	Vous avez indiqué avoir assisté à la formation "2012- Propriété intellectuelle, propriété industrielle, brevets et valorisation des résultats de la recherche".
1448	46	2014-01-30 08:22:38	76	137	Vous avez indiqué avoir assisté à la formation "2009- Recyclage Sauveteur Secours du Travail - CNRS".
1449	46	2014-01-30 08:24:12	76	138	Vous avez indiqué avoir assisté à la formation "2009- Anglais classique - CNRS".
1450	46	2014-01-30 08:25:53	76	105	Vous avez indiqué avoir assisté à la formation "2011- Equipier d'évacuation - INPT".
1451	46	2014-01-30 08:34:18	76	72	Vous avez indiqué avoir assisté à la formation "2010- Recyclage Sauveteur Secours du Travail - CNRS".
1452	52	2014-02-04 13:19:30	76	222	Vous avez indiqué avoir assisté à la formation "2011- Gestion et contribution de contenus avec SharePoint 2010".
1453	52	2014-02-04 13:20:05	76	152	Vous avez indiqué avoir assisté à la formation "2012- Cours NetApp Fondamentaux d'administration".
1454	166	2014-02-06 18:14:15	26	\N	Création de votre compte.
1455	166	2014-02-07 09:05:11	27	53	Ajout du besoin "ANSYS Maxwell".
1456	166	2014-02-07 09:08:44	28	269	Inscription à la demande "2013- Habilitation électrique du personnel non électricien - INPT".
1457	166	2014-02-07 09:20:38	44	269	Votre inscription à la demande de formation "2013- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
1458	166	2014-02-07 09:20:38	76	269	Vous avez indiqué avoir assisté à la formation "2013- Habilitation électrique du personnel non électricien - INPT".
1459	53	2014-02-10 15:58:59	28	279	Inscription à la demande "Word Perfectionnement".
1460	53	2014-02-10 16:01:57	28	280	Inscription à la demande "Professionalisation des membres du jury".
1461	53	2014-02-10 21:17:17	44	279	Votre inscription à la demande de formation "2014- Word Perfectionnement" a été acceptée.
1462	53	2014-02-10 21:18:51	44	280	Votre inscription à la demande de formation "2014- Professionalisation des membres du jury" a été acceptée.
1463	71	2014-02-14 09:17:22	76	144	Vous avez indiqué avoir assisté à la formation "2013- Journée thématique "Matériaux"".
1465	69	2014-03-04 14:59:43	28	289	Inscription à la demande "2013- Journée d'information des agents préventeur - CNRS".
1466	69	2014-03-04 15:00:09	44	289	Votre inscription à la demande de formation "2013- Journée d'information des agents préventeur - CNRS" a été acceptée.
1467	69	2014-03-04 15:00:09	76	289	Vous avez indiqué avoir assisté à la formation "2013- Journée d'information des agents préventeur - CNRS".
1468	37	2014-03-04 15:03:57	76	130	Vous avez indiqué avoir assisté à la formation "2013- 17èmes Rencontre régionale du réseau des électronicien DR14".
1469	37	2014-03-04 15:05:29	28	290	Inscription à la demande "2013- Programmation orientée objet - CNRS".
1470	37	2014-03-04 15:06:52	28	291	Inscription à la demande "2013- Transmission de données sans fil - CNRS".
1471	37	2014-03-04 15:07:58	28	292	Inscription à la demande "2013- Séminaire sur les processeur ARM - CNRS".
1472	37	2014-03-04 15:08:16	44	290	Votre inscription à la demande de formation "2013- Programmation orientée objet - CNRS" a été acceptée.
1473	37	2014-03-04 15:08:16	76	290	Vous avez indiqué avoir assisté à la formation "2013- Programmation orientée objet - CNRS".
1474	37	2014-03-04 15:08:43	44	291	Votre inscription à la demande de formation "2013- Transmission de données sans fil - CNRS" a été acceptée.
1475	37	2014-03-04 15:08:43	76	291	Vous avez indiqué avoir assisté à la formation "2013- Transmission de données sans fil - CNRS".
1476	37	2014-03-04 15:09:06	44	292	Votre inscription à la demande de formation "2013- Séminaire sur les processeur ARM - CNRS" a été acceptée.
1477	37	2014-03-04 15:09:06	76	292	Vous avez indiqué avoir assisté à la formation "2013- Séminaire sur les processeur ARM - CNRS".
1478	44	2014-03-04 15:12:58	28	293	Inscription à la demande "2013- Membres CHSCT session1 - CNRS".
1479	44	2014-03-04 15:13:21	28	294	Inscription à la demande "2013- Membres CHSCT session 2 - CNRS".
1480	44	2014-03-04 15:15:57	28	295	Inscription à la demande "2013- ACC Cellule Compétence - CNRS".
1481	44	2014-03-04 15:16:13	44	295	Votre inscription à la demande de formation "2013- ACC Cellule Compétence - CNRS" a été acceptée.
1482	44	2014-03-04 15:16:13	76	295	Vous avez indiqué avoir assisté à la formation "2013- ACC Cellule Compétence - CNRS".
1483	44	2014-03-04 15:16:26	44	294	Votre inscription à la demande de formation "2013- Membres CHSCT session 2 - CNRS" a été acceptée.
1484	44	2014-03-04 15:16:26	76	294	Vous avez indiqué avoir assisté à la formation "2013- Membres CHSCT session 2 - CNRS".
1485	44	2014-03-04 15:16:37	44	293	Votre inscription à la demande de formation "2013- Membres CHSCT session1 - CNRS" a été acceptée.
1486	44	2014-03-04 15:16:37	76	293	Vous avez indiqué avoir assisté à la formation "2013- Membres CHSCT session1 - CNRS".
1487	43	2014-03-04 15:17:36	76	147	Vous avez indiqué avoir assisté à la formation "2013- Inventor, par le réseau des électroniciens DR14".
1488	43	2014-03-04 15:17:49	76	130	Vous avez indiqué avoir assisté à la formation "2013- 17èmes Rencontre régionale du réseau des électronicien DR14".
1489	43	2014-03-04 15:22:39	28	296	Inscription à la demande "2013- Préparation aux concours internes session 1 - CNRS".
1490	43	2014-03-04 15:22:58	44	296	Votre inscription à la demande de formation "2013- Préparation aux concours internes session 1 - CNRS" a été acceptée.
1491	43	2014-03-04 15:22:58	76	296	Vous avez indiqué avoir assisté à la formation "2013- Préparation aux concours internes session 1 - CNRS".
1492	97	2014-03-04 15:28:31	28	297	Inscription à la demande "2013- Kit sensibilisation SSI - CNRS".
1493	97	2014-03-04 15:28:50	44	297	Votre inscription à la demande de formation "2013- Kit sensibilisation SSI - CNRS" a été acceptée.
1494	97	2014-03-04 15:28:50	76	297	Vous avez indiqué avoir assisté à la formation "2013- Kit sensibilisation SSI - CNRS".
1495	144	2014-03-04 15:29:18	28	297	Inscription à la demande "2013- Kit sensibilisation SSI - CNRS".
1496	144	2014-03-04 15:29:29	44	297	Votre inscription à la demande de formation "2013- Kit sensibilisation SSI - CNRS" a été acceptée.
1497	144	2014-03-04 15:29:29	76	297	Vous avez indiqué avoir assisté à la formation "2013- Kit sensibilisation SSI - CNRS".
1498	35	2014-03-04 15:37:16	28	298	Inscription à la demande "2013- Recyclage Sauveteur Secours du Travail - CNRS".
1499	35	2014-03-04 15:37:31	44	298	Votre inscription à la demande de formation "2013- Recyclage Sauveteur Secours du Travail - CNRS" a été acceptée.
1500	35	2014-03-04 15:37:31	76	298	Vous avez indiqué avoir assisté à la formation "2013- Recyclage Sauveteur Secours du Travail - CNRS".
1501	167	2014-03-04 15:43:54	28	299	Inscription à la demande "2013- Fonctionalisation surface par plasma - CNRS".
1502	167	2014-03-04 15:44:07	44	299	Votre inscription à la demande de formation "2013- Fonctionalisation surface par plasma - CNRS" a été acceptée.
1503	167	2014-03-04 15:44:07	76	299	Vous avez indiqué avoir assisté à la formation "2013- Fonctionalisation surface par plasma - CNRS".
1504	168	2014-03-04 15:46:01	28	299	Inscription à la demande "2013- Fonctionalisation surface par plasma - CNRS".
1505	168	2014-03-04 15:46:21	44	299	Votre inscription à la demande de formation "2013- Fonctionnalisation surface par plasma - CNRS" a été acceptée.
1506	168	2014-03-04 15:46:21	76	299	Vous avez indiqué avoir assisté à la formation "2013- Fonctionnalisation surface par plasma - CNRS".
1507	169	2014-03-04 15:50:18	28	300	Inscription à la demande "2013- Journée Réseau Plasmas Froids - CNRS".
1508	170	2014-03-04 15:51:57	28	300	Inscription à la demande "2013- Journée Réseau Plasmas Froids - CNRS".
1509	171	2014-03-04 15:53:54	28	300	Inscription à la demande "2013- Journée Réseau Plasmas Froids - CNRS".
1510	169	2014-03-04 15:54:06	44	300	Votre inscription à la demande de formation "2013- Journée Réseau Plasmas Froids - CNRS" a été acceptée.
1511	169	2014-03-04 15:54:06	76	300	Vous avez indiqué avoir assisté à la formation "2013- Journée Réseau Plasmas Froids - CNRS".
1512	170	2014-03-04 15:54:11	44	300	Votre inscription à la demande de formation "2013- Journée Réseau Plasmas Froids - CNRS" a été acceptée.
1513	170	2014-03-04 15:54:11	76	300	Vous avez indiqué avoir assisté à la formation "2013- Journée Réseau Plasmas Froids - CNRS".
1514	171	2014-03-04 15:54:16	44	300	Votre inscription à la demande de formation "2013- Journée Réseau Plasmas Froids - CNRS" a été acceptée.
1515	171	2014-03-04 15:54:16	76	300	Vous avez indiqué avoir assisté à la formation "2013- Journée Réseau Plasmas Froids - CNRS".
1516	123	2014-03-05 10:02:06	76	66	Vous avez indiqué avoir assisté à la formation "2011- Formation risques chimiques - INPT".
1517	137	2014-03-05 10:39:26	76	235	Vous avez indiqué avoir assisté à la formation "2011- Hygiène et sécurité - Nouveaux entrants - INPT".
1518	42	2014-03-05 10:42:35	76	86	Vous avez indiqué avoir assisté à la formation "2011- Les évolutions de la Fonction Publique".
1519	42	2014-03-05 10:43:05	76	125	Vous avez indiqué avoir assisté à la formation "2011- SIFAC Référentiel".
1520	42	2014-03-05 10:43:23	76	123	Vous avez indiqué avoir assisté à la formation "2011- SIFAC Dépenses - INPT".
1521	42	2014-03-05 10:43:35	76	124	Vous avez indiqué avoir assisté à la formation "2011- SIFAC Missions - INPT".
1522	38	2014-03-05 10:56:21	28	109	Inscription à la demande "2012- SIFAC Fiscalité".
1523	38	2014-03-05 10:56:34	44	109	Votre inscription à la demande de formation "2012- SIFAC Fiscalité" a été acceptée.
1524	38	2014-03-05 10:56:34	76	109	Vous avez indiqué avoir assisté à la formation "2012- SIFAC Fiscalité".
1525	38	2014-03-05 11:01:01	28	301	Inscription à la demande "2012- SIFAC Missions - INPT".
1526	38	2014-03-05 11:02:33	28	302	Inscription à la demande "2012- SIFAC Recettes - INPT".
1527	38	2014-03-05 11:03:36	44	302	Votre inscription à la demande de formation "2012- SIFAC Recettes - INPT" a été acceptée.
1528	38	2014-03-05 11:03:36	76	302	Vous avez indiqué avoir assisté à la formation "2012- SIFAC Recettes - INPT".
1529	38	2014-03-05 11:04:20	44	301	Votre inscription à la demande de formation "2012- SIFAC Missions - INPT" a été acceptée.
1530	38	2014-03-05 11:04:20	76	301	Vous avez indiqué avoir assisté à la formation "2012- SIFAC Missions - INPT".
1531	38	2014-03-05 11:06:03	28	303	Inscription à la demande "2012- SIFAC Prestations internes - INPT".
1532	38	2014-03-05 11:06:13	44	303	Votre inscription à la demande de formation "2012- SIFAC Prestations internes - INPT" a été acceptée.
1533	38	2014-03-05 11:06:13	76	303	Vous avez indiqué avoir assisté à la formation "2012- SIFAC Prestations internes - INPT".
1534	124	2014-03-05 11:08:14	76	66	Vous avez indiqué avoir assisté à la formation "2011- Formation risques chimiques - INPT".
1535	109	2014-03-05 11:13:38	76	55	Vous avez indiqué avoir assisté à la formation "2011- Formation Thermographie".
1536	82	2014-03-05 11:14:33	76	55	Vous avez indiqué avoir assisté à la formation "2011- Formation Thermographie".
1537	108	2014-03-05 11:37:05	76	55	Vous avez indiqué avoir assisté à la formation "2011- Formation Thermographie".
1538	129	2014-03-05 11:38:47	76	66	Vous avez indiqué avoir assisté à la formation "2011- Formation risques chimiques - INPT".
1539	39	2014-03-05 11:44:17	76	210	Vous avez indiqué avoir assisté à la formation "2012- SIFAC Fiscalité - INPT".
1540	39	2014-03-05 11:44:53	76	108	Vous avez indiqué avoir assisté à la formation "2011- Rédaction du rapport d'activités".
1541	39	2014-03-05 11:45:35	76	107	Vous avez indiqué avoir assisté à la formation "2011- Préparation aux concours session 2".
1542	39	2014-03-05 11:46:15	76	98	Vous avez indiqué avoir assisté à la formation "2011- SIFAC Dépenses".
1543	107	2014-03-05 11:49:42	76	55	Vous avez indiqué avoir assisté à la formation "2011- Formation Thermographie".
1544	106	2014-03-05 11:54:10	76	55	Vous avez indiqué avoir assisté à la formation "2011- Formation Thermographie".
1545	105	2014-03-05 11:55:57	76	55	Vous avez indiqué avoir assisté à la formation "2011- Formation Thermographie".
1546	35	2014-03-05 12:01:28	76	55	Vous avez indiqué avoir assisté à la formation "2011- Formation Thermographie".
1547	116	2014-03-17 10:34:54	28	305	Inscription à la demande "Sécurité incendie - Manipulation des moyens de secours".
1550	111	2014-03-17 16:02:54	28	305	Inscription à la demande "2014- Sécurité incendie - Manipulation des moyens de secours - INPT".
1551	111	2014-03-17 16:03:06	44	305	Votre inscription à la demande de formation "2014- Sécurité incendie - Manipulation des moyens de secours - INPT" a été acceptée.
1552	68	2014-03-17 16:28:35	28	305	Inscription à la demande "2014- Sécurité incendie - Manipulation des moyens de secours - INPT".
1554	172	2014-03-20 14:18:40	26	\N	Création de votre compte.
1555	71	2014-03-20 16:23:51	27	47	Ajout du besoin "Manipulation extincteur".
1556	156	2014-03-20 18:05:16	43	5	Le besoin "Habilitation Electrique pour non électricien - Formation initiale" a été pourvu.
1557	4	2014-03-21 14:34:07	28	305	Inscription à la demande "2014- Sécurité incendie - Manipulation des moyens de secours - INPT".
1558	4	2014-03-21 15:15:02	44	305	Votre inscription à la demande de formation "2014- Sécurité incendie - Manipulation des moyens de secours - INPT" a été acceptée.
1559	174	2014-03-25 15:23:25	26	\N	Création de votre compte.
1560	174	2014-03-25 16:14:05	28	307	Inscription à la demande "Initiation aux méthodes de spectroscopie laser pour l'analyse de milieux réactifs".
1561	174	2014-03-25 17:44:46	44	307	Votre inscription à la demande de formation "2014- Initiation aux méthodes de spectroscopie laser pour l'analyse de milieux réactifs" a été acceptée.
1562	175	2014-04-02 11:49:53	26	\N	Création de votre compte.
1565	54	2014-04-02 17:25:38	27	12	Ajout du besoin "Anglais".
1568	71	2014-04-04 11:32:22	44	271	Votre inscription à la demande de formation "2014- logiciel ESPRIT 4eme axe" a été acceptée.
1569	71	2014-04-04 11:32:22	76	271	Vous avez indiqué avoir assisté à la formation "2014- logiciel ESPRIT 4eme axe".
1570	49	2014-04-04 11:32:29	44	271	Votre inscription à la demande de formation "2014- logiciel ESPRIT 4eme axe" a été acceptée.
1571	49	2014-04-04 11:32:29	76	271	Vous avez indiqué avoir assisté à la formation "2014- logiciel ESPRIT 4eme axe".
1572	51	2014-04-04 11:32:33	44	271	Votre inscription à la demande de formation "2014- logiciel ESPRIT 4eme axe" a été acceptée.
1573	51	2014-04-04 11:32:33	76	271	Vous avez indiqué avoir assisté à la formation "2014- logiciel ESPRIT 4eme axe".
1574	71	2014-04-04 15:16:04	28	305	Inscription à la demande "2014- Sécurité incendie - Manipulation des moyens de secours - INPT".
1580	116	2014-04-04 18:26:19	44	305	Votre inscription à la demande de formation "2014- Sécurité incendie - Manipulation des moyens de secours - INPT" a été acceptée.
1581	68	2014-04-04 18:26:28	44	305	Votre inscription à la demande de formation "2014- Sécurité incendie - Manipulation des moyens de secours - INPT" a été acceptée.
1588	35	2014-04-08 18:19:53	76	229	Vous avez indiqué avoir assisté à la formation "2011- Recyclage Sauveteur Secours du Travail - CNRS".
1590	73	2014-04-09 10:12:09	44	16	Votre inscription à la demande de formation "2009- Mieux connaitre le fonctionnement de l'Union Européenne" a été acceptée.
1591	73	2014-04-09 10:12:09	76	16	Vous avez indiqué avoir assisté à la formation "2009- Mieux connaitre le fonctionnement de l'Union Européenne".
1592	56	2014-04-09 10:12:39	44	170	Votre inscription à la demande de formation "2009- Connaissances de l'UPS" a été acceptée.
1593	56	2014-04-09 10:12:39	76	170	Vous avez indiqué avoir assisté à la formation "2009- Connaissances de l'UPS".
1594	76	2014-04-09 10:13:21	44	195	Votre inscription à la demande de formation "2009- Développer la motivation de l'étudiant" a été acceptée.
1595	76	2014-04-09 10:13:21	76	195	Vous avez indiqué avoir assisté à la formation "2009- Développer la motivation de l'étudiant".
1596	56	2014-04-09 10:13:41	44	22	Votre inscription à la demande de formation "2009- SIFAC Missions" a été acceptée.
1597	56	2014-04-09 10:13:41	76	22	Vous avez indiqué avoir assisté à la formation "2009- SIFAC Missions".
1598	56	2014-04-09 10:13:52	44	176	Votre inscription à la demande de formation "2009- Achat public : Formation à l'outil "Achatpublic.com", session 2" a été acceptée.
1599	56	2014-04-09 10:13:52	76	176	Vous avez indiqué avoir assisté à la formation "2009- Achat public : Formation à l'outil "Achatpublic.com", session 2".
1600	56	2014-04-09 10:14:10	44	175	Votre inscription à la demande de formation "2009- Achat public : Le DCE/les éléments et pièces d'un marché" a été acceptée.
1601	56	2014-04-09 10:14:10	76	175	Vous avez indiqué avoir assisté à la formation "2009- Achat public : Le DCE/les éléments et pièces d'un marché".
1602	56	2014-04-09 10:14:24	44	171	Votre inscription à la demande de formation "2009- Mettre en œuvre les principes généraux de l'achat public à l'UPS" a été acceptée.
1603	56	2014-04-09 10:14:24	76	171	Vous avez indiqué avoir assisté à la formation "2009- Mettre en œuvre les principes généraux de l'achat public à l'UPS".
1604	56	2014-04-09 10:14:35	44	23	Votre inscription à la demande de formation "2009- SIFAC Dépenses (1)" a été acceptée.
1605	56	2014-04-09 10:14:35	76	23	Vous avez indiqué avoir assisté à la formation "2009- SIFAC Dépenses (1)".
1606	56	2014-04-09 10:14:45	44	174	Votre inscription à la demande de formation "2009- SIFAC Edition - Consultation" a été acceptée.
1607	56	2014-04-09 10:14:45	76	174	Vous avez indiqué avoir assisté à la formation "2009- SIFAC Edition - Consultation".
1608	58	2014-04-09 10:14:49	44	174	Votre inscription à la demande de formation "2009- SIFAC Edition - Consultation" a été acceptée.
1609	58	2014-04-09 10:14:49	76	174	Vous avez indiqué avoir assisté à la formation "2009- SIFAC Edition - Consultation".
1610	56	2014-04-09 10:14:58	44	173	Votre inscription à la demande de formation "2009- Achat public : Formation à l'outil "Achatpublic.com"" a été acceptée.
1611	56	2014-04-09 10:14:58	76	173	Vous avez indiqué avoir assisté à la formation "2009- Achat public : Formation à l'outil "Achatpublic.com"".
1612	56	2014-04-09 10:15:08	44	24	Votre inscription à la demande de formation "2009- SIFAC Référentiel" a été acceptée.
1613	56	2014-04-09 10:15:08	76	24	Vous avez indiqué avoir assisté à la formation "2009- SIFAC Référentiel".
1614	35	2014-04-09 10:15:42	44	137	Votre inscription à la demande de formation "2009- Recyclage Sauveteur Secours du Travail - CNRS" a été acceptée.
1615	35	2014-04-09 10:15:42	76	137	Vous avez indiqué avoir assisté à la formation "2009- Recyclage Sauveteur Secours du Travail - CNRS".
1616	40	2014-04-09 10:15:58	44	9	Votre inscription à la demande de formation "2009- Recyclage Sauveteur Secours du Travail - INPT" a été acceptée.
1617	40	2014-04-09 10:15:58	76	9	Vous avez indiqué avoir assisté à la formation "2009- Recyclage Sauveteur Secours du Travail - INPT".
1618	67	2014-04-09 10:16:02	44	9	Votre inscription à la demande de formation "2009- Recyclage Sauveteur Secours du Travail - INPT" a été acceptée.
1619	67	2014-04-09 10:16:02	76	9	Vous avez indiqué avoir assisté à la formation "2009- Recyclage Sauveteur Secours du Travail - INPT".
1620	65	2014-04-09 10:16:07	44	9	Votre inscription à la demande de formation "2009- Recyclage Sauveteur Secours du Travail - INPT" a été acceptée.
1621	65	2014-04-09 10:16:07	76	9	Vous avez indiqué avoir assisté à la formation "2009- Recyclage Sauveteur Secours du Travail - INPT".
1622	66	2014-04-09 10:16:16	44	9	Votre inscription à la demande de formation "2009- Recyclage Sauveteur Secours du Travail - INPT" a été acceptée.
1623	66	2014-04-09 10:16:16	76	9	Vous avez indiqué avoir assisté à la formation "2009- Recyclage Sauveteur Secours du Travail - INPT".
1624	78	2014-04-09 10:16:35	44	197	Votre inscription à la demande de formation "2009- Compétences en radioprotection - CNRS" a été acceptée.
1625	78	2014-04-09 10:16:35	76	197	Vous avez indiqué avoir assisté à la formation "2009- Compétences en radioprotection - CNRS".
1626	74	2014-04-09 10:16:44	44	194	Votre inscription à la demande de formation "2009- Manipulation des extincteurs - UPS" a été acceptée.
1627	74	2014-04-09 10:16:44	76	194	Vous avez indiqué avoir assisté à la formation "2009- Manipulation des extincteurs - UPS".
1628	75	2014-04-09 10:16:48	44	194	Votre inscription à la demande de formation "2009- Manipulation des extincteurs - UPS" a été acceptée.
1629	75	2014-04-09 10:16:48	76	194	Vous avez indiqué avoir assisté à la formation "2009- Manipulation des extincteurs - UPS".
1630	58	2014-04-09 10:17:01	44	164	Votre inscription à la demande de formation "2009- GRAAL, session 1" a été acceptée.
1631	58	2014-04-09 10:17:01	76	164	Vous avez indiqué avoir assisté à la formation "2009- GRAAL, session 1".
1632	4	2014-04-09 10:17:12	44	148	Votre inscription à la demande de formation "2009- Journée PLUME" a été acceptée.
1633	4	2014-04-09 10:17:12	76	148	Vous avez indiqué avoir assisté à la formation "2009- Journée PLUME".
1634	61	2014-04-09 10:19:55	44	187	Votre inscription à la demande de formation "2009- Méthode de conception d'un QCM" a été acceptée.
1635	61	2014-04-09 10:19:55	76	187	Vous avez indiqué avoir assisté à la formation "2009- Méthode de conception d'un QCM".
1636	63	2014-04-09 10:20:27	44	188	Votre inscription à la demande de formation "2009- Découverte de Moodle" a été acceptée.
1637	63	2014-04-09 10:20:27	76	188	Vous avez indiqué avoir assisté à la formation "2009- Découverte de Moodle".
1638	61	2014-04-09 10:20:33	44	188	Votre inscription à la demande de formation "2009- Découverte de Moodle" a été acceptée.
1639	61	2014-04-09 10:20:33	76	188	Vous avez indiqué avoir assisté à la formation "2009- Découverte de Moodle".
1640	41	2014-04-09 10:20:45	44	119	Votre inscription à la demande de formation "2009- HTML XHTML CSS, session 2" a été acceptée.
1641	41	2014-04-09 10:20:45	76	119	Vous avez indiqué avoir assisté à la formation "2009- HTML XHTML CSS, session 2".
1642	59	2014-04-09 10:21:18	44	186	Votre inscription à la demande de formation "2009- Conception des convertisseurs d'énergie" a été acceptée.
1643	59	2014-04-09 10:21:18	76	186	Vous avez indiqué avoir assisté à la formation "2009- Conception des convertisseurs d'énergie".
1644	73	2014-04-09 10:21:35	44	193	Votre inscription à la demande de formation "2009- Matériaux pour l’énergie, Galerne" a été acceptée.
1645	73	2014-04-09 10:21:35	76	193	Vous avez indiqué avoir assisté à la formation "2009- Matériaux pour l’énergie, Galerne".
1646	77	2014-04-09 10:21:43	44	196	Votre inscription à la demande de formation "2009- 11èmes Rencontres nationales du réseau des électroniciens CNRS" a été acceptée.
1647	77	2014-04-09 10:21:43	76	196	Vous avez indiqué avoir assisté à la formation "2009- 11èmes Rencontres nationales du réseau des électroniciens CNRS".
1648	35	2014-04-09 10:21:53	44	236	Votre inscription à la demande de formation "2009- Journées de l'optique" a été acceptée.
1649	35	2014-04-09 10:21:53	76	236	Vous avez indiqué avoir assisté à la formation "2009- Journées de l'optique".
1650	100	2014-04-09 10:55:59	44	213	Votre inscription à la demande de formation "2010- Anglais classique" a été acceptée.
1651	100	2014-04-09 10:55:59	76	213	Vous avez indiqué avoir assisté à la formation "2010- Anglais classique".
1652	76	2014-04-09 10:56:52	44	206	Votre inscription à la demande de formation "2010- Plongée sous-marine niveau 2" a été acceptée.
1653	76	2014-04-09 10:56:52	76	206	Vous avez indiqué avoir assisté à la formation "2010- Plongée sous-marine niveau 2".
1654	101	2014-04-09 10:58:17	44	214	Votre inscription à la demande de formation "2010- Bien-être, Mal-être : Le point sur les risques psychosociaux" a été acceptée.
1655	101	2014-04-09 10:58:17	76	214	Vous avez indiqué avoir assisté à la formation "2010- Bien-être, Mal-être : Le point sur les risques psychosociaux".
1656	102	2014-04-09 10:58:23	44	215	Votre inscription à la demande de formation "2010- Conduite de réunion" a été acceptée.
1657	102	2014-04-09 10:58:23	76	215	Vous avez indiqué avoir assisté à la formation "2010- Conduite de réunion".
1658	56	2014-04-09 10:58:32	44	178	Votre inscription à la demande de formation "2010- Achat public : Formation à l'outil "Achatpublic.com"" a été acceptée.
1659	56	2014-04-09 10:58:32	76	178	Vous avez indiqué avoir assisté à la formation "2010- Achat public : Formation à l'outil "Achatpublic.com"".
1881	49	2014-09-12 09:03:25	43	52	Le besoin "Formation CFAO ESPRIT Module 4/5 Axes" a été pourvu.
1660	56	2014-04-09 10:58:51	44	177	Votre inscription à la demande de formation "2010- SIFAC Convention" a été acceptée.
1661	56	2014-04-09 10:58:51	76	177	Vous avez indiqué avoir assisté à la formation "2010- SIFAC Convention".
1662	56	2014-04-09 10:59:06	44	26	Votre inscription à la demande de formation "2010- SIFAC Budget" a été acceptée.
1663	56	2014-04-09 10:59:06	76	26	Vous avez indiqué avoir assisté à la formation "2010- SIFAC Budget".
1664	56	2014-04-09 10:59:22	44	25	Votre inscription à la demande de formation "2010- SIFAC Missions Métiers" a été acceptée.
1665	56	2014-04-09 10:59:22	76	25	Vous avez indiqué avoir assisté à la formation "2010- SIFAC Missions Métiers".
1666	67	2014-04-09 10:59:47	44	56	Votre inscription à la demande de formation "2010- Recyclage Sauveteur Secours du Travail - INPT" a été acceptée.
1667	67	2014-04-09 10:59:47	76	56	Vous avez indiqué avoir assisté à la formation "2010- Recyclage Sauveteur Secours du Travail - INPT".
1668	65	2014-04-09 10:59:52	44	56	Votre inscription à la demande de formation "2010- Recyclage Sauveteur Secours du Travail - INPT" a été acceptée.
1669	65	2014-04-09 10:59:52	76	56	Vous avez indiqué avoir assisté à la formation "2010- Recyclage Sauveteur Secours du Travail - INPT".
1670	98	2014-04-09 11:00:00	44	56	Votre inscription à la demande de formation "2010- Recyclage Sauveteur Secours du Travail - INPT" a été acceptée.
1671	98	2014-04-09 11:00:00	76	56	Vous avez indiqué avoir assisté à la formation "2010- Recyclage Sauveteur Secours du Travail - INPT".
1672	66	2014-04-09 11:00:04	44	56	Votre inscription à la demande de formation "2010- Recyclage Sauveteur Secours du Travail - INPT" a été acceptée.
1673	66	2014-04-09 11:00:04	76	56	Vous avez indiqué avoir assisté à la formation "2010- Recyclage Sauveteur Secours du Travail - INPT".
1674	74	2014-04-09 11:00:12	44	211	Votre inscription à la demande de formation "2010- SST Formation initiale - UPS" a été acceptée.
1675	74	2014-04-09 11:00:12	76	211	Vous avez indiqué avoir assisté à la formation "2010- SST Formation initiale - UPS".
1676	35	2014-04-09 11:00:20	44	72	Votre inscription à la demande de formation "2010- Recyclage Sauveteur Secours du Travail - CNRS" a été acceptée.
1677	35	2014-04-09 11:00:20	76	72	Vous avez indiqué avoir assisté à la formation "2010- Recyclage Sauveteur Secours du Travail - CNRS".
1678	78	2014-04-09 11:01:04	44	157	Votre inscription à la demande de formation "2010- Recyclage Sauveteur Secours du Travail - UPS" a été acceptée.
1679	78	2014-04-09 11:01:04	76	157	Vous avez indiqué avoir assisté à la formation "2010- Recyclage Sauveteur Secours du Travail - UPS".
1680	82	2014-04-09 11:01:08	44	157	Votre inscription à la demande de formation "2010- Recyclage Sauveteur Secours du Travail - UPS" a été acceptée.
1681	82	2014-04-09 11:01:08	76	157	Vous avez indiqué avoir assisté à la formation "2010- Recyclage Sauveteur Secours du Travail - UPS".
1682	99	2014-04-09 11:01:12	44	157	Votre inscription à la demande de formation "2010- Recyclage Sauveteur Secours du Travail - UPS" a été acceptée.
1683	99	2014-04-09 11:01:12	76	157	Vous avez indiqué avoir assisté à la formation "2010- Recyclage Sauveteur Secours du Travail - UPS".
1684	89	2014-04-09 11:01:21	44	212	Votre inscription à la demande de formation "2010- Ethique et Déontologie" a été acceptée.
1685	89	2014-04-09 11:01:21	76	212	Vous avez indiqué avoir assisté à la formation "2010- Ethique et Déontologie".
1686	35	2014-04-09 11:01:49	44	71	Votre inscription à la demande de formation "2010- Inventor, formation interne LAPLACE" a été acceptée.
1687	35	2014-04-09 11:01:49	76	71	Vous avez indiqué avoir assisté à la formation "2010- Inventor, formation interne LAPLACE".
1688	35	2014-04-09 11:02:34	44	70	Votre inscription à la demande de formation "2010- Kit SPIP contributeur" a été acceptée.
1689	35	2014-04-09 11:02:34	76	70	Vous avez indiqué avoir assisté à la formation "2010- Kit SPIP contributeur".
1690	4	2014-04-09 11:02:42	44	149	Votre inscription à la demande de formation "2010- Compilation bases de données pour la science" a été acceptée.
1691	4	2014-04-09 11:02:42	76	149	Vous avez indiqué avoir assisté à la formation "2010- Compilation bases de données pour la science".
1692	43	2014-04-09 11:02:48	44	128	Votre inscription à la demande de formation "2010- Initiation C embarqué microcontrôleurs" a été acceptée.
1693	43	2014-04-09 11:02:48	76	128	Vous avez indiqué avoir assisté à la formation "2010- Initiation C embarqué microcontrôleurs".
1694	95	2014-04-09 11:02:56	44	132	Votre inscription à la demande de formation "2010- Outils Cadence : ALLEGRO PCB DESIGNER" a été acceptée.
1695	95	2014-04-09 11:02:56	76	132	Vous avez indiqué avoir assisté à la formation "2010- Outils Cadence : ALLEGRO PCB DESIGNER".
1696	37	2014-04-09 11:03:06	44	129	Votre inscription à la demande de formation "2010- Modelsim 2010" a été acceptée.
1698	4	2014-04-09 11:03:26	44	150	Votre inscription à la demande de formation "2010- Langage de scripts" a été acceptée.
1699	4	2014-04-09 11:03:26	76	150	Vous avez indiqué avoir assisté à la formation "2010- Langage de scripts".
1700	94	2014-04-09 11:03:36	44	207	Votre inscription à la demande de formation "2010- Techniciens webconférences" a été acceptée.
1701	94	2014-04-09 11:03:36	76	207	Vous avez indiqué avoir assisté à la formation "2010- Techniciens webconférences".
1702	76	2014-04-09 11:03:46	44	205	Votre inscription à la demande de formation "2010- Produire et exploiter des QCM en ligne" a été acceptée.
1703	76	2014-04-09 11:03:46	76	205	Vous avez indiqué avoir assisté à la formation "2010- Produire et exploiter des QCM en ligne".
1704	94	2014-04-09 11:03:54	44	204	Votre inscription à la demande de formation "2010- Méthode de conception d'un QCM" a été acceptée.
1705	94	2014-04-09 11:03:54	76	204	Vous avez indiqué avoir assisté à la formation "2010- Méthode de conception d'un QCM".
1706	80	2014-04-09 11:04:09	44	199	Votre inscription à la demande de formation "2010- Microscopie champ proche" a été acceptée.
1707	80	2014-04-09 11:04:09	76	199	Vous avez indiqué avoir assisté à la formation "2010- Microscopie champ proche".
1708	82	2014-04-09 11:04:23	44	200	Votre inscription à la demande de formation "2010- Spectrométrie de masse" a été acceptée.
1709	82	2014-04-09 11:04:23	76	200	Vous avez indiqué avoir assisté à la formation "2010- Spectrométrie de masse".
1710	86	2014-04-09 11:04:48	44	64	Votre inscription à la demande de formation "2010- Journée Réseau Plasmas Froids" a été acceptée.
1711	86	2014-04-09 11:04:48	76	64	Vous avez indiqué avoir assisté à la formation "2010- Journée Réseau Plasmas Froids".
1712	84	2014-04-09 11:04:52	44	64	Votre inscription à la demande de formation "2010- Journée Réseau Plasmas Froids" a été acceptée.
1713	84	2014-04-09 11:04:52	76	64	Vous avez indiqué avoir assisté à la formation "2010- Journée Réseau Plasmas Froids".
1714	89	2014-04-09 11:04:59	44	64	Votre inscription à la demande de formation "2010- Journée Réseau Plasmas Froids" a été acceptée.
1715	89	2014-04-09 11:04:59	76	64	Vous avez indiqué avoir assisté à la formation "2010- Journée Réseau Plasmas Froids".
1716	87	2014-04-09 11:05:04	44	64	Votre inscription à la demande de formation "2010- Journée Réseau Plasmas Froids" a été acceptée.
1717	87	2014-04-09 11:05:04	76	64	Vous avez indiqué avoir assisté à la formation "2010- Journée Réseau Plasmas Froids".
1718	88	2014-04-09 11:05:08	44	64	Votre inscription à la demande de formation "2010- Journée Réseau Plasmas Froids" a été acceptée.
1719	88	2014-04-09 11:05:08	76	64	Vous avez indiqué avoir assisté à la formation "2010- Journée Réseau Plasmas Froids".
1720	85	2014-04-09 11:05:14	44	64	Votre inscription à la demande de formation "2010- Journée Réseau Plasmas Froids" a été acceptée.
1721	85	2014-04-09 11:05:14	76	64	Vous avez indiqué avoir assisté à la formation "2010- Journée Réseau Plasmas Froids".
1722	77	2014-04-09 11:05:28	44	202	Votre inscription à la demande de formation "2010- 12èmes Rencontres nationales du réseau des électroniciens CNRS" a été acceptée.
1723	77	2014-04-09 11:05:28	76	202	Vous avez indiqué avoir assisté à la formation "2010- 12èmes Rencontres nationales du réseau des électroniciens CNRS".
1724	37	2014-04-09 11:05:41	44	127	Votre inscription à la demande de formation "2010- 14èmes Rencontre régionale du réseau des électroniciens DR14" a été acceptée.
1725	37	2014-04-09 11:05:41	76	127	Vous avez indiqué avoir assisté à la formation "2010- 14èmes Rencontre régionale du réseau des électroniciens DR14".
1726	43	2014-04-09 11:05:45	44	127	Votre inscription à la demande de formation "2010- 14èmes Rencontre régionale du réseau des électroniciens DR14" a été acceptée.
1727	43	2014-04-09 11:05:45	76	127	Vous avez indiqué avoir assisté à la formation "2010- 14èmes Rencontre régionale du réseau des électroniciens DR14".
1728	63	2014-04-09 11:05:58	44	201	Votre inscription à la demande de formation "2010- Atelier chimie poudre immergé dans plasma" a été acceptée.
1729	63	2014-04-09 11:05:58	76	201	Vous avez indiqué avoir assisté à la formation "2010- Atelier chimie poudre immergé dans plasma".
1730	84	2014-04-09 11:06:02	44	201	Votre inscription à la demande de formation "2010- Atelier chimie poudre immergé dans plasma" a été acceptée.
1731	84	2014-04-09 11:06:02	76	201	Vous avez indiqué avoir assisté à la formation "2010- Atelier chimie poudre immergé dans plasma".
1732	83	2014-04-09 11:06:07	44	201	Votre inscription à la demande de formation "2010- Atelier chimie poudre immergé dans plasma" a été acceptée.
1733	83	2014-04-09 11:06:07	76	201	Vous avez indiqué avoir assisté à la formation "2010- Atelier chimie poudre immergé dans plasma".
1734	63	2014-04-09 11:06:14	44	203	Votre inscription à la demande de formation "2010- Atelier ECOMOD" a été acceptée.
1735	63	2014-04-09 11:06:14	76	203	Vous avez indiqué avoir assisté à la formation "2010- Atelier ECOMOD".
1736	90	2014-04-09 11:06:20	44	203	Votre inscription à la demande de formation "2010- Atelier ECOMOD" a été acceptée.
1737	90	2014-04-09 11:06:20	76	203	Vous avez indiqué avoir assisté à la formation "2010- Atelier ECOMOD".
1738	166	2014-04-09 15:00:09	28	304	Inscription à la demande "2014- Maxwell et Couplage Workbench".
1739	166	2014-04-09 15:00:18	44	304	Votre inscription à la demande de formation "2014- Maxwell et Couplage Workbench" a été acceptée.
1740	166	2014-04-09 15:00:18	76	304	Vous avez indiqué avoir assisté à la formation "2014- Maxwell et Couplage Workbench".
1741	50	2014-04-09 15:01:16	28	304	Inscription à la demande "2014- Maxwell et Couplage Workbench".
1742	50	2014-04-09 15:01:26	44	304	Votre inscription à la demande de formation "2014- Maxwell et Couplage Workbench" a été acceptée.
1743	50	2014-04-09 15:01:26	76	304	Vous avez indiqué avoir assisté à la formation "2014- Maxwell et Couplage Workbench".
1744	177	2014-04-09 15:05:11	28	304	Inscription à la demande "2014- Maxwell et Couplage Workbench".
1745	177	2014-04-09 15:05:26	44	304	Votre inscription à la demande de formation "2014- Maxwell et Couplage Workbench" a été acceptée.
1746	177	2014-04-09 15:05:26	76	304	Vous avez indiqué avoir assisté à la formation "2014- Maxwell et Couplage Workbench".
1747	178	2014-04-09 15:07:09	28	304	Inscription à la demande "2014- Maxwell et Couplage Workbench".
1748	178	2014-04-09 15:07:23	44	304	Votre inscription à la demande de formation "2014- Maxwell et Couplage Workbench" a été acceptée.
1749	178	2014-04-09 15:07:23	76	304	Vous avez indiqué avoir assisté à la formation "2014- Maxwell et Couplage Workbench".
1750	179	2014-04-09 15:09:02	28	304	Inscription à la demande "2014- Maxwell et Couplage Workbench".
1751	179	2014-04-09 15:09:17	44	304	Votre inscription à la demande de formation "2014- Maxwell et Couplage Workbench" a été acceptée.
1752	179	2014-04-09 15:09:17	76	304	Vous avez indiqué avoir assisté à la formation "2014- Maxwell et Couplage Workbench".
1753	180	2014-04-09 15:15:12	28	304	Inscription à la demande "2014- Maxwell et Couplage Workbench".
1754	180	2014-04-09 15:15:32	44	304	Votre inscription à la demande de formation "2014- Maxwell et Couplage Workbench" a été acceptée.
1755	180	2014-04-09 15:15:32	76	304	Vous avez indiqué avoir assisté à la formation "2014- Maxwell et Couplage Workbench".
1756	181	2014-04-09 15:19:48	28	304	Inscription à la demande "2014- Maxwell et Couplage Workbench".
1757	181	2014-04-09 15:23:56	44	304	Votre inscription à la demande de formation "2014- Maxwell et Couplage Workbench" a été acceptée.
1758	181	2014-04-09 15:23:56	76	304	Vous avez indiqué avoir assisté à la formation "2014- Maxwell et Couplage Workbench".
1759	182	2014-04-09 15:26:36	28	304	Inscription à la demande "2014- Maxwell et Couplage Workbench".
1760	182	2014-04-09 15:26:48	44	304	Votre inscription à la demande de formation "2014- Maxwell et Couplage Workbench" a été acceptée.
1761	182	2014-04-09 15:26:48	76	304	Vous avez indiqué avoir assisté à la formation "2014- Maxwell et Couplage Workbench".
1762	48	2014-04-09 16:24:58	27	56	Ajout du besoin "JAPONAIS débutant".
1763	166	2014-04-09 16:30:34	43	53	Le besoin "ANSYS Maxwell" a été pourvu.
1764	50	2014-04-09 16:30:57	43	53	Le besoin "ANSYS Maxwell" a été pourvu.
1765	50	2014-04-09 16:31:09	43	27	Le besoin "Labview" a été pourvu.
1766	56	2014-04-09 17:21:49	44	172	Votre inscription à la demande de formation "2009- Achat public : la procédure adaptée ou simplifiée" a été acceptée.
1882	51	2014-09-12 09:03:37	43	52	Le besoin "Formation CFAO ESPRIT Module 4/5 Axes" a été pourvu.
1767	56	2014-04-09 17:21:49	76	172	Vous avez indiqué avoir assisté à la formation "2009- Achat public : la procédure adaptée ou simplifiée".
1768	4	2014-04-17 11:32:06	28	314	Inscription à la demande "2014- Configuration avancé, tuning pour MySQL - CNRS".
1769	4	2014-04-17 11:32:34	44	314	Votre inscription à la demande de formation "2014- Configuration avancé, tuning pour MySQL - CNRS" a été acceptée.
1770	4	2014-04-17 11:32:48	76	305	Vous avez indiqué avoir assisté à la formation "2014- Sécurité incendie - Manipulation des moyens de secours - INPT".
1771	4	2014-04-17 11:40:44	28	315	Inscription à la demande "2014- Sauveteur secouriste du travail – Formation initiale - INPT".
1772	4	2014-04-17 11:45:35	44	315	Votre inscription à la demande de formation "2014- Sauveteur secouriste du travail – Formation initiale - INPT" a été acceptée.
1773	42	2014-04-18 08:48:46	28	316	Inscription à la demande "Excel Concolidations et TCD".
1774	42	2014-04-18 08:49:33	28	317	Inscription à la demande "POWERPOINT INITIATION".
1775	42	2014-04-18 08:50:05	28	318	Inscription à la demande "POWERPOINT PERFECTIONNEMENT".
1776	42	2014-04-18 08:50:33	28	319	Inscription à la demande "WORD RAPPORTS ET DOCUMENTS".
1777	42	2014-04-18 08:51:09	28	320	Inscription à la demande "EXCEL INITIATION AUX MACRO COMMANDES".
1783	134	2014-04-24 14:15:57	44	226	Votre inscription à la demande de formation "2011- Word Perfectionnement" a été acceptée.
1784	134	2014-04-24 14:15:57	76	226	Vous avez indiqué avoir assisté à la formation "2011- Word Perfectionnement".
1785	2	2014-04-24 14:50:45	28	323	Inscription à la demande "2013- Réunion des correspondants formation du CNRS DR14".
1786	2	2014-04-24 14:50:58	44	323	Votre inscription à la demande de formation "2013- Réunion des correspondants formation du CNRS DR14" a été acceptée.
1787	2	2014-04-24 14:50:58	76	323	Vous avez indiqué avoir assisté à la formation "2013- Réunion des correspondants formation du CNRS DR14".
1788	57	2014-05-15 11:27:32	28	333	Inscription à la demande "Gestion du temps et des priorités".
1789	57	2014-05-15 11:28:32	44	333	Votre inscription à la demande de formation "2014- Gestion du temps et des priorités - UPS" a été acceptée.
1790	57	2014-05-15 11:29:29	76	333	Vous avez indiqué avoir assisté à la formation "2014- Gestion du temps et des priorités - UPS".
1791	57	2014-05-15 11:39:34	28	334	Inscription à la demande "DEMATERIALISATION : FORMATION GESTIONNAIRES".
1792	3	2014-05-15 11:39:45	28	335	Inscription à la demande "MPI/OpenMP".
1793	3	2014-05-15 11:40:15	44	335	Votre inscription à la demande de formation "MPI/OpenMP" a été acceptée.
1794	57	2014-05-15 11:41:06	44	334	Votre inscription à la demande de formation "2014- Dématérialisation : Formation gestionnaires - UPS" a été acceptée.
1795	54	2014-05-15 13:58:47	28	336	Inscription à la demande "sécurité incendie et manie des extincteurs".
1796	54	2014-05-15 14:43:38	44	336	Votre inscription à la demande de formation "2014- Sécurité incendie et manipulation des extincteurs - UPS" a été acceptée.
1797	68	2014-05-16 14:38:22	76	305	Vous avez indiqué avoir assisté à la formation "2014- Sécurité incendie - Manipulation des moyens de secours - INPT".
1798	112	2014-05-28 09:23:06	28	326	Inscription à la demande "2014- Gestion des conflits - PFRH".
1799	112	2014-05-28 09:23:36	44	326	Votre inscription à la demande de formation "2014- Gestion des conflits - PFRH" a été acceptée.
1800	106	2014-05-28 10:34:29	28	339	Inscription à la demande "formation initiale "assistant de prévention" (ACMO)".
1801	106	2014-05-28 10:49:33	28	340	Inscription à la demande "Formation Tutorée à l'Habilitation Electrique".
1802	106	2014-05-28 11:01:42	44	339	Votre inscription à la demande de formation "2014- formation initiale "assistant de prévention" (ACMO)" a été acceptée.
1803	106	2014-05-28 11:02:17	44	340	Votre inscription à la demande de formation "2014- Formation Tutorée à l'Habilitation Electrique" a été acceptée.
1804	106	2014-05-28 11:07:14	76	339	Vous avez indiqué avoir assisté à la formation "2014- formation initiale "assistant de prévention" (ACMO)".
1805	106	2014-05-28 11:08:05	76	340	Vous avez indiqué avoir assisté à la formation "2014- Formation Tutorée à l'Habilitation Electrique".
1806	4	2014-06-11 13:17:51	76	314	Vous avez indiqué avoir assisté à la formation "2014- Configuration avancé, tuning pour MySQL - CNRS".
1807	4	2014-06-11 13:18:09	92	315	Vous avez indiqué n'avoir pas assisté à la formation "2014- Sauveteur secouriste du travail – Formation initiale - INPT".
1808	4	2014-06-16 10:09:09	28	331	Inscription à la demande "2014- Formation Management de projet - CNRS".
1809	4	2014-06-16 10:14:47	44	331	Votre inscription à la demande de formation "2014- Formation Management de projet - CNRS" a été acceptée.
1810	158	2014-07-01 08:42:00	28	338	Inscription à la demande "2014- Journée "Initiation au montage de projets de recherche" - INPT".
1811	158	2014-07-01 10:25:57	44	338	Votre inscription à la demande de formation "2014- Journée "Initiation au montage de projets de recherche" - INPT" a été acceptée.
1812	183	2014-07-23 16:01:23	26	\N	Création de votre compte.
1813	183	2014-07-23 16:06:51	28	346	Inscription à la demande "2014- Habilitation électrique des personnels non électricien -INPT".
1814	183	2014-07-23 16:08:13	44	346	Votre inscription à la demande de formation "2014- Habilitation électrique des personnels non électricien -INPT" a été acceptée.
1815	183	2014-07-23 16:09:40	76	346	Vous avez indiqué avoir assisté à la formation "2014- Habilitation électrique des personnels non électricien -INPT".
1816	183	2014-07-23 16:26:50	28	347	Inscription à la demande "InInternational summer school de l'université de Toulouse. session d'été 2014".
1817	183	2014-07-23 16:39:00	44	347	Votre inscription à la demande de formation "2014- InInternational summer school de l'université de Toulouse. session d'été" a été acceptée.
1818	4	2014-08-28 12:22:49	28	322	Inscription à la demande "2014- Formations informatiques - CNRS".
1819	4	2014-08-28 12:25:32	28	349	Inscription à la demande "Formations informatiques - CNRS - perl".
1820	4	2014-08-28 12:28:50	44	322	Votre inscription à la demande de formation "2014- Formations informatiques - CNRS" a été acceptée.
1821	4	2014-08-28 12:29:21	44	349	Votre inscription à la demande de formation "Formations informatiques - CNRS - perl" a été acceptée.
1822	4	2014-08-29 16:17:44	92	322	Vous avez indiqué n'avoir pas assisté à la formation "2014- Formations informatiques - CNRS".
1823	4	2014-08-29 16:18:16	28	350	Inscription à la demande "2014- SPIP – Développement et adaptation du nouveau kit CNRS - CNRS".
1824	4	2014-08-29 16:18:27	44	350	Votre inscription à la demande de formation "2014- SPIP – Développement et adaptation du nouveau kit CNRS - CNRS" a été acceptée.
1825	122	2014-09-02 14:35:49	76	223	Vous avez indiqué avoir assisté à la formation "2011- Habilitation électrique du personnel non électricien - INPT".
1826	130	2014-09-02 14:49:21	76	66	Vous avez indiqué avoir assisté à la formation "2011- Formation risques chimiques - INPT".
1827	3	2014-09-02 16:22:53	76	335	Vous avez indiqué avoir assisté à la formation "2014- Calculs parallèles MPI/OpenMP - CNRS".
1828	157	2014-09-02 16:58:31	28	351	Inscription à la demande "2014- Microscopie à Force Atomique".
1829	157	2014-09-02 16:58:47	44	351	Votre inscription à la demande de formation "2014- Microscopie à Force Atomique" a été acceptée.
1830	157	2014-09-02 16:58:47	76	351	Vous avez indiqué avoir assisté à la formation "2014- Microscopie à Force Atomique".
1831	158	2014-09-02 17:16:10	76	338	Vous avez indiqué avoir assisté à la formation "2014- Journée "Initiation au montage de projets de recherche" - INPT".
1832	183	2014-09-02 17:17:58	76	347	Vous avez indiqué avoir assisté à la formation "2014- International summer school de l'université de Toulouse, session d'été".
1833	103	2014-09-02 19:00:22	76	66	Vous avez indiqué avoir assisté à la formation "2011- Formation risques chimiques - INPT".
1834	103	2014-09-02 19:01:18	76	55	Vous avez indiqué avoir assisté à la formation "2011- Formation Thermographie".
1835	116	2014-09-03 07:37:40	76	305	Vous avez indiqué avoir assisté à la formation "2014- Sécurité incendie - Manipulation des moyens de secours - INPT".
1836	111	2014-09-03 09:42:58	76	305	Vous avez indiqué avoir assisté à la formation "2014- Sécurité incendie - Manipulation des moyens de secours - INPT".
1837	111	2014-09-03 09:43:16	76	105	Vous avez indiqué avoir assisté à la formation "2011- Equipier d'évacuation - INPT".
1838	67	2014-09-03 15:23:52	27	34	Ajout du besoin "Calculs scientfiques parallèles".
1839	59	2014-09-05 09:39:21	27	2	Ajout du besoin "Sauveteur Secouriste du Travail - Formation initiale".
1840	77	2014-09-08 09:51:25	76	55	Vous avez indiqué avoir assisté à la formation "2011- Formation Thermographie".
1841	77	2014-09-08 09:52:55	76	220	Vous avez indiqué avoir assisté à la formation "2011- Séminaire Réseau Plasmas Froids".
1842	112	2014-09-08 09:53:00	92	253	Vous avez indiqué n'avoir pas assisté à la formation "2013- Altium Designer".
1843	77	2014-09-08 09:56:41	92	248	Vous avez indiqué n'avoir pas assisté à la formation "2013- Le management pour les responsables d'équipes et les chefs d'équipes".
1844	77	2014-09-08 09:56:55	76	219	Vous avez indiqué avoir assisté à la formation "2011- Electronique et instrumentation pour le vivant".
1845	77	2014-09-08 10:01:54	27	57	Ajout du besoin "Le management pour les responsables d'équipes et les chefs d'équipes".
1846	77	2014-09-08 10:03:00	27	12	Ajout du besoin "Anglais".
1847	53	2014-09-08 10:11:26	76	277	Vous avez indiqué avoir assisté à la formation "2014- Les Réseaux Sociaux pour la Culture Scientifique".
1848	53	2014-09-08 10:11:39	76	279	Vous avez indiqué avoir assisté à la formation "2014- Word Perfectionnement".
1849	53	2014-09-08 10:11:51	76	280	Vous avez indiqué avoir assisté à la formation "2014- Professionalisation des membres du jury".
1850	37	2014-09-08 10:15:53	44	21	Votre inscription à la demande de formation "2011- 15èmes Rencontre régionale du réseau des électroniciens DR14" a été acceptée.
1851	37	2014-09-08 10:15:53	76	21	Vous avez indiqué avoir assisté à la formation "2011- 15èmes Rencontre régionale du réseau des électroniciens DR14".
1852	59	2014-09-08 10:15:58	44	21	Votre inscription à la demande de formation "2011- 15èmes Rencontre régionale du réseau des électroniciens DR14" a été acceptée.
1853	59	2014-09-08 10:15:58	76	21	Vous avez indiqué avoir assisté à la formation "2011- 15èmes Rencontre régionale du réseau des électroniciens DR14".
1854	59	2014-09-08 10:30:31	28	352	Inscription à la demande "2014- Sauveteur secouriste du travail - CNRS".
1856	82	2014-09-08 14:00:32	92	249	Vous avez indiqué n'avoir pas assisté à la formation "2013- Conception d'installations sous vide".
1857	82	2014-09-08 14:00:50	92	114	Vous avez indiqué n'avoir pas assisté à la formation "2012- Journées Techniques LabVIEW".
1858	82	2014-09-08 14:01:00	76	230	Vous avez indiqué avoir assisté à la formation "2011- SST Formation initiale - UPS".
1859	82	2014-09-08 14:01:12	76	31	Vous avez indiqué avoir assisté à la formation "2011- Inventor - Initiation avancée et perfectionnement".
1860	82	2014-09-08 14:01:24	76	111	Vous avez indiqué avoir assisté à la formation "2011- Journée thématique du vide".
1861	82	2014-09-08 14:01:35	76	218	Vous avez indiqué avoir assisté à la formation "2011- Conception et utilisation d'un réacteur a plasma de type industriel".
1862	82	2014-09-08 14:01:45	76	73	Vous avez indiqué avoir assisté à la formation "2011- Formation Caméra thermique".
1863	43	2014-09-09 09:39:35	76	21	Vous avez indiqué avoir assisté à la formation "2011- 15èmes Rencontre régionale du réseau des électroniciens DR14".
1864	104	2014-09-09 09:47:10	76	122	Vous avez indiqué avoir assisté à la formation "2012- Journée Nano - CNRS".
1865	104	2014-09-09 09:47:24	76	55	Vous avez indiqué avoir assisté à la formation "2011- Formation Thermographie".
1866	57	2014-09-09 11:35:12	76	334	Vous avez indiqué avoir assisté à la formation "2014- Dématérialisation : Formation gestionnaires - UPS".
1867	95	2014-09-09 18:22:39	76	147	Vous avez indiqué avoir assisté à la formation "2013- Inventor, par le réseau des électroniciens DR14".
1869	43	2014-09-10 11:13:34	27	58	Ajout du besoin "Programmation interface LABVIEW , debutant et avancé".
1871	43	2014-09-10 11:33:02	27	59	Ajout du besoin "Programmation Language C, débutant et avancé".
1872	35	2014-09-11 11:34:17	76	117	Vous avez indiqué avoir assisté à la formation "2012- Formation Communication assistant de prévention - CNRS".
1873	35	2014-09-11 11:34:32	76	116	Vous avez indiqué avoir assisté à la formation "2012- Préparation aux concours internes - CNRS".
1874	35	2014-09-11 11:34:41	76	115	Vous avez indiqué avoir assisté à la formation "2012- Formation assistant de prévention - CNRS".
1875	35	2014-09-11 11:34:53	76	114	Vous avez indiqué avoir assisté à la formation "2012- Journées Techniques LabVIEW".
1876	35	2014-09-11 11:35:06	76	66	Vous avez indiqué avoir assisté à la formation "2011- Formation risques chimiques - INPT".
1877	35	2014-09-11 11:35:19	76	111	Vous avez indiqué avoir assisté à la formation "2011- Journée thématique du vide".
1878	35	2014-09-11 11:35:31	76	31	Vous avez indiqué avoir assisté à la formation "2011- Inventor - Initiation avancée et perfectionnement".
1879	35	2014-09-11 11:35:59	76	73	Vous avez indiqué avoir assisté à la formation "2011- Formation Caméra thermique".
1880	157	2014-09-12 09:02:11	43	54	Le besoin "Formation Microscopie à Force Atomique" a été pourvu.
1883	71	2014-09-12 09:03:51	43	52	Le besoin "Formation CFAO ESPRIT Module 4/5 Axes" a été pourvu.
1884	71	2014-09-15 13:50:40	44	305	Votre inscription à la demande de formation "2014- Sécurité incendie - Manipulation des moyens de secours - INPT" a été acceptée.
1885	71	2014-09-15 13:50:40	76	305	Vous avez indiqué avoir assisté à la formation "2014- Sécurité incendie - Manipulation des moyens de secours - INPT".
1886	156	2014-09-15 13:52:59	28	305	Inscription à la demande "2014- Sécurité incendie - Manipulation des moyens de secours - INPT".
1887	156	2014-09-15 13:53:12	44	305	Votre inscription à la demande de formation "2014- Sécurité incendie - Manipulation des moyens de secours - INPT" a été acceptée.
1888	156	2014-09-15 13:53:12	76	305	Vous avez indiqué avoir assisté à la formation "2014- Sécurité incendie - Manipulation des moyens de secours - INPT".
1889	132	2014-09-15 14:02:51	76	246	Vous avez indiqué avoir assisté à la formation "2013- SST Formation initiale - INPT".
1890	4	2014-09-15 15:48:55	76	350	Vous avez indiqué avoir assisté à la formation "2014- SPIP – Développement et adaptation du nouveau kit CNRS - CNRS".
1891	4	2014-09-15 15:49:33	28	352	Inscription à la demande "2014- Sauveteur secouriste du travail - CNRS".
1892	4	2014-09-15 16:44:09	44	352	Votre inscription à la demande de formation "2014- Sauveteur secouriste du travail - CNRS" a été acceptée.
1893	155	2014-09-17 10:01:16	27	60	Ajout du besoin "Cours Allemand".
1894	35	2014-09-19 13:14:43	28	354	Inscription à la demande "Rencontre Métallographie et Microscopie".
1895	54	2014-09-19 14:16:59	28	354	Inscription à la demande "2014- Rencontre Métallographie et Microscopie".
1896	41	2014-09-19 14:17:12	28	354	Inscription à la demande "2014- Rencontre Métallographie et Microscopie".
1897	35	2014-09-19 14:17:26	44	354	Votre inscription à la demande de formation "2014- Rencontre Métallographie et Microscopie" a été acceptée.
1898	35	2014-09-19 14:17:26	76	354	Vous avez indiqué avoir assisté à la formation "2014- Rencontre Métallographie et Microscopie".
1899	41	2014-09-19 14:17:33	44	354	Votre inscription à la demande de formation "2014- Rencontre Métallographie et Microscopie" a été acceptée.
1900	41	2014-09-19 14:17:33	76	354	Vous avez indiqué avoir assisté à la formation "2014- Rencontre Métallographie et Microscopie".
1901	54	2014-09-19 14:17:42	44	354	Votre inscription à la demande de formation "2014- Rencontre Métallographie et Microscopie" a été acceptée.
1902	54	2014-09-19 14:17:42	76	354	Vous avez indiqué avoir assisté à la formation "2014- Rencontre Métallographie et Microscopie".
1903	40	2014-09-23 11:11:38	28	356	Inscription à la demande "2014- Le droit à l'image appliqué à la production scientifique - CNRS".
1904	40	2014-09-23 11:24:15	44	356	Votre inscription à la demande de formation "2014- Le droit à l'image appliqué à la production scientifique - CNRS" a été acceptée.
1905	51	2014-09-24 14:24:02	28	357	Inscription à la demande "2014- La fabrication additive métallique - CNRS".
1906	51	2014-09-24 14:32:03	44	357	Votre inscription à la demande de formation "2014- La fabrication additive métallique - CNRS" a été acceptée.
1907	71	2014-09-24 14:34:28	28	357	Inscription à la demande "2014- La fabrication additive métallique - CNRS".
1909	54	2014-10-02 08:25:39	76	336	Vous avez indiqué avoir assisté à la formation "2014- Sécurité incendie et manipulation des extincteurs - UPS".
1910	119	2014-10-07 11:20:09	44	223	Votre inscription à la demande de formation "2011- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
1911	119	2014-10-07 11:20:09	76	223	Vous avez indiqué avoir assisté à la formation "2011- Habilitation électrique du personnel non électricien - INPT".
1912	148	2014-10-07 11:20:15	44	223	Votre inscription à la demande de formation "2011- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
1913	148	2014-10-07 11:20:15	76	223	Vous avez indiqué avoir assisté à la formation "2011- Habilitation électrique du personnel non électricien - INPT".
1914	120	2014-10-07 11:20:56	44	223	Votre inscription à la demande de formation "2011- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
1915	120	2014-10-07 11:20:56	76	223	Vous avez indiqué avoir assisté à la formation "2011- Habilitation électrique du personnel non électricien - INPT".
1916	118	2014-10-07 11:21:02	44	223	Votre inscription à la demande de formation "2011- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
1917	118	2014-10-07 11:21:02	76	223	Vous avez indiqué avoir assisté à la formation "2011- Habilitation électrique du personnel non électricien - INPT".
1924	186	2014-10-07 14:24:36	28	360	Inscription à la demande "2009- Habilitation électrique du personnel non électricien - INPT".
1925	186	2014-10-07 14:28:02	44	360	Votre inscription à la demande de formation "2009- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
1926	186	2014-10-07 14:28:02	76	360	Vous avez indiqué avoir assisté à la formation "2009- Habilitation électrique du personnel non électricien - INPT".
1927	184	2014-10-07 14:28:41	28	360	Inscription à la demande "2009- Habilitation électrique du personnel non électricien - INPT".
1928	185	2014-10-07 14:29:16	28	223	Inscription à la demande "2011- Habilitation électrique du personnel non électricien - INPT".
1929	184	2014-10-07 14:30:08	44	360	Votre inscription à la demande de formation "2009- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
1930	184	2014-10-07 14:30:08	76	360	Vous avez indiqué avoir assisté à la formation "2009- Habilitation électrique du personnel non électricien - INPT".
1931	185	2014-10-07 14:30:21	44	223	Votre inscription à la demande de formation "2011- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
1932	185	2014-10-07 14:30:21	76	223	Vous avez indiqué avoir assisté à la formation "2011- Habilitation électrique du personnel non électricien - INPT".
1933	132	2014-10-07 14:31:30	28	223	Inscription à la demande "2011- Habilitation électrique du personnel non électricien - INPT".
1934	132	2014-10-07 14:31:44	44	223	Votre inscription à la demande de formation "2011- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
1935	132	2014-10-07 14:31:44	76	223	Vous avez indiqué avoir assisté à la formation "2011- Habilitation électrique du personnel non électricien - INPT".
1936	115	2014-10-07 14:33:03	28	360	Inscription à la demande "2009- Habilitation électrique du personnel non électricien - INPT".
1937	115	2014-10-07 14:33:17	44	360	Votre inscription à la demande de formation "2009- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
1938	115	2014-10-07 14:33:17	76	360	Vous avez indiqué avoir assisté à la formation "2009- Habilitation électrique du personnel non électricien - INPT".
1939	187	2014-10-07 14:34:55	28	360	Inscription à la demande "2009- Habilitation électrique du personnel non électricien - INPT".
1940	187	2014-10-07 14:35:08	44	360	Votre inscription à la demande de formation "2009- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
1941	187	2014-10-07 14:35:08	76	360	Vous avez indiqué avoir assisté à la formation "2009- Habilitation électrique du personnel non électricien - INPT".
1942	188	2014-10-07 14:37:52	28	360	Inscription à la demande "2009- Habilitation électrique du personnel non électricien - INPT".
1943	188	2014-10-07 14:38:13	44	360	Votre inscription à la demande de formation "2009- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
1944	188	2014-10-07 14:38:13	76	360	Vous avez indiqué avoir assisté à la formation "2009- Habilitation électrique du personnel non électricien - INPT".
1945	189	2014-10-07 14:40:16	28	360	Inscription à la demande "2009- Habilitation électrique du personnel non électricien - INPT".
1946	189	2014-10-07 14:40:29	44	360	Votre inscription à la demande de formation "2009- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
1947	189	2014-10-07 14:40:29	76	360	Vous avez indiqué avoir assisté à la formation "2009- Habilitation électrique du personnel non électricien - INPT".
1948	190	2014-10-07 14:42:29	28	360	Inscription à la demande "2009- Habilitation électrique du personnel non électricien - INPT".
1949	190	2014-10-07 14:42:55	44	360	Votre inscription à la demande de formation "2009- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
1950	190	2014-10-07 14:42:55	76	360	Vous avez indiqué avoir assisté à la formation "2009- Habilitation électrique du personnel non électricien - INPT".
1951	191	2014-10-07 14:44:44	28	360	Inscription à la demande "2009- Habilitation électrique du personnel non électricien - INPT".
1952	191	2014-10-07 14:45:00	44	360	Votre inscription à la demande de formation "2009- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
1953	191	2014-10-07 14:45:00	76	360	Vous avez indiqué avoir assisté à la formation "2009- Habilitation électrique du personnel non électricien - INPT".
1954	192	2014-10-07 14:46:27	28	360	Inscription à la demande "2009- Habilitation électrique du personnel non électricien - INPT".
1955	192	2014-10-07 14:46:43	44	360	Votre inscription à la demande de formation "2009- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
1956	192	2014-10-07 14:46:43	76	360	Vous avez indiqué avoir assisté à la formation "2009- Habilitation électrique du personnel non électricien - INPT".
1957	193	2014-10-07 14:48:45	28	360	Inscription à la demande "2009- Habilitation électrique du personnel non électricien - INPT".
1958	193	2014-10-07 14:49:10	44	360	Votre inscription à la demande de formation "2009- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
1959	193	2014-10-07 14:49:10	76	360	Vous avez indiqué avoir assisté à la formation "2009- Habilitation électrique du personnel non électricien - INPT".
1960	194	2014-10-07 14:50:24	28	360	Inscription à la demande "2009- Habilitation électrique du personnel non électricien - INPT".
1961	194	2014-10-07 14:50:42	44	360	Votre inscription à la demande de formation "2009- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
1962	194	2014-10-07 14:50:42	76	360	Vous avez indiqué avoir assisté à la formation "2009- Habilitation électrique du personnel non électricien - INPT".
1963	195	2014-10-07 14:53:04	28	360	Inscription à la demande "2009- Habilitation électrique du personnel non électricien - INPT".
1964	195	2014-10-07 14:53:19	44	360	Votre inscription à la demande de formation "2009- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
1965	195	2014-10-07 14:53:19	76	360	Vous avez indiqué avoir assisté à la formation "2009- Habilitation électrique du personnel non électricien - INPT".
1966	196	2014-10-07 14:54:30	28	360	Inscription à la demande "2009- Habilitation électrique du personnel non électricien - INPT".
1967	196	2014-10-07 14:54:43	44	360	Votre inscription à la demande de formation "2009- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
1968	196	2014-10-07 14:54:43	76	360	Vous avez indiqué avoir assisté à la formation "2009- Habilitation électrique du personnel non électricien - INPT".
1969	197	2014-10-07 14:56:26	28	360	Inscription à la demande "2009- Habilitation électrique du personnel non électricien - INPT".
1970	197	2014-10-07 14:56:39	44	360	Votre inscription à la demande de formation "2009- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
1971	197	2014-10-07 14:56:39	76	360	Vous avez indiqué avoir assisté à la formation "2009- Habilitation électrique du personnel non électricien - INPT".
1972	198	2014-10-07 14:58:14	28	360	Inscription à la demande "2009- Habilitation électrique du personnel non électricien - INPT".
1973	198	2014-10-07 14:58:29	44	360	Votre inscription à la demande de formation "2009- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
1974	198	2014-10-07 14:58:29	76	360	Vous avez indiqué avoir assisté à la formation "2009- Habilitation électrique du personnel non électricien - INPT".
1975	199	2014-10-07 14:59:57	28	360	Inscription à la demande "2009- Habilitation électrique du personnel non électricien - INPT".
1976	199	2014-10-07 15:00:11	44	360	Votre inscription à la demande de formation "2009- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
1977	199	2014-10-07 15:00:11	76	360	Vous avez indiqué avoir assisté à la formation "2009- Habilitation électrique du personnel non électricien - INPT".
1978	200	2014-10-07 15:03:04	28	361	Inscription à la demande "2010- Habilitation électrique du personnel non électricien - INPT".
1979	200	2014-10-07 15:03:16	44	361	Votre inscription à la demande de formation "2010- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
1980	200	2014-10-07 15:03:16	76	361	Vous avez indiqué avoir assisté à la formation "2010- Habilitation électrique du personnel non électricien - INPT".
1981	201	2014-10-07 15:05:47	28	361	Inscription à la demande "2010- Habilitation électrique du personnel non électricien - INPT".
1982	201	2014-10-07 15:06:00	44	361	Votre inscription à la demande de formation "2010- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
1983	201	2014-10-07 15:06:00	76	361	Vous avez indiqué avoir assisté à la formation "2010- Habilitation électrique du personnel non électricien - INPT".
1984	202	2014-10-07 15:07:21	28	361	Inscription à la demande "2010- Habilitation électrique du personnel non électricien - INPT".
1985	202	2014-10-07 15:07:36	44	361	Votre inscription à la demande de formation "2010- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
1986	202	2014-10-07 15:07:36	76	361	Vous avez indiqué avoir assisté à la formation "2010- Habilitation électrique du personnel non électricien - INPT".
1987	128	2014-10-07 15:08:44	28	361	Inscription à la demande "2010- Habilitation électrique du personnel non électricien - INPT".
1988	128	2014-10-07 15:08:55	44	361	Votre inscription à la demande de formation "2010- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
1989	128	2014-10-07 15:08:55	76	361	Vous avez indiqué avoir assisté à la formation "2010- Habilitation électrique du personnel non électricien - INPT".
1990	139	2014-10-07 15:09:51	28	361	Inscription à la demande "2010- Habilitation électrique du personnel non électricien - INPT".
1991	139	2014-10-07 15:10:07	44	361	Votre inscription à la demande de formation "2010- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
1992	139	2014-10-07 15:10:07	76	361	Vous avez indiqué avoir assisté à la formation "2010- Habilitation électrique du personnel non électricien - INPT".
1993	203	2014-10-07 15:13:18	28	223	Inscription à la demande "2011- Habilitation électrique du personnel non électricien - INPT".
1994	203	2014-10-07 15:13:32	44	223	Votre inscription à la demande de formation "2011- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
1995	203	2014-10-07 15:13:32	76	223	Vous avez indiqué avoir assisté à la formation "2011- Habilitation électrique du personnel non électricien - INPT".
1996	204	2014-10-07 15:17:17	28	223	Inscription à la demande "2011- Habilitation électrique du personnel non électricien - INPT".
1997	204	2014-10-07 15:17:34	44	223	Votre inscription à la demande de formation "2011- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
1998	204	2014-10-07 15:17:34	76	223	Vous avez indiqué avoir assisté à la formation "2011- Habilitation électrique du personnel non électricien - INPT".
1999	205	2014-10-07 15:19:47	28	223	Inscription à la demande "2011- Habilitation électrique du personnel non électricien - INPT".
2000	205	2014-10-07 15:20:02	44	223	Votre inscription à la demande de formation "2011- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
2001	205	2014-10-07 15:20:02	76	223	Vous avez indiqué avoir assisté à la formation "2011- Habilitation électrique du personnel non électricien - INPT".
2002	206	2014-10-07 15:38:09	28	269	Inscription à la demande "2013- Habilitation électrique du personnel non électricien - INPT".
2003	206	2014-10-07 15:38:33	44	269	Votre inscription à la demande de formation "2013- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
2004	206	2014-10-07 15:38:33	76	269	Vous avez indiqué avoir assisté à la formation "2013- Habilitation électrique du personnel non électricien - INPT".
2005	207	2014-10-07 15:40:51	28	269	Inscription à la demande "2013- Habilitation électrique du personnel non électricien - INPT".
2006	207	2014-10-07 15:41:04	44	269	Votre inscription à la demande de formation "2013- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
2007	207	2014-10-07 15:41:04	76	269	Vous avez indiqué avoir assisté à la formation "2013- Habilitation électrique du personnel non électricien - INPT".
2008	208	2014-10-07 15:58:02	28	362	Inscription à la demande "2012- Habilitation électrique du personnel non électricien - INPT".
2108	46	2014-10-24 12:00:15	27	66	Ajout du besoin "gestion de la maintenance d'un parc d'appareil de métrologie".
2009	208	2014-10-07 15:58:15	44	362	Votre inscription à la demande de formation "2012- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
2010	208	2014-10-07 15:58:15	76	362	Vous avez indiqué avoir assisté à la formation "2012- Habilitation électrique du personnel non électricien - INPT".
2011	180	2014-10-07 15:59:38	28	362	Inscription à la demande "2012- Habilitation électrique du personnel non électricien - INPT".
2012	180	2014-10-07 15:59:49	44	362	Votre inscription à la demande de formation "2012- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
2013	180	2014-10-07 15:59:49	76	362	Vous avez indiqué avoir assisté à la formation "2012- Habilitation électrique du personnel non électricien - INPT".
2014	209	2014-10-07 16:01:40	28	362	Inscription à la demande "2012- Habilitation électrique du personnel non électricien - INPT".
2015	209	2014-10-07 16:01:52	44	362	Votre inscription à la demande de formation "2012- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
2016	209	2014-10-07 16:01:52	76	362	Vous avez indiqué avoir assisté à la formation "2012- Habilitation électrique du personnel non électricien - INPT".
2017	210	2014-10-07 16:02:57	28	362	Inscription à la demande "2012- Habilitation électrique du personnel non électricien - INPT".
2018	210	2014-10-07 16:03:09	44	362	Votre inscription à la demande de formation "2012- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
2019	210	2014-10-07 16:03:09	76	362	Vous avez indiqué avoir assisté à la formation "2012- Habilitation électrique du personnel non électricien - INPT".
2020	211	2014-10-07 16:04:38	28	362	Inscription à la demande "2012- Habilitation électrique du personnel non électricien - INPT".
2021	211	2014-10-07 16:04:49	44	362	Votre inscription à la demande de formation "2012- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
2022	211	2014-10-07 16:04:49	76	362	Vous avez indiqué avoir assisté à la formation "2012- Habilitation électrique du personnel non électricien - INPT".
2023	212	2014-10-07 16:06:13	28	362	Inscription à la demande "2012- Habilitation électrique du personnel non électricien - INPT".
2024	212	2014-10-07 16:06:29	44	362	Votre inscription à la demande de formation "2012- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
2025	212	2014-10-07 16:06:29	76	362	Vous avez indiqué avoir assisté à la formation "2012- Habilitation électrique du personnel non électricien - INPT".
2026	213	2014-10-07 16:07:46	28	362	Inscription à la demande "2012- Habilitation électrique du personnel non électricien - INPT".
2027	213	2014-10-07 16:08:00	44	362	Votre inscription à la demande de formation "2012- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
2028	213	2014-10-07 16:08:00	76	362	Vous avez indiqué avoir assisté à la formation "2012- Habilitation électrique du personnel non électricien - INPT".
2029	214	2014-10-07 16:09:37	28	362	Inscription à la demande "2012- Habilitation électrique du personnel non électricien - INPT".
2030	214	2014-10-07 16:09:50	44	362	Votre inscription à la demande de formation "2012- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
2031	214	2014-10-07 16:09:50	76	362	Vous avez indiqué avoir assisté à la formation "2012- Habilitation électrique du personnel non électricien - INPT".
2032	215	2014-10-07 16:25:06	28	362	Inscription à la demande "2012- Habilitation électrique du personnel non électricien - INPT".
2033	215	2014-10-07 16:25:28	44	362	Votre inscription à la demande de formation "2012- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
2034	215	2014-10-07 16:25:28	76	362	Vous avez indiqué avoir assisté à la formation "2012- Habilitation électrique du personnel non électricien - INPT".
2035	216	2014-10-07 16:28:53	28	362	Inscription à la demande "2012- Habilitation électrique du personnel non électricien - INPT".
2036	216	2014-10-07 16:29:10	44	362	Votre inscription à la demande de formation "2012- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
2037	216	2014-10-07 16:29:10	76	362	Vous avez indiqué avoir assisté à la formation "2012- Habilitation électrique du personnel non électricien - INPT".
2038	217	2014-10-07 16:31:08	28	362	Inscription à la demande "2012- Habilitation électrique du personnel non électricien - INPT".
2039	217	2014-10-07 16:31:19	44	362	Votre inscription à la demande de formation "2012- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
2040	217	2014-10-07 16:31:19	76	362	Vous avez indiqué avoir assisté à la formation "2012- Habilitation électrique du personnel non électricien - INPT".
2041	218	2014-10-07 16:34:07	28	362	Inscription à la demande "2012- Habilitation électrique du personnel non électricien - INPT".
2042	218	2014-10-07 16:34:20	44	362	Votre inscription à la demande de formation "2012- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
2043	218	2014-10-07 16:34:20	76	362	Vous avez indiqué avoir assisté à la formation "2012- Habilitation électrique du personnel non électricien - INPT".
2044	219	2014-10-07 16:35:23	28	362	Inscription à la demande "2012- Habilitation électrique du personnel non électricien - INPT".
2045	219	2014-10-07 16:35:38	44	362	Votre inscription à la demande de formation "2012- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
2046	219	2014-10-07 16:35:38	76	362	Vous avez indiqué avoir assisté à la formation "2012- Habilitation électrique du personnel non électricien - INPT".
2047	220	2014-10-07 16:36:49	28	362	Inscription à la demande "2012- Habilitation électrique du personnel non électricien - INPT".
2048	220	2014-10-07 16:37:08	44	362	Votre inscription à la demande de formation "2012- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
2049	220	2014-10-07 16:37:08	76	362	Vous avez indiqué avoir assisté à la formation "2012- Habilitation électrique du personnel non électricien - INPT".
2050	221	2014-10-07 16:38:10	28	362	Inscription à la demande "2012- Habilitation électrique du personnel non électricien - INPT".
2051	221	2014-10-07 16:38:26	44	362	Votre inscription à la demande de formation "2012- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
2052	221	2014-10-07 16:38:26	76	362	Vous avez indiqué avoir assisté à la formation "2012- Habilitation électrique du personnel non électricien - INPT".
2053	222	2014-10-07 17:01:36	28	269	Inscription à la demande "2013- Habilitation électrique du personnel non électricien - INPT".
2109	11	2014-10-24 12:05:43	27	61	Ajout du besoin "initiation à MATLAB".
2054	222	2014-10-07 17:02:01	44	269	Votre inscription à la demande de formation "2013- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
2055	222	2014-10-07 17:02:01	76	269	Vous avez indiqué avoir assisté à la formation "2013- Habilitation électrique du personnel non électricien - INPT".
2056	223	2014-10-07 17:03:36	28	269	Inscription à la demande "2013- Habilitation électrique du personnel non électricien - INPT".
2057	223	2014-10-07 17:03:52	44	269	Votre inscription à la demande de formation "2013- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
2058	223	2014-10-07 17:03:52	76	269	Vous avez indiqué avoir assisté à la formation "2013- Habilitation électrique du personnel non électricien - INPT".
2059	224	2014-10-07 17:05:32	28	269	Inscription à la demande "2013- Habilitation électrique du personnel non électricien - INPT".
2060	224	2014-10-07 17:05:47	44	269	Votre inscription à la demande de formation "2013- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
2061	224	2014-10-07 17:05:47	76	269	Vous avez indiqué avoir assisté à la formation "2013- Habilitation électrique du personnel non électricien - INPT".
2062	225	2014-10-07 17:07:38	28	269	Inscription à la demande "2013- Habilitation électrique du personnel non électricien - INPT".
2063	225	2014-10-07 17:07:51	44	269	Votre inscription à la demande de formation "2013- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
2064	225	2014-10-07 17:07:51	76	269	Vous avez indiqué avoir assisté à la formation "2013- Habilitation électrique du personnel non électricien - INPT".
2065	226	2014-10-07 17:10:31	28	269	Inscription à la demande "2013- Habilitation électrique du personnel non électricien - INPT".
2066	226	2014-10-07 17:10:47	44	269	Votre inscription à la demande de formation "2013- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
2067	226	2014-10-07 17:10:47	76	269	Vous avez indiqué avoir assisté à la formation "2013- Habilitation électrique du personnel non électricien - INPT".
2068	227	2014-10-07 17:11:58	28	269	Inscription à la demande "2013- Habilitation électrique du personnel non électricien - INPT".
2069	227	2014-10-07 17:12:13	44	269	Votre inscription à la demande de formation "2013- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
2070	227	2014-10-07 17:12:13	76	269	Vous avez indiqué avoir assisté à la formation "2013- Habilitation électrique du personnel non électricien - INPT".
2071	228	2014-10-07 17:13:22	28	269	Inscription à la demande "2013- Habilitation électrique du personnel non électricien - INPT".
2072	228	2014-10-07 17:13:37	44	269	Votre inscription à la demande de formation "2013- Habilitation électrique du personnel non électricien - INPT" a été acceptée.
2073	228	2014-10-07 17:13:37	76	269	Vous avez indiqué avoir assisté à la formation "2013- Habilitation électrique du personnel non électricien - INPT".
2074	229	2014-10-08 10:14:48	28	346	Inscription à la demande "2014- Habilitation électrique des personnels non électricien -INPT".
2075	229	2014-10-08 10:16:49	44	346	Votre inscription à la demande de formation "2014- Habilitation électrique des personnels non électricien -INPT" a été acceptée.
2076	229	2014-10-08 10:16:49	76	346	Vous avez indiqué avoir assisté à la formation "2014- Habilitation électrique des personnels non électricien -INPT".
2080	231	2014-10-08 10:25:03	28	346	Inscription à la demande "2014- Habilitation électrique des personnels non électricien -INPT".
2081	231	2014-10-08 10:25:16	44	346	Votre inscription à la demande de formation "2014- Habilitation électrique des personnels non électricien -INPT" a été acceptée.
2082	231	2014-10-08 10:25:16	76	346	Vous avez indiqué avoir assisté à la formation "2014- Habilitation électrique des personnels non électricien -INPT".
2083	232	2014-10-08 10:28:02	28	346	Inscription à la demande "2014- Habilitation électrique des personnels non électricien -INPT".
2084	232	2014-10-08 10:28:17	44	346	Votre inscription à la demande de formation "2014- Habilitation électrique des personnels non électricien -INPT" a été acceptée.
2085	232	2014-10-08 10:28:17	76	346	Vous avez indiqué avoir assisté à la formation "2014- Habilitation électrique des personnels non électricien -INPT".
2086	233	2014-10-08 10:29:43	28	346	Inscription à la demande "2014- Habilitation électrique des personnels non électricien -INPT".
2087	233	2014-10-08 10:29:56	44	346	Votre inscription à la demande de formation "2014- Habilitation électrique des personnels non électricien -INPT" a été acceptée.
2088	233	2014-10-08 10:29:56	76	346	Vous avez indiqué avoir assisté à la formation "2014- Habilitation électrique des personnels non électricien -INPT".
2089	234	2014-10-08 10:32:31	28	346	Inscription à la demande "2014- Habilitation électrique des personnels non électricien -INPT".
2090	234	2014-10-08 10:32:50	44	346	Votre inscription à la demande de formation "2014- Habilitation électrique des personnels non électricien -INPT" a été acceptée.
2091	234	2014-10-08 10:32:50	76	346	Vous avez indiqué avoir assisté à la formation "2014- Habilitation électrique des personnels non électricien -INPT".
2092	54	2014-10-08 12:29:23	27	61	Ajout du besoin "formation MATLAB".
2095	174	2014-10-08 12:35:38	76	307	Vous avez indiqué avoir assisté à la formation "2014- Initiation aux méthodes de spectroscopie laser pour l'analyse de milieux réactifs".
2096	235	2014-10-08 14:26:57	26	\N	Création de votre compte.
2097	112	2014-10-09 09:38:34	27	12	Ajout du besoin "Anglais".
2099	53	2014-10-09 13:53:16	28	367	Inscription à la demande "Le droit à l'image appliqué à la production scientifique".
2102	53	2014-10-10 17:59:02	28	356	Inscription à la demande "2014- Le droit à l'image appliqué à la production scientifique - CNRS".
2103	53	2014-10-10 18:11:55	27	63	Ajout du besoin "Formation au montage vidéo".
2104	53	2014-10-10 18:12:05	27	62	Ajout du besoin "Formation PAO".
2105	53	2014-10-10 18:14:34	44	356	Votre inscription à la demande de formation "2014- Le droit à l'image appliqué à la production scientifique - CNRS" a été acceptée.
2106	46	2014-10-24 11:56:01	27	64	Ajout du besoin "Logiciel PLECS".
2107	46	2014-10-24 11:59:15	27	65	Ajout du besoin "Anglais à l'écrit".
2110	46	2014-10-24 12:09:32	28	370	Inscription à la demande "2014- Thermique pour le dimensionnement de dissipateurs".
2111	46	2014-10-24 12:11:00	44	370	Votre inscription à la demande de formation "2014- Thermique pour le dimensionnement de dissipateurs" a été acceptée.
2112	46	2014-10-24 12:11:00	76	370	Vous avez indiqué avoir assisté à la formation "2014- Thermique pour le dimensionnement de dissipateurs".
2113	46	2014-10-24 12:11:35	27	67	Ajout du besoin "hydraulique pour le dimensionnement de dissipateurs (plaque à eau)".
2114	46	2014-10-24 12:11:43	43	50	Le besoin "thermique et hydraulique pour le dimensionnement de dissipateurs (refroidisseur à air forcé, plaque à eau)" a été pourvu.
2115	59	2014-10-24 12:12:55	43	2	Le besoin "Sauveteur Secouriste du Travail - Formation initiale" a été pourvu.
2116	35	2014-10-24 12:15:24	43	3	Le besoin "Sauveteur Secouriste du Travail - Recyclage" a été pourvu.
2117	112	2014-10-24 12:16:33	27	64	Ajout du besoin "Logiciel PLECS".
2118	66	2014-10-24 12:17:04	27	64	Ajout du besoin "Logiciel PLECS".
2119	59	2014-10-24 12:17:30	27	64	Ajout du besoin "Logiciel PLECS".
2120	51	2014-10-24 15:11:23	27	69	Ajout du besoin "Logiciel "ESPRIT 3D"".
2121	51	2014-10-24 15:11:35	27	70	Ajout du besoin "Soudure "mécanique"".
2122	49	2014-10-24 15:12:01	27	69	Ajout du besoin "Logiciel "ESPRIT 3D"".
2123	49	2014-10-24 15:12:08	27	70	Ajout du besoin "Soudure "mécanique"".
2124	71	2014-10-24 15:12:28	27	69	Ajout du besoin "Logiciel "ESPRIT 3D"".
2125	71	2014-10-24 15:12:34	27	70	Ajout du besoin "Soudure "mécanique"".
2126	114	2014-10-24 15:13:13	27	68	Ajout du besoin "Inventor 2015".
2127	82	2014-10-24 15:13:28	27	68	Ajout du besoin "Inventor 2015".
2128	40	2014-10-24 15:13:43	27	68	Ajout du besoin "Inventor 2015".
2129	113	2014-10-24 15:13:59	27	68	Ajout du besoin "Inventor 2015".
2130	4	2014-11-19 17:58:09	76	352	Vous avez indiqué avoir assisté à la formation "2014- Sauveteur secouriste du travail - CNRS".
2131	4	2014-11-19 17:58:38	76	349	Vous avez indiqué avoir assisté à la formation "2014- Formation informatique : PERL - CNRS".
2132	57	2014-11-20 16:05:28	28	375	Inscription à la demande "IMMOBILISATIONS METIER/OUTIL".
2133	57	2014-11-24 10:52:22	44	375	Votre inscription à la demande de formation "IMMOBILISATIONS METIER/OUTIL" a été acceptée.
2134	57	2014-11-24 10:52:22	76	375	Vous avez indiqué avoir assisté à la formation "IMMOBILISATIONS METIER/OUTIL".
2135	182	2014-11-24 11:19:18	28	371	Inscription à la demande "2014- Habilitation électrique des personnels non électricien (session novembre) -INPT".
2136	182	2014-11-24 11:19:29	44	371	Votre inscription à la demande de formation "2014- Habilitation électrique des personnels non électricien (session novembre) -INPT" a été acceptée.
2137	182	2014-11-24 11:19:29	76	371	Vous avez indiqué avoir assisté à la formation "2014- Habilitation électrique des personnels non électricien (session novembre) -INPT".
2138	236	2014-11-24 11:24:17	28	371	Inscription à la demande "2014- Habilitation électrique des personnels non électricien (session novembre) -INPT".
2139	236	2014-11-24 11:24:30	44	371	Votre inscription à la demande de formation "2014- Habilitation électrique des personnels non électricien (session novembre) -INPT" a été acceptée.
2140	236	2014-11-24 11:24:30	76	371	Vous avez indiqué avoir assisté à la formation "2014- Habilitation électrique des personnels non électricien (session novembre) -INPT".
2141	237	2014-11-24 11:27:18	28	371	Inscription à la demande "2014- Habilitation électrique des personnels non électricien (session novembre) -INPT".
2142	237	2014-11-24 11:27:32	44	371	Votre inscription à la demande de formation "2014- Habilitation électrique des personnels non électricien (session novembre) -INPT" a été acceptée.
2143	237	2014-11-24 11:27:32	76	371	Vous avez indiqué avoir assisté à la formation "2014- Habilitation électrique des personnels non électricien (session novembre) -INPT".
2144	238	2014-11-24 11:31:06	28	371	Inscription à la demande "2014- Habilitation électrique des personnels non électricien (session novembre) -INPT".
2145	238	2014-11-24 11:31:16	44	371	Votre inscription à la demande de formation "2014- Habilitation électrique des personnels non électricien (session novembre) -INPT" a été acceptée.
2146	238	2014-11-24 11:31:16	76	371	Vous avez indiqué avoir assisté à la formation "2014- Habilitation électrique des personnels non électricien (session novembre) -INPT".
2147	239	2014-11-24 11:34:19	28	371	Inscription à la demande "2014- Habilitation électrique des personnels non électricien (session novembre) -INPT".
2148	239	2014-11-24 11:34:30	44	371	Votre inscription à la demande de formation "2014- Habilitation électrique des personnels non électricien (session novembre) -INPT" a été acceptée.
2149	239	2014-11-24 11:34:30	76	371	Vous avez indiqué avoir assisté à la formation "2014- Habilitation électrique des personnels non électricien (session novembre) -INPT".
2153	241	2014-11-24 11:44:01	28	371	Inscription à la demande "2014- Habilitation électrique des personnels non électricien (session novembre) -INPT".
2154	241	2014-11-24 11:44:20	44	371	Votre inscription à la demande de formation "2014- Habilitation électrique des personnels non électricien (session novembre) -INPT" a été acceptée.
2155	241	2014-11-24 11:44:20	76	371	Vous avez indiqué avoir assisté à la formation "2014- Habilitation électrique des personnels non électricien (session novembre) -INPT".
2157	240	2014-11-24 11:45:14	28	371	Inscription à la demande "2014- Habilitation électrique des personnels non électricien (session novembre) -INPT".
2158	240	2014-11-24 11:45:24	44	371	Votre inscription à la demande de formation "2014- Habilitation électrique des personnels non électricien (session novembre) -INPT" a été acceptée.
2159	240	2014-11-24 11:45:24	76	371	Vous avez indiqué avoir assisté à la formation "2014- Habilitation électrique des personnels non électricien (session novembre) -INPT".
2160	242	2014-11-24 11:48:29	28	371	Inscription à la demande "2014- Habilitation électrique des personnels non électricien (session novembre) -INPT".
2161	242	2014-11-24 11:49:11	44	371	Votre inscription à la demande de formation "2014- Habilitation électrique des personnels non électricien (session novembre) -INPT" a été acceptée.
2162	242	2014-11-24 11:49:11	76	371	Vous avez indiqué avoir assisté à la formation "2014- Habilitation électrique des personnels non électricien (session novembre) -INPT".
2163	243	2014-11-24 11:52:34	28	371	Inscription à la demande "2014- Habilitation électrique des personnels non électricien (session novembre) -INPT".
2164	243	2014-11-24 11:52:50	44	371	Votre inscription à la demande de formation "2014- Habilitation électrique des personnels non électricien (session novembre) -INPT" a été acceptée.
2535	4	2015-10-12 16:54:03	28	413	Inscription à la demande "2009- des bonnes pratiques pour les ASR".
2165	243	2014-11-24 11:52:50	76	371	Vous avez indiqué avoir assisté à la formation "2014- Habilitation électrique des personnels non électricien (session novembre) -INPT".
2166	244	2014-11-24 11:55:25	28	371	Inscription à la demande "2014- Habilitation électrique des personnels non électricien (session novembre) -INPT".
2167	244	2014-11-24 11:55:37	44	371	Votre inscription à la demande de formation "2014- Habilitation électrique des personnels non électricien (session novembre) -INPT" a été acceptée.
2168	244	2014-11-24 11:55:37	76	371	Vous avez indiqué avoir assisté à la formation "2014- Habilitation électrique des personnels non électricien (session novembre) -INPT".
2169	245	2014-11-24 11:57:45	28	371	Inscription à la demande "2014- Habilitation électrique des personnels non électricien (session novembre) -INPT".
2170	245	2014-11-24 11:57:57	44	371	Votre inscription à la demande de formation "2014- Habilitation électrique des personnels non électricien (session novembre) -INPT" a été acceptée.
2171	245	2014-11-24 11:57:57	76	371	Vous avez indiqué avoir assisté à la formation "2014- Habilitation électrique des personnels non électricien (session novembre) -INPT".
2172	246	2014-11-24 12:00:19	28	371	Inscription à la demande "2014- Habilitation électrique des personnels non électricien (session novembre) -INPT".
2173	246	2014-11-24 12:00:28	44	371	Votre inscription à la demande de formation "2014- Habilitation électrique des personnels non électricien (session novembre) -INPT" a été acceptée.
2174	246	2014-11-24 12:00:28	76	371	Vous avez indiqué avoir assisté à la formation "2014- Habilitation électrique des personnels non électricien (session novembre) -INPT".
2175	235	2014-11-24 12:01:23	28	371	Inscription à la demande "2014- Habilitation électrique des personnels non électricien (session novembre) -INPT".
2176	235	2014-11-24 12:01:33	44	371	Votre inscription à la demande de formation "2014- Habilitation électrique des personnels non électricien (session novembre) -INPT" a été acceptée.
2177	235	2014-11-24 12:01:33	76	371	Vous avez indiqué avoir assisté à la formation "2014- Habilitation électrique des personnels non électricien (session novembre) -INPT".
2178	231	2014-11-24 12:02:27	28	371	Inscription à la demande "2014- Habilitation électrique des personnels non électricien (session novembre) -INPT".
2179	231	2014-11-24 12:02:59	44	371	Votre inscription à la demande de formation "2014- Habilitation électrique des personnels non électricien (session novembre) -INPT" a été acceptée.
2180	231	2014-11-24 12:02:59	76	371	Vous avez indiqué avoir assisté à la formation "2014- Habilitation électrique des personnels non électricien (session novembre) -INPT".
2181	247	2014-11-24 12:05:03	28	371	Inscription à la demande "2014- Habilitation électrique des personnels non électricien (session novembre) -INPT".
2182	247	2014-11-24 12:05:16	44	371	Votre inscription à la demande de formation "2014- Habilitation électrique des personnels non électricien (session novembre) -INPT" a été acceptée.
2183	247	2014-11-24 12:05:16	76	371	Vous avez indiqué avoir assisté à la formation "2014- Habilitation électrique des personnels non électricien (session novembre) -INPT".
2184	248	2014-12-02 16:34:36	28	371	Inscription à la demande "2014- Habilitation électrique des personnels non électricien (session novembre) -INPT".
2185	248	2014-12-02 16:34:49	44	371	Votre inscription à la demande de formation "2014- Habilitation électrique des personnels non électricien (session novembre) -INPT" a été acceptée.
2186	248	2014-12-02 16:34:49	76	371	Vous avez indiqué avoir assisté à la formation "2014- Habilitation électrique des personnels non électricien (session novembre) -INPT".
2187	249	2014-12-02 16:41:24	28	346	Inscription à la demande "2014- Habilitation électrique des personnels non électricien (session juillet) -INPT".
2188	249	2014-12-02 16:41:40	44	346	Votre inscription à la demande de formation "2014- Habilitation électrique des personnels non électricien (session juillet) -INPT" a été acceptée.
2189	249	2014-12-02 16:41:40	76	346	Vous avez indiqué avoir assisté à la formation "2014- Habilitation électrique des personnels non électricien (session juillet) -INPT".
2190	35	2014-12-02 17:28:19	28	376	Inscription à la demande "Journée des assistants de prévention".
2191	35	2014-12-03 08:12:19	44	376	Votre inscription à la demande de formation "2014- Journée des assistants de prévention - CNRS" a été acceptée.
2192	250	2014-12-03 13:35:33	26	\N	Création de votre compte.
2193	2	2014-12-18 15:58:38	28	377	Inscription à la demande "2014- Réunion des correspondants formation du CNRS (DR14ç - CNRS".
2194	2	2014-12-18 16:00:52	28	378	Inscription à la demande "2014- Rencontre nationales du réseau des électroniciens du CNRS".
2195	2	2014-12-18 16:01:38	44	377	Votre inscription à la demande de formation "2014- Réunion des correspondants formation du CNRS (DR14) - CNRS" a été acceptée.
2196	2	2014-12-18 16:01:38	76	377	Vous avez indiqué avoir assisté à la formation "2014- Réunion des correspondants formation du CNRS (DR14) - CNRS".
2197	37	2014-12-18 16:03:23	28	378	Inscription à la demande "2014- Rencontre nationales du réseau des électroniciens du CNRS".
2198	44	2014-12-18 16:03:38	28	378	Inscription à la demande "2014- Rencontre nationales du réseau des électroniciens du CNRS".
2199	43	2014-12-18 16:04:22	28	378	Inscription à la demande "2014- Rencontre nationales du réseau des électroniciens du CNRS".
2200	37	2014-12-18 16:04:46	44	378	Votre inscription à la demande de formation "2014- Rencontre nationales du réseau des électroniciens du CNRS" a été acceptée.
2201	37	2014-12-18 16:04:46	76	378	Vous avez indiqué avoir assisté à la formation "2014- Rencontre nationales du réseau des électroniciens du CNRS".
2202	2	2014-12-18 16:05:00	44	378	Votre inscription à la demande de formation "2014- Rencontre nationales du réseau des électroniciens du CNRS" a été acceptée.
2203	2	2014-12-18 16:05:00	76	378	Vous avez indiqué avoir assisté à la formation "2014- Rencontre nationales du réseau des électroniciens du CNRS".
2204	44	2014-12-18 16:05:06	44	378	Votre inscription à la demande de formation "2014- Rencontre nationales du réseau des électroniciens du CNRS" a été acceptée.
2205	44	2014-12-18 16:05:06	76	378	Vous avez indiqué avoir assisté à la formation "2014- Rencontre nationales du réseau des électroniciens du CNRS".
2206	43	2014-12-18 16:05:19	44	378	Votre inscription à la demande de formation "2014- Rencontre nationales du réseau des électroniciens du CNRS" a été acceptée.
2207	43	2014-12-18 16:05:19	76	378	Vous avez indiqué avoir assisté à la formation "2014- Rencontre nationales du réseau des électroniciens du CNRS".
2208	4	2015-01-08 18:48:38	76	331	Vous avez indiqué avoir assisté à la formation "2014- Formation Management de projet - CNRS".
2209	81	2015-01-13 16:38:56	28	379	Inscription à la demande "Encadrer un doctorant à l’étranger".
2707	53	2016-02-10 11:38:12	28	456	Inscription à la demande "Les altmetrics".
2210	81	2015-01-13 21:09:27	44	379	Votre inscription à la demande de formation "2015- Encadrer un doctorant à l’étranger - CNRS" a été acceptée.
2211	112	2015-01-14 08:35:58	27	47	Ajout du besoin "Manipulation extincteur".
2212	53	2015-01-15 11:41:15	28	380	Inscription à la demande "Faire un screen cast".
2213	53	2015-01-15 16:02:23	44	380	Votre inscription à la demande de formation "2014- Faire un screen cast" a été acceptée.
2214	149	2015-01-16 10:07:23	28	338	Inscription à la demande "2014- Journée "Initiation au montage de projets de recherche" - INPT".
2216	149	2015-01-16 10:07:45	44	338	Votre inscription à la demande de formation "2014- Journée "Initiation au montage de projets de recherche" - INPT" a été acceptée.
2217	149	2015-01-16 10:07:45	76	338	Vous avez indiqué avoir assisté à la formation "2014- Journée "Initiation au montage de projets de recherche" - INPT".
2218	251	2015-01-19 12:21:25	26	\N	Création de votre compte.
2219	252	2015-02-05 12:33:49	26	\N	Création de votre compte.
2220	252	2015-03-02 17:20:15	27	22	Ajout du besoin "Normes de sécurité sur les manips".
2221	252	2015-03-02 17:23:52	27	19	Ajout du besoin "Inventor - Initiation".
2223	252	2015-03-02 17:27:08	27	5	Ajout du besoin "Habilitation Electrique pour non électricien - Formation initiale".
2224	252	2015-03-02 17:46:21	27	71	Ajout du besoin "Risques liés à la manipulation, l'installation des bouteilles de gaz".
2225	252	2015-03-02 17:51:10	27	72	Ajout du besoin "Gestes et postures pour le déplacement de charges".
2226	252	2015-03-24 14:37:40	27	47	Ajout du besoin "Manipulation extincteur".
2227	53	2015-04-02 10:23:45	28	381	Inscription à la demande "L'image numérique : création, traitement spécificités".
2228	53	2015-04-02 10:25:18	28	382	Inscription à la demande "Publication multimédia avec indesign".
2229	53	2015-04-02 11:03:47	44	382	Votre inscription à la demande de formation "2015- Publication multimédia avec indesign" a été acceptée.
2230	53	2015-04-02 11:03:54	44	381	Votre inscription à la demande de formation "2015- L'image numérique : création, traitement spécificités" a été acceptée.
2231	46	2015-04-28 09:15:48	43	67	Le besoin "L'hydraulique pour le dimensionnement de dissipateurs (plaque à eau)" a été pourvu.
2232	53	2015-04-28 14:03:04	92	356	Vous avez indiqué n'avoir pas assisté à la formation "2014- Le droit à l'image appliqué à la production scientifique - CNRS".
2233	53	2015-04-28 14:03:20	76	380	Vous avez indiqué avoir assisté à la formation "2014- Faire un screen cast".
2234	53	2015-04-28 14:03:40	76	381	Vous avez indiqué avoir assisté à la formation "2015- L'image numérique : création, traitement spécificités".
2235	53	2015-04-28 14:03:52	76	382	Vous avez indiqué avoir assisté à la formation "2015- Publication multimédia avec indesign".
2236	53	2015-04-28 14:07:39	28	383	Inscription à la demande "Powerpoint : concevoir une présentation réussie et efficace".
2237	53	2015-04-28 14:09:29	28	384	Inscription à la demande "Infographie : identité visuelle et chartre graphique".
2238	53	2015-04-28 14:10:35	28	385	Inscription à la demande "Photoshop et Indesign : concevoir des posters".
2239	53	2015-04-28 14:13:06	28	386	Inscription à la demande "Powerpoint :dynamiser ses présentations".
2240	53	2015-04-28 15:36:03	44	383	Votre inscription à la demande de formation "Powerpoint : concevoir une présentation réussie et efficace" a été acceptée.
2244	46	2015-05-05 13:46:22	28	387	Inscription à la demande "Conception d'une carte électronique dite "high speed"".
2245	46	2015-05-05 13:54:52	44	387	Votre inscription à la demande de formation "2015- Conception d'une carte électronique dite "high speed"" a été acceptée.
2246	44	2015-05-12 17:24:57	28	388	Inscription à la demande "Altium Spice".
2247	44	2015-05-12 17:26:20	28	389	Inscription à la demande "Alitent Intégrité de signal".
2250	67	2015-05-20 13:58:18	28	390	Inscription à la demande "Journées de développement logiciel (Jdev)".
2251	67	2015-05-20 13:59:57	44	390	Votre inscription à la demande de formation "2015- Journées de développement logiciel (Jdev)" a été acceptée.
2252	97	2015-05-22 08:10:48	28	391	Inscription à la demande "2015- Gestion des Incidents SSI - CNRS".
2253	144	2015-05-22 08:11:07	28	391	Inscription à la demande "2015- Gestion des Incidents SSI - CNRS".
2254	52	2015-05-22 08:11:32	28	391	Inscription à la demande "2015- Gestion des Incidents SSI - CNRS".
2258	51	2015-05-22 08:14:18	28	392	Inscription à la demande "2015- Esprit Module Fraisage 3 D".
2259	49	2015-05-22 08:15:33	28	392	Inscription à la demande "2015- Esprit Module Fraisage 3 D".
2260	71	2015-05-22 08:15:52	28	392	Inscription à la demande "2015- Esprit Module Fraisage 3 D".
2264	252	2015-05-22 08:19:20	28	393	Inscription à la demande "2015- ANF VIDE POUR UTILISATEUR - CNRS".
2265	252	2015-05-22 08:19:53	44	393	Votre inscription à la demande de formation "2015- ANF VIDE POUR UTILISATEUR - CNRS" a été acceptée.
2266	52	2015-05-22 08:59:56	44	391	Votre inscription à la demande de formation "2015- Gestion des Incidents SSI - CNRS" a été acceptée.
2267	52	2015-05-22 08:59:56	76	391	Vous avez indiqué avoir assisté à la formation "2015- Gestion des Incidents SSI - CNRS".
2268	97	2015-05-22 09:00:02	44	391	Votre inscription à la demande de formation "2015- Gestion des Incidents SSI - CNRS" a été acceptée.
2708	53	2016-02-10 11:48:46	44	452	Votre inscription à la demande de formation "2016- PREZY" a été acceptée.
2269	97	2015-05-22 09:00:02	76	391	Vous avez indiqué avoir assisté à la formation "2015- Gestion des Incidents SSI - CNRS".
2270	144	2015-05-22 09:00:07	44	391	Votre inscription à la demande de formation "2015- Gestion des Incidents SSI - CNRS" a été acceptée.
2271	144	2015-05-22 09:00:07	76	391	Vous avez indiqué avoir assisté à la formation "2015- Gestion des Incidents SSI - CNRS".
2272	253	2015-06-15 11:09:37	26	\N	Création de votre compte.
2273	54	2015-06-22 09:18:11	28	394	Inscription à la demande "2015- Initiation à Matlab".
2274	11	2015-06-22 09:18:54	28	394	Inscription à la demande "2015- Initiation à Matlab".
2275	46	2015-06-22 09:19:13	28	394	Inscription à la demande "2015- Initiation à Matlab".
2276	170	2015-06-22 09:19:32	28	394	Inscription à la demande "2015- Initiation à Matlab".
2281	253	2015-06-22 09:20:53	28	394	Inscription à la demande "2015- Initiation à Matlab".
2283	254	2015-06-22 09:26:56	28	394	Inscription à la demande "2015- Initiation à Matlab".
2285	35	2015-06-22 11:34:44	76	376	Vous avez indiqué avoir assisté à la formation "2014- Journée des assistants de prévention - CNRS".
2286	40	2015-06-22 14:24:20	76	356	Vous avez indiqué avoir assisté à la formation "2014- Le droit à l'image appliqué à la production scientifique - CNRS".
2287	255	2015-06-23 13:28:58	26	\N	Création de votre compte.
2288	35	2015-06-25 14:11:00	28	395	Inscription à la demande "Formation continue d'assistant de prévention sur les thème des Registres de sécurité".
2289	35	2015-06-25 17:15:10	44	395	Votre inscription à la demande de formation "2015- Formation continue d'assistant de prévention sur les thème des Registres de sécurité" a été acceptée.
2290	106	2015-06-26 08:21:22	28	395	Inscription à la demande "2015- Formation continue d'assistant de prévention sur les thème des Registres de sécurité".
2291	106	2015-06-26 08:21:34	44	395	Votre inscription à la demande de formation "2015- Formation continue d'assistant de prévention sur les thème des Registres de sécurité" a été acceptée.
2292	35	2015-06-30 11:56:41	28	396	Inscription à la demande "PREVENTION DU RISQUE LASER – FORMATION DES RSL".
2293	35	2015-06-30 13:33:14	44	396	Votre inscription à la demande de formation "2015- PREVENTION DU RISQUE LASER – FORMATION DES RSL - CNRS" a été acceptée.
2294	252	2015-07-07 15:32:34	76	393	Vous avez indiqué avoir assisté à la formation "2015- ANF VIDE POUR UTILISATEUR - CNRS".
2295	54	2015-07-16 13:15:38	44	394	Votre inscription à la demande de formation "2015- Initiation à Matlab" a été acceptée.
2296	54	2015-07-16 13:15:38	76	394	Vous avez indiqué avoir assisté à la formation "2015- Initiation à Matlab".
2297	46	2015-07-16 13:15:42	44	394	Votre inscription à la demande de formation "2015- Initiation à Matlab" a été acceptée.
2298	46	2015-07-16 13:15:42	76	394	Vous avez indiqué avoir assisté à la formation "2015- Initiation à Matlab".
2299	11	2015-07-16 13:15:49	44	394	Votre inscription à la demande de formation "2015- Initiation à Matlab" a été acceptée.
2300	11	2015-07-16 13:15:49	76	394	Vous avez indiqué avoir assisté à la formation "2015- Initiation à Matlab".
2301	170	2015-07-16 13:15:54	44	394	Votre inscription à la demande de formation "2015- Initiation à Matlab" a été acceptée.
2302	170	2015-07-16 13:15:54	76	394	Vous avez indiqué avoir assisté à la formation "2015- Initiation à Matlab".
2303	253	2015-07-16 13:15:59	44	394	Votre inscription à la demande de formation "2015- Initiation à Matlab" a été acceptée.
2304	253	2015-07-16 13:15:59	76	394	Vous avez indiqué avoir assisté à la formation "2015- Initiation à Matlab".
2305	254	2015-07-16 13:16:04	44	394	Votre inscription à la demande de formation "2015- Initiation à Matlab" a été acceptée.
2306	254	2015-07-16 13:16:04	92	394	Vous avez indiqué n'avoir pas assisté à la formation "2015- Initiation à Matlab".
2307	67	2015-07-22 09:01:04	76	390	Vous avez indiqué avoir assisté à la formation "2015- Journées de développement logiciel (Jdev)".
2308	67	2015-07-22 09:01:11	76	242	Vous avez indiqué avoir assisté à la formation "2012- Recyclage Sauveteur Secours du Travail - INPT".
2309	67	2015-07-22 09:01:16	76	227	Vous avez indiqué avoir assisté à la formation "2011- Recyclage Sauveteur Secours du Travail - INPT".
2310	137	2015-07-22 09:03:26	76	238	Vous avez indiqué avoir assisté à la formation "2012- Atelier analyse micro-structurale des couches minces".
2311	86	2015-07-22 09:09:26	76	238	Vous avez indiqué avoir assisté à la formation "2012- Atelier analyse micro-structurale des couches minces".
2312	136	2015-07-22 09:10:28	76	234	Vous avez indiqué avoir assisté à la formation "2011- Anglais".
2313	39	2015-07-22 09:17:18	76	105	Vous avez indiqué avoir assisté à la formation "2011- Equipier d'évacuation - INPT".
2314	56	2015-07-22 09:29:22	76	28	Vous avez indiqué avoir assisté à la formation "2011- La fiscalité de l'UPS, session 2".
2315	56	2015-07-22 09:29:25	76	179	Vous avez indiqué avoir assisté à la formation "2011- La fiscalité de l'UPS, session 3".
2316	56	2015-07-22 09:29:28	76	169	Vous avez indiqué avoir assisté à la formation "2011- La mobilité internationale des scientifiques étrangers".
2317	56	2015-07-22 09:29:31	76	180	Vous avez indiqué avoir assisté à la formation "2011- Information et complément de formation des correspondants achats".
2318	51	2015-07-22 09:46:31	76	31	Vous avez indiqué avoir assisté à la formation "2011- Inventor - Initiation avancée et perfectionnement".
2319	51	2015-07-22 09:46:35	76	146	Vous avez indiqué avoir assisté à la formation "2012- CFAO ESPRIT Fraisage 2 axes 1/2".
2320	145	2015-07-22 10:00:50	76	243	Vous avez indiqué avoir assisté à la formation "2012- Technologie des cellules photovoltaïques organiques et hybrides".
2321	35	2015-07-24 09:52:01	28	397	Inscription à la demande "Formation recyclage SST".
2322	35	2015-07-24 09:52:49	76	395	Vous avez indiqué avoir assisté à la formation "2015- Formation continue d'assistant de prévention sur les thème des Registres de sécurité".
2324	136	2015-08-26 10:48:12	28	398	Inscription à la demande "2015- Espagnol - CNRS".
2326	52	2015-08-26 13:30:44	27	13	Ajout du besoin "Automatiser l’administration avec Powershell 2.0".
2327	52	2015-08-26 13:31:16	27	15	Ajout du besoin "Espagnol - Initiation".
2328	48	2015-08-26 13:53:28	27	73	Ajout du besoin "Habilitation électrique BE Essai HE essai (BT et HT)".
2329	48	2015-08-26 13:54:57	43	73	Le besoin "Habilitation électrique BE Essai HE essai (BT et HT)" a été pourvu.
2330	53	2015-08-28 15:47:49	76	383	Vous avez indiqué avoir assisté à la formation "2015- Powerpoint : concevoir une présentation réussie et efficace".
2332	54	2015-08-31 11:04:00	27	74	Ajout du besoin "Logiciel CFAO ESPRIT - Module de base".
2333	112	2015-08-31 11:14:24	28	399	Inscription à la demande "2015- Formation initiale à l'habilitation électrique".
2334	256	2015-08-31 11:20:26	28	399	Inscription à la demande "2015- Formation initiale à l'habilitation électrique".
2335	112	2015-08-31 11:20:37	44	399	Votre inscription à la demande de formation "2015- Formation initiale à l'habilitation électrique" a été acceptée.
2336	112	2015-08-31 11:20:37	76	399	Vous avez indiqué avoir assisté à la formation "2015- Formation initiale à l'habilitation électrique".
2337	256	2015-08-31 11:20:43	44	399	Votre inscription à la demande de formation "2015- Formation initiale à l'habilitation électrique" a été acceptée.
2338	256	2015-08-31 11:20:43	76	399	Vous avez indiqué avoir assisté à la formation "2015- Formation initiale à l'habilitation électrique".
2339	48	2015-08-31 11:21:54	28	399	Inscription à la demande "2015- Formation initiale à l'habilitation électrique".
2340	48	2015-08-31 11:22:16	44	399	Votre inscription à la demande de formation "2015- Formation initiale à l'habilitation électrique" a été acceptée.
2341	48	2015-08-31 11:22:16	76	399	Vous avez indiqué avoir assisté à la formation "2015- Formation initiale à l'habilitation électrique".
2342	257	2015-08-31 11:28:05	28	399	Inscription à la demande "2015- Formation initiale à l'habilitation électrique".
2343	257	2015-08-31 11:28:24	44	399	Votre inscription à la demande de formation "2015- Formation initiale à l'habilitation électrique" a été acceptée.
2344	257	2015-08-31 11:28:24	76	399	Vous avez indiqué avoir assisté à la formation "2015- Formation initiale à l'habilitation électrique".
2345	87	2015-08-31 11:30:16	28	399	Inscription à la demande "2015- Formation initiale à l'habilitation électrique".
2346	258	2015-08-31 11:32:49	28	399	Inscription à la demande "2015- Formation initiale à l'habilitation électrique".
2347	258	2015-08-31 11:33:13	44	399	Votre inscription à la demande de formation "2015- Formation initiale à l'habilitation électrique" a été acceptée.
2348	258	2015-08-31 11:33:13	76	399	Vous avez indiqué avoir assisté à la formation "2015- Formation initiale à l'habilitation électrique".
2349	87	2015-08-31 11:33:20	44	399	Votre inscription à la demande de formation "2015- Formation initiale à l'habilitation électrique" a été acceptée.
2350	87	2015-08-31 11:33:20	76	399	Vous avez indiqué avoir assisté à la formation "2015- Formation initiale à l'habilitation électrique".
2351	259	2015-08-31 11:38:19	28	399	Inscription à la demande "2015- Formation initiale à l'habilitation électrique".
2352	259	2015-08-31 11:38:31	44	399	Votre inscription à la demande de formation "2015- Formation initiale à l'habilitation électrique" a été acceptée.
2353	259	2015-08-31 11:38:31	76	399	Vous avez indiqué avoir assisté à la formation "2015- Formation initiale à l'habilitation électrique".
2354	260	2015-08-31 11:42:08	28	399	Inscription à la demande "2015- Formation initiale à l'habilitation électrique".
2355	260	2015-08-31 11:42:21	44	399	Votre inscription à la demande de formation "2015- Formation initiale à l'habilitation électrique" a été acceptée.
2356	260	2015-08-31 11:42:21	76	399	Vous avez indiqué avoir assisté à la formation "2015- Formation initiale à l'habilitation électrique".
2357	261	2015-08-31 11:44:11	28	399	Inscription à la demande "2015- Formation initiale à l'habilitation électrique".
2358	261	2015-08-31 11:44:25	44	399	Votre inscription à la demande de formation "2015- Formation initiale à l'habilitation électrique" a été acceptée.
2359	261	2015-08-31 11:44:25	76	399	Vous avez indiqué avoir assisté à la formation "2015- Formation initiale à l'habilitation électrique".
2360	262	2015-08-31 11:46:16	28	399	Inscription à la demande "2015- Formation initiale à l'habilitation électrique".
2361	262	2015-08-31 11:46:36	44	399	Votre inscription à la demande de formation "2015- Formation initiale à l'habilitation électrique" a été acceptée.
2362	262	2015-08-31 11:46:36	76	399	Vous avez indiqué avoir assisté à la formation "2015- Formation initiale à l'habilitation électrique".
2363	263	2015-08-31 11:49:36	28	399	Inscription à la demande "2015- Formation initiale à l'habilitation électrique".
2364	263	2015-08-31 11:49:48	44	399	Votre inscription à la demande de formation "2015- Formation initiale à l'habilitation électrique" a été acceptée.
2365	263	2015-08-31 11:49:48	76	399	Vous avez indiqué avoir assisté à la formation "2015- Formation initiale à l'habilitation électrique".
2366	264	2015-08-31 11:51:35	28	399	Inscription à la demande "2015- Formation initiale à l'habilitation électrique".
2367	264	2015-08-31 11:51:53	44	399	Votre inscription à la demande de formation "2015- Formation initiale à l'habilitation électrique" a été acceptée.
2368	264	2015-08-31 11:51:53	76	399	Vous avez indiqué avoir assisté à la formation "2015- Formation initiale à l'habilitation électrique".
2369	46	2015-08-31 15:23:39	27	75	Ajout du besoin "Régles et méthodes d'assurance qualité".
2371	46	2015-08-31 15:31:33	27	77	Ajout du besoin "recyclage habilitation électrique".
2372	46	2015-08-31 15:41:57	76	387	Vous avez indiqué avoir assisté à la formation "2015- Conception d'une carte électronique dite "high speed"".
2373	46	2015-08-31 15:44:50	27	50	Ajout du besoin "thermique et hydraulique pour le dimensionnement de dissipateurs (refroidisseur à air forcé, plaque à eau)".
2374	46	2015-08-31 15:47:49	43	50	Le besoin "thermique et hydraulique pour le dimensionnement de dissipateurs (refroidisseur à air forcé, plaque à eau)" a été pourvu.
2375	265	2015-09-02 10:58:44	27	78	Ajout du besoin "Anglais - perfectionnement".
2376	265	2015-09-02 10:58:56	27	79	Ajout du besoin "Espagnol - Perfectionnement".
2377	54	2015-09-02 12:10:21	43	61	Le besoin "initiation à MATLAB" a été pourvu.
2378	11	2015-09-02 12:10:38	43	61	Le besoin "initiation à MATLAB" a été pourvu.
2379	39	2015-09-07 11:50:34	28	400	Inscription à la demande "Prezi : outil de création de supports de présentation orale".
2380	39	2015-09-07 13:02:22	44	400	Votre inscription à la demande de formation "2015- Prezi : outil de création de supports de présentation orale" a été acceptée.
2381	106	2015-09-08 09:32:48	76	395	Vous avez indiqué avoir assisté à la formation "2015- Formation continue d'assistant de prévention sur les thème des Registres de sécurité".
2382	106	2015-09-08 09:47:46	28	401	Inscription à la demande "Formation COMMUNICATION « Assistants de Prévention »".
2383	106	2015-09-08 13:01:37	28	376	Inscription à la demande "2014- Journée des assistants de prévention - CNRS".
2384	106	2015-09-08 13:01:51	44	376	Votre inscription à la demande de formation "2014- Journée des assistants de prévention - CNRS" a été acceptée.
2385	106	2015-09-08 13:01:51	76	376	Vous avez indiqué avoir assisté à la formation "2014- Journée des assistants de prévention - CNRS".
2386	106	2015-09-08 13:03:03	44	401	Votre inscription à la demande de formation "2014- Formation COMMUNICATION « Assistants de Prévention »" a été acceptée.
2387	106	2015-09-08 13:03:03	76	401	Vous avez indiqué avoir assisté à la formation "2014- Formation COMMUNICATION « Assistants de Prévention »".
2388	136	2015-09-08 13:50:11	44	398	Votre inscription à la demande de formation "2015- Espagnol - CNRS" a été acceptée.
2389	136	2015-09-08 13:50:11	76	398	Vous avez indiqué avoir assisté à la formation "2015- Espagnol - CNRS".
2390	81	2015-09-09 13:13:14	27	80	Ajout du besoin "Recruter un chercheur post- doctoral".
2393	42	2015-10-12 09:00:21	44	85	Votre inscription à la demande de formation "2011- Révision Générale des Politiques Publiques" a été acceptée.
2394	42	2015-10-12 09:00:21	76	85	Vous avez indiqué avoir assisté à la formation "2011- Révision Générale des Politiques Publiques".
2395	42	2015-10-12 09:14:43	44	126	Votre inscription à la demande de formation "2013- Du projet professionnel au recrutement : comment réussir ?" a été acceptée.
2396	42	2015-10-12 09:14:43	76	126	Vous avez indiqué avoir assisté à la formation "2013- Du projet professionnel au recrutement : comment réussir ?".
2397	81	2015-10-12 09:42:08	76	379	Vous avez indiqué avoir assisté à la formation "2015- Encadrer un doctorant à l’étranger - CNRS".
2398	78	2015-10-12 10:33:52	44	228	Votre inscription à la demande de formation "2011- Recyclage Sauveteur Secours du Travail - UPS" a été acceptée.
2399	78	2015-10-12 10:33:52	76	228	Vous avez indiqué avoir assisté à la formation "2011- Recyclage Sauveteur Secours du Travail - UPS".
2400	74	2015-10-12 10:33:58	44	230	Votre inscription à la demande de formation "2011- Recyclage Sauveteur Secours du Travail (2) - UPS" a été acceptée.
2401	74	2015-10-12 10:33:58	76	230	Vous avez indiqué avoir assisté à la formation "2011- Recyclage Sauveteur Secours du Travail (2) - UPS".
2402	59	2015-10-12 10:37:54	44	352	Votre inscription à la demande de formation "2014- Sauveteur secouriste du travail - CNRS" a été acceptée.
2403	59	2015-10-12 10:37:54	76	352	Vous avez indiqué avoir assisté à la formation "2014- Sauveteur secouriste du travail - CNRS".
2404	146	2015-10-12 10:47:05	44	244	Votre inscription à la demande de formation "2012- Sécurité Laser" a été acceptée.
2405	146	2015-10-12 10:47:05	76	244	Vous avez indiqué avoir assisté à la formation "2012- Sécurité Laser".
2406	66	2015-10-12 10:52:17	44	105	Votre inscription à la demande de formation "2011- Equipier d'évacuation - INPT" a été acceptée.
2407	66	2015-10-12 10:52:17	76	105	Vous avez indiqué avoir assisté à la formation "2011- Equipier d'évacuation - INPT".
2408	52	2015-10-12 10:56:57	28	403	Inscription à la demande "2015- Recyclage de l’Habilitation électrique du personnel non électricien - INPT".
2409	111	2015-10-12 10:57:12	28	403	Inscription à la demande "2015- Recyclage de l’Habilitation électrique du personnel non électricien - INPT".
2410	140	2015-10-12 10:57:38	28	403	Inscription à la demande "2015- Recyclage de l’Habilitation électrique du personnel non électricien - INPT".
2411	44	2015-10-12 10:57:53	28	403	Inscription à la demande "2015- Recyclage de l’Habilitation électrique du personnel non électricien - INPT".
2412	116	2015-10-12 10:58:08	28	403	Inscription à la demande "2015- Recyclage de l’Habilitation électrique du personnel non électricien - INPT".
2413	68	2015-10-12 10:58:23	28	403	Inscription à la demande "2015- Recyclage de l’Habilitation électrique du personnel non électricien - INPT".
2414	149	2015-10-12 10:58:39	28	403	Inscription à la demande "2015- Recyclage de l’Habilitation électrique du personnel non électricien - INPT".
2415	68	2015-10-12 10:58:59	44	403	Votre inscription à la demande de formation "2015- Recyclage de l’Habilitation électrique du personnel non électricien - INPT" a été acceptée.
2416	68	2015-10-12 10:58:59	76	403	Vous avez indiqué avoir assisté à la formation "2015- Recyclage de l’Habilitation électrique du personnel non électricien - INPT".
2417	44	2015-10-12 10:59:05	44	403	Votre inscription à la demande de formation "2015- Recyclage de l’Habilitation électrique du personnel non électricien - INPT" a été acceptée.
2418	44	2015-10-12 10:59:05	76	403	Vous avez indiqué avoir assisté à la formation "2015- Recyclage de l’Habilitation électrique du personnel non électricien - INPT".
2419	52	2015-10-12 10:59:12	44	403	Votre inscription à la demande de formation "2015- Recyclage de l’Habilitation électrique du personnel non électricien - INPT" a été acceptée.
2420	52	2015-10-12 10:59:12	76	403	Vous avez indiqué avoir assisté à la formation "2015- Recyclage de l’Habilitation électrique du personnel non électricien - INPT".
2468	114	2015-10-12 14:32:27	76	31	Vous avez indiqué avoir assisté à la formation "2011- Inventor - Initiation avancée et perfectionnement".
2421	149	2015-10-12 10:59:17	44	403	Votre inscription à la demande de formation "2015- Recyclage de l’Habilitation électrique du personnel non électricien - INPT" a été acceptée.
2422	149	2015-10-12 10:59:17	76	403	Vous avez indiqué avoir assisté à la formation "2015- Recyclage de l’Habilitation électrique du personnel non électricien - INPT".
2423	140	2015-10-12 10:59:23	44	403	Votre inscription à la demande de formation "2015- Recyclage de l’Habilitation électrique du personnel non électricien - INPT" a été acceptée.
2424	140	2015-10-12 10:59:23	76	403	Vous avez indiqué avoir assisté à la formation "2015- Recyclage de l’Habilitation électrique du personnel non électricien - INPT".
2425	116	2015-10-12 10:59:30	44	403	Votre inscription à la demande de formation "2015- Recyclage de l’Habilitation électrique du personnel non électricien - INPT" a été acceptée.
2426	116	2015-10-12 10:59:30	76	403	Vous avez indiqué avoir assisté à la formation "2015- Recyclage de l’Habilitation électrique du personnel non électricien - INPT".
2427	111	2015-10-12 10:59:37	44	403	Votre inscription à la demande de formation "2015- Recyclage de l’Habilitation électrique du personnel non électricien - INPT" a été acceptée.
2428	111	2015-10-12 10:59:37	76	403	Vous avez indiqué avoir assisté à la formation "2015- Recyclage de l’Habilitation électrique du personnel non électricien - INPT".
2429	266	2015-10-12 11:02:36	28	403	Inscription à la demande "2015- Recyclage de l’Habilitation électrique du personnel non électricien - INPT".
2430	266	2015-10-12 11:02:50	44	403	Votre inscription à la demande de formation "2015- Recyclage de l’Habilitation électrique du personnel non électricien - INPT" a été acceptée.
2431	266	2015-10-12 11:02:50	76	403	Vous avez indiqué avoir assisté à la formation "2015- Recyclage de l’Habilitation électrique du personnel non électricien - INPT".
2432	267	2015-10-12 11:05:59	28	403	Inscription à la demande "2015- Recyclage de l’Habilitation électrique du personnel non électricien - INPT".
2433	267	2015-10-12 11:06:14	44	403	Votre inscription à la demande de formation "2015- Recyclage de l’Habilitation électrique du personnel non électricien - INPT" a été acceptée.
2434	267	2015-10-12 11:06:14	76	403	Vous avez indiqué avoir assisté à la formation "2015- Recyclage de l’Habilitation électrique du personnel non électricien - INPT".
2435	2	2015-10-12 11:17:54	28	404	Inscription à la demande "2015- 18èmes Rencontre régionale du réseau des électroniciens DR14".
2436	44	2015-10-12 11:18:12	28	404	Inscription à la demande "2015- 18èmes Rencontre régionale du réseau des électroniciens DR14".
2437	43	2015-10-12 11:18:25	28	404	Inscription à la demande "2015- 18èmes Rencontre régionale du réseau des électroniciens DR14".
2438	37	2015-10-12 11:18:41	28	404	Inscription à la demande "2015- 18èmes Rencontre régionale du réseau des électroniciens DR14".
2439	37	2015-10-12 11:18:57	44	404	Votre inscription à la demande de formation "2015- 18èmes Rencontre régionale du réseau des électroniciens DR14" a été acceptée.
2440	37	2015-10-12 11:18:57	76	404	Vous avez indiqué avoir assisté à la formation "2015- 18èmes Rencontre régionale du réseau des électroniciens DR14".
2441	43	2015-10-12 11:19:03	44	404	Votre inscription à la demande de formation "2015- 18èmes Rencontre régionale du réseau des électroniciens DR14" a été acceptée.
2442	43	2015-10-12 11:19:03	76	404	Vous avez indiqué avoir assisté à la formation "2015- 18èmes Rencontre régionale du réseau des électroniciens DR14".
2443	44	2015-10-12 11:19:08	44	404	Votre inscription à la demande de formation "2015- 18èmes Rencontre régionale du réseau des électroniciens DR14" a été acceptée.
2444	44	2015-10-12 11:19:08	76	404	Vous avez indiqué avoir assisté à la formation "2015- 18èmes Rencontre régionale du réseau des électroniciens DR14".
2445	2	2015-10-12 11:19:13	44	404	Votre inscription à la demande de formation "2015- 18èmes Rencontre régionale du réseau des électroniciens DR14" a été acceptée.
2446	2	2015-10-12 11:19:13	76	404	Vous avez indiqué avoir assisté à la formation "2015- 18èmes Rencontre régionale du réseau des électroniciens DR14".
2447	129	2015-10-12 11:29:21	44	122	Votre inscription à la demande de formation "2012- Journée Nano - CNRS" a été acceptée.
2448	129	2015-10-12 11:29:21	76	122	Vous avez indiqué avoir assisté à la formation "2012- Journée Nano - CNRS".
2449	124	2015-10-12 11:29:26	44	122	Votre inscription à la demande de formation "2012- Journée Nano - CNRS" a été acceptée.
2450	124	2015-10-12 11:29:26	76	122	Vous avez indiqué avoir assisté à la formation "2012- Journée Nano - CNRS".
2451	113	2015-10-12 11:33:30	44	111	Votre inscription à la demande de formation "2011- Journée thématique du vide" a été acceptée.
2452	113	2015-10-12 11:33:30	76	111	Vous avez indiqué avoir assisté à la formation "2011- Journée thématique du vide".
2453	114	2015-10-12 11:33:38	44	111	Votre inscription à la demande de formation "2011- Journée thématique du vide" a été acceptée.
2454	114	2015-10-12 11:33:38	76	111	Vous avez indiqué avoir assisté à la formation "2011- Journée thématique du vide".
2455	144	2015-10-12 11:45:04	44	240	Votre inscription à la demande de formation "2012- Formation Netasq" a été acceptée.
2456	144	2015-10-12 11:45:04	76	240	Vous avez indiqué avoir assisté à la formation "2012- Formation Netasq".
2457	35	2015-10-12 14:11:55	44	397	Votre inscription à la demande de formation "2015- Formation recyclage SST - CNRS" a été acceptée.
2458	35	2015-10-12 14:11:55	76	397	Vous avez indiqué avoir assisté à la formation "2015- Formation recyclage SST - CNRS".
2459	71	2015-10-12 14:13:10	44	357	Votre inscription à la demande de formation "2014- La fabrication additive métallique - CNRS" a été acceptée.
2460	71	2015-10-12 14:13:10	76	357	Vous avez indiqué avoir assisté à la formation "2014- La fabrication additive métallique - CNRS".
2461	49	2015-10-12 14:31:09	44	392	Votre inscription à la demande de formation "2015- Esprit Module Fraisage 3 D" a été acceptée.
2462	49	2015-10-12 14:31:09	76	392	Vous avez indiqué avoir assisté à la formation "2015- Esprit Module Fraisage 3 D".
2463	51	2015-10-12 14:31:13	44	392	Votre inscription à la demande de formation "2015- Esprit Module Fraisage 3 D" a été acceptée.
2464	51	2015-10-12 14:31:13	76	392	Vous avez indiqué avoir assisté à la formation "2015- Esprit Module Fraisage 3 D".
2465	71	2015-10-12 14:31:18	44	392	Votre inscription à la demande de formation "2015- Esprit Module Fraisage 3 D" a été acceptée.
2466	71	2015-10-12 14:31:18	76	392	Vous avez indiqué avoir assisté à la formation "2015- Esprit Module Fraisage 3 D".
2467	114	2015-10-12 14:32:27	44	31	Votre inscription à la demande de formation "2011- Inventor - Initiation avancée et perfectionnement" a été acceptée.
2469	113	2015-10-12 14:32:33	44	31	Votre inscription à la demande de formation "2011- Inventor - Initiation avancée et perfectionnement" a été acceptée.
2470	113	2015-10-12 14:32:33	76	31	Vous avez indiqué avoir assisté à la formation "2011- Inventor - Initiation avancée et perfectionnement".
2471	44	2015-10-12 15:13:51	44	388	Votre inscription à la demande de formation "2015- Altium Spice" a été acceptée.
2472	44	2015-10-12 15:13:51	76	388	Vous avez indiqué avoir assisté à la formation "2015- Altium Spice".
2473	44	2015-10-12 15:13:54	44	389	Votre inscription à la demande de formation "2015- Altium Intégrité de signal" a été acceptée.
2474	44	2015-10-12 15:13:54	76	389	Vous avez indiqué avoir assisté à la formation "2015- Altium Intégrité de signal".
2475	53	2015-10-12 15:15:56	44	384	Votre inscription à la demande de formation "2015- Infographie : identité visuelle et chartre graphique" a été acceptée.
2476	53	2015-10-12 15:15:56	76	384	Vous avez indiqué avoir assisté à la formation "2015- Infographie : identité visuelle et chartre graphique".
2477	53	2015-10-12 15:18:16	44	385	Votre inscription à la demande de formation "2015- Photoshop et Indesign : concevoir des posters" a été acceptée.
2478	53	2015-10-12 15:18:16	92	385	Vous avez indiqué n'avoir pas assisté à la formation "2015- Photoshop et Indesign : concevoir des posters".
2479	53	2015-10-12 15:18:35	44	386	Votre inscription à la demande de formation "2015- Powerpoint :dynamiser ses présentations" a été acceptée.
2480	53	2015-10-12 15:18:35	76	386	Vous avez indiqué avoir assisté à la formation "2015- Powerpoint :dynamiser ses présentations".
2481	42	2015-10-12 15:19:55	44	320	Votre inscription à la demande de formation "2014- EXCEL INITIATION AUX MACRO COMMANDES -INPT" a été acceptée.
2482	42	2015-10-12 15:19:55	76	320	Vous avez indiqué avoir assisté à la formation "2014- EXCEL INITIATION AUX MACRO COMMANDES -INPT".
2483	42	2015-10-12 15:20:00	44	316	Votre inscription à la demande de formation "2014- Excel Consolidations et TCD -INPT" a été acceptée.
2484	42	2015-10-12 15:20:00	76	316	Vous avez indiqué avoir assisté à la formation "2014- Excel Consolidations et TCD -INPT".
2485	42	2015-10-12 15:20:20	44	319	Votre inscription à la demande de formation "2014- WORD 2010 Documents longs -INPT" a été acceptée.
2486	42	2015-10-12 15:20:20	76	319	Vous avez indiqué avoir assisté à la formation "2014- WORD 2010 Documents longs -INPT".
2487	71	2015-10-12 15:25:49	43	69	Le besoin "Logiciel "ESPRIT 3D"" a été pourvu.
2488	49	2015-10-12 15:26:03	43	69	Le besoin "Logiciel "ESPRIT 3D"" a été pourvu.
2489	51	2015-10-12 15:26:17	43	69	Le besoin "Logiciel "ESPRIT 3D"" a été pourvu.
2490	40	2015-10-12 15:30:24	27	81	Ajout du besoin "Initiation au logiciel JMAG".
2491	178	2015-10-12 15:30:37	27	81	Ajout du besoin "Initiation au logiciel JMAG".
2492	42	2015-10-12 15:40:35	44	317	Votre inscription à la demande de formation "2014- POWERPOINT INITIATION -INPT" a été acceptée.
2493	42	2015-10-12 15:40:35	76	317	Vous avez indiqué avoir assisté à la formation "2014- POWERPOINT INITIATION -INPT".
2494	42	2015-10-12 15:40:42	44	318	Votre inscription à la demande de formation "2014- POWERPOINT PERFECTIONNEMENT -INPT" a été acceptée.
2495	42	2015-10-12 15:40:42	76	318	Vous avez indiqué avoir assisté à la formation "2014- POWERPOINT PERFECTIONNEMENT -INPT".
2496	42	2015-10-12 15:54:41	27	9	Ajout du besoin "Rédaction du rapport d'activités".
2497	42	2015-10-12 15:54:52	27	8	Ajout du besoin "Préparation aux concours".
2498	113	2015-10-12 16:01:18	27	17	Ajout du besoin "Technologie du vide, ultra-vide et mesures".
2499	113	2015-10-12 16:02:27	27	82	Ajout du besoin "Techniques de collage / assemblage".
2500	54	2015-10-12 16:07:01	27	19	Ajout du besoin "Inventor - Initiation".
2501	268	2015-10-12 16:15:17	28	405	Inscription à la demande "2014- Réunion d'information sur les versions des logiciels et OS, et le chiffrement des postes".
2502	268	2015-10-12 16:16:59	27	83	Ajout du besoin "Windows 8".
2503	268	2015-10-12 16:17:15	27	84	Ajout du besoin "OS pour serveurs".
2504	268	2015-10-12 16:19:12	44	405	Votre inscription à la demande de formation "2014- Réunion d'information sur les versions des logiciels et OS, et le chiffrement des postes" a été acceptée.
2505	268	2015-10-12 16:19:12	76	405	Vous avez indiqué avoir assisté à la formation "2014- Réunion d'information sur les versions des logiciels et OS, et le chiffrement des postes".
2506	77	2015-10-12 16:24:49	27	27	Ajout du besoin "Labview".
2507	77	2015-10-12 16:25:53	28	406	Inscription à la demande "2013- Séminaire OSRAM sur l'éclairage LED".
2508	77	2015-10-12 16:26:10	44	406	Votre inscription à la demande de formation "2013- Séminaire OSRAM sur l'éclairage LED" a été acceptée.
2509	77	2015-10-12 16:26:10	76	406	Vous avez indiqué avoir assisté à la formation "2013- Séminaire OSRAM sur l'éclairage LED".
2510	97	2015-10-12 16:29:25	28	407	Inscription à la demande "2011- Représentant CNIL - UPS".
2511	97	2015-10-12 16:31:08	28	408	Inscription à la demande "2013- Tech Series OS X & iOS - Fonctionnement et techniques de déploiement".
2512	97	2015-10-12 16:32:13	28	409	Inscription à la demande "2014- Préparation et formation presidence de jury de concours - UPS".
2513	97	2015-10-12 16:32:47	28	410	Inscription à la demande "2014- Outils de récupération de données".
2514	97	2015-10-12 16:37:00	27	85	Ajout du besoin "Sécurité, détection d'intrusion".
2515	97	2015-10-12 16:37:22	27	86	Ajout du besoin "Windows 7 sécurisation postes clients".
2516	97	2015-10-12 16:37:31	27	87	Ajout du besoin "Évolution Mac OSX Server".
2517	97	2015-10-12 16:38:37	44	407	Votre inscription à la demande de formation "2011- Représentant CNIL - UPS" a été acceptée.
2518	97	2015-10-12 16:38:37	76	407	Vous avez indiqué avoir assisté à la formation "2011- Représentant CNIL - UPS".
2519	97	2015-10-12 16:39:01	44	408	Votre inscription à la demande de formation "2013- Tech Series OS X & iOS - Fonctionnement et techniques de déploiement" a été acceptée.
2520	97	2015-10-12 16:39:01	76	408	Vous avez indiqué avoir assisté à la formation "2013- Tech Series OS X & iOS - Fonctionnement et techniques de déploiement".
2521	97	2015-10-12 16:39:42	44	410	Votre inscription à la demande de formation "2014- Outils de récupération de données" a été acceptée.
2522	97	2015-10-12 16:39:42	76	410	Vous avez indiqué avoir assisté à la formation "2014- Outils de récupération de données".
2523	97	2015-10-12 16:39:56	44	409	Votre inscription à la demande de formation "2014- Préparation et formation présidence de jury de concours - UPS" a été acceptée.
2524	97	2015-10-12 16:39:56	76	409	Vous avez indiqué avoir assisté à la formation "2014- Préparation et formation présidence de jury de concours - UPS".
2525	43	2015-10-12 16:43:29	28	411	Inscription à la demande "2012- Formation Technique de brasage, retouche et réparation".
2526	43	2015-10-12 16:43:52	44	411	Votre inscription à la demande de formation "2012- Formation Technique de brasage, retouche et réparation" a été acceptée.
2527	43	2015-10-12 16:43:52	76	411	Vous avez indiqué avoir assisté à la formation "2012- Formation Technique de brasage, retouche et réparation".
2528	59	2015-10-12 16:46:34	28	412	Inscription à la demande "2012- Simulateur SPICE".
2529	59	2015-10-12 16:48:33	44	412	Votre inscription à la demande de formation "2012- Simulateur SPICE" a été acceptée.
2530	59	2015-10-12 16:48:33	76	412	Vous avez indiqué avoir assisté à la formation "2012- Simulateur SPICE".
2531	59	2015-10-12 16:49:14	28	389	Inscription à la demande "2015- Altium Intégrité de signal".
2532	59	2015-10-12 16:49:33	44	389	Votre inscription à la demande de formation "2015- Altium Intégrité de signal" a été acceptée.
2533	59	2015-10-12 16:49:33	76	389	Vous avez indiqué avoir assisté à la formation "2015- Altium Intégrité de signal".
2534	4	2015-10-12 16:52:00	27	57	Ajout du besoin "Le management pour les responsables d'équipes et les chefs d'équipes".
2536	4	2015-10-12 16:54:34	28	414	Inscription à la demande "2009- Formation A2IMP pour le réseau de métier CAPITOUL, Toulouse".
2537	4	2015-10-12 16:55:47	28	415	Inscription à la demande "2012- Une "démarche qualité" dans un service informatique, ça veut dire quoi en pratique ?".
2538	4	2015-10-12 16:57:00	44	415	Votre inscription à la demande de formation "2012- Une "démarche qualité" dans un service informatique, ça veut dire quoi en pratique ?" a été acceptée.
2539	4	2015-10-12 16:57:00	76	415	Vous avez indiqué avoir assisté à la formation "2012- Une "démarche qualité" dans un service informatique, ça veut dire quoi en pratique ?".
2540	4	2015-10-12 16:57:14	44	414	Votre inscription à la demande de formation "2009- Formation A2IMP pour le réseau de métier CAPITOUL, Toulouse" a été acceptée.
2541	4	2015-10-12 16:57:14	76	414	Vous avez indiqué avoir assisté à la formation "2009- Formation A2IMP pour le réseau de métier CAPITOUL, Toulouse".
2542	4	2015-10-12 16:57:22	44	413	Votre inscription à la demande de formation "2009- des bonnes pratiques pour les ASR" a été acceptée.
2543	4	2015-10-12 16:57:22	76	413	Vous avez indiqué avoir assisté à la formation "2009- des bonnes pratiques pour les ASR".
2544	114	2015-10-12 16:59:29	28	416	Inscription à la demande "2009- Formation de base, Principes fondamentaux Inventor 2009".
2545	114	2015-10-12 17:00:30	44	416	Votre inscription à la demande de formation "2010- Formation de base, Principes fondamentaux Inventor 2009" a été acceptée.
2546	114	2015-10-12 17:00:30	76	416	Vous avez indiqué avoir assisté à la formation "2010- Formation de base, Principes fondamentaux Inventor 2009".
2547	114	2015-10-12 17:01:07	28	417	Inscription à la demande "2011- CAE "Colloque Arcs Electriques"".
2548	114	2015-10-12 17:01:18	28	418	Inscription à la demande "2013- CAE "Colloque Arcs Electriques"".
2549	114	2015-10-12 17:01:37	44	418	Votre inscription à la demande de formation "2013- CAE "Colloque Arcs Electriques"" a été acceptée.
2550	114	2015-10-12 17:01:37	76	418	Vous avez indiqué avoir assisté à la formation "2013- CAE "Colloque Arcs Electriques"".
2551	114	2015-10-12 17:02:44	44	417	Votre inscription à la demande de formation "2011- CAE "Colloque Arcs Electriques"" a été acceptée.
2552	114	2015-10-12 17:02:44	76	417	Vous avez indiqué avoir assisté à la formation "2011- CAE "Colloque Arcs Electriques"".
2553	38	2015-10-12 17:06:56	28	125	Inscription à la demande "2011- SIFAC Référentiel".
2554	38	2015-10-12 17:07:52	28	419	Inscription à la demande "2011- SIFAC module Recettes".
2555	38	2015-10-12 17:08:13	28	420	Inscription à la demande "2015- SIFAC Immobilisations".
2556	38	2015-10-12 17:08:54	28	421	Inscription à la demande "2015- SIFAC module Dématérialisation".
2557	38	2015-10-12 17:09:12	44	421	Votre inscription à la demande de formation "2015- SIFAC module Dématérialisation" a été acceptée.
2558	38	2015-10-12 17:09:12	76	421	Vous avez indiqué avoir assisté à la formation "2015- SIFAC module Dématérialisation".
2559	38	2015-10-12 17:09:25	44	420	Votre inscription à la demande de formation "2015- SIFAC Immobilisations" a été acceptée.
2560	38	2015-10-12 17:09:25	76	420	Vous avez indiqué avoir assisté à la formation "2015- SIFAC Immobilisations".
2561	38	2015-10-12 17:09:41	44	419	Votre inscription à la demande de formation "2011- SIFAC module Recettes" a été acceptée.
2562	38	2015-10-12 17:09:41	76	419	Vous avez indiqué avoir assisté à la formation "2011- SIFAC module Recettes".
2563	38	2015-10-12 17:11:07	28	422	Inscription à la demande "2011- Formation plateforme réservation en ligne Carlson".
2564	38	2015-10-12 17:11:20	44	422	Votre inscription à la demande de formation "2011- Formation plateforme réservation en ligne Carlson" a été acceptée.
2565	38	2015-10-12 17:11:20	76	422	Vous avez indiqué avoir assisté à la formation "2011- Formation plateforme réservation en ligne Carlson".
2566	41	2015-10-12 17:15:00	28	423	Inscription à la demande "2014- Inventor".
2567	41	2015-10-12 17:17:11	28	424	Inscription à la demande "2014- Habilitation électrique des personnels non électricien (session février) -UPS".
2568	41	2015-10-12 17:17:45	44	424	Votre inscription à la demande de formation "2014- Habilitation électrique des personnels non électricien (session février) -UPS" a été acceptée.
2569	41	2015-10-12 17:17:45	76	424	Vous avez indiqué avoir assisté à la formation "2014- Habilitation électrique des personnels non électricien (session février) -UPS".
2570	41	2015-10-12 17:18:02	44	423	Votre inscription à la demande de formation "2014- Inventor" a été acceptée.
2571	41	2015-10-12 17:18:02	76	423	Vous avez indiqué avoir assisté à la formation "2014- Inventor".
2572	38	2015-10-12 17:18:37	44	125	Votre inscription à la demande de formation "2011- SIFAC Référentiel" a été acceptée.
2573	38	2015-10-12 17:18:37	76	125	Vous avez indiqué avoir assisté à la formation "2011- SIFAC Référentiel".
2574	51	2015-10-12 17:21:06	76	357	Vous avez indiqué avoir assisté à la formation "2014- La fabrication additive métallique - CNRS".
2575	46	2015-10-12 17:21:58	27	88	Ajout du besoin "Conception d' une carte électronique orientée haute fréquence".
2576	144	2015-10-12 17:25:03	27	89	Ajout du besoin "Formation Expert - Maitrise des différentes fonctionnalités des firewall Netasq".
2577	144	2015-10-12 17:25:39	27	90	Ajout du besoin "Administration OSX Server Snow Leopard".
2578	144	2015-10-12 17:28:16	28	410	Inscription à la demande "2014- Outils de récupération de données".
2579	144	2015-10-12 17:28:37	44	410	Votre inscription à la demande de formation "2014- Outils de récupération de données" a été acceptée.
2580	144	2015-10-12 17:28:37	76	410	Vous avez indiqué avoir assisté à la formation "2014- Outils de récupération de données".
2581	144	2015-10-12 17:29:39	28	409	Inscription à la demande "2014- Préparation et formation présidence de jury de concours - UPS".
2582	144	2015-10-12 17:29:53	44	409	Votre inscription à la demande de formation "2014- Préparation et formation présidence de jury de concours - UPS" a été acceptée.
2583	144	2015-10-12 17:29:53	76	409	Vous avez indiqué avoir assisté à la formation "2014- Préparation et formation présidence de jury de concours - UPS".
2584	144	2015-10-12 17:30:36	28	425	Inscription à la demande "2014- Commission Informatique et Liberté".
2585	144	2015-10-12 17:31:53	44	425	Votre inscription à la demande de formation "2014- Commission Informatique et Liberté" a été acceptée.
2586	144	2015-10-12 17:31:53	76	425	Vous avez indiqué avoir assisté à la formation "2014- Commission Informatique et Liberté".
2587	144	2015-10-12 17:32:20	28	426	Inscription à la demande "2013- Netvault logiciel de sauvegarde".
2588	144	2015-10-12 17:32:42	44	426	Votre inscription à la demande de formation "2013- Netvault logiciel de sauvegarde" a été acceptée.
2589	144	2015-10-12 17:32:42	76	426	Vous avez indiqué avoir assisté à la formation "2013- Netvault logiciel de sauvegarde".
2590	37	2015-10-12 17:35:05	28	427	Inscription à la demande "2009- Perfectionnement VHDL - CNRS".
2591	37	2015-10-12 17:36:25	44	427	Votre inscription à la demande de formation "2009- Perfectionnement VHDL - CNRS" a été acceptée.
2592	37	2015-10-12 17:36:25	76	427	Vous avez indiqué avoir assisté à la formation "2009- Perfectionnement VHDL - CNRS".
2593	56	2015-10-12 17:38:41	27	91	Ajout du besoin "Gestion financière des contrats européens".
2594	56	2015-10-12 17:39:16	28	420	Inscription à la demande "2015- SIFAC Immobilisations".
2595	56	2015-10-12 17:39:56	44	420	Votre inscription à la demande de formation "2015- SIFAC Immobilisations" a été acceptée.
2596	56	2015-10-12 17:39:56	76	420	Vous avez indiqué avoir assisté à la formation "2015- SIFAC Immobilisations".
2597	56	2015-10-12 17:41:52	28	428	Inscription à la demande "2013- SIFAC module Convention Métier/Outils".
2598	56	2015-10-12 17:42:13	44	428	Votre inscription à la demande de formation "2013- SIFAC module Convention Métier/Outils" a été acceptée.
2599	56	2015-10-12 17:42:13	76	428	Vous avez indiqué avoir assisté à la formation "2013- SIFAC module Convention Métier/Outils".
2600	56	2015-10-12 17:42:55	28	429	Inscription à la demande "2013- Membre de jury de concours".
2601	56	2015-10-12 17:43:19	44	429	Votre inscription à la demande de formation "2013- Membre de jury de concours" a été acceptée.
2602	56	2015-10-12 17:43:19	76	429	Vous avez indiqué avoir assisté à la formation "2013- Membre de jury de concours".
2603	56	2015-10-12 17:45:35	28	430	Inscription à la demande "2013- La fiscalité de l'UPS".
2604	56	2015-10-12 17:45:55	44	430	Votre inscription à la demande de formation "2013- La fiscalité de l'UPS" a été acceptée.
2605	56	2015-10-12 17:45:55	76	430	Vous avez indiqué avoir assisté à la formation "2013- La fiscalité de l'UPS".
2606	56	2015-10-12 17:46:59	28	431	Inscription à la demande "2012- Nouveautés législatives sur les droits des étrangers - UPS".
2607	56	2015-10-12 17:47:24	28	432	Inscription à la demande "2011- Contrats européens - UPS".
2608	56	2015-10-12 17:47:37	44	432	Votre inscription à la demande de formation "2011- Contrats européens - UPS" a été acceptée.
2609	56	2015-10-12 17:47:37	76	432	Vous avez indiqué avoir assisté à la formation "2011- Contrats européens - UPS".
2610	56	2015-10-12 17:49:43	44	431	Votre inscription à la demande de formation "2012- Nouveautés législatives sur les droits des étrangers - UPS" a été acceptée.
2611	56	2015-10-12 17:49:43	76	431	Vous avez indiqué avoir assisté à la formation "2012- Nouveautés législatives sur les droits des étrangers - UPS".
2612	44	2015-10-12 17:50:59	27	57	Ajout du besoin "Le management pour les responsables d'équipes et les chefs d'équipes".
2613	44	2015-10-12 17:52:41	28	433	Inscription à la demande "2012- 16èmes Rencontre régionale du réseau des électroniciens DR14".
2614	44	2015-10-12 17:54:13	28	434	Inscription à la demande "2012- Circuits PSoC 3/5 coeur C8051 ou Arm32".
2615	44	2015-10-12 17:55:21	28	435	Inscription à la demande "2014- Alimentations à découpage AC/DC et DC/DC".
2616	44	2015-10-12 17:56:12	44	435	Votre inscription à la demande de formation "2014- Alimentations à découpage AC/DC et DC/DC" a été acceptée.
2617	44	2015-10-12 17:56:12	76	435	Vous avez indiqué avoir assisté à la formation "2014- Alimentations à découpage AC/DC et DC/DC".
2618	44	2015-10-12 17:56:25	44	434	Votre inscription à la demande de formation "2012- Circuits PSoC 3/5 coeur C8051 ou Arm32" a été acceptée.
2619	44	2015-10-12 17:56:25	76	434	Vous avez indiqué avoir assisté à la formation "2012- Circuits PSoC 3/5 coeur C8051 ou Arm32".
2620	44	2015-10-12 17:56:40	44	433	Votre inscription à la demande de formation "2012- 16èmes Rencontre régionale du réseau des électroniciens DR14" a été acceptée.
2621	44	2015-10-12 17:56:40	76	433	Vous avez indiqué avoir assisté à la formation "2012- 16èmes Rencontre régionale du réseau des électroniciens DR14".
2622	35	2015-10-12 18:19:11	27	12	Ajout du besoin "Anglais".
2623	35	2015-10-12 18:19:21	27	27	Ajout du besoin "Labview".
2624	112	2015-10-12 20:58:20	28	436	Inscription à la demande "2015- Anglais courant".
2625	112	2015-10-12 20:58:48	44	436	Votre inscription à la demande de formation "2015- Anglais courant - INPT" a été acceptée.
2626	112	2015-10-12 20:58:48	76	436	Vous avez indiqué avoir assisté à la formation "2015- Anglais courant - INPT".
2627	116	2015-10-12 21:01:25	27	12	Ajout du besoin "Anglais".
2628	116	2015-10-12 21:01:36	27	3	Ajout du besoin "Sauveteur Secouriste du Travail - Recyclage".
2629	116	2015-10-12 21:03:00	28	423	Inscription à la demande "2014- Inventor".
2630	116	2015-10-12 21:03:18	44	423	Votre inscription à la demande de formation "2014- Inventor" a été acceptée.
2631	116	2015-10-12 21:03:18	76	423	Vous avez indiqué avoir assisté à la formation "2014- Inventor".
2632	11	2015-10-12 21:11:42	27	92	Ajout du besoin "Matlab avancé".
2633	11	2015-10-12 21:15:21	28	437	Inscription à la demande "2015- Tronçonneuse diamantée".
2634	11	2015-10-12 21:15:56	28	438	Inscription à la demande "2014- Utilisation d'une machine de bonding".
2635	11	2015-10-12 21:17:10	44	438	Votre inscription à la demande de formation "2014- Utilisation d'une machine de bonding" a été acceptée.
2636	11	2015-10-12 21:17:10	76	438	Vous avez indiqué avoir assisté à la formation "2014- Utilisation d'une machine de bonding".
2637	11	2015-10-12 21:17:30	44	437	Votre inscription à la demande de formation "2015- Utilisation d'une tronçonneuse diamantée" a été acceptée.
2638	11	2015-10-12 21:17:30	76	437	Vous avez indiqué avoir assisté à la formation "2015- Utilisation d'une tronçonneuse diamantée".
2639	54	2015-10-12 21:31:41	28	424	Inscription à la demande "2014- Habilitation électrique des personnels non électricien (session février) -UPS".
2640	54	2015-10-12 21:31:58	44	424	Votre inscription à la demande de formation "2014- Habilitation électrique des personnels non électricien (session février) -UPS" a été acceptée.
2641	54	2015-10-12 21:31:58	76	424	Vous avez indiqué avoir assisté à la formation "2014- Habilitation électrique des personnels non électricien (session février) -UPS".
2642	53	2015-10-12 21:51:27	27	93	Ajout du besoin "Formation aux métiers de la communication".
2643	57	2015-10-12 22:16:50	28	439	Inscription à la demande "2015- SIFAC Missions".
2644	57	2015-10-12 22:17:04	44	439	Votre inscription à la demande de formation "2015- SIFAC Missions" a été acceptée.
2645	57	2015-10-12 22:17:04	76	439	Vous avez indiqué avoir assisté à la formation "2015- SIFAC Missions".
2646	57	2015-10-12 22:18:47	27	94	Ajout du besoin "Règlementation dans la justification des contrats en fonction de leur nature".
2647	269	2015-10-12 22:22:53	27	95	Ajout du besoin "SIFAC Immobilisations".
2648	269	2015-10-12 22:24:47	28	440	Inscription à la demande "2014- SIFAC Missions".
2649	269	2015-10-12 22:25:10	44	440	Votre inscription à la demande de formation "2014- SIFAC Missions" a été acceptée.
2650	269	2015-10-12 22:25:10	76	440	Vous avez indiqué avoir assisté à la formation "2014- SIFAC Missions".
2651	138	2015-10-12 22:27:48	27	12	Ajout du besoin "Anglais".
2652	138	2015-10-12 22:30:12	27	96	Ajout du besoin "SIFAC - Module inventaire".
2653	82	2015-10-12 22:36:19	28	424	Inscription à la demande "2014- Habilitation électrique des personnels non électricien (session février) -UPS".
2654	82	2015-10-12 22:39:04	28	441	Inscription à la demande "2014- Recyclage Sauveteur Secours du Travail".
2655	82	2015-10-12 22:39:24	44	424	Votre inscription à la demande de formation "2014- Habilitation électrique des personnels non électricien (session février) -UPS" a été acceptée.
2656	82	2015-10-12 22:39:24	76	424	Vous avez indiqué avoir assisté à la formation "2014- Habilitation électrique des personnels non électricien (session février) -UPS".
2657	82	2015-10-12 22:39:40	44	441	Votre inscription à la demande de formation "2014- Recyclage Sauveteur Secours du Travail" a été acceptée.
2658	82	2015-10-12 22:39:40	76	441	Vous avez indiqué avoir assisté à la formation "2014- Recyclage Sauveteur Secours du Travail".
2659	82	2015-10-12 22:40:24	27	20	Ajout du besoin "Inventor - Perfectionnement".
2660	82	2015-10-12 22:41:25	27	97	Ajout du besoin "\tFormation sur les réacteurs à plasma".
2661	46	2015-10-13 08:31:39	27	98	Ajout du besoin "Initiation à COMSOL".
2662	270	2015-10-13 11:56:01	26	\N	Création de votre compte.
2663	147	2015-10-14 13:04:38	27	99	Ajout du besoin "conduite de réunion, animation de table ronde, animation d'atelier de créativité".
2664	147	2015-10-14 13:09:35	27	80	Ajout du besoin "Recruter un chercheur post- doctoral".
2665	35	2015-10-19 15:10:17	28	442	Inscription à la demande "Journée d’échanges entre les Assistants de Prévention".
2666	54	2015-11-02 11:44:25	28	443	Inscription à la demande "2015 - Photoshop CS6".
2667	54	2015-11-04 08:15:21	44	443	Votre inscription à la demande de formation "2015- Photoshop CS6" a été acceptée.
2668	106	2015-11-19 10:50:15	28	444	Inscription à la demande "2015- Formation COMMUNICATION « Assistants de Prévention »".
2669	106	2015-11-19 11:27:49	28	445	Inscription à la demande "2015- Journée des assistants de prévention - CNRS".
2670	106	2015-11-19 15:07:25	44	444	Votre inscription à la demande de formation "2015- Formation COMMUNICATION « Assistants de Prévention »" a été acceptée.
2671	106	2015-11-19 15:07:44	44	445	Votre inscription à la demande de formation "2015- Journée des assistants de prévention - CNRS" a été acceptée.
2672	51	2015-11-24 16:10:12	28	446	Inscription à la demande "2015- La fabrication numérique - CNRS".
2673	49	2015-11-24 16:10:25	28	446	Inscription à la demande "2015- La fabrication numérique - CNRS".
2674	71	2015-11-24 16:10:44	28	446	Inscription à la demande "2015- La fabrication numérique - CNRS".
2675	82	2015-11-24 16:11:01	28	446	Inscription à la demande "2015- La fabrication numérique - CNRS".
2676	49	2015-11-24 16:11:12	44	446	Votre inscription à la demande de formation "2015- La fabrication numérique - CNRS" a été acceptée.
2677	49	2015-11-24 16:11:12	92	446	Vous avez indiqué n'avoir pas assisté à la formation "2015- La fabrication numérique - CNRS".
2678	51	2015-11-24 16:11:18	44	446	Votre inscription à la demande de formation "2015- La fabrication numérique - CNRS" a été acceptée.
2679	51	2015-11-24 16:11:18	76	446	Vous avez indiqué avoir assisté à la formation "2015- La fabrication numérique - CNRS".
2680	71	2015-11-24 16:11:26	44	446	Votre inscription à la demande de formation "2015- La fabrication numérique - CNRS" a été acceptée.
2681	71	2015-11-24 16:11:26	76	446	Vous avez indiqué avoir assisté à la formation "2015- La fabrication numérique - CNRS".
2682	82	2015-11-24 16:11:31	44	446	Votre inscription à la demande de formation "2015- La fabrication numérique - CNRS" a été acceptée.
2683	82	2015-11-24 16:11:31	76	446	Vous avez indiqué avoir assisté à la formation "2015- La fabrication numérique - CNRS".
2684	81	2015-11-27 15:07:51	27	100	Ajout du besoin "Conduire un entretien de résolution de problèmes".
2685	271	2015-12-01 10:01:15	26	\N	Création de votre compte.
2686	252	2016-01-06 14:04:21	27	101	Ajout du besoin "Détection de Fuites".
2687	252	2016-01-06 14:05:16	43	101	Le besoin "Détection de Fuites" a été pourvu.
2689	252	2016-01-06 14:11:56	28	447	Inscription à la demande "Détection de fuites pour le vide".
2690	252	2016-01-06 14:26:13	44	447	Votre inscription à la demande de formation "2015- Détection de fuites pour le vide - CNRS" a été acceptée.
2691	252	2016-01-06 15:04:55	76	447	Vous avez indiqué avoir assisté à la formation "2015- Détection de fuites pour le vide - CNRS".
2692	272	2016-01-18 15:18:11	26	\N	Création de votre compte.
2693	272	2016-01-18 15:25:20	27	102	Ajout du besoin "Formation spécifique destinée aux personnes réalisant des procédures expérimentales sur modèle rongeur (ancien niveau 2)".
2694	54	2016-01-21 17:29:48	28	448	Inscription à la demande "Préparation pour le dossier de RAEP".
2695	54	2016-01-22 08:37:28	44	448	Votre inscription à la demande de formation "2016- Préparation pour le dossier de RAEP - UPS" a été acceptée.
2696	268	2016-01-22 10:19:39	28	449	Inscription à la demande "Protection de données et de services par le chiffrement".
2697	268	2016-01-22 11:00:03	44	449	Votre inscription à la demande de formation "2016- Protection de données et de services par le chiffrement - CNRS" a été acceptée.
2698	81	2016-02-10 08:31:55	28	450	Inscription à la demande "2016- Conduire un entretien de résolution de problèmes - CNRS".
2700	273	2016-02-10 08:44:24	28	451	Inscription à la demande "2016- Expérimentation animale - CNRS".
2701	81	2016-02-10 08:44:59	44	450	Votre inscription à la demande de formation "2016- Conduire un entretien de résolution de problèmes - CNRS" a été acceptée.
2702	81	2016-02-10 08:44:59	76	450	Vous avez indiqué avoir assisté à la formation "2016- Conduire un entretien de résolution de problèmes - CNRS".
2703	53	2016-02-10 11:30:39	28	452	Inscription à la demande "PREZY".
2704	53	2016-02-10 11:33:08	28	453	Inscription à la demande "Prendre en main l'environnement ADOBE".
2705	53	2016-02-10 11:34:19	28	454	Inscription à la demande "concevoir des posters photoshop et illustrator".
2706	53	2016-02-10 11:35:49	28	455	Inscription à la demande "twitter".
2709	53	2016-02-10 11:49:01	44	453	Votre inscription à la demande de formation "2016- Prendre en main l'environnement ADOBE" a été acceptée.
2710	53	2016-02-10 11:49:46	44	454	Votre inscription à la demande de formation "2016- Concevoir des posters photoshop et illustrator" a été acceptée.
2711	53	2016-02-10 11:50:00	44	455	Votre inscription à la demande de formation "2016- Twitter" a été acceptée.
2712	53	2016-02-10 11:50:14	44	456	Votre inscription à la demande de formation "2016- Les altmetrics" a été acceptée.
2713	252	2016-02-10 14:20:21	28	457	Inscription à la demande "Maintenance des installations électriques".
2714	252	2016-02-10 14:33:56	44	457	Votre inscription à la demande de formation "2016- Maintenance des installations électriques" a été acceptée.
2715	252	2016-02-10 15:00:29	76	457	Vous avez indiqué avoir assisté à la formation "2016- Maintenance des installations électriques".
2716	57	2016-02-15 14:18:48	28	458	Inscription à la demande "Formation GBCP".
2717	57	2016-02-15 14:21:03	28	459	Inscription à la demande "CONVENTIONS".
2718	57	2016-02-15 14:22:07	44	458	Votre inscription à la demande de formation "2016- Formation GBCP" a été acceptée.
2719	57	2016-02-15 14:22:28	44	459	Votre inscription à la demande de formation "2016- CONVENTIONS" a été acceptée.
2720	57	2016-02-15 14:22:56	76	458	Vous avez indiqué avoir assisté à la formation "2016- Formation GBCP".
2721	97	2016-03-01 09:22:42	28	460	Inscription à la demande "SPIP - Administrer et personnaliser un site web Labo -CNRS".
2722	97	2016-03-01 11:02:23	44	460	Votre inscription à la demande de formation "2016- SPIP - Administrer et personnaliser un site web Labo -CNRS" a été acceptée.
2723	116	2016-03-01 11:38:45	28	461	Inscription à la demande "2016- Formation initiale Assistant prévention - CNRS".
2724	116	2016-03-01 11:38:56	44	461	Votre inscription à la demande de formation "2016- Formation initiale Assistant prévention - CNRS" a été acceptée.
2725	54	2016-03-01 11:41:46	28	462	Inscription à la demande "2016- Recyclage SST- UPS".
2726	82	2016-03-01 11:42:01	28	462	Inscription à la demande "2016- Recyclage SST- UPS".
2727	54	2016-03-01 11:42:13	44	462	Votre inscription à la demande de formation "2016- Recyclage SST- UPS" a été acceptée.
2728	82	2016-03-01 11:42:17	44	462	Votre inscription à la demande de formation "2016- Recyclage SST- UPS" a été acceptée.
2729	274	2016-03-08 13:41:34	26	\N	Création de votre compte.
2730	274	2016-03-08 14:13:32	27	103	Ajout du besoin "Formation Windev Winweb".
2731	116	2016-03-23 14:39:37	28	463	Inscription à la demande "FORMATION INITIALE D’ASSISTANT DE PREVENTION".
2732	116	2016-03-23 14:53:43	44	463	Votre inscription à la demande de formation "2016- FORMATION INITIALE D’ASSISTANT DE PREVENTION" a été acceptée.
2733	157	2016-03-25 10:04:53	27	54	Ajout du besoin "Formation Microscopie à Force Atomique".
2734	157	2016-03-25 10:12:41	28	464	Inscription à la demande "Encadrement de thèse".
2735	157	2016-03-25 13:29:05	44	464	Votre inscription à la demande de formation "2016- Encadrement de thèse" a été acceptée.
2736	44	2016-04-05 15:41:36	27	104	Ajout du besoin "Programmation d'Interface Homme-Machine en C#".
2737	43	2016-04-05 15:54:27	27	105	Ajout du besoin "Programmation Interface Homme Machine en C#".
2738	43	2016-04-06 10:16:20	28	465	Inscription à la demande "Language C++".
2739	43	2016-04-07 14:00:48	44	465	Votre inscription à la demande de formation "2016- Language C++ - CNRS" a été acceptée.
2740	95	2016-04-12 11:08:41	28	466	Inscription à la demande "Se préparer à l'épreuve orale d'un concours ou examen professionnel - UPS".
2741	53	2016-04-12 16:16:44	76	453	Vous avez indiqué avoir assisté à la formation "2016- Prendre en main l'environnement ADOBE".
2742	53	2016-04-12 16:21:03	92	456	Vous avez indiqué n'avoir pas assisté à la formation "2016- Les altmetrics".
2743	53	2016-04-12 16:23:53	28	467	Inscription à la demande "Photoshop : maitriser les bases".
2744	53	2016-04-12 16:25:47	28	468	Inscription à la demande "concevoir et deployer un plan de communication".
2745	53	2016-04-12 16:29:01	28	469	Inscription à la demande "Indesign : première approche".
2746	95	2016-04-13 08:11:53	44	466	Votre inscription à la demande de formation "2016- Se préparer à l'épreuve orale d'un concours ou examen professionnel - UPS" a été acceptée.
2747	53	2016-04-13 08:12:20	44	467	Votre inscription à la demande de formation "2016- Photoshop : maitriser les bases - CNRS" a été acceptée.
2748	53	2016-04-13 08:12:37	44	468	Votre inscription à la demande de formation "2016- concevoir et déployer un plan de communication" a été acceptée.
2749	53	2016-04-13 08:12:52	44	469	Votre inscription à la demande de formation "2016- Indesign : première approche" a été acceptée.
2750	106	2016-04-28 09:47:49	76	444	Vous avez indiqué avoir assisté à la formation "2015- Formation COMMUNICATION « Assistants de Prévention »".
2751	106	2016-04-28 09:48:03	76	445	Vous avez indiqué avoir assisté à la formation "2015- Journée des assistants de prévention - CNRS".
2755	35	2016-05-11 08:30:49	28	470	Inscription à la demande "Formation à l'outil EVRP".
2756	35	2016-05-12 15:30:22	44	470	Votre inscription à la demande de formation "2016- Formation à l'outil EVRP - CNRS" a été acceptée.
2757	49	2016-05-12 15:40:02	28	471	Inscription à la demande "2016- Initiation au soudage oxyacétylene".
2759	275	2016-05-12 15:40:26	26	\N	Création de votre compte.
2760	49	2016-05-12 15:41:27	44	471	Votre inscription à la demande de formation "2016- Initiation au soudage oxyacétylene" a été acceptée.
2762	51	2016-05-12 15:42:41	28	471	Inscription à la demande "2016- Initiation au soudage oxyacétylene".
2763	51	2016-05-12 15:42:52	44	471	Votre inscription à la demande de formation "2016- Initiation au soudage oxyacétylene" a été acceptée.
2764	116	2016-05-13 16:30:58	76	463	Vous avez indiqué avoir assisté à la formation "2016- FORMATION INITIALE D’ASSISTANT DE PREVENTION".
2765	116	2016-05-13 16:31:20	76	461	Vous avez indiqué avoir assisté à la formation "2016- Formation initiale Assistant prévention - CNRS".
2766	4	2016-05-17 08:58:17	43	57	Le besoin "Le management pour les responsables d'équipes et les chefs d'équipes" a été pourvu.
2767	4	2016-05-17 09:24:36	28	472	Inscription à la demande "2015 - le management pour les responsables d’équipes et les chefs de service".
2768	4	2016-05-17 09:26:01	44	472	Votre inscription à la demande de formation "2015 - le management pour les responsables d’équipes et les chefs de service" a été acceptée.
2769	4	2016-05-17 09:26:26	76	472	Vous avez indiqué avoir assisté à la formation "2015 - le management pour les responsables d’équipes et les chefs de service".
2770	116	2016-05-17 14:31:22	28	470	Inscription à la demande "2016- Formation à l'outil EVRP - CNRS".
2771	116	2016-05-19 08:29:41	44	470	Votre inscription à la demande de formation "2016- Formation à l'outil EVRP - CNRS" a été acceptée.
2772	81	2016-05-27 10:19:46	27	106	Ajout du besoin "Management de projet".
2773	39	2016-05-31 09:23:29	92	400	Vous avez indiqué n'avoir pas assisté à la formation "2015- Prezi : outil de création de supports de présentation orale".
2774	39	2016-05-31 10:51:10	28	473	Inscription à la demande "l'environement externer et interne de l'INPT".
2775	39	2016-05-31 10:52:47	28	474	Inscription à la demande "le rayonnement de l'INPT".
2776	39	2016-05-31 10:54:18	28	475	Inscription à la demande "LES FINANCES".
2777	39	2016-05-31 10:56:00	28	476	Inscription à la demande "methodologie oral des concours".
2778	39	2016-05-31 10:58:14	28	477	Inscription à la demande "formation outil et metier GBCP".
2779	39	2016-05-31 10:58:37	44	476	Votre inscription à la demande de formation "2016- Méthodologie oral des concours" a été acceptée.
2780	39	2016-05-31 10:58:37	76	476	Vous avez indiqué avoir assisté à la formation "2016- Méthodologie oral des concours".
2781	39	2016-05-31 10:59:30	44	477	Votre inscription à la demande de formation "2016- Formation outil et métier GBCP" a été acceptée.
2782	39	2016-05-31 10:59:30	76	477	Vous avez indiqué avoir assisté à la formation "2016- Formation outil et métier GBCP".
2783	39	2016-05-31 10:59:56	44	475	Votre inscription à la demande de formation "2016- LES FINANCES" a été acceptée.
2784	39	2016-05-31 10:59:56	76	475	Vous avez indiqué avoir assisté à la formation "2016- LES FINANCES".
2785	39	2016-05-31 11:00:15	44	474	Votre inscription à la demande de formation "2016- Le rayonnement de l'INPT" a été acceptée.
2786	39	2016-05-31 11:00:15	76	474	Vous avez indiqué avoir assisté à la formation "2016- Le rayonnement de l'INPT".
2787	39	2016-05-31 11:01:13	44	473	Votre inscription à la demande de formation "2016- L'environement externe et interne de l'INPT" a été acceptée.
2788	39	2016-05-31 11:01:13	76	473	Vous avez indiqué avoir assisté à la formation "2016- L'environement externe et interne de l'INPT".
2789	39	2016-05-31 11:01:42	28	452	Inscription à la demande "2016- PREZY".
2790	39	2016-05-31 11:02:01	44	452	Votre inscription à la demande de formation "2016- PREZY" a été acceptée.
2791	276	2016-06-13 13:52:39	26	\N	Création de votre compte.
2792	276	2016-06-15 17:37:31	28	486	Inscription à la demande "2010- Bases statistiques Module 1".
2795	276	2016-06-16 14:08:59	44	486	Votre inscription à la demande de formation "2010- Bases statistiques Module 1" a été acceptée.
2796	276	2016-06-16 14:08:59	76	486	Vous avez indiqué avoir assisté à la formation "2010- Bases statistiques Module 1".
2797	276	2016-06-20 09:30:26	28	488	Inscription à la demande "Test".
2798	276	2016-06-20 09:31:50	28	489	Inscription à la demande "Test".
2799	276	2016-06-20 09:35:56	28	490	Inscription à la demande "Test".
2800	276	2016-06-20 09:36:30	28	491	Inscription à la demande "Test".
2801	276	2016-06-20 09:37:24	28	492	Inscription à la demande "Test".
2802	276	2016-06-20 09:37:44	28	493	Inscription à la demande "Test".
2803	276	2016-06-20 09:37:59	28	494	Inscription à la demande "Test".
2804	276	2016-06-20 09:39:50	28	495	Inscription à la demande "Test".
2805	276	2016-06-20 09:51:14	28	496	Inscription à la demande "Test2".
2806	276	2016-06-20 10:13:11	28	497	Inscription à la demande "Test3".
2807	276	2016-06-20 10:14:05	28	498	Inscription à la demande "Test4".
2809	276	2016-06-21 14:46:58	28	500	Inscription à la demande "TestAlpha".
2810	276	2016-06-21 14:50:37	28	501	Inscription à la demande "DemandeAlpha1".
2811	276	2016-06-21 16:01:45	28	502	Inscription à la demande "TestOmega".
2812	276	2016-06-21 16:02:20	28	503	Inscription à la demande "TestOmegaFinal".
2813	276	2016-06-23 10:26:36	44	492	Votre inscription à la demande de formation "Test" a été acceptée.
\.


--
-- Data for Name: infoagent; Type: TABLE DATA; Schema: public; Owner: laplace_formation
--

COPY infoagent (id, statut_id) FROM stdin;
2	5
4	5
5	4
6	9
8	8
9	9
10	7
11	6
14	5
19	9
21	8
22	12
24	8
34	2
35	7
36	8
37	5
38	7
39	9
40	6
41	7
42	12
43	7
44	6
1	1
46	6
49	8
51	9
52	7
53	6
54	7
55	7
57	8
58	1
59	7
60	4
61	4
62	3
63	4
66	8
67	4
68	4
69	6
70	2
71	8
73	4
74	4
75	4
76	4
77	5
78	4
80	1
81	2
82	7
83	1
84	1
85	4
86	1
89	1
91	13
94	1
95	5
96	13
98	4
99	3
100	4
101	3
102	3
103	4
104	4
105	4
107	4
108	3
109	4
112	8
113	8
114	8
111	6
133	3
134	8
136	1
140	4
141	3
142	4
143	4
144	6
147	2
148	4
149	4
56	6
152	4
154	4
157	4
158	4
160	4
106	7
167	1
168	2
169	1
170	2
171	2
172	4
174	4
175	2
177	2
178	4
179	4
90	1
188	13
195	11
250	2
252	7
97	6
253	14
65	16
260	4
264	4
265	3
266	13
268	17
116	18
269	13
138	19
274	1
\.


--
-- Data for Name: infodoctorant; Type: TABLE DATA; Schema: public; Owner: laplace_formation
--

COPY infodoctorant (id, statut_id, debutthese, responsable, ecoledoctorale, bourse) FROM stdin;
3	1	2011-10-03	Nathalie Raveu	GEET	DGA
13	1	2008-10-01	Olivier Pascal	GEET	\N
23	1	2010-10-01	GENESYS	\N	\N
27	1	2010-01-01	inconnu	\N	\N
28	2	2010-01-01	MDCE	\N	\N
33	1	2009-01-01	inconnu	\N	\N
47	1	2013-10-01	LEBEY	GEET	CNRS
48	1	2013-10-01	Laurent	GEET	\N
50	1	2013-10-01	Carole Henaux	GEET	CNES
93	1	2009-10-01	inconnu	\N	\N
110	1	2009-10-01	inconnu MDCE	\N	\N
115	1	2009-04-21	inconnu CODIASE	\N	\N
125	1	2010-10-01	inconnu MDCE	\N	\N
126	1	2010-10-01	inconnu MDCE	\N	\N
127	1	2010-10-01	inconnu MDCE	\N	\N
129	1	2010-10-01	inconnu MPP	\N	\N
135	1	2010-10-01	Poirier	\N	\N
139	1	2010-10-01	inconnu CODIASE	\N	\N
146	1	2010-10-01	inconnu	\N	\N
12	1	2009-12-04	Jacques David	GEET	\N
79	1	2009-10-01	inconnu PRHE	\N	\N
122	1	2011-04-11	inconnu CODIASE	\N	\N
145	1	2012-09-04	inconnu LM	\N	\N
130	1	2010-10-06	inconnu DSF	\N	\N
128	1	2010-02-08	inconnu MPP	\N	\N
132	1	2010-11-08	inconnu GENESYS	\N	\N
120	1	2011-09-14	Pigache	\N	\N
231	1	2013-09-11	GENESYS	GEET	\N
119	1	2011-10-01	inconnu CS	\N	\N
233	3	2014-06-16	GENESYS	\N	\N
123	2	2009-10-19	inconnu MDCE	\N	\N
64	1	2009-01-01	inconnu MPP	\N	\N
137	1	2011-10-03	inconnu MPP	\N	\N
92	1	2009-09-03	inconnu MPP	\N	\N
150	1	2013-03-05	Nathalie Raveu	GEET	Gouvernement du Brésil - CSF
153	1	2013-10-01	teyssedre	\N	\N
155	1	2014-01-01	Thierry Meynard / Hubert Piquet	\N	\N
156	1	2013-10-01	Pascal Maussion	GEET	Ministérielle
159	1	2012-12-05	MDCE	\N	\N
161	1	2011-11-03	Richardeau	\N	\N
162	1	2013-10-01	François PIGACHE - Jean François ROUCHON	GEET	\N
163	1	2013-10-01	Jean-René Poirier	\N	\N
164	1	2013-10-01	Maria DAVID-PIETRZAK	GEET	\N
165	1	2013-10-01	Raveu Nathalie	GEET	\N
166	1	2013-02-15	Yvan Lefevre	GEET	CIFRE
232	3	2014-03-17	GENESYS	\N	\N
180	1	2012-03-23	Llibre	GEET	ANRT
181	1	2013-03-18	GREM3	GEET	Ciffre Thalès
182	1	2013-01-15	GREM3	GEET	ANRT
183	1	2013-09-01	FADEL	GEET	CEDRE + USJ
87	2	2009-10-01	inconnu MPP	\N	\N
118	2	2011-06-22	inconnu CS	\N	\N
124	2	2010-06-25	inconnu MDCE	\N	\N
88	1	2010-02-04	inconnu MPP	\N	\N
15	2	2009-01-01	Nathalie Raveu	\N	\N
184	2	2009-03-09	CODIASE	\N	\N
185	3	2011-02-14	GENESYS	\N	\N
186	1	2009-02-04	Nogarède GREM3	GEET	\N
187	1	2009-02-04	CODIASE	GEET	\N
189	1	2009-02-04	GENESYS	GEET	\N
190	1	2009-02-04	CODIASE	GEET	\N
191	1	2009-02-04	CODIASE	GEET	\N
192	3	2009-03-02	CS	\N	\N
193	3	2009-03-17	CS	\N	\N
194	2	2009-05-04	CS	\N	\N
196	1	2009-09-04	CODIASE	\N	\N
197	3	2009-09-02	GREM3	\N	\N
198	3	2009-11-05	GENESYS	\N	\N
199	3	2009-09-02	GENESYS	\N	\N
200	1	2010-02-08	CODIASE	\N	\N
201	1	2010-04-08	CS	\N	\N
202	2	2010-06-03	CS	\N	\N
203	1	2011-02-07	GENESYS	\N	\N
204	1	2011-03-14	GENESYS	\N	\N
205	3	2011-02-04	CODIASE	\N	\N
206	1	2013-03-04	GREM3	GEET	\N
207	1	2012-10-01	CODIASE	GEET	\N
208	1	2011-11-14	CODIASE	GEET	\N
209	1	2012-03-19	GENESYS	GEET	\N
210	1	2012-03-23	CS	GEET	\N
211	3	2012-03-01	inconnu	\N	\N
212	3	2012-02-27	CODIASE	\N	\N
213	3	2012-03-06	GREPHE	\N	\N
214	3	2012-03-19	GENESYS	\N	\N
215	3	2012-03-22	GREM3	\N	\N
216	3	2012-03-22	GREM3	\N	\N
217	3	2012-11-03	GENESYS	\N	\N
218	3	2012-03-26	GENESYS	\N	\N
219	3	2012-03-19	GREM3	\N	\N
220	3	2012-03-19	GRE	\N	\N
221	3	2012-03-12	GRE	\N	\N
222	1	2012-08-27	GREM3	GEET	\N
223	1	2012-04-25	GENESYS	GEET	\N
224	1	2012-10-01	GREM3	GEET	\N
225	1	2012-08-27	CODIASE	GEET	\N
226	1	2012-10-01	CODIASE	GEET	\N
227	3	2012-09-14	GRE Raveu	\N	\N
228	1	2011-06-22	CS	GEET	\N
229	3	2014-03-21	CODIASE	\N	\N
234	3	2014-07-18	CS	\N	\N
235	2	2009-10-01	Christophe Turpin	\N	\N
236	1	2014-09-01	Maurice FADEL	GEET	cotutelle Université Saint-Joseph (Liban)
237	2	2014-01-06	Philippe LADOUX	GEET	CIFRE
238	1	2013-12-03	Maurice FADEL	GEET	CIFRE
239	1	2014-09-01	Jean-François ROUCHON	GEET	contrat doctoral
240	1	2013-11-12	RICHARDEAU	GEET	CIFRE
241	1	2014-10-01	MAUSSION	GEET	Contrat doctoral Bourse du Gouvernement Français (BGF)
242	1	2014-10-01	MAUSSION et ZISSIS	GEET	Bourse du gouvernement du Conseil Scientifique Chinois
243	1	2014-10-01	MAUSSION et PICOT	GEET	CIFRE
244	1	2014-10-01	MEYNARD et DUTOUR	\N	l'Institut de Recherche et Technologie Saint-Exupéry
245	1	2014-10-08	FADEL	GEET	CSC Chinoise
246	1	2014-10-15	Junwu TAO	GEET	gouvernement chinois
247	4	2014-11-13	LADOUX	\N	\N
248	3	2014-11-25	F. LACRESSONNIERE	\N	\N
249	1	2014-09-18	MAurice FADEL	\N	\N
251	1	2014-10-01	Severine LEROY	GEET	MESR
254	1	2014-01-01	Pascal DUPUIS (LM)	GEET	Bourse doctorale du gouvernement syrien
255	1	2013-10-01	Nicolas Gherardi	GEET	quebecoise
131	2	2010-10-01	inconnu GREPHE	\N	\N
256	1	2014-11-09	Marc MISCEVIC	GEET	\N
257	3	2015-03-16	ASTIER	\N	\N
258	1	0005-02-28	Fadel	\N	\N
259	1	2015-02-02	Juan MARTINEZ VEGA	GEET	Cifre  Emerson LEROY SOMER
261	3	2015-03-16	ASTIER	\N	\N
262	3	2015-03-16	LADOUX	\N	\N
263	1	2013-10-01	BERQUEZ Laurent	GEET	contrat doctoral UPS
267	4	2015-08-24	LADOUX	\N	\N
270	1	2015-02-02	David MALEC	GEET	cifre
271	1	2015-11-01	Pigache François	GEET	ANR
272	1	2013-12-12	Merbahi Nofel	GEET	Ministérielle
273	1	2015-10-01	Nofel MERBAHI	GEEET	Bourse doctorale
275	1	2015-10-01	Freddy Gaboriau	GEET	ministérielle
276	3	2016-06-06	Olivier PIGAGLIO	\N	\N
\.


--
-- Data for Name: message; Type: TABLE DATA; Schema: public; Owner: laplace_formation
--

COPY message (id, thread_id, author_id, date, text) FROM stdin;
1	1	51	2013-11-12 10:42:44	formation sur le 4eme axes du logiciel esprit 2013
2	2	51	2013-11-12 10:47:18	formation sur le logiciel esprit pour l'utilisation du 4eme axes
3	2	2	2013-11-12 11:33:13	Bonjour\r\nIl s'agit de la formation dont m'a parlé Robert Larroche ce matin.\r\nTu as ce besoin je suppose?\r\nCordialement\r\nOlivier Pigaglio
4	2	51	2013-11-12 11:46:24	oui c'est ça, en fait c'est pour Robert, Thierry et moi cette formation esprit\r\non est les seuls a utiliser ce logiciel de FAO\r\nThierry est en arrêt, mais il va t'envoyer sa réponse par le web mail.\r\n\r\nen fait tout a l'heure ça marchais pas quand j'ai voulu envoyé le dossier complété\r\net apparemment tu l'as reçu.\r\n\r\nmerci d'avance\r\n@+ Sébastien
5	2	2	2013-11-12 12:02:42	tu as ajouté une discussion et pas un besoin de formation en fait.\r\nil faut que tu choisisses si ce besoin est T1, T2 ou T3 et que tu cliques sur Envoyer.\r\nC'est noté pour Thierry\r\nOlivier
6	3	147	2014-01-22 14:11:34	Pour faire un retour, la formation s'est déroulée comme prévue (date, lieu, durée) et a été très intéressante pour les deux participants.\r\nAvec nos remerciements pour le soutien apporté.\r\nBien cordialement,\r\nML Locatelli.
7	3	2	2014-01-22 14:17:34	Je note que vous y avez participé tous les 2.\r\nPour les prochaines fois, il faudra cocher la cas " OUI " à la question " Avez-vous assisté à la formation ? "\r\nBonne journée\r\nOlivier
11	5	106	2014-05-28 10:53:53	j'avais mis en jours\r\nDurée de la formation (en nombre d'heures):\r\n14h30
12	6	106	2014-05-28 11:00:12	j'avais mis en jours:\r\nDurée de la formation (en nombre d'heures) :\r\n37 heures
13	5	2	2014-05-28 11:02:48	bonjour\r\ndemande validée, tu n'as plus qu'à indiquer que tu y as assisté\r\nmerci\r\nolivier
14	6	2	2014-05-28 11:02:57	bonjour\r\ndemande validée, tu n'as plus qu'à indiquer que tu y as assisté\r\nmerci\r\nolivier
15	7	183	2014-07-23 16:30:26	la session dure 1 semaine et pas uniquement "1" comme affiché dans le nombre d'heures..... je n'ai pas réussi à modifier.\r\nmerci
16	7	2	2014-07-23 16:42:42	c'est modifié
17	7	183	2014-09-02 17:17:09	Bonjour,\r\nJ'avais bien commencé la formation le vendredi 29 aout. Elle est en cours, et on termine le vendredi 5 sep.\r\nCordialement
18	7	2	2014-09-02 17:31:31	Bonjour\r\nJ'ai regardé quelles étaient les formations pour lesquelles ce n'était pas renseigné, je n'ai pas regardé quand elles avaient lieu.\r\nMerci pour la réponse.\r\nCordialement
19	8	77	2014-09-08 09:55:17	Salut Olivier, \r\n\r\nj'avais prévu d'assister à cette formation.\r\nJ'ai appris que je partais en mission quelques temps après mettre inscrit et je n'avais pas pu y assister. \r\nSi elle se représente, je suis toujours intéressé.\r\n\r\nLaurent.
20	8	2	2014-09-08 09:58:02	Salut\r\nAjoute le besoin dans ce cas\r\nMerci\r\nOlivier
21	9	43	2014-09-09 09:44:36	Formation effectué
24	11	43	2014-09-10 11:15:54	Cette formation peut-être lié avec la formation programmation en C d'un microcontrôleur.
25	12	2	2014-09-24 14:33:50	Bonjour Sébastien\r\nN'oublie pas de remplir la fiche d'inscription CNRS et de la faire signer par Christian Laurent\r\nBonne journée\r\nOlivier
26	13	51	2014-09-24 14:38:18	Bonjour Olivier!\r\n\r\nje viens de m'inscrire a la formation frabrication additive métallique du réseau méca du cnrs\r\nj'ai coché la case ici et maintenant, c'était peu être ici et demain!\r\n\r\net comme j'ai pas reçu le mail de gilles Roudil, je suis pas sur que de juste cocher cette case \r\nsuffise a l'inscription.\r\n\r\ntu peux me donner un ou deux conseils sur la marche a suivre.\r\n\r\nmerci d'avance\r\n@+ Sébastien Dalll'ava
27	13	2	2014-09-24 14:44:20	ton inscription sur l'application disponible sur l'intranet du LAPLACE me permet de lister les besoins et les inscriptions aux formations des agents du labo (parce que je dois faire les bilans pour transmettre aux tutelles).\r\nc'est une étape qui s'ajoute aux fiches d'inscription à transmettre aux tutelles qui proposent des formations.\r\nIci c'est le CNRS, donc :\r\n- soit il faut que tu remplisses une fiche à la main ( http://www.dr14.cnrs.fr/midi-pyrenees/FormationPermanente/DemandeFormation.rtf ) et que tu la fasses signer par Christian Laurent;\r\n- soit tu t'inscris en ligne sur le site du CNRS ( http://www.cnrs.fr/midi-pyrenees/FormationPermanente/InscriptionEnLigne/ ) et je peux valider ta demande en ligne\r\nEncore une fois, l'application sur l'intranet du laboratoire est une étape supplémentaire (désolé) mais qui me permet à moi pour fournir des bilans les plus complets possibles et à toi d'avoir une trace des formations que tu as suivi pour ton dossier de carrière.\r\nA+\r\nOlivier
30	9	43	2014-12-18 08:33:48	Bonjour,\r\nnous avons été à Nallioux en formation une semaine.\r\ncordialement,\r\nNordine.
31	15	149	2015-01-14 09:32:55	Merci d'indiquer que j'ai participé à cette formation:\r\nManagement/qualité > Qualité et management des projets > 2014-Journée "Initiative au montage de projets de recherche"- INPT
32	15	2	2015-01-14 15:01:24	Il faut d'abord s'y inscrire.\r\nLa formation existant déjà, il faut choisir le nouveau de priorisation (T1 ici) et s'ajouter.\r\nJe valide ensuite la demande (normalement avant la formation) et ensuite on peut indiquer si on a effectué ou pas la formation
33	15	149	2015-01-15 09:16:27	Cette formation a disparu de la liste.
34	15	2	2015-01-15 16:01:31	elle existe toujours\r\nhttp://formation.laplace.univ-tlse.fr/admin/demandes/voir/338
35	16	252	2015-03-24 14:35:58	Niveau d'habilitation demandée:BC-B2Vessai-BR-H2Vessai
36	17	252	2015-03-24 14:39:44	Formation faite en février 2012
37	16	2	2015-03-24 17:29:42	Nous essayons en ce moment de planifier les recyclages de l'habilitation électrique\r\nPour le niveau demandé, il faudra en discuter avec le formateur je pense, je ne pourrai pas moi te dire ce dont tu as besoin\r\nTu seras informé du recyclage\r\nOlivier
38	17	2	2015-03-24 17:32:12	PSC ça veut dire quoi? je ne connais pas\r\nEn tout cas, si c'est pour être SST, il faut en parler avec Benoit Lantin pour l'aspect hygiène et sécurité sur le site UPS. \r\nOlivier
39	18	2	2015-04-02 11:03:25	Salut\r\ntu as les dates de la formation?\r\nOlivier
40	19	106	2015-09-08 09:52:05	13, 14,15 octobre 2014
41	20	106	2015-09-08 10:00:59	J'y ai participé, mais pour l'indiquer, ce n'est plus possible.
42	20	2	2015-09-08 13:02:08	Je t'ai ajouté à cette formation\r\nOlivier
43	19	2	2015-09-08 13:03:19	j'ai validé ta présence à cette formation
44	21	81	2015-09-09 13:17:00	Je suis impliqué dans un projet qui aura besoin d'un chercheur post-doctoral en 08/2016. Je participerai à toutes les étapes de la sélection.\r\nLa formation a été proposé par le CNRS en 09/2015, mais je n'étais pas disponible. Cordialement,
45	22	35	2016-05-11 08:32:34	Voilà c'est fait, je vois Robert et jacques ce matin. (Olivier R. Excusé)\r\n\r\nDès que tu valides, peux tu leur signaler pour qu'il s'inscrive selon le processus ?\r\n\r\nmerci,\r\n\r\na +
46	23	39	2016-05-31 09:24:40	olivier,\r\n\r\nserait il possible de me ré inscrire à cette formation puisqu'elle que j'ai été refoulé pour manque de place la dernière fois.\r\nmerci\r\nCathy
47	23	2	2016-05-31 09:32:40	Il faut en ajouter une nouvelle pour 2016 ;)
\.


--
-- Data for Name: need; Type: TABLE DATA; Schema: public; Owner: laplace_formation
--

COPY need (id, author_id, category_id, issuedate, title, description) FROM stdin;
2	1	41	2013-08-22 10:21:02	Sauveteur Secouriste du Travail - Formation initiale	\N
4	1	38	2013-08-22 10:23:00	Risques chimiques	\N
5	1	38	2013-08-22 10:25:31	Habilitation Electrique pour non électricien - Formation initiale	\N
3	1	41	2013-08-22 10:22:23	Sauveteur Secouriste du Travail - Recyclage	\N
1	1	38	2013-08-09 20:48:38	Habilitation Electrique pour non électricien - Recyclage	\N
6	1	69	2013-08-22 10:31:31	Prise de notes	\N
7	1	28	2013-08-22 10:32:11	Excel - Perfectionnement	\N
8	1	57	2013-08-22 10:32:43	Préparation aux concours	\N
9	1	57	2013-08-22 10:32:56	Rédaction du rapport d'activités	\N
10	1	66	2013-08-22 10:33:39	Droits et devoirs du fonctionnaire	\N
11	1	66	2013-08-22 10:33:52	Connaitre l'Europe	\N
12	1	59	2013-08-22 10:34:54	Anglais	\N
13	1	23	2013-08-22 10:35:29	Automatiser l’administration avec Powershell 2.0	\N
14	1	23	2013-08-22 10:35:40	Planification et administration de la virtualisation de bureaux	\N
15	1	62	2013-08-22 10:39:14	Espagnol - Initiation	\N
16	1	60	2013-08-22 10:39:27	Russe - Initiation	\N
17	1	16	2013-08-22 10:40:02	Technologie du vide, ultra-vide et mesures	\N
19	1	31	2013-08-22 10:42:11	Inventor - Initiation	\N
20	1	31	2013-08-22 10:42:24	Inventor - Perfectionnement	\N
21	1	15	2013-08-22 10:45:05	Matériaux innovants : Céramiques, polymères, …	\N
23	1	66	2013-08-22 10:46:15	Les évolutions de la Fonction Publique	\N
22	1	16	2013-08-22 10:45:36	Normes de sécurité sur les manips	\N
24	1	29	2013-08-22 10:49:17	Horizon – Acquisition, initiation	\N
25	1	29	2013-08-22 10:49:30	Horizon – Bulletinage	\N
26	1	28	2013-08-22 10:51:07	Word - Perfectionnement	\N
27	1	24	2013-08-22 10:51:53	Labview	\N
28	1	38	2013-08-22 10:53:47	Analyser les risques sur les nouveaux prototypes	\N
29	1	15	2013-08-22 10:54:13	Usinage de nouveaux matériaux	\N
30	1	48	2013-08-22 10:55:28	SIFAC - Module Dépenses	\N
33	1	24	2013-08-22 10:57:56	Modélisation orientée objet "Unified Modelling Language" (UML)	\N
34	1	25	2013-08-22 10:58:11	Calculs scientfiques parallèles	\N
35	1	23	2013-08-22 10:58:24	Réseaux informatiques	\N
36	1	24	2013-08-22 10:58:47	Méthodes de développement "agiles"	\N
37	1	16	2013-08-22 10:59:09	Thermodynamique avancée	\N
39	1	16	2013-08-22 10:59:47	Conception, optimisation, vérification et mise au point de circuits PSOC	\N
40	1	59	2013-08-22 11:00:16	Anglais par téléphone	\N
41	1	16	2013-08-22 11:00:38	Responsable Laser	\N
42	1	24	2013-08-22 11:01:04	Comsol, formation spécifique sur des problèmes d'optimisation	\N
43	1	58	2013-08-22 11:02:25	Gestion du temps de travail	\N
44	1	31	2013-08-22 11:03:17	Multiphysics d'AUTODESK	\N
32	1	70	2013-08-22 10:57:26	Altium Designer	\N
45	1	38	2013-08-22 11:09:50	Technique et sécurité LASER	\N
46	1	38	2013-08-22 11:10:18	Personnel Compétent en Radioprotection - Recyclage	\N
47	1	38	2013-08-22 11:11:49	Manipulation extincteur	\N
48	1	70	2013-08-22 11:14:45	PSIM, simulations de circuits électriques	\N
50	46	16	2013-10-25 13:57:40	thermique et hydraulique pour le dimensionnement de dissipateurs (refroidisseur à air forcé, plaque à eau)	\N
52	2	31	2013-11-12 09:05:42	Formation CFAO ESPRIT Module 4/5 Axes	Utilisation CFAO Esprit pour usinage Commande Numérique .
53	2	25	2013-11-14 17:53:24	ANSYS Maxwell	initiation au logiciel
55	69	57	2014-04-03 16:54:16	préparation à la retraite	\N
56	2	60	2014-04-08 18:32:09	JAPONAIS débutant	\N
57	2	54	2014-09-08 10:01:54	Le management pour les responsables d'équipes et les chefs d'équipes	Le management pour les responsables d'équipes et les chefs d'équipes
60	155	62	2014-09-17 10:01:16	Cours Allemand	\N
61	2	24	2014-10-08 12:29:22	initiation à MATLAB	formation pour Débutants
63	2	28	2014-10-10 18:11:07	Formation au montage vidéo	\N
54	2	15	2013-12-03 11:04:48	Formation Microscopie à Force Atomique	Formation de base
65	2	59	2014-10-24 11:59:15	Anglais à l'écrit	\N
66	2	16	2014-10-24 12:00:15	Gestion de la maintenance d'un parc d'appareil de métrologie	\N
68	2	31	2014-10-24 15:09:52	Inventor 2015	\N
69	2	31	2014-10-24 15:10:11	Logiciel "ESPRIT 3D"	\N
58	2	24	2014-09-10 11:13:34	Programmation interface LABVIEW , débutant et avancé	Cette formation permettrait les rappels de base pour programmer avec LABVIEW.\r\nLe but de cette formation est de communiqué entre un ordinateur et un microcontrôleur par l'USB voir une liaison série RS232. ( A savoir les ordinateurs ne sont plus équipés de liaison RS232) .
70	2	15	2014-10-24 15:10:49	Soudure "mécanique"	prototypes, bancs de d’expérimentation …
67	2	16	2014-10-24 12:11:35	L'hydraulique pour le dimensionnement de dissipateurs (plaque à eau)	\N
51	2	16	2013-10-25 13:58:46	simulations de géométries pour connaitre les valeurs des éléments parasites d'un câblage défini (bus bar)	en priorité : les éléments parasites (inductance, mutuelle, capacité, résistance)\r\ndans un deuxième temps : thermique (conduction, convection … rayonnement) \r\nperturbation électromagnétique
71	252	38	2015-03-02 17:46:21	Risques liés à la manipulation, l'installation des bouteilles de gaz	Dans le cadre de la mise en place de manips utilisant des bouteilles, je souhaiterai connaître les risques inhérents aux différents types de gaz, des consignes et moyens de prévention à mettre en oeuvre pour leurs utilisations.\r\nL'idée serait une sorte d'habilitation à utiliser les différents types de gaz
38	2	2	2013-08-22 10:59:22	Méthode de Monte Carlo	\N
31	2	2	2013-08-22 10:56:33	Calcul de depot d'energie par simulation Monte Carlo tel que PENELOPE, GEANT4, EGS	\N
62	2	72	2014-10-10 18:10:04	Formation PAO	recherche d'une formation montrant des outils d'aide à la création et édition de documents
64	2	70	2014-10-24 11:56:01	Logiciel PLECS	Besoin d'une formation en français\r\nprise en main / simulation de base pour les personnels techniques
72	252	38	2015-03-02 17:51:10	Gestes et postures pour le déplacement de charges	Connaître tous les bonnes pratiques, outils et gestuels à appliquer pour la manipulation de différents types de charges, notamment pour les charges lourdes.
73	48	38	2015-08-26 13:53:27	Habilitation électrique BE Essai HE essai (BT et HT)	Participation au stage de formation avec Sorin Dinculescu, le 24 et 25 juin 2015
77	46	38	2015-08-31 15:31:32	recyclage habilitation électrique	dernier recyclage en mai 2012
59	2	24	2014-09-10 11:33:02	Programmation Langage C, débutant et avancé	Cette formation, dans mon cas, serait un outils à fin de programmer un microcontrôleur "PSoC".\r\nGestion d'entrées numérique et analogique ainsi que des commandes numériques et analogiques.\r\nPendant cette formation, le but est de faire un programme : exemple.
78	265	59	2015-09-02 10:58:44	Anglais - perfectionnement	\N
79	265	62	2015-09-02 10:58:56	Espagnol - Perfectionnement	\N
75	2	55	2015-08-31 15:23:39	Règles et méthodes d'assurance qualité	Connaissances générales : \r\nobjectifs, méthodes, outils, mise en place d'un plan qualité, normes\r\nmode fonctionnement projet\r\nrecette, contrôle qualité ....
80	81	55	2015-09-09 13:13:14	Recruter un chercheur post- doctoral	Outils et techniques pour l'entretien.\r\nMéthodes de recherche du candidat.
81	2	31	2015-10-12 15:29:44	Initiation au logiciel JMAG	\N
82	2	15	2015-10-12 16:02:27	Techniques de collage / assemblage	\N
74	2	31	2015-08-31 11:04:00	Logiciel CFAO ESPRIT - contrôle de robot d'usinage	\N
87	2	25	2015-10-12 16:37:31	Évolution Mac OSX Server	\N
86	2	23	2015-10-12 16:37:22	Windows 7 sécurisation postes clients	\N
85	2	23	2015-10-12 16:37:00	Sécurité, détection d'intrusion	\N
84	2	25	2015-10-12 16:17:15	OS pour serveurs	\N
83	2	25	2015-10-12 16:16:59	Windows 8	\N
88	2	16	2015-10-12 17:21:58	Conception d' une carte électronique orientée haute fréquence	\N
89	2	23	2015-10-12 17:25:03	Formation Expert - Maitrise des différentes fonctionnalités des firewall Netasq	\N
90	2	23	2015-10-12 17:25:39	Administration OSX Server Snow Leopard	\N
91	2	48	2015-10-12 17:38:41	Gestion financière des contrats européens	Mise en adéquation avec les nouvelles procédures
92	2	24	2015-10-12 21:11:42	Matlab avancé	\N
93	2	58	2015-10-12 21:51:26	Formation aux métiers de la communication	\N
94	2	48	2015-10-12 22:18:47	Règlementation dans la justification des contrats en fonction de leur nature	\N
95	2	48	2015-10-12 22:22:53	SIFAC Immobilisations	\N
96	2	48	2015-10-12 22:30:12	SIFAC - Module inventaire	\N
97	2	16	2015-10-12 22:41:25	Formation sur les réacteurs à plasma	\N
98	2	25	2015-10-13 08:31:39	Initiation à COMSOL	\N
99	2	55	2015-10-14 13:04:38	conduite de réunion, animation de table ronde, animation d'atelier de créativité	\N
100	81	55	2015-11-27 15:07:51	Conduire un entretien de résolution de problèmes	Dans l'encadrement de doctorants il est nécessaire, ponctuellement, de résoudre de dysfonctionnements. Il est important de les identifier, et proposer de solutions et actions/stratégies correctives. Il est nécessaire de bien conduire la réunion et le suivi pour que les actions proposés soient efficaces.
101	252	16	2016-01-06 14:04:21	Détection de Fuites	\N
102	272	14	2016-01-18 15:25:20	Formation spécifique destinée aux personnes réalisant des procédures expérimentales sur modèle rongeur (ancien niveau 2)	Acquérir la qualification obligatoire de personne réalisant des procédures expérimentales sur modèle rongeur - niveau opérateurs\r\n\r\nPersonnels appelés à participer directement aux expériences sur modèle rongeur (techniciens, ingénieurs et étudiants)
103	2	27	2016-03-08 14:13:32	Formation Windev Winweb	Cette formation aurait pour objectif de valoriser les outils scientifiques réalisés dans l'équipe en proposant une formation au développement d'interfaces multi plateformes conviviales permettant de proposer nos outils à la communauté scientifique.\r\nIl faudrait donc acquérir les bases du langage afin de faire converser nos outils (Fortran, C++) avec le logiciel Windev (générateur d'interface).
104	44	16	2016-04-05 15:41:36	Programmation d'Interface Homme-Machine en C#	Objectifs de la formation\r\n•\tcomprendre les principes essentiels de  la programmation objet\r\n•\tpouvoir construire une application autonome de type IHM en langage C# sur PC Windows en environnement dotNet, utiliser la documentation en ligne\r\n•\tfaire communiquer l'IHM avec une application embarquée sur un système à base de circuits PSoC
105	43	16	2016-04-05 15:54:27	Programmation Interface Homme Machine en C#	Objectifs de la formation\r\n•\tcomprendre les principes essentiels de  la programmation objet\r\n•\tpouvoir construire une application autonome de type IHM en langage C# sur PC Windows en environnement dotNet, utiliser la documentation en ligne\r\n•\tfaire communiquer l'IHM avec une application embarquée sur un système à base de circuits PSoC
106	81	55	2016-05-27 10:19:46	Management de projet	Méthodes de planification des projets et avancements\r\nOutils d'animation et coordination des projets multipartenaires
\.


--
-- Data for Name: needsubscription; Type: TABLE DATA; Schema: public; Owner: laplace_formation
--

COPY needsubscription (need_id, user_id, type_id, subscriptiondate) FROM stdin;
12	5	1	2013-09-16 15:55:39
2	128	1	2013-10-15 14:47:01
3	68	1	2013-10-16 10:53:46
33	67	2	2013-10-16 13:53:48
26	42	1	2013-10-25 12:12:12
7	42	1	2013-10-25 12:12:18
51	46	1	2013-10-25 13:58:46
12	50	2	2013-11-14 17:54:37
12	157	2	2013-12-03 11:01:38
27	157	2	2013-12-03 11:02:15
4	48	2	2013-12-05 09:34:07
47	71	2	2014-03-20 16:23:51
56	48	1	2014-04-09 16:24:58
34	67	3	2014-09-03 15:23:52
57	77	1	2014-09-08 10:01:54
12	77	1	2014-09-08 10:03:00
58	43	3	2014-09-10 11:13:59
59	43	3	2014-09-10 11:33:02
60	155	1	2014-09-17 10:01:16
12	112	1	2014-10-09 09:38:34
63	53	2	2014-10-10 18:11:55
62	53	2	2014-10-10 18:12:05
64	46	1	2014-10-24 11:56:01
65	46	1	2014-10-24 11:59:15
66	46	2	2014-10-24 12:00:15
64	112	1	2014-10-24 12:16:33
64	66	1	2014-10-24 12:17:04
64	59	1	2014-10-24 12:17:30
70	51	1	2014-10-24 15:11:35
70	49	1	2014-10-24 15:12:08
70	71	1	2014-10-24 15:12:34
68	82	1	2014-10-24 15:13:28
68	40	1	2014-10-24 15:13:43
68	113	1	2014-10-24 15:13:59
46	78	1	2014-10-24 16:06:07
47	112	2	2015-01-14 08:35:58
22	252	1	2015-03-02 17:20:15
19	252	1	2015-03-02 17:23:52
5	252	1	2015-03-02 17:27:08
71	252	1	2015-03-02 17:46:21
72	252	1	2015-03-02 17:51:10
47	252	2	2015-03-24 14:37:40
13	52	2	2015-08-26 13:30:48
15	52	1	2015-08-26 13:31:34
12	54	1	2015-08-31 10:54:45
75	46	1	2015-08-31 15:23:39
77	46	1	2015-08-31 15:31:33
78	265	1	2015-09-02 10:58:44
79	265	1	2015-09-02 10:58:56
80	81	2	2015-09-09 13:13:14
81	40	1	2015-10-12 15:30:24
81	178	1	2015-10-12 15:30:37
9	42	1	2015-10-12 15:54:41
8	42	1	2015-10-12 15:54:52
17	113	1	2015-10-12 16:01:18
82	113	1	2015-10-12 16:02:27
19	54	1	2015-10-12 16:07:01
74	54	1	2015-10-12 16:07:07
83	268	1	2015-10-12 16:16:59
84	268	1	2015-10-12 16:17:15
27	77	3	2015-10-12 16:24:55
85	97	3	2015-10-12 16:37:00
86	97	3	2015-10-12 16:37:22
87	97	3	2015-10-12 16:37:31
68	114	3	2015-10-12 16:58:10
88	46	3	2015-10-12 17:21:58
89	144	3	2015-10-12 17:25:03
90	144	3	2015-10-12 17:25:39
91	56	1	2015-10-12 17:38:41
57	44	3	2015-10-12 17:50:59
12	35	2	2015-10-12 18:19:11
27	35	1	2015-10-12 18:19:21
12	116	1	2015-10-12 21:01:25
3	116	1	2015-10-12 21:01:36
92	11	1	2015-10-12 21:11:42
93	53	1	2015-10-12 21:51:27
94	57	1	2015-10-12 22:18:47
95	269	1	2015-10-12 22:22:53
12	138	1	2015-10-12 22:27:48
96	138	1	2015-10-12 22:30:12
20	82	1	2015-10-12 22:40:24
97	82	1	2015-10-12 22:41:25
98	46	1	2015-10-13 08:31:39
99	147	1	2015-10-14 13:04:38
80	147	2	2015-10-14 13:09:35
100	81	2	2015-11-27 15:07:51
102	272	2	2016-01-18 15:25:20
103	274	1	2016-03-08 14:14:56
54	157	1	2016-03-25 10:04:53
104	44	1	2016-04-05 15:41:36
105	43	1	2016-04-05 15:54:27
106	81	1	2016-05-27 10:19:46
\.


--
-- Data for Name: request; Type: TABLE DATA; Schema: public; Owner: laplace_formation
--

COPY request (id, author_id, category_id, issuedate, title, description, validated, close, processingdate, trainingdate, trainingduration, subscriptiondeadline, availableplacescount, agentsprioritaires, funding) FROM stdin;
4	2	71	2013-08-22 08:46:40	2011- Formation des correspondants formation	à la Grande Motte	t	t	2013-08-22 00:00:00	2011-03-22 00:00:00	21	\N	\N	\N	CNRS
128	2	24	2013-09-20 14:09:26	2010- Initiation C embarqué microcontrôleurs	\N	t	t	2013-09-20 00:00:00	2010-01-01 00:00:00	35	\N	\N	\N	CNRS
242	2	41	2013-10-15 15:39:08	2012- Recyclage Sauveteur Secours du Travail - INPT	\N	t	t	2013-10-15 00:00:00	2012-10-26 00:00:00	4	\N	\N	\N	INPT
15	2	66	2013-09-16 10:44:37	2009- L'état actuel de la mondialisation	\N	t	t	2013-09-16 00:00:00	2009-11-12 00:00:00	7	\N	\N	\N	UPS
16	2	66	2013-09-16 10:45:11	2009- Mieux connaitre le fonctionnement de l'Union Européenne	\N	t	t	2013-09-16 00:00:00	2009-12-08 00:00:00	\N	\N	\N	\N	UPS
14	2	71	2013-09-16 10:43:53	2009- Réunion des correspondants formation du CNRS	\N	t	t	2013-09-16 00:00:00	2009-10-09 00:00:00	7	\N	\N	\N	CNRS
106	2	57	2013-09-20 11:00:54	2011- Préparation aux concours session 1	\N	t	t	2013-09-20 00:00:00	2011-05-19 00:00:00	\N	\N	\N	\N	UPS
19	2	71	2013-09-16 10:58:26	2012- Réunion des correspondants formation du CNRS DR14	à l'Hotel Grand Orléans, Toulouse	t	t	2013-09-16 00:00:00	2012-10-17 00:00:00	7	\N	\N	\N	CNRS
59	2	38	2013-09-19 12:23:22	2009- AR14 Sécurité d'emploi des lasers	2 et 3 mars 2009	t	t	2013-09-19 00:00:00	2009-03-02 00:00:00	14	\N	\N	\N	CNRS
107	2	57	2013-09-20 11:01:19	2011- Préparation aux concours session 2	\N	t	t	2013-09-20 00:00:00	2011-05-30 00:00:00	7	\N	\N	\N	UPS
67	2	59	2013-09-19 17:20:03	2009- Anglais module complémentaire - CNRS	\N	t	t	2013-09-19 00:00:00	2009-03-19 00:00:00	7	\N	\N	\N	CNRS
73	2	16	2013-09-19 17:40:33	2011- Formation Caméra thermique	FLIR Limoges	t	t	2013-09-19 00:00:00	2011-03-28 00:00:00	7	\N	\N	\N	CNRS
221	2	24	2013-10-14 15:26:12	2011- Langage C++	\N	t	t	2013-10-14 00:00:00	2011-01-17 00:00:00	35	\N	\N	\N	CNRS
111	2	16	2013-09-20 11:14:34	2011- Journée thématique du vide	à la DR14	t	t	2013-09-20 00:00:00	2011-11-08 00:00:00	7	\N	\N	\N	CNRS
129	2	70	2013-09-20 14:14:42	2010- Modelsim 2010	\N	t	t	2013-09-20 00:00:00	2010-01-01 00:00:00	14	\N	\N	\N	CNRS
61	2	61	2013-09-19 15:55:12	2009- Français Langue Etrangère, module de 24h - CNRS	\N	t	t	2013-09-19 00:00:00	2009-10-05 00:00:00	24	\N	\N	\N	CNRS
193	2	15	2013-10-11 17:57:25	2009- Matériaux pour l’énergie, Galerne	à Amiens	t	t	2013-10-11 00:00:00	2009-10-12 00:00:00	28	\N	\N	\N	CNRS
66	2	38	2013-09-19 17:17:07	2011- Formation risques chimiques - INPT	ENSIACET, salle du conseil	t	t	2013-09-19 00:00:00	2011-11-16 00:00:00	4	\N	\N	\N	INPT
20	2	38	2013-09-16 11:11:46	2011- Sécurité incendie - manipulation d'extincteur - INPT	\N	t	t	2013-09-16 00:00:00	2011-03-29 00:00:00	\N	\N	\N	\N	INPT
452	2	28	2016-02-10 11:30:39	2016- PREZY	\N	t	f	2016-02-10 00:00:00	2016-06-07 00:00:00	7	\N	\N	\N	\N
195	2	52	2013-10-11 18:14:14	2009- Développer la motivation de l'étudiant	\N	t	t	2013-10-11 00:00:00	2009-11-26 00:00:00	4	\N	\N	\N	UPS
57	2	28	2013-09-18 17:35:16	2012- Word - Perfectionnement	\N	t	t	2013-09-18 00:00:00	2012-06-07 00:00:00	7	\N	\N	\N	INPT
58	2	28	2013-09-18 17:35:56	2012- Excel - Perfectionnement	\N	t	t	2013-09-18 00:00:00	2012-05-24 00:00:00	7	\N	\N	\N	INPT
403	2	38	2015-10-12 10:54:16	2015- Recyclage de l’Habilitation électrique du personnel non électricien - INPT	\N	t	t	2015-10-12 00:00:00	2015-10-06 00:00:00	14	\N	12	\N	CNRS
109	2	48	2013-09-20 11:05:36	2012- SIFAC Fiscalité	\N	t	t	2013-09-20 00:00:00	2012-01-09 00:00:00	7	\N	\N	\N	INPT
29	2	42	2013-09-16 11:39:07	2009- Formation initiale ACMO - UPS	\N	t	t	2013-09-16 00:00:00	2009-05-11 00:00:00	46	\N	\N	\N	UPS
32	2	59	2013-09-17 15:52:29	2010- Anglais classique - CNRS	\N	t	t	2013-09-17 00:00:00	2010-02-01 00:00:00	17	\N	\N	\N	CNRS
71	2	31	2013-09-19 17:38:46	2010- Inventor, formation interne LAPLACE	Journée de formation faite en interne	t	t	2013-09-19 00:00:00	2010-01-14 00:00:00	7	\N	\N	\N	LAPLACE
203	2	4	2013-10-14 10:03:57	2010- Atelier ECOMOD	\N	t	t	2013-10-14 00:00:00	2010-11-15 00:00:00	21	\N	\N	\N	CNRS
458	2	48	2016-02-15 14:18:48	2016- Formation GBCP	\N	t	f	2016-02-15 00:00:00	2016-02-11 00:00:00	14	\N	\N	\N	\N
122	2	15	2013-09-20 13:40:14	2012- Journée Nano - CNRS	\N	t	t	2013-09-20 00:00:00	2012-12-06 00:00:00	7	\N	\N	\N	CNRS
6	2	16	2013-08-22 08:58:11	2013- CEM des équipements électroniques	NEXIO, au 3R2, organisée par le réseau régional des électroniciens CNRS	t	t	2013-08-22 00:00:00	2013-03-12 00:00:00	21	\N	\N	\N	CNRS Réseau des électroniciens
68	2	59	2013-09-19 17:28:23	2010- Anglais débutant, module de 30h - PRES	\N	t	t	2013-09-19 00:00:00	2010-02-01 00:00:00	30	\N	\N	\N	PRES
65	2	24	2013-09-19 16:14:58	2010- PHP MySQL niveau 1	\N	t	t	2013-09-19 00:00:00	2010-01-01 00:00:00	28	\N	\N	\N	CNRS
63	2	16	2013-09-19 16:12:48	2010- Perfectionnement thermographie IR	\N	t	t	2013-09-19 00:00:00	2010-01-01 00:00:00	14	\N	\N	\N	CNRS
30	2	15	2013-09-16 11:40:08	2010- Nanoparticules	\N	t	t	2013-09-16 00:00:00	2010-10-20 00:00:00	4	\N	\N	\N	UPS
62	2	24	2013-09-19 16:01:31	2010- HTML XHTML CSS	\N	t	t	2013-09-19 00:00:00	2010-01-01 00:00:00	28	\N	\N	\N	CNRS
10	2	28	2013-09-16 10:31:56	2009- EXCEL - Se mettre à niveau sur les bases (tableaux et graphiques)	SCC Toulouse	t	t	2013-09-16 00:00:00	2009-02-26 00:00:00	7	\N	\N	\N	CNRS
23	2	48	2013-09-16 11:18:24	2009- SIFAC Dépenses (1)	1,5 jours à 3 jours	t	t	2013-09-16 00:00:00	2009-02-04 00:00:00	11	\N	\N	\N	UPS
25	2	48	2013-09-16 11:20:02	2010- SIFAC Missions Métiers	\N	t	t	2013-09-16 00:00:00	2010-01-25 00:00:00	7	\N	\N	\N	UPS
79	2	28	2013-09-20 10:09:35	2010- EXCEL 2003 : Gestion bases de données	par les CNRS à SCC Toulouse	t	t	2013-09-20 00:00:00	2010-01-29 00:00:00	7	\N	\N	\N	CNRS
124	2	48	2013-09-20 13:50:40	2011- SIFAC Missions - INPT	\N	t	t	2013-09-20 00:00:00	2011-01-01 00:00:00	14	\N	\N	\N	INPT
13	2	50	2013-09-16 10:42:38	2009- Initiation en droit	\N	t	t	2013-09-16 00:00:00	2009-12-10 00:00:00	7	\N	\N	\N	UPS
22	2	48	2013-09-16 11:17:26	2009- SIFAC Missions	\N	t	t	2013-09-16 00:00:00	2009-03-09 00:00:00	14	\N	\N	\N	UPS
1	2	27	2013-08-22 08:32:13	2009- Kit SPIP Contributeur	à l' IForm	t	t	2013-08-22 00:00:00	2009-04-29 00:00:00	14	\N	\N	\N	CNRS
12	2	28	2013-09-16 10:32:48	2009- EXCEL - Gérer une base de données, créer des tableaux croisés	SCC Toulouse	t	t	2013-09-16 00:00:00	2009-03-10 00:00:00	\N	\N	\N	\N	CNRS
26	2	48	2013-09-16 11:20:45	2010- SIFAC Budget	\N	t	t	2013-09-16 00:00:00	2010-03-01 00:00:00	11	\N	\N	\N	UPS
9	2	41	2013-09-16 10:30:08	2009- Recyclage Sauveteur Secours du Travail - INPT	1 journée en 2009, dates possibles :\r\n- 23/10/09\r\n- 20/11/09	t	t	2013-09-16 00:00:00	2009-10-23 00:00:00	7	\N	\N	\N	INPT
76	2	44	2013-09-20 10:03:40	2009- Zotero: un logiciel bibliographique	organisée par URFIST Toulouse	t	t	2013-09-20 00:00:00	2009-11-12 00:00:00	7	\N	\N	\N	\N
78	2	28	2013-09-20 10:09:10	2010- EXCEL 2003 : calculs avancés	par les CNRS à SCC Toulouse	t	t	2013-09-20 00:00:00	2010-01-28 00:00:00	7	\N	\N	\N	CNRS
17	2	66	2013-09-16 10:51:41	2010- Mieux comprendre la décentralisation	\N	t	t	2013-09-16 00:00:00	2010-01-14 00:00:00	7	\N	\N	\N	UPS
207	2	23	2013-10-14 10:58:16	2010- Techniciens webconférences	\N	t	t	2013-10-14 00:00:00	2010-04-07 00:00:00	14	\N	\N	\N	UPS
155	2	28	2013-10-04 14:02:03	2010- Acrobat Pro	\N	t	t	2013-10-04 00:00:00	2010-05-20 00:00:00	14	\N	\N	\N	CNRS
28	2	48	2013-09-16 11:22:21	2011- La fiscalité de l'UPS, session 2	\N	t	t	2013-09-16 00:00:00	2011-02-08 00:00:00	4	\N	\N	\N	UPS
72	2	41	2013-09-19 17:39:46	2010- Recyclage Sauveteur Secours du Travail - CNRS	Formation d'une demi journée. Il y a eu des sessions les :\r\n- 22/03/10\r\n- 28/09/10	t	t	2013-09-19 00:00:00	2010-03-22 00:00:00	4	\N	\N	\N	CNRS
212	2	40	2013-10-14 11:34:13	2010- Ethique et Déontologie	\N	t	t	2013-10-14 00:00:00	2010-02-01 00:00:00	4	\N	\N	\N	CNRS
83	2	56	2013-09-20 10:12:27	2010- Connaissances de l'UPS	12 et 14 octobre 2010	t	t	2013-09-20 00:00:00	2010-10-12 00:00:00	14	\N	\N	\N	UPS
246	2	41	2013-10-15 16:19:00	2013- SST Formation initiale - INPT	\N	t	t	2013-10-15 00:00:00	2013-06-03 00:00:00	21	\N	\N	\N	INPT
96	2	48	2013-09-20 10:35:44	2009- Puma, Cougar et marché mission	\N	t	t	2013-09-20 00:00:00	2009-03-27 00:00:00	7	\N	\N	\N	CNRS
134	2	12	2013-09-20 14:29:37	2011- 13èmes Rencontres nationales du réseau des électroniciens CNRS	du 6 au 10 juin à Fréjus	t	t	2013-09-20 00:00:00	2011-06-06 00:00:00	35	\N	\N	\N	CNRS
132	2	70	2013-09-20 14:27:55	2010- Outils Cadence : ALLEGRO PCB DESIGNER	\N	t	t	2013-09-20 00:00:00	2010-10-05 00:00:00	21	\N	\N	\N	CNRS
21	2	12	2013-09-16 11:12:49	2011- 15èmes Rencontre régionale du réseau des électroniciens DR14	au LAAS	t	t	2013-09-16 00:00:00	2011-10-20 00:00:00	7	\N	\N	\N	CNRS
154	2	62	2013-10-04 14:00:24	2009- Espagnol	Formation continue pour débutant	t	t	2013-10-04 00:00:00	2009-09-29 00:00:00	50	\N	\N	\N	UPS
148	2	48	2013-10-04 09:14:55	2009- Journée PLUME	\N	t	t	2013-10-04 00:00:00	2009-09-22 00:00:00	11	\N	\N	\N	CNRS
82	2	9	2013-09-20 10:11:30	2010- Veille et diffusion d'information sur internet avec le RSS	organisée par URFIST Toulouse	t	t	2013-09-20 00:00:00	2010-03-04 00:00:00	7	\N	\N	\N	INPT
86	2	66	2013-09-20 10:15:31	2011- Les évolutions de la Fonction Publique	\N	t	t	2013-09-20 00:00:00	2011-04-18 00:00:00	7	\N	\N	\N	UPS
335	2	70	2014-05-15 11:39:45	2014- Calculs parallèles MPI/OpenMP - CNRS	\N	t	t	2014-05-15 00:00:00	2014-06-16 00:00:00	5	\N	\N	CNRS et éducation nationale	CNRS
216	2	66	2013-10-14 12:09:51	2011- Mieux connaitre le fonctionnement de l'UE	\N	t	t	2013-10-14 00:00:00	2011-01-28 00:00:00	7	\N	\N	\N	UPS
400	2	31	2015-09-07 11:50:34	2015- Prezi : outil de création de supports de présentation orale	connaitre les fonctionnalités de base de Prezi	t	t	2015-09-07 00:00:00	2015-12-08 00:00:00	3	2015-11-07 00:00:00	\N	\N	\N
130	2	12	2013-09-20 14:23:19	2013- 17èmes Rencontre régionale du réseau des électroniciens DR14	\N	t	t	2013-09-20 00:00:00	2013-05-30 00:00:00	7	\N	\N	\N	CNRS
55	2	16	2013-09-18 17:19:55	2011- Formation Thermographie	6 et 7 septembre 2001 au 3R3 financée par le Service Instrumentation	t	t	2013-09-18 00:00:00	2011-09-06 00:00:00	14	\N	\N	\N	LAPLACE
200	2	16	2013-10-14 09:31:34	2010- Spectrométrie de masse	\N	t	t	2013-10-14 00:00:00	2010-06-16 00:00:00	21	\N	\N	\N	INPT
104	2	28	2013-09-20 10:51:52	2011- Excel - Perfectionnement	\N	t	t	2013-09-20 00:00:00	2011-04-12 00:00:00	7	\N	\N	\N	INPT
204	2	18	2013-10-14 10:15:06	2010- Méthode de conception d'un QCM	\N	t	t	2013-10-14 00:00:00	2010-01-22 00:00:00	4	\N	\N	\N	UPS
105	2	38	2013-09-20 11:00:04	2011- Equipier d'évacuation - INPT	\N	t	t	2013-09-20 00:00:00	2011-05-23 00:00:00	14	\N	\N	\N	INPT
121	2	38	2013-09-20 13:38:47	2011- Gestion des déchets chimiques - UPS	\N	t	t	2013-09-20 00:00:00	2011-04-14 00:00:00	4	\N	\N	\N	UPS
70	2	27	2013-09-19 17:37:42	2010- Kit SPIP contributeur	\N	t	t	2013-09-19 00:00:00	2010-05-31 00:00:00	14	\N	\N	\N	CNRS
232	2	60	2013-10-14 17:53:22	2011- Japonais débutant	\N	t	t	2013-10-14 00:00:00	2011-02-01 00:00:00	30	\N	\N	\N	PRES
87	2	24	2013-09-20 10:16:27	2012- HTML5/CSS3	12 au 14 mars 2012	t	t	2013-09-20 00:00:00	2012-03-12 00:00:00	21	\N	\N	\N	CNRS
80	2	57	2013-09-20 10:10:23	2010- Rédaction du rapport d'activités, session 1	2 et 15 mars 2010	t	t	2013-09-20 00:00:00	2010-03-02 00:00:00	14	\N	\N	\N	INPT
89	2	24	2013-09-20 10:17:20	2012- PHP/MySQL Niveau 2	29 au 30 mai 2012	t	t	2013-09-20 00:00:00	2012-05-29 00:00:00	14	\N	\N	\N	CNRS
131	2	24	2013-09-20 14:26:44	2009- AR14 Initiation Pic Electronicien	\N	t	t	2013-09-20 00:00:00	2009-06-17 00:00:00	42	\N	\N	\N	CNRS
118	2	27	2013-09-20 13:33:45	2009- Kit SPIP administrateur	\N	t	t	2013-09-20 00:00:00	2009-05-15 00:00:00	7	\N	\N	\N	CNRS
119	2	24	2013-09-20 13:36:18	2009- HTML XHTML CSS, session 2	\N	t	t	2013-09-20 00:00:00	2009-02-23 00:00:00	21	\N	\N	\N	CNRS
97	2	48	2013-09-20 10:36:42	2010- SIFAC Référentiel	\N	t	t	2013-09-20 00:00:00	2010-12-14 00:00:00	\N	\N	\N	\N	INPT
160	2	24	2013-10-07 16:05:03	2009- HTML XHTML CSS, session 1	\N	t	t	2013-10-07 00:00:00	2009-02-04 00:00:00	21	\N	\N	\N	CNRS
100	2	29	2013-09-20 10:47:44	2009- XLAB débutants 1er module	\N	t	t	2013-09-20 00:00:00	2009-02-26 00:00:00	21	\N	\N	\N	CNRS
75	2	44	2013-09-20 10:03:04	2009- La publication scientifique en réseau: les archives ouvertes et le libre accès	organisée par URFIST Toulouse	t	t	2013-09-20 00:00:00	2009-03-23 00:00:00	7	\N	\N	\N	\N
102	2	57	2013-09-20 10:50:06	2010- Rédaction du rapport d'activités, session 2	une demi journée (sans date)	t	t	2013-09-20 00:00:00	2010-01-01 00:00:00	4	\N	\N	\N	INPT
81	2	57	2013-09-20 10:10:54	2010- Se préparer à l'oral d'un concours	\N	t	t	2013-09-20 00:00:00	2010-05-11 00:00:00	4	\N	\N	\N	INPT
103	2	66	2013-09-20 10:50:40	2010- Mieux comprendre aujourd'hui le fonctionnement de l'UE	\N	t	t	2013-09-20 00:00:00	2010-06-18 00:00:00	4	\N	\N	\N	INPT
98	2	48	2013-09-20 10:37:46	2011- SIFAC Dépenses	27 et 28 janvier 2011	t	t	2013-09-20 00:00:00	2011-01-27 00:00:00	14	\N	\N	\N	UPS
123	2	48	2013-09-20 13:49:59	2011- SIFAC Dépenses - INPT	\N	t	t	2013-09-20 00:00:00	2011-01-01 00:00:00	14	\N	\N	\N	INPT
210	2	48	2013-10-14 11:15:26	2012- SIFAC Fiscalité - INPT	\N	t	t	2013-10-14 00:00:00	2012-02-01 00:00:00	7	\N	\N	\N	INPT
120	2	38	2013-09-20 13:37:57	2010- Risques Chimiques - Nouvel étiquetage	\N	t	t	2013-09-20 00:00:00	2010-12-03 00:00:00	7	\N	\N	\N	CNRS
114	2	24	2013-09-20 13:24:26	2012- Journées Techniques LabVIEW	\N	t	t	2013-09-20 00:00:00	2012-10-02 00:00:00	\N	\N	\N	\N	Labview
90	2	29	2013-09-20 10:18:09	2012- Catalogage des monographies imprimées session 1	26 et 27 janvier + 6 et 7 février	t	t	2013-09-20 00:00:00	2012-01-26 00:00:00	28	\N	\N	\N	Université de Toulouse
91	2	29	2013-09-20 10:19:13	2012- Catalogage des monographies imprimées session 2	\N	t	t	2013-09-20 00:00:00	2012-10-04 00:00:00	25	\N	\N	\N	Université de Toulouse
94	2	29	2013-09-20 10:21:06	2012- Horizon – Catalogage	23  octobre 2012, SICD de l'Université de Toulouse	t	t	2013-09-20 00:00:00	2012-11-23 00:00:00	7	\N	\N	\N	Université de Toulouse
95	2	29	2013-09-20 10:21:33	2012- Horizon – Bulletinage	29 et 30  octobre 2012, SICD de l'Université de Toulouse	t	t	2013-09-20 00:00:00	2012-11-29 00:00:00	14	\N	\N	\N	Université de Toulouse
93	2	29	2013-09-20 10:20:32	2012- Horizon – Acquisition initiation	18 et 19  octobre 2012, SICD de l'Université de Toulouse	t	t	2013-09-20 00:00:00	2012-10-18 00:00:00	14	\N	\N	\N	Université de Toulouse
113	2	12	2013-09-20 13:22:38	2012- Journées de l'Optique	à Cargèse	t	t	2013-09-20 00:00:00	2012-10-17 00:00:00	21	\N	\N	\N	CNRS
116	2	57	2013-09-20 13:26:24	2012- Préparation aux concours internes - CNRS	à la DR14	t	t	2013-09-20 00:00:00	2012-02-27 00:00:00	7	\N	\N	\N	\N
208	2	48	2013-10-14 11:09:33	2011- SIFAC Missions - UPS	21 et 22 mars 2011	t	t	2013-10-14 00:00:00	2011-03-21 00:00:00	14	\N	\N	\N	UPS
117	2	42	2013-09-20 13:27:06	2012- Formation Communication assistant de prévention - CNRS	à la DR 14 du 28 au 30/11/12	t	t	2013-09-20 00:00:00	2012-11-28 00:00:00	21	\N	\N	\N	CNRS
115	2	42	2013-09-20 13:25:17	2012- Formation assistant de prévention - CNRS	27 au 29 mars à Purpan et 3 au 5 avril à la DR14	t	t	2013-09-20 00:00:00	2012-03-27 00:00:00	42	\N	\N	\N	CNRS
126	2	57	2013-09-20 13:52:02	2013- Du projet professionnel au recrutement : comment réussir ?	\N	t	t	2013-09-20 00:00:00	2013-09-16 00:00:00	28	\N	\N	\N	CNRS
110	2	28	2013-09-20 11:06:14	2013- Excel Consolidations et Tableaux Croisés Dynamiques	\N	t	t	2013-09-20 00:00:00	2013-05-28 00:00:00	\N	\N	\N	\N	INPT
243	2	16	2013-10-15 16:10:45	2012- Technologie des cellules photovoltaïques organiques et hybrides	\N	t	t	2013-10-15 00:00:00	2012-10-08 00:00:00	7	\N	\N	\N	CNRS
158	2	43	2013-10-04 14:04:45	2010- Réseau des correspondants Information/Communication	1, 2, 25 et 26 mars 2010	t	t	2013-10-04 00:00:00	2010-03-01 00:00:00	28	\N	\N	\N	CNRS
108	2	57	2013-09-20 11:02:27	2011- Rédaction du rapport d'activités	13/01/11 9h-13@ l'INPT / 27/01/11 9h-12h @ l'A7 / 04/02/11 9h-12h @ l'A7	t	t	2013-09-20 00:00:00	2011-01-13 00:00:00	10	\N	\N	\N	INPT
401	2	42	2015-09-08 09:47:46	2014- Formation COMMUNICATION « Assistants de Prévention »	A l'issue de cette formation,  les stagiaires seront capables :\r\n- de connaître les principes généraux de la communication humaine et de les mettre en œuvre,\r\n- d’identifier les différentes situations de communication et s’y adapter,\r\n- développer les qualités relationnelles et de communication indispensables à l’exercice de cette fonction.\r\n\r\n13, 14,15 octobre 2014	t	t	2015-09-08 00:00:00	2014-10-13 00:00:00	21	2014-09-25 00:00:00	15	\N	\N
146	2	31	2013-10-04 08:39:51	2012- CFAO ESPRIT Fraisage 2 axes 1/2	\N	t	t	2013-10-04 00:00:00	2012-11-27 00:00:00	21	\N	\N	\N	?
147	2	31	2013-10-04 08:55:53	2013- Inventor, par le réseau des électroniciens DR14	- Public concerné : Débutants, électroniciens de la DR14\r\n- Finalité de la formation : Savoir modéliser en 3D des composants électroniques ou mécaniques avec le logiciel de CAO Autodesk Inventor Professionnel.\r\n- Objectifs pédagogiques : Les participants apprendront à modéliser des pièces et des ensembles en 3D. Comment importer et exporter des modèles 3D. Mise en plan 2D de pièce ou d’ensemble 3D. Paramétrages de base du logiciel.\r\n- Modalités : Prérequis Connaissances de l’environnement Windows et d’un outil de CAO électronique	t	t	2013-10-04 00:00:00	2013-11-12 00:00:00	21	\N	\N	\N	CNRS
213	2	59	2013-10-14 11:40:38	2010- Anglais classique	Dispensée par "Langues Promotion"	t	t	2013-10-14 00:00:00	2010-01-27 00:00:00	20	\N	\N	\N	Autre
454	2	18	2016-02-10 11:34:19	2016- Concevoir des posters photoshop et illustrator	\N	t	f	2016-02-10 00:00:00	2016-10-10 00:00:00	14	\N	\N	\N	\N
168	2	58	2013-10-10 11:07:28	2009- Formation membres des instances représentatives des personnels	\N	t	t	2013-10-10 00:00:00	2009-07-08 00:00:00	4	\N	\N	\N	UPS
170	2	56	2013-10-10 11:47:19	2009- Connaissances de l'UPS	\N	t	t	2013-10-10 00:00:00	2009-10-06 00:00:00	7	\N	\N	\N	UPS
459	2	48	2016-02-15 14:21:03	2016- CONVENTIONS	Réglementation sur les conventions	t	f	2016-02-15 00:00:00	2016-02-18 00:00:00	4	\N	\N	\N	\N
139	2	70	2013-09-27 08:54:04	2011- PSIM pour utilisateurs avancés	par PowerSys à l'N7	t	t	2013-09-27 00:00:00	2011-10-06 00:00:00	7	\N	\N	\N	CNRS
215	2	52	2013-10-14 11:50:54	2010- Conduite de réunion	\N	t	t	2013-10-14 00:00:00	2010-12-10 00:00:00	7	\N	\N	\N	UPS
163	2	38	2013-10-07 16:13:32	2012- Responsable Laser - CNRS	formation collective DR14, 18 et 19 octobre 2012	t	t	2013-10-07 00:00:00	2012-10-18 00:00:00	14	\N	\N	\N	CNRS
460	2	27	2016-03-01 09:22:42	2016- SPIP - Administrer et personnaliser un site web Labo -CNRS	\N	t	f	2016-03-01 00:00:00	2016-06-13 00:00:00	0	\N	\N	\N	\N
85	2	66	2013-09-20 10:14:47	2011- Révision Générale des Politiques Publiques	UPS, bât U4, amphi Concorde	t	t	2013-09-20 00:00:00	2011-04-04 00:00:00	7	\N	\N	\N	UPS
162	2	57	2013-10-07 16:09:10	2011- Prise de parole en public - CNRS	26, 27 septembre et 4 novembre	t	t	2013-10-07 00:00:00	2011-09-26 00:00:00	21	\N	\N	\N	CNRS
404	2	12	2015-10-12 11:17:19	2015- 18èmes Rencontre régionale du réseau des électroniciens DR14	\N	t	t	2015-10-12 00:00:00	2015-09-29 00:00:00	7	\N	\N	\N	CNRS
421	2	48	2015-10-12 17:08:54	2015- SIFAC module Dématérialisation	\N	t	t	2015-10-12 00:00:00	2015-11-01 00:00:00	7	\N	\N	\N	\N
217	2	57	2013-10-14 12:11:49	2011- Préparation aux concours internes - CNRS	\N	t	t	2013-10-14 00:00:00	2011-03-23 00:00:00	35	\N	\N	\N	CNRS
463	2	42	2016-03-23 14:39:37	2016- FORMATION INITIALE D’ASSISTANT DE PREVENTION	\N	t	f	2016-03-23 00:00:00	2016-04-05 00:00:00	37	\N	25	\N	\N
138	2	59	2013-09-27 08:52:44	2009- Anglais classique - CNRS	Cépière Formation, financée par le CNRS	t	t	2013-09-27 00:00:00	2009-04-21 00:00:00	27	\N	\N	\N	\N
149	2	26	2013-10-04 09:16:14	2010- Compilation bases de données pour la science	\N	t	t	2013-10-04 00:00:00	2010-01-01 00:00:00	7	\N	\N	\N	CNRS
150	2	24	2013-10-04 09:16:41	2010- Langage de scripts	\N	t	t	2013-10-04 00:00:00	2010-01-01 00:00:00	7	\N	\N	\N	CNRS
31	2	31	2013-09-16 11:42:07	2011- Inventor - Initiation avancée et perfectionnement	La formation d'initiation avancé s'est tenue du 21 au 25 novembre 2011 (6 participants)\r\nLa formation de perfectionnement s'est tenue du 14 au 18 novembre 2011 (4 participants)\r\nFormations faites à la DR14 dans le cadre du service mécanique	t	t	2013-09-16 00:00:00	2011-11-21 00:00:00	35	\N	\N	\N	UPS
145	2	31	2013-10-04 08:38:11	2010- Base Inventor Professionnel - CNRS	\N	t	t	2013-10-04 00:00:00	2010-01-01 00:00:00	42	\N	\N	\N	CNRS
169	2	50	2013-10-10 11:09:33	2011- La mobilité internationale des scientifiques étrangers	\N	t	t	2013-10-10 00:00:00	2011-05-31 00:00:00	4	\N	\N	\N	UPS
157	2	41	2013-10-04 14:04:06	2010- Recyclage Sauveteur Secours du Travail - UPS	Formation d'une journée. Il y a eu des sessions les :\r\n- 16/03/10\r\n- 05/01/10	t	t	2013-10-04 00:00:00	2010-03-16 00:00:00	7	\N	\N	\N	UPS
166	2	29	2013-10-10 11:06:07	2009- APOGEE 5C	\N	t	t	2013-10-10 00:00:00	2009-12-03 00:00:00	7	\N	\N	\N	UPS
228	2	41	2013-10-14 17:45:36	2011- Recyclage Sauveteur Secours du Travail - UPS	\N	t	t	2013-10-14 00:00:00	2011-05-31 00:00:00	7	\N	\N	\N	UPS
167	2	29	2013-10-10 11:06:40	2009- Logiciel APOGEE	\N	t	t	2013-10-10 00:00:00	2009-06-26 00:00:00	10	\N	\N	\N	UPS
231	2	62	2013-10-14 17:52:51	2011- Portugais débutant	\N	t	t	2013-10-14 00:00:00	2011-02-01 00:00:00	30	\N	\N	\N	PRES
165	2	29	2013-10-10 11:05:26	2009- GRAAL, session 2	\N	t	t	2013-10-10 00:00:00	2009-07-06 00:00:00	7	\N	\N	\N	UPS
164	2	29	2013-10-10 11:04:58	2009- GRAAL, session 1	\N	t	t	2013-10-10 00:00:00	2009-05-11 00:00:00	14	\N	\N	\N	UPS
137	2	41	2013-09-27 08:49:48	2009- Recyclage Sauveteur Secours du Travail - CNRS	1 journée en 2009, dates possibles :\r\n- 08/09/09\r\n- 16/03/09\r\n- 13/10/09	t	t	2013-09-27 00:00:00	2009-09-08 00:00:00	4	\N	\N	\N	CNRS
234	2	59	2013-10-14 18:00:12	2011- Anglais	CETRADEL, 82 route de bayonne	t	t	2013-10-14 00:00:00	2011-11-03 00:00:00	23	\N	\N	\N	CNRS
88	2	24	2013-09-20 10:16:57	2012- PHP/MySQL Niveau 1	23 au 26 avril 2012	t	t	2013-09-20 00:00:00	2012-04-23 00:00:00	28	\N	\N	\N	CNRS
92	2	29	2013-09-20 10:20:02	2012- Horizon – Circulation initiation	20 et 21  septembre 2012, SICD de l'Université de Toulouse	t	t	2013-09-20 00:00:00	2012-09-20 00:00:00	14	\N	\N	\N	Université de Toulouse
236	2	12	2013-10-15 14:51:26	2009- Journées de l'optique	\N	t	t	2013-10-15 00:00:00	2009-06-03 00:00:00	21	\N	\N	\N	CNRS
141	2	44	2013-09-27 08:59:29	2012- Propriété intellectuelle, propriété industrielle, brevets et valorisation des résultats de la recherche	le 30/3/12 à l'URFIST	t	t	2013-09-27 00:00:00	2012-03-30 00:00:00	7	\N	\N	\N	Université de Toulouse
153	2	23	2013-10-04 11:04:12	2013- Bonnes Pratiques Organisationnelles pour les Administrateurs Systèmes et Réseaux (ASR)	ANF organisée par la DR4	t	t	2013-10-04 00:00:00	2013-10-08 00:00:00	28	\N	\N	\N	CNRS DR4
159	2	43	2013-10-04 14:07:12	2011- Formation correspondants communication	19 et 20 septembre 2011 au relais du Bois Perché à Aspet	t	t	2013-10-04 00:00:00	2011-09-19 00:00:00	14	\N	\N	\N	CNRS
194	2	38	2013-10-11 18:08:54	2009- Manipulation des extincteurs - UPS	1/2 journée organisée par l'UPS\r\nsession possible : \r\n- 16/11/09\r\n- 19/11/09	t	t	2013-10-11 00:00:00	2009-11-16 00:00:00	4	\N	\N	\N	UPS
197	2	38	2013-10-14 08:52:25	2009- Compétences en radioprotection - CNRS	\N	t	t	2013-10-14 00:00:00	2009-02-02 00:00:00	9	\N	\N	\N	UPS
214	2	52	2013-10-14 11:48:42	2010- Bien-être, Mal-être : Le point sur les risques psychosociaux	\N	t	t	2013-10-14 00:00:00	2010-01-01 00:00:00	7	\N	\N	\N	INPT
199	2	16	2013-10-14 09:14:27	2010- Microscopie champ proche	FORMATION AUX DIFFERENTES TECHNIQUES D'IMMUNOMARQUAGE EN MICROSCOPIE ELECTRONIQUE A TRANSMISSION	t	t	2013-10-14 00:00:00	2010-05-17 00:00:00	21	\N	\N	\N	CNRS
161	2	16	2013-10-07 16:06:20	2010- Introduction à la rhéologie et à la rhéométrie	3 au 5 mai 2010 au Mans	t	t	2013-10-07 00:00:00	2010-05-03 00:00:00	17	\N	\N	\N	?
201	2	5	2013-10-14 09:34:06	2010- Atelier chimie poudre immergé dans plasma	\N	t	t	2013-10-14 00:00:00	2010-07-07 00:00:00	21	\N	\N	\N	CNRS
439	2	48	2015-10-12 22:15:22	2015- SIFAC Missions	\N	t	t	2016-02-10 00:00:00	2015-11-01 00:00:00	\N	\N	\N	\N	\N
180	2	49	2013-10-10 16:41:32	2011- Information et complément de formation des correspondants achats	\N	t	t	2013-10-10 00:00:00	2011-05-19 00:00:00	4	\N	\N	\N	UPS
249	2	16	2013-10-15 16:25:48	2013- Conception d'installations sous vide	organisée par la délégation IdF CNRS, Gif sur Yvette	t	t	2013-10-15 00:00:00	2013-06-25 00:00:00	28	\N	\N	\N	CNRS
18	2	71	2013-09-16 10:55:39	2011- Réunion des correspondants formation du CNRS DR14	à l'Hotel Grand Orléans, Toulouse	t	t	2013-09-16 00:00:00	2011-10-18 00:00:00	7	\N	\N	\N	CNRS
192	2	71	2013-10-11 17:38:39	2009- Formation des correspondants formation - CNRS	\N	t	t	2013-10-11 00:00:00	2009-03-18 00:00:00	21	\N	\N	\N	CNRS
64	2	12	2013-09-19 16:14:10	2010- Journée Réseau Plasmas Froids	Le Réseau des Plasmas Froids du CNRS organise les 1ères Journées Plasmas Quebec France\r\n(Tarn)	t	t	2013-09-19 00:00:00	2010-06-01 00:00:00	28	\N	\N	\N	CNRS
188	2	27	2013-10-11 16:31:11	2009- Découverte de Moodle	1/2 journée parmi les créneaux :\r\n08/01/09\r\n14/01/09\r\n15/10/09	t	t	2013-10-11 00:00:00	2009-01-08 00:00:00	4	\N	\N	\N	UPS
133	2	42	2013-09-20 14:28:34	2010- Membre CHS	\N	t	t	2013-09-20 00:00:00	2010-06-03 00:00:00	35	\N	\N	\N	CNRS
136	2	12	2013-09-27 08:47:21	2009- 13èmes Rencontres régionales du réseau des électroniciens DR14	à l'INSA	t	t	2013-09-27 00:00:00	2009-02-01 00:00:00	4	\N	\N	\N	CNRS
186	2	16	2013-10-11 16:15:43	2009- Conception des convertisseurs d'énergie	13 au 15 octobre 2009	t	t	2013-10-11 00:00:00	2009-10-13 00:00:00	21	\N	\N	\N	CNRS
205	2	18	2013-10-14 10:18:38	2010- Produire et exploiter des QCM en ligne	\N	t	t	2013-10-14 00:00:00	2010-01-05 00:00:00	4	\N	\N	\N	UPS
202	2	12	2013-10-14 09:45:35	2010- 12èmes Rencontres nationales du réseau des électroniciens CNRS	\N	t	t	2013-10-14 00:00:00	2010-05-31 00:00:00	35	\N	\N	\N	CNRS
127	2	12	2013-09-20 14:08:41	2010- 14èmes Rencontre régionale du réseau des électroniciens DR14	11 et 12 octobre 2010 à Aspet	t	t	2013-09-20 00:00:00	2010-10-11 00:00:00	14	\N	\N	\N	CNRS
3	2	71	2013-08-22 08:44:42	2010- Réunion des correspondants formation du CNRS DR14	Au "Grand Hôtel d'Orléans", Toulouse	t	t	2013-08-22 00:00:00	2010-10-09 00:00:00	7	\N	\N	\N	CNRS
367	2	44	2014-10-09 13:53:16	2014- Le droit à l'image appliqué à la production scientifique	\N	t	t	2014-11-24 00:00:00	2014-11-24 00:00:00	12	2014-10-10 00:00:00	\N	\N	\N
184	2	49	2013-10-11 15:20:26	2010- Achat public : la gestion des imprévus en phase d'exécution	\N	t	t	2013-10-11 00:00:00	2010-02-11 00:00:00	4	\N	\N	\N	UPS
177	2	48	2013-10-10 13:51:49	2010- SIFAC Convention	\N	t	t	2013-10-10 00:00:00	2010-04-29 00:00:00	14	\N	\N	\N	UPS
183	2	49	2013-10-11 15:19:58	2010- Achat public : les marchés négociés et la pratique de la négociation	\N	t	t	2013-10-11 00:00:00	2010-01-14 00:00:00	4	\N	\N	\N	UPS
173	2	49	2013-10-10 12:00:26	2009- Achat public : Formation à l'outil "Achatpublic.com"	\N	t	t	2013-10-10 00:00:00	2009-09-24 00:00:00	4	\N	\N	\N	UPS
171	2	49	2013-10-10 11:57:31	2009- Mettre en œuvre les principes généraux de l'achat public à l'UPS	\N	t	t	2013-10-10 00:00:00	2009-09-10 00:00:00	4	\N	\N	\N	UPS
156	2	28	2013-10-04 14:02:48	2010- Environnement administratif des BU	Journée d'etude:  le nouvel environnement administratif des bibliothèques universitaires - BU,LRU, RCE, PRES qu'est ce qui change	t	t	2013-10-04 00:00:00	2010-10-11 00:00:00	7	\N	\N	\N	?
27	2	48	2013-09-16 11:21:58	2011- La fiscalité de l'UPS, session 1	\N	t	t	2013-09-16 00:00:00	2011-01-07 00:00:00	4	\N	\N	\N	UPS
211	2	41	2013-10-14 11:19:14	2010- SST Formation initiale - UPS	\N	t	t	2013-10-14 00:00:00	2010-06-23 00:00:00	14	\N	\N	\N	UPS
24	2	48	2013-09-16 11:19:04	2009- SIFAC Référentiel	\N	t	t	2013-09-16 00:00:00	2009-02-03 00:00:00	4	\N	\N	\N	UPS
174	2	48	2013-10-10 12:01:18	2009- SIFAC Edition - Consultation	\N	t	t	2013-10-10 00:00:00	2009-10-20 00:00:00	4	\N	\N	\N	\N
187	2	28	2013-10-11 16:25:24	2009- Méthode de conception d'un QCM	\N	t	t	2013-10-11 00:00:00	2009-07-06 00:00:00	4	\N	\N	\N	UPS
181	2	49	2013-10-10 16:51:49	2009- Achat public : Formation à l'outil "Achatpublic.com", session 3	\N	t	t	2013-10-10 00:00:00	2009-05-19 00:00:00	4	\N	\N	\N	UPS
11	2	28	2013-09-16 10:32:21	2009- EXCEL - Se perfectionner dans les calculs, lier les tableaux, faire des simulations	SCC Toulouse	t	t	2013-09-16 00:00:00	2009-03-09 00:00:00	7	\N	\N	\N	CNRS
101	2	29	2013-09-20 10:48:47	2009- XLAB débutants 2ème module	16 et 17 mars 2009	t	t	2013-09-20 00:00:00	2009-03-16 00:00:00	14	\N	\N	\N	CNRS
206	2	68	2013-10-14 10:44:23	2010- Plongée sous-marine niveau 2	7,5 jours  partir du 17/02/10 et 5 jours à partir du 26/09/10	t	t	2013-10-14 00:00:00	2010-02-17 00:00:00	88	\N	\N	\N	Autre
189	2	42	2013-10-11 17:15:50	2009- Formation initiale ACMO - CNRS	\N	t	t	2013-10-11 00:00:00	2009-10-02 00:00:00	7	\N	\N	\N	CNRS
77	2	28	2013-09-20 10:08:34	2010- Word : création de documents longs et structurés	organisée par URFIST Toulouse	t	t	2013-09-20 00:00:00	2010-01-18 00:00:00	7	\N	\N	\N	\N
179	2	48	2013-10-10 16:36:15	2011- La fiscalité de l'UPS, session 3	1 journée le 18/01/2011 ou le 21/01/2011	t	t	2013-10-10 00:00:00	2011-01-18 00:00:00	7	\N	\N	\N	UPS
175	2	49	2013-10-10 13:47:41	2009- Achat public : Le DCE/les éléments et pièces d'un marché	\N	t	t	2013-10-10 00:00:00	2009-10-22 00:00:00	4	\N	\N	\N	UPS
209	2	49	2013-10-14 11:14:29	2012- ACHAT PUBLIC - Animation Réseau	\N	t	t	2013-10-14 00:00:00	2012-01-26 00:00:00	3	\N	\N	\N	UPS
172	2	49	2013-10-10 11:59:50	2009- Achat public : la procédure adaptée ou simplifiée	\N	t	t	2013-10-10 00:00:00	2009-09-17 00:00:00	4	\N	\N	\N	UPS
56	2	41	2013-09-18 17:26:56	2010- Recyclage Sauveteur Secours du Travail - INPT	Recyclage d'une journée\r\nDates possibles :\r\n- 15/10/10\r\n- 20/11/10	t	t	2013-09-18 00:00:00	2010-10-15 00:00:00	7	\N	\N	\N	INPT
182	2	49	2013-10-10 16:58:09	2009- Achat public : Les éléments et pièces dans le cadre de l'analyse	\N	t	t	2013-10-10 00:00:00	2009-12-17 00:00:00	4	\N	\N	\N	UPS
185	2	49	2013-10-11 15:20:52	2010- Achat public : formation complémentaire ordonnance	\N	t	t	2013-10-11 00:00:00	2010-05-11 00:00:00	4	\N	\N	\N	UPS
178	2	49	2013-10-10 13:52:26	2010- Achat public : Formation à l'outil "Achatpublic.com"	\N	t	t	2013-10-10 00:00:00	2010-05-11 00:00:00	2	\N	\N	\N	UPS
176	2	49	2013-10-10 13:48:43	2009- Achat public : Formation à l'outil "Achatpublic.com", session 2	\N	t	t	2013-10-10 00:00:00	2009-11-26 00:00:00	4	\N	\N	\N	UPS
218	2	16	2013-10-14 14:51:05	2011- Conception et utilisation d'un réacteur a plasma de type industriel	par le réseau plasma froid	t	t	2013-10-14 00:00:00	2011-09-28 00:00:00	21	\N	\N	\N	CNRS
220	2	12	2013-10-14 14:56:05	2011- Séminaire Réseau Plasmas Froids	24 au 27 mai 2011 à Toulouse	t	t	2013-10-14 00:00:00	2011-05-24 00:00:00	28	\N	\N	\N	CNRS
456	2	30	2016-02-10 11:38:12	2016- Les altmetrics	\N	t	f	2016-02-10 00:00:00	2016-05-12 00:00:00	3	\N	\N	\N	\N
222	2	24	2013-10-14 15:27:41	2011- Gestion et contribution de contenus avec SharePoint 2010	à 'I'Form	t	t	2013-10-14 00:00:00	2011-10-11 00:00:00	28	\N	\N	\N	CNRS
84	2	29	2013-09-20 10:13:31	2011- Formation Archives ouvertes HAL	9h à 12h30, salle de la Rotonde, Délégation CNRS Midi Pyrénées	t	t	2013-09-20 00:00:00	2011-01-09 00:00:00	4	\N	\N	\N	CNRS
250	2	42	2013-10-15 16:27:23	2013- Formation assistant de prévention	DR14	t	t	2013-10-15 00:00:00	2013-04-26 00:00:00	7	\N	\N	\N	CNRS
252	2	57	2013-10-15 16:30:50	2013- Préparation à la retraite	\N	t	t	2013-10-15 00:00:00	2013-11-05 00:00:00	21	\N	\N	\N	CNRS
225	2	42	2013-10-14 17:40:24	2011- Communication au profit des ACMO	16 au 18 mai 2011	t	t	2013-10-14 00:00:00	2011-05-16 00:00:00	21	\N	\N	\N	CNRS
244	2	38	2013-10-15 16:12:05	2012- Sécurité Laser	au CEMES par la DR14	t	t	2013-10-15 00:00:00	2012-11-26 00:00:00	10	\N	\N	\N	CNRS
226	2	28	2013-10-14 17:43:58	2011- Word Perfectionnement	dispensée par JCM Solutions	t	t	2013-10-14 00:00:00	2011-07-04 00:00:00	7	\N	\N	\N	UPS
229	2	41	2013-10-14 17:46:54	2011- Recyclage Sauveteur Secours du Travail - CNRS	\N	t	t	2013-10-14 00:00:00	2011-03-22 00:00:00	7	\N	\N	\N	CNRS
461	2	42	2016-03-01 11:38:05	2016- Formation initiale Assistant prévention - CNRS	du 5 au 7 avril et du 11 au 13 avril	t	f	2016-03-01 00:00:00	2016-04-05 00:00:00	42	\N	\N	\N	CNRS
464	2	54	2016-03-25 10:12:41	2016- Encadrement de thèse	\N	t	f	2016-03-25 00:00:00	2016-05-23 00:00:00	\N	2016-04-29 00:00:00	12	CNRS	UPS
336	2	38	2014-05-15 13:58:47	2014- Sécurité incendie et manipulation des extincteurs - UPS	\N	t	t	2014-05-15 00:00:00	2014-05-13 00:00:00	\N	\N	\N	UPS	UPS
320	2	28	2014-04-18 08:51:09	2014- EXCEL INITIATION AUX MACRO COMMANDES -INPT	\N	t	t	2014-04-18 00:00:00	2014-06-10 00:00:00	1	2014-04-16 00:00:00	\N	\N	INPT
227	2	41	2013-10-14 17:45:13	2011- Recyclage Sauveteur Secours du Travail - INPT	Recyclage d'une journée\r\nDates possibles du recyclage : \r\n- 29/09/2011\r\n- 21/10/2011	t	t	2013-10-14 00:00:00	2011-10-21 00:00:00	7	\N	\N	\N	INPT
235	2	38	2013-10-14 18:04:26	2011- Hygiène et sécurité - Nouveaux entrants - INPT	\N	t	t	2013-10-14 00:00:00	2011-11-30 00:00:00	7	\N	\N	\N	INPT
152	2	25	2013-10-04 11:03:12	2012- Cours NetApp Fondamentaux d'administration	du 31/01 au 03/02	t	t	2013-10-04 00:00:00	2012-01-30 00:00:00	28	\N	\N	\N	CNRS-LAPLACE SC Info
370	2	8	2014-10-24 12:09:32	2014- Thermique pour le dimensionnement de dissipateurs	8h de cours au département GEA de l'N7, parcours EMEC	t	t	2014-10-24 00:00:00	2014-02-01 00:00:00	\N	\N	\N	\N	N7
237	2	8	2013-10-15 14:54:07	2012- Méthodes numériques pour les problèmes d'optimisation	9 au 11 mai 2012, formation organisée par le CERFACS	t	t	2013-10-15 00:00:00	2012-05-09 00:00:00	21	\N	\N	\N	CODIASE
238	2	12	2013-10-15 14:56:07	2012- Atelier analyse micro-structurale des couches minces	Réseau métier – Plasma Froid\r\ndu 29 au 30 mai à l'institut européen des membranes (Montpellier)	t	t	2013-10-15 00:00:00	2012-05-29 00:00:00	14	\N	\N	\N	CNRS
140	2	16	2013-09-27 08:57:20	2012- CEM des mesures physiques	16 au 18 octobre 2012 AEMC	t	t	2013-09-27 00:00:00	2012-10-16 00:00:00	21	\N	\N	\N	CNRS / LAPLACE
240	2	25	2013-10-15 15:26:07	2012- Formation Netasq	26 au 28 mars\r\nà la DR14	t	t	2013-10-15 00:00:00	2012-03-26 00:00:00	21	\N	\N	\N	CNRS
239	2	41	2013-10-15 15:17:40	2012- Recyclage Sauveteur Secours du Travail - CNRS	à la DR14 la 10/05/12 ou le 06/06/12	t	t	2013-10-15 00:00:00	2012-05-10 00:00:00	7	\N	\N	\N	CNRS
219	2	16	2013-10-14 14:55:33	2011- Electronique et instrumentation pour le vivant	6 au 11 juin 2011 à Fréjus (ANGD)	t	t	2013-10-14 00:00:00	2011-06-06 00:00:00	35	\N	\N	\N	CNRS
191	2	31	2013-10-11 17:34:46	2013- Journée "Découverte" du logiciel INVENTOR	Formation d'une journée d'initiation au logiciel INVENTOR\r\nFormation faite en interne par Dominique Harribey\r\n2 sessions d'une journée pour 6 personnes, dates :\r\n- 11/12/13\r\n- 18/12/13	t	t	2013-10-11 00:00:00	2013-12-11 00:00:00	7	\N	12	\N	Interne LAPLACE (gratuit)
422	2	48	2015-10-12 17:11:07	2011- Formation plateforme réservation en ligne Carlson	\N	t	t	2015-10-12 00:00:00	2011-11-01 00:00:00	\N	\N	\N	\N	\N
196	2	12	2013-10-14 08:47:56	2009- 11èmes Rencontres nationales du réseau des électroniciens CNRS	"L'électronique en conditions extrêmes" à Roscoff du 25 au 29 mai 2009	t	t	2013-10-14 00:00:00	2009-05-25 00:00:00	35	\N	\N	\N	CNRS
440	2	48	2015-10-12 22:24:47	2014- SIFAC Missions	\N	t	t	2015-10-12 00:00:00	2014-11-01 00:00:00	\N	\N	\N	\N	\N
442	2	42	2015-10-19 15:10:16	2015- Journée d’échanges entre les Assistants de Prévention - CNRS	Présentation de NEO 2. \r\nPoint EvRP, RATIS\r\n\r\n3. Plan de Prévention 2016\r\n4. Retour d’expériences\r\n- Accident dans le cadre du transport des matières dangereuses - Exercice de gestion de crise	t	t	2015-10-20 00:00:00	2015-12-07 00:00:00	7	2015-11-19 00:00:00	\N	Aps du CNRS	Aucun
33	2	59	2013-09-17 16:04:38	2011- Anglais débutant, module de 30h - PRES	AC.1 bis Anglais élémentaire 1° choix	t	t	2013-09-17 00:00:00	2011-09-17 00:00:00	30	\N	\N	\N	PRES
443	2	27	2015-11-02 11:44:25	2015- Photoshop CS6	\N	t	t	2015-11-04 00:00:00	2015-11-24 00:00:00	21	2015-11-14 00:00:00	14	\N	\N
271	2	31	2013-12-19 11:06:34	2014- logiciel ESPRIT 4eme axe	2 eme formation au logiciel esprit pour utilisation du 4eme axe	t	t	2013-12-19 00:00:00	2014-01-11 00:00:00	21	\N	3	\N	\N
230	2	41	2013-10-14 17:49:57	2011- Recyclage Sauveteur Secours du Travail (2) - UPS	1 journée le 14/04/11 ou le 27/05/11	t	t	2013-10-14 00:00:00	2011-04-14 00:00:00	7	\N	\N	\N	UPS
299	2	12	2014-03-04 15:43:54	2013- Fonctionnalisation surface par plasma - CNRS	\N	t	t	2014-03-04 00:00:00	2013-06-12 00:00:00	21	\N	\N	\N	CNRS
292	2	16	2014-03-04 15:07:58	2013- Séminaire sur les processeur ARM - CNRS	\N	t	t	2014-03-04 00:00:00	2013-11-25 00:00:00	21	\N	\N	\N	CNRS
223	2	38	2013-10-14 15:54:24	2011- Habilitation électrique du personnel non électricien - INPT	1ère session : 21 et 22 mars 2011 (5 participants)\r\n2ème session : 18 et 21 novembre 2011 (7 participants)\r\n\r\nFormation effectuée sur le site N7 avec Pierre Rossi	t	t	2013-10-14 00:00:00	2011-03-21 00:00:00	14	\N	\N	\N	INPT
362	2	38	2014-10-07 15:57:51	2012- Habilitation électrique du personnel non électricien - INPT	session les 4 et 5 avril 2012\r\n\r\nFormation effectuée sur le site N7 avec Pierre Rossi	t	t	2014-10-07 00:00:00	2012-04-04 00:00:00	14	\N	\N	\N	INPT-N7
453	2	28	2016-02-10 11:33:08	2016- Prendre en main l'environnement ADOBE	\N	t	f	2016-02-10 00:00:00	2016-03-29 00:00:00	\N	\N	\N	\N	\N
245	2	31	2013-10-15 16:13:47	2012- Inventor - Initiation avancée	à la DR14, organisée par Boulanger	t	t	2013-10-15 00:00:00	2012-11-20 00:00:00	21	\N	\N	\N	CNRS
376	2	42	2014-12-02 17:28:19	2014- Journée des assistants de prévention - CNRS	Journée annuelle réunissant les assistantes de prévention de la DR14.	t	t	2014-12-03 00:00:00	2014-12-04 00:00:00	7	\N	\N	Les assistants de prévention	CNRS
379	2	54	2015-01-13 16:38:56	2015- Encadrer un doctorant à l’étranger - CNRS	\N	t	t	2015-01-13 00:00:00	2015-02-02 00:00:00	12	2015-01-16 00:00:00	12	Tout agent étant amené à encadrer un doctorant à l’étranger	CNRS
5	2	38	2013-08-22 08:54:57	2012- Recyclage de l’Habilitation électrique du personnel non électricien - INPT	Recyclage de l’Habilitation électrique du personnel non électricien fait à l'N7 par Pierre Rossi\r\nDates possibles : \r\n- 25/05/2012\r\n- 22/06/2012	t	t	2013-08-22 00:00:00	2012-05-25 00:00:00	7	\N	\N	\N	INPT-N7
423	2	31	2015-10-12 17:15:00	2014- Inventor	\N	t	t	2015-10-12 00:00:00	2014-01-01 00:00:00	7	\N	\N	\N	interne LAPLACE
293	2	42	2014-03-04 15:12:58	2013- Membres CHSCT session1 - CNRS	\N	t	t	2014-03-04 00:00:00	2013-04-10 00:00:00	21	\N	\N	\N	CNRS
470	2	42	2016-05-11 08:30:49	2016- Formation à l'outil EVRP - CNRS	1- Présentation de l’outil CNRS EVRP dans l’objectif d’établir le document unique\r\n2- Les mises à jour du document unique dans EVRP	t	f	2016-05-12 00:00:00	2016-05-31 00:00:00	8	2016-05-17 00:00:00	\N	Les assistants de prévention	CNRS
381	2	72	2015-04-02 10:23:44	2015- L'image numérique : création, traitement spécificités	\N	t	t	2015-04-02 00:00:00	2015-01-01 00:00:00	\N	\N	\N	\N	\N
462	2	41	2016-03-01 11:41:20	2016- Recyclage SST- UPS	Lieu : IUT A , 137 avenue de Rangueil (salle du conseil du GCGP au rez- de chaussée).\r\n \r\nHoraires : 8h30-12h / 13h-16h30	t	f	\N	2016-03-15 00:00:00	6	\N	\N	\N	UPS
371	2	38	2014-10-27 11:30:50	2014- Habilitation électrique des personnels non électricien (session novembre) -INPT	Session Habilitation électrique prévue les  6 et 7 Novembre 2014\r\nSalle E422 de 9H00  à 17H00 avec une pause déjeuner 12/13H.\r\nSession assurée par Mr Sorin DINCULESCU.	t	t	2014-11-24 00:00:00	2014-11-06 00:00:00	14	\N	12	\N	\N
247	276	4	2013-10-15 16:22:00	Thermo-Mécanique	dispensée par Epsilon Alcen	t	t	2013-10-15 00:00:00	2013-04-15 00:00:00	7	\N	\N	\N	tutelles respectives
257	2	54	2013-10-17 10:39:58	2013- Formation à l'encadrement de thèse	Le rôle de tuteur :\r\n· Rappel du cadre réglementaire\r\n· Formalisation du rôle de l’encadrant\r\n· Quelques clés pour le recrutement\r\n· Accueil dans l’équipe et prise de fonction\r\n· Outil d’évaluation des compétences tout au long de la thèse\r\n\r\nAccompagnement du projet de recherche :\r\n· Méthodes de planification et de fixation d'objectifs\r\n· Réunion d’avancement de projet\r\n· Outil de suivi des risques du projet\r\n\r\nGestion de la relation :\r\n· Les styles d’apprentissages\r\n· Les styles de management\r\n· Quelques théories de la motivation et leur application tout au long de la thèse\r\n· Méthode de résolution de problème utile lors de la gestion des aléas\r\n· Prévention et gestion des conflits éventuels.\r\n\r\nFacilitation du projet professionnel\r\n· Quelques éléments d’accompagnement vers l’emploi\r\n· Mobilisation du réseau de l’encadrant\r\n· Connaissances des métiers ouverts aux scientifiques\r\n· Aide à la formalisation du projet professionnel du doctorant\r\n\r\nFormation destinée à tout agent étant amené à encadrer un doctorant\r\n\r\nPour en savoir plus : http://www.cnrs.fr/midi-pyrenees/FormationPermanente/Programmes/2013/2013-10-16AnnonceEncadrementThese.pdf\r\n\r\nInscription en remplissant le formulaire : http://intranet.laplace.univ-tlse.fr/spip.php?article311\r\nen l’envoyant à Laurence Neuville : Laurence.Neuville@dr14.cnrs.fr\r\nen mettant en copie le correspondant formation : correspondants-formation@laplace.univ-tlse.fr	t	t	2013-10-17 00:00:00	2013-12-03 00:00:00	14	2013-11-22 00:00:00	12	tout agent étant amené à encadrer un doctorant	CNRS
441	2	41	2015-10-12 22:39:04	2014- Recyclage Sauveteur Secours du Travail	\N	t	t	2015-10-12 00:00:00	2014-01-01 00:00:00	\N	\N	\N	\N	\N
251	2	16	2013-10-15 16:29:44	2013- Concevoir son alimentation : la partie Puissance	Séminaire proposé par Captronic au LAAS	t	t	2013-10-15 00:00:00	2013-06-19 00:00:00	7	\N	\N	\N	CAPTRONIC (gratuit)
241	2	38	2013-10-15 15:38:01	2012- Transport de produits chimiques	\N	t	t	2013-10-15 00:00:00	2012-01-09 00:00:00	4	\N	\N	\N	inconnu
466	2	57	2016-04-12 11:08:41	2016- Se préparer à l'épreuve orale d'un concours ou examen professionnel - UPS	Savoir mettre en évidence ses compétences et présenter au jury les points forts de sa carrière avec aisance et assurance	t	f	2016-04-13 00:00:00	2016-05-09 00:00:00	16	2016-04-15 00:00:00	12	\N	\N
300	2	12	2014-03-04 15:48:40	2013- Journée Réseau Plasmas Froids - CNRS	à La Rochelle	t	t	2014-10-02 00:00:00	2013-10-14 00:00:00	21	\N	\N	\N	CNRS
144	2	12	2013-10-03 11:28:42	2013- Journée thématique "Matériaux"	Journée organisée dans le cadre du réseau interrégional des mécaniciens LR-MP\r\n•\tIntroduction générale sur les matériaux\r\n•\tProcédés de fabrication\r\n•\tUsinabilité des matériaux (céramiques, titane, molybdène, inox réfractaires ...)\r\n\r\nCette formation est destinée aux mécaniciens du laboratoire.\r\n\r\nPour en savoir plus : \r\nhttp://www.cnrs.fr/midi-pyrenees/FormationPermanente/Programmes/2013/2013-09-25AnnonceProg-V5-Mat.pdf\r\n\r\nInscription en ligne possible :\r\nhttp://www.dr14.cnrs.fr/midi-pyrenees/FormationPermanente/InfosInscriptions.aspx\r\nou en remplissant le formulaire :\r\nhttp://intranet.laplace.univ-tlse.fr/spip.php?article311 \r\nen l’envoyant à Roxane Castanet : \r\nRoxane.Castanet@dr14.cnrs.fr \r\net en mettant en copie le correspondant formation :\r\ncorrespondants-formation@laplace.univ-tlse.fr	t	t	2013-10-15 00:00:00	2013-11-20 00:00:00	7	\N	\N	\N	CNRS
375	2	48	2014-11-20 16:05:27	2014- SIFAC IMMOBILISATIONS METIER/OUTIL	IMMOBILISATIONS METIER/OUTIL SUR SIFAC	t	t	2014-11-20 00:00:00	2014-11-24 00:00:00	12	2014-11-20 00:00:00	12	\N	\N
471	2	15	2016-05-12 15:38:52	2016- Initiation au soudage oxyacétylene	\N	t	f	2016-05-12 00:00:00	2016-06-20 00:00:00	35	\N	\N	\N	UPS
472	2	54	2016-05-17 09:24:35	2015 - le management pour les responsables d’équipes et les chefs de service - CNRS	module 1: le rôle du responsable d’équipe\r\nmodule 2: délégation et négociation\r\nmodule 3: communication et techniques d'entretien	t	t	2016-05-17 00:00:00	2015-09-08 00:00:00	20	\N	\N	\N	CNRS
473	2	68	2016-05-31 10:51:10	2016- L'environement externe et interne de l'INPT	formation culture d'établissement et de l'ES	t	f	2016-05-31 00:00:00	2016-05-13 00:00:00	1	\N	\N	\N	INP
383	2	72	2015-04-28 14:07:39	2015- Powerpoint : concevoir une présentation réussie et efficace	\N	t	t	2015-04-28 00:00:00	2015-04-07 00:00:00	11	\N	\N	\N	\N
377	2	71	2014-12-18 15:58:38	2014- Réunion des correspondants formation du CNRS (DR14) - CNRS	\N	t	t	2014-12-18 00:00:00	2014-11-14 00:00:00	7	\N	\N	\N	CNRS
318	2	72	2014-04-18 08:50:05	2014- POWERPOINT PERFECTIONNEMENT -INPT	\N	t	t	2014-04-18 00:00:00	2014-06-23 00:00:00	1	2014-04-16 00:00:00	\N	\N	INPT
248	2	54	2013-10-15 16:24:36	2013- Le management pour les responsables d'équipes et les chefs d'équipes	\N	t	t	2013-10-15 00:00:00	2013-04-16 00:00:00	35	\N	\N	\N	CNRS
387	2	12	2015-05-05 13:46:22	2015- Conception d'une carte électronique dite "high speed"	Acquisition de savoir faire pour le routage de pistes d'un circuit imprimé avec des signaux rapides prise en compte des problématiques : CEM, intégrité du signal, comportement thermique, densité de placement.\r\nSimulation du comportement du circuit imprimé sous SPICE. Création du modèle, simulation fréquentielle, temporelle, impact du bruit.	t	t	2015-05-05 00:00:00	2015-06-03 00:00:00	14	\N	\N	CNRS	Réseau des électroniciens
382	2	18	2015-04-02 10:25:18	2015- Publication multimédia avec indesign	URFIST	t	t	2015-04-02 00:00:00	2015-03-26 00:00:00	13	\N	\N	\N	\N
390	2	25	2015-05-20 13:58:18	2015- Journées de développement logiciel (Jdev)	"Le développement de l'embarqué, des systèmes mobiles, de l'internet des objets et des capteurs ouvre des possibles inattendus pour la science avec ces nouveaux moyens d'acquisition de données et d'expérimentation. Nous traiterons de l'ingénierie des systèmes pour faciliter la spécification, la conception et le développement de ces systèmes scientifiques.\r\n\r\nEn cette époque de ruée vers la donnée, les données massives (big data) résultant de l'explosion des capteurs, de l'open-data, de la complexité et de l'interdisciplinarité des recherches actuelles seront à l'honneur.\r\n\r\nNous aborderons les systèmes d'information collaboratifs à destination des communautés de recherche à l'ère de l'internet telles que les clouds de la recherche et les infrastructures basées sur les services web. Nous verrons en quoi ces solutions permettent de décloisonner les disciplines par une fertilisation croisée inter-disciplinaire et la combinaison à loisir des ressources de données et de calcul pour l'émergence de nouveaux objets de recherche. Nous ouvrirons la voie à des services de haute valeur ajoutée soit en données, soit en calcul.\r\n\r\nNous ferons le point sur l'état de l'art et les tendances dans le domaine des outils et des environnements de développement, des langages, leur paradigme et leur éco-système.\r\n\r\nNous réserverons une place particulière pour la mise en œuvre de la simulation scientifique par les langages, par les prologiciels et par la modélisation.\r\n\r\nLes transferts marchand et non marchand liés directement ou portés par le logiciel seront également à l'ordre du jour."	t	t	2015-05-20 00:00:00	2015-06-30 00:00:00	30	\N	250	\N	\N
455	2	30	2016-02-10 11:35:48	2016- Twitter	\N	t	f	2016-02-10 00:00:00	2016-06-02 00:00:00	4	\N	\N	\N	\N
384	2	72	2015-04-28 14:09:29	2015- Infographie : identité visuelle et chartre graphique	\N	t	t	2015-04-28 00:00:00	2015-09-21 00:00:00	14	\N	\N	\N	\N
424	2	38	2015-10-12 17:17:11	2014- Habilitation électrique des personnels non électricien (session février) -UPS	\N	t	t	2015-10-12 00:00:00	2014-02-24 00:00:00	7	\N	\N	\N	\N
378	2	12	2014-12-18 16:00:52	2014- Rencontre nationales du réseau des électroniciens du CNRS	à Nailloux	t	t	2014-12-18 00:00:00	2014-11-03 00:00:00	49	\N	\N	\N	CNRS
467	2	72	2016-04-12 16:23:53	2016- Photoshop : maitriser les bases - CNRS	\N	t	f	2016-04-13 00:00:00	2016-05-12 00:00:00	14	\N	\N	\N	formation CNRS
474	2	68	2016-05-31 10:52:47	2016- Le rayonnement de l'INPT	Formation culture d'établissement et de l'enseignement supérieur RI COM SCD	t	f	2016-05-31 00:00:00	2016-03-31 00:00:00	1	\N	\N	\N	\N
405	2	25	2015-10-12 16:15:17	2014- Réunion d'information sur les versions des logiciels et OS, et le chiffrement des postes	\N	t	t	2015-10-12 00:00:00	2014-01-01 00:00:00	7	\N	\N	\N	\N
388	2	70	2015-05-12 17:24:57	2015- Altium Spice	\N	t	t	2015-05-12 00:00:00	2015-06-03 00:00:00	8	2015-05-15 00:00:00	6	\N	\N
385	2	72	2015-04-28 14:10:35	2015- Photoshop et Indesign : concevoir des posters	\N	t	t	2015-04-28 00:00:00	2015-10-05 00:00:00	14	\N	\N	\N	\N
457	2	38	2016-02-10 14:20:21	2016- Maintenance des installations électriques	\N	t	f	2016-02-10 00:00:00	2016-02-03 00:00:00	35	2016-02-09 00:00:00	10	\N	UPS-FSI
425	2	21	2015-10-12 17:30:36	2014- Commission Informatique et Liberté	\N	t	t	2015-10-12 00:00:00	2014-01-01 00:00:00	7	\N	\N	\N	\N
468	2	18	2016-04-12 16:25:47	2016- concevoir et déployer un plan de communication	\N	t	f	2016-04-13 00:00:00	2016-06-20 00:00:00	14	2016-05-27 00:00:00	12	chargés de communication	\N
294	2	42	2014-03-04 15:13:21	2013- Membres CHSCT session 2 - CNRS	\N	t	t	2014-03-04 00:00:00	2013-05-06 00:00:00	14	\N	\N	\N	CNRS
269	2	38	2013-12-03 16:32:15	2013- Habilitation électrique du personnel non électricien - INPT	1ère session : 10 et 11 janvier 2013 sur le site N7 avec Pierre Rossi\r\n\r\n2ème session sur le site N7 avec Pierre Rossi :\r\nPour tous les nouveaux entrants du Laboratoire LAPLACE  sur le site N7, une session Habilitation  électrique est prévue les 18 et 19 Décembre 2013. Cette formation se déroulera sur le site N7 Salle  E422. Les Horaires sont 8h30-11h30 et 13h00-16h00.\r\n\r\nCette habilitation est obligatoire.\r\n \r\nImpératif : Envoyer un mail de confirmation de votre présence pour une bonne organisation de la session à Robert Larroche : larroche@laplace.univ-tlse.fr\r\n\r\nPensez à ajouter cette demande à votre compte sur la base de données des formations :\r\nhttp://formation.laplace.univ-tlse.fr/connexion	t	t	2013-12-05 00:00:00	2013-12-18 00:00:00	12	\N	\N	nouveaux entrants du Laboratoire LAPLACE  sur le site N7	N7
475	2	48	2016-05-31 10:54:18	2016- LES FINANCES	Les finances, le patrimoine les marchés , formation culture d'établissement et de l'enseignement supérieur	t	f	2016-05-31 00:00:00	2016-03-24 00:00:00	1	\N	\N	\N	\N
253	2	70	2013-10-15 16:35:56	2013- Altium Designer	par le réseau des électroniciens	t	t	2013-10-15 00:00:00	2013-12-17 00:00:00	21	\N	\N	\N	CNRS
395	2	42	2015-06-25 14:11:00	2015- Formation continue d'assistant de prévention sur les thème des Registres de sécurité	Reçu la convocation par mail le 16/06/15\r\n\r\nDans le cadre de votre formation continue, le Service développement des compétences vous informe que le Service prévention et sécurité met en place une ½ journée de rencontre, le Jeudi 02 Juillet 2015 de 13h45 à 16h30 à l’Université Paul Sabatier - amphi Concorde, bâtiment U4, 118 route de Narbonne à Toulouse.\r\n\r\nProgramme :\r\n-          13h45 – 14h : accueil avec pause-café \r\n-          14h - 14h30 : bilan annuel de prévention 2014 – perspectives 2015/2016\r\n-     14h30 – 15h30 : les registres de sécurité : présentation du registre santé et sécurité numérique\r\n-     15h45 – 16h30 : Table ronde\r\n\r\n Ce mail vaut convocation et ordre de mission	t	t	2015-06-25 00:00:00	2015-07-02 00:00:00	0	\N	\N	Assistant de prévention	néant
426	2	25	2015-10-12 17:32:20	2013- Netvault logiciel de sauvegarde	\N	t	t	2015-10-12 00:00:00	2013-01-01 00:00:00	14	\N	\N	\N	\N
305	2	38	2014-03-17 10:34:54	2014- Sécurité incendie - Manipulation des moyens de secours - INPT	\N	t	t	2014-03-17 00:00:00	2014-03-25 00:00:00	3	\N	\N	\N	proposée par l'INPT donc aucun frais pour les agents du LAPLACE s'y inscrivant
386	2	72	2015-04-28 14:13:06	2015- Powerpoint :dynamiser ses présentations	\N	t	t	2015-04-28 00:00:00	2015-09-29 00:00:00	14	\N	\N	\N	\N
406	2	8	2015-10-12 16:25:53	2013- Séminaire OSRAM sur l'éclairage LED	\N	t	t	2015-10-12 00:00:00	2013-11-01 00:00:00	7	\N	\N	\N	OSRAM
389	2	70	2015-05-12 17:26:20	2015- Altium Intégrité de signal	\N	t	t	2015-05-12 00:00:00	2015-06-04 00:00:00	8	2015-05-15 00:00:00	1	\N	\N
469	2	18	2016-04-12 16:29:01	2016- Indesign : première approche	\N	t	f	2016-04-13 00:00:00	2016-04-14 00:00:00	7	\N	\N	\N	stage URFIST
279	2	28	2014-02-10 15:58:59	2014- Word Perfectionnement	\N	t	t	2014-02-10 00:00:00	2014-02-24 00:00:00	14	\N	\N	\N	université Paul sabatier
289	2	42	2014-03-04 14:59:43	2013- Journée d'information des agents préventeur - CNRS	\N	t	t	2014-03-04 00:00:00	2013-04-26 00:00:00	7	\N	\N	\N	CNRS
445	2	42	2015-11-19 11:27:49	2015- Journée des assistants de prévention - CNRS	Objectif de la formation:\r\nJournée d’échanges entre les Assistants de Prévention\r\n\r\nProgramme de la formation:\r\nMatin :\r\n1. Présentation de NEO\r\n2. Point EvRP, RATIS\r\nDEJEUNER A LA CANTINE\r\n3. Plan de Prévention 2016\r\n4. Retour d’expériences\r\n- Accident dans le cadre du transport des matières dangereuses\r\n- Exercice de gestion de crise	t	t	2015-11-19 00:00:00	2015-12-07 00:00:00	7	2015-11-19 00:00:00	\N	\N	\N
304	2	70	2014-03-10 10:11:23	2014- Maxwell et Couplage Workbench	Formation dispensée par ANSYS France et effectuée à l'N7.\r\nFormation sur 2 jours, les 7 et 8 avril 2014	t	t	2014-04-09 00:00:00	2014-04-07 00:00:00	14	\N	8	\N	INPT, CNRS, les industriels ....
476	2	57	2016-05-31 10:56:00	2016- Méthodologie oral des concours	Méthodologie oral des concours Formation culture d'établissement et de l'enseignement supérieur	t	f	2016-05-31 00:00:00	2016-03-11 00:00:00	1	\N	\N	\N	\N
295	2	56	2014-03-04 15:15:57	2013- ACC Cellule Compétence - CNRS	\N	t	t	2014-03-04 00:00:00	2013-12-03 00:00:00	7	\N	\N	\N	CNRS
277	2	9	2014-01-20 10:55:40	2014- Les Réseaux Sociaux pour la Culture Scientifique	- Découvrir les outils du web social utiles à la gestion de projet et à la communication.\r\n\r\n- Identifier les communautés en ligne.\r\n\r\n- Apprendre à développer et gérer sa présence en ligne.\r\n\r\n- Connaître les limites et risques de ces outils et pratiques.	t	t	2014-01-20 00:00:00	2014-01-27 00:00:00	14	\N	\N	\N	cnrs
143	2	24	2013-10-03 11:27:26	2013- Journées Techniques LabVIEW	Lors de ces journées, il y aura deux sessions en parallèle :\r\nPour bien démarrer \r\n•\tDécouvrir la plate-forme LabVIEW\r\n•\tConstruire une application dans les règles\r\n•\tRéaliser des mesures physiques\r\n•\tAnalyser les données et les présenter\r\nPour bien développer \r\n•\tAméliorer l’aspect de votre application\r\n•\tDécouvrir les outils adaptés pour débrider les performances de vos applications\r\n•\tMettre à profit de nombreuses techniques et méthodes pour accroître vos compétences\r\n\r\nPour plus d’information :\r\nhttp://sine.ni.com/nievents/app/overview/p/eventId/828079/site/nie/country/fr/lang/fr/offeringId/2459222\r\n\r\nInscription gratuite : \r\nhttps://lumen.ni.com/nicif/F/gb_nievent/content.xhtml?du=http://sine.ni.com/nievents/app/validate/p/offeringId/2459222/site/nie/country/fr/lang/fr\r\n\r\nInformez-en le correspondant formation :\r\ncorrespondants-formation@laplace.univ-tlse.fr	t	t	2013-10-03 00:00:00	2013-10-17 00:00:00	7	\N	\N	\N	Gratuit (Labview)
427	2	25	2015-10-12 17:35:05	2009- Perfectionnement VHDL - CNRS	\N	t	t	2015-10-12 00:00:00	2009-01-01 00:00:00	21	\N	\N	\N	CNRS
280	2	58	2014-02-10 16:01:57	2014- Professionnalisation des membres du jury	Réglementation des concours à destination des membres des jurys.	t	t	2014-02-10 00:00:00	2014-02-17 00:00:00	3	\N	\N	\N	\N
315	2	41	2014-04-17 11:32:24	2014- Sauveteur secouriste du travail – Formation initiale - INPT	L’INPT met en place une action de formation relative au domaine Hygiène & Sécurité « SAUVETEUR SECOURISTE DU TRAVAIL – FORMATION INITIALE »,\r\n\r\nqui aura lieu les 18, 19 et 20 juin 2014 (3 journées consécutives).\r\n\r\nCette action se déroulera sur le site de Labège et sera animée par M. Vincent KOEHRET.\r\n\r\nPour vous inscrire, vous devez compléter le formulaire ci-joint et le retourner à la Cellule de Formation des Personnels par courriel : cellule-formation@inp-toulouse.fr ou par courrier avant le 16 mai 2014.	t	t	2015-06-22 00:00:00	2014-06-18 00:00:00	21	2014-05-16 00:00:00	\N	Formation pour les permanents seulement	INPT
477	2	48	2016-05-31 10:58:14	2016- Formation outil et métier GBCP	formation outil et métier GBCP	t	f	2016-05-31 00:00:00	2016-02-08 00:00:00	2	\N	\N	\N	CNRS
407	2	23	2015-10-12 16:29:25	2011- Représentant CNIL - UPS	\N	t	t	2015-10-12 00:00:00	2011-01-01 00:00:00	7	\N	\N	\N	UPS
302	2	48	2014-03-05 11:01:49	2012- SIFAC Recettes - INPT	Date inconnue	t	t	2014-03-05 00:00:00	2012-01-09 00:00:00	12	\N	\N	\N	INPT
290	2	24	2014-03-04 15:05:29	2013- Programmation orientée objet - CNRS	\N	t	t	2014-03-04 00:00:00	2013-03-28 00:00:00	4	\N	\N	\N	CNRS
297	2	23	2014-03-04 15:28:31	2013- Kit sensibilisation SSI - CNRS	\N	t	t	2014-03-04 00:00:00	2013-06-25 00:00:00	7	\N	\N	\N	CNRS
391	2	23	2015-05-22 08:10:06	2015- Gestion des Incidents SSI - CNRS	\N	t	t	2015-05-22 00:00:00	2015-05-19 00:00:00	7	2015-05-11 00:00:00	\N	\N	CNRS
408	2	25	2015-10-12 16:31:08	2013- Tech Series OS X & iOS - Fonctionnement et techniques de déploiement	\N	t	t	2015-10-12 00:00:00	2013-01-01 00:00:00	7	\N	\N	\N	Apple
323	2	71	2014-04-24 14:50:44	2013- Réunion des correspondants formation du CNRS DR14	à l'Hotel Grand Orléans, Toulouse	t	t	2014-04-24 00:00:00	2013-11-15 00:00:00	7	\N	\N	\N	CNRS
339	2	42	2014-05-28 10:34:29	2014- formation initiale "assistant de prévention" (ACMO)	Permettre d’assurer les missions des nouveaux Assistants de Prévention au sein des unités ou laboratoires de recherche.\r\n\r\n14, 15,16 avril 2014 et 22, 23, 24 avril 2014	t	t	2014-05-28 00:00:00	2014-04-14 00:00:00	42	2014-03-26 00:00:00	25	\N	\N
316	2	28	2014-04-18 08:48:46	2014- Excel Consolidations et TCD -INPT	\N	t	t	2014-04-18 00:00:00	2014-05-23 00:00:00	1	2014-04-16 00:00:00	\N	\N	INPT
298	2	41	2014-03-04 15:37:16	2013- Recyclage Sauveteur Secours du Travail - CNRS	\N	t	t	2014-03-04 00:00:00	2013-03-21 00:00:00	7	\N	\N	\N	CNRS
396	2	38	2015-06-30 11:56:41	2015- PREVENTION DU RISQUE LASER – FORMATION DES RSL - CNRS	Objectifs :\r\n• Connaître la politique du CNRS en matière de prévention du risque LASER • Evaluer les risques liés à une installation LASER\r\n• Dimensionner les dispositifs de protection à mettre en place.\r\nPublic visé :\r\nLa formation s’adresse aux expérimentateurs intervenant sur des installations LASER, pressentis\r\npour assurer le rôle de « référent sécurité LASER » ainsi qu’aux assistants de prévention.\r\nNB : La priorité sera donnée aux agents CNRS, toutefois s’il reste des places disponibles les agents non rémunérés par l CNRS mais travaillant dans une unité CNRS pourront accéder à cette formation.\r\nPrérequis :\r\nAvoir des connaissances / compétences en optique, en LASER ou en prévention.\r\nProgramme :\r\n• Généralités sur les LASER\r\n• Les sources de risque liées à l’utilisation de LASER\r\n• La réglementation et les normes applicables\r\n• Les grandeurs physiques utilisées en prévention du risque LASER et leur mode de calcul\r\n• Les moyens de prévention\r\n• Le suivi médical\r\n• L’organisation de la prévention du risque LASER dans les unités\r\n• Visite d’une installation	t	t	2015-06-30 00:00:00	2015-11-18 00:00:00	16	2015-07-31 00:00:00	\N	CNRS	CNRS
394	2	24	2015-06-22 08:40:06	2015- Initiation à Matlab	Formation faite en interne \r\n\r\n    Comment se présente Matlab\r\n    Écriture d'un script, d'une fonction\r\n    Les commandes utiles\r\n    Création de matrices et vecteurs\r\n    Codage de base (boucles for, if, ...)\r\n    Tracer des courbes\r\n    Lecture et écriture de fichiers	t	t	2015-07-16 00:00:00	2015-07-07 00:00:00	7	\N	\N	\N	faite en interne
428	2	48	2015-10-12 17:41:52	2013- SIFAC module Convention Métier/Outils	\N	t	t	2015-10-12 00:00:00	2015-11-01 00:00:00	14	\N	\N	\N	UPS
303	2	48	2014-03-05 11:05:27	2012- SIFAC Prestations internes - INPT	Date inconnue	t	t	2014-05-15 00:00:00	2012-01-09 00:00:00	6	\N	\N	\N	INPT
291	2	16	2014-03-04 15:06:52	2013- Transmission de données sans fil - CNRS	\N	t	t	2014-03-04 00:00:00	2013-04-19 00:00:00	4	\N	\N	\N	CNRS
398	2	62	2015-08-26 10:48:12	2015- Espagnol - CNRS	Instituto Cervantes, 31 rue des Chalets - 31000 TOULOUSE	t	t	2015-08-26 00:00:00	2015-10-01 00:00:00	60	\N	\N	\N	CNRS
392	2	31	2015-05-22 08:14:18	2015- Esprit Module Fraisage 3 D	\N	t	t	2015-05-22 00:00:00	2015-09-30 00:00:00	21	\N	\N	\N	\N
346	2	38	2014-07-23 16:06:51	2014- Habilitation électrique des personnels non électricien (session juillet) -INPT	Formation dispensée les 15 et 16 juillet à l'N7	t	t	2014-07-23 00:00:00	2014-07-15 00:00:00	14	\N	\N	\N	INPT
340	2	38	2014-05-28 10:49:33	2014- Formation Tutorée à l'Habilitation Electrique - UPS	Dans le contexte de la prévention, la legislation impose aux établissements tels que le notre l’obligation\r\nde procéder à l’habilitation électrique de son personnel, permanent comme non-permanent, dès qu’une exposition à ce risque est identifiée.\r\n\r\n1/04/2014 et 02/04/2014, toute la journée	t	t	2014-05-28 00:00:00	2014-04-01 00:00:00	14	2014-03-31 00:00:00	10	\N	\N
322	2	27	2014-04-22 08:43:05	2014- Formations informatiques - CNRS	La DR14 du CNRS propose des formations WEB. La liste est disponible à l'adresse :\r\nhttp://www.cnrs.fr/midi-pyrenees/FormationPermanente/Programmes/2014/2014-04-10FormationWEB.pdf\r\n\r\nDes dates limites d’inscription ont été rajoutées dans la présentation des différentes formations.\r\nCes formations resteront en ligne jusqu’au 30 septembre 2014.\r\nINSCRIPTION EN LIGNE : Merci de préciser la formation souhaitée dans « attentes concernant le stage »\r\n\r\nInscription en ligne possible : http://www.dr14.cnrs.fr/midi-pyrenees/FormationPermanente/InfosInscriptions.aspx\r\nou en remplissant le formulaire : http://www.dr14.cnrs.fr/midi-pyrenees/FormationPermanente/DemandeFormation.rtf \r\nen l’envoyant à Roxane Castanet : Roxane.Castanet@dr14.cnrs.fr \r\net en mettant en copie le correspondant formation : correspondants-formation@laplace.univ-tlse.fr	t	t	2014-09-23 00:00:00	2014-05-19 00:00:00	\N	2014-04-25 00:00:00	\N	Les agents CNRS sont prioritaires	CNRS
399	2	38	2015-08-31 10:25:40	2015- Formation initiale à l'habilitation électrique	\N	t	t	2015-08-31 00:00:00	2015-06-24 00:00:00	16	\N	12	\N	interne LAPLACE
380	2	18	2015-01-15 11:41:15	2014- Faire un screen cast	Savoir faire un screencast ou capture vidéo de l'écran d'ordinateur accompagné de commentaire audio, opération de montage mettre en ligne.	t	t	2015-01-15 00:00:00	2015-02-02 00:00:00	1	\N	\N	\N	gratuit
317	2	72	2014-04-18 08:49:33	2014- POWERPOINT INITIATION -INPT	\N	t	t	2014-04-18 00:00:00	2014-06-19 00:00:00	1	2014-04-16 00:00:00	\N	\N	INPT
429	2	58	2015-10-12 17:42:55	2013- Membre de jury de concours	\N	t	t	2015-10-12 00:00:00	2013-01-01 00:00:00	7	\N	\N	\N	UPS
409	2	58	2015-10-12 16:32:13	2014- Préparation et formation présidence de jury de concours - UPS	\N	t	t	2015-10-12 00:00:00	2014-01-01 00:00:00	14	\N	\N	\N	UPS
397	2	41	2015-07-24 09:52:01	2015- Formation recyclage SST - CNRS	Recyclage périodique obligatoire des SST	t	t	2015-07-24 00:00:00	2015-10-06 00:00:00	6	\N	\N	CNRS	CNRS
410	2	25	2015-10-12 16:32:47	2014- Outils de récupération de données	La Grande Motte	t	t	2015-10-12 00:00:00	2014-01-01 00:00:00	21	\N	\N	\N	Agnosys
393	2	16	2015-05-22 08:19:20	2015- ANF VIDE POUR UTILISATEUR - CNRS	\N	t	t	2015-05-22 00:00:00	2015-06-22 00:00:00	28	\N	\N	\N	CNRS - DR4
333	2	69	2014-05-15 11:27:32	2014- Gestion du temps et des priorités - UPS	Gestion du temps et des priorités	t	t	2014-05-15 00:00:00	2014-05-12 00:00:00	7	\N	\N	\N	UPS
347	2	59	2014-07-23 16:26:50	2014- International summer school de l'université de Toulouse, session d'été	30 h cours de langues + visites et conversations dans les cafés du centre ville + tours guidés	t	t	2014-07-23 00:00:00	2014-08-28 00:00:00	35	2014-08-23 00:00:00	\N	\N	LABO
430	2	48	2015-10-12 17:45:35	2013- La fiscalité de l'UPS	\N	t	t	2015-10-12 00:00:00	2013-11-01 00:00:00	7	\N	\N	\N	UPS
334	2	47	2014-05-15 11:39:34	2014- Dématérialisation : Formation gestionnaires - UPS	DEMATERIALISATION : FORMATION GESTIONNAIRES	t	t	2014-05-15 00:00:00	2014-05-27 00:00:00	3	\N	\N	\N	UPS
446	2	12	2015-11-24 16:09:39	2015- La fabrication numérique - CNRS	\N	t	t	2015-11-24 00:00:00	2015-11-05 00:00:00	7	2015-10-16 00:00:00	40	\N	CNRS
448	2	57	2016-01-21 17:29:47	2016- Préparation pour le dossier de RAEP - UPS	\N	t	f	2016-01-22 00:00:00	2016-02-15 00:00:00	14	\N	\N	\N	\N
450	2	54	2016-02-10 08:31:55	2016- Conduire un entretien de résolution de problèmes - CNRS	\N	t	t	2016-02-10 00:00:00	2016-01-26 00:00:00	14	\N	12	\N	CNRS
411	2	16	2015-10-12 16:43:29	2012- Formation Technique de brasage, retouche et réparation	MICRONIKS Europe	t	t	2015-10-12 00:00:00	2012-01-01 00:00:00	21	\N	\N	\N	CNRS
431	2	50	2015-10-12 17:46:59	2012- Nouveautés législatives sur les droits des étrangers - UPS	\N	t	t	2015-10-12 00:00:00	2012-11-01 00:00:00	7	\N	\N	\N	UPS
447	2	16	2016-01-06 14:10:03	2015- Détection de fuites pour le vide - CNRS	Formation CNRS via le réseau du vide	t	t	2016-01-06 00:00:00	2015-10-13 00:00:00	14	2015-09-01 00:00:00	10	CNRS	Réseau du vide CNRS
412	2	70	2015-10-12 16:46:34	2012- Simulateur SPICE	\N	t	t	2015-10-12 00:00:00	2012-01-01 00:00:00	21	\N	\N	\N	CNRS
301	2	48	2014-03-05 11:00:48	2012- SIFAC Missions - INPT	Date inconnue	t	t	2014-03-05 00:00:00	2012-01-09 00:00:00	12	\N	\N	\N	INPT
319	2	28	2014-04-18 08:50:33	2014- WORD 2010 Documents longs -INPT	Objectifs :\r\nGérer la numérotation des pages – Présenter du texte sur plusieurs colonnes – Créer des sections – Travailler en mode plan – Créer des signets, des index, des renvois – Concevoir une table des matières automatique\r\n\r\nContenu :\r\nPrésentation de la formation\r\nProcédure objectifs et moyens\r\nTour de table (analyse des attentes des participants)\r\nLes documents longs\r\nDéplacements dans un long document - Signets, annotations - Notes de bas de page et de fin de document - Explorateur de documents\r\nOrganiser un document par le mode plan\r\nAvantages de la technique du Plan\r\nHausser et abaisser les niveaux - Afficher, développer et réduire les niveaux - Numéroter les titres\r\nComprendre et utiliser les sections, en-têtes et pieds de page\r\nQu’est-ce qu’une section ?\r\nSaut de page et saut de section - En-têtes et pieds de page différents par sections\r\nNumérotation des pages\r\nTable des matières et des illustrations\r\nCréation de la table des matières - Choix des niveaux - Choix des styles - Modification et mise à jour de la table\r\nIndex\r\nMarquage des entrées et sous entrées - Compilation et mise à jour de l’index\r\nDocument maître\r\nQu’est-ce qu’un document maître et des sous-documents ?\r\nCréation et gestion des en-têtes et tables des matières\r\nTravailler à plusieurs sur un même document en suivant les modifications\r\nMise en pratique\r\nRéalisation d’exercices après chaque partie théorique\r\nLibre parcours : chacun s’exerce sur des thèmes correspondant à ses objectifs\r\n\r\nRemplir le formulaire d'inscription : http://intranet.laplace.univ-tlse.fr/IMG/pdf/Fiche_inscription_-_Actions_collectives.pdf\r\nla renvoyer par email à Raddia KADDAOUI : kaddaoui.formation@inp-toulouse.fr\r\nen mettant en copie le correspondant formation : correspondants-formation@laplace.univ-tlse.fr	t	t	2014-04-18 00:00:00	2014-06-04 00:00:00	7	2014-04-16 00:00:00	\N	\N	INPT
451	2	14	2016-02-10 08:44:23	2016- Expérimentation animale - CNRS	Niveau Opérateur Rongeur	t	f	2016-02-10 00:00:00	\N	\N	\N	\N	\N	UPS
449	2	23	2016-01-22 10:19:39	2016- Protection de données et de services par le chiffrement - CNRS	•\tIntroduction : risques \r\n•\tChiffrement : aspects théoriques \r\n•\tPanorama des outils\r\n•\tChiffrement de services (web, smtp, imap…)\r\n•\tKeepass\r\n•\tEquipements mobiles : état des lieux\r\n•\tConclusion : préconisations \r\n•\tQuestions/Réponses\r\nSessions de TP\r\n\tTP Windows : bitlocker \r\n                             ZoneCentral \r\n                                   TrueCrypt (conteneurs)\r\n\tTP LINUX : TrueCrypt et dm-crypt\r\n\tTP MAC :   FileVault	t	f	2016-01-22 00:00:00	2016-02-12 00:00:00	14	2016-01-29 00:00:00	50	Correspondants Sécurité Système Information des Unités	CNRS
432	2	48	2015-10-12 17:47:24	2011- Contrats européens - UPS	\N	t	t	2015-10-12 00:00:00	2011-11-01 00:00:00	7	\N	\N	\N	UPS
352	2	41	2014-09-08 10:29:57	2014- Sauveteur secouriste du travail - CNRS	Sauveteurs secouristes du travail \r\nà la Délégation Midi-Pyrénées	t	t	2015-06-22 00:00:00	2014-10-06 00:00:00	14	\N	\N	CNRS	CNRS
433	2	12	2015-10-12 17:52:41	2012- 16èmes Rencontre régionale du réseau des électroniciens DR14	\N	t	t	2015-10-12 00:00:00	2012-01-01 00:00:00	7	\N	\N	\N	CNRS
354	2	15	2014-09-19 13:14:43	2014- Rencontre Métallographie et Microscopie	Thèmes abordés :\r\n\r\n- Réaliser correctement les préparations micrographiques des échantillons avec revêtements (plasma...) ou traités thermiquement, des composants électroniques et des composites\r\n- Préparation de matériaux hétérogènes ; comment choisir la bonne méthode de polissage\r\n- Les méthodes de polissage rapides\r\n- Mieux connaître son microscope et l'acquisition d'images\r\n- Les différentes méthodes d'observation en microscopie\r\n- Analyse d'image et mesure de précision\r\n- Dureté et microdureté : les nouvelles technologies\r\n- Ateliers pratiques sur les équipements en fonctionnement	t	t	2014-09-19 00:00:00	2014-05-20 00:00:00	8	\N	\N	\N	\N
413	2	23	2015-10-12 16:54:03	2009- des bonnes pratiques pour les ASR	\N	t	t	2015-10-12 00:00:00	2009-01-01 00:00:00	35	\N	\N	\N	CNRS
314	2	26	2014-04-17 11:29:49	2014- Configuration avancé, tuning pour MySQL - CNRS	Contenu de la formation\r\n\r\nPoints généraux :\r\n-\tDifférentes versions\r\n-\tPoints d’intérêts particuliers sur l’installation et les mises à jour\r\n-\tComposants avancés de Mysql\r\n-\tLes processus (mysql, mysqld_safe)\r\n-\tFichier de configuration : /etc/my.cnf \r\n-\tGestion des écritures\r\n-\tVerrous, lock tables\r\n\r\nMYSQL – MariaDB – NoSQL - Oracle :\r\n-\tDifférences techniques entre MySQL et MariaDB (sur la gestion des données et le langage)\r\n-\tMigrations d’une base Oracle vers MySQL\r\n-\tIntérêt des SGBD NoSQL\r\n\r\nLes différents moteurs de stockages   et leurs spécificités :\r\n-\tInnoDB : structure, configuration, optimisation\r\n\r\n\r\nTuning, optimisation :\r\n-\tOutils d’administration de paramétrage. Points avancés de phpmyadmin\r\n-\tUtilisation des ressources serveur (disque et mémoire) : Eléments de dimensionnement du serveur linux à partir des éléments connus de la base à héberger (taille de la base, nombre d’accés/sec et taux de READ/WRITE)\r\n-\tComment mySQL est scalale sur le hardware : upgrade de RAM et CPU en fonction de l’utilisation.\r\n-\tFonctionnement et configuration des  cache (query, threads, tables) et des buffers pour maximiser les performances\r\n-\tGestion des index : analyse du log des requêtes non indexées\r\n-\tBase INFORMATION_SCHEMA\r\n-\tOutils de tuning :  tuning-primer.sh et mysqltuner , autres ?\r\n-\tUtilisation de Mysql en RAM : intérêt et méthode\r\n-\t\r\nLogs :\r\n-\tOutils d’analyse des requêtes\r\n-\tCollecte des statistiques ; Analyse des logs : comment logguer en continu pour avoir les bonnes informations sans risquer de pénaliser les performances ?\r\n\r\nRésolution de problèmes :\r\n-\tComment diagnostiquer des ralentissements constatés ?\r\n-\tTrouver les requêtes consommatrices / Calculer le coût d’une requête\r\n/ Outil EXPLAIN\r\n-\tBest Practices en cas de crash ou corruptions\r\n\r\nSauvegardes / Sécurité :\r\n-\tSauvegardes à froid, à chaud\r\n-\tRestauration : outils dédiés\r\n-\tPoints importants de sécurité\r\n-\tIntégrité des données\r\n-\tGestion fine des droits utilisateurs (profils utilisateurs ?)\r\n\r\nCluster Galera/MariaDB :\r\n-\tSpécificités\r\n\r\nLa formation aura lieu à la DSI Labège les 26 et 27 mai	t	t	2014-07-08 00:00:00	2014-05-26 00:00:00	14	\N	\N	personnels informatiques CNRS	CNRS
414	2	12	2015-10-12 16:54:34	2009- Formation A2IMP pour le réseau de métier CAPITOUL, Toulouse	\N	t	t	2015-10-12 00:00:00	2009-01-01 00:00:00	7	\N	\N	\N	\N
326	2	54	2014-04-30 10:29:24	2014- Gestion des conflits - PFRH	OBJECTIFS :\r\n· Reconnaître et réduire les tensions qui nous envahissent lors d’un conflit.\r\n· Savoir repérer les sources de conflits afin de pouvoir agir à temps.\r\n· Obtenir des méthodes et outils pour mieux gérer les conflits.\r\n\r\nCONTENU :\r\nDéfinition et différents types de conflits.\r\n·   Critiques, agressions et conflits des situations à différencier.\r\n·  Gestion des conflits au travail (fuite, attaque et affirmation de soi)\r\n·   Outils et méthodes pour gérer le stress dû à certaines situations conflictuelles\r\n·  Affirmation de soi (mise en pratique).\r\n\r\nMETHODE PEDAGOGIQUE : Apports méthodologiques- réflexions et discussions, et\r\nexercices d’application.\r\n\r\nFORMATEUR : Philippe Vilmen, formateur Management/Rh.\r\nLIEU : Toulouse.\r\nDUREE et DATE : 3 jours - les 12, 13 et 14 novembre 2014.\r\n\r\nhttp://www.midi-pyrenees.safire.fonction-publique.gouv.fr/web/midi-pyrenees/198-details-d-une-formation.php?Zm9ybWF0aW9uPTQ3MTImZG9tYWluPTI3NCZpZFJlZ2lvbj0xNg==\r\n\r\nPour les agents INPT, lors de votre inscription, n’oubliez pas d’indiquer les courriels de votre supérieur hiérarchique et du responsable de la cellule formation, M. François LLANAS : francois.llanas@inp-toulouse.fr, afin qu’ils valident votre demande. La préinscription ne vaut pas convocation, l’inscription ne sera effective qu’après validation de votre hiérarchie et de la Cellule Formation des Personnels et en fonction des places disponibles.\r\n\r\nPour les agents UPS, a priori de même avec Mme Geneviève BERNIS : genevieve.bernis@adm.ups-tlse.fr\r\n\r\nPour les agents CNRS, a priori de même avec M. Alexandre TESTE : Alexandre.Teste@dr14.cnrs.fr	t	t	2015-06-22 00:00:00	2014-11-12 00:00:00	21	\N	12	\N	\N
357	2	12	2014-09-23 10:57:38	2014- La fabrication additive métallique - CNRS	La fabrication additive métallique (journée thématique organisée par le réseau inter-régional des mécaniciens Languedoc-Roussillon / Midi-Pyrénées)\r\n\r\nObjectifs de la formation :\r\nDonner une vision d'ensemble de la fabrication additive et de l'intégration de ce nouveau procédé tout au long de la vie du produit afin d’évaluer la possibilité d’utiliser ces techniques dans le cadre de nos développements instrumentaux.\r\n\r\nProgramme de la formation :\r\n- Présentation générale du procédé, retour d’expérience\r\n- Méthodes de conception\r\n- Fabrication additive et réalisation\r\n- La fabrication additive et les matériaux\r\n\r\nTout le programme :\r\nhttp://www.cnrs.fr/midi-pyrenees/FormationPermanente/Programmes/2014/2014-09-22AnnonceFabricationAdditiveMetal.pdf\r\n\r\nInscription en ligne possible : http://www.cnrs.fr/midi-pyrenees/FormationPermanente/InscriptionEnLigne/\r\nou en remplissant le formulaire : http://www.dr14.cnrs.fr/midi-pyrenees/FormationPermanente/DemandeFormation.rtf\r\nen l’envoyant à Roxane Castanet : Roxane.Castanet@dr14.cnrs.fr \r\net en mettant en copie le correspondant formation : correspondants-formation@laplace.univ-tlse.fr	t	t	2014-09-23 00:00:00	2014-11-06 00:00:00	7	2014-10-08 00:00:00	40	mécaniciens	CNRS
434	2	16	2015-10-12 17:54:13	2012- Circuits PSoC 3/5 coeur C8051 ou Arm32	\N	t	t	2015-10-12 00:00:00	2012-01-01 00:00:00	21	\N	\N	\N	CNRS
444	2	42	2015-11-19 10:50:14	2015- Formation COMMUNICATION « Assistants de Prévention »	Objectifs de la formation:\r\nA l'issue de cette formation, les stagiaires seront capables :\r\n- de connaître les principes généraux de la communication humaine et de les mettre en oeuvre,\r\n- d’identifier les différentes situations de communication et s’y adapter,\r\n- de développer les qualités relationnelles et de communication indispensables à l’exercice de cette fonction.\r\n\r\nProgramme de la formation:\r\nLes principes généraux de la communication humaine :\r\n- les principaux éléments intervenant dans une situation de communication,\r\n- les lois fondamentales régissant les communications interindividuelles,\r\n- les comportements propices à renforcer le positionnement de l’AP,\r\n- les écueils de la communication.\r\nDes techniques pour des situations spécifiques :\r\nL’intervention en situation de « flagrant délit « de non respect des règles de prévention et de sécurité\r\n- l’art de faire un recadrage\r\nLorsque l’AP est pris à partie\r\n- l’art de faire une médiation entre agents en conflit à cause des règles d’hygiène et de sécurité\r\nLa participation à des réunions :\r\n- positionnement et communication de l’AP :\r\n- au sein du comité d’hygiène et de sécurité comme participant\r\n- avec les membres du laboratoire comme conseiller de sécurité\r\n- avec les participants extérieurs comme représentant du laboratoire en matière d’hygiène et de sécurité\r\nL’AP, force de proposition :\r\n- des plans pour convaincre\r\n- le plan argumentaire\r\n- le plan rhétorique	t	t	2015-11-19 00:00:00	2015-11-23 00:00:00	21	2015-10-28 00:00:00	12	\N	\N
351	2	16	2014-09-02 16:58:31	2014- Microscopie à Force Atomique	Cours théoriques + Travaux dirigés (1 jour)\r\n- Principe de la microscopie à force atomique\r\n- Les différents modes de caractérisation topographique\r\n- Les principaux modes dérivés : EFM, MFM, LFM, lithographie ...\r\n- Les pointes\r\n- Les principaux artéfacts\r\n\r\nTravaux pratiques (2 jours) [modulables en fonction du niveau et des attentes des stagiaires]\r\n- Apprentissage de l’observation topographique en modes contact et tapping sur des échantillons\r\ntrès variés au choix (microstructures, CD, nanoparticules, nanotubes, bactéries, films polymères...) : choix du mode le plus pertinent, choix de la pointe la mieux adaptée, optimisation\r\ndes paramètres d’acquisition et de traitement d’images, mise en évidence des artéfacts...\r\n- Initiation à quelques modes dérivés : EFM, nanolithographie...	t	t	2014-09-02 00:00:00	2014-06-24 00:00:00	21	\N	\N	\N	\N
435	2	16	2015-10-12 17:55:21	2014- Alimentations à découpage AC/DC et DC/DC	\N	t	t	2015-10-12 00:00:00	2014-01-01 00:00:00	21	\N	\N	\N	CNRS
415	2	21	2015-10-12 16:55:47	2012- Une "démarche qualité" dans un service informatique, ça veut dire quoi en pratique ?	\N	t	t	2015-10-12 00:00:00	2012-01-01 00:00:00	14	\N	\N	\N	\N
416	2	31	2015-10-12 16:59:29	2010- Formation de base, Principes fondamentaux Inventor 2009	\N	t	t	2015-10-12 00:00:00	2010-01-01 00:00:00	7	\N	\N	\N	\N
296	2	57	2014-03-04 15:22:39	2013- Préparation aux concours internes session 1 - CNRS	Objectifs de la formation :\r\nApporter aux participants une aide dans la préparation individuelle des concours internes\r\n\r\nProgramme de la formation :\r\n\r\nModule 1 : Le cadre juridique et institutionnel des concours internes, la préparation du dossier\r\n1/Le contexte et les caractéristiques des concours internes\r\n- Les concours internes dans la gestion des carrières\r\n- L’utilisation des fiches d’emploi type dans le cadre des concours internes\r\n- L’approche compétences dans les concours internes\r\n2/Le Rapport d’Activité - objectifs :\r\n- Etre capable de décrire avec précision les fonctions et les activités exercées depuis 5 ans (coeur du métier) .\r\n- Etre capable de préciser les connaissances et compétences mises en oeuvre en matière d’organisation, communication, coordination, documentation et formation\r\nProgramme :\r\n- Les objectifs du rapport d’activité\r\n- Les critères de rédaction et de présentation\r\n- Analyse de la trajectoire professionnelle, les différentes stratégies\r\n\r\nModule 2 : La préparation de l’audition - objectifs :\r\nEtre capable d’affronter de manière optimale une situation d’audition\r\nAcquérir des outils de communication favorisant l’expression orale\r\nProgramme :\r\n- L’exploitation orale du rapport d’activité\r\n- Les aspects fondamentaux de l’expression orale\r\n- L’audition : simulation d’entretien, l’organisation de la pensée et de l’exposé, la reformulation, la gestion du temps, des documents et de l’espace\r\n\r\nPublic concerné :\r\nAgents CNRS : Ingénieurs, Techniciens, Administratifs, se présentant à un ou plusieurs concours internes CNRS dans l’année. Les personnes ayant déjà suivies cette formation ne seront pas sélectionnées\r\n\r\nOBLIGATOIRE, avant toute inscription :\r\nMerci de contacter le bureau des concours de la Délégation Midi-Pyrénées pour savoir si vous avez l’ancienneté requise pour passer le concours.	t	t	2014-03-04 00:00:00	2013-03-21 00:00:00	21	2013-02-15 00:00:00	12	Agents CNRS : Ingénieurs, Techniciens, Administratifs, se présentant à un ou plusieurs concours internes CNRS dans l’année	CNRS
338	2	55	2014-05-27 17:23:07	2014- Journée "Initiation au montage de projets de recherche" - INPT	La Direction de la Recherche de l'INP Toulouse organise jeudi 17 juillet 2014 une journée de formation au montage de projets de recherche. Ouverte sur inscription à toute personne ayant vocation à faire gérer son projet par l'INP Toulouse (enseignant-chercheur, chercheur hébergé...)\r\n\r\nProgramme :\r\nPrésentation d'une cartographie des appels à projets (INP, Université de Toulouse, IdEX, Région, ANR...) : spécificités, attendus, éligibilité, processus de sélection...\r\nDeux mises en situation via un travail en petites équipes tutorées permettant de simuler :\r\n•une réponse à un Appel à Projets Jeune Chercheur de l'ANR\r\n•le montage d'une collaboration industrielle incluant une thèse CIFRE\r\n\r\nOBJECTIFS\r\n\r\nL'objectif de cette journée est de donner les outils méthodologiques, sémantiques, la connaissance des services support, les principes comptables et financiers de base, des notions de propriété intellectuelle, pour permettre aux participants de : \r\n•mener une négociation financière éclairée\r\n•élaborer une fiche de coûts juste\r\n\r\nLe programme de cette journée a été conçu par un groupe de travail composé d'élus du Conseil Scientifique, de correspondants recherche, de personnels du SAIC et de la direction de la recherche. Il s'appuiera sur les compétences du SAIC, les retours d'expériences de quelques collègues, le travail de la direction de la recherche, le regard de Toulouse Tech Transfer.\r\n\r\nLes aspects scientifiques proprement dits ne seront pas abordés, relevant évidemment de la compétence propre du chercheur et de la responsabilité du laboratoire.\r\n\r\nLes places étant limitées, la journée est ouverte sur pré-inscription en ligne AVANT LE 13 JUIN.\r\npréinscription :\r\nhttp://www.inp-toulouse.fr/fr/la-recherche/l-excellence-autrement/l-actualite-recherche/journee-initiation-au-montage-de-projets-de-recherche/pre-inscription.html\r\n\r\nLes réponses et confirmations seront envoyées dans la semaine du 16 juin.	t	t	2015-06-22 00:00:00	2014-07-17 00:00:00	7	2014-06-13 00:00:00	\N	\N	INPT
436	2	59	2015-10-12 20:58:20	2015- Anglais courant - INPT	\N	t	t	2015-10-12 00:00:00	2015-01-01 00:00:00	35	\N	\N	\N	\N
360	2	38	2014-10-07 14:24:16	2009- Habilitation électrique du personnel non électricien - INPT	1° session : 27 et 28 avril 2009 (9 participants)\r\n2° session : 16 et 17 novembre 2009 (7 participants)\r\n\r\nFormation effectuée sur le site N7	t	t	2014-10-07 00:00:00	2009-04-27 00:00:00	14	\N	\N	\N	INPT-N7
417	2	12	2015-10-12 17:01:07	2011- CAE "Colloque Arcs Electriques"	\N	t	t	2015-10-12 00:00:00	2011-01-01 00:00:00	14	\N	\N	\N	\N
437	2	16	2015-10-12 21:15:21	2015- Utilisation d'une tronçonneuse diamantée	\N	t	t	2015-10-12 00:00:00	2015-01-01 00:00:00	7	\N	\N	\N	\N
361	2	38	2014-10-07 14:26:19	2010- Habilitation électrique du personnel non électricien - INPT	session les 28 et 29 octobre 2010\r\n\r\nFormation effectuée sur le site N7 avec Pierre Rossi	t	t	2014-10-07 00:00:00	2010-10-28 00:00:00	14	\N	\N	\N	INPT-N7
349	2	27	2014-08-28 12:25:32	2014- Formation informatique : PERL - CNRS	Objectif global :\r\nIntégrer le langage et sa philosophie\r\n\r\nContenu :\r\nPrésentation\r\n Philosophie\r\n Historique\r\n Spécificités\r\n Exemple de code commenté\r\nSyntaxe de base\r\n Scalaires\r\n Tableaux\r\n Hachages\r\n Références\r\n Opérateurs\r\n Structures de contrôle\r\nModifieurs et raccourcis\r\nSyntaxe avancée\r\n Références\r\n Structures complexes\r\n Les fonctions\r\n définir des fonctions\r\n types de paramètres\r\n prototypage\r\n fonctions prédéfinies\r\nLes expressions régulières\r\nOpérations sur les fichiers\r\n Entrées/Sorties standard\r\n Manipuler des fichiers texte\r\n Manipuler des répertoires\r\n Manipuler des fichiers binaires\r\n Formater des données textes et binaires\r\nProgrammation orientée objet\r\n Définitions et intérêt\r\n Classes, encapsulation et héritage\r\n Paquetages, Modules et bibliothèques\r\n utilisation de modules tiers\r\n Les archives CPAN\r\n installation automatique\r\n Créer ses propres modules\r\nExemples d'applications\r\n Accès aux SGBD par l'interface DBI\r\n Traitement de flux XML\r\n Programmation système & réseau\r\nNote: les thèmes applicatifs peuvent être librement choisis par les participants en début de formation\r\n\r\n\r\nInscription en ligne possible : http://www.dr14.cnrs.fr/midi-pyrenees/FormationPermanente/InfosInscriptions.aspx\r\nou en remplissant le formulaire : http://www.dr14.cnrs.fr/midi-pyrenees/FormationPermanente/DemandeFormation.rtf\r\nen l’envoyant à Roxane Castanet : Roxane.Castanet@dr14.cnrs.fr \r\net en mettant en copie le correspondant formation : correspondants-formation@laplace.univ-tlse.fr	t	t	2014-08-28 00:00:00	2014-11-03 00:00:00	3	2014-09-30 00:00:00	\N	Développeurs et administrateurs systèmes	CNRS
418	2	12	2015-10-12 17:01:18	2013- CAE "Colloque Arcs Electriques"	\N	t	t	2015-10-12 00:00:00	2013-01-01 00:00:00	14	\N	\N	\N	\N
438	2	16	2015-10-12 21:15:56	2014- Utilisation d'une machine de bonding	\N	t	t	2015-10-12 00:00:00	2014-01-01 00:00:00	7	\N	\N	\N	\N
125	2	48	2013-09-20 13:51:11	2011- SIFAC Référentiel	\N	t	t	2013-09-20 00:00:00	2011-01-01 00:00:00	7	\N	\N	\N	INPT
419	2	48	2015-10-12 17:07:52	2011- SIFAC module Recettes	\N	t	t	2015-10-12 00:00:00	2015-11-01 00:00:00	7	\N	\N	\N	\N
356	2	44	2014-09-22 14:15:59	2014- Le droit à l'image appliqué à la production scientifique - CNRS	Objectifs de la formation :\r\n- Connaitre les bases de la propriété intellectuelle et du droit à l’image.\r\n- Connaitre les droits sur sa propre production de photographies ou de films.\r\n- Savoir comment utiliser en toute légalité une photographie dans ses communications (publications, posters, site internet, films, présentations…) et avoir conscience des risques liés à l’utilisation d’une image ou d’un film appartenant à un tiers.\r\n- Savoir à qui s’adresser au CNRS pour trouver de l’aide sur les questions de droit à l’image.\r\n\r\nProgramme de la formation :\r\nModule 1 : Notion de Propriété intellectuelle et de droit à l’image\r\n- Présentation de la propriété intellectuelle\r\n- La protection de l’image par la propriété intellectuelle et par le droit à la vie privée\r\n- Les titulaires des droits\r\n- Les droits conférés (usages autorisés)\r\n- Exercice pratique\r\n\r\nModule 2 : Réaliser une photographie ou un film et l’utiliser – Comment le faire dans\r\nles règles ?\r\n- Le droit du modèle / du propriétaire du bien\r\n- Autorisation de prise de vue - présentation des modèles CNRS\r\n- Le titulaire des droits sur la photo (employeur ? Photographe ?)\r\n- Utiliser la photographie ou le film dans ses propres productions scientifiques\r\n(site, posters, publication …)\r\n- Autoriser un tiers à utiliser la photographie\r\n- Exercice pratique\r\n\r\nModule 3 : Utiliser une photo / un film réalisée par un tiers – Eviter la contrefaçon.\r\n- Qu’est-ce que la contrefaçon ?\r\n- L’utilisation de photos publiées sur internet ou dans les publications scientifiques\r\n- Les Licences : définition et contenu\r\n- Focus sur les licences « creative commons »\r\n- Exercice pratique\r\n\r\nModule 4 : Les bases de données de photographies / de films\r\n- Utiliser les photographies des bases de données d’images / films scientifiques\r\n- Contribuer aux bases de données d’images / films scientifiques\r\n\r\nLa formation se déroulera à la Délégation les 4 et 5 novembre 2014.\r\nPour plus d'information :\r\nhttp://www.cnrs.fr/midi-pyrenees/FormationPermanente/Programmes/2014/2014-08-07AnnonceDroitImage.pdf\r\n\r\nInscription en remplissant le formulaire : http://www.dr14.cnrs.fr/midi-pyrenees/FormationPermanente/DemandeFormation.rtf \r\nen l’envoyant à Laurence Neuville : Laurence.Neuville@dr14.cnrs.fr\r\net en mettant en copie le correspondant formation : correspondants-formation@laplace.univ-tlse.fr	t	t	2014-09-22 00:00:00	2014-11-04 00:00:00	14	2014-10-10 00:00:00	12	chercheurs et ITA	CNRS
350	2	27	2014-08-29 16:16:48	2014- SPIP – Développement et adaptation du nouveau kit CNRS - CNRS	Objectif :\r\nPersonnaliser un site web harmonisé à la charte CNRS avec le gestionnaire de contenu SPIP. Découvrir les paramétrage avancé du "Kit SPIP CNRS". Modifier les squelettes, feuilles de styles et scripts. Maîtriser les boucles, balises, critères et filtres des squelettes. upgrader la version de spip. Et Installer des plugins spip (crayon, phpMyVisites, Typo enrichie, …).\r\n\r\nContenu :\r\nPréparer et gérer son environnement de développement\r\n- Identifier les ressources en ligne (Aides, tutoriaux, contributions, assistants de conception)\r\n- Installation avancé du kit SPIP en local\r\n- Configurer : la gestion des langues, les options d'interactivité, la gestion du contenu et les options avancées\r\nAdaptation des squelettes du kit SPIP\r\n- Rôle du plugin du kit CNRS\r\n- Rôle et structure d'un squelette\r\n- Squelette "page d'accueil"\r\n- Squelette "page rubrique"\r\n- Squelette "page article"\r\n- Squelette "page de l'annuaire"\r\nBoucle et affichage des composants\r\n- Rôles et structures des boucles\r\n- Boucle du menu déroulant gauche\r\n- Balise article et rubrique : titre, sous titre, sur titre, auteur, date…\r\n- Application de la feuille de style CSS\r\nConstruire une BOUCLE\r\n- Choisir le type de BOUCLE (rubrique, article, brève, document, ...)\r\n- Choisir les BALISES à afficher, les critères de sélection et les critères d'affichage\r\n- Utiliser les filtres sur les BALISES pour adapter l'affichage des BALISES\r\n- Identifier les relations entre les différents types de BOUCLES\r\nRéaliser des squelettes\r\n- Créer un nouveau squelette\r\n- Imbriquer les éléments récurent par le système INCLURE ou balise MODELE\r\nRéaliser des squelettes (techniques avancées)\r\n- Internationaliser les squelettes\r\n- Créer et utiliser des filtres personnalisés pour gérer l'affichage des BALISES\r\n- Inclure des traitements PHP dans les squelettes\r\nIntégrer des plugins\r\n- Principes et évolutivité des plugins\r\n- Identifier les ressources en ligne (Aides, tutoriaux, contributions)\r\n- Exemple d’installation et de paramétrage : Accès restreint, Barre Typographique, Crayon, Couteau suisse\r\n\r\nInscription en ligne possible : http://www.dr14.cnrs.fr/midi-pyrenees/FormationPermanente/InfosInscriptions.aspx\r\nou en remplissant le formulaire : http://intranet.laplace.univ-tlse.fr/spip.php?article311 \r\nen l’envoyant à Roxane Castanet : Roxane.Castanet@dr14.cnrs.fr \r\net en mettant en copie le correspondant formation : correspondants-formation@laplace.univ-tlse.fr	t	t	2014-08-29 00:00:00	2014-11-10 00:00:00	21	\N	\N	Toute personne amenée à installer un site web avec le "Kit SPIP CNRS"	CNRS
331	2	55	2014-05-15 10:07:59	2014- Management de projet - CNRS	Objectifs de la formation : A l'issue de cette formation, les stagiaires sauront :\r\n• maîtriser les principaux outils de conception de projet (formalisation, objectifs…..)\r\n• planifier et mettre sous contrôle le processus de réalisation (pilotage budgétaire, mesure des résultats…)\r\n• optimiser leur capacité à animer une équipe projet et à communiquer efficacement avec tous les acteurs du projet : commanditaires, financeurs, partenaires……..\r\n\r\nProgramme de la formation :\r\nINTRODUCTION\r\n• Caractéristiques de la gestion de projet\r\n• Spécificités des projets de recherche/ou de réalisation (technique, administratif……)\r\n• Schématisation du déroulement-type d’un projet de recherche\r\n\r\nMETHODOLOGIES DE CONCEPTION DE PROJET (QUOI FAIRE ?)\r\nAnalyse de la demande - Deux cas de figure :\r\n• de l’idée au projet (le chef de projet apporte l’idée du projet)\r\n• de l’impulsion politique au projet (l’orientation du projet est dicté par l’institution ou par les instances de financement de type Europe ou ANR)\r\nAnalyse stratégique :\r\n• financement du projet (exemple projet ANR)\r\n• choix de partenariats\r\n• constitution des instances de pilotage (comité de pilotage, comité scientifique)\r\nAnalyse fonctionnelle :\r\n• Cadrage du cahier des charges du projet\r\n\r\nMETHODOLOGIES DE SUIVI DU PROJET (COMMENT FAIRE ?)\r\n• Planning du projet (PERT, GANTT) ; Bases budgétaires\r\n• Répartition et définition des rôles des membres de l’équipe projet\r\n• Méthode d’élaboration de tableau de bord de gestion des risques\r\n• Eclairage des méthodes « qualité » de la recherche\r\n• Evaluation du projet et capitalisation\r\n\r\nANIMATION DE L’EQUIPE PROJET ET GESTION DES RELATIONS INTERPERSONNELLES\r\n• Motivation des équipes transverses sur la durée du projet\r\n• Animation de revues d’avancement de projet\r\n• Outils de formalisation des collaborations : contractualisation…..\r\n\r\n\r\nhttp://www.cnrs.fr/midi-pyrenees/FormationPermanente/Programmes/2014/2014-05-12FormationConduiteProjet.pdf\r\n\r\nInscription en remplissant le formulaire : http://www.dr14.cnrs.fr/midi-pyrenees/FormationPermanente/DemandeFormation.rtf \r\nen l’envoyant à Laurence Neuville : Laurence.Neuville@dr14.cnrs.fr\r\net en mettant en copie le correspondant formation : correspondants-formation@laplace.univ-tlse.fr	t	t	2014-09-23 00:00:00	2014-10-07 00:00:00	21	2014-06-27 00:00:00	12	Tout agent étant amené à manager un projet	CNRS
420	2	48	2015-10-12 17:08:13	2015- SIFAC Immobilisations	\N	t	t	2015-10-12 00:00:00	2015-11-01 00:00:00	7	\N	\N	\N	\N
198	276	2	2013-10-14 09:03:27	Bases statistiques Module 1	\N	t	t	2013-10-14 00:00:00	2010-02-01 00:00:00	14	\N	\N	\N	CNRS
486	276	2	2016-06-15 17:37:31	Bases statistiques Module 1	\N	t	t	2016-06-15 00:00:00	2016-06-15 00:00:00	10	2016-06-13 00:00:00	5	\N	\N
465	276	\N	2016-04-06 10:16:20	Language C++ - CNRS	\N	t	f	2016-04-07 00:00:00	2011-10-09 00:00:00	\N	2016-04-15 00:00:00	12	CNRS	\N
60	276	61	2013-09-19 12:24:47	Français Langue Etrangère, module de 27h - CNRS	\N	t	t	2013-09-19 00:00:00	2009-10-05 00:00:00	27	\N	\N	\N	CNRS
307	276	4	2014-03-25 16:14:05	Initiation aux méthodes de spectroscopie laser pour l'analyse de milieux réactifs	\N	t	t	2014-03-25 00:00:00	2014-09-08 00:00:00	24	2014-06-06 00:00:00	\N	\N	pris en charge par l'organisation
487	276	4	2016-06-17 11:16:04	Initiation aux méthodes de spectroscopie laser pour l'analyse de milieux réactifs	Test du mailer (mail envoyé uniquement à correspondants-formation@laplace.univ-tlse.fr)	t	f	\N	2016-06-25 00:00:00	10	2016-06-19 00:00:00	3	\N	\N
488	276	\N	2016-06-20 09:30:26	Test	Test	f	f	\N	\N	\N	\N	\N	\N	\N
490	276	\N	2016-06-20 09:35:56	Test	Test	f	f	\N	\N	\N	\N	\N	\N	\N
491	276	\N	2016-06-20 09:36:30	Test	Test	f	f	\N	\N	\N	\N	\N	\N	\N
493	276	\N	2016-06-20 09:37:44	Test	Test	f	f	\N	\N	\N	\N	\N	\N	\N
494	276	\N	2016-06-20 09:37:59	Test	Test	f	f	\N	\N	\N	\N	\N	\N	\N
495	276	\N	2016-06-20 09:39:50	Test	Test	f	f	\N	\N	\N	\N	\N	\N	\N
496	276	\N	2016-06-20 09:51:14	Test2	Test2	f	f	\N	\N	\N	\N	\N	\N	\N
497	276	\N	2016-06-20 10:13:11	Test3	Test3	f	f	\N	\N	\N	\N	\N	\N	\N
498	276	\N	2016-06-20 10:14:05	Test4	Test4	f	f	\N	\N	\N	\N	\N	\N	\N
500	276	\N	2016-06-21 14:46:58	TestAlpha	\N	f	f	\N	\N	\N	\N	\N	\N	\N
501	276	\N	2016-06-21 14:50:37	DemandeAlpha1	\N	f	f	\N	\N	\N	\N	\N	\N	\N
502	276	\N	2016-06-21 16:01:45	TestOmega	\N	f	f	\N	\N	\N	\N	\N	\N	\N
503	276	\N	2016-06-21 16:02:20	TestOmegaFinal	\N	f	f	\N	\N	\N	\N	\N	\N	\N
489	276	\N	2016-06-20 09:31:50	Test	Test	t	f	2016-06-23 00:00:00	\N	\N	\N	\N	\N	\N
492	276	\N	2016-06-20 09:37:24	Test	Test	t	f	2016-06-23 00:00:00	\N	\N	\N	\N	\N	\N
\.


--
-- Data for Name: requestsubscription; Type: TABLE DATA; Schema: public; Owner: laplace_formation
--

COPY requestsubscription (request_id, user_id, type_id, subscriptiondate, accepted, attended) FROM stdin;
1	2	1	2013-08-22 08:32:13	t	t
3	2	1	2013-08-22 08:44:42	t	t
4	2	1	2013-08-22 08:46:40	t	t
5	2	1	2013-08-22 08:54:57	t	t
6	2	1	2013-08-22 08:58:11	t	t
9	6	1	2013-09-16 10:30:08	t	t
10	6	1	2013-09-16 10:31:56	t	t
11	6	1	2013-09-16 10:32:21	t	t
12	6	1	2013-09-16 10:32:48	t	t
13	8	3	2013-09-16 10:42:38	t	t
14	8	1	2013-09-16 10:43:53	t	t
15	8	1	2013-09-16 10:44:37	t	t
16	8	3	2013-09-16 10:45:11	t	t
19	2	1	2013-09-16 10:58:26	t	t
17	8	3	2013-09-16 10:51:41	t	t
18	2	1	2013-09-16 10:55:39	t	t
20	2	1	2013-09-16 11:11:46	t	t
21	2	1	2013-09-16 11:12:49	t	t
27	9	1	2013-09-16 11:21:59	t	t
28	9	1	2013-09-16 11:22:22	t	t
24	9	1	2013-09-16 11:19:04	t	t
23	9	1	2013-09-16 11:18:24	t	t
25	9	1	2013-09-16 11:20:02	t	t
22	9	1	2013-09-16 11:17:26	t	t
31	10	1	2013-09-16 11:42:07	t	t
30	10	1	2013-09-16 11:40:08	t	t
29	10	1	2013-09-16 11:39:07	t	t
3	8	1	2013-09-16 10:52:16	t	t
5	5	1	2013-09-16 15:57:36	t	t
32	14	1	2013-09-17 15:52:29	t	t
33	13	1	2013-09-17 16:04:38	t	t
33	12	1	2013-09-17 16:26:36	t	t
59	33	1	2013-09-19 12:23:23	t	t
60	33	1	2013-09-19 12:24:47	t	t
61	33	1	2013-09-19 15:55:12	t	t
61	34	1	2013-09-19 15:58:40	t	t
62	34	1	2013-09-19 16:01:31	t	t
65	34	1	2013-09-19 16:14:58	t	t
64	34	1	2013-09-19 16:14:10	t	t
63	24	1	2013-09-19 16:12:48	t	t
18	8	1	2013-09-19 16:22:13	t	t
33	15	1	2013-09-18 16:53:40	t	t
24	19	1	2013-09-18 17:01:34	t	t
23	19	1	2013-09-18 17:01:58	t	t
22	19	1	2013-09-18 17:02:26	t	t
55	5	1	2013-09-18 17:19:55	t	t
9	21	1	2013-09-18 17:25:41	t	t
56	21	1	2013-09-18 17:26:56	t	t
57	22	1	2013-09-18 17:35:17	t	f
58	22	1	2013-09-18 17:35:56	t	f
66	28	1	2013-09-19 17:17:07	t	t
67	27	1	2013-09-19 17:20:03	t	t
68	23	1	2013-09-19 17:28:23	t	t
80	36	1	2013-09-20 10:10:23	t	t
81	36	1	2013-09-20 10:10:54	t	t
82	36	1	2013-09-20 10:11:30	t	t
83	36	1	2013-09-20 10:12:28	t	t
84	36	1	2013-09-20 10:13:31	t	t
85	36	1	2013-09-20 10:14:47	t	t
86	36	2	2013-09-20 10:15:31	t	t
87	36	1	2013-09-20 10:16:27	t	t
88	36	1	2013-09-20 10:16:57	t	t
95	36	1	2013-09-20 10:21:33	t	f
93	36	1	2013-09-20 10:20:32	t	f
1	36	1	2013-09-20 10:01:33	t	t
75	36	1	2013-09-20 10:03:04	t	t
76	36	1	2013-09-20 10:03:40	t	t
77	36	1	2013-09-20 10:08:34	t	t
78	36	1	2013-09-20 10:09:10	t	t
79	36	1	2013-09-20 10:09:35	t	t
89	36	1	2013-09-20 10:17:20	t	t
120	41	1	2013-09-20 13:37:57	t	t
91	36	1	2013-09-20 10:19:13	t	t
92	36	1	2013-09-20 10:20:02	t	t
94	36	1	2013-09-20 10:21:06	t	t
31	40	1	2013-09-20 11:15:52	t	t
111	40	1	2013-09-20 11:15:27	t	t
118	36	1	2013-09-20 13:33:46	t	t
66	41	1	2013-09-20 13:39:28	t	t
90	36	1	2013-09-20 10:18:09	t	f
130	2	1	2013-09-20 14:23:19	t	t
96	38	1	2013-09-20 10:35:44	t	t
26	9	1	2013-09-16 11:20:45	t	t
97	38	1	2013-09-20 10:36:42	t	t
100	39	1	2013-09-20 10:47:44	t	t
98	38	1	2013-09-20 10:37:46	t	t
122	41	1	2013-09-20 13:40:14	t	f
5	40	1	2013-09-20 11:16:48	t	t
110	39	1	2013-09-20 11:06:14	t	t
101	39	1	2013-09-20 10:48:48	t	t
102	39	1	2013-09-20 10:50:06	t	t
103	39	1	2013-09-20 10:50:40	t	t
97	39	1	2013-09-20 10:50:57	t	t
104	39	1	2013-09-20 10:51:52	t	t
104	38	1	2013-09-20 10:52:56	t	t
106	39	1	2013-09-20 11:00:54	t	t
109	39	1	2013-09-20 11:05:36	t	t
121	41	1	2013-09-20 13:38:47	t	f
86	42	2	2013-09-20 13:48:41	t	t
125	42	1	2013-09-20 13:51:11	t	t
123	42	1	2013-09-20 13:49:59	t	t
124	42	1	2013-09-20 13:50:40	t	t
108	39	1	2013-09-20 11:02:27	t	t
107	39	1	2013-09-20 11:01:19	t	t
98	39	1	2013-09-20 11:04:41	t	t
55	35	1	2013-09-19 17:40:54	t	t
9	40	1	2013-09-20 11:10:29	t	t
119	41	2	2013-09-20 13:36:18	t	t
72	35	1	2013-09-19 17:39:46	t	t
71	35	1	2013-09-19 17:38:46	t	t
70	35	1	2013-09-19 17:37:42	t	t
128	43	1	2013-09-20 14:09:26	t	t
129	37	1	2013-09-20 14:14:42	t	t
127	37	1	2013-09-20 14:13:30	t	t
127	43	1	2013-09-20 14:08:41	t	t
21	37	1	2013-09-20 14:13:39	t	t
21	43	1	2013-09-20 14:10:55	t	t
117	35	1	2013-09-20 13:27:06	t	t
116	35	2	2013-09-20 13:26:24	t	t
115	35	1	2013-09-20 13:25:17	t	t
114	35	2	2013-09-20 13:24:26	t	t
66	35	1	2013-09-20 13:21:47	t	t
111	35	1	2013-09-20 11:14:34	t	t
31	35	1	2013-09-19 17:41:24	t	t
73	35	1	2013-09-19 17:40:33	t	t
105	39	1	2013-09-20 11:00:04	t	t
85	42	2	2013-09-20 13:49:00	t	t
126	42	2	2013-09-20 13:52:02	t	t
113	35	2	2013-09-20 13:22:38	t	t
139	46	1	2013-09-27 08:54:04	t	t
173	56	1	2013-10-10 12:00:26	t	t
194	75	1	2013-10-11 18:10:06	t	t
9	65	1	2013-10-11 17:00:18	t	t
180	57	1	2013-10-11 15:33:45	t	t
177	56	1	2013-10-10 13:51:49	t	t
183	57	1	2013-10-11 15:19:58	t	t
25	56	1	2013-10-10 13:50:48	t	t
193	73	1	2013-10-11 17:57:25	t	t
163	54	1	2013-10-07 16:13:32	t	t
9	67	1	2013-10-11 17:02:12	t	t
188	63	2	2013-10-11 16:32:50	t	t
189	69	1	2013-10-11 17:15:50	t	t
186	59	1	2013-10-11 16:15:43	t	t
188	61	2	2013-10-11 16:33:22	t	t
187	60	2	2013-10-11 16:25:24	t	t
188	62	2	2013-10-11 16:31:11	t	t
148	64	1	2013-10-11 16:44:30	t	t
165	55	1	2013-10-10 11:05:26	t	t
166	55	1	2013-10-10 11:06:07	t	t
148	4	1	2013-10-04 09:14:55	t	t
1	41	1	2013-09-20 13:30:38	t	t
1	54	1	2013-10-07 16:04:04	t	t
168	55	1	2013-10-10 11:07:28	t	t
167	55	1	2013-10-10 11:06:40	t	t
179	57	1	2013-10-11 15:31:53	t	t
179	56	1	2013-10-10 16:36:15	t	t
169	56	1	2013-10-10 16:40:52	t	t
185	57	1	2013-10-11 15:20:52	t	t
31	51	2	2013-10-04 08:43:58	t	t
28	57	1	2013-10-11 15:32:07	t	t
180	56	1	2013-10-10 16:41:32	t	t
28	56	1	2013-10-10 16:35:11	t	t
22	57	1	2013-10-10 16:46:04	t	t
175	56	1	2013-10-10 13:47:41	t	t
172	57	1	2013-10-10 16:48:37	t	t
175	57	1	2013-10-10 16:55:43	t	t
174	56	1	2013-10-10 12:01:18	t	t
24	57	1	2013-10-10 16:43:35	t	t
173	57	1	2013-10-10 16:48:42	t	t
171	56	1	2013-10-10 11:57:31	t	t
181	57	1	2013-10-10 16:51:49	t	t
26	56	1	2013-10-10 13:51:12	t	t
174	58	1	2013-10-11 16:04:54	t	t
182	57	1	2013-10-10 16:58:09	t	t
137	35	1	2013-10-11 17:05:00	t	t
199	81	1	2013-10-14 09:24:23	t	t
184	57	1	2013-10-11 15:20:26	t	t
23	57	1	2013-10-10 16:43:57	t	t
176	56	1	2013-10-10 13:48:43	t	t
149	4	1	2013-10-04 09:16:14	t	t
23	56	1	2013-10-10 11:55:59	t	t
188	60	2	2013-10-11 16:34:20	t	t
174	57	1	2013-10-10 16:54:49	t	t
24	56	1	2013-10-10 11:49:03	t	t
176	57	1	2013-10-10 16:48:39	t	t
9	66	1	2013-10-11 17:01:18	t	t
197	78	1	2013-10-14 08:52:25	t	t
201	64	1	2013-10-14 09:40:34	t	t
164	55	1	2013-10-10 11:04:58	t	t
146	49	1	2013-10-04 08:39:51	t	t
14	67	1	2013-10-11 17:39:40	t	t
192	67	1	2013-10-11 17:38:39	t	t
195	76	3	2013-10-11 18:14:14	t	t
85	55	1	2013-10-10 11:08:43	t	t
170	56	1	2013-10-10 11:47:19	t	t
196	77	1	2013-10-14 08:47:56	t	t
164	58	1	2013-10-11 16:50:06	t	t
22	56	1	2013-10-10 11:56:32	t	t
178	56	1	2013-10-10 13:52:26	t	t
194	74	1	2013-10-11 18:08:54	t	t
169	55	1	2013-10-10 11:09:33	t	t
200	82	1	2013-10-14 09:31:34	t	t
134	44	1	2013-09-20 14:29:37	t	t
64	86	1	2013-10-14 09:52:07	t	t
201	84	1	2013-10-14 09:40:06	t	t
9	68	1	2013-10-11 17:03:09	t	t
172	56	1	2013-10-10 11:59:50	t	t
201	83	1	2013-10-14 09:39:25	t	t
201	63	1	2013-10-14 09:34:06	t	t
202	77	1	2013-10-14 09:45:36	t	t
64	85	1	2013-10-14 09:51:24	t	t
171	57	1	2013-10-10 16:48:34	t	t
159	53	1	2013-10-04 14:07:13	t	t
84	53	1	2013-10-04 14:06:10	t	t
158	53	1	2013-10-04 14:04:46	t	t
157	53	1	2013-10-04 14:04:06	t	t
156	53	1	2013-10-04 14:02:48	t	t
155	53	1	2013-10-04 14:02:03	t	t
76	53	1	2013-10-04 14:00:57	t	t
154	53	1	2013-10-04 14:00:24	t	t
20	54	1	2013-10-07 16:07:06	t	t
20	46	1	2013-09-27 08:55:35	t	t
198	79	1	2013-10-14 09:03:27	t	t
137	70	1	2013-10-11 17:11:25	t	t
21	44	1	2013-09-20 14:28:59	t	t
133	44	1	2013-09-20 14:28:34	t	t
132	44	1	2013-09-20 14:27:55	t	t
127	44	1	2013-09-20 14:27:20	t	t
131	44	1	2013-09-20 14:26:44	t	t
189	71	1	2013-10-11 17:17:17	t	t
145	49	1	2013-10-04 08:38:11	t	t
143	50	1	2013-10-03 11:41:30	t	t
137	69	1	2013-10-11 17:08:07	t	t
162	54	1	2013-10-07 16:09:10	t	t
66	54	1	2013-10-07 16:08:14	t	t
121	54	1	2013-10-07 16:07:27	t	t
161	54	1	2013-10-07 16:06:20	t	t
138	54	1	2013-10-07 16:05:24	t	t
160	54	1	2013-10-07 16:05:03	t	t
153	52	1	2013-10-04 11:04:12	t	t
136	46	1	2013-09-27 08:47:21	t	t
140	46	1	2013-09-27 08:57:20	t	t
141	46	1	2013-09-27 08:59:29	t	t
137	46	1	2013-09-27 08:49:48	t	t
138	46	1	2013-09-27 08:52:45	t	t
105	46	1	2013-09-27 08:56:02	t	t
72	46	1	2013-10-04 09:22:08	t	t
152	52	1	2013-10-04 11:03:12	t	t
130	43	1	2013-09-20 14:24:48	t	t
16	73	3	2013-10-11 17:55:10	t	t
187	61	2	2013-10-11 16:27:55	t	t
150	4	1	2013-10-04 09:16:41	t	t
199	80	1	2013-10-14 09:14:27	t	t
146	51	1	2013-10-04 08:49:41	t	t
203	63	1	2013-10-14 10:03:57	t	t
64	87	1	2013-10-14 09:54:11	t	t
203	90	1	2013-10-14 10:05:10	t	t
226	134	1	2013-10-14 17:43:58	t	t
203	93	1	2013-10-14 10:09:05	t	t
30	69	1	2013-10-14 10:11:40	t	t
66	131	1	2013-10-14 17:32:57	t	t
72	69	1	2013-10-14 11:26:56	t	t
64	84	1	2013-10-14 09:49:30	t	t
204	94	2	2013-10-14 10:15:07	t	t
207	94	1	2013-10-14 10:59:15	t	t
214	101	1	2013-10-14 11:48:42	t	t
81	96	1	2013-10-14 10:51:00	t	t
33	116	1	2013-10-14 17:59:08	t	t
205	76	2	2013-10-14 10:18:38	t	t
66	103	1	2013-10-14 16:30:34	t	t
66	81	1	2013-10-14 16:31:09	t	t
55	107	1	2013-10-14 14:26:51	t	t
157	78	1	2013-10-14 11:30:23	t	t
56	98	1	2013-10-14 11:24:53	t	t
211	74	1	2013-10-14 11:19:15	t	t
56	65	1	2013-10-14 11:22:03	t	t
216	57	3	2013-10-14 12:09:52	t	t
56	66	1	2013-10-14 11:22:46	t	t
31	69	1	2013-10-14 15:34:53	t	t
231	115	2	2013-10-14 17:52:52	t	t
212	89	2	2013-10-14 11:34:13	t	t
157	99	1	2013-10-14 11:31:42	t	t
157	82	1	2013-10-14 11:29:54	t	t
132	95	1	2013-10-14 10:26:36	t	t
206	76	3	2013-10-14 10:44:23	t	t
215	102	1	2013-10-14 11:50:54	t	t
56	67	1	2013-10-14 11:23:11	t	t
31	71	1	2013-10-14 15:37:11	t	t
114	82	1	2013-10-14 18:10:21	t	f
122	104	1	2013-10-15 15:28:43	t	t
220	77	1	2013-10-14 14:56:05	t	t
55	108	1	2013-10-14 14:28:20	t	t
105	111	1	2013-10-14 16:22:27	t	t
223	119	1	2013-10-14 16:08:04	t	t
229	35	1	2013-10-14 17:47:27	t	t
55	105	1	2013-10-14 14:15:06	t	t
55	106	1	2013-10-14 14:17:03	t	t
66	129	1	2013-10-14 17:31:47	t	t
55	82	1	2013-10-14 14:29:28	t	t
66	110	1	2013-10-14 17:19:10	t	t
73	82	1	2013-10-14 14:02:12	t	t
219	77	1	2013-10-14 14:55:33	t	t
21	59	1	2013-10-14 12:14:50	t	t
105	71	1	2013-10-14 16:23:28	t	t
66	128	1	2013-10-14 17:31:17	t	t
203	92	1	2013-10-14 10:08:19	t	t
139	112	1	2013-10-14 15:00:27	t	t
139	59	1	2013-10-14 14:59:43	t	t
139	66	1	2013-10-14 14:59:12	t	t
139	111	1	2013-10-14 14:58:47	t	t
218	82	1	2013-10-14 14:51:05	t	t
111	114	1	2013-10-14 15:04:44	t	t
240	144	1	2013-10-15 15:26:07	t	t
221	115	1	2013-10-14 15:26:12	t	f
66	123	1	2013-10-14 17:29:10	t	t
31	113	1	2013-10-14 15:34:24	t	t
111	82	1	2013-10-14 15:02:29	t	t
208	38	1	2013-10-14 11:09:33	t	t
86	39	1	2013-09-20 11:01:42	t	t
5	116	1	2013-10-14 15:49:54	t	t
5	111	1	2013-10-14 15:46:48	t	t
5	11	1	2013-10-14 15:50:36	t	t
55	110	1	2013-10-14 14:33:21	t	t
223	3	1	2013-10-14 15:54:56	t	t
227	67	1	2013-10-14 17:47:48	t	t
223	120	1	2013-10-14 16:08:40	t	t
223	118	1	2013-10-14 16:07:38	t	t
66	130	1	2013-10-14 17:32:26	t	t
237	139	2	2013-10-15 14:54:07	t	t
203	91	1	2013-10-14 10:07:25	t	t
122	129	1	2013-10-15 15:29:12	t	t
56	68	1	2013-10-14 11:23:56	t	t
225	71	1	2013-10-14 17:40:24	t	t
55	77	1	2013-10-14 14:05:22	t	t
55	103	1	2013-10-14 14:04:59	t	t
105	68	1	2013-10-14 16:23:52	t	t
217	71	1	2013-10-14 12:11:50	t	t
235	137	1	2013-10-14 18:04:27	t	t
55	109	1	2013-10-14 14:30:49	t	t
66	127	1	2013-10-14 17:30:52	t	t
66	125	1	2013-10-14 17:29:57	t	t
209	57	1	2013-10-14 11:14:29	t	t
207	97	1	2013-10-14 10:58:16	t	t
210	39	1	2013-10-14 11:15:26	t	t
20	111	1	2013-10-14 17:36:58	t	t
20	116	1	2013-10-14 17:37:31	t	t
20	132	1	2013-10-14 17:38:50	t	t
146	71	1	2013-10-15 15:06:39	t	t
223	122	1	2013-10-14 16:21:01	t	t
236	35	1	2013-10-15 14:51:53	t	t
238	137	1	2013-10-15 14:57:14	t	t
230	74	1	2013-10-14 17:50:40	t	t
105	66	1	2013-10-14 16:22:56	t	t
31	82	1	2013-10-14 15:35:45	t	t
232	115	2	2013-10-14 17:53:22	t	t
66	126	1	2013-10-14 17:30:22	t	t
33	135	1	2013-10-14 17:58:17	t	t
239	46	1	2013-10-15 15:18:01	t	t
228	78	1	2013-10-14 17:48:07	t	t
66	124	1	2013-10-14 17:29:33	t	t
230	82	1	2013-10-14 17:50:16	t	t
57	138	1	2013-10-14 18:15:02	t	f
58	138	1	2013-10-14 18:15:17	t	f
213	100	1	2013-10-14 11:40:38	t	t
72	70	1	2013-10-14 11:27:31	t	t
234	136	1	2013-10-14 18:00:51	t	t
238	86	1	2013-10-15 14:57:40	t	t
31	49	1	2013-10-14 15:36:22	t	t
222	52	1	2013-10-14 15:27:41	t	t
5	143	1	2013-10-15 15:23:44	t	t
5	142	1	2013-10-15 15:22:49	t	t
5	141	1	2013-10-15 15:20:55	t	t
5	140	1	2013-10-15 15:19:56	t	t
31	114	1	2013-10-14 15:36:44	t	t
55	104	1	2013-10-14 14:06:23	t	t
111	113	1	2013-10-14 15:03:29	t	t
122	124	1	2013-10-15 15:28:13	t	t
64	89	1	2013-10-14 09:56:20	t	t
64	88	1	2013-10-14 09:55:32	t	t
6	59	1	2013-10-15 15:34:32	t	t
243	145	1	2013-10-15 16:10:45	t	t
352	59	1	2014-09-08 10:30:31	t	t
357	71	1	2014-09-24 14:34:28	t	t
144	71	1	2013-10-15 16:27:40	t	t
271	71	1	2014-01-13 18:32:37	t	t
350	4	1	2014-08-29 16:18:16	t	t
289	69	1	2014-03-04 14:59:43	t	t
130	37	1	2013-09-20 14:25:24	t	t
279	53	3	2014-02-10 15:58:59	t	t
334	57	1	2014-05-15 11:39:34	t	t
340	106	1	2014-05-28 10:49:33	t	t
251	2	1	2013-10-15 16:29:44	t	t
248	77	2	2013-10-15 16:24:36	t	f
290	37	1	2014-03-04 15:05:29	t	t
291	37	1	2014-03-04 15:06:52	t	t
292	37	1	2014-03-04 15:07:58	t	t
227	68	1	2013-10-16 10:56:42	t	t
5	68	1	2013-10-16 10:58:50	t	t
130	44	1	2013-10-16 08:27:52	t	t
245	71	1	2013-10-15 16:13:47	t	t
250	71	1	2013-10-15 16:27:23	t	t
223	149	1	2013-10-16 09:12:25	t	t
257	81	1	2013-11-12 15:19:25	t	t
295	44	1	2014-03-04 15:15:57	t	t
191	2	1	2013-12-11 11:59:22	t	t
294	44	1	2014-03-04 15:13:21	t	t
191	103	1	2013-12-11 17:05:09	t	t
191	111	1	2013-12-11 12:10:53	t	t
191	54	1	2013-12-11 17:10:22	t	t
191	41	1	2013-12-16 17:46:40	t	t
191	154	1	2013-12-16 17:47:07	t	t
191	81	1	2013-12-16 17:46:05	t	t
191	11	1	2013-12-16 17:45:22	t	t
191	159	1	2013-12-16 17:54:35	t	t
191	160	1	2013-12-16 17:57:52	t	f
191	161	1	2013-12-16 18:00:54	t	t
323	2	1	2014-04-24 14:50:45	t	t
280	53	3	2014-02-10 16:01:57	t	t
293	44	1	2014-03-04 15:12:58	t	t
269	162	1	2014-01-21 10:54:20	t	t
269	155	1	2013-12-11 18:02:23	t	t
269	158	1	2013-12-05 15:52:25	t	t
269	163	1	2014-01-21 11:35:23	t	t
269	165	1	2014-01-22 08:42:00	t	t
269	164	1	2014-01-22 08:44:10	t	t
269	50	1	2014-01-22 08:45:22	t	t
239	69	1	2013-10-15 15:32:13	t	t
241	69	1	2013-10-15 15:38:01	t	t
252	69	1	2013-10-15 16:30:50	t	t
246	116	1	2013-10-15 16:19:37	t	t
247	104	1	2013-10-15 16:22:21	t	t
247	147	1	2013-10-15 16:23:05	t	t
246	112	1	2013-10-15 16:19:17	t	t
147	112	1	2013-10-15 16:34:40	t	t
269	156	1	2014-01-24 16:11:11	t	t
147	46	1	2013-10-15 16:33:05	t	t
269	166	1	2014-02-07 09:08:44	t	t
244	146	1	2013-10-15 16:12:05	t	t
147	43	1	2013-10-15 16:34:21	t	t
296	43	1	2014-03-04 15:22:39	t	t
297	97	1	2014-03-04 15:28:31	t	t
297	144	1	2014-03-04 15:29:18	t	t
298	35	1	2014-03-04 15:37:16	t	t
299	167	1	2014-03-04 15:43:54	t	t
299	168	1	2014-03-04 15:46:01	t	t
300	169	1	2014-03-04 15:50:18	t	t
300	170	1	2014-03-04 15:51:57	t	t
300	171	1	2014-03-04 15:53:54	t	t
109	38	1	2014-03-05 10:56:21	t	t
302	38	1	2014-03-05 11:02:33	t	t
301	38	1	2014-03-05 11:01:01	t	t
303	38	1	2014-03-05 11:06:03	t	t
277	53	1	2014-01-20 10:55:41	t	t
304	166	1	2014-04-09 15:00:09	t	t
314	4	1	2014-04-17 11:32:06	t	t
352	4	1	2014-09-15 15:49:33	t	t
304	50	1	2014-04-09 15:01:16	t	t
271	49	2	2013-12-19 11:06:34	t	t
271	51	1	2013-12-19 11:22:34	t	t
333	57	1	2014-05-15 11:27:32	t	t
304	177	1	2014-04-09 15:05:11	t	t
305	111	1	2014-03-17 16:02:54	t	t
304	178	1	2014-04-09 15:07:09	t	t
304	179	1	2014-04-09 15:09:02	t	t
304	180	1	2014-04-09 15:15:12	t	t
304	181	1	2014-04-09 15:19:48	t	t
304	182	1	2014-04-09 15:26:36	t	t
253	112	1	2013-10-15 16:35:56	t	f
305	4	1	2014-03-21 14:34:07	t	t
319	42	1	2014-04-18 08:50:33	t	t
318	42	3	2014-04-18 08:50:05	t	t
357	51	1	2014-09-24 14:24:02	t	t
317	42	3	2014-04-18 08:49:33	t	t
316	42	1	2014-04-18 08:48:46	t	t
351	157	1	2014-09-02 16:58:31	t	t
147	95	1	2013-10-15 16:33:52	t	t
315	4	1	2014-04-17 11:40:44	t	f
223	148	1	2013-10-16 08:58:43	t	t
305	68	1	2014-03-17 16:28:35	t	t
326	112	2	2014-05-28 09:23:06	t	\N
339	106	1	2014-05-28 10:34:29	t	t
356	40	1	2014-09-23 11:11:38	t	t
347	183	1	2014-07-23 16:26:50	t	t
346	183	1	2014-07-23 16:06:51	t	t
331	4	2	2014-06-16 10:09:09	t	t
322	4	1	2014-08-28 12:22:49	t	f
335	3	1	2014-05-15 11:39:45	t	t
338	158	1	2014-07-01 08:42:00	t	t
305	116	1	2014-03-17 10:34:54	t	t
249	82	1	2013-10-15 16:25:48	t	f
305	71	1	2014-04-04 15:16:14	t	t
305	156	1	2014-09-15 13:52:59	t	t
246	132	1	2013-10-15 16:20:04	t	t
349	4	1	2014-08-28 12:25:32	t	t
354	41	1	2014-09-19 14:17:12	t	t
354	35	1	2014-09-19 13:14:54	t	t
354	54	1	2014-09-19 14:16:59	t	t
242	67	1	2013-10-15 15:39:09	t	t
320	42	1	2014-04-18 08:51:09	t	t
336	54	1	2014-05-15 13:59:28	t	t
360	186	1	2014-10-07 14:24:36	t	t
360	184	1	2014-10-07 14:28:41	t	t
223	185	1	2014-10-07 14:29:16	t	t
223	132	1	2014-10-07 14:31:30	t	t
360	115	1	2014-10-07 14:33:03	t	t
360	187	1	2014-10-07 14:34:55	t	t
360	188	1	2014-10-07 14:37:52	t	t
360	189	1	2014-10-07 14:40:16	t	t
360	190	1	2014-10-07 14:42:29	t	t
360	191	1	2014-10-07 14:44:44	t	t
360	192	1	2014-10-07 14:46:27	t	t
360	193	1	2014-10-07 14:48:45	t	t
360	194	1	2014-10-07 14:50:24	t	t
360	195	1	2014-10-07 14:53:04	t	t
360	196	1	2014-10-07 14:54:30	t	t
360	197	1	2014-10-07 14:56:26	t	t
360	198	1	2014-10-07 14:58:14	t	t
360	199	1	2014-10-07 14:59:57	t	t
361	200	1	2014-10-07 15:03:04	t	t
361	201	1	2014-10-07 15:05:47	t	t
361	202	1	2014-10-07 15:07:21	t	t
361	128	1	2014-10-07 15:08:44	t	t
361	139	1	2014-10-07 15:09:51	t	t
223	203	1	2014-10-07 15:13:18	t	t
223	204	1	2014-10-07 15:17:17	t	t
223	205	1	2014-10-07 15:19:47	t	t
269	206	1	2014-10-07 15:38:09	t	t
269	207	1	2014-10-07 15:40:51	t	t
362	208	1	2014-10-07 15:58:02	t	t
362	180	1	2014-10-07 15:59:38	t	t
362	209	1	2014-10-07 16:01:40	t	t
362	210	1	2014-10-07 16:02:57	t	t
362	211	1	2014-10-07 16:04:38	t	t
362	212	1	2014-10-07 16:06:13	t	t
362	213	1	2014-10-07 16:07:46	t	t
362	214	1	2014-10-07 16:09:37	t	t
362	215	1	2014-10-07 16:25:06	t	t
362	216	1	2014-10-07 16:28:53	t	t
362	217	1	2014-10-07 16:31:08	t	t
362	218	1	2014-10-07 16:34:07	t	t
362	219	1	2014-10-07 16:35:23	t	t
362	220	1	2014-10-07 16:36:49	t	t
362	221	1	2014-10-07 16:38:10	t	t
269	222	1	2014-10-07 17:01:36	t	t
269	223	1	2014-10-07 17:03:36	t	t
269	224	1	2014-10-07 17:05:32	t	t
269	225	1	2014-10-07 17:07:38	t	t
269	226	1	2014-10-07 17:10:31	t	t
269	227	1	2014-10-07 17:11:58	t	t
269	228	1	2014-10-07 17:13:22	t	t
346	229	1	2014-10-08 10:14:48	t	t
400	39	1	2015-09-07 11:51:18	t	f
346	231	1	2014-10-08 10:25:03	t	t
346	232	1	2014-10-08 10:28:02	t	t
346	233	1	2014-10-08 10:29:43	t	t
346	234	1	2014-10-08 10:32:31	t	t
386	53	2	2015-04-28 14:38:01	t	t
307	174	2	2014-03-25 16:14:33	t	t
367	53	1	2014-10-09 13:53:16	\N	\N
370	46	1	2014-10-24 12:09:32	t	t
375	57	1	2014-11-20 16:05:45	t	t
371	182	1	2014-11-24 11:19:18	t	t
371	236	1	2014-11-24 11:24:17	t	t
371	237	1	2014-11-24 11:27:18	t	t
371	238	1	2014-11-24 11:31:06	t	t
371	239	1	2014-11-24 11:34:19	t	t
371	241	1	2014-11-24 11:44:01	t	t
371	240	1	2014-11-24 11:45:14	t	t
371	242	1	2014-11-24 11:48:29	t	t
371	243	1	2014-11-24 11:52:34	t	t
371	244	1	2014-11-24 11:55:25	t	t
371	245	1	2014-11-24 11:57:45	t	t
371	246	1	2014-11-24 12:00:19	t	t
371	235	1	2014-11-24 12:01:23	t	t
371	231	1	2014-11-24 12:02:27	t	t
371	247	1	2014-11-24 12:05:03	t	t
371	248	1	2014-12-02 16:34:36	t	t
346	249	1	2014-12-02 16:41:24	t	t
377	2	1	2014-12-18 15:58:38	t	t
378	37	1	2014-12-18 16:03:23	t	t
378	2	1	2014-12-18 16:00:52	t	t
378	44	1	2014-12-18 16:03:38	t	t
378	43	1	2014-12-18 16:04:22	t	t
338	149	1	2015-01-16 10:07:23	t	t
356	53	1	2014-10-10 17:59:02	t	f
380	53	1	2015-01-15 11:41:15	t	t
381	53	1	2015-04-02 10:23:45	t	t
382	53	1	2015-04-02 10:25:18	t	t
385	53	2	2015-04-28 14:10:35	t	f
389	44	1	2015-05-12 17:26:26	t	t
384	53	2	2015-04-28 14:09:29	t	t
390	67	1	2015-05-20 13:58:27	t	t
399	112	1	2015-08-31 11:14:24	t	t
395	35	1	2015-06-25 14:11:17	t	t
392	49	1	2015-05-22 08:15:33	t	t
392	51	1	2015-05-22 08:14:18	t	t
392	71	1	2015-05-22 08:15:52	t	t
388	44	1	2015-05-12 17:25:14	t	t
391	52	1	2015-05-22 08:11:32	t	t
391	97	1	2015-05-22 08:10:48	t	t
391	144	1	2015-05-22 08:11:07	t	t
383	53	1	2015-04-28 14:07:39	t	t
376	35	1	2014-12-02 17:28:19	t	t
396	35	1	2015-06-30 11:56:41	t	\N
399	256	1	2015-08-31 11:20:26	t	t
393	252	1	2015-05-22 08:19:20	t	t
394	54	1	2015-06-22 09:18:11	t	t
394	46	1	2015-06-22 09:19:13	t	t
394	11	1	2015-06-22 09:18:54	t	t
394	170	1	2015-06-22 09:19:32	t	t
394	253	1	2015-06-22 09:20:53	t	t
394	254	1	2015-06-22 09:26:56	t	f
399	48	1	2015-08-31 11:21:54	t	t
399	257	1	2015-08-31 11:28:05	t	t
399	258	1	2015-08-31 11:32:49	t	t
399	87	1	2015-08-31 11:30:16	t	t
399	259	1	2015-08-31 11:38:19	t	t
399	260	1	2015-08-31 11:42:08	t	t
399	261	1	2015-08-31 11:44:11	t	t
399	262	1	2015-08-31 11:46:16	t	t
399	263	1	2015-08-31 11:49:36	t	t
399	264	1	2015-08-31 11:51:35	t	t
387	46	1	2015-05-05 13:48:59	t	t
395	106	1	2015-06-26 08:21:22	t	t
376	106	1	2015-09-08 13:01:37	t	t
401	106	1	2015-09-08 09:47:46	t	t
398	136	1	2015-08-26 10:48:12	t	t
379	81	1	2015-01-13 16:38:56	t	t
397	35	1	2015-07-24 09:52:01	t	t
403	68	1	2015-10-12 10:58:23	t	t
403	44	1	2015-10-12 10:57:53	t	t
403	52	1	2015-10-12 10:56:57	t	t
403	149	1	2015-10-12 10:58:39	t	t
403	140	1	2015-10-12 10:57:38	t	t
403	116	1	2015-10-12 10:58:08	t	t
403	111	1	2015-10-12 10:57:12	t	t
403	266	1	2015-10-12 11:02:36	t	t
403	267	1	2015-10-12 11:05:59	t	t
404	37	1	2015-10-12 11:18:41	t	t
404	43	1	2015-10-12 11:18:25	t	t
404	44	1	2015-10-12 11:18:12	t	t
404	2	1	2015-10-12 11:17:54	t	t
405	268	1	2015-10-12 16:15:17	t	t
406	77	1	2015-10-12 16:25:53	t	t
407	97	1	2015-10-12 16:29:25	t	t
408	97	1	2015-10-12 16:31:08	t	t
410	97	1	2015-10-12 16:32:47	t	t
409	97	1	2015-10-12 16:32:13	t	t
411	43	1	2015-10-12 16:43:29	t	t
412	59	1	2015-10-12 16:46:34	t	t
389	59	1	2015-10-12 16:49:14	t	t
415	4	1	2015-10-12 16:55:47	t	t
414	4	1	2015-10-12 16:54:34	t	t
413	4	1	2015-10-12 16:54:03	t	t
416	114	1	2015-10-12 16:59:29	t	t
418	114	1	2015-10-12 17:01:18	t	t
417	114	1	2015-10-12 17:01:07	t	t
421	38	1	2015-10-12 17:08:54	t	t
420	38	1	2015-10-12 17:08:13	t	t
419	38	1	2015-10-12 17:07:52	t	t
422	38	1	2015-10-12 17:11:07	t	t
424	41	1	2015-10-12 17:17:11	t	t
423	41	1	2015-10-12 17:15:00	t	t
125	38	1	2015-10-12 17:06:56	t	t
410	144	1	2015-10-12 17:28:16	t	t
409	144	1	2015-10-12 17:29:39	t	t
425	144	1	2015-10-12 17:30:36	t	t
426	144	1	2015-10-12 17:32:20	t	t
427	37	1	2015-10-12 17:35:05	t	t
420	56	1	2015-10-12 17:39:16	t	t
428	56	1	2015-10-12 17:41:52	t	t
429	56	1	2015-10-12 17:42:55	t	t
430	56	1	2015-10-12 17:45:35	t	t
432	56	1	2015-10-12 17:47:24	t	t
431	56	1	2015-10-12 17:46:59	t	t
435	44	1	2015-10-12 17:55:21	t	t
434	44	1	2015-10-12 17:54:13	t	t
433	44	1	2015-10-12 17:52:41	t	t
436	112	1	2015-10-12 20:58:20	t	t
423	116	1	2015-10-12 21:03:00	t	t
438	11	1	2015-10-12 21:15:56	t	t
437	11	1	2015-10-12 21:15:21	t	t
424	54	1	2015-10-12 21:31:41	t	t
439	57	1	2015-10-12 22:16:50	t	t
440	269	1	2015-10-12 22:24:47	t	t
424	82	1	2015-10-12 22:36:19	t	t
441	82	1	2015-10-12 22:39:04	t	t
442	35	1	2015-10-19 15:10:17	\N	\N
443	54	1	2015-11-02 11:44:25	t	\N
452	39	2	2016-05-31 11:01:58	t	\N
446	49	1	2015-11-24 16:10:25	t	f
446	51	1	2015-11-24 16:10:12	t	t
446	71	1	2015-11-24 16:10:44	t	t
446	82	1	2015-11-24 16:11:01	t	t
469	53	1	2016-04-12 16:29:01	t	\N
444	106	1	2015-11-19 10:50:15	t	t
447	252	3	2016-01-06 14:11:56	t	t
448	54	1	2016-01-21 17:29:48	t	\N
445	106	1	2015-11-19 11:27:49	t	t
449	268	3	2016-01-22 10:24:08	t	\N
470	35	1	2016-05-11 08:30:49	t	\N
451	273	1	2016-02-10 08:44:24	\N	\N
450	81	1	2016-02-10 08:31:55	t	t
452	53	3	2016-02-10 11:31:12	t	\N
454	53	3	2016-02-10 11:34:19	t	\N
455	53	3	2016-02-10 11:35:49	t	\N
471	49	1	2016-05-12 15:40:02	t	\N
457	252	1	2016-02-10 14:20:21	t	t
459	57	1	2016-02-15 14:21:03	t	\N
458	57	1	2016-02-15 14:18:48	t	t
460	97	2	2016-03-01 09:22:42	t	\N
462	54	1	2016-03-01 11:41:46	t	\N
462	82	1	2016-03-01 11:42:01	t	\N
464	157	1	2016-03-25 10:12:41	t	\N
465	43	1	2016-04-06 10:16:20	t	\N
453	53	3	2016-02-10 11:33:08	t	t
456	53	3	2016-02-10 11:38:12	t	f
466	95	3	2016-04-12 11:08:41	t	\N
467	53	1	2016-04-12 16:23:53	t	\N
468	53	1	2016-04-12 16:25:47	t	\N
471	51	1	2016-05-12 15:42:41	t	\N
463	116	2	2016-03-23 14:39:37	t	t
461	116	1	2016-03-01 11:38:45	t	t
472	4	2	2016-05-17 09:24:36	t	t
470	116	2	2016-05-17 14:31:22	t	\N
476	39	1	2016-05-31 10:56:00	t	t
477	39	1	2016-05-31 10:58:14	t	t
475	39	1	2016-05-31 10:54:18	t	t
474	39	1	2016-05-31 10:52:47	t	t
473	39	1	2016-05-31 10:51:10	t	t
486	276	1	2016-06-15 17:37:31	t	t
488	276	3	2016-06-20 09:30:26	\N	\N
489	276	2	2016-06-20 09:31:50	\N	\N
490	276	2	2016-06-20 09:35:56	\N	\N
491	276	3	2016-06-20 09:36:30	\N	\N
493	276	2	2016-06-20 09:37:44	\N	\N
494	276	2	2016-06-20 09:37:59	\N	\N
495	276	2	2016-06-20 09:39:50	\N	\N
496	276	1	2016-06-20 09:51:14	\N	\N
497	276	3	2016-06-20 10:13:11	\N	\N
498	276	2	2016-06-20 10:14:05	\N	\N
500	276	2	2016-06-21 14:46:58	\N	\N
501	276	2	2016-06-21 14:50:37	\N	\N
502	276	3	2016-06-21 16:01:45	\N	\N
503	276	3	2016-06-21 16:02:20	\N	\N
492	276	2	2016-06-20 09:37:24	t	\N
\.


--
-- Data for Name: site; Type: TABLE DATA; Schema: public; Owner: laplace_formation
--

COPY site (id, name) FROM stdin;
1	N7 - bâtiment E
2	UPS - bâtiment 3R2
3	UPS - bâtiment 3R3
4	UPS - bâtiment 3R1
\.


--
-- Data for Name: statutagent; Type: TABLE DATA; Schema: public; Owner: laplace_formation
--

COPY statutagent (id, intitule, groupe) FROM stdin;
11	ATER	INPT/UPS
1	Directeur de Recherche	CNRS
2	Chargé de Recherche	CNRS
3	Professeur	INPT/UPS
5	Ingénieur de Recherche	CRNS/INPT/UPS
6	Ingénieur d'Etudes	CRNS/INPT/UPS
7	Assistant Ingénieur	CRNS/INPT/UPS
8	Technicien	CRNS/INPT/UPS
9	Adjoint Technique	CRNS/INPT/UPS
10	Agent Technique	CRNS/INPT/UPS
12	ADT	CRNS/INPT/UPS
13	CDD	CRNS/INPT/UPS
14	Ingénieur de Recherche CDD	CRNS/INPT/UPS
15	Ingénieur d'Etudes CDD	CRNS/INPT/UPS
4	Maître de Conférences	INPT/UPS
16	Professeur Emérite	INPT/UPS
17	Technicien CDI	CRNS/INPT/UPS
18	Ingénieur de Recherche CDI	CRNS/INPT/UPS
19	ADTP	CRNS/INPT/UPS
\.


--
-- Data for Name: statutdoctorant; Type: TABLE DATA; Schema: public; Owner: laplace_formation
--

COPY statutdoctorant (id, intitule, groupe) FROM stdin;
1	Doctorant	Doctorants
2	Post-Doctorant	Doctorants
3	Stagiaire	Autre
4	Invité	Autre
\.


--
-- Data for Name: thread; Type: TABLE DATA; Schema: public; Owner: laplace_formation
--

COPY thread (id, author_id, need_id, request_id, date, title) FROM stdin;
1	51	52	\N	2013-11-12 10:42:44	Utilisation CFAO Esprit pour usinage Commande Numérique
2	51	52	\N	2013-11-12 10:47:18	Utilisation CFAO Esprit pour usinage Commande Numérique
3	147	\N	247	2014-01-22 14:11:34	formation "Fiabilité thermo-mécanique Epsilon - Alcen
5	106	\N	340	2014-05-28 10:53:53	heures Formation Tutorée à l'Habilitation Electrique
6	106	\N	339	2014-05-28 11:00:12	heures formation initiale "assistant de prévention" (ACMO)
7	183	\N	347	2014-07-23 16:30:26	rectification
8	77	\N	248	2014-09-08 09:55:17	en mission lors de la formation
9	43	\N	21	2014-09-09 09:44:36	Formation effectué
11	43	58	\N	2014-09-10 11:15:54	Programmation interface LABVIEW , debutant et avancé
12	2	\N	357	2014-09-24 14:33:50	N'oublie pas de remplir la fiche d'inscription CNRS
13	51	\N	357	2014-09-24 14:38:18	formation réseau des mécanos!
15	149	\N	338	2015-01-14 09:32:55	Formation effectuée
16	252	5	\N	2015-03-24 14:35:58	Habilitation électrique
17	252	3	\N	2015-03-24 14:39:44	Formation PSC1
18	2	\N	381	2015-04-02 11:03:25	date de la formation
19	106	\N	401	2015-09-08 09:52:05	dates formation
20	106	\N	376	2015-09-08 10:00:59	Participé à cette journée annuelle.
21	81	80	\N	2015-09-09 13:17:00	[VALDEZ] Argumentation
22	35	\N	470	2016-05-11 08:32:34	création EVRP
23	39	\N	400	2016-05-31 09:24:40	re inscription
\.


--
-- Data for Name: thread_user; Type: TABLE DATA; Schema: public; Owner: laplace_formation
--

COPY thread_user (thread_id, user_id) FROM stdin;
1	51
2	51
3	147
5	106
6	106
7	183
8	77
9	43
11	43
13	51
15	149
16	252
17	252
19	106
20	106
21	81
22	35
23	39
\.


--
-- Data for Name: tutelle; Type: TABLE DATA; Schema: public; Owner: laplace_formation
--

COPY tutelle (id, nom) FROM stdin;
1	CNRS
2	INPT
5	Autre
4	UT2-Jean Jaurès
3	UT3- Paul Sabatier
\.


--
-- Data for Name: type; Type: TABLE DATA; Schema: public; Owner: laplace_formation
--

COPY type (id, name, description) FROM stdin;
1	T1	Ici et Maintenant
2	T2	Ici et Demain
3	T3	Ailleurs et Demain
\.


--
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: laplace_formation
--

COPY "user" (id, tutelle_id, site_id, username, emailaddress, gender, name, role, since, telephonenumber1, telephonenumber2, userinfo_id, firstname) FROM stdin;
37	1	3	salon	salon@laplace.univ-tlse.fr	0	SALON Jacques	ROLE_USER	2013-09-20 09:43:45	0561556893	\N	37	prenom
34	1	3	pancheshnyi local		0	PANCHESHNYI Sergey (local) (parti)	ROLE_LIMITED	2013-09-19 15:53:02	\N	\N	34	prenom
68	2	1	roux	roux@laplace.univ-tlse.fr	0	ROUX Nicolas	ROLE_USER	2013-10-11 17:03:01	\N	\N	68	prenom
123	3	3	sili	sili@laplace.univ-tlse.fr	1	SILI Elise	ROLE_USER	2013-10-14 17:20:37	\N	\N	123	prenom
95	3	3	sorin	sorin@laplace.univ-tlse.fr	0	DINCULESCU Sorin	ROLE_USER	2013-10-14 10:26:23	0561558476	\N	95	prenom
59	1	1	mosser	mosser@laplace.univ-tlse.fr	0	MOSSER Franck	ROLE_USER	2013-10-11 16:14:46	0534322352	\N	59	prenom
35	1	3	lantin	lantin@laplace.univ-tlse.fr	0	LANTIN Benoit	ROLE_USER	2013-09-19 16:27:15	0561558472	\N	35	prenom
41	1	3	schlegel	schlegel@laplace.univ-tlse.fr	0	SCHLEGEL Benoit	ROLE_USER	2013-09-20 11:24:37	0561558472	\N	41	prenom
43	1	3	ouahhabi	ouahhabi@laplace.univ-tlse.fr	0	OUAHHABI Nordine	ROLE_USER	2013-09-20 13:55:32	0561556893	\N	43	prenom
44	1	3	martin	martin@laplace.univ-tlse.fr	0	MARTIN Stephane	ROLE_USER	2013-09-20 14:23:09	05 61 55 76 14	\N	44	prenom
47	1	3	belijar	belijar@laplace.univ-tlse.fr	0	BELIJAR Guillaume	ROLE_USER	2013-09-27 15:08:24	0561557529	\N	47	prenom
48	1	3	leveque	leveque@laplace.univ-tlse.fr	0	LEVEQUE Louis	ROLE_USER	2013-10-01 14:13:56	05 61 55 89 87	\N	48	prenom
49	3	2	delmas	delmas@laplace.univ-tlse.fr	0	DELMAS Thierry	ROLE_USER	2013-10-03 11:15:45	0561556862	\N	49	prenom
54	3	3	combettes	combettes@laplace.univ-tlse.fr	1	COMBETTES Celine	ROLE_USER	2013-10-07 10:38:04	05 61 55 89 38	05 34 32 24 18	54	prenom
51	3	2	dallava	dallava@laplace.univ-tlse.fr	0	DALL'AVA Sebastien	ROLE_USER	2013-10-03 15:35:00	0561556862	\N	51	prenom
55	3	3	dziadowiec	dziadowiec@laplace.univ-tlse.fr	1	DZIADOWIEC Myriam (partie)	ROLE_LIMITED	2013-10-10 11:03:26	\N	\N	55	prenom
218	5	1	local_ngotta		0	N GOTTA Patrick (parti)	ROLE_LIMITED	2014-10-07 16:33:57	\N	\N	218	prenom
38	1	1	schwarz	schwarz@laplace.univ-tlse.fr	1	SCHWARZ Valerie	ROLE_USER	2013-09-20 10:02:38	05.34.32.23.94	\N	38	prenom
27	3	2	delatorre		0	DE LA TORRE Jérémie (local) (parti)	ROLE_LIMITED	2013-09-18 17:48:05	\N	\N	27	prenom
3	1	1	cgirard	cgirard@laplace.univ-tlse.fr	1	GIRARD Caroline	ROLE_LIMITED	2013-09-03 18:04:05	05 34 32 23 87	\N	3	prenom
57	3	3	monnereau	monnereau@laplace.univ-tlse.fr	1	MONNEREAU Chirstine	ROLE_USER	2013-10-10 16:43:08	\N	\N	57	prenom
268	3	3	devivo	philippe.de-vivo@laplace.univ-tlse.fr	0	DE VIVO Philippe	ROLE_USER	2015-10-12 16:11:40	\N	\N	268	prenom
53	3	3	biblio	agp@laplace.univ-tlse.fr	1	GAUNIE-PICART Agnès	ROLE_USER	2013-10-04 11:21:37	0561556861	\N	53	prenom
62	3	3	ablart_local		0	ABLART Guy (local) (parti)	ROLE_LIMITED	2013-10-11 16:30:01	\N	\N	62	prenom
22	3	3	eck		1	ECK Emilie (local) (partie)	ROLE_LIMITED	2013-09-18 17:33:44	\N	\N	22	prenom
46	1	1	blaquiere	blaquiere@laplace.univ-tlse.fr	0	BLAQUIERE Jean-Marc	ROLE_USER	2013-09-25 13:07:15	0534322358	\N	46	prenom
52	1	1	jbn	jbn@laplace.univ-tlse.fr	0	BENAIOUN Jacques	ROLE_USER	2013-10-04 10:47:17	0534322363	\N	52	prenom
5	2	1	pigache	pigache@laplace.univ-tlse.fr	0	PIGACHE Francois	ROLE_USER	2013-09-10 14:49:59	2369	\N	5	prenom
40	1	1	harribey	harribey@laplace.univ-tlse.fr	0	HARRIBEY Dominique	ROLE_USER	2013-09-20 10:41:49	05 34 32 23 66	\N	40	prenom
11	2	1	flumian	flumian@laplace.univ-tlse.fr	0	FLUMIAN Didier	ROLE_USER	2013-09-17 10:53:52	05 34 32 24 26	\N	11	prenom
39	2	1	mollma	mollma@laplace.univ-tlse.fr	1	MOLL MAZELLA Catherine	ROLE_USER	2013-09-20 10:02:51	0534322395	\N	39	prenom
42	2	1	bastie	bastie@laplace.univ-tlse.fr	1	BASTIE Carine	ROLE_USER	2013-09-20 11:36:24	0534322403	\N	42	prenom
50	5	1	arossi	arossi@laplace.univ-tlse.fr	0	ROSSI Alberto	ROLE_USER	2013-10-03 11:26:20	0785845146	\N	50	prenom
12	2	1	almustafa		0	ALMUSTAFA Mohamad (local)	ROLE_LIMITED	2013-09-17 15:41:33	\N	\N	12	prenom
2	2	1	pigaglio	pigaglio@laplace.univ-tlse.fr	0	PIGAGLIO Olivier	ROLE_ADMIN	2013-08-01 23:04:05	2383	\N	2	prenom
67	2	1	fontes	fontes@laplace.univ-tlse.fr	0	FONTES Guillaume	ROLE_USER	2013-10-11 17:02:01	\N	\N	67	prenom
65	2	1	astier	astier@laplace.univ-tlse.fr	0	ASTIER Stéphane	ROLE_USER	2013-10-11 17:00:00	\N	\N	65	prenom
6	2	1	mebrek_local		1	MEBREK Fatima (partie)	ROLE_LIMITED	2013-09-16 08:32:42	\N	\N	6	prenom
21	2	1	luga		0	LUGA Gilles (local) (N7-parti)	ROLE_LIMITED	2013-09-18 17:25:22	\N	\N	21	prenom
63	3	3	caquineau	caquineau@laplace.univ-tlse.fr	0	CAQUINEAU Hubert	ROLE_USER	2013-10-11 16:32:31	\N	\N	63	prenom
14	1	3	farenc		0	FARENC Jean (local) (parti)	ROLE_LIMITED	2013-09-17 15:43:23	\N	\N	14	prenom
36	2	1	daguillanes	daguillanes@laplace.univ-tlse.fr	1	DAGUILLANES Cecile (partie)	ROLE_LIMITED	2013-09-20 09:41:43	05 34 32 23 91	\N	36	prenom
9	3	3	curcuru_local		1	CURCURU Sonia (local) (partie)	ROLE_LIMITED	2013-09-16 11:16:32	\N	\N	9	prenom
19	3	3	dedieu		1	DEDIEU Josiane (local) (partie)	ROLE_LIMITED	2013-09-18 17:00:50	\N	\N	19	prenom
74	3	2	ducasse	ducasse@laplace.univ-tlse.fr	0	DUCASSE Olivier	ROLE_USER	2013-10-11 18:07:48	\N	\N	74	prenom
33	3	3	mirzaei_local		0	MIRZAEI Sedighedh (local) (parti)	ROLE_LIMITED	2013-09-19 12:06:31	\N	\N	33	prenom
23	2	1	deandrade		0	DE ANDRADE André (local) (parti)	ROLE_LIMITED	2013-09-18 17:42:39	\N	\N	23	prenom
60	3	3	castelan	castelan@laplace.univ-tlse.fr	0	CASTELAN Philippe	ROLE_USER	2013-10-11 16:24:40	\N	\N	60	prenom
8	1	2	brand_local		1	BRAND Raymonde (local) (partie)	ROLE_LIMITED	2013-09-16 10:41:42	\N	\N	8	prenom
66	2	1	durrieu	durrieu@laplace.univ-tlse.fr	0	DURRIEU DE MADRON Olivier	ROLE_USER	2013-10-11 17:01:09	\N	\N	66	prenom
13	3	2	hamiaz		0	HAMIAZ Adnane (local) (parti)	ROLE_LIMITED	2013-09-17 15:42:39	\N	\N	13	prenom
56	3	3	jarraud	jarraud@laplace.univ-tlse.fr	1	JARRAUD Christine	ROLE_USER	2013-10-10 11:45:52	\N	\N	56	prenom
73	3	3	jolinat	jolinat@laplace.univ-tlse.fr	1	JOLINAT Pascale	ROLE_USER	2013-10-11 17:54:50	\N	\N	73	prenom
71	1	1	larroche	larroche@laplace.univ-tlse.fr	0	LARROCHE Robert	ROLE_USER	2013-10-11 17:17:03	\N	\N	71	prenom
58	1	3	laurent	laurent@laplace.univ-tlse.fr	0	LAURENT Christian	ROLE_USER	2013-10-11 16:04:29	\N	\N	58	prenom
70	1	3	leroy_local		1	LEROY Séverine (local) (partie)	ROLE_LIMITED	2013-10-11 17:09:46	\N	\N	70	prenom
28	3	3	luan local		0	LUAN Hung (local) (parti)	ROLE_LIMITED	2013-09-18 17:48:57	\N	\N	28	prenom
125	3	2	nguyen_local		0	NGUYEN Man-Hai (local) (parti)	ROLE_LIMITED	2013-10-14 17:22:57	\N	\N	125	prenom
127	3	3	pham_local		0	PHAM Cong-Duc (local) (parti)	ROLE_LIMITED	2013-10-14 17:24:06	\N	\N	127	prenom
148	2	1	picot	picot@laplace.univ-tlse.fr	0	PICOT Antoine	ROLE_USER	2013-10-16 08:58:09	\N	\N	148	prenom
129	3	3	profili	profili@laplace.univ-tlse.fr	0	PROFILI Jacopo	ROLE_USER	2013-10-14 17:25:33	\N	\N	129	prenom
119	2	1	rossignol	rossignol@laplace.univ-tlse.fr	0	ROSSIGNOL Timotté	ROLE_LIMITED	2013-10-14 15:57:21	\N	\N	119	prenom
120	2	1	tmartin	tmartin@laplace.univ-tlse.fr	0	MARTIN Thomas	ROLE_LIMITED	2013-10-14 15:57:50	\N	\N	120	prenom
86	1	3	raynaud	raynaud@laplace.univ-tlse.fr	0	RAYNAUD Patrice	ROLE_USER	2013-10-14 09:51:56	\N	\N	86	prenom
135	2	1	roubaid	roubaid@laplace.univ-tlse.fr	1	ISMAIL AL HZZOURY Rania	ROLE_LIMITED	2013-10-14 17:58:06	\N	\N	135	prenom
81	1	3	valdez	valdez@laplace.univ-tlse.fr	0	VALDEZ NAVA Zarel	ROLE_USER	2013-10-14 09:24:01	0561558387	\N	81	prenom
84	1	3	ricard	ricard@laplace.univ-tlse.fr	0	RICARD André	ROLE_USER	2013-10-14 09:39:58	\N	\N	84	prenom
138	3	3	roques	roques@laplace.univ-tlse.fr	1	ROQUES Gisèle	ROLE_USER	2013-10-14 18:14:51	\N	\N	138	prenom
118	2	1	serbia	serbia@laplace.univ-tlse.fr	0	SERBIA Nicola	ROLE_LIMITED	2013-10-14 15:56:43	\N	\N	118	prenom
124	3	1	saysouk	saysouk@laplace.univ-tlse.fr	0	SAYSOUK François	ROLE_USER	2013-10-14 17:21:10	\N	\N	124	prenom
64	3	2	slim_local		0	SLIM Aref (local) (parti)	ROLE_LIMITED	2013-10-11 16:44:07	\N	\N	64	prenom
91	5	3	unfer_local		0	UNFER Thomas (local) (parti)	ROLE_LIMITED	2013-10-14 10:07:16	\N	\N	91	prenom
109	3	3	valensi	valensi@laplace.univ-tlse.fr	0	VALENSI Flavien	ROLE_USER	2013-10-14 14:30:15	\N	\N	109	prenom
112	2	1	vinnac	vinnac@laplace.univ-tlse.fr	0	VINNAC Sébastien	ROLE_USER	2013-10-14 15:00:14	\N	\N	112	prenom
93	3	3	zhu_local		0	ZHU Yu (local) (parti)	ROLE_LIMITED	2013-10-14 10:08:55	\N	\N	93	prenom
78	3	3	griseri	griseri@laplace.univ-tlse.fr	1	GRISERI Virginie	ROLE_USER	2013-10-14 08:49:45	0561557326	\N	78	prenom
108	3	3	razafinimanana	razafinimanana@laplace.univ-tlse.fr	0	RAZAFINIMANANA Manitra	ROLE_USER	2013-10-14 14:27:57	0561556020	\N	108	prenom
82	3	3	trupin	trupin@laplace.univ-tlse.fr	0	TRUPIN Cédric	ROLE_USER	2013-10-14 09:30:59	0561557675	\N	82	prenom
136	1	1	roboam	roboam@laplace.univ-tlse.fr	0	ROBOAM Xavier	ROLE_USER	2013-10-14 18:00:42	05 34 32 24 22	\N	136	prenom
149	3	1	risaletto	risaletto@laplace.univ-tlse.fr	0	RISALETTO Damien	ROLE_USER	2013-10-16 09:12:13	05 34 32 24 12	\N	149	prenom
140	2	1	duhayon	duhayon@laplace.univ-tlse.fr	0	DUHAYON Eric	ROLE_USER	2013-10-15 15:19:31	0534322375	\N	140	prenom
97	1	3	bajon	bajon@laplace.univ-tlse.fr	0	BAJON Patrice	ROLE_USER	2013-10-14 10:57:27	\N	\N	97	prenom
100	3	2	baudoin	baudoin@laplace.univ-tlse.fr	0	BAUDOIN Fulbert	ROLE_USER	2013-10-14 11:39:54	\N	\N	100	prenom
133	3	3	berquez	berquez@laplace.univ-tlse.fr	0	BERQUEZ Laurent	ROLE_USER	2013-10-14 17:42:23	\N	\N	133	prenom
102	3	3	bidan	bidan@laplace.univ-tlse.fr	0	BIDAN Pierre	ROLE_USER	2013-10-14 11:50:15	\N	\N	102	prenom
103	3	3	bley	bley@laplace.univ-tlse.fr	0	BLEY Vincent	ROLE_USER	2013-10-14 14:04:26	\N	\N	103	prenom
111	2	1	bru	bru@laplace.univ-tlse.fr	0	BRU Eric	ROLE_USER	2013-10-14 14:58:15	\N	\N	111	prenom
77	3	3	canale	canale@laplace.univ-tlse.fr	0	CANALE Laurent	ROLE_USER	2013-10-14 08:45:47	\N	\N	77	prenom
87	3	3	cozzolino	cozzolino@laplace.univ-tlse.fr	0	COZZOLINO Raphaël	ROLE_USER	2013-10-14 09:53:55	\N	\N	87	prenom
98	2	1	dagues	dagues@laplace.univ-tlse.fr	0	DAGUES Bruno	ROLE_USER	2013-10-14 11:24:44	\N	\N	98	prenom
134	3	3	caminal_local		0	CAMINAL Gilbert (local) (parti)	ROLE_LIMITED	2013-10-14 17:43:28	\N	\N	134	prenom
104	3	3	diaham	diaham@laplace.univ-tlse.fr	0	DIAHAM Sombel	ROLE_USER	2013-10-14 14:06:05	\N	\N	104	prenom
99	3	2	eichwald	eichwald@laplace.univ-tlse.fr	0	EICHWALD Olivier	ROLE_USER	2013-10-14 11:31:33	\N	\N	99	prenom
101	2	1	fadel	fadel@laplace.univ-tlse.fr	0	FADEL Maurice	ROLE_USER	2013-10-14 11:48:01	\N	\N	101	prenom
114	1	3	fort	fort@laplace.univ-tlse.fr	0	FORT Pierre	ROLE_USER	2013-10-14 15:04:10	\N	\N	114	prenom
139	2	1	guimri_local		0	GUIMRI Mouloud (local) (parti)	ROLE_LIMITED	2013-10-15 14:53:05	\N	\N	139	prenom
90	1	2	hagelaar	hagelaar@laplace.univ-tlse.fr	0	HAGELAAR Gerjan	ROLE_USER	2013-10-14 10:04:42	\N	\N	90	prenom
132	2	1	labach	labach@laplace.univ-tlse.fr	1	LABACH Isabelle	ROLE_USER	2013-10-14 17:38:10	\N	\N	132	prenom
141	2	1	ladoux	ladoux@laplace.univ-tlse.fr	0	LADOUX Philippe	ROLE_USER	2013-10-15 15:20:45	\N	\N	141	prenom
131	3	2	mancio	mancio@laplace.univ-tlse.fr	0	MANCIO REIS Felipe Miguel	ROLE_USER	2013-10-14 17:28:35	\N	\N	131	prenom
107	3	3	masquere	masquere@laplace.univ-tlse.fr	0	MASQUERE Mathieu	ROLE_USER	2013-10-14 14:26:32	\N	\N	107	prenom
76	3	2	merbahi	merbahi@laplace.univ-tlse.fr	0	MERBAHI Nofel	ROLE_USER	2013-10-11 18:13:36	\N	\N	76	prenom
115	1	1	montenon_local		0	MONTENON Alaric (parti)	ROLE_LIMITED	2013-10-14 15:25:26	\N	\N	115	prenom
92	3	3	zakari	zakari@laplace.univ-tlse.fr	0	ZAKARI Mustapha	ROLE_LIMITED	2013-10-14 10:08:08	\N	\N	92	prenom
106	3	2	lluc	lluc@laplace.univ-tlse.fr	0	LLUC Jacques	ROLE_USER	2013-10-14 14:16:42	0561558182	\N	106	prenom
1	1	1	jb	correspondants-formation@laplace.univ-tlse.fr	0	Junior Bonnafous	ROLE_LIMITED	2013-07-25 13:19:33	\N	\N	1	prenom
116	2	1	rallieres	rallieres@laplace.univ-tlse.fr	0	RALLIERES Olivier	ROLE_USER	2013-10-14 15:48:25	05 34 32 24 24	05 34 32 30 94	116	prenom
122	2	1	baubert	baubert@laplace.univ-tlse.fr	0	AUBERT Brice	ROLE_LIMITED	2013-10-14 16:20:40	\N	\N	122	prenom
83	1	3	despax	despax@laplace.univ-tlse.fr	0	DESPAX Bernard	ROLE_LIMITED	2013-10-14 09:39:12	\N	\N	83	prenom
130	3	3	bouchareb	bouchareb@laplace.univ-tlse.fr	0	BOUCHAREB Siham	ROLE_LIMITED	2013-10-14 17:26:09	\N	\N	130	prenom
89	1	3	segui	segui@laplace.univ-tlse.fr	0	SEGUI Yvan	ROLE_LIMITED	2013-10-14 09:56:12	\N	\N	89	prenom
110	3	3	perel	perel@laplace.univ-tlse.fr	0	PEREL Thomas	ROLE_LIMITED	2013-10-14 14:32:39	\N	\N	110	prenom
113	3	2	berge	berge@laplace.univ-tlse.fr	0	BERGE Thierry	ROLE_USER	2013-10-14 15:03:18	\N	\N	113	prenom
142	4	1	sarraute	emmanuel.sarraute@univ-tlse2.fr	0	SARRAUTE Emmanuel	ROLE_USER	2013-10-15 15:21:43	\N	\N	142	prenom
214	5	1	local_annouche		0	ANNOUCHE Amirouche (parti)	ROLE_LIMITED	2014-10-07 16:09:27	\N	\N	214	prenom
150	2	1	medeiros	medeiros@laplace.univ-tlse.fr	1	MEDEIROS Luciana	ROLE_LIMITED	2013-11-04 13:22:01	\N	\N	150	prenom
105	3	2	plavieille	plavieille@laplace.univ-tlse.fr	0	LAVIEILLE Pascal	ROLE_USER	2013-10-14 14:14:36	0561556831	\N	105	prenom
75	3	3	ternisien	ternisien@laplace.univ-tlse.fr	0	TERNISIEN Marc	ROLE_USER	2013-10-11 18:09:54	0561556506	\N	75	prenom
79	5	3	arnaud_local		0	ARNAUD François Xavier (local)	ROLE_LIMITED	2013-10-14 08:59:17	\N	\N	79	prenom
80	1	3	lebey	lebey@laplace.univ-tlse.fr	0	LEBEY Thierry	ROLE_USER	2013-10-14 09:12:00	0561558473	0534322418	80	prenom
145	3	2	barreyre	barreyre@laplace.univ-tlse.fr	1	BARREYRE PANDELE Laure	ROLE_USER	2013-10-15 16:09:51	\N	\N	145	prenom
143	2	1	caux	caux@laplace.univ-tlse.fr	0	CAUX Stéphane	ROLE_USER	2013-10-15 15:23:30	\N	\N	143	prenom
85	3	3	cressault	cressault@laplace.univ-tlse.fr	0	CRESSAULT Yann	ROLE_USER	2013-10-14 09:51:12	\N	\N	85	prenom
24	1	2	bonneval_local		0	BONNEVAL Jean-Luc (local) (parti)	ROLE_LIMITED	2013-09-18 17:46:02	\N	\N	24	prenom
96	5	3	aubes_local		1	AUBES Soraya (local) (partie)	ROLE_LIMITED	2013-10-14 10:50:29	\N	\N	96	prenom
144	1	3	ferre	ferre@laplace.univ-tlse.fr	0	FERRE Patrick	ROLE_USER	2013-10-15 15:25:21	\N	\N	144	prenom
94	1	3	gleizes	gleizes@laplace.univ-tlse.fr	0	GLEIZES Alain	ROLE_USER	2013-10-14 10:14:21	\N	\N	94	prenom
10	3	3	hunel_local		0	HUNEL Julien (local) (parti)	ROLE_LIMITED	2013-09-16 11:27:39	\N	\N	10	prenom
147	1	3	locatelli	locatelli@laplace.univ-tlse.fr	1	LOCATELLI Marie-Laure	ROLE_USER	2013-10-15 16:22:54	\N	\N	147	prenom
146	3	3	marghad	marghad@laplace.univ-tlse.fr	0	MARGHAD Ikbal	ROLE_USER	2013-10-15 16:11:37	\N	\N	146	prenom
61	3	2	mary	mary@laplace.univ-tlse.fr	1	MARY Dominique	ROLE_USER	2013-10-11 16:27:08	\N	\N	61	prenom
126	3	3	mouawad_local		0	MOUAWAD Bassem (local) (parti)	ROLE_LIMITED	2013-10-14 17:23:33	\N	\N	126	prenom
164	5	1	sahri	sahri@laplace.univ-tlse.fr	0	SAHRI Khaldoune	ROLE_USER	2014-01-21 13:30:00	0667313915	\N	164	prenom
152	3	3	freton	freton@laplace.univ-tlse.fr	0	FRETON Pierre	ROLE_USER	2013-11-04 14:26:48	0561556855	\N	152	prenom
153	3	3	milliere	milliere@laplace.univ-tlse.fr	0	MILLIERE Laurent	ROLE_USER	2013-11-12 09:51:06	\N	\N	153	prenom
4	1	1	dbonnafo	dbonnafo@laplace.univ-tlse.fr	0	BONNAFOUS David	ROLE_ADMIN	2013-09-09 16:07:17	\N	\N	4	prenom
154	3	2	liard	liard@laplace.univ-tlse.fr	0	LIARD Laurent	ROLE_USER	2013-11-20 09:53:15	\N	\N	154	prenom
157	3	3	lahoud	lahoud@laplace.univ-tlse.fr	1	LAHOUD Nadine	ROLE_USER	2013-12-02 17:35:03	0561556129	\N	157	prenom
162	2	1	kahalerras	kahalerras@laplace.univ-tlse.fr	0	KAHALERRAS Mohamed Khaled	ROLE_USER	2014-01-21 10:50:59	0651166626	\N	162	prenom
156	2	1	salameh	salameh@laplace.univ-tlse.fr	1	SALAMEH Farah	ROLE_USER	2013-11-25 14:19:05	\N	\N	156	prenom
158	2	1	franc	anne-laure.franc@laplace.univ-tlse.fr	1	FRANC Anne Laure	ROLE_USER	2013-12-05 10:58:22	05 34 32 23 77	\N	158	prenom
159	5	3	le	trung.le@laplace.univ-tlse.fr	0	LE Trung	ROLE_USER	2013-12-16 17:53:51	\N	\N	159	prenom
160	3	2	miscevic	marc.miscevic@laplace.univ-tlse.fr	0	MISCEVIC Marc	ROLE_USER	2013-12-16 17:56:52	\N	\N	160	prenom
165	5	1	byrne	byrne@laplace.univ-tlse.fr	0	BYRNE Benedikt	ROLE_USER	2014-01-21 15:38:32	\N	\N	165	prenom
163	2	1	daquin	daquin@laplace.univ-tlse.fr	1	DAQUIN Priscillia	ROLE_USER	2014-01-21 11:23:05	\N	\N	163	prenom
174	3	3	dap	dap@laplace.univ-tlse.fr	0	DAP Simon	ROLE_USER	2014-03-25 15:23:25	0561558453	\N	174	prenom
155	5	1	morentinetayo	Alvaro.Morentin.Etayo@laplace.univ-tlse.fr	0	MORENTIN ETAYO Alvaro	ROLE_USER	2013-11-25 13:51:03	\N	\N	155	prenom
166	5	1	jarrot	jarrot@laplace.univ-tlse.fr	0	JARROT Damien	ROLE_USER	2014-02-06 18:14:15	\N	\N	166	prenom
180	2	1	allias	Jean-Francois.Allias@laplace.univ-tlse.fr	0	ALLIAS Jean-François	ROLE_USER	2014-04-09 15:10:52	\N	\N	180	prenom
181	2	1	delphin	thomas.delphin@laplace.univ-tlse.fr	0	DELPHIN Thomas	ROLE_USER	2014-04-09 15:18:49	\N	\N	181	prenom
167	1	2	boeuf	jean-pierre.boeuf@laplace.univ-tlse.fr	0	BOEUF Jean-Pierre	ROLE_USER	2014-03-04 15:41:25	\N	\N	167	prenom
168	1	2	makasheva	kremena.makasheva@laplace.univ-tlse.fr	1	MAKASHEVA Kremena	ROLE_USER	2014-03-04 15:45:18	\N	\N	168	prenom
169	1	2	pitchford	leanne.pitchford@laplace.univ-tlse.fr	1	PITCHFORD Leanne	ROLE_USER	2014-03-04 15:49:30	\N	\N	169	prenom
170	1	3	clergereaux	richard.clergereaux@laplace.univ-tlse.fr	0	CLERGEREAUX Richard	ROLE_USER	2014-03-04 15:51:10	\N	\N	170	prenom
171	1	3	gherardi	nicolas.gherardi@laplace.univ-tlse.fr	0	GHERARDI Nicolas	ROLE_USER	2014-03-04 15:53:15	\N	\N	171	prenom
172	3	3	belinger	belinger@laplace.univ-tlse.fr	0	BELINGER Antoine	ROLE_USER	2014-03-20 14:18:40	\N	\N	172	prenom
175	1	1	perrussel	perrussel@laplace.univ-tlse.fr	0	PERRUSSEL Ronan	ROLE_USER	2014-04-02 11:49:53	0534322389	\N	175	prenom
182	2	1	yammine	Samer.Yammine@laplace.univ-tlse.fr	0	YAMMINE Samer	ROLE_USER	2014-04-09 15:25:21	\N	\N	182	prenom
88	3	3	robin	robin@laplace.univ-tlse.fr	0	ROBIN Philippe	ROLE_LIMITED	2013-10-14 09:55:23	\N	\N	88	prenom
69	1	3	boulanger	boulanger@laplace.univ-tlse.fr	0	BOULANGER Alain (parti)	ROLE_LIMITED	2013-10-11 17:07:52	\N	\N	69	prenom
177	1	1	lefevre	yvan.lefevre@laplace.univ-tlse.fr	0	LEFEVRE Yvan	ROLE_USER	2014-04-09 15:04:21	\N	\N	177	prenom
178	4	1	llibre	llibre@laplace.univ-tlse.fr	0	LLIBRE Jean-François	ROLE_USER	2014-04-09 15:06:23	\N	\N	178	prenom
179	2	1	henaux	Carole.Henaux@laplace.univ-tlse.fr	1	HENAUX Carole	ROLE_USER	2014-04-09 15:08:42	\N	\N	179	prenom
183	2	1	hannanohra	hannanohra@laplace.univ-tlse.fr	0	HANNA NOHRA Antoine	ROLE_USER	2014-07-23 16:01:18	\N	\N	183	prenom
185	2	1	local_aos		1	AOS  INNARA Maria (partie)	ROLE_LIMITED	2014-10-07 14:16:04	\N	\N	185	prenom
187	2	1	local_obeid		0	OBEID Ziad (parti)	ROLE_LIMITED	2014-10-07 14:34:30	\N	\N	187	prenom
186	2	1	local_amokrane		0	AMOKRANE Mounir (parti)	ROLE_LIMITED	2014-10-07 14:22:13	\N	\N	186	prenom
184	2	1	local_arbi		0	ARBI Jihen (parti)	ROLE_LIMITED	2014-10-07 14:09:55	\N	\N	184	prenom
188	2	1	local_zeller		0	ZELLER Augustin (parti)	ROLE_LIMITED	2014-10-07 14:37:24	\N	\N	188	prenom
189	5	1	local_charrada		1	CHARRADA Madiha (partie)	ROLE_LIMITED	2014-10-07 14:39:57	\N	\N	189	prenom
190	5	1	local_souley		0	SOULEY Majid (parti)	ROLE_LIMITED	2014-10-07 14:42:13	\N	\N	190	prenom
191	5	1	local_ziani		0	ZIANI Aziz (parti)	ROLE_LIMITED	2014-10-07 14:44:21	\N	\N	191	prenom
192	5	1	local_driard		0	DRIARD Flavien (parti)	ROLE_LIMITED	2014-10-07 14:46:13	\N	\N	192	prenom
193	5	1	local_larcher		0	LARCHER Sébastien (parti)	ROLE_LIMITED	2014-10-07 14:48:31	\N	\N	193	prenom
194	5	1	local_raimondo		0	RAIMONDO Giuliano (parti)	ROLE_LIMITED	2014-10-07 14:50:12	\N	\N	194	prenom
195	2	1	local_achour		0	ACHOUR Tahar (parti)	ROLE_LIMITED	2014-10-07 14:52:46	\N	\N	195	prenom
197	5	1	local_assila		0	ASSILA Ahmed (parti)	ROLE_LIMITED	2014-10-07 14:56:16	\N	\N	197	prenom
196	5	1	local_neffati		0	NEFFATI Ahmed (parti)	ROLE_LIMITED	2014-10-07 14:54:12	\N	\N	196	prenom
198	5	1	local_delhon		0	DELHON Jérémie (parti)	ROLE_LIMITED	2014-10-07 14:58:05	\N	\N	198	prenom
199	5	1	local_medale		0	MEDALE Olivier (parti)	ROLE_LIMITED	2014-10-07 14:59:44	\N	\N	199	prenom
200	5	1	debbou		0	DEBBOU Mustapha (parti)	ROLE_LIMITED	2014-10-07 15:02:54	\N	\N	200	prenom
201	5	1	local_xiao		0	XIAO Zi Jian (parti)	ROLE_LIMITED	2014-10-07 15:05:38	\N	\N	201	prenom
202	5	1	local_videau		0	VIDEAU Nicolas (parti)	ROLE_LIMITED	2014-10-07 15:07:11	\N	\N	202	prenom
128	3	3	bouzidi	bouzidi@laplace.univ-tlse.fr	0	BOUZIDI Mohamed Cherif	ROLE_LIMITED	2013-10-14 17:24:47	\N	\N	128	prenom
213	5	2	local_reviere		0	RIVIERE Patrice (parti)	ROLE_LIMITED	2014-10-07 16:07:35	\N	\N	213	prenom
215	5	1	local_ryndzionek		0	RYNDZIONEK Roland (parti)	ROLE_LIMITED	2014-10-07 16:11:38	\N	\N	215	prenom
204	5	1	geneve		0	GENEVE Thomas	ROLE_USER	2014-10-07 15:16:55	\N	\N	204	prenom
205	5	1	local_kim		1	KIN Sopheaktra (partie)	ROLE_LIMITED	2014-10-07 15:19:34	\N	\N	205	prenom
206	5	1	velazquez	Amanda.Velazquez@laplace.univ-tlse.fr	1	VELAZQUEZ SALAZAR Amanda	ROLE_USER	2014-10-07 15:37:09	\N	\N	206	prenom
216	5	1	local_sienkiewicz		0	SIENKIEWICZ Lukasz (parti)	ROLE_LIMITED	2014-10-07 16:28:40	\N	\N	216	prenom
207	5	1	damdoum	damdoum@laplace.univ-tlse.fr	0	DAMDOUM Amel	ROLE_USER	2014-10-07 15:40:30	\N	\N	207	prenom
208	5	1	gillet	gillet@laplace.univ-tlse.fr	0	GILLET Jules	ROLE_USER	2014-10-07 15:55:42	\N	\N	208	prenom
209	5	1	ounis	ounis@laplace.univ-tlse.fr	0	OUNIS Houdhayfa	ROLE_USER	2014-10-07 16:01:26	\N	\N	209	prenom
210	5	1	havez	havez@laplace.univ-tlse.fr	0	HAVEZ Léon	ROLE_USER	2014-10-07 16:02:46	\N	\N	210	prenom
211	5	1	local_hidalgo		0	HIDALGO Lopez (parti)	ROLE_LIMITED	2014-10-07 16:04:30	\N	\N	211	prenom
212	5	1	local_miravalles		1	MIRAVALLES Soler (partie)	ROLE_LIMITED	2014-10-07 16:06:01	\N	\N	212	prenom
217	5	1	local_vanlaethem		0	VAN LAETHEM Dries (parti)	ROLE_LIMITED	2014-10-07 16:31:00	\N	\N	217	prenom
219	5	1	local_ghenna		0	GHENNA Sofiane (parti)	ROLE_LIMITED	2014-10-07 16:35:13	\N	\N	219	prenom
220	5	1	local_essabbar		0	ESSABBAR Reda (parti)	ROLE_LIMITED	2014-10-07 16:36:37	\N	\N	220	prenom
221	5	1	local_naitsaid		1	NAIT SAID Zahoua (partie)	ROLE_LIMITED	2014-10-07 16:38:01	\N	\N	221	prenom
222	5	1	scheller	Johannes.Scheller@laplace.univ-tlse.fr	0	SCHELLER Johannes	ROLE_USER	2014-10-07 17:01:20	\N	\N	222	prenom
234	5	1	yu	yu@laplace.univ-tlse.fr	0	YU Chenjiang	ROLE_USER	2014-10-08 10:32:20	\N	\N	234	prenom
223	5	1	mfaucher	Mickael.Faucher@laplace.univ-tlse.fr	0	FAUCHER Mickael	ROLE_USER	2014-10-07 17:03:27	\N	\N	223	prenom
224	5	1	mesurolle	Mael.Mesurolle@laplace.univ-tlse.fr	0	MESUROLLE Maël	ROLE_USER	2014-10-07 17:05:20	\N	\N	224	prenom
225	5	1	chrin	Phok.Chrin@laplace.univ-tlse.fr	0	CHRIN Phok	ROLE_USER	2014-10-07 17:07:29	\N	\N	225	prenom
226	5	1	efournier	Etienne.Fournier@laplace.univ-tlse.fr	0	FOURNIER Etienne	ROLE_USER	2014-10-07 17:10:22	\N	\N	226	prenom
227	5	1	local_candeia		0	CANDEIA DE ARAUJO Wellington (parti)	ROLE_LIMITED	2014-10-07 17:11:47	\N	\N	227	prenom
228	5	1	local_aulagnier		0	AULAGNIER Guillaume (parti)	ROLE_LIMITED	2014-10-07 17:13:12	\N	\N	228	prenom
229	5	1	local_ouk		0	OUK Sovannaroith (parti)	ROLE_LIMITED	2014-10-08 10:14:34	\N	\N	229	prenom
231	5	1	diop	Mame.Andallah.Diop@laplace.univ-tlse.fr	0	DIOP Mame Andallah	ROLE_USER	2014-10-08 10:24:53	\N	\N	231	prenom
232	5	1	local_boukadoum		0	BOUKADOUM Redouane (parti)	ROLE_LIMITED	2014-10-08 10:27:53	\N	\N	232	prenom
233	5	1	local_landaburru		0	LANDABURRU Itsane (parti)	ROLE_LIMITED	2014-10-08 10:29:34	\N	\N	233	prenom
236	5	1	khoury	Gabriel.Khoury@laplace.univ-tlse.fr	0	KHOURY Gabriel	ROLE_USER	2014-11-24 11:22:57	\N	\N	236	prenom
237	5	1	jfabre	Joseph.Fabre@laplace.univ-tlse.fr	0	FABRE Joseph	ROLE_USER	2014-11-24 11:26:27	\N	\N	237	prenom
243	5	1	filleau	Clement.Filleau@laplace.univ-tlse.fr	0	FILLEAU Clément	ROLE_USER	2014-11-24 11:52:15	\N	\N	243	prenom
238	5	1	sepulchre	Leopold.Sepulchre@laplace.univ-tlse.fr	0	SEPULCHRE Léopold	ROLE_USER	2014-11-24 11:30:16	\N	\N	238	prenom
239	2	1	jodin	Gurvan.Jodin@laplace.univ-tlse.fr	0	JODIN Gurvan	ROLE_USER	2014-11-24 11:33:51	\N	\N	239	prenom
240	5	1	sanfins	William.Sanfins@laplace.univ-tlse.fr	0	SANFINS William	ROLE_USER	2014-11-24 11:36:08	\N	\N	240	prenom
241	5	1	bkim	bunthern.kim@laplace.univ-tlse.fr	0	KIM Bunthern	ROLE_USER	2014-11-24 11:43:28	\N	\N	241	prenom
242	5	1	flei	Fang.Lei@laplace.univ-tlse.fr	1	LEI Fang	ROLE_USER	2014-11-24 11:48:03	\N	\N	242	prenom
244	5	1	acastelan	Anne.Castelan@laplace.univ-tlse.fr	1	CASTELAN Anne	ROLE_USER	2014-11-24 11:55:04	\N	\N	244	prenom
245	5	1	tliu	Tianyi.Liu@laplace.univ-tlse.fr	0	LIU Tianyi	ROLE_USER	2014-11-24 11:57:23	\N	\N	245	prenom
246	5	1	jywang	Jing-yi.WANG@laplace.univ-tlse.fr	1	WANG Jing-yi	ROLE_USER	2014-11-24 11:59:42	\N	\N	246	prenom
247	5	1	rubino_local		0	RUBINO Luigi (parti) (local)	ROLE_USER	2014-11-24 12:04:52	\N	\N	247	prenom
248	5	1	schmitte	Emiliano.Schmittendorf@laplace.univ-tlse.fr	0	SCHMITTENDORF Emiliano	ROLE_USER	2014-12-02 16:33:46	\N	\N	248	prenom
249	5	1	bouarfa	Abdelkader.Bouarfa@laplace.univ-tlse.fr	0	BOUARFA Abdelkader	ROLE_USER	2014-12-02 16:40:39	\N	\N	249	prenom
250	1	3	leroy	leroy@laplace.univ-tlse.fr	1	LEROY Severine	ROLE_USER	2014-12-03 13:35:33	0561557302	\N	250	prenom
251	3	3	banda	banda@laplace.univ-tlse.fr	0	BANDA Mallys	ROLE_USER	2015-01-19 12:21:25	\N	\N	251	prenom
253	3	3	mlopes	mlopes@laplace.univ-tlse.fr	0	LOPES Manuel	ROLE_USER	2015-06-15 11:09:37	\N	\N	253	prenom
252	3	3	sidor	sidor@laplace.univ-tlse.fr	0	SIDOR Frédéric	ROLE_USER	2015-02-05 12:33:49	05 61 55 84 75	\N	252	prenom
203	5	1	bonnin		0	BONNIN Xavier	ROLE_LIMITED	2014-10-07 15:12:22	\N	\N	203	prenom
254	5	3	alchaddoud	alaa.alchaddoud@laplace.univ-tlse.fr	0	ALCHADDOUD  Alaa	ROLE_USER	2015-06-22 09:26:09	\N	\N	254	prenom
255	5	3	mlaurent	mlaurent@laplace.univ-tlse.fr	1	LAURENT Morgane	ROLE_USER	2015-06-23 13:28:57	0561557677	\N	255	prenom
137	1	1	verhoef	verhoef@laplace.univ-tlse.fr	0	VERHOEF Rick	ROLE_LIMITED	2013-10-14 18:02:40	\N	\N	137	prenom
161	3	1	sanchez	sanchez@laplace.univ-tlse.fr	0	SANCHEZ Sébastien	ROLE_LIMITED	2013-12-16 17:59:36	\N	\N	161	prenom
15	2	1	ahmad	ahmad@laplace.univ-tlse.fr	0	ISMAIL AL HZZOURY Ahmad	ROLE_LIMITED	2013-09-17 15:48:32	\N	\N	15	prenom
256	3	2	blaineau	baptiste.blaineau@laplace.univ-tlse.fr	0	BLAINEAU Baptiste	ROLE_USER	2015-08-31 11:19:42	\N	\N	256	prenom
257	2	1	acasado	Adrian.Casado.Merinero@laplace.univ-tlse.fr	0	CASADO MERINERO Adrian	ROLE_USER	2015-08-31 11:27:27	\N	\N	257	prenom
258	2	1	drhorhi	Ismail.Drhorhi@laplace.univ-tlse.fr	0	DRHORHI Ismail	ROLE_USER	2015-08-31 11:32:22	\N	\N	258	prenom
259	5	3	fetouhi	louiza.fetouhi@laplace.univ-tlse.fr	1	FETOUHI Louiza	ROLE_USER	2015-08-31 11:35:39	\N	\N	259	prenom
260	2	1	jaafar	Amine.Jaafar@laplace.univ-tlse.fr	0	JAAFAR Amine	ROLE_USER	2015-08-31 11:41:40	\N	\N	260	prenom
261	2	1	cmartin	Carlos.Martin.Miramon@laplace.univ-tlse.fr	0	MARTIN MIRAMON Carlos	ROLE_USER	2015-08-31 11:43:42	\N	\N	261	prenom
262	2	1	mogavero	Giorgio.Mogavero@laplace.univ-tlse.fr	0	MOGAVERO Giorgio	ROLE_USER	2015-08-31 11:45:49	\N	\N	262	prenom
263	3	3	riffaud	jonathan.riffaud@laplace.univ-tlse.fr	0	RIFFAUD Jonathan	ROLE_USER	2015-08-31 11:49:02	\N	\N	263	prenom
264	3	2	sokoloff	jerome.sokoloff@laplace.univ-tlse.fr	0	SOKOLOFF Jérôme	ROLE_USER	2015-08-31 11:51:13	\N	\N	264	prenom
265	2	1	raveu	raveu@laplace.univ-tlse.fr	1	RAVEU Nathalie	ROLE_USER	2015-09-02 10:55:29	\N	\N	265	prenom
266	5	1	pulido	Joaquin.Pulido@laplace.univ-tlse.fr	0	PULIDO AGUILAR Joaquin Mauricio	ROLE_USER	2015-10-12 11:01:25	\N	\N	266	prenom
267	5	1	seleme	Isaac.Seleme@laplace.univ-tlse.fr	0	SELEME JUNIOR Isaac	ROLE_USER	2015-10-12 11:04:25	\N	\N	267	prenom
269	3	3	rastelli	rastelli@laplace.univ-tlse.fr	0	RASTELLI Cédric	ROLE_USER	2015-10-12 22:21:48	\N	\N	269	prenom
270	5	3	szczepanski	szczepanski@laplace.univ-tlse.fr	0	Mateusz SZCZEPANSKI	ROLE_USER	2015-10-13 11:56:01	\N	\N	270	prenom
271	2	1	jstekke	jstekke@laplace.univ-tlse.fr	0	Stekke	ROLE_USER	2015-12-01 10:01:15	\N	\N	271	prenom
272	3	2	judee	judee@laplace.univ-tlse.fr	0	JUDEE Florian	ROLE_USER	2016-01-18 15:18:11	\N	\N	272	prenom
273	3	2	chauvin	julie.chauvin@laplace.univ-tlse.fr	1	CHAUVIN Julie	ROLE_USER	2016-02-10 08:34:39	\N	\N	273	prenom
274	1	3	gonzalez	gonzalez@laplace.univ-tlse.fr	0	Gonzalez Jean-Jacques	ROLE_USER	2016-03-08 13:41:34	0561556855	\N	274	prenom
275	3	2	dubois	dubois@laplace.univ-tle.fr	0	Loic DUBOIS	ROLE_USER	2016-05-12 15:40:25	0643326494	\N	275	prenom
235	2	1	morin	morin@laplace.univ-tlse.fr	0	MORIN Benoit	ROLE_USER	2014-10-08 14:26:57	\N	\N	235	prenom
276	2	1	albert	vincent.albert@telecomnancy.eu	0	ALBERT Vincent	ROLE_ADMIN	2016-06-13 13:52:39	\N	\N	276	prenom
\.


--
-- Data for Name: userinfo; Type: TABLE DATA; Schema: public; Owner: laplace_formation
--

COPY userinfo (id, subclass) FROM stdin;
1	0
2	0
3	1
4	0
5	0
6	0
8	0
9	0
10	0
11	0
12	1
13	1
14	0
15	1
19	0
21	0
22	0
23	1
24	0
27	1
28	1
33	1
34	0
35	0
36	0
37	0
38	0
39	0
40	0
41	0
42	0
43	0
44	0
46	0
47	1
48	1
49	0
50	1
51	0
52	0
53	0
54	0
55	0
56	0
57	0
58	0
59	0
60	0
61	0
62	0
63	0
64	1
65	0
66	0
67	0
68	0
69	0
70	0
71	0
73	0
74	0
75	0
76	0
77	0
78	0
79	1
80	0
81	0
82	0
83	0
84	0
85	0
86	0
87	1
88	1
89	0
90	0
91	0
92	1
93	1
94	0
95	0
96	0
97	0
98	0
99	0
100	0
101	0
102	0
103	0
104	0
105	0
106	0
107	0
108	0
109	0
110	1
111	0
112	0
113	0
114	0
115	1
116	0
118	1
119	1
120	1
122	1
123	1
124	1
125	1
126	1
127	1
128	1
129	1
130	1
131	1
132	1
133	0
134	0
135	1
136	0
137	1
138	0
139	1
140	0
141	0
142	0
143	0
144	0
145	1
146	1
147	0
148	0
149	0
150	1
152	0
153	1
154	0
155	1
156	1
157	0
158	0
159	1
160	0
161	1
162	1
163	1
164	1
165	1
166	1
167	0
168	0
169	0
170	0
171	0
172	0
174	0
175	0
177	0
178	0
179	0
180	1
181	1
182	1
183	1
184	1
185	1
186	1
187	1
188	0
189	1
190	1
191	1
192	1
193	1
194	1
195	0
196	1
197	1
198	1
199	1
200	1
201	1
202	1
203	1
204	1
205	1
206	1
207	1
208	1
209	1
210	1
211	1
212	1
213	1
214	1
215	1
216	1
217	1
218	1
219	1
220	1
221	1
222	1
223	1
224	1
225	1
226	1
227	1
228	1
229	1
231	1
232	1
233	1
234	1
235	1
236	1
237	1
238	1
239	1
240	1
241	1
242	1
243	1
244	1
245	1
246	1
247	1
248	1
249	1
250	0
251	1
252	0
253	0
254	1
255	1
256	1
257	1
258	1
259	1
260	0
261	1
262	1
263	1
264	0
265	0
266	0
267	1
268	0
269	0
270	1
271	1
272	1
273	1
274	0
275	1
276	1
\.


--
-- Name: alert_pkey; Type: CONSTRAINT; Schema: public; Owner: laplace_formation; Tablespace: 
--

ALTER TABLE ONLY alert
    ADD CONSTRAINT alert_pkey PRIMARY KEY (id);


--
-- Name: category_pkey; Type: CONSTRAINT; Schema: public; Owner: laplace_formation; Tablespace: 
--

ALTER TABLE ONLY category
    ADD CONSTRAINT category_pkey PRIMARY KEY (id);


--
-- Name: domain_pkey; Type: CONSTRAINT; Schema: public; Owner: laplace_formation; Tablespace: 
--

ALTER TABLE ONLY domain
    ADD CONSTRAINT domain_pkey PRIMARY KEY (id);


--
-- Name: event_pkey; Type: CONSTRAINT; Schema: public; Owner: laplace_formation; Tablespace: 
--

ALTER TABLE ONLY event
    ADD CONSTRAINT event_pkey PRIMARY KEY (id);


--
-- Name: infoagent_pkey; Type: CONSTRAINT; Schema: public; Owner: laplace_formation; Tablespace: 
--

ALTER TABLE ONLY infoagent
    ADD CONSTRAINT infoagent_pkey PRIMARY KEY (id);


--
-- Name: infodoctorant_pkey; Type: CONSTRAINT; Schema: public; Owner: laplace_formation; Tablespace: 
--

ALTER TABLE ONLY infodoctorant
    ADD CONSTRAINT infodoctorant_pkey PRIMARY KEY (id);


--
-- Name: message_pkey; Type: CONSTRAINT; Schema: public; Owner: laplace_formation; Tablespace: 
--

ALTER TABLE ONLY message
    ADD CONSTRAINT message_pkey PRIMARY KEY (id);


--
-- Name: need_pkey; Type: CONSTRAINT; Schema: public; Owner: laplace_formation; Tablespace: 
--

ALTER TABLE ONLY need
    ADD CONSTRAINT need_pkey PRIMARY KEY (id);


--
-- Name: needsubscription_pkey; Type: CONSTRAINT; Schema: public; Owner: laplace_formation; Tablespace: 
--

ALTER TABLE ONLY needsubscription
    ADD CONSTRAINT needsubscription_pkey PRIMARY KEY (need_id, user_id);


--
-- Name: request_pkey; Type: CONSTRAINT; Schema: public; Owner: laplace_formation; Tablespace: 
--

ALTER TABLE ONLY request
    ADD CONSTRAINT request_pkey PRIMARY KEY (id);


--
-- Name: requestsubscription_pkey; Type: CONSTRAINT; Schema: public; Owner: laplace_formation; Tablespace: 
--

ALTER TABLE ONLY requestsubscription
    ADD CONSTRAINT requestsubscription_pkey PRIMARY KEY (request_id, user_id);


--
-- Name: site_pkey; Type: CONSTRAINT; Schema: public; Owner: laplace_formation; Tablespace: 
--

ALTER TABLE ONLY site
    ADD CONSTRAINT site_pkey PRIMARY KEY (id);


--
-- Name: statutagent_pkey; Type: CONSTRAINT; Schema: public; Owner: laplace_formation; Tablespace: 
--

ALTER TABLE ONLY statutagent
    ADD CONSTRAINT statutagent_pkey PRIMARY KEY (id);


--
-- Name: statutdoctorant_pkey; Type: CONSTRAINT; Schema: public; Owner: laplace_formation; Tablespace: 
--

ALTER TABLE ONLY statutdoctorant
    ADD CONSTRAINT statutdoctorant_pkey PRIMARY KEY (id);


--
-- Name: thread_pkey; Type: CONSTRAINT; Schema: public; Owner: laplace_formation; Tablespace: 
--

ALTER TABLE ONLY thread
    ADD CONSTRAINT thread_pkey PRIMARY KEY (id);


--
-- Name: thread_user_pkey; Type: CONSTRAINT; Schema: public; Owner: laplace_formation; Tablespace: 
--

ALTER TABLE ONLY thread_user
    ADD CONSTRAINT thread_user_pkey PRIMARY KEY (thread_id, user_id);


--
-- Name: tutelle_pkey; Type: CONSTRAINT; Schema: public; Owner: laplace_formation; Tablespace: 
--

ALTER TABLE ONLY tutelle
    ADD CONSTRAINT tutelle_pkey PRIMARY KEY (id);


--
-- Name: type_pkey; Type: CONSTRAINT; Schema: public; Owner: laplace_formation; Tablespace: 
--

ALTER TABLE ONLY type
    ADD CONSTRAINT type_pkey PRIMARY KEY (id);


--
-- Name: user_pkey; Type: CONSTRAINT; Schema: public; Owner: laplace_formation; Tablespace: 
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- Name: userinfo_pkey; Type: CONSTRAINT; Schema: public; Owner: laplace_formation; Tablespace: 
--

ALTER TABLE ONLY userinfo
    ADD CONSTRAINT userinfo_pkey PRIMARY KEY (id);


--
-- Name: idx_22c53e08f6203804; Type: INDEX; Schema: public; Owner: laplace_formation; Tablespace: 
--

CREATE INDEX idx_22c53e08f6203804 ON infoagent USING btree (statut_id);


--
-- Name: idx_31204c83427eb8a5; Type: INDEX; Schema: public; Owner: laplace_formation; Tablespace: 
--

CREATE INDEX idx_31204c83427eb8a5 ON thread USING btree (request_id);


--
-- Name: idx_31204c83624af264; Type: INDEX; Schema: public; Owner: laplace_formation; Tablespace: 
--

CREATE INDEX idx_31204c83624af264 ON thread USING btree (need_id);


--
-- Name: idx_31204c83f675f31b; Type: INDEX; Schema: public; Owner: laplace_formation; Tablespace: 
--

CREATE INDEX idx_31204c83f675f31b ON thread USING btree (author_id);


--
-- Name: idx_3b978f9f12469de2; Type: INDEX; Schema: public; Owner: laplace_formation; Tablespace: 
--

CREATE INDEX idx_3b978f9f12469de2 ON request USING btree (category_id);


--
-- Name: idx_3b978f9ff675f31b; Type: INDEX; Schema: public; Owner: laplace_formation; Tablespace: 
--

CREATE INDEX idx_3b978f9ff675f31b ON request USING btree (author_id);


--
-- Name: idx_3bae0aa7a76ed395; Type: INDEX; Schema: public; Owner: laplace_formation; Tablespace: 
--

CREATE INDEX idx_3bae0aa7a76ed395 ON event USING btree (user_id);


--
-- Name: idx_64c19c1115f0ee5; Type: INDEX; Schema: public; Owner: laplace_formation; Tablespace: 
--

CREATE INDEX idx_64c19c1115f0ee5 ON category USING btree (domain_id);


--
-- Name: idx_8d93d649e1847958; Type: INDEX; Schema: public; Owner: laplace_formation; Tablespace: 
--

CREATE INDEX idx_8d93d649e1847958 ON "user" USING btree (tutelle_id);


--
-- Name: idx_8d93d649f6bd1646; Type: INDEX; Schema: public; Owner: laplace_formation; Tablespace: 
--

CREATE INDEX idx_8d93d649f6bd1646 ON "user" USING btree (site_id);


--
-- Name: idx_922cac7a76ed395; Type: INDEX; Schema: public; Owner: laplace_formation; Tablespace: 
--

CREATE INDEX idx_922cac7a76ed395 ON thread_user USING btree (user_id);


--
-- Name: idx_922cac7e2904019; Type: INDEX; Schema: public; Owner: laplace_formation; Tablespace: 
--

CREATE INDEX idx_922cac7e2904019 ON thread_user USING btree (thread_id);


--
-- Name: idx_9ae63821624af264; Type: INDEX; Schema: public; Owner: laplace_formation; Tablespace: 
--

CREATE INDEX idx_9ae63821624af264 ON needsubscription USING btree (need_id);


--
-- Name: idx_9ae63821a76ed395; Type: INDEX; Schema: public; Owner: laplace_formation; Tablespace: 
--

CREATE INDEX idx_9ae63821a76ed395 ON needsubscription USING btree (user_id);


--
-- Name: idx_9ae63821c54c8c93; Type: INDEX; Schema: public; Owner: laplace_formation; Tablespace: 
--

CREATE INDEX idx_9ae63821c54c8c93 ON needsubscription USING btree (type_id);


--
-- Name: idx_b6bd307fe2904019; Type: INDEX; Schema: public; Owner: laplace_formation; Tablespace: 
--

CREATE INDEX idx_b6bd307fe2904019 ON message USING btree (thread_id);


--
-- Name: idx_b6bd307ff675f31b; Type: INDEX; Schema: public; Owner: laplace_formation; Tablespace: 
--

CREATE INDEX idx_b6bd307ff675f31b ON message USING btree (author_id);


--
-- Name: idx_c2028b24f6203804; Type: INDEX; Schema: public; Owner: laplace_formation; Tablespace: 
--

CREATE INDEX idx_c2028b24f6203804 ON infodoctorant USING btree (statut_id);


--
-- Name: idx_d6877578427eb8a5; Type: INDEX; Schema: public; Owner: laplace_formation; Tablespace: 
--

CREATE INDEX idx_d6877578427eb8a5 ON requestsubscription USING btree (request_id);


--
-- Name: idx_d6877578a76ed395; Type: INDEX; Schema: public; Owner: laplace_formation; Tablespace: 
--

CREATE INDEX idx_d6877578a76ed395 ON requestsubscription USING btree (user_id);


--
-- Name: idx_d6877578c54c8c93; Type: INDEX; Schema: public; Owner: laplace_formation; Tablespace: 
--

CREATE INDEX idx_d6877578c54c8c93 ON requestsubscription USING btree (type_id);


--
-- Name: idx_e6f46c4412469de2; Type: INDEX; Schema: public; Owner: laplace_formation; Tablespace: 
--

CREATE INDEX idx_e6f46c4412469de2 ON need USING btree (category_id);


--
-- Name: idx_e6f46c44f675f31b; Type: INDEX; Schema: public; Owner: laplace_formation; Tablespace: 
--

CREATE INDEX idx_e6f46c44f675f31b ON need USING btree (author_id);


--
-- Name: uniq_64c19c15e237e06; Type: INDEX; Schema: public; Owner: laplace_formation; Tablespace: 
--

CREATE UNIQUE INDEX uniq_64c19c15e237e06 ON category USING btree (name);


--
-- Name: uniq_694309e45e237e06; Type: INDEX; Schema: public; Owner: laplace_formation; Tablespace: 
--

CREATE UNIQUE INDEX uniq_694309e45e237e06 ON site USING btree (name);


--
-- Name: uniq_6b5375326c6e55b5; Type: INDEX; Schema: public; Owner: laplace_formation; Tablespace: 
--

CREATE UNIQUE INDEX uniq_6b5375326c6e55b5 ON tutelle USING btree (nom);


--
-- Name: uniq_8cde57295e237e06; Type: INDEX; Schema: public; Owner: laplace_formation; Tablespace: 
--

CREATE UNIQUE INDEX uniq_8cde57295e237e06 ON type USING btree (name);


--
-- Name: uniq_8d93d649e5704da; Type: INDEX; Schema: public; Owner: laplace_formation; Tablespace: 
--

CREATE UNIQUE INDEX uniq_8d93d649e5704da ON "user" USING btree (userinfo_id);


--
-- Name: uniq_8d93d649f85e0677; Type: INDEX; Schema: public; Owner: laplace_formation; Tablespace: 
--

CREATE UNIQUE INDEX uniq_8d93d649f85e0677 ON "user" USING btree (username);


--
-- Name: uniq_a7a91e0b5e237e06; Type: INDEX; Schema: public; Owner: laplace_formation; Tablespace: 
--

CREATE UNIQUE INDEX uniq_a7a91e0b5e237e06 ON domain USING btree (name);


--
-- Name: uniq_e6f46c442b36786b; Type: INDEX; Schema: public; Owner: laplace_formation; Tablespace: 
--

CREATE UNIQUE INDEX uniq_e6f46c442b36786b ON need USING btree (title);


--
-- Name: fk_22c53e08bf396750; Type: FK CONSTRAINT; Schema: public; Owner: laplace_formation
--

ALTER TABLE ONLY infoagent
    ADD CONSTRAINT fk_22c53e08bf396750 FOREIGN KEY (id) REFERENCES userinfo(id) ON DELETE CASCADE;


--
-- Name: fk_22c53e08f6203804; Type: FK CONSTRAINT; Schema: public; Owner: laplace_formation
--

ALTER TABLE ONLY infoagent
    ADD CONSTRAINT fk_22c53e08f6203804 FOREIGN KEY (statut_id) REFERENCES statutagent(id);


--
-- Name: fk_31204c83427eb8a5; Type: FK CONSTRAINT; Schema: public; Owner: laplace_formation
--

ALTER TABLE ONLY thread
    ADD CONSTRAINT fk_31204c83427eb8a5 FOREIGN KEY (request_id) REFERENCES request(id);


--
-- Name: fk_31204c83624af264; Type: FK CONSTRAINT; Schema: public; Owner: laplace_formation
--

ALTER TABLE ONLY thread
    ADD CONSTRAINT fk_31204c83624af264 FOREIGN KEY (need_id) REFERENCES need(id);


--
-- Name: fk_31204c83f675f31b; Type: FK CONSTRAINT; Schema: public; Owner: laplace_formation
--

ALTER TABLE ONLY thread
    ADD CONSTRAINT fk_31204c83f675f31b FOREIGN KEY (author_id) REFERENCES "user"(id);


--
-- Name: fk_3b978f9f12469de2; Type: FK CONSTRAINT; Schema: public; Owner: laplace_formation
--

ALTER TABLE ONLY request
    ADD CONSTRAINT fk_3b978f9f12469de2 FOREIGN KEY (category_id) REFERENCES category(id);


--
-- Name: fk_3b978f9ff675f31b; Type: FK CONSTRAINT; Schema: public; Owner: laplace_formation
--

ALTER TABLE ONLY request
    ADD CONSTRAINT fk_3b978f9ff675f31b FOREIGN KEY (author_id) REFERENCES "user"(id);


--
-- Name: fk_3bae0aa7a76ed395; Type: FK CONSTRAINT; Schema: public; Owner: laplace_formation
--

ALTER TABLE ONLY event
    ADD CONSTRAINT fk_3bae0aa7a76ed395 FOREIGN KEY (user_id) REFERENCES "user"(id);


--
-- Name: fk_64c19c1115f0ee5; Type: FK CONSTRAINT; Schema: public; Owner: laplace_formation
--

ALTER TABLE ONLY category
    ADD CONSTRAINT fk_64c19c1115f0ee5 FOREIGN KEY (domain_id) REFERENCES domain(id);


--
-- Name: fk_8d93d649e1847958; Type: FK CONSTRAINT; Schema: public; Owner: laplace_formation
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT fk_8d93d649e1847958 FOREIGN KEY (tutelle_id) REFERENCES tutelle(id);


--
-- Name: fk_8d93d649e5704da; Type: FK CONSTRAINT; Schema: public; Owner: laplace_formation
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT fk_8d93d649e5704da FOREIGN KEY (userinfo_id) REFERENCES userinfo(id);


--
-- Name: fk_8d93d649f6bd1646; Type: FK CONSTRAINT; Schema: public; Owner: laplace_formation
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT fk_8d93d649f6bd1646 FOREIGN KEY (site_id) REFERENCES site(id);


--
-- Name: fk_922cac7a76ed395; Type: FK CONSTRAINT; Schema: public; Owner: laplace_formation
--

ALTER TABLE ONLY thread_user
    ADD CONSTRAINT fk_922cac7a76ed395 FOREIGN KEY (user_id) REFERENCES "user"(id) ON DELETE CASCADE;


--
-- Name: fk_922cac7e2904019; Type: FK CONSTRAINT; Schema: public; Owner: laplace_formation
--

ALTER TABLE ONLY thread_user
    ADD CONSTRAINT fk_922cac7e2904019 FOREIGN KEY (thread_id) REFERENCES thread(id) ON DELETE CASCADE;


--
-- Name: fk_9ae63821624af264; Type: FK CONSTRAINT; Schema: public; Owner: laplace_formation
--

ALTER TABLE ONLY needsubscription
    ADD CONSTRAINT fk_9ae63821624af264 FOREIGN KEY (need_id) REFERENCES need(id);


--
-- Name: fk_9ae63821a76ed395; Type: FK CONSTRAINT; Schema: public; Owner: laplace_formation
--

ALTER TABLE ONLY needsubscription
    ADD CONSTRAINT fk_9ae63821a76ed395 FOREIGN KEY (user_id) REFERENCES "user"(id);


--
-- Name: fk_9ae63821c54c8c93; Type: FK CONSTRAINT; Schema: public; Owner: laplace_formation
--

ALTER TABLE ONLY needsubscription
    ADD CONSTRAINT fk_9ae63821c54c8c93 FOREIGN KEY (type_id) REFERENCES type(id);


--
-- Name: fk_b6bd307fe2904019; Type: FK CONSTRAINT; Schema: public; Owner: laplace_formation
--

ALTER TABLE ONLY message
    ADD CONSTRAINT fk_b6bd307fe2904019 FOREIGN KEY (thread_id) REFERENCES thread(id);


--
-- Name: fk_b6bd307ff675f31b; Type: FK CONSTRAINT; Schema: public; Owner: laplace_formation
--

ALTER TABLE ONLY message
    ADD CONSTRAINT fk_b6bd307ff675f31b FOREIGN KEY (author_id) REFERENCES "user"(id);


--
-- Name: fk_c2028b24bf396750; Type: FK CONSTRAINT; Schema: public; Owner: laplace_formation
--

ALTER TABLE ONLY infodoctorant
    ADD CONSTRAINT fk_c2028b24bf396750 FOREIGN KEY (id) REFERENCES userinfo(id) ON DELETE CASCADE;


--
-- Name: fk_c2028b24f6203804; Type: FK CONSTRAINT; Schema: public; Owner: laplace_formation
--

ALTER TABLE ONLY infodoctorant
    ADD CONSTRAINT fk_c2028b24f6203804 FOREIGN KEY (statut_id) REFERENCES statutdoctorant(id);


--
-- Name: fk_d6877578427eb8a5; Type: FK CONSTRAINT; Schema: public; Owner: laplace_formation
--

ALTER TABLE ONLY requestsubscription
    ADD CONSTRAINT fk_d6877578427eb8a5 FOREIGN KEY (request_id) REFERENCES request(id);


--
-- Name: fk_d6877578a76ed395; Type: FK CONSTRAINT; Schema: public; Owner: laplace_formation
--

ALTER TABLE ONLY requestsubscription
    ADD CONSTRAINT fk_d6877578a76ed395 FOREIGN KEY (user_id) REFERENCES "user"(id);


--
-- Name: fk_d6877578c54c8c93; Type: FK CONSTRAINT; Schema: public; Owner: laplace_formation
--

ALTER TABLE ONLY requestsubscription
    ADD CONSTRAINT fk_d6877578c54c8c93 FOREIGN KEY (type_id) REFERENCES type(id);


--
-- Name: fk_e6f46c4412469de2; Type: FK CONSTRAINT; Schema: public; Owner: laplace_formation
--

ALTER TABLE ONLY need
    ADD CONSTRAINT fk_e6f46c4412469de2 FOREIGN KEY (category_id) REFERENCES category(id);


--
-- Name: fk_e6f46c44f675f31b; Type: FK CONSTRAINT; Schema: public; Owner: laplace_formation
--

ALTER TABLE ONLY need
    ADD CONSTRAINT fk_e6f46c44f675f31b FOREIGN KEY (author_id) REFERENCES "user"(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

