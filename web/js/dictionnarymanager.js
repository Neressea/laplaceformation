
/**
 * Récupère le dictionnaire de données du serveur et les affiche.
 * 
 * @returns {undefined}
 */
function getData(){
    
    //On remplit les données du dictionnaire
    $.get('/admin/modeles/dictionnaire/config', function(data){
        dico = JSON.parse(data);
        
        dico.forEach(function(element, index){
            
            addRow(element.data_name, element.request, index);
            
        });

    });

}

/**
 * Ajoute une ligne au tableau avec les fonctionnalités qui en découlent
 * (suppression de ligne, modification nom et requête).
 * 
 * @param {type} data_name
 * @param {type} request
 * @param {type} index
 * @param {type} prepend
 * @returns {undefined}
 */
function addRow(data_name, request, index, prepend){
    var name = $('<input type="text" value="' + data_name  + '" placeholder="nom de la donnée" index="' + index + '" />');
    var input = $('<input type="text" value="' + request + '" placeholder="requête SQL" index="' + index + '" />');
    var remove = $('<button id="delete-template" class="btn-template" title="Supprimer la donnée sélectionné"><i class="fa fa-times-circle-o"></i></button>');

    var row = $('<tr><td class="name"></td><td class="request"></td><td class="rm"></td></tr>');

    row.find('.name').append(name);
    row.find('.request').append(input);
    row.find('.rm').append(remove);

    //On fait les modifications en temps réel
    remove.click(function(){
        //On retire la donnée de l'objet
        dico.splice(input.attr('index'), 1);
        
        //On supprime l'élément graphique
        $(this).parent().parent().remove();
        
    });

    name.keyup(function(){
        dico[$(this).attr('index')].data_name = $(this).val();
    });

    input.keyup(function(){
        dico[$(this).attr('index')].request = $(this).val();
    });

    if(prepend == true)
        $('#data-dictionnary').find('tr:first-child').after(row);
    else
        $('#data-dictionnary').append(row);
}

function saveDico(){
    
    $.ajax({
        
        url: '/admin/modeles/dictionnaire/sauvegarder',
        method: 'post',
        data: {dico: JSON.stringify(dico)},
        
    }).done(function(mess){
        
        //On affiche le message envoyé par le serveur
        showDialog('Confirmation', mess);
       
    });
    
}

$(document).ready(function(){
    
    dico  = [];
    getData();
    
    //On active le bouton d'ajout
    $('.add-data').click(function(){
        
        //on ajoute la nouvelle donnée au tableau
        dico.unshift({data_name: '', request: ''});
        
        //On décale tous les index dans la vue
        $('input').each(function(){
            var new_val = parseInt($(this).attr('index')) + 1;
            $(this).attr('index', new_val);
        });
        
        //on crée et on ajoute l'élément graphique
        addRow('', '', 0, true);
    });
    
    $('.save-data').click(saveDico);
});

