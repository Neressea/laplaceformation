
/////////////////// DEBUT DU JAVASCRIPT OBJET //////////////////////


/**
 * Objet mémorisant la localisation des éléments ajoutés par l'utilisateur en javascript.
 * 
 * Cet objet est mis à jour lors du premier chargement avec le contenu du fichier de configuration 
 * à chaque ajout d'élément sur une page.
 * 
 * Il est consulté lors de la navigation entre les pages d'un pdf et entre les pdfs, ainsi que lors 
 * de la sauvegarde.
 * 
 * Cet objet est un tableau de PDFs.
 * 
 * @type type
 */
var MyTemplateManager = function(){
    
    /**
     * Liste des pdfs des modèles
     * @type Array
     */
    this.templates = [];
    
    /**
     * Dictionnaire des données
     */
    this.dictionnary = [];
    
}

function jsonToTemplates(json){
    var templates = [];
    var version_brute = JSON.parse(json);
    
    version_brute.forEach(function(element, index){
        //On crée chaque PDF
        templates.push(jsonToPDF(element));
    });
    
    return templates;
}

function jsonToPDF(element){
    
    var pdf = new PDF(element.name, element.nb_pages);
    
    //on ajoute les textes
    element.texts.forEach(function(subelement, index){
        pdf.texts.push(jsonToText(subelement));
    });
    
    //On ajoute les data
    element.data.forEach(function(subelement, index){
        pdf.data.push(jsonToData(subelement));
    });
    
    return pdf;
}

function jsonToData(element){
    return new Data(element.name, element.pos.page, element.pos.x, element.pos.y, element.width);
}

function jsonToText(element){
    return new Text(element.value, element.pos.page, element.pos.x, element.pos.y);
}

/**
 * 
 * Méthode convertissant l'objet en json
 * 
 * @returns {String} JSON représentant tous ls templates
 */
MyTemplateManager.prototype.toJSON = function(){
    
    var json = '[';
    
    //On parcourt tous les pdfs
    for(var i=0; i<this.templates.length; i++){
        //Pour chaque pdf, on ajoute son json à la chaine
        var pdf = this.templates[i];
        json += pdf.toJSON();
        
        if(i != this.templates.length - 1)
            json += ',';
    }
    
    return json + ']';
    
}

/**
 * Supprime un PDF de la liste à partir de son nom.
 * 
 * @param {type} name Nom du pdf.
 * @returns {undefined}
 */
MyTemplateManager.prototype.delete = function(name){
    var i = 0, found = false;
    
    while (i < this.templates.length && !found){
        
        if(this.templates[i].name == name){
            found = true;
            this.templates.splice(i, 1);
        }
        
        i++;
    }
}

/**
 * méthode permettant d'ajouter facilement un texte depuis l'objet de gestion
 * 
 * @param {type} pdf_name
 * @param {type} page
 * @param {type} x
 * @param {type} y
 * @returns {Number|MyTemplateManager.prototype.addText@arr;templates@pro;texts@pro;length}
 */
MyTemplateManager.prototype.addText = function(pdf_name, page, x, y){

    //On cherche le bon PDF
    var i = 0, found = false, nb_textes = -1;
    while (i < this.templates.length && !found){
        
        //Une fois trouvé, on ajoute l'élément texte
        if(this.templates[i].name == pdf_name){
            found = true;
            
            //on ajoute l'élément texte vide
            nb_textes = this.templates[i].texts.length;
            this.templates[i].texts.push(new Text('', page, (x + 30) / $('.full-page').width(), y / $('.full-page').height()));
        }
        
        i++;
    }

    //Onr etourne le nombre de champ textes : joue le rôle d'ID.
    return nb_textes;
}

/**
 * Modifie le contenu d'un champ de texte à partir de son ID.
 * 
 * @param {type} pdf_name
 * @param {type} id
 * @param {type} value
 * @returns {undefined}
 */
MyTemplateManager.prototype.setText = function(pdf_name, id, value){
    
    //On cherche le bon PDF
    var i = 0, found = false;
    while (i < this.templates.length && !found){
        
        //Une fois trouvé, on modifie l'élément texte
        if(this.templates[i].name == pdf_name){
            found = true;
            
            //on ajoute l'élément texte vide
            this.templates[i].texts[id].setText(value);
        }
        
        i++;
    }
    
}

/**
 * Modifie les coordonnées d'un champ texte dans le gestionnaire
 * (champ toujours sur la même page).
 * 
 * @param {type} pdf_name
 * @param {type} id
 * @param {type} x
 * @param {type} y
 * @returns {undefined}
 */
MyTemplateManager.prototype.moveText = function(pdf_name, id, x, y){
    
    //On cherche le bon PDF
    var i = 0, found = false;
    while (i < this.templates.length && !found){
        
        //Une fois trouvé, on déplace l'élément texte
        if(this.templates[i].name == pdf_name){
            found = true;
            
            //on ajoute l'élément texte vide : on sauvegarde les rapports pour faciliter la conversion pixel / unité PDF
            //On retire les 30px du curseur de déplacement
            this.templates[i].texts[id].pos.x = (x + 30) / $('.full-page').width();
            this.templates[i].texts[id].pos.y = y / $('.full-page').height();
        }
        
        i++;
    }
    
}

/**
 * Supprime le champ texte d'un PDF.
 * 
 * @param {type} pdf_name
 * @param {type} id
 * @returns {undefined}
 */
MyTemplateManager.prototype.deleteText = function(pdf_name, id){
    
    //On cherche le bon PDF
    var i = 0, found = false;
    while (i < this.templates.length && !found){
        
        //Une fois trouvé, on supprime l'élément texte
        if(this.templates[i].name == pdf_name){
            found = true;
            
            this.templates[i].texts.splice(id, 1);
        }
        
        i++;
    }
    
}

/**
 * Dépose une donnée sur la page d'un PDF.
 * 
 * @param {type} data_name Nom de la donnée
 * @param {type} pdf_name Nom du pdf
 * @param {type} page Page du PDF concernée
 * @param {type} x
 * @param {type} y
 * @returns {undefined}
 */
MyTemplateManager.prototype.addData = function(data_name, pdf_name, page, x, y, width){
    
    //On cherche le bon PDF
    var i = 0, found = false;
    while (i < this.templates.length && !found){
        
        //Une fois trouvé, on cherche si la data se trouve déjà dans le manager pour ce PDF
        if(this.templates[i].name == pdf_name){
            found = true;
            
            var j = 0, data_found = false, pdf = this.templates[i];
            
            while (j < pdf.data.length && !data_found){
                
                //Si la donnée se trouvait déjà sur le PDF, on la déplace simplement
                if(pdf.data[j].name == data_name){
                    data_found = true;
                    
                    pdf.data[j].pos = new Position(page, x / $('.full-page').width(), y / $('.full-page').height());
                    pdf.data[j].width = width / $('.full-page').width();
                }
                
                j++;
            }
            
            //Si la donnée n'a pas été trouvée, on l'ajoute
            if(!data_found){
                pdf.data.push(new Data(data_name, page, x / $('.full-page').width(), y / $('.full-page').height(), width / $('.full-page').width()));
            }
        }
        
        i++;
    }
    
}

/**
 * Supprime une donnée du gestionnaire.
 * Appelée quand on remet une donnée dans la boîte à badges.
 * 
 * @param {type} data_name
 * @param {type} pdf_name
 * @returns {undefined}
 */
MyTemplateManager.prototype.deleteData = function(data_name, pdf_name){
    //On cherche le bon PDF
    var i = 0, found = false;
    while (i < this.templates.length && !found){
        
        //Une fois trouvé, on cherche la data 
        if(this.templates[i].name == pdf_name){
            found = true;
            
            var j = 0, data_found = false, pdf = this.templates[i];
            
            while (j < pdf.data.length && !data_found){
                
                //On supprime la donnée quand on la trouve
                if(pdf.data[j].name == data_name){
                   pdf.data.splice(j, 1); 
                }
                
                j++;
            }
        }
        
        i++;
    }
}

/**
 * Récupère les données placées par Twig et initialise le gestionnaire avec.
 * 
 * @returns {undefined}
 */
MyTemplateManager.prototype.feed = function(){
    
    //On récupère le json du serveur
    $.get("/admin/modeles/config", function(data){
        //on transforme le JSON en objet
        var templates = jsonToTemplates(data.templates);
        templateManager.templates = templates;
        templateManager.dictionnary = JSON.parse(data.dico);
        
        //on remplit le .data-badges avec les données
        templateManager.dictionnary.forEach(function(element, index){
            var badge = $('<span class="badge data-badge" style="position: relative;">' + element + '</span>');
            initBadge(badge);
           $('#data-badges').append(badge); 
        });
    });
    
}

/**
 * Renvoie les textes de la page du pdf passée en paramètre.
 * 
 * @param {type} pdf_name Nom du pdf
 * @param {type} num_page Numéro de la page
 * @returns {Array} Tableau des Text.
 */
MyTemplateManager.prototype.setTexts = function(pdf_name, num_page){
    
    //On cherche le bon PDF
    var i = 0; 
    var found = false;
    
    while (i < this.templates.length && !found){
        
        var pdf = this.templates[i];
        
        //Une fois trouvé, on parcours les textes et on cherche ceux de la page désirée
        if(pdf.name == pdf_name){
            found = true;
            
            //On parcourt tous les textes
            for(var j = 0; j < pdf.texts.length; j++){
                
                var txt = pdf.texts[j];
                
                //Si le texte est sur la page voulue, on l'ajoute
                if(txt.pos.page == num_page){
                    addInput(j, txt.pos.x * $('.full-page').width() - 30, txt.pos.y * $('.full-page').height(), txt.value);
                }
            }
        }
        
        i++;
    }
}


/**
 * 
 * Classe représentant les PDFs.
 * 
 * @param {string} name le nom du pdf
 * @param {int} nb_pages Le nombre de pages
 * @returns {PDF} L'objet PDF initialisé.
 */
var PDF = function(name, nb_pages){
    
    /**
     * Nom du pdf (sans l'extension)
     */
    this.name = name;
    
    /**
     * Son nombre de pages
     */
    this.nb_pages = nb_pages;
    
    /**
     * Liste des objets 'données' placées par l'admin.
     */
    this.data = [];
    
    /**
     * Liste des objets 'textes bruts' ajoutés par l'admin.
     */
    this.texts = [];
}

/**
 * 
 * Méthode convertissant un pdf en JSON.
 * 
 * @returns {String} JSON représentant un PDF de template
 */
PDF.prototype.toJSON = function(){
    var json = '{"name" : "' + this.name + '", ';
    
    json += '"nb_pages" : ' + this.nb_pages + ', "data" : [';
    
        //On ajoute toutes les données
        for (var i = 0; i < this.data.length; i++){
            //Pour chaque donnée, on ajoute son json à la chaine
            json += this.data[i].toJSON();

            if(i != this.data.length - 1)
                json += ',';
        }
    
    json += '], "texts" : [';
    
        //Puis tous les textes
        for (var i = 0; i < this.texts.length; i++){
            //Pour chaque texte brut, on ajoute son json à la chaine
            json += this.texts[i].toJSON();

            if(i != this.texts.length - 1)
                json += ',';
        }
    
    return json + ']}';
}

/**
 * Représente les données ajoutées par l'admin sur un PDF.
 * 
 * @param {type} name Nom de la donnée. Ex. : tutelle
 * @param {type} page N° de page du PDF sur laquelle se trouve la donnée
 * @param {type} x Coordonée x de la donnée sur la page
 * @param {type} y Coordonnée y de la donnée sur la page
 * @returns {Data}
 */
var Data = function(name, page, x, y, width){
    
    /**
     * Nom de la donnée. Ex. : 'tutelle'
     */
    this.name = name;
    
    /**
     * Position de la donnée dans le pdf
     */
    this.pos = new Position(page, x, y);
    
    this.width = width;
    
}

/**
 * Tranforme une donnée en chaine JSON.
 * 
 * @returns {String} JSON représentant la chaine.
 */
Data.prototype.toJSON = function(){
    var json = '{"name" : "' + this.name + '",';
    
    json += '"pos" : ' + this.pos.toJSON() + ', ';
    json += '"width" : ' + this.width;
    
    return json + '}';
}

/**
 * Représente les textes bruts ajoutés par l'amdin
 * 
 * @param {type} value valeud du champ de texte
 * @param {type} page N° de la page du PDF sur laquelle se trouve le texte
 * @param {type} x Coordonnée x du champ sur la page
 * @param {type} y Coordonnée y du champ sur la page
 * @returns {Text} objet représentant un champ de texte.
 */
var Text = function(value, page, x, y){
    
    /**
     * Contenu du champ de texte
     */
    this.value = value;
    
    /**
     * Position du champ dans le PDF
     */
    this.pos = new Position(page, x, y);
}

Text.prototype.toJSON = function(){
    var json = '{"value" : "' + this.value + '",';
    
    json += '"pos" : ' + this.pos.toJSON();
    
    return json + '}';
}

/**
 * Modifie la valeur d'un champ de texte
 * 
 * @param {type} value
 * @returns {undefined}
 */
Text.prototype.setText = function(value){
    this.value = value;
}

/**
 * 
 * Représente la position d'un élément ajouté par l'admin dans un PDF.
 * 
 * @param {type} page N° de la page où l'élément a été ajouté
 * @param {type} x Coordonnée x de l'élément
 * @param {type} y Coordonnée y de l'élément
 * 
 * @returns {Position} L'objet Position représentant ces données.
 */
var Position = function(page, x, y){
    
    /**
    * Numéro de la page du pdf sur laquelle se trouve la donnée
    */
    this.page = page;
    
    /**
     * Position x sur la page
     */
    this.x = x;
    
    /**
     * Position y sur la page
     */
    this.y = y;
    
}

/**
 * Transforme la position dans un pdf en JSON.
 * 
 * @returns {String} JSON représentant la position
 */
Position.prototype.toJSON = function(){
    return '{"page" : ' + this.page + ', "x" : ' + this.x + ', "y" : ' + this.y + '}';
}





/////////////////// FIN DU JAVASCRIPT OBJET / DEBUT JS CLASSIQUE //////////////////////




/**
 * Ajoute un modèleà la liste des modèles disponibles.
 * Appelée par Dropzone.
 * 
 * @param {type} template
 * @returns {undefined}
 */
function addTemplate(template){
    
    //On ajoute le template au gestionnaire
    templateManager.templates.push(new PDF(template.png, template.nbpages));
    
    //on crée l'image à ajouter à la page
    var img = $('<img src="/web/images/thumbnails/' + template.png + '/0.png" alt="miniature du pdf modèle : ' + template.pdf + '" class="thumb" selected="false" pdf="' + template.pdf + '" nb-pages="' + template.nbpages + '" />');
    
    //On ajoute l'événement click
    img.click(setTemplate);
    
    img.mousemove(function (e) {
        $container.css({
            top: e.pageY + 10 + 'px',
            left: e.pageX + 10 + 'px'
        });
    }).hover(function () {
        var link = this;
        $container.show();
        $img.load(function () {
            $img.addClass('img-thumbnail');
            $img.show();
        }).attr('src', $(link).prop('src')).attr('width', '400px');
    }, function () {
        $container.hide();
        $img.unbind('load').attr('src', '').hide();
    });
    
    var div = $('<div class="thumb-container"><i class="fa times-circle-o"></i></div>');
    div.append(img);
    
    //On ajoute l'image au début de la liste
    $('#pdf-thumb').append(div);
}

/**
 * Fonction permettant d'afficher la vignette des pdfs en taille réelle en les survolant
 * @returns {undefined}
 */
function showThumbnailTrueSize(){

    //on crée un div qui contiendra l'image. Il sera positionné en absolu afin de suivre le curseur de l'utilisateur.
    $container = $('<div/>').attr('id', 'imgPreviewWithStyles').append('<img/>').hide().css('position', 'absolute').css('z-index', '1').appendTo('#show-real-size'),

    //On modifie l'image se trouvant dans le conteneur.
    $img = $('img', $container),
            $('.thumb').mousemove(function (e) {
                $container.css({
                    top: e.pageY + 10 + 'px',
                    left: e.pageX + 10 + 'px'
                });
            }).hover(function () {
                var link = this;
                $container.show();
                $img.load(function () {
                    $img.addClass('img-thumbnail');
                    $img.show();
                }).attr('src', $(link).prop('src')).attr('width', '400px');
            }, function () {
                $container.hide();
                $img.unbind('load').attr('src', '').hide();
            });

}

/**
 * Sélectionne un PDF : met les miniatures de toutes ses pages dans la barre gauche de 
 * navigation, et mt sa première page en tant que page principale courante à droite.
 * 
 * @returns {undefined}
 */
function setTemplate(){
    
    //on commence par désélectionner les autres (un seul pdf à la fois)
    $('.thumb').attr('selection', 'false');
    
    //on sélectionne le pdf courant 
    $(this).attr('selection', 'true');
    
    //on vide la zone d'affichage
    $('#nav-thumb').empty();
    $('#full-thumb').empty();
    
    //On affiche le pdf dans la zone adéquate.
    var nb_pages = $(this).attr('nb-pages');
    
    //On ajoute toutes les pages au navigateur
    for (var i = 0; i < nb_pages; i++){
        var img = $('<img src="/web/images/thumbnails/' + $(this).attr('pdf').replace('.pdf', '') + '/' + i + '.png" alt="page ' + i + ' miniature du pdf modèle : ' + $(this).attr('pdf') + '" class="thumb page" selected="false" page="' + i + '" pdf="' + $(this).attr('pdf') + '" png="' + $(this).attr('pdf').replace('.pdf', '') + '" />');
        img.click(setCurrentPage);
        $('#nav-thumb').append(img);
        
        //Si on survole la miniature en grabbing, on change la page courante
        img.mouseover(function(){
            
            //On vérifie qu'on tient bien un élément dans la souris
            if($(this).css('cursor') == 'grabbing'){
                setCurrentPage.call($(this));
            }
            
        });
    }
    
    //On sélectionne la première page (on utilise call pour changer la valeur de this dans la fonction)
    setCurrentPage.call($('.page').first());
    
    //On active le bouton de suppression
    switchControls();
}

/**
 * Change la page courante qui est affichée à droite.
 * 
 * @returns {undefined}
 */
function setCurrentPage(){
    
    //on commence par déseléctbionner les autres (un seul pdf à la fois)
    $('.page').attr('selection', 'false');
    
    //on sélectionne le pdf courant 
    $(this).attr('selection', 'true');
    
    //On vide le contenu du panel principal
    $('#full-thumb').empty();
    
    //On met l'image courante en plein écran
    var n_page = $(this).attr('page');
    var img = $('<img src="/web/images/thumbnails/' + $(this).attr('pdf').replace('.pdf', '') + '/' + n_page + '.png" alt="page ' + n_page + ' du pdf modèle : ' + $(this).attr('pdf') + '" class="full-page" page="' + n_page + '" pdf="' + $(this).attr('pdf') + '" png="' + $(this).attr('pdf').replace('.pdf', '') + '"/>');;
    
    $('#full-thumb').append(img);
    
    //On affiche tous les textes de la page courante
    templateManager.setTexts($(this).attr('png'), $(this).attr('page'));
    
    //On change l'affichage des badges de données
    changeBadges($(this).attr('png'), $(this).attr('page'));
    
}

/**
 * 
 * Modifie l'affichage des badges de données.
 * Supprime de la box les badges qui sont utilisés dans le template.
 * Ajoute sur la page affichée les badges se trouvant sur la page.
 * 
 * @param {type} template_name
 * @param {type} page
 * @returns {undefined}
 */
function changeBadges(template_name, page){
    
    //On supprime tous les badges affichés
    $('.data-badge').remove();
    
    var template_index = -1;
    
    //On parcourt les templates jusqu'à trouver le bon
    templateManager.templates.forEach(function(template, index){
        
        if(template.name == template_name){
            template_index = index;
        }
        
    });
        
    //On parcourt toutes les données de ce template
    var used = [];
    templateManager.templates[template_index].data.forEach(function(data, index){

        //On indique que le badge est utilisé dans le template
        used.push(data.name);
        
        //Si le badge est utilisé sur la page courante, on l'ajoute à la vue
        if(data.pos.page == page){
            
            var badge = $('<span class="badge data-badge" style="position: relative;">' + data.name + '</span>');

            //On calcule ses coordonnées
            var x = data.pos.x * $('.full-page').width() + $('.full-page').offset().left;
            var y = data.pos.y * $('.full-page').height() + $('.full-page').offset().top - 10;

            //On passe le positionnement en absolu
            badge.css('top', y);
            badge.css('left', x);
            badge.css('position', 'absolute');
            badge.width(data.width * $('.full-page').width());

            //On ajoute les listeners au badge
            initBadge(badge);
            
            //On ajoute le badge à la page
            $('#data-badges').append(badge); 
        }
        
    });
        
    //On parcourt toutes les données du dictionnaire
    templateManager.dictionnary.forEach(function(data, index){
        
        //On vérifie si la donnée est utilisée dans le template
        if(used.indexOf(data) == -1){
            //Si ce n'est pas le cas, on l'ajoute à la databox
            var badge = $('<span class="badge data-badge" style="position: relative;">' + data + '</span>');
            initBadge(badge);
            $('#data-badges').append(badge); 
        }
 
    });
    
}

/**
 * Fonction supprimant le template sélectionné.
 * Appelée quand on clique sur le bouton tdelete-template.
 * 
 * Requête POST en AJAX.
 * 
 * @returns {undefined}
 */
function deleteTemplate(){
    
    //On supprime le modèle du gestionnaire
    templateManager.delete($('.thumb[selection="true"]').attr('png'));
    
    //On récupère le nom du modèle sélectionné
    var pdf_name = $('.thumb[selection="true"]').attr('pdf');
    
    //On envoie la requête au serveur en AJAX.
    $.ajax({
        
        url: '/admin/modeles/supprimer',
        method: 'post',
        data: {pdfname: pdf_name, new_config: templateManager.toJSON()},
        
    }).done(function(mess){
        
        //on vide la zone d'affichage
        $('#nav-thumb').empty();
        $('#full-thumb').empty();

        //On supprime l'élément de l'affichage
        $('.thumb[selection="true"]').remove();
        
        //On désactive les bouton
        switchControls();
        
        //On affiche le message envoyé par le serveur
        showDialog('Confirmation', mess);
    });
}

/**
 * When an event occurs, it checks that the cursor is over the pdf.
 * 
 * @returns {boolean / [x, y]} Renvoie false ou les coordonnées de l'élément sur le PDF
 */
function checkPos(event, showMess){
    
    //On récupère la position du curseur
    var cursor_x = event.pageX;
    var cursor_y = event.pageY;

    //On récupère la position du pdf
    var pdf_offset = $('#full-thumb').offset();
    
    //On calcule la différence
    var diff_x = cursor_x - pdf_offset.left;
    var diff_y = cursor_y - pdf_offset.top;
    
    //On récupère ses dimensions
    var pdf_height = $('#full-thumb').innerHeight();
    var pdf_width = $('#full-thumb').innerWidth();
    
    if(diff_x < 0 || diff_y < 0 || diff_x > pdf_width || diff_y > pdf_height){
        
        //On affiche le message envoyé par le serveur
        if(showMess == true)
            showDialog('Erreur !', 'Vous devez mettre l\'élément sur la page du pdf actuellement visualisée !');
        
        return false;
        
    }else{
        return {x: diff_x, y: diff_y};
    }
    
}

/**
 * Fonction qui va permettre à l'utilisateur d'ajouter un champ de texte sur la page courante.
 * 
 * @returns {undefined}
 */
function addText(event){
    
    var new_text_button = $('<button id="add-text" class="btn-template" data-toggle="tooltip" data-placement="bottom" title="Ajouter une zone de texte au modèle"><i class="fa fa-font" aria-hidden="true"></i></button>');

    //On change le curseur de l'utilisateur pour lui indiquer qu'il peut ajouter du texte sur le corps
    $('body').css('cursor', 'cell');
    
    //peu importe où l'on clique (sauf sur le bouton d'annulation), ça crée un événement
    $('body').not('#btn-cancel').on("click", function(subevent){
        
        //On recrée le bouton d'ajout de texte et on l'active
        new_text_button.tooltip({
                    trigger : 'hover'
                });

        $('#btn-cancel').replaceWith(new_text_button);
        new_text_button.click(addText);
                
        //On remet le curseur par défaut
        $('body').css('cursor', 'default');
        $('body').off('click');
        
        var pos = checkPos(subevent, true);
        
        if(pos){
            
            //On récupère le pdf sélectionné
            var pdf = $('.full-page');
            
            //on récupère le nom du pdf courant
            var name = pdf.attr('png');
            
            //On récupère le numéro de la page courante
            var page = pdf.attr('page');
            
            //On ajoute le champ de texte dans le gestionnaire : la fonction renvoie l'id du champ texte
            var id = templateManager.addText(name, page, pos.x, pos.y);
            
            addInput(id, subevent.pageX - pdf.offset().left, subevent.pageY - pdf.offset().top, '');
        }
    
    });
    
    //on remplace le bouton par un bouton d'annulation
    var btn_cancel = $('<button id="btn-cancel" data-toggle="tooltip" data-placement="bottom" title="Annuler" class="btn-template"><i class="fa fa-ban"></i></button>');

    btn_cancel.click(function(subevent){
        
        //On empêche le body de récupérer l'event
        subevent.stopPropagation();
        
        $(this).next('.tooltip').remove();
        
        //On remet l'ancien bouton
        $(this).replaceWith(new_text_button);
        
        //On réactive l'ancien bouton
        new_text_button.click(addText);
        
        new_text_button.tooltip({
                    trigger : 'hover'
                });
        
        //On change le curseur
        $('body').css('cursor', 'default');
        
        //On annule l'événement du click
        $('body').off('click');
    });

    btn_cancel.tooltip({
                    trigger : 'hover'
                });

    $(this).next('.tooltip').remove();
    $(this).replaceWith(btn_cancel);
    
    event.stopPropagation();
    
}

/**
 * Ajoute un input avec un listener pur MàJ le templateManager
 * 
 * @param {type} id ID de l'input
 * @param {type} x position x en absolue sur le body
 * @param {type} y position y en absolue sur le body
 * @param {string} initial_value Valeur initiale du champ (optionnel)
 * @returns {undefined}
 */
function addInput(id, x, y, initial_value){
    
    //On décale x et y par rapport à la position du PDF
    x += $('#full-thumb').offset().left;
    y += $('#full-thumb').offset().top;
    
    //On crée un champ de texte
    var input = $('<div class="drag-text"><i class="fa fa-arrows move-text" aria-hidden="true"></i><input type="text" class="form-control raw-text" id="' + id + '"/><i class="fa fa-times remove-text" aria-hidden="true" style="color: red"></i></div>');
    input.find('input').val(initial_value);

    //On change la position de l'input en fonction de la position de la souris
    input.css('position', 'absolute');
    input.css('top', y + 'px');
    input.css('left', x + 'px');

    //Quand l'input est modifié, on MàJ la valeur du texte
    input.find('input').keyup(function(e){

        templateManager.setText($('.full-page').attr('png'), id, input.find('input').val());

    });
    
    //On rend le tout draggable
    var init_pos = {x: x, y: y};
    
    input.draggable({
        
        start: function(){
            
        },
        
        drag: function(){
            input.css('cursor', 'grabbing');
        },
        
        stop: function(event){
            
            input.css('cursor', 'grab');
            var pos = checkPos(event, false);
            
            //Si on n'est pas sur le PDF, on remet le badge à sa position initiale
            if(pos == false){
                
                //Autrement on la remet là où elle était
                input.css('left', init_pos.x);
                input.css('top', init_pos.y);
                
                showDialog('Erreur !', 'Vous devez déplacer le texte sur la page courante du PDF !');
                
            }else{
                //Autrement, on ajoute la data au templateManager
                var x = input.offset().left - $('.full-page').offset().left;
                var y = input.offset().top - $('.full-page').offset().top;
                
                //On déplace le texte dans le manager
                templateManager.moveText($('.full-page').attr('png'), input.find('input').attr('id'), x, y);
            }
        }
    });
    
    //On change le curseur n fonction des événements
    input.mousedown(function(){
        input.css('cursor', 'grabbing');
    });
    
    input.mouseup(function(){
        input.css('cursor', 'grab');
    });
    
    //On ajoute le listener de suppression du champ texte
    input.find('.remove-text').click(function(){
        var id_remove = input.find('input').attr('id');
        
        //On supprime l'élément du gestionnaire
        templateManager.deleteText($('.full-page').attr('png'), id_remove);
        
        //Puis on supprime l'élément de la vue
        input.remove();
    });

    //On l'ajoute sur le pdf à la position de la souris
    $('#full-thumb').append(input);
}

function switchControls(){
    if($('.thumb[selection="true"]').length == 0){
        //On désactive les boutons
        $('.btn-template').attr('disabled', '');
    }else{
        //On active les boutons
        $('.btn-template').removeAttr('disabled');
    }
}

function saveTemplate(){
    //On envoie l'objet courant au serveur, et il s'occupe de mettre à jour le fichier de configuration
    $.ajax({
        
        url: '/admin/modeles/sauvegarder',
        method: 'post',
        data: {templates: templateManager.toJSON()},
        
    }).done(function(mess){
        
        //On affiche le message envoyé par le serveur
        showDialog('Confirmation', mess);
       
    });
}

function initBadge(badge){
    
    var init_pos = {x: 0, y: 0};
    
    //on rend le badge drag-and-droppable
    badge.draggable({
        
        start: function(){
            init_pos = {x: badge.css("left"), y: badge.css("top")};
        },
        
        drag: function(){
            badge.css('cursor', 'grabbing');
        },
        
        stop: function(event){
            
            badge.css('cursor', 'grab');
            var pos = checkPos(event, false);
            
            //Si on n'est pas sur le PDF, on remet le badge à sa position initiale
            if(pos == false){
                
                //Si donnée est sur la boxe des badges, on la supprime du PDF
                var badges = $('#data-badges');
                var pos_badges = badges.offset();
                
                if( (event.pageX > pos_badges.left && event.pageX < pos_badges.left + badges.innerWidth()) && 
                        (event.pageY > pos_badges.top && event.pageY < pos_badges.top + badges.innerHeight()) ){
                    
                    //On remet la donnée dans la boîte à badges
                    badge.attr('style', '');
                    badge.css('position', 'relative');
                    $('#data-badges').prepend(badge);
                    
                    //Et enfin on supprime la donnée
                    templateManager.deleteData(badge.text(), $('.full-page').attr('png'));
                    
                }else{
                    //Autrement on la remet là où elle était
                    badge.css('left', init_pos.x);
                    badge.css('top', init_pos.y);
                    
                    //Et on affiche un message
                    showDialog('Erreur !', 'Vous devez déposer les données soit sur le PDF, soit sur la boîte à données !');
                }
                
            }else{
                //Autrement, on ajoute la data au templateManager
                var x = badge.offset().left - $('.full-page').offset().left;
                var y = badge.offset().top - $('.full-page').offset().top;
                
                //On repasse le positionnement en absolu
                badge.css('top', badge.offset().top);
                badge.css('left', badge.offset().left);
                badge.css('position', 'absolute');
                
                templateManager.addData(badge.text(), $('.full-page').attr('png'), $('.full-page').attr('page'), x, y, badge.width());
            }
        }
    });
    
    //on rend le badge resizable
    badge.resizable({handles: 'e'});
    
    //On ajoute un listener quand la taille du badge est modifiée
    badge.resize(function(){
        var x = badge.offset().left - $('.full-page').offset().left;
        var y = badge.offset().top - $('.full-page').offset().top;
        
        //On change la taille du badge dans le template
        templateManager.addData(badge.text(), $('.full-page').attr('png'), $('.full-page').attr('page'), x, y, badge.width());
    });
    
    //On change le curseur n fonction des événements
    badge.mousedown(function(){
        badge.css('cursor', 'grabbing');
    });
    
    badge.mouseup(function(){
        badge.css('cursor', 'grab');
    });
}

$(document).ready(function(){
    
    //On crée l'objet qui va nous servir à gérer les modifications de template
    templateManager = new MyTemplateManager();
    
    //On récupère les données du serveur pour initialiser le gestionnaire
    templateManager.feed();
    
    //On modifie les fichiers acceptés par Dropzone.
    Dropzone.prototype.defaultOptions.acceptedFiles = '.pdf';
        
    //On active la fonction
    showThumbnailTrueSize();
    
    //Quand on clique sur un pdf, on le sélectionne et on affiche le pdf.
    $('.thumb').click(setTemplate);
    
    //On ajoute le listener au bouton de suppression
    $('#delete-template').click(deleteTemplate);
    
    //Listener du bouton d'ajout de texte
    $('#add-text').click(addText);
    
    //Listener de sauvegarde
    $('#save-template').click(saveTemplate);
    
    //On initialise les contrôles
    switchControls();
    
    //On crée une variable super globale pour Dropzone.
    window.add = addTemplate;

});