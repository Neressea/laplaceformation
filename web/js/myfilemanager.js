/* 
 * Un gestionnaire de fichiers simpliste permettant à l'utilisateur de gérer facilement ses fichiers.
 */

/**
 * Classe javascript représentant un gestionnaire de fichier.
 * Il prend en paramètre l'objet HTML sur lequel on va construire le file manager
 * et le json de la structure des fichiers.
 * 
 * Structure du json pour un dossier utilisateur 
 * [
 *      {"name" : "abc", "type" : "dir", children : [ 
 *              {"name" : "subfile", "type" : "png", children : [] } 
 *      ]}, 
 *      {"name" : "coucou", "type" : "pdf", children : [] }
 * ]
 * 
 * @param {objet HTML} element Elément sur lequel on va créer le file manager
 * @param {JSON} tree_json Arbre des fichier sen format JSON
 * @returns {MyFileManager} Objet file manager.
 */
MyFileManager = {
    
    /**
     * Arbre représentant la structure totale des fichiers de l'utilisateur 
     *(comme ça la navigation ne se fait que du côté client).
     * Permet les retours en arrière.
     */
    tree: null,
    
    /**
     * Arbre des fichiers courant
     * (pratique pour l'affichage et l'avancée en profondeur).
     */
    current_tree: null,
    
    /**
     * Chemin courant à partir de la racine du fichier utilisateur
     * (affiché à l'utilisateur et utile dans les requêtes AJAX).
     */
    current_path: '/',
    
    /**
     * Début des url.
     */
    url: ""
}

/**
 * Fonction listant les fichiers dans le file manager.+
 * 
 * @param {type} files Tableau des fichiers (json parsé)
 * @returns {undefined} Rien. Modifie l'affichage directement.
 */
function listFiles(files){
    if(files == null) return;

    //Pour chaque fichier, on a son nom, son type (image / pdf / folder) et ses enfants (vide si c'est un fichier).
    for(var i = 0; i<files.length; i++){
        var element = files[i];

        var icon = '<i class="fa fa-image" aria-hidden="true"></i>';
        var type = '';
        if(element.type == 'pdf'){
            icon = '<i class="fa fa-file-pdf-o" aria-hidden="true"></i>';
        }else if(element.type == 'dir'){
            icon = '<i class="fa fa-folder" aria-hidden="true"></i>';
            type = 'folder';
        }

        if(type != 'folder'){
            $('#files-container').append('<div class="file singleton" selection="false">'+icon+'<br /><span class="file-info">'+element.name+'</span></div>');
        }else{
            $('#folder-container').append('<div class="file folder" selection="false">'+icon+'<br /><span class="file-info">'+element.name+'</span></div>');
        }
    }

    //On ajoute le listener sur tous les dossiers
    $('.folder').dblclick(function(){
        navigate($(this));
    });

    $('.singleton').dblclick(function(){
        $('#btn-download').click(); //On force le téléchargement
        $(this).attr('selection', 'false'); //On considère que l'utilisateur télécharge direct

        //On désactive les contrôles
        switchControls();
    });

    //On ajoute une action quand on clique sur les fichiers
    $('.file').click(fileListener);
}

/**
 * Fonction de navigation permettant de parrcourir les dossiers en profondeur.
 * Pour remonter dans l'arbre des fichiers, on utilise la fonction appliquée
 * au bouton goback.
 * 
 * @param {HTML} folder Elément HTML sur lequel l'utilisateur a double-cliqué.
 * @returns {undefined} Ne retourne rien, modifie direct l'affichage.
 */
function navigate(folder){
    
    //on récupère le nom du dossier de l'élément HTML
    var folder_name = folder.find('.file-info').text();
    var children = null;

    //On parcourt l'arbre courant jusqu'à trouver le fils avec le bon nom
    for(var i = 0; i<MyFileManager.current_tree.length; i++){
        if(MyFileManager.current_tree[i].name == folder_name){
            //Une fois trouvé, on le sauvegarde
            children = MyFileManager.current_tree[i].children;
            break;
        }
    }
    
    //On change le dossier courant pour le fils qui a été trouvé. 
    MyFileManager.current_tree = children;
    
    //On met à jour le chemin courant
    MyFileManager.current_path += folder_name + '/';

    //On met à jour l'affichage 
    $('#show-path').text(MyFileManager.current_path);

    //On affiche le bouton pour retourner en arrière
    $('#btn-goback').show();
    
    refresh();

    //On désactive les boutons de navigation
    switchControls();
    
}

/**
 * Listener à ajouter sur tous les éléments HTML représentant les fichiers.
 * 
 * @param {Event javascript} event Evénement qui a eu lieu sur l'élément.
 * @returns {undefined} Rien.
 */
function fileListener(event){
    
    //Si on maintenu MAJ ou ctrl appuyé, on ajoute le fichier à ceux sélectionnés
    if ($('[selection="true"]').length == 0 || event.shiftKey || event.ctrlKey ) {
        
        selectFile(this);
    
    }else{
        
        //Sinon on commencer par tout déslectionner.
        $('.file').attr('selection', 'false');

        //Puis on ne sélectionne que le bon fichier
        selectFile(this);
        
    }
    
}

function selectFile(file){
    
    //On inverse la sélectino des fichiers
    if($(file).attr('selection') == 'false'){
        $(file).attr('selection', 'true');
    }else{
        $(file).attr('selection', 'false');
    }

    //On met à jour les boutons de contrôle.
    switchControls();
    
}

/**
 * Vérifie les éléments sélectionnés et met à jour les boutons de contrôle.
 * 
 * @returns {undefined}
 */
function switchControls(){
    
    //Si on a des fichiers/dossiers sélectionnés, on active le bouton
    if($('[selection="true"]').length > 0){
        $('#btn-del').attr('disabled', null);
        $('#btn-download').attr('disabled', null);
        $('#btn-rename').attr('disabled', null);
        
        //Pour le bouton de renommage, on doit avoir EXACTEMENT un fichier/dossier sélectionné.
        if($('[selection="true"]').length > 1)
            $('#btn-rename').attr('disabled', 'true');
    }else{
        //Sinon si aucun fichier/dossier n'est sélectionné, on désactive les boutons
        $('#btn-del').attr('disabled', 'true');
        $('#btn-download').attr('disabled', 'true');
        $('#btn-rename').attr('disabled', 'true');
    }
    
}

/**
 * Retourne dans le dossier parent dans l'arbre total des fichiers.
 * Fonction appelée au clic sur le bouton goback.
 * 
 * @returns {undefined}
 */
function levelUp(){
    
    //On change la valeur du current_path : on supprime le dernier élément du chemin
    MyFileManager.current_path = MyFileManager.current_path.split('/');
    MyFileManager.current_path.pop();
    MyFileManager.current_path.pop();
    MyFileManager.current_path = MyFileManager.current_path.join('/') + '/';
    $('#show-path').text(MyFileManager.current_path);

    //On change la valeur de current_tree
    MyFileManager.current_tree = MyFileManager.tree;
    MyFileManager.current_path.split('/').forEach(function(elemp){

        //On cherche le bon fils
        MyFileManager.current_tree.forEach(function(elemt){
            if(elemp == elemt.name){
                MyFileManager.current_tree = elemt.children;
            }
        });

    });

    //On rafraichit l'affichage avec le nouvel arbre
    refresh();

    //On désactive les boutons de navigation
    switchControls();

    //Si on est à la racine du user, on cache le bouton
    if(MyFileManager.current_path == '/'){
        $('#btn-goback').hide();
    }
    
}

/**
 * Ajoute un fichier à l'affichage en fonction de son JSON.
 * 
 * @param {type} json
 * @returns {undefined}
 */
function addFile(json){
    
    //On parse le json en objet javascript
    var file = jQuery.parseJSON(json);

    //On ajoute le fichier à l'arbre courant
    MyFileManager.current_tree.push(file);

    //Puis on l'ajoute à l'arbre total
    var tmp_tree = MyFileManager.tree;

    //On parcourt tout l'arbre jusqu'à arriver au niveau courant
    MyFileManager.current_path.split('/').forEach(function(elemp){
        //On cherche le bon fils
        tmp_tree.forEach(function(elemt){
            if(elemp == elemt.name){
                tmp_tree = elemt.children;
            }
        });
    });

    //Une fois le bon sous-arbre trouvé, on ajoute le fichier
    tmp_tree.push(file);

    //On rafraichit la page
    refresh();
}

/**
 * Rafraichit les données affichées en fonction de la valeur de l'arbre courant.
 * @returns {undefined}
 */
function refresh(){
    //On vide  l'affichage
    $('#files-container').empty(); 
    $('#folder-container').empty();
    
    //on remplit de nouveau l'interface
    listFiles(MyFileManager.current_tree);
}

/**
 * Crée un nouveau dossier et l'ajoute au json avant de rafraichir la page.
 * Appelée quand on clique sur le bouton btn-fldr.
 * 
 * @returns {undefined}
 */
function addFolder(){
    
    //On crée les éléments HTML du dossier
    var div = $('<div class="file folder" selection="false"></div>');
    
    //Input pour le nom du nouveau dossier
    var input = $('<input type="text" placeholder="Nom du dossier" />');
    var icon = $('<i class="fa fa-folder" aria-hidden="true"></i><br/>');
    div.append(icon);
    div.append(input);

    //On ajoute le  nouveau dossier au début des dossiers
    $('#folder-container').prepend(div);
    
    //On donne le focus au nouveau nom du dossier.
    input.focus();

    //Quand il y a une entrée clavier sur l'input du nom de dossier
    input.keyup(function(e){
        
        //Si c'est ENTREE, alors on crée le dossier
        if(e.keyCode == 13){
            
            //Si le champ est vide, on supprime le nouveau dossier
            if(input.val() == ''){
                div.remove();
            }else{
                
                //Sinon on crée le sous-dossier sur le serveur en AJAX
                $.ajax({
                    url: MyFileManager.url + 'creer-dossier',
                    method: 'post',
                    data: {path: MyFileManager.current_path, folder_name: input.val().replace(/ /g, '_').replace("'", "_")},
                }).done(function(json){

                    //Maintenant on ajoute le fichier à la liste
                    addFile(json);
                    


                });
                
            }
        }

    });
}

/**
 * Fonction permettant de renommer un fichier/dossier préexistant.
 * Appelée quand on clique sur btn-rename
 * 
 * @returns {undefined}
 */
function renameFile(){
    
    //on récupère le fichier sélectionné
    var file = $('[selection="true"]');
    var span = file.find('.file-info');
    var input = $('<input type="text" value="' + span.text() + '" />');
    var element = $(this);

    //On remplace la description par un input 
    span.replaceWith(input);
    input.focus();
    input.select();

    input.keyup(function(e){

        //On vérifie que c'est bien la touche ENTREE qui a été appuyée
        if(e.keyCode == 13){
            
            //Si le champ est vide, on remet l'ancier span
            if(input.val() == ''){
                //On remet l'ancien span s'il n'y a pas de nouveau nom qui ont été choisis
                input.replaceWith(span);
            }else{
                //Autrement, on renomme le fichier (que ce soit un dossier ou un fichier).
                $.ajax({
                    url: MyFileManager.url + 'renommer',
                    method: 'post',
                    data: {path: MyFileManager.current_path, old_name: span.text(), new_name: input.val().replace(/ /gi, '_').replace("'", "_")},
                }).done(function(msg){

                    //On modifie le current_tree
                    for(var i = 0; i<MyFileManager.current_tree.length; i++){
                        if(MyFileManager.current_tree[i].name == span.text()){
                           MyFileManager.current_tree[i].name = input.val().replace(/ /gi, '_').replace("'", "_");
                            break;
                        }
                    }

                    //On modifie le tree
                    var tmp_tree = MyFileManager.tree;

                    //On parcourt tout l'arbre jusqu'à arriver au niveau courant
                    MyFileManager.current_path.split('/').forEach(function(elemp){
                        //On cherche le bon fils
                        tmp_tree.forEach(function(elemt){
                            if(elemp == elemt.name){
                                tmp_tree = elemt.children;
                            }
                        });
                    });

                    for(var i = 0; i<tmp_tree.length; i++){
                        if(tmp_tree[i].name == span.text()){
                            tmp_tree[i].name = input.val().replace(/ /gi, '_').replace("'", "_");
                            break;
                        }
                    }

                    //On refresh
                    refresh();

                    //On remet le bouton et on l'active
                    $('#btn-cancel').replaceWith(element);
                    element.click(renameFile);

                });
            }
        }
    });

    //on remplace le bouton par un bouton d'annulation
    var btn_cancel = $('<button id="btn-cancel" title="Annuler" class="btn btn-primary"><i class="fa fa-ban"></i></button>');

    btn_cancel.click(function(){
        $(this).replaceWith(element);
        element.click(renameFile);
        input.replaceWith(span);
    });

    $(this).replaceWith(btn_cancel);
}

/**
 * Supprime les fichiers sélectionnés.
 * 
 * @returns {undefined}
 */
function deleteFile(){

    //on récupère le nom de tous les fichiers sélectionnés dans un tableau
    var files_names = $('[selection="true"]').find('.file-info').map(function() {
         return $(this).text();
      }).get();

    //On supprime le(s) fichiers sur le serveur en AJAX
    $.ajax({

        url: MyFileManager.url + 'supprimer',
        method: 'post',
        data: {files: files_names, subpath: MyFileManager.current_path},

    }).done(function(msg){

        //On change la valeur de current_tree
        files_names.forEach(function(element, index){
            //On supprime la valeur de l'arbre courant
            for(var i = 0; i<MyFileManager.current_tree.length; i++){
                if(MyFileManager.current_tree[i].name == element){
                    MyFileManager.current_tree.splice(i, 1);
                    break;
                }
            }
        });

        //On modifie aussi le tree
        var tmp_tree = MyFileManager.tree;

        //On parcourt tout l'arbre jusqu'à arriver au niveau courant
        MyFileManager.current_path.split('/').forEach(function(elemp){
            //On cherche le bon fils
            tmp_tree.forEach(function(elemt){
                if(elemp == elemt.name){
                    tmp_tree = elemt.children;
                }
            });
        });

        //Puis on supprime ce qu'il faut dans le tree
        files_names.forEach(function(element, index){
            //On supprime la valeur de l'arbre courant
            for(var i = 0; i<tmp_tree.length; i++){
                if(tmp_tree[i].name == element){
                    tmp_tree.splice(i, 1);
                    break;
                }
            }
        });

        //On rafraichit le file manager
        refresh();

        //On remet les boutons en disable
        switchControls();

    });
}

/**
 * Provoque le téléchargement des fichiers sélectionnés.
 * Appelé quand on clique sur btn-download ou quand 
 * on double-clique sur un fichier.
 * 
 * @returns {undefined}
 */
function download(){

    //on récupère tous les fichiers sélectionnés
    var files_names = $('[selection="true"]').find('.file-info').map(function() {
         return $(this).text();
    }).get();

    //On concatène les éléments en les séparant par |
    var param = '';
    $.each(files_names, function(index, value){
        param += value + '|';
    });
    param = param.slice(0, -1); //On supprime le dernier caractère (le | en trop).

    //On change l'url
    window.open(MyFileManager.url + 'telecharger/'  + param + '/' + MyFileManager.current_path, '_blank');
}



