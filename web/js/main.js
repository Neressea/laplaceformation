/* 
 * Fonctions utilisées dans plusieurs pages.
 */

function activateCalendar(){
    $('.calendar').datepicker({
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        dateFormat: 'dd/mm/yy',
        onClose: function(dateText, inst) { 
            $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, inst.selectedDay));
        }
    });
}

function initHelp(){
    //On cache toutes les aides

    $('.show-help').each(function(index, element){
       //On affiche tous les boutons
        var new_elem = $.parseHTML('<span class="label label-info show-help">Afficher aide <i class="fa fa-caret-down" aria-hidden="true"></i></span>');
        $(element).replaceWith(new_elem);
        $(new_elem).click(displayHelp); 
    });
}

function displayHelp(){
    //On récupère les prochains frères du bouton qui ont la classe help et on les cache.
    var help_elem = $(this).nextAll('.help');

    //On change le contenu du bouton
    var new_elem;
    if($(help_elem).is(':hidden')){
        new_elem = $.parseHTML('<span class="label label-info show-help">Cacher aide <i class="fa fa-caret-up" aria-hidden="true"></i></span>');
    }else
        new_elem = $.parseHTML('<span class="label label-info show-help">Afficher aide <i class="fa fa-caret-down" aria-hidden="true"></i></span>');

    if($(help_elem).css('display') == 'none')
        $(help_elem).fadeIn('slow');
    else
        $(help_elem).fadeOut('fast');

    $(this).replaceWith(new_elem);
    $(new_elem).click(displayHelp);
}

function showDialog(title, mess){
    var div = $('<div id="template-modal" class="modal fade" aria-labelledby="myLargeModalLabel" aria-hidden="true">'
               + '<div class="modal-dialog modal-lg" role="document">'
               +   '<div class="modal-content">'
               +       '<div class="modal-header">'
               +           '<button type="button" class="close" data-dismiss="modal" aria-label="Close">'
               +               '<span aria-hidden="true">&times;</span>'
               +           '</button>'
               +           '<h4 class="modal-title">' + title + '</h4>'
               +       '</div>'  
               +       '<div class="modal-body">'
               +           '<p>' + mess + '</p>'
               +       '</div>'
               +       '<div class="modal-footer">'
               +           '<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>'
               +       '</div>'
               +   '</div>'
               + '</div>'
              + '</div>');
      
    //On ajoute ladiv à la page
    $('body').append(div);
    $('#template-modal').modal('show');
    
    //on supprime l'élément quand on le cache
    $('#template-modal').on('hidden.bs.modal', function (e) {
        div.remove();
    });
}

$(document).ready(function() {
    
        //On active les boutons déroulant (ex. : bouton pour changer les droits sur la liste des profils utilisateurs)
        $('.menu').dropit();

        //On active les calendriers : les utilisateurs peuvent changer l'année et le mois directement
        activateCalendar();

        //On active les belles tooltips bootstraps :p
        $('[title]').attr("data-toggle", "tooltip"); //On indique que toutes les balises avec title deviennent des title de boostrap
        $('[title]').attr("data-placement", "bottom"); //On indique que toutes les tooltips s'afficheront en-dessous
        $('[data-toggle="tooltip"]').tooltip({
                                        trigger : 'hover'
                                    }); //jQuery initialise les tooltips

        //Ajout d'un plugin à datatable : recherche générale sur tous les tableaux de la page
       jQuery.fn.dataTableExt.oApi.fnFilterAll = function(oSettings, sInput, iColumn, bRegex, bSmart) {
            var settings = $.fn.dataTableSettings;

            for ( var i=0 ; i<settings.length ; i++ ) {
              settings[i].oInstance.fnFilter( sInput, iColumn, bRegex, bSmart);
            }
        };

        var dt = $('.myTable').dataTable({paging: false, sDom: '<"top">tip', "columnDefs": [ {"targets": [7, 8, 9], "orderable": false}], "order": [[ 1, "asc" ]]});

        //On ajoute cette fonctonnalité à notre input 
        $("#search_all_datatable").keyup(function(){
            // Filter on the column (the index) of this element
            dt.fnFilterAll(this.value);
        });

        //Si une recherche est faite sur un tableau et que celui-ci est vide, on le cache
        $("#search_all_datatable").keyup(function(){

            //On récupère toutes les tables
            var tables = $('.myTable');

            //On les parcourt et on regarde si elles sont vides
            tables.each(function(index, element){
                //On récupère les cellules de la table
                var cells = $(element).find('td');

                //On vérifie que la table est bien vide
                if(cells.length == 1 && cells[0].innerHTML === 'No matching records found'){
                    $(element).parent("div").hide('slow');
                }else{
                    $(element).parent("div").show('slow');
                }

            });
        });

        //On initialise les boutons d'aide
        initHelp();
        
        //S'il y a un message de symfony, on le remplace par un message maison
        if( $('.alert').length && window.location.pathname != '/admin/dashboard'){
            var title = $('.alert').find('strong').text();
            var msg = $('.alert').clone()    //clone the element
                                .children() //select all the children
                                .remove()   //remove all the children
                                .end()  //again go back to selected element
                                .text();
            
            $('.alert').remove();
            
            showDialog(title, msg)
        }
});