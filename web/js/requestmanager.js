
/**
 * Fonction permettant d'afficher la vignette des pdfs en taille réelle en les survolant
 * @returns {undefined}
 */
function showThumbnailTrueSize(){

    //on crée un div qui contiendra l'image. Il sera positionné en absolu afin de suivre le curseur de l'utilisateur.
    $container = $('<div/>').attr('id', 'imgPreviewWithStyles').append('<img/>').hide().css('position', 'absolute').css('z-index', '1').appendTo('#show-real-size'),

    //On modifie l'image se trouvant dans le conteneur.
    $img = $('img', $container),
            $('.thumb').mousemove(function (e) {
                $container.css({
                    top: e.pageY + 10 + 'px',
                    left: e.pageX + 10 + 'px'
                });
            }).hover(function () {
                var link = this;
                $container.show();
                $img.load(function () {
                    $img.addClass('img-thumbnail');
                    $img.show();
                }).attr('src', $(link).prop('src')).attr('width', '400px');
            }, function () {
                $container.hide();
                $img.unbind('load').attr('src', '').hide();
            });

}

function showTemplates(){
    if($('#templates').css('display') == 'none'){
        $('#templates').css('display', '');
        $('#show-templates').text('Cacher les formulaires');
    }else{
        $('#templates').css('display', 'none');
        $('#show-templates').text('Télécharger un formulaire pré-rempli');
    }
}

function downloadTemplate(){
    
    //On change l'url
    window.open(window.location + '/telecharger/' + $(this).attr('name'), '_blank');
}

function createSame(){
    window.location = window.location.pathname.replace('voir', 'nouvelle-demande');
}

$(document).ready(function(){
    
    $('#show-templates').click(showTemplates);
    showThumbnailTrueSize();
    $('.thumb').click(downloadTemplate);
    
    $('#create-same').click(createSame);
    
});

