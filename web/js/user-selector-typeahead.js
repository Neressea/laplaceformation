
$(function () {
    
    var api_url = $('#api_find_users').attr('data-url');
    
    var params = {
        'source': source,
        'matcher': match,
        'updater': update,
        'highlighter': highlight
    };
    
    function source(query, process)
    {
        var data = { 'query': query };
        
        $.post(api_url, data, function (results) {
            var items = [];
            for(var i = 0, len = results.length; i < len; i++)
            {
                var username = results[i].username,
                    fullName = results[i].fullname;
                
                items.push('"' + fullName + '" ' + username);
            }
            process(items);
        }, 'json');
    }
    
    function update(item)
    {
        return item.match(/\s(\w+)$/)[1];
    }
    
    function highlight(item)
    {        
       return item;
    }
    
    function match()
    {
        // results are queried from database, they always match
        return true;
    }
    
    $('input.username-typeahead').each(function () {
        $(this).typeahead(params);
    });

});

$(document).ready(function() {
    $('#search_form_user').on('input',function(e){
        //TODO: enlever de l'affichage les données qui ne correspondent pas à la recherche
    });
});