

$(function () {
    
    var api_url = $('#api_laplace_alert').attr('data-url');
    
    $('.laplace-alert').each(function () {
        $(this).on('close', function () {
            var data = { alert: $(this).attr('data-alert-id') };
            
            $.post(api_url, data, null, 'json');
        });
    });

});
