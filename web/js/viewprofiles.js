

$(document).ready(function(){
    
    //On va permettre à l'administrateur de modifier la présence d'un utilisateur en utilisant l'AJAX
   $('.drop-presence').click(function(){
       
       var elem = $(this);
       
        $.ajax({
            
            //On indique au serveur qu'on veut modifier la présence d'un utilisateur, et on renseigne son username dans l'url.
            url: '/admin/profils/modifier/' + elem.attr('username') + '/change-presence',
            method: 'post',
            
        }).done(function(msg){
            
            //Traitement de la réponse du serveur
            
            //On crée une alerte bootstrap que l'on ajoute sous le titre de la page
            var mess = 'La présence de l\'utilisateur ' + msg['name'] + ' a bien été modifiée à l\'état : ' + msg['presence'] + '.';
            showDialog('Modification enregistrée !', mess);
            
            //On change le contenu des boutons en fonction du retour du serveur. La vue ne sert qu'à afficher les résultats.
            elem.parent().find('button').html(msg['presence'] + '<span class="caret"></span>');
            elem.find('a').text(msg['list']);
        });
    });
    
    //On permet à l'administrateur de modifier les droits d'un utilisateur en utilisant l'AJAX
    $('.drop-droits').click(function(){
        
        var elem = $(this);
        
        //On récupère les nouveaux droits qui seront accordés à l'utilisateur.
        var new_droits = elem.find('a').text();
        
        $.ajax({
            
            url: '/admin/profils/modifier/' + elem.attr('username') + '/change-droits',
            method: 'post',
            data: {droits: new_droits}
            
        }).done(function(msg){
            
            var mess = 'Les droits de l\'utilisateur ' + msg['name'] +' ont bien été modifiés. Il est maintenant ' + msg['droits'] + '.';
            showDialog('Modification enregistrée !', mess);
            
            //On modifie les droits actuels de l'utilisateur
            elem.parent().parent().find('button').html(msg['droits'] + '<span class="caret"></span>');
            
            //Et on modifie les deux choix de la liste
            var lis = elem.parent().find('li');
            
            $(lis[0]).find('a').text(msg['choix1']);
            $(lis[1]).find('a').text(msg['choix2']);

        });
    });
    
});

